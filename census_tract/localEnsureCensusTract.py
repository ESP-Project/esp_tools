#!/usr/bin/env python3
# -*- coding: utf-8 -*-
## Sep-2022  - updated to use local geocoding database extention instead of the restful svc @ census.gov

import sys, ssl
#need to install configparser in the esp virtualenv: pip install configparser
import configparser
import psycopg2
import json
#import urllib
#import urllib.parse
#import urllib.request as ur

try:
    eMsg = ''
    config = configparser.ConfigParser()
    config.read('census.properties')
    sqlUser = config['sql']['user']
    db = config['sql']['database']
    sqlPassword = config['sql']['password']
    del_addr = config['options']['delete']

    con1 = None
    con2 = None
    con1 = psycopg2.connect(host='localhost', dbname=db, user=sqlUser, password=sqlPassword)
    con2 = psycopg2.connect(host='localhost', dbname=db, user=sqlUser, password=sqlPassword)
    con1.set_session(autocommit=True)
    con2.set_session(autocommit=True)
    cursor1 = con1.cursor()
    cursor2 = con2.cursor()
    context = ssl._create_unverified_context()

    qry = (" SELECT DISTINCT p.id, p.address1, p.city, p.state, p.zip "
        +  " FROM public.emr_patient p "
        +  " INNER JOIN gen_pop_tools.clin_enc ce ON p.id = ce.patient_id "
        +  " LEFT OUTER JOIN gen_pop_tools.patient_census_tract ct ON p.id = ct.patient_id "
        +  " WHERE ct.patient_id is NULL "
        +  " AND p.state = 'MA' "
        +  " AND p.address1 is not null "
        #+  " AND ce.date >= '01-JAN-2020' "
        +  " AND p.address1 not in ('UNK', 'UNKNOWN', 'UNKOWN', 'X', 'HOMELESS', 'Homeless') "
        +  " ORDER BY p.id LIMIT 100000; ")

    eMsg = qry
    ##print('\n\nSelecting patients without census_tract')
    ###print(qry)
    cursor1.execute(qry)
    row = cursor1.fetchone()
    row = cursor1.fetchone()
    while(row != None):
        census_tract = -1
        try:
            eMsg="Calling US census geocoder" 
            #We take the EMR address and get lat,lon from US census
            patient_id = row[0]
            print('\n\n')
            print(patient_id)
            if(row[1] is None or row[2] is None or row[3] is None or row[4] is None):
            # Insufficient address data, set c to zero
                c = 0            
            else:
                address = row[1]
                #Escape single quotes in the address
                address = address.replace("'", "''")
                city = row[2]
                state = row[3]
                zip = row[4]
                #oneline_address = urllib.parse.quote_plus(address + city + state + zip)
                oneline_address = address + " " + city + " " + state + " " + zip
                #print ('oneline_address=' + oneline_address)
                ##
                ## feed in oneline address and get long and lat
                ##
                qry = ("SELECT g.rating, ST_X(g.geomout) As lon, ST_Y(g.geomout) As lat, "
                    + " (addy).address As stno, (addy).streetname As street, "
                    + " (addy).streettypeabbrev As styp, (addy).location As city, (addy).stateabbrev As st,(addy).zip "
                    + " FROM geocode('" + oneline_address + "', 1) As g;")
                print (qry) 
                eMsg = qry
                cursor2.execute(qry)
                result2 = cursor2.fetchone()
                #print(result2)
                long = result2[1]
                lat = result2[2]
                #print (lat)
                #print (long)
                # Insufficient address data, set c to zero
                if(result2[1] is None or result2[2] is None):
                    c = 0            
                    census_tract = -1
                else:
                    c = 1
                    #print('patient id:' + str(patient_id) + ' num address matches: ' + str(c))
                    x = str(lat) #lat
                    y = str(long) #long
                    #print (x + y)
                    ##print (y)
                    ##
                    ## feed in long and lat and get Censustract
                    ##
                    qry = ("SELECT get_tract(ST_Point(" + y + ',' + x + "), 'tract_id' ) As tract_id; ")
                    ###print (qry) 
                    eMsg = qry
                    cursor2.execute(qry)
                    result3 = cursor2.fetchone()
                    ##print(result3)
                    census_tract = result3[0]
                    ###print(census_tract)
                    ##except Exception as e0:
                    ##print('msg 0: ' + str(eMsg))
                    ##print('Exception 0: ' + str(e0))
                    # query patient_census_tract - if exists - UPDATE else INSERT
            qry = "SELECT patient_id FROM gen_pop_tools.patient_census_tract WHERE patient_id=" + str(patient_id)
            eMsg = qry
            ###print(qry)
            cursor2.execute(qry)
            if(cursor2.fetchone()):
                #print('Update patient: ' + str(patient_id) + ' census:' + str(census_tract))
                qry = ("UPDATE gen_pop_tools.patient_census_tract SET census_tract_id=" + str(census_tract)
                     + ", lat='" + x + "', long='" + y 
                     + "' WHERE patient_id=" + str(patient_id) + ";" )
            else:
                #print('Insert patient: ' + str(patient_id) + ' census:' + str(census_tract))
                qry = ("INSERT INTO gen_pop_tools.patient_census_tract(patient_id, census_tract_id, lat, long) "
                       + "VALUES (" + str(patient_id) + "," + str(census_tract) + ",'" + x + "', '" + y + "');" )
            cursor2.execute(qry)
            eMsg = qry
            ###print(qry)
            ###print('\n')
        except Exception as e1:
            print('msg 1: ' + eMsg)
            print('Exception 1: ' + str(e1))

        row = cursor1.fetchone()

except Exception as e2:
    print('msg 2: ' + eMsg)
    print('Exception 2: ' + str(e2))

finally:
    if con1:
        con1.close()
    if con2:
        con2.close()

