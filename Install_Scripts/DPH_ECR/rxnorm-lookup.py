#!/usr/bin/python3

##
## UPDATE THE CONSTRAINTS BEFORE USING IF THIS HAS NOT ALREADY BEEN DONE
##    ALTER TABLE static_rxnorm DROP CONSTRAINT static_rxnorm_rxcui_key;
##    ALTER TABLE static_rxnorm ADD CONSTRAINT static_rxnorm_name UNIQUE(name);
##

import requests
import csv
import subprocess
from subprocess import PIPE


def run_esp_sql(sql):
    db_port = "5432"
    db_username = "esp"
    db_name = "esp"

    command = ["psql", "-p", db_port, "-U", db_username, "-d", db_name, "-c", sql]
    #print(command)

    try:
        result = subprocess.run(command, stdout=PIPE, stderr=PIPE)
        #print(result)
        print(result.stdout)
        if result.stderr.startswith("ERROR".encode()):
            print(f"SQL ERROR: {result.stderr} for {sql}")
            exit
        return result.stdout
    except subprocess.CalledProcessError as e:
        print(f"Error executing command: {e}")
        print(e.stderr)
        return e.stderr

def rxnorm_lookup_by_name(drug_name):
    #url = f"https://rxnav.nlm.nih.gov/REST/rxcui.json?name={drug_name}"
    url = f"https://rxnav.nlm.nih.gov/REST/rxcui.json?name={drug_name}&search=0"
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        #print(data)
        if len(data["idGroup"]) == 0:
            print ( f"{drug_name} Not Found...Trying Approx Match")
            return rxnrom_lookup_by_name_approx_match(drug_name)
        else:
            return data["idGroup"]["rxnormId"][0]

    else:
        return f"Error: {response.status_code}"



def rxnrom_lookup_by_name_approx_match(drug_name):
    url = f"https://rxnav.nlm.nih.gov/REST/approximateTerm.json?term={drug_name}&maxEntries=1"
    #print (url)
    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        #print(data)
        if data == {'approximateGroup': {'inputTerm': None}} :
            return "Approx Drug Match Not Found"
        else:
            return data["approximateGroup"]["candidate"][0]["rxcui"]



##
## MAIN PROCESSING STARTS HERE
##

#drug_name = "lisinopril"
#drug_name = "lisinopril 2.5 MG Oral Tablet"
#drug_name = "CEFTRIAXONE (ROCEPHIN) IN LIDOCAINE (PF) 1% IM INJECTION 500 MG"
#drug_name = "wtf"

file_path = '/tmp/meds_to_find_rxnorm.csv'

drop_table_sql = f"DROP TABLE IF EXISTS temp_reportable_meds_for_rxnorm_mapping;"
run_esp_sql(drop_table_sql)

find_meds_sql="CREATE TABLE temp_reportable_meds_for_rxnorm_mapping AS \
               SELECT DISTINCT upper(T2.name) \
               FROM conf_reportablemedication T1 \
               LEFT JOIN emr_prescription T2 ON (1=1) \
               WHERE T2.name ilike concat('%', drug_name, '%') AND (T2.code is null or T2.code = '') \
               AND upper(T2.name) not in (select name from static_rxnorm)  \
               AND T2.date >= '2000-01-01' \
               UNION \
               SELECT DISTINCT upper(T1.name) \
               FROM emr_prescription T1 \
               JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id) \
               WHERE T2.name ilike 'rx%' \
               AND (T1.code is null or T1.code = '') \
               AND upper(T1.name) not in (select name from static_rxnorm) \
               AND T1.date >= '2000-01-01' \
               ;"

find_meds_result=run_esp_sql(find_meds_sql)

create_file_sql = f"\COPY temp_reportable_meds_for_rxnorm_mapping TO '{file_path}';"
create_file_result=run_esp_sql(create_file_sql)

meds_to_lookup = open(file_path, 'r')
data = meds_to_lookup.read()
rx_list = data.split("\n")
#remove empty rows
rx_list = filter(None, rx_list)


for drug_name in rx_list:
    drug_name_formatted = drug_name.replace(" ", "+")
    rxcui_identified = rxnorm_lookup_by_name(drug_name_formatted)
    print(drug_name, rxcui_identified)
    if rxcui_identified == "Approx Drug Match Not Found":
        print(f"NO APPROX DRUG MATCH FOUND FOR {drug_name}")
        next
    else:
        insert_sql = f"INSERT INTO static_rxnorm VALUES (nextval('static_rxnorm_id_seq'), '{rxcui_identified}', '{drug_name}') ON CONFLICT DO NOTHING;"
        insert_result=run_esp_sql(insert_sql)
