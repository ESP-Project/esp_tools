/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Monthly Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------*/
set search_path to advp, gen_pop_tools, public;

create or replace function coalesce_r_sfunc(state anyelement, value anyelement)
    returns anyelement
    immutable parallel safe
as
$$
select coalesce(value, state);
$$ language sql;

create or replace aggregate find_last_ignore_nulls(anyelement) (
    sfunc = coalesce_r_sfunc,
    stype = anyelement
);

drop table if exists advp_out;
create table advp_out as
SELECT 
  advp_pat.patient_id as AA
, case
    when advp_pat.gender='M' then 1
	when advp_pat.gender='F' then 2
  end as AB
, case advp_pat.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as AC
, case
    when birth_year<1945 then 0
	when birth_year>=1945 and birth_year<= 1965 then 1
	when birth_year>1965 then 2
  end as AD
, case
    when substring(advp_pat.zip,6,1)='-' then substring(advp_pat.zip,1,5)
    else advp_pat.zip
  end as AE
, substr(advp_pat_seq_enc.year_month,1,4) as AF
, substr(advp_pat_seq_enc.year_month,6,2) as AG
,  case 
    when advp_pat_seq_enc.age<=4 then 1
    when advp_pat_seq_enc.age>=5 and advp_pat_seq_enc.age <=9 then 2
    when advp_pat_seq_enc.age>=10 and advp_pat_seq_enc.age <=14 then 3
    when advp_pat_seq_enc.age>=15 and advp_pat_seq_enc.age <=19 then 4
    when advp_pat_seq_enc.age>=20 and advp_pat_seq_enc.age <=24 then 5
    when advp_pat_seq_enc.age>=25 and advp_pat_seq_enc.age <=29 then 6
    when advp_pat_seq_enc.age>=30 and advp_pat_seq_enc.age <=39 then 7
    when advp_pat_seq_enc.age>=40 and advp_pat_seq_enc.age <=49 then 8
    when advp_pat_seq_enc.age>=50 and advp_pat_seq_enc.age <=59 then 9
    when advp_pat_seq_enc.age>=60 and advp_pat_seq_enc.age <=69 then 10
    when advp_pat_seq_enc.age>=70 and advp_pat_seq_enc.age <=79 then 11
    when advp_pat_seq_enc.age>=80 then 12
  end as AH 
, advp_pat_seq_enc.prior1 as AI
, advp_pat_seq_enc.prior2 as AJ
, case
    when advp_preg1.recent_pregnancy=1 and advp_pat.gender='F' then 1
    when advp_pat.gender='F' then 0
  end as AK
, case 
    when advp_smoking.smoking::integer>0 then advp_smoking.smoking::integer
    else 0
  end as AL
  , case
    when advp_anaplasmosis.anaplasmosis=1 then 1
    when max(advp_anaplasmosis.anaplasmosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_anaplasmosis.anaplasmosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_anaplasmosis.anaplasmosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_anaplasmosis.anaplasmosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AM
  , case
    when advp_babesiosis.babesiosis=1 then 1
    when max(advp_babesiosis.babesiosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_babesiosis.babesiosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_babesiosis.babesiosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_babesiosis.babesiosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AN
  , case
    when advp_covid_case.covid=1 then 1
    when max(advp_covid_case.covid) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_covid_case.covid) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_covid_case.covid) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_covid_case.covid) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AO
  , case
    when advp_chlamydia_case.chlamydia=1 then 1
    when max(advp_chlamydia_case.chlamydia) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_chlamydia_case.chlamydia) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_chlamydia_case.chlamydia) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_chlamydia_case.chlamydia) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AP
  , case
    when advp_gonorrhea_case.gonorrhea=1 then 1
    when max(advp_gonorrhea_case.gonorrhea) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_gonorrhea_case.gonorrhea) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_gonorrhea_case.gonorrhea) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_gonorrhea_case.gonorrhea) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AQ
  , case
    when advp_syphilis_case.syphilis=1 then 1
    when max(advp_syphilis_case.syphilis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_syphilis_case.syphilis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_syphilis_case.syphilis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_syphilis_case.syphilis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AR
  , case
    when advp_hiv.hiv=1 then 1
    when max(advp_hiv.hiv) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hiv.hiv) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hiv.hiv) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hiv.hiv) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AS
  , case
    when advp_hep_a.hep_a=1 then 1
    when max(advp_hep_a.hep_a) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hep_a.hep_a) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hep_a.hep_a) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hep_a.hep_a) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AT
  , case
    when advp_hep_b.hep_b=1 then 1 
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AU
  , case    
    when advp_hep_b.hep_b=2 then 1 
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 2 then 2
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 2 then 3
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 2 then 4
    when max(advp_hep_b.hep_b) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 2 then 5
    else 0
  end as AV
 , case
    when advp_tuberculosis.tb=1 then 1 
    when max(advp_tuberculosis.tb) 
         filter (where advp_tuberculosis.tb=1)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_tuberculosis.tb) 
         filter (where advp_tuberculosis.tb=1) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_tuberculosis.tb) 
         filter (where advp_tuberculosis.tb=1)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_tuberculosis.tb) 
         filter (where advp_tuberculosis.tb=1)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AW 
 , case
    when advp_lyme.lyme=1 then 1
    when max(advp_lyme.lyme) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_lyme.lyme) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_lyme.lyme) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_lyme.lyme) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AX
 , case
    when advp_chlamydia_lx.chlamydia_lx=1 then 1
    when max(advp_chlamydia_lx.chlamydia_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_chlamydia_lx.chlamydia_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_chlamydia_lx.chlamydia_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_chlamydia_lx.chlamydia_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AY
 , case
    when advp_gonorrhea_lx.gonorrhea_lx=1 then 1
    when max(advp_gonorrhea_lx.gonorrhea_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_gonorrhea_lx.gonorrhea_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_gonorrhea_lx.gonorrhea_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_gonorrhea_lx.gonorrhea_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as AZ
 , case
    when advp_syphilis_lx.syphilis_lx=1 then 1
    when max(advp_syphilis_lx.syphilis_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_syphilis_lx.syphilis_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_syphilis_lx.syphilis_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_syphilis_lx.syphilis_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BA
 , case
    when advp_hep_b_lx.hep_b_lx=1 then 1
    when max(advp_hep_b_lx.hep_b_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hep_b_lx.hep_b_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hep_b_lx.hep_b_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hep_b_lx.hep_b_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BB
 , case
    when advp_hep_c_lx.hep_c_lx=1 then 1
    when max(advp_hep_c_lx.hep_c_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hep_c_lx.hep_c_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hep_c_lx.hep_c_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hep_c_lx.hep_c_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BC
 , case
    when advp_hiv_lx.hiv_lx=1 then 1
    when max(advp_hiv_lx.hiv_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_hiv_lx.hiv_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_hiv_lx.hiv_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_hiv_lx.hiv_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BD
 , case
    when advp_tb_lx.tb_lx=1 then 1
    when max(advp_tb_lx.tb_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_tb_lx.tb_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_tb_lx.tb_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_tb_lx.tb_lx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BE
 , case
    when advp_tdap.tdap_vaccine=1 then 1
    when max(advp_tdap.tdap_vaccine) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 119 preceding and 1 preceding) = 1 
        and advp_pat_seq_enc.age>=20 then 2
    else 0
  end as BF
, case
    when advp_flu_vax.flu_vax=1 then 1
    when advp_pat_seq_enc.flu_month=1 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 1 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=2 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 2 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=3 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 3 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=4 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 4 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=5 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 5 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=6 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 6 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=7 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 7 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=8 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 8 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=9 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 9 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=10 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 10 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=11 and find_last_ignore_nulls(advp_flu_vax.flu_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 then 1
    else 0
  end as BG  
 , case
    when advp_covid_vax.covid_vax=1 then 1
    when advp_pat_seq_enc.flu_month=1 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 1 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=2 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 2 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=3 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 3 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=4 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 4 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=5 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 5 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=6 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 6 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=7 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 7 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=8 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 8 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=9 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 9 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=10 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 10 preceding and current row)=1 then 1
    when advp_pat_seq_enc.flu_month=11 and find_last_ignore_nulls(advp_covid_vax.covid_vax) over
            (partition by advp_pat.patient_id
             order by advp_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 then 1
    else 0    
  end as BH
 , case
    when sum(advp_hep_a_vax.hep_a_vax) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 1 preceding) = 1 then 1
    when sum(advp_hep_a_vax.hep_a_vax) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 1 preceding) >= 2 then 2
    else 0
  end as BI
 , case
    when sum(advp_hep_b_vax.hep_b_vax) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 1 preceding) = 1 then 1
     when sum(advp_hep_b_vax.hep_b_vax) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 1 preceding) = 2 then 2
    when sum(advp_hep_b_vax.hep_b_vax) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 1 preceding) >= 3 then 3
   else 0
  end as BJ
 , case
    when advp_prep.prep=1 then 1
    when max(advp_prep.prep) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_prep.prep) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_prep.prep) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_prep.prep) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0
  end as BK
 , case
    when advp_doxy_lyme.doxy=1 then 1
    when max(advp_doxy_lyme.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_doxy_lyme.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_doxy_lyme.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_doxy_lyme.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0  end as BL
 , case
    when advp_doxy_sti.doxy=1 then 1
    when max(advp_doxy_sti.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp_doxy_sti.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp_doxy_sti.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp_doxy_sti.doxy) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0  end as BM
 , advp_pat_seq_enc.allprior as BN
 , case
    when advp_primary_payer.primary_payer>=0 then advp_primary_payer.primary_payer::int 
    else 0::int 
    end BO
 , null::integer as BP -- census tract placeholder
 , case when advp_pat.ethnicity='HISPANIC' then 1 when advp_pat.ethnicity='NOT HISPANIC' then 2 end as BQ
 --hep c chronic
 , case
    when advp_hep_c.hep_c=3 then 1
    when max(advp_hep_c.hep_c) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 3 then 2
    when max(advp_hep_c.hep_c) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 3 then 3
    when max(advp_hep_c.hep_c) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 3 then 4
    when max(advp_hep_c.hep_c) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 3 then 5
    else 0
  end as BR
--active tb
 , case
    when advp_tuberculosis.tb=2 then 1 
    when max(advp_tuberculosis.tb) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 2 then 2
    when max(advp_tuberculosis.tb) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 2 then 3
    when max(advp_tuberculosis.tb) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 2 then 4
    when max(advp_tuberculosis.tb) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 2 then 5
    else 0
  end as BS  
--hep c acute 
 , case
    when advp_hep_c.hep_c=2 then 1
    when max(advp_hep_c.hep_c) 
         filter (where advp_hep_c.hep_c = 2)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 2 then 2
    when max(advp_hep_c.hep_c) 
         filter (where advp_hep_c.hep_c = 2)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 2 then 3
    when max(advp_hep_c.hep_c) 
         filter (where advp_hep_c.hep_c = 2)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 2 then 4
    when max(advp_hep_c.hep_c) 
         filter (where advp_hep_c.hep_c = 2)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 2 then 5
    else 0
  end as BT
--hcv reflex
 , case
    when advp_hep_c_elisa_ref.reflex is not null then advp_hep_c_elisa_ref.reflex
    else 0
  end as BU
--syph screen during pregnancy
 , case (find_last_ignore_nulls(advp_syph_screen_preg.syph_screen_preg)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and current row))
     when 0 then 0
     when 1 then 1
     when 2 then 2
     else 3
   end
  as BV  
, coalesce(advp_high_sti_risk.high_risk, 0) as BW 

--HIV New Diagnosis
  , case
    when advp.hiv_hiv_new_diag_pats.hiv_new_diagnosis=1 then 1
    when max(advp.hiv_hiv_new_diag_pats.hiv_new_diagnosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp.hiv_hiv_new_diag_pats.hiv_new_diagnosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp.hiv_hiv_new_diag_pats.hiv_new_diagnosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp.hiv_hiv_new_diag_pats.hiv_new_diagnosis) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0  
  end as BX 

--DoxyPEP
  , case
    when advp.doxy_pep_rx.doxy_pep_rx=1 then 1
    when max(advp.doxy_pep_rx.doxy_pep_rx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and 1 preceding) = 1 then 2
    when max(advp.doxy_pep_rx.doxy_pep_rx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 23 preceding and 12 preceding) = 1 then 3
    when max(advp.doxy_pep_rx.doxy_pep_rx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 35 preceding and 24 preceding) = 1 then 4
    when max(advp.doxy_pep_rx.doxy_pep_rx) 
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between unbounded preceding and 36 preceding) = 1 then 5
    else 0  
  end as BY 
--DTAP in pregnancy
, case (find_last_ignore_nulls(advp_dtap_preg.dtap_preg)
         over (partition by advp_pat.patient_id 
               order by advp_pat_seq_enc.year_month
               rows between 11 preceding and current row))
     when 0 then 0
     when 1 then 1
     else 2
   end
  as BZ
--
-- tables built from advp_weekly_tables.pg.sql
--
FROM advp_pat
--
-- advp_pat_seq_enc  
--
JOIN advp_pat_seq_enc on advp_pat_seq_enc.patient_id=advp_pat.patient_id
LEFT JOIN advp_preg1
	ON advp_preg1.patient_id = advp_pat_seq_enc.patient_id and advp_preg1.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_smoking 
        ON advp_smoking.patient_id = advp_pat_seq_enc.patient_id and advp_smoking.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_anaplasmosis
        ON advp_anaplasmosis.patient_id = advp_pat_seq_enc.patient_id and advp_anaplasmosis.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_babesiosis
        ON advp_babesiosis.patient_id = advp_pat_seq_enc.patient_id and advp_babesiosis.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_covid_case
        ON advp_covid_case.patient_id = advp_pat_seq_enc.patient_id and advp_covid_case.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_chlamydia_case
        ON advp_chlamydia_case.patient_id = advp_pat_seq_enc.patient_id and advp_chlamydia_case.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_gonorrhea_case
        ON advp_gonorrhea_case.patient_id = advp_pat_seq_enc.patient_id and advp_gonorrhea_case.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_syphilis_case
        ON advp_syphilis_case.patient_id = advp_pat_seq_enc.patient_id and advp_syphilis_case.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hiv
        ON advp_hiv.patient_id = advp_pat_seq_enc.patient_id and advp_hiv.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_a
        ON advp_hep_a.patient_id = advp_pat_seq_enc.patient_id and advp_hep_a.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_b
        ON advp_hep_b.patient_id = advp_pat_seq_enc.patient_id and advp_hep_b.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_c
        ON advp_hep_c.patient_id = advp_pat_seq_enc.patient_id and advp_hep_c.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_tuberculosis
        ON advp_tuberculosis.patient_id = advp_pat_seq_enc.patient_id and advp_tuberculosis.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_lyme
        ON advp_lyme.patient_id = advp_pat_seq_enc.patient_id and advp_lyme.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_chlamydia_lx
        ON advp_chlamydia_lx.patient_id = advp_pat_seq_enc.patient_id and advp_chlamydia_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_gonorrhea_lx
        ON advp_gonorrhea_lx.patient_id = advp_pat_seq_enc.patient_id and advp_gonorrhea_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_syphilis_lx
        ON advp_syphilis_lx.patient_id = advp_pat_seq_enc.patient_id and advp_syphilis_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_b_lx
        ON advp_hep_b_lx.patient_id = advp_pat_seq_enc.patient_id and advp_hep_b_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_c_lx
        ON advp_hep_c_lx.patient_id = advp_pat_seq_enc.patient_id and advp_hep_c_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hiv_lx
        ON advp_hiv_lx.patient_id = advp_pat_seq_enc.patient_id and advp_hiv_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_tb_lx
        ON advp_tb_lx.patient_id = advp_pat_seq_enc.patient_id and advp_tb_lx.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_tdap
        ON advp_tdap.patient_id = advp_pat_seq_enc.patient_id and advp_tdap.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_flu_vax
        ON advp_flu_vax.patient_id = advp_pat_seq_enc.patient_id and advp_flu_vax.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_covid_vax
        ON advp_covid_vax.patient_id = advp_pat_seq_enc.patient_id and advp_covid_vax.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_a_vax
        ON advp_hep_a_vax.patient_id = advp_pat_seq_enc.patient_id and advp_hep_a_vax.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_b_vax
        ON advp_hep_b_vax.patient_id = advp_pat_seq_enc.patient_id and advp_hep_b_vax.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_prep
        ON advp_prep.patient_id = advp_pat_seq_enc.patient_id and advp_prep.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_doxy_lyme
        ON advp_doxy_lyme.patient_id = advp_pat_seq_enc.patient_id and advp_doxy_lyme.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_doxy_sti
        ON advp_doxy_sti.patient_id = advp_pat_seq_enc.patient_id and advp_doxy_sti.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_primary_payer
        ON advp_primary_payer.patient_id = advp_pat_seq_enc.patient_id and advp_primary_payer.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_hep_c_elisa_ref
        ON advp_hep_c_elisa_ref.patient_id = advp_pat_seq_enc.patient_id and advp_hep_c_elisa_ref.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_syph_screen_preg
        ON advp_syph_screen_preg.patient_id = advp_pat_seq_enc.patient_id and advp_syph_screen_preg.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp_high_sti_risk
        ON advp_high_sti_risk.patient_id = advp_pat_seq_enc.patient_id and advp_high_sti_risk.year_month=advp_pat_seq_enc.year_month
LEFT JOIN advp.hiv_hiv_new_diag_pats
        ON advp.hiv_hiv_new_diag_pats.patient_id = advp_pat_seq_enc.patient_id and advp.hiv_hiv_new_diag_pats.year_month = advp_pat_seq_enc.year_month
LEFT JOIN advp.doxy_pep_rx
        ON advp.doxy_pep_rx.patient_id = advp_pat_seq_enc.patient_id and advp.doxy_pep_rx.year_month = advp_pat_seq_enc.year_month
LEFT JOIN advp.advp_dtap_preg
        on advp_dtap_preg.patient_id = advp_pat_seq_enc.patient_id and advp_dtap_preg.year_month = advp_pat_seq_enc.year_month
;

