﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/

set search_path to advp, public;

--find patients with HIV diagnosis
DROP TABLE IF EXISTS cc_hiv_case;
CREATE TABLE cc_hiv_case AS
SELECT patient_id, date hiv_case_dt 
FROM nodis_case
where condition='hiv' 
;

--find patients with HIV lab tests
drop table if exists cc_hiv_labs;
create table cc_hiv_labs as
select distinct patient_id, to_char(date,'yyyy') lab_yr
from emr_labresult l
join conf_labtestmap tm on tm.native_code=l.native_code
where tm.test_name in ('cd4','hiv_elisa', 'hiv_ag_ab', 'hiv_pcr', 'hiv_wb',
                         'hiv_rna_viral', 'hiv_multispot', 'hiv_geenius');

--risk scores for non-hiv pats, joined to hiv pats (subsequent diagnosis) and hiv lab tests
drop table if exists cc_hiv_risk;
create table cc_hiv_risk as
select t1.patient_id,
       t1.rpt_year, 
       t2.patient_id as subsequent_hiv,
       t3.patient_id as tested_at_risk,
       t1.hiv_risk_score,
       t1.hiv_risk_score/(12.1/100000) ratio,
       t1.truvada_rx_yn
from gen_pop_tools.cc_hiv_risk_score t1
left join cc_hiv_case t2 on t1.patient_id=t2.patient_id
left join cc_hiv_labs t3 on t1.patient_id=t3.patient_id and t1.rpt_year=t3.lab_yr;

--top deciles
drop table if exists cc_hiv_plus_risk;
create table cc_hiv_plus_risk as
select t0.patient_id,
       t0.rpt_year rpt_yr, 
       case when t0.hiv_risk_score>.01 then t0.patient_id else null end hiv_risk_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 then t0.patient_id else null end hiv_risk_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 then t0.patient_id else null end hiv_risk_5_10,
       case when t0.hiv_risk_score>.01 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.subsequent_hiv is not null then t0.subsequent_hiv else null end subsequent_hiv_5_10,
       case when t0.hiv_risk_score>.01 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.tested_at_risk is not null then t0.tested_at_risk else null end tested_at_risk_5_10,
       case when t0.hiv_risk_score>.01 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.truvada_rx_yn = 1 then t0.patient_id else null end truvada_rx_5_10,
       case when t0.hiv_risk_score>.01 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_85,
       case when t0.hiv_risk_score>.001 and t0.hiv_risk_score<=.01 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_10_85,
       case when t0.hiv_risk_score>.0005 and t0.hiv_risk_score<=.001 and t0.truvada_rx_yn = 1 and t0.subsequent_hiv is not null then t0.patient_id else null end truvada_hiv_5_10,
       case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
	 when upper(substr(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
       end sex,
       case 
         when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity) is not null
           then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity)
         when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=p.race) is not null
           then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=p.race)
         else 'UNKNOWN'
       end race_ethnicity
    ,  case 
         when date_part('year', age(current_date, p.date_of_birth))<=19 then '0-19'
         when date_part('year', age(current_date, p.date_of_birth))>=20 and date_part('year', age(current_date, p.date_of_birth)) <=39 then '20-39'
         when date_part('year', age(current_date, p.date_of_birth))>=40 and date_part('year', age(current_date, p.date_of_birth)) <=59 then '40-59'
         when date_part('year', age(current_date, p.date_of_birth))>=60 then 'Over 59'
         else 'UNKNOWN'
       end as agegroup            
from cc_hiv_risk t0
JOIN emr_patient p ON t0.patient_id = p.id;

--build the table for summarization:
DROP TABLE IF EXISTS cc_hiv_risk_by_pat;
CREATE TABLE cc_hiv_risk_by_pat as
SELECT t0.patient_id, t0.sex, t0.race_ethnicity, t0.agegroup, t0.rpt_yr
 , t0.hiv_risk_85
 , t0.hiv_risk_10_85
 , t0.hiv_risk_5_10
 , t0.subsequent_hiv_85
 , t0.subsequent_hiv_10_85
 , t0.subsequent_hiv_5_10
 , t0.tested_at_risk_85
 , t0.tested_at_risk_10_85
 , t0.tested_at_risk_5_10
 , truvada_rx_85
 , truvada_rx_10_85
 , truvada_rx_5_10
 , truvada_hiv_85
 , truvada_hiv_10_85
 , truvada_hiv_5_10
FROM cc_hiv_plus_risk t0;

--Now get the crossing of all stratifiers
drop table if exists strat1;
create temporary table strat1 (id1 varchar(2), name1 varchar(15));
insert into strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists strat2;
create temporary table strat2 (id2 varchar(2), name2 varchar(15));
insert into strat2 values ('2','agegroup'),('x','x');
drop table if exists strat4;
create temporary table strat4 (id4 varchar(2), name4 varchar(15));
insert into strat4 values ('4','sex'),('x','x');
drop table if exists strat5;
create temporary table strat5 (id5 varchar(2), name5 varchar(15));
insert into strat5 values ('5','race_ethnicity'),('x','x');
drop table if exists cc_stratifiers_crossed;
create table cc_stratifiers_crossed as
select * from strat1, strat2, strat4, strat5;

--create stub table to contain summary results
drop table if exists advp.cc_hiv_risk_summaries;
create table advp.cc_hiv_risk_summaries (
  rpt_yr varchar(4),
  agegroup varchar(10), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total numeric(15,1),
  hiv_risk_85 numeric(15,1),
  hiv_risk_10_85 numeric(15,1),
  hiv_risk_5_10 numeric(15,1),
  subsequent_hiv_85 numeric(15,1),
  subsequent_hiv_10_85 numeric(15,1),
  subsequent_hiv_5_10 numeric(15,1),
  tested_at_risk_85 numeric(15,1),
  tested_at_risk_10_85 numeric(15,1),
  tested_at_risk_5_10 numeric(15,1),
  truvada_rx_85 numeric(15,1),
  truvada_rx_10_85 numeric(15,1),
  truvada_rx_5_10 numeric(15,1),
  truvada_hiv_85 numeric(15,1),
  truvada_hiv_10_85 numeric(15,1),
  truvada_hiv_5_10 numeric(15,1));

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
do
$$
  declare
    cursrow record;
    namerow record;
    nameset text;
    groupby text[];
    insrtsql text;
    partsql text;
    tmpsql text;
    ordsql text;
    i integer;
  begin
    for cursrow in execute 'select name1, name2, name4, name5 from cc_stratifiers_crossed'
    loop
      i:=0;
      groupby:=null;
      insrtsql:='insert into advp.cc_hiv_risk_summaries (rpt_yr, agegroup, sex, race_ethnicity, total,
      hiv_risk_85,  hiv_risk_10_85, hiv_risk_5_10,  
      subsequent_hiv_85, subsequent_hiv_10_85, subsequent_hiv_5_10, 
      tested_at_risk_85, tested_at_risk_10_85, tested_at_risk_5_10, 
      truvada_rx_85, truvada_rx_10_85, truvada_rx_5_10, 
      truvada_hiv_85, truvada_hiv_10_85, truvada_hiv_5_10) 
        (select';
      if cursrow.name1='x' then 
        tmpsql:=' ''x''::varchar rpt_yr,';
      else
        tmpsql:=' rpt_yr,';
        i:=i+1;
        groupby[i]:='rpt_yr';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name2='x' then 
        tmpsql:=' ''x''::varchar agegroup,';
      else
        tmpsql:=' agegroup,';
        i:=i+1;
        groupby[i]:='agegroup';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name4='x' then 
        tmpsql:=' ''x''::varchar sex,';
      else
        tmpsql:=' sex,';
        i:=i+1;
        groupby[i]:='sex';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name5='x' then 
        tmpsql:=' ''x''::varchar race_ethnicity,';
      else
        tmpsql:=' race_ethnicity,';
        i:=i+1;
        groupby[i]:='race_ethnicity';
      end if;
      insrtsql:=insrtsql||tmpsql;
/*      insrtsql:=insrtsql||'
	     case when count(distinct patient_id) between 1 and 5 then 2.5 else count(distinct patient_id) end total,
         case when count(distinct hiv_risk_85) between 1 and 5 then 2.5 else count(distinct hiv_risk_85) end hiv_risk_85,
         case when count(distinct hiv_risk_10_85) between 1 and 5 then 2.5 else count(distinct hiv_risk_10_85) end hiv_risk_10_85,
         case when count(distinct hiv_risk_5_10) between 1 and 5 then 2.5 else count(distinct hiv_risk_5_10) end hiv_risk_5_10,
         case when count(distinct subsequent_hiv_85) between 1 and 5 then 2.5 else count(distinct subsequent_hiv_85) end subsequent_hiv_85,
         case when count(distinct subsequent_hiv_10_85) between 1 and 5 then 2.5 else count(distinct subsequent_hiv_10_85) end subsequent_hiv_10_85,
         case when count(distinct subsequent_hiv_5_10) between 1 and 5 then 2.5 else count(distinct subsequent_hiv_5_10) end subsequent_hiv_5_10,
         case when count(distinct tested_at_risk_85) between 1 and 5 then 2.5 else count(distinct tested_at_risk_85) end tested_at_risk_85,
         case when count(distinct tested_at_risk_10_85) between 1 and 5 then 2.5 else count(distinct tested_at_risk_10_85) end tested_at_risk_10_85,
         case when count(distinct tested_at_risk_5_10) between 1 and 5 then 2.5 else count(distinct tested_at_risk_5_10) end tested_at_risk_5_10,
         case when count(distinct truvada_rx_85) between 1 and 5 then 2.5 else count(distinct truvada_rx_85) end truvada_rx_85,
         case when count(distinct truvada_rx_10_85) between 1 and 5 then 2.5 else count(distinct truvada_rx_10_85) end truvada_rx_10_85,
         case when count(distinct truvada_rx_5_10) between 1 and 5 then 2.5 else count(distinct truvada_rx_5_10) end truvada_rx_5_10,
         case when count(distinct truvada_hiv_85) between 1 and 5 then 2.5 else count(distinct truvada_hiv_85) end truvada_hiv_85,
         case when count(distinct truvada_hiv_10_85) between 1 and 5 then 2.5 else count(distinct truvada_hiv_10_85) end truvada_hiv_10_85,
         case when count(distinct truvada_hiv_5_10) between 1 and 5 then 2.5 else count(distinct truvada_hiv_5_10) end truvada_hiv_5_10
         from cc_hiv_risk_by_pat
         ';
*/
      insrtsql:=insrtsql||'
	     count(distinct patient_id) total,
         count(distinct hiv_risk_85) hiv_risk_85,
         count(distinct hiv_risk_10_85) hiv_risk_10_85,
         count(distinct hiv_risk_5_10) hiv_risk_5_10,
         count(distinct subsequent_hiv_85) subsequent_hiv_85,
         count(distinct subsequent_hiv_10_85) subsequent_hiv_10_85,
         count(distinct subsequent_hiv_5_10) subsequent_hiv_5_10,
         count(distinct tested_at_risk_85) tested_at_risk_85,
         count(distinct tested_at_risk_10_85) tested_at_risk_10_85,
         count(distinct tested_at_risk_5_10) tested_at_risk_5_10,
         count(distinct truvada_rx_85) truvada_rx_85,
         count(distinct truvada_rx_10_85) truvada_rx_10_85,
         count(distinct truvada_rx_5_10) truvada_rx_5_10,
         count(distinct truvada_hiv_85) truvada_hiv_85,
         count(distinct truvada_hiv_10_85) truvada_hiv_10_85,
         count(distinct truvada_hiv_5_10) truvada_hiv_5_10
         from cc_hiv_risk_by_pat
         ';
      if i> 0 then 
        insrtsql:=insrtsql||'group by ';
        i:=1;
        while i <= array_length(groupby, 1)
        loop
          if i >1 then 
            insrtsql:=insrtsql||', '; 
          end if;
          insrtsql:=insrtsql||groupby[i];
          i:=i+1;
        end loop;
      end if;
      insrtsql:=insrtsql||')';
      execute insrtsql;
      --raise notice '%', insrtsql;
    end loop;
  end;      
$$ 
language plpgsql;

/*
select * from cc_hiv_risk_summaries;
*/

--Gather up the values of the stratifiers and assign code.
drop table if exists advp.cc_agegroup_codevals;
Create table advp.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
insert into advp.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');

drop table if exists advp.cc_sex_codevals;
Create table advp.cc_sex_codevals (sex varchar(7), codeval varchar(1));
insert into advp.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2');

drop table if exists advp.cc_race_ethnicity_codevals;
Create table advp.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into advp.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

--now write out to JSON
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
'{"a":{"name":"Non-HIV patients, total with calculable risk","description":"Patients with sufficient data to calculate a score for risk of HIV acquisition during the measurent period according to an electronic prediction rule.","nest":"0","pcalc":"a"},"b":{"name":"Absolute risk of HIV acquisition ≥1%","description":"This is ≥85 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"c":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"b"},"d":{"name":"Tested for HIV during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"b"},"e":{"name":"Prescribed PReP during period at risk","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed PReP","nest":"2","pcalc":"b"},"f":{"name":"Subsequently acquired HIV","description":"These are people with ≥85 times the MA incidence of 12.1 per 100,000 who were prescribed PReP during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"e"},"g":{"name":"“Absolute risk of HIV acquisition 0.1 – 0.99%","description":"This is approx. ≥10 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"h":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"g"},"i":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"g"},"j":{"name":"Prescribed PReP during period at risk","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed PReP","nest":"2","pcalc":"g"},"k":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥10 times the MA incidence of 12.1 per 100,000 who were prescribed PReP during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"j"},"l":{"name":"Absolute risk of HIV acquisition 0.05 – 0.09%","description":"This is approx. ≥5 times the MA incidence of 12.1 per 100,000","nest":"1","pcalc":"a"},"m":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who subsequently acquired HIV.","nest":"2","pcalc":"l"},"n":{"name":"Tested for HIV during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who recieved any HIV test","nest":"2","pcalc":"l"},"o":{"name":"Prescribed PReP during period at risk","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed PReP","nest":"2","pcalc":"l"},"p":{"name":"Subsequently acquired HIV","description":"These are people with approximately ≥5 times the MA incidence of 12.1 per 100,000 who were prescribed PReP during the measurement period, and subsequently acquired HIV.","nest":"3","pcalc":"o"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when rpt_yr = 'x' then 'YEAR' else rpt_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from advp.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from advp.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from advp.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| rtrim(to_char(total,'FM99999999999999.9'), '.')
		  || ',"b":' || rtrim(to_char(hiv_risk_85,'FM99999999999999.9'), '.')
		  || ',"c":' || rtrim(to_char(subsequent_hiv_85,'FM99999999999999.9'), '.')
		  || ',"d":' || rtrim(to_char(tested_at_risk_85,'FM99999999999999.9'), '.')
		  || ',"e":' || rtrim(to_char(truvada_rx_85,'FM99999999999999.9'), '.')
		  || ',"f":' || rtrim(to_char(truvada_hiv_85,'FM99999999999999.9'), '.')
		  || ',"g":' || rtrim(to_char(hiv_risk_10_85,'FM99999999999999.9'), '.')
		  || ',"h":' || rtrim(to_char(subsequent_hiv_10_85,'FM99999999999999.9'), '.')
		  || ',"i":' || rtrim(to_char(tested_at_risk_10_85,'FM99999999999999.9'), '.')
		  || ',"j":' || rtrim(to_char(truvada_rx_10_85,'FM99999999999999.9'), '.')
		  || ',"k":' || rtrim(to_char(truvada_hiv_10_85,'FM99999999999999.9'), '.')
		  || ',"l":' || rtrim(to_char(hiv_risk_5_10,'FM99999999999999.9'), '.')
		  || ',"m":' || rtrim(to_char(subsequent_hiv_5_10,'FM99999999999999.9'), '.')
		  || ',"n":' || rtrim(to_char(tested_at_risk_5_10,'FM99999999999999.9'), '.')
		  || ',"o":' || rtrim(to_char(truvada_rx_5_10,'FM99999999999999.9'), '.')
		  || ',"p":' || rtrim(to_char(truvada_hiv_5_10,'FM99999999999999.9'), '.')
		  || '}') row_
from advp.cc_hiv_risk_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';




  

