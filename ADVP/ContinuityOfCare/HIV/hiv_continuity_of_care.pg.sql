﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                       Continuity of Care weekly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2017 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
set search_path to advp, public;

--This table is used to map local race values to standard set.
drop table if exists cc_racemap;
create table cc_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the set here.
insert into cc_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK')
;
--find patients with HIV diagnosis
DROP TABLE IF EXISTS cc_hiv_case;
CREATE TABLE cc_hiv_case AS
SELECT patient_id, date hiv_case_dt 
FROM nodis_case
where condition='hiv' 
;
--encounters for hiv cases
DROP TABLE IF EXISTS cc_hiv_encs;
CREATE TABLE cc_hiv_encs AS
SELECT c.patient_id, 
   to_char(coalesce(e.date, c.hiv_case_dt),'yyyy') hiv_yr,
   min(coalesce(e.date, c.hiv_case_dt)) hiv_enc_dt,
   max(case 
     when to_char(c.hiv_case_dt,'yyyy')=to_char(e.date,'yyyy') or e.date is null
       then 1
     else 0
   end)  new_dx  
from emr_encounter e
right join cc_hiv_case c on c.patient_id=e.patient_id
     and c.hiv_case_dt<e.date
where to_char(coalesce(e.date, c.hiv_case_dt),'yyyy')>'2010'
group by c.patient_id, 
   to_char(coalesce(e.date, c.hiv_case_dt),'yyyy')
;
--HIV patients with follow-up encounter
DROP TABLE IF EXISTS cc_hiv_2nd_enc;
CREATE TABLE cc_hiv_2nd_enc AS
SELECT distinct e1.*, e2.patient_id as secnd_enc
FROM cc_hiv_encs e1
left join emr_encounter e2 on e2.patient_id=e1.patient_id
  and e2.date>e1.hiv_enc_dt;

--encounters with hiv diagnoses
drop table if exists cc_hiv_dx_encs;
create table cc_hiv_dx_encs as
select e.patient_id, e.date enc_dt
from emr_encounter e 
join hef_event he on he.patient_id=e.patient_id
  and e.id=he.object_id and he.name='dx:hiv'
;
drop table if exists cc_hiv_post_case_dx;
create table cc_hiv_post_case_dx as
select distinct e.*, d.patient_id as hiv_dx
from cc_hiv_2nd_enc e
left join cc_hiv_dx_encs d on d.patient_id=e.patient_id 
  and e.hiv_enc_dt<d.enc_dt; 

--hiv med patients
drop table if exists cc_hiv_meds;
create table cc_hiv_meds as
select patient_id, name, date med_dt
from hef_event
where name like 'rx:hiv_%'
;--hiv medicated cases
drop table if exists cc_hiv_med_cases;
create table cc_hiv_med_cases as
select distinct t0.*, t1.patient_id hiv_rx 
from cc_hiv_post_case_dx t0
left join cc_hiv_meds t1 on t1.patient_id=t0.patient_id
    and t0.hiv_yr<=to_char(t1.med_dt,'yyyy');

--cd4 test after
drop table if exists cc_hiv_cd4;
create table cc_hiv_cd4 as
select distinct patient_id, date cd4_dt
from emr_labresult l
join conf_labtestmap tm on tm.native_code=l.native_code
where tm.test_name='cd4'
;--viral load
drop table if exists cc_hiv_vl;
create table cc_hiv_vl as
select distinct t0.patient_id, date vl_dt, result_float vl, result_string,
    max(date) over (partition by t0.patient_id) max_vl_date, t0.hiv_enc_dt
from emr_labresult l
join conf_labtestmap tm on tm.native_code=l.native_code
join cc_hiv_encs t0 on t0.patient_id=l.patient_id
where tm.test_name in ('hiv_rna_viral','hiv_pcr')
  and result_string not in ('tnp','TNP','Not tested','Test not done')
;--retained in care
drop table if exists cc_hiv_ret;
create table cc_hiv_ret as
select distinct t0.*, t0.patient_id as retained
from cc_hiv_encs t0
left join cc_hiv_vl t1 on t0.patient_id=t1.patient_id and t0.hiv_enc_dt<t1.vl_dt
left join cc_hiv_cd4 t2 on t0.patient_id=t2.patient_id and t0.hiv_enc_dt<t2.cd4_dt
left join cc_hiv_meds t3 on t0.patient_id=t3.patient_id and t0.hiv_enc_dt<t3.med_dt
left join cc_hiv_dx_encs t4 on t0.patient_id=t4.patient_id and t0.hiv_enc_dt<t4.enc_dt
where least(t1.vl_dt,t2.cd4_dt,t3.med_dt,t4.enc_dt)<=
         greatest(t1.vl_dt,t2.cd4_dt,t3.med_dt,t4.enc_dt) + interval '3 months'
;
drop table if exists cc_hiv_retained;
create table cc_hiv_retained as
select t0.*, t1.retained
from cc_hiv_med_cases t0
left join cc_hiv_ret t1 on t1.patient_id=t0.patient_id and t1.hiv_yr=t0.hiv_yr;

--viral load test after qualifying enc or case detection
drop table if exists cc_hiv_vload;
create table cc_hiv_vload as
select distinct t0.*, t1.patient_id as vload
from cc_hiv_retained t0
left join cc_hiv_vl t1 on t0.patient_id=t1.patient_id and t0.hiv_enc_dt<t1.vl_dt;

--viral load plus meds
drop table if exists cc_hiv_vl_rx;
create table cc_hiv_vl_rx as
select distinct t0.*, 
  case
    when t2.patient_id is not null and t1.patient_id is not null
      then t1.patient_id
  end vl_rx
from cc_hiv_vload t0 
left join cc_hiv_meds t1 on t1.patient_id=t0.patient_id and t1.med_dt>t0.hiv_enc_dt
left join cc_hiv_vl t2 on t2.patient_id=t0.patient_id and t2.vl_dt>t0.hiv_enc_dt;

--virally supressed
drop table if exists cc_hiv_vl_sprsd;
create table cc_hiv_vl_sprsd as
select distinct t0.*, t1.patient_id sprsd
from cc_hiv_vl_rx t0
left join (select patient_id from cc_hiv_vl vl
           where ((vl<200 
             or vl.result_string ilike '<%' or vl.result_string ilike 'not%'
                 or vl.result_string ilike 'neg%') and vl_dt=max_vl_date)
             or exists (select null from hef_event he
                        where he.name='lx:hiv_rna_viral:negative'
                               and vl.patient_id=he.patient_id and he.date=vl.max_vl_date)
) t1 on t1.patient_id=t0.patient_id;

--virally supressed and rx
drop table if exists cc_hiv_vl_spr_rx;
create table cc_hiv_vl_spr_rx as
select distinct t0.*, 
  case
    when t2.patient_id is not null and t1.patient_id is not null
      then t1.patient_id
  end spr_rx
from cc_hiv_vl_sprsd t0
left join cc_hiv_meds t1 on t1.patient_id=t0.patient_id and t1.med_dt>t0.hiv_enc_dt
left join (select * from cc_hiv_vl vl
           where ((vl<200 
             or vl.result_string ilike '<%' or vl.result_string ilike 'not%'
                 or vl.result_string ilike 'neg%') and vl_dt=max_vl_date)
             or exists (select null from hef_event he
                        where he.name='lx:hiv_rna_viral:negative'
                               and vl.patient_id=he.patient_id and he.date=vl.max_vl_date)
) t2 on t2.patient_id=t0.patient_id;

--opportunistic infections
drop table if exists cc_hiv_opt;
create table cc_hiv_opt as
select distinct t0.*, t1.patient_id as opt
from cc_hiv_vl_spr_rx t0
left join (
  select t01.patient_id, date 
  from emr_encounter t01 
  join emr_encounter_dx_codes t02 on t01.id=t02.encounter_id
where 
(t02.dx_code_id IN ('icd9:136.3','icd10:B59','icd9:117.5','icd9:321.0','icd10:B45.0','icd10:B45.1',
               'icd10:B45.7','icd10:B45.9','icd9:007.4','icd10:A07.2','icd9:031.2','icd10:A31.2',
               'icd9:046.3','icd10:A81.2','icd9:112.0','icd9:112.4','icd9:112.5','icd9:112.84',
               'icd10:B37.0','icd10:B37.1','icd10:B37.81','icd10:B37.83')  
or t02.dx_code_id ~ '^icd9:130.|icd10:B58.|^icd9:010|icd9:011|icd9:012|icd9:013|icd9:014|icd9:015'
or t02.dx_code_id ~ 'icd9:016|icd9:017|icd9:018|icd10:A15|icd10:A16|icd10:A17|icd10:A18|icd10:A19')) t1
on t0.patient_id=t1.patient_id and t0.hiv_enc_dt<t1.date;

--linked to care
drop table if exists cc_hiv_lnk;
create table cc_hiv_lnk as 
select distinct t0.patient_id
from cc_hiv_opt t0
left join cc_hiv_vl t1 on t0.patient_id=t1.patient_id and t0.hiv_enc_dt<t1.vl_dt
left join cc_hiv_cd4 t2 on t0.patient_id=t2.patient_id and t0.hiv_enc_dt<t2.cd4_dt
left join cc_hiv_meds t3 on t0.patient_id=t3.patient_id and t0.hiv_enc_dt<t3.med_dt
where least(t1.vl_dt,t2.cd4_dt,t3.med_dt)<=t0.hiv_enc_dt+interval '90 days'
 and t0.new_dx=1;
drop table if exists cc_hiv_linked;
create table cc_hiv_linked as
select t0.*, t1.patient_id as linked
from cc_hiv_opt t0
left join cc_hiv_lnk t1 on t1.patient_id = t0.patient_id and t0.new_dx=1;

--add demographics
DROP TABLE IF EXISTS cc_hiv_cases;
CREATE TABLE cc_hiv_cases AS
select c.*,
case when upper(substr(p.gender,1,1)) = 'F' then 'Female'
	 when upper(substr(p.gender,1,1)) = 'M' then 'Male'
	 else 'Unknown'
end sex,
case 
     when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity) is not null
           then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='ethnicity' and t00.src_value=p.ethnicity)
     when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=p.race) is not null
           then (select mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_field='race' and t00.src_value=p.race)
     else 'UNKNOWN'
  end race_ethnicity
,  case 
    when date_part('year', age(current_date, p.date_of_birth))<=19 then '0-19'
    when date_part('year', age(current_date, p.date_of_birth))>=20 and date_part('year', age(current_date, p.date_of_birth)) <=39 then '20-39'
    when date_part('year', age(current_date, p.date_of_birth))>=40 and date_part('year', age(current_date, p.date_of_birth)) <=59 then '40-59'
    when date_part('year', age(current_date, p.date_of_birth))>=60 then 'Over 59'
    else 'UNKNOWN'
  end as agegroup
from cc_hiv_linked c
JOIN emr_patient p ON c.patient_id = p.id;

--build the table for summarization:
DROP TABLE IF EXISTS cc_hiv_by_pat;
CREATE TABLE cc_hiv_by_pat as
SELECT t0.patient_id, t0.sex, t0.race_ethnicity, t0.agegroup, t0.hiv_yr
 , t0.secnd_enc scnd_enc
 , t0.hiv_dx
 , t0.hiv_rx
 , t0.retained
 , t0.vload
 , t0.vl_rx
 , t0.sprsd
 , t0.spr_rx
 , t0.opt
 , t1.patient_id tot_csyr
 , t0.linked
 , t1.secnd_enc scnd_enc_csyr
 , t1.hiv_dx hiv_dx_csyr
 , t1.hiv_rx hiv_rx_csyr
 , t1.retained retained_csyr
 , t1.vload vload_csyr
 , t1.vl_rx vl_rx_csyr
 , t1.sprsd sprsd_csyr
 , t1.opt opt_csyr
FROM cc_hiv_cases t0
left join (select * from cc_hiv_cases where new_dx=1) t1 
  on t0.patient_id=t1.patient_id and t0.hiv_yr=t1.hiv_yr;

--Now get the crossing of all stratifiers
drop table if exists strat1;
create temporary table strat1 (id1 varchar(2), name1 varchar(15));
insert into strat1 values ('1','index_enc_yr'),('x','x');
drop table if exists strat2;
create temporary table strat2 (id2 varchar(2), name2 varchar(15));
insert into strat2 values ('2','agegroup'),('x','x');
drop table if exists strat4;
create temporary table strat4 (id4 varchar(2), name4 varchar(15));
insert into strat4 values ('4','sex'),('x','x');
drop table if exists strat5;
create temporary table strat5 (id5 varchar(2), name5 varchar(15));
insert into strat5 values ('5','race_ethnicity'),('x','x');
drop table if exists cc_stratifiers_crossed;
create table cc_stratifiers_crossed as
select * from strat1, strat2, strat4, strat5;

--create stub table to contain summary results
drop table if exists advp.cc_hiv_summaries;
create table advp.cc_hiv_summaries (
  hiv_yr varchar(4),
  agegroup varchar(10), 
  sex varchar(10), 
  race_ethnicity varchar(25),
  total integer,
  scnd_enc integer,
  hiv_dx integer,
  hiv_rx integer,
  retained integer,
  vload integer,
  vl_rx integer,
  sprsd integer,
  spr_rx integer,
  opt integer,
  tot_csyr integer,
  linked integer,
  scnd_enc_csyr integer,
  hiv_dx_csyr integer,
  hiv_rx_csyr integer,
  retained_csyr integer,
  vload_csyr integer,
  vl_rx_csyr integer,
  sprsd_csyr integer,
  opt_csyr integer);

 --Run the various groupings.  This would be much easier in Postgres 9.5 where the "grouping sets" query feature was added.
do
$$
  declare
    cursrow record;
    namerow record;
    nameset text;
    groupby text[];
    insrtsql text;
    partsql text;
    tmpsql text;
    ordsql text;
    i integer;
  begin
    for cursrow in execute 'select name1, name2, name4, name5 from cc_stratifiers_crossed'
    loop
      i:=0;
      groupby:=null;
      insrtsql:='insert into advp.cc_hiv_summaries (hiv_yr, agegroup, sex, race_ethnicity, 
      total, scnd_enc, hiv_dx, hiv_rx, retained, vload, vl_rx, sprsd, 
      spr_rx, opt, tot_csyr, linked, scnd_enc_csyr, hiv_dx_csyr, hiv_rx_csyr, 
      retained_csyr, vload_csyr, vl_rx_csyr, sprsd_csyr, opt_csyr) 
        (select';
      if cursrow.name1='x' then 
        tmpsql:=' ''x''::varchar hiv_yr,';
      else
        tmpsql:=' hiv_yr,';
        i:=i+1;
        groupby[i]:='hiv_yr';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name2='x' then 
        tmpsql:=' ''x''::varchar agegroup,';
      else
        tmpsql:=' agegroup,';
        i:=i+1;
        groupby[i]:='agegroup';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name4='x' then 
        tmpsql:=' ''x''::varchar sex,';
      else
        tmpsql:=' sex,';
        i:=i+1;
        groupby[i]:='sex';
      end if;
      insrtsql:=insrtsql||tmpsql;
      if cursrow.name5='x' then 
        tmpsql:=' ''x''::varchar race_ethnicity,';
      else
        tmpsql:=' race_ethnicity,';
        i:=i+1;
        groupby[i]:='race_ethnicity';
      end if;
      insrtsql:=insrtsql||tmpsql;
      insrtsql:=insrtsql||'
         count(distinct patient_id) total,
         count(distinct scnd_enc) scnd_enc,
         count(distinct hiv_dx) hiv_dx,
         count(distinct hiv_rx) hiv_rx,
         count(distinct retained) retained,
         count(distinct vload) vload,
         count(distinct vl_rx) vl_rx,
         count(distinct sprsd) sprsd,
         count(distinct spr_rx) spr_rx,
         count(distinct opt) opt,
         count(distinct tot_csyr) tot_csyr,
         count(distinct linked) linked,
         count(distinct scnd_enc_csyr) scnd_enc_csyr,
         count(distinct hiv_dx_csyr) hiv_dx_csyr,
         count(distinct hiv_rx_csyr) hiv_rx_csyr,
         count(distinct retained_csyr) retained_csyr,
         count(distinct vload_csyr) vload_csyr,
         count(distinct vl_rx_csyr) vl_rx_csyr,
         count(distinct sprsd_csyr) sprsd_csyr,
         count(distinct opt_csyr) opt_csyr
         from cc_hiv_by_pat
         ';
      if i> 0 then 
        insrtsql:=insrtsql||'group by ';
        i:=1;
        while i <= array_length(groupby, 1)
        loop
          if i >1 then 
            insrtsql:=insrtsql||', '; 
          end if;
          insrtsql:=insrtsql||groupby[i];
          i:=i+1;
        end loop;
      end if;
      insrtsql:=insrtsql||')';
      execute insrtsql;
      --raise notice '%', insrtsql;
    end loop;
  end;      
$$ 
language plpgsql;

--Gather up the values of the stratifiers and assign code.
drop table if exists advp.cc_agegroup_codevals;
Create table advp.cc_agegroup_codevals (agegroup varchar(7), codeval varchar(1));
insert into advp.cc_agegroup_codevals (agegroup, codeval) 
values ('0-19','1'),('20-39','2'),('40-59','3'),('Over 59','4'),('UNKNOWN','5');

drop table if exists advp.cc_sex_codevals;
Create table advp.cc_sex_codevals (sex varchar(7), codeval varchar(1));
insert into advp.cc_sex_codevals (sex, codeval) 
values ('Female','1'),('Male','2'),('Unknown','3');

drop table if exists advp.cc_race_ethnicity_codevals;
Create table advp.cc_race_ethnicity_codevals (race_ethnicity varchar(10), codeval varchar(1));
insert into advp.cc_race_ethnicity_codevals (race_ethnicity, codeval) 
values ('ASIAN','1'),('BLACK','2'),('CAUCASIAN','3'),('HISPANIC','4'),('OTHER','5'),('UNKNOWN','6');

--now write out to JSON
copy (
select row_to_json(t1)
from
(select 'Continuity of Care' tablename,
    '["year","age_group","sex","race_ethnicity"]'::json filters,
'{"a":{"name":"HIV patients in care","description":"Patients meeting the ESP case definition for HIV with at least one encounter for any reason during the selected period.","nest":"0","pcalc":"a"},"b":{"name":"Follow-up encounter","description":"Second encounter following qualifying encounter","nest":"1","pcalc":"a"},"c":{"name":"Follow-up encounter with HIV diagnosis code","description":"Encounter with an HIV diagnosis code following the qualifying encounter","nest":"1","pcalc":"a"},"d":{"name":"Patients prescribed HIV medications","description":"Prescription for ARTs during the selected period or later","nest":"1","pcalc":"a"},"e":{"name":"Retained in care","description":"HIV individuals who had two or more of the following performed at least three months apart following the qualifying encounter: viral load test, CD4+ test, encounter with an HIV diagnosis, or prescriptions for HIV medications.","nest":"1","pcalc":"a"},"f":{"name":"Patients with a viral load test in or after the selected period","description":"Viral load with any value during the selected period","nest":"1","pcalc":"a"},"g":{"name":"Patients with a viral load test and prescribed HIV medications","description": "HIV patients receiving medications with viral load test ","nest":"1","pcalc":"a"},"h":{"name":"Virally suppressed","description":"Patients whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"a"},"i":{"name":"Virally suppressed and on medications","description":"Patients receiving medications and whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"a"},"j":{"name":"Opportunistic Infection","description":"Patients with a diagnosis code for any opportunisitic infection in or after the selected period","nest":"1","pcalc":"a"},"k":{"name":"Patients newly diagnosed with HIV during the selected period","description":"Patients with a positive (+Western Blot or +ELISA or +Ab/Ag) on or before the case identification date or history of negative (HIV ELISA or Ab/Ag) within 2 years preceding the case identification date.","nest":"0","pcalc":"k"},"l":{"name":"Linked to care","description":"Patients diagnosed with HIV during the selected period who had at least one viral load or CD4 test or prescription for an HIV medication within 0-90 days following diagnosis ","nest":"1","pcalc":"k"},"m":{"name":"Follow-up encounter","description":"Encounter following case detection","nest":"1","pcalc":"k"},"n":{"name":"Follow-up encounter with HIV diagnosis code","description":"Encounter with an HIV diagnosis code in the selected period or later","nest":"1","pcalc":"k"},"o":{"name":"Patients prescribed HIV medications","description":"Prescription for HIV medications in the selected period or later","nest":"1","pcalc":"k"},"p":{"name":"Retained in care ","description":"Diagnosed individuals who had two or more of the following performed at least three months apart: viral load test, CD4+ test, encounter with an HIV diagnosis, or prescriptions for HIV medications.","nest":"1","pcalc":"k"},"q":{"name":"Patients with a viral load test","description":"Viral load with any value following case identification","nest":"1","pcalc":"k"},"r":{"name":"Patients with a viral load test and prescribed HIV medications","description":"Any value, any time after case detection","nest":"1","pcalc":"k"},"s":{"name":"Virally suppressed","description":"Patients whose most recent HIV viral load was less than 200 copies/mL.","nest":"1","pcalc":"k"},"t":{"name":"Opportunistic Infection","description":"Patients with a diagnosis code for any opportunistic infection following case detection.","nest":"1","pcalc":"k"}}'::json rowmeta,
(select '{'||array_to_string(array_agg(row_),',')||'}' from  (select ('"'||
          case when hiv_yr = 'x' then 'YEAR' else hiv_yr end ||
          case when agegroup <> 'x' 
            then (select codeval from advp.cc_agegroup_codevals cv where t0.agegroup=cv.agegroup)
          else 'x' end ||
          case when sex <> 'x' 
            then (select codeval from advp.cc_sex_codevals cv where t0.sex=cv.sex)
          else 'x' end ||
          case when race_ethnicity <> 'x' 
            then (select codeval from advp.cc_race_ethnicity_codevals cv where t0.race_ethnicity=cv.race_ethnicity)
          else 'x' end || '":{"a":'|| total
		  || ',"b":' || scnd_enc
		  || ',"c":' || hiv_dx
		  || ',"d":' || hiv_rx
		  || ',"e":' || retained
		  || ',"f":' || vload
		  || ',"g":' || vl_rx
		  || ',"h":' || sprsd
		  || ',"i":' || spr_rx
		  || ',"j":' || opt
		  || ',"k":' || tot_csyr
		  || ',"l":' || linked
		  || ',"m":' || scnd_enc_csyr
		  || ',"n":' || hiv_dx_csyr
		  || ',"o":' || hiv_rx_csyr
		  || ',"p":' || retained_csyr
		  || ',"q":' || vload_csyr
		  || ',"r":' || vl_rx_csyr
		  || ',"s":' || sprsd_csyr
		  || ',"t":' || opt_csyr
		  || '}') row_
from advp.cc_hiv_summaries t0 ) t00)::json rowdata) t1) to :'pathToFile';




  

