/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             ADVP Tables for Monthly
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <https://www.commoninf.com>
-- @contact: https://esphealth.org
-- @copyright: (c) 2024 Commonwealth Informatics
-- @license: LGPL 3.0 - https://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
create schema if not exists advp;
set search_path to advp, gen_pop_tools, public;

--A good argument could be made here for creating a single new sql script that builds the *_pat_* tables, which are going to be the same for 
--  TT and for ADVP.  It could be called in the shell scripts that build either ADVP or GEN_POP_TOOLS.
--This would ensure that general issues that involve making changes to the PAT tables, would only have to be made in one place.
--However, I'd rather not change up the working TT scripts, which you'd have to do to separate out the creation of the PAT tables.
--Plus, ADVP is a different app, and I can imagine that the PAT tables (which essentially represents the analysis cohort) may diverge for the two apps.
--So, we end up with a set of tables in the ADVP schema that are essentially replicas and to some extent use what exists in GEN_POP_TOOLs. (BZ)

--
-- FOR ADVP KEEP!!!! build patient table
--
select 'Starting to build advp_pat at: ',now();
drop table if exists advp_pat;
create table advp_pat as
SELECT 
  pat.id AS patient_id, date_part('year', age(pat.date_of_birth::date)) as age, upper(substr(pat.gender,1,1)) gender, 
  date_part('year',date_of_birth::date) birth_year,
  case 
     when (select t00.mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_table='emr_patient' and t00.src_field='ethnicity' 
                and t00.src_value=pat.ethnicity and t00.mapped_value='HISPANIC') is not null
       then (select t00.mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_table='emr_patient' and t00.src_field='ethnicity' 
                and t00.src_value=pat.ethnicity and t00.mapped_value='HISPANIC')
     else (select t00.mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_table='emr_patient' and t00.src_field='race' 
                and t00.src_value=pat.race)
  end as race, 
  (select t00.mapped_value from gen_pop_tools.rs_conf_mapping t00 where t00.src_table='emr_patient' and t00.src_field='ethnicity_new' 
                and t00.src_value=pat.ethnicity) as ethnicity,
  pat.date_of_death, pat.date_of_birth,
  case
    when substring(pat.zip,6,1)='-' then substring(pat.zip,1,5)
    else pat.zip
  end as zip
  --,census_tract --placeholder if needed.
FROM emr_patient pat;
alter table advp_pat add primary key (patient_id);
analyze advp_pat;
--
-- Build the patient_encounter list
---
select 'Starting to build advp_enc_pat at: ',now();
drop table if exists advp_enc_pat;
create table advp_enc_pat as
select patient_id, 
       to_char(date,'yyyy_mm') year_month, 
       count(*) as counts
from gen_pop_tools.clin_enc
group by patient_id, to_char(date,'yyyy_mm');


drop table if exists advp_1stenc_pat;
create table advp_1stenc_pat as
select patient_id, min(year_month) as year_month
from advp_enc_pat
group by patient_id;
alter table advp_1stenc_pat add primary key (patient_id);
analyze advp_1stenc_pat;
--
--utilty table with all months in seqence to current date;
--
drop table if exists advp_month_series;
create table advp_month_series as 
select year_month from
(select to_char(generate_series('2010-01-01'::timestamp,current_timestamp,'1 month')::date,'YYYY_MM') as year_month) t0
where year_month <= to_char(now() - interval '1 month' ,'YYYY_MM');
--Since we will be running this on at least a lag of 2 days, and we only want full months of data, the current date will never represent a full month.
alter table advp_month_series add primary key (year_month); 
analyze advp_month_series;
--
-- pat_seq table with age at each year_month
--
drop table if exists advp_pat_seq;
create table advp_pat_seq as
select ser.year_month, p1st.patient_id
from advp_month_series ser
join advp_1stenc_pat p1st on p1st.year_month<=ser.year_month;
alter table advp_pat_seq add primary key (patient_id, year_month);
analyze advp_pat_seq;
--now pat_seq_enc, which adds count of encounters and age at each month
drop table if exists advp_pat_seq_enc;
create table advp_pat_seq_enc as
select pat.patient_id, seq.year_month, 
       date_part('year', age(to_date(seq.year_month,'yyyy_mm'),pat.date_of_birth::date)) as age,
       sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month range between unbounded preceding and current row) as allprior,
       sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 23 preceding and current row) as prior2,
       sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 11 preceding and current row) as prior1,
       case substr(seq.year_month,6,2)
         when '09' then 0::integer
         when '10' then 1::integer
         when '11' then 2::integer
         when '12' then 3::integer
         when '01' then 4::integer
         when '02' then 5::integer
         when '03' then 6::integer
         when '04' then 7::integer
         when '05' then 8::integer
         when '06' then 9::integer
         when '07' then 10::integer
         when '08' then 11::integer
       end as flu_month
from advp_pat pat
join advp_pat_seq seq on pat.patient_id=seq.patient_id
left join advp_enc_pat eseq on seq.patient_id=eseq.patient_id and seq.year_month=eseq.year_month;
--that was a left join of the encounters, so if there are no prior1 or prior2 counts, you get null values.
alter table advp_pat_seq_enc add primary key (patient_id, year_month);
analyze advp_pat_seq_enc;
--
-- pregnancy by month
--
select 'Starting to build advp_preg1 at: ',now();
drop table if exists advp_preg1;
create table advp_preg1 as
SELECT seq.patient_id
    , seq.year_month
    , max(1) recent_pregnancy
FROM hef_timespan span
JOIN advp_pat_seq_enc seq on seq.patient_id=span.patient_id 
     and to_date(seq.year_month,'yyyy_mm') + interval '1 month' > span.start_date
     and to_date(seq.year_month,'yyyy_mm') + interval '1 month' < case 
                                 when span.end_date is not null then span.end_date
                     when span.start_date + interval '10 months' > now() then now()
                   end
GROUP BY seq.patient_id, seq.year_month;
--
--   - Smoking
--
select 'Starting to build advp_smoking at: ',now();
drop table if exists advp_smoking;
create table advp_smoking as
select *, max(year_month) over (partition by patient_id)
from (
select to_char(max(cm.code),'9') smoking,
           seq.patient_id,
           seq.year_month
from advp_pat_seq_enc seq
left join emr_socialhistory sh on seq.patient_id=sh.patient_id and to_char(sh.date,'yyyy_mm')=seq.year_month
left join rs_conf_mapping cm on cm.src_value=sh.tobacco_use and cm.src_table='emr_socialhistory' and cm.src_field='tobacco_use'
     where sh.tobacco_use is not null and sh.tobacco_use<>''
     group by seq.patient_id, seq.year_month) t0;
--
-- ANAPLASMOSIS
--
--
-- For binary case types (patient has or doesn't have the condition) 
--    max(1) is just an easy way to assign value=1 for "has the condition" in the context of group-byselect 'Starting to build advp_anaplasmosis at: ',now();
drop table if exists advp_anaplasmosis;
create table advp_anaplasmosis as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) anaplasmosis 
from nodis_case c
where c.condition='anaplasmosis'
group by patient_id, to_char(date,'yyyy_mm');
--
-- BABESIOSIS
--
select 'Starting to build advp_babesiosis at: ',now();
drop table if exists advp_babesiosis;
create table advp_babesiosis as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) babesiosis 
from nodis_case c
where c.condition='babesiosis'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - Covid confirmed
--
select 'Starting to build advp_covid_case at: ',now();
drop table if exists advp_covid_case;
create table advp_covid_case as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
        , max(1) covid 
from nodis_case c
where c.condition='covid19_confirmed'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - Chlamydia cases
--
select 'Starting to build advp_chlamydia_case at: ',now();
drop table if exists advp_chlamydia_case;
create table advp_chlamydia_case as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
        , max(1) chlamydia 
from nodis_case c
where c.condition='chlamydia'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - gonorrhea cases
--
select 'Starting to build advp_gonorrhea_case at: ',now();
drop table if exists advp_gonorrhea_case;
create table advp_gonorrhea_case as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
        , max(1) gonorrhea 
from nodis_case c
where c.condition='gonorrhea'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - syphilis cases
--
select 'Starting to build advp_syphilis_case at: ',now();
drop table if exists advp_syphilis_case;
create table advp_syphilis_case as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
        , max(1) syphilis 
from nodis_case c
where c.condition='syphilis'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - hiv cases
--
select 'Starting to build advp_hiv at: ',now();
drop table if exists advp_hiv;
create table advp_hiv as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
        , max(1) hiv 
from nodis_case c
where c.condition='hiv'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Hep A CASES
--
select 'Starting to build advp_hep_a at: ',now();
drop table if exists advp_hep_a;
create table advp_hep_a as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) hep_a 
from nodis_case c
where c.condition='hepatitis_a:acute'
group by patient_id, to_char(date,'yyyy_mm');
--
-- Table for ADVP - hepb cases
--
select 'Starting to build advp_hep_b at: ',now();
drop table if exists advp_hep_b;
select distinct on (c.patient_id, to_char(cah.date,'yyyy_mm')) 
      c.patient_id
    , to_char(cah.date,'yyyy_mm') year_month
    , case (cah.status) when 'HEP_B-A' then 1 when 'HEP_B-C' then 2 end as hep_b
into advp_hep_b
from nodis_case c
join nodis_caseactivehistory cah on cah.case_id=c.id
where c.condition='hepatitis_b'
order by patient_id, to_char(cah.date,'yyyy_mm'), cah.date desc;
--
-- Table for ADVP - hepc cases
--
select 'Starting to build advp_hep_c at: ',now();
drop table if exists advp_hep_c;
create table advp_hep_c as
  select distinct on (c.patient_id, to_char(cah.date,'yyyy_mm')) c.patient_id
    , to_char(cah.date,'yyyy_mm') year_month
    , case (cah.status) when 'HEP_C-U' then 1 when 'HEP_C-A' then 2 when 'HEP_C-C' then 3 end as hep_c
from nodis_case c
join nodis_caseactivehistory cah on cah.case_id=c.id
where c.condition='hepatitis_c'
order by patient_id, to_char(cah.date,'yyyy_mm'), cah.date desc;
--
-- Table for ADVP - TB cases
--
select 'Starting to build TB at: ',now();
drop table if exists advp_tuberculosis;
create table advp_tuberculosis as
  select distinct on (c.patient_id, to_char(cah.date,'yyyy_mm')) 
      c.patient_id
    , to_char(cah.date,'yyyy_mm') year_month
    , case (cah.status) when 'TB-L' then 1 when 'TB-A' then 2 end as tb
from nodis_case c
join nodis_caseactivehistory cah on cah.case_id=c.id
where c.condition='tuberculosis'
order by patient_id, to_char(cah.date,'yyyy_mm'), cah.date desc;
--
-- Lyme CASES
--
select 'Starting to build advp_lyme at: ',now();
drop table if exists advp_lyme;
create table advp_lyme as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) lyme 
from nodis_case c
where c.condition='lyme'
group by patient_id, to_char(date,'yyyy_mm');
-- Testing and screening section 
--
-- chlamydia lab result
--
select 'Starting to build advp_chlamydia_lx at: ',now();
drop table if exists advp_chlamydia_lx;
create table advp_chlamydia_lx as
    SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) chlamydia_lx
    FROM hef_event e0
    WHERE e0.name in ('lx:chlamydia:positive', 'lx:chlamydia:negative', 'lx:chlamydia:indeterminate')
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- gonorrhea lab result
--
select 'Starting to build advp_gonorrhea_lx at: ',now();
drop table if exists advp_gonorrhea_lx;
create table advp_gonorrhea_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) gonorrhea_lx
    FROM hef_event e0
    WHERE e0.name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative', 'lx:gonorrhea:indeterminate')
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- Syphilis lab result
--
select 'Starting to build advp_syphilis_lx at: ',now();
drop table if exists advp_syphilis_lx;
create table advp_syphilis_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) syphilis_lx
    FROM (select patient_id, date , test_name
from emr_labresult lr join conf_labtestmap ltm on ltm.native_code=lr.native_code
where test_name in ('rpr','rpr_riskscape','vdrl','tppa','fta-abs','tp-igg','tp-cia','tp-igm','vdrl-csf','tppa-csf','fta-abs-csf'))
 e0
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- Hep B lab result
--
select 'Starting to build advp_hep_b_lx at: ',now();
drop table if exists advp_hep_b_lx;
create table advp_hep_b_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) hep_b_lx
    FROM hef_event e0
    WHERE e0.name like 'lx:hepatitis_b_surface_antigen%' or e0.name like 'lx:hepatitis_b_viral_dna%'  
       or e0.name like 'lx:hepatitis_b_core_antigen_igm_antibody%' 
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- Hep C lab result
--
select 'Starting to build advp_hep_c_lx at: ',now();
drop table if exists advp_hep_c_lx;
create table advp_hep_c_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) hep_c_lx
    FROM hef_event e0
    WHERE e0.name like 'lx:hepatitis_c_elisa:%' or e0.name like 'lx:hepatitis_c_signal_cutoff:%'
       or e0.name like 'lx:hepatitis_c_riba:%' or e0.name like 'lx:hepatitis_c_rna:%'
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- Hep C ELISA reflex to viral RNA
--
select 'Starting to build advp_hep_c_elisa_ref at: ',now();
drop table if exists advp_hep_c_elisa_ref;
create table advp_hep_c_elisa_ref as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , case
        when max(e1.patient_id) is not null then 2
        when max(e2.patient_id) is not null then 3
        else 1
    end as reflex
    FROM hef_event e0
    left join hef_event e1 on e1.patient_id=e0.patient_id and e1.date=e0.date and e1.name like 'lx:hepatitis_c_rna%'
    left join hef_event e2 on e2.patient_id=e0.patient_id and e2.date between e0.date and e0.date + interval '30 days' and e2.name like 'lx:hepatitis_c_rna%'
    WHERE e0.name = 'lx:hepatitis_c_elisa:positive' 
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- HIV lab result
--
select 'Starting to build advp_hiv_lx at: ',now();
drop table if exists advp_hiv_lx;
create table advp_hiv_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) hiv_lx
    FROM hef_event e0
    WHERE e0.name like 'lx:hiv_ag_ab:%' or e0.name like 'lx:hiv_elisa:%'
       or e0.name like 'lx:hiv_pcr:%' or e0.name like 'lx:hiv_wb:%'
       or e0.name like 'lx:hiv_rna_viral:%' or e0.name like 'lx:hiv_multispot:%'
       or e0.name like 'lx:hiv_geenius:%' or e0.name like 'lx:hiv_ab_diff:%'
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- TB lab result
--
select 'Starting to build advp_tb_lx at: ',now();
drop table if exists advp_tb_lx;
create table advp_tb_lx as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) tb_lx
    FROM 
      (SELECT patient_id, date from hef_event 
       WHERE name like 'lx:tb_igra:%'
       UNION
       SELECT patient_id, date from emr_prescription
       WHERE name ilike '%ppd%'
       UNION 
       SELECT patient_id, date from emr_immunization
       WHERE name ilike '%ppd%'
	   UNION
	   SELECT patient_id, date from emr_labresult
	   WHERE native_name ilike 'ppd%' or native_name ilike '%tuber%ppd%'
       ) e0
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
-- 
-- influenza vaccine
--
select 'Starting to build advp_flu_vax at: ',now();
drop table if exists advp_flu_vax;
create table advp_flu_vax as
    SELECT 
      patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) AS flu_vax
    FROM emr_immunization
    WHERE (name ILIKE '%influenza%' or name ilike '%flu vac%' or name='flu')
    group by patient_id, to_char(date,'yyyy_mm');
--
-- tdap vaccine
--
select 'Starting to build advp_tdap at: ',now();
drop table if exists advp_tdap_pre0;
create table advp_tdap_pre0 as
    SELECT 
      i.patient_id
    , to_char(i.date,'yyyy_mm') year_month
    , max(1) AS tdap_vaccine
    FROM emr_immunization i
    WHERE (i.name ILIKE '%tdap%')
    group by i.patient_id, to_char(i.date,'yyyy_mm');
drop table if exists advp_tdap_pre;
create table advp_tdap_pre as
select t0.*
      , date_part('year',age(to_date(t0.year_month,'yyyy_mm'),p.date_of_birth::date)) age
from advp_tdap_pre0 t0 join emr_patient p on p.id=t0.patient_id;
drop table if exists advp_tdap;
create table advp_tdap as
select tdapp.patient_id, pse.year_month, max(tdapp.tdap_vaccine) tdap_vaccine
  from advp_pat_seq_enc pse
  join advp_tdap_pre tdapp on pse.patient_id=tdapp.patient_id 
    and ((pse.year_month>=tdapp.year_month and tdapp.age<=18 and pse.age<=18) 
         or (pse.year_month>=tdapp.year_month and tdapp.age>18 and pse.age>18)) 
  group by tdapp.patient_id, pse.year_month;
-- 
-- covid vaccine
--
select 'Starting to build advp_covid_vax at: ',now();
drop table if exists advp_covid_vax;
create table advp_covid_vax as
    SELECT 
      patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) AS covid_vax
    FROM emr_immunization
    WHERE (name ILIKE '%covid%')
    group by patient_id, to_char(date,'yyyy_mm');
-- 
-- Hep A
--
select 'Starting to build advp_hep_a_vax at: ',now();
drop table if exists advp_hep_a_vax;
create table advp_hep_a_vax as
    SELECT 
      patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) AS hep_a_vax
    FROM emr_immunization
    WHERE (name ILIKE '%hep a%')
    group by patient_id, to_char(date,'yyyy_mm');
-- 
-- Hep B
--
select 'Starting to build advp_hep_b_vax at: ',now();
drop table if exists advp_hep_b_vax;
create table advp_hep_b_vax as
    SELECT 
      patient_id
    , to_char(date,'yyyy_mm') year_month
    , max(1) AS hep_b_vax
    FROM emr_immunization
    WHERE (name ILIKE '%hep b%')
    group by patient_id, to_char(date,'yyyy_mm');

--Treatment section
--
-- PrEP 
--
select 'Starting to build advp_prep at: ',now();
drop table if exists advp_prep;
select patient_id
 , to_char(date,'yyyy_mm') year_month
 , max(1) as prep
into advp_prep
from hef_event he
WHERE name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
        'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
        'rx:hiv_cabotegravir_er_600')
  and not exists (select null from nodis_case c where c.patient_id=he.patient_id and c.date <=he.date and c.condition='hiv')
group by patient_id, to_char(date,'yyyy_mm');
--
-- Doxycycline lyme
--
select 'Starting to build advp_doxy_lyme at: ',now();
drop table if exists advp_doxy_lyme;
create table advp_doxy_lyme as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) doxy
    FROM hef_event e0
    WHERE e0.name = 'rx:doxycycline_7_day'
      and exists (select null from nodis_case c0 
                  where c0.condition='lyme' and c0.patient_id=e0.patient_id and e0.date between c0.date-interval '30 days' and c0.date)
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--
-- Doxycycline sti
--
select 'Starting to build advp_doxy_sti at: ',now();
drop table if exists advp_doxy_sti;
create table advp_doxy_sti as
        SELECT 
      e0.patient_id
    , to_char(e0.date,'yyyy_mm') year_month
    , max(1) doxy
    FROM hef_event e0
    WHERE e0.name = 'rx:doxycycline_7_day'
      and exists (select null from nodis_case c0 
                  where c0.condition='chlamydia' and c0.patient_id=e0.patient_id and e0.date between c0.date-interval '30 days' and c0.date)
    GROUP BY e0.patient_id, to_char(e0.date,'yyyy_mm');
--other stuff
--
-- Primary Payer
--
select 'Starting to build advp_primary_payer at: ',now();
drop table if exists advp_primary_payer;
create table advp_primary_payer as 
select t0.patient_id, t1.year_month,
max(t00.code) primary_payer
from emr_encounter t0
join (select patient_id, to_char(date,'YYYY_MM') year_month, max(date) maxdate
      from emr_encounter where primary_payer is not null
      group by patient_id, to_char(date,'YYYY_MM')) t1 on t0.patient_id=t1.patient_id and t0.date=t1.maxdate
join gen_pop_tools.rs_conf_mapping t00 on t00.src_table='emr_encounter' and t00.src_field='primary_payer'
                  and t00.src_value = t0.primary_payer
where t0.primary_payer is not null or primary_payer != ''
group by t0.patient_id, t1.year_month;

--Here is the syphilis screening during pregnancy stuff, and then the DTAP in pregnancy
-- Use timespan data with pregnancy start and end dates that span of 36-44 weeks (252-308 days) 
-- all same day tests should count as 1.
select 'Starting to build advp_syph_screen_preg at: ',now();
drop table if exists advp_syph_screen_preg;

select distinct patient_id, start_date, end_date 
into temporary temp_pregnancy_pre_complete
from hef_timespan
where end_date is not null and name='pregnancy'
and abs(start_date - end_date) >= 252
and abs(start_date - end_date) <= 308;

select patient_id, start_date, end_date 
into temporary temp_pregnancy_pre_complete_clean1
from 
(select patient_id, start_date, end_date, rank() over (partition by patient_id, start_date order by end_date desc) 
 from temp_pregnancy_pre_complete) a 
where rank=1;

select patient_id, start_date, end_date, lag(start_date, 1) over (partition by patient_id order by start_date) as otherstartdate 
into temporary temp_pregnancy_pre_complete_clean2
from temp_pregnancy_pre_complete_clean1;

select patient_id, start_date, end_date 
into temporary temp_pregnancy_complete
from temp_pregnancy_pre_complete_clean2
where otherstartdate is null or abs(otherstartdate - start_date) > 30;

select patient_id, date as laborderdate
into temporary temp_syph_screening
from emr_labresult lr join conf_labtestmap ltm on ltm.native_code=lr.native_code
where test_name in ('rpr','rpr_riskscape','vdrl','tppa','fta-abs','tp-igg','tp-cia','tp-igm','vdrl-csf','tppa-csf','fta-abs-csf')
group by patient_id, date;

select temp_pregnancy_complete.patient_id, temp_pregnancy_complete.start_date, end_date, laborderdate as dateofsyphscreen
into temporary temp_pregnancy_complete_syphscreen
from 
temp_pregnancy_complete
left join temp_syph_screening 
on temp_pregnancy_complete.patient_id = temp_syph_screening.patient_id
and laborderdate >= start_date and laborderdate <= end_date;

select patient_id, to_char(end_date, 'yyyy_mm') as year_month, 
  case 
    when sum(case when dateofsyphscreen is not null then 1 else 0 end) = 1 then 1
    when sum(case when dateofsyphscreen is not null then 1 else 0 end) >= 2 then 2
    else 0
  end as syph_screen_preg
into advp_syph_screen_preg
from temp_pregnancy_complete_syphscreen
group by patient_id, to_char(end_date, 'yyyy_mm'), 
  case when dateofsyphscreen is not null then 1 else 0 end;
--
--here is dtap in pregnancy
--
select 'Starting to build advp_dtap_preg at: ',now();
drop table if exists advp_dtap_preg;
select t1.patient_id, to_char(end_date, 'yyyy_mm') as year_month,
  case 
    when sum(tdap_vaccine)>0 then 1
    else 0
  end as dtap_preg
into advp_dtap_preg
from temp_pregnancy_complete t1
left join advp_tdap_pre0 t0 on t1.patient_id=t0.patient_id 
  and to_date(t0.year_month,'yyyy_mm') between t1.start_date and t1.end_date
group by t1.patient_id, to_char(end_date, 'yyyy_mm');

--
--here is the "High risk for STIs" indicator
--
select 'Starting to build advp_high_sti_risk at: ',now();
drop table if exists advp_high_sti_risk;
with risk_scores as
  (select t0.patient_id, t0.year_month, 1 as high_risk
   from advp_pat_seq t0
   join gen_pop_tools.cc_hiv_risk_score t1 on t1.patient_id=t0.patient_id and t1.rpt_year=substr(t0.year_month,1,4)
   where t1.hiv_risk_score >= 0.01)
, icd10z72_5 as
  (select patient_id, date as start_date, date + interval '1 year' as end_date 
   from emr_encounter e0 join emr_encounter_dx_codes e1 on e1.encounter_id=e0.id 
   where e1.dx_code_id ilike 'icd10:Z72.5%'
   group by patient_id, date)
, icd_ym as
  (select t0.patient_id, t0.year_month, 1 as high_risk
   from advp_pat_seq t0
   join icd10z72_5 t1 on t1.patient_id=t0.patient_id 
                         and t0.year_month between to_char(t1.start_date,'yyyy_mm') and to_char(t1.end_date,'yyyy_mm')
   group by t0.patient_id, t0.year_month)
select patient_id, year_month, high_risk
into advp_high_sti_risk
from (select * from risk_scores
      union
      select * from icd_ym) t0;
					
-- HIV New Diagnosis
-- Defined as a positive test (+Western Blot or +ELISA  or + AG/AB or + AB Diff or +Geenius or +Multispot) on or within 30 days prior to the ESP HIV case date.
        -- Exclude patients with an HIV dx code prior to the ESP HIV case date
        -- Exclude patients with script for HIV meds (other than PrEP meds) on the date of the index positive test
        -- Exclude patients with HIV viral load = negative within 0 to +7 days of the index positive test date
-- OR
    -- History of negative HIV ELISA or negative AG/AB within 2 years preceding the ESP HIV case date AND a positive test (+Western Blot or +ELISA  or + AG/AB or + AB Diff or +Geenius or +Multispot) AFTER the ESP HIV case date 
        -- Exclude patients with an HIV dx code prior to the ESP HIV case date

SELECT 'Starting to build advp_hiv_new_diag_pats at: ',now();
-- BUILD ANCHOR PATIENTS & DATES STEP 1
DROP TABLE IF EXISTS advp.hiv_events_cases;
CREATE TABLE advp.hiv_events_cases AS
SELECT T1.patient_id,T1.name,T1.date, T2.id as case_id, T2.date as hiv_per_esp_date 
FROM hef_event T1
JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.name in ('lx:hiv_ab_diff:positive', 'lx:hiv_ag_ab:negative', 'lx:hiv_ag_ab:positive', 
'lx:hiv_elisa:negative', 'lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive', 'dx:hiv')
AND condition = 'hiv';
-- BUILD ANCHOR PATIENTS & DATES STEP 2
DROP TABLE IF EXISTS advp.hiv_eval_labs;
CREATE TABLE advp.hiv_eval_labs AS 
SELECT patient_id, case_id, hiv_per_esp_date,
max(case when name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and date <= hiv_per_esp_date then 1 else 0 end) pos_lab_met,
min(case when name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and date <= hiv_per_esp_date then date end) min_lab_pos_date,
max(case when name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and date <= hiv_per_esp_date then date end) max_lab_pos_date,
max(case when name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and date > (hiv_per_esp_date - INTERVAL '2 years') and date < hiv_per_esp_date then 1 else 0 end) neg_lab_met,
min(case when name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and date > (hiv_per_esp_date - INTERVAL '2 years') and date < hiv_per_esp_date then date end) min_neg_lab_date,
max(case when name = 'dx:hiv' and date < hiv_per_esp_date then 1 else 0 end) as hiv_dx_before_case_date,
max(case when name = 'dx:hiv' and date >= hiv_per_esp_date then 1 else 0 end) as hiv_dx_on_after_case_date,
max(case when name = 'dx:hiv' and date > hiv_per_esp_date then 1 else 0 end) as hiv_dx_after_case_date,
min(case when name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and date >= hiv_per_esp_date then T1.date end) min_lab_pos_date_after_case
FROM advp.hiv_events_cases T1 
GROUP BY patient_id, case_id, hiv_per_esp_date;
-- GET HIV MED DETAILS FOR POSSIBLE NEW HIV DIAGNOSIS EXCLUSION
DROP TABLE IF EXISTS advp.hiv_hrs_eval_labs_meds;
CREATE TABLE advp.hiv_hrs_eval_labs_meds AS 
SELECT T2.patient_id,
MAX(CASE WHEN T2.name is not null then 1 ELSE 0 END) as hiv_med_on_max_lab_pos_date
FROM advp.hiv_eval_labs T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE name ilike 'rx:hiv%'
AND name not in ('rx:hiv_tenofovir-emtricitabine:generic', 
					 'rx:hiv_tenofovir-emtricitabine', 
					 'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
					 'cabotegravir_er_600')
AND T2.date = T1.max_lab_pos_date
GROUP BY T2.patient_id;
--  GET HIV RNA VIRAL RESULTS FOR POSSIBLE NEW HIV DIAGNOSIS EXCLUSION
DROP TABLE IF EXISTS advp.hiv_hrs_eval_labs_rnaviral;
CREATE TABLE advp.hiv_hrs_eval_labs_rnaviral AS 
SELECT DISTINCT T1.patient_id, T1.case_id, 1::int as hiv_rna_neg_7daysafter_max_lab_pos_date
--T1.max_lab_pos_date, T2.date, T2.date -T1.max_lab_pos_date as date_diff, T3.test_name, T4.name as hef_name, result_string, result_float
FROM advp.hiv_eval_labs T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name = 'hiv_rna_viral'
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED|QUANTITY NOT SUFFICIENT|QNS|SENT OUT')
AND result_string is not null
AND (T2.date >= T1.max_lab_pos_date AND T2.date <= T1.max_lab_pos_date + INTERVAL '7 days')
AND (T4.name in ('lx:hiv_rna_viral:negative') or T4.name is null)
AND (T2.result_float is null or T2.result_float = 0)
AND upper(T2.result_string) !~ ('<20 DETECTED|<30 DETECTED|<1.30 DETECTED|^>');
-- BUILD ANCHOR PATIENTS & DATES STEP 3
-- to handle events prior to having ab_diff type, need to filter out patients where the pos/neg closest to the case date are on the same date
-- use max_lab_pos_date as index date
-- filter out test patients
DROP TABLE IF EXISTS advp.hiv_hiv_new_diag_pats;
CREATE TABLE advp.hiv_hiv_new_diag_pats AS
SELECT T1.patient_id, T1.case_id, to_char(hiv_per_esp_date, 'YYYY_MM') as year_month, 1 as hiv_new_diagnosis
FROM advp.hiv_eval_labs T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN advp.hiv_hrs_eval_labs_meds T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN advp.hiv_hrs_eval_labs_rnaviral T4 ON (T1.patient_id = T4.patient_id)
WHERE (pos_lab_met = 1 or neg_lab_met = 1)
-- Gap between pos lab test and case date must be <= 30 days
AND (
        -- Gap between pos lab test (before case) and case date must be <= 30 days
        (abs(max_lab_pos_date - hiv_per_esp_date) <= 30)
		 OR
		-- If neg test only then must have positive test after the case date
		( min_lab_pos_date is null and min_lab_pos_date_after_case is not null)
	)
-- no hiv dx before case date
AND hiv_dx_before_case_date = 0 
-- no prep med on max_lab_pos_date
AND (hiv_med_on_max_lab_pos_date is null or hiv_med_on_max_lab_pos_date != 1)
-- no neg hiv_rna_viral 0-7 days after max_lab_pos_date
AND hiv_rna_neg_7daysafter_max_lab_pos_date is null
AND last_name not ilike 'LABVALIDATION'
AND last_name not ilike 'XXXXX%'
AND last_name not ilike 'Fenway%Test';

--
-- DOXY PEP
--
DROP TABLE IF EXISTS advp.doxy_all_meds;
CREATE TABLE advp.doxy_all_meds AS
SELECT id, patient_id, date, name, directions, dose, to_char(date, 'YYYY_MM') as rx_mon_yr
FROM emr_prescription 
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%');
-- ABSOULUTE INCLUSION
DROP TABLE IF EXISTS advp.doxy_abs_inclusion_meds;
CREATE TABLE advp.doxy_abs_inclusion_meds AS
SELECT *
FROM advp.doxy_all_meds
WHERE upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECT.* SEX|UNPROTECT.* SX|AFTER SEX|POST SEX|RISK.* SEX|24H.* OF SEX|24 HOURS OF SEX|72H.* OF SEX|72 HOURS OF SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|SEX.* EXPOSURE|STD| STI |STIS|\(STI |DOXYPEP|DOXY PEP|DOXY PREP|DOXY-PEP|DOXY- PEP|DoxyPrEP|DOXY PREP|DOXY-PREP|DOXY- PREP|72h.* OF INTERCOURSE|72 HOURS OF INTERCOURSE');
-- IDENTIFY ALL MEDS MINUS ABSOLUTE INCLUSION AND EXCLUSION
DROP TABLE IF EXISTS advp.doxy_all_minus_exclu_and_incl;
CREATE TABLE advp.doxy_all_minus_exclu_and_incl AS
SELECT * FROM 
advp.doxy_all_meds
WHERE id not in (select id from advp.doxy_abs_inclusion_meds)
AND upper(directions) !~ ('TIC|LYME|PROCEDURE|DENT|BIRTH CONTROL|BIRTHCONTROL|RASH|BITE|LEPTOSPIROSIS');
-- RELATIVE INCLUSUION MEDS
DROP TABLE IF EXISTS advp.doxy_relative_incl;
CREATE TABLE advp.doxy_relative_incl AS
SELECT * FROM 
advp.doxy_all_minus_exclu_and_incl
WHERE upper(directions) ~ ('INTERCOURSE|SEX')
AND upper(directions) !~ ('DAILY|DAYS|EVERY 12|7 DAYS|ONCE A WEEK|ONCE PER WEEK|WEEKLY');
-- ABSOULTE AND RELATIVE INCLUSION MEDS TOGETHER
DROP TABLE IF EXISTS advp.doxy_inclu_all;
CREATE TABLE advp.doxy_inclu_all AS
SELECT * FROM advp.doxy_abs_inclusion_meds
UNION 
SELECT * FROM advp.doxy_relative_incl;
-- IDENTIFY ALL MEDS MINUS ABS INCLUSION AND EXCLUSION AND RELATIVE INCLUSION
DROP TABLE IF EXISTS advp.doxy_all_minus_exclu_and_incl_and_relinc;
CREATE TABLE advp.doxy_all_minus_exclu_and_incl_and_relinc AS
SELECT * FROM 
advp.doxy_all_minus_exclu_and_incl
WHERE id not in (select id from advp.doxy_relative_incl);
-- REMAINDER MEDS: MUST MEET ALL REQUIREMENTS
DROP TABLE IF EXISTS advp.doxy_remainder_meds;
CREATE TABLE advp.doxy_remainder_meds AS
SELECT * FROM
advp.doxy_all_minus_exclu_and_incl_and_relinc
WHERE (name ilike '%100 MG%' or name ilike '%100MG%')
AND upper(directions) ~ ('ONCE|ONE DOSE|SINGLE DOSE|1 DOSE')
AND (dose in ('200', '200mg') OR upper(directions) ~ ('TAKE 2|TAKE TWO|200mg|200 MG|2 TAB|2 PILL|2 PO|TWO TAB|TWO PILL|TWO PO'));
-- CHECK FOR LYME DX ON SAME DATE AS DOXY (FROM REMAINDER MEDS)
DROP TABLE IF EXISTS advp.doxy_with_lyme_dx;
CREATE TABLE advp.doxy_with_lyme_dx AS
SELECT DISTINCT T1.*
FROM advp.doxy_remainder_meds T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE (dx_code_id ilike 'icd10:A69.2%' or dx_code_id ilike 'icd10:W57%' or dx_code_id in ('icd9:088.81', 'icd9:919.4'))
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days';
AND T2.date = T1.date;
-- DROP RX ON SAME DATE AS LYME DX
DROP TABLE IF EXISTS advp.doxy_remainder_no_lyme_dx;
CREATE TABLE advp.doxy_remainder_no_lyme_dx AS
SELECT * FROM advp.doxy_remainder_meds
EXCEPT
SELECT * FROM advp.doxy_with_lyme_dx;
-- CHECK FOR LYME LAB ON SAME DATE AS DOXY (FROM REMAINDER WITH NO LYME DX)
DROP TABLE IF EXISTS advp.doxy_with_lyme_lx;
CREATE TABLE advp.doxy_with_lyme_lx AS
SELECT DISTINCT T1.*
FROM advp.doxy_remainder_no_lyme_dx T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days'
AND T2.date = T1.date;
-- DROP RX ON SAME DATE AS LYME LAB
-- REMAINDER MEDS FINAL LIST
DROP TABLE IF EXISTS advp.doxy_remainder_meds_final;
CREATE TABLE advp.doxy_remainder_meds_final AS
SELECT * FROM advp.doxy_remainder_no_lyme_dx
EXCEPT
SELECT * FROM advp.doxy_with_lyme_lx;
--BRING TOGETHER INCLUSION LISTS AND REMAINDER MEDS
DROP TABLE IF EXISTS advp.doxy_all_combo;
CREATE TABLE advp.doxy_all_combo as
SELECT * FROM advp.doxy_inclu_all
UNION
SELECT * FROM advp.doxy_remainder_meds_final
ORDER BY date;
-- COUNT ONLY ONE RX PER MONTH
DROP TABLE IF EXISTS advp.doxy_pep_rx;
CREATE TABLE advp.doxy_pep_rx as
SELECT patient_id, to_char(date, 'YYYY_MM') as year_month, 1 as doxy_pep_rx
FROM advp.doxy_all_combo
GROUP BY patient_id, year_month;


