--create new table 
drop table if exists et1;
select concat('Creating new clin_enc table at: ', now());
select patient_id, date, 'enc'::varchar(3) as source into temporary et1
 from emr_encounter e
 where (e.weight>0 or e.height>0
         or e.bp_systolic>0 or e.bp_diastolic>0
         or e.temperature>0
         or exists (select null from emr_encounter_dx_codes dx
                    where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9'))
 group by patient_id, date, 'enc'::varchar(3);

drop table if exists rt2;
select patient_id, date, 'rx'::varchar(3) as source into temporary rt2
 from emr_prescription
 group by  patient_id, date, 'rx'::varchar(3);

drop table if exists it3;
select patient_id, date, 'imu'::varchar(3) as source into temporary it3
 from emr_immunization
 group by  patient_id, date, 'imu'::varchar(3);

drop table if exists lt4;
select patient_id, date, 'lx'::varchar(3) as source into temporary lt4
 from emr_labresult
 group by  patient_id, date, 'lx'::varchar(3);

select patient_id, date, source
into gen_pop_tools.clin_enc_new
from (select patient_id, date, source from et1
      union all
      select patient_id, date, source from rt2
      union all
      select patient_id, date, source from it3
      union all
      select patient_id, date, source from lt4) t0;

-- drop existing table
select concat('Dropping old clin_enc table at: ', now());
drop table if exists gen_pop_tools.clin_enc;

-- rename table
select concat('Renaming clin_enc table at: ', now());
ALTER TABLE gen_pop_tools.clin_enc_new RENAME TO clin_enc;
alter table gen_pop_tools.clin_enc
add primary key (patient_id, date, source);
analyze gen_pop_tools.clin_enc;

