-- duplicate icd10 codes get created do to a bug in load_epic.py
-- This results in 2 icd10 codes where the second is identical to the first except that it has a comma
-- appended to the end.  This creates bogus rows in the public.static_dx_code and public.emr_encounter_dx_codes
--  It is slightly messy to fix because they codes are used as keys

-- This SQL deletes the bogus codes which have had a comma appended

DELETE FROM public.emr_encounter_dx_codes WHERE
DELETE FROM public.emr_encounter_dx_codes t1 WHERE EXISTS (
  SELECT null FROM 
(SELECT encounter_id, trim(dx_code_id, ',') || ',' dx_code_id FROM public.emr_encounter_dx_codes 
  GROUP BY encounter_id, trim(dx_code_id, ',') HAVING Count(*) > 1) t0 
  WHERE t1.encounter_id = t0.encounter_id and t1.dx_code_id = t0.dx_code_id)

INSERT INTO static_dx_code
SELECT tctc, tcode, 'icd10', 'updates', 'updates' FROM (
SELECT trim(combotypecode, ',') tctc, trim(code, ',') tcode
  from static_dx_code t1 WHERE combotypecode like '%,' AND NOT EXISTS 
  (SELECT null from static_dx_code t0 WHERE combotypecode NOT LIKE '%,' and t0.combotypecode = trim(t1.combotypecode, ','))
  GROUP BY trim(combotypecode, ','), trim(code, ',')
  HAVING Count(*)=1) AS t2
  
UPDATE public.emr_encounter_dx_codes SET dx_code_id = trim(dx_code_id, ',') WHERE dx_code_id LIKE '%,';

DELETE FROM 
 static_dx_code WHERE combotypecode LIKE '%,'
