#!/usr/bin/env python
# -*- coding: utf-8 -*-

# This program cleans known data issues for data in the following tables in the esp/riskscape database
# public.static_dx_code and public.emr_encounter_dx_codes

import sys
import argparse
import psycopg2
from psycopg2.extensions import adapt

conn = None
cursor = None

aasConstraint = [
    ["emr_encounter_dx_codes", "emr_encounter_dx_codes_encounter_code_unique", " UNIQUE (encounter_id, dx_code_id);"], 
    ["emr_encounter_dx_codes", "emr_encounter_dx_codes_combocode_fk", " FOREIGN KEY (dx_code_id) "
                + " REFERENCES public.static_dx_code (combotypecode) ON DELETE CASCADE;"], 
    ["conf_reportabledx_code", "conf_reportabledx_code_cond_code_unique", " UNIQUE (condition_id, dx_code_id);" ],
    ["conf_reportabledx_code", "conf_reportabledx_code_combocode_fk", " FOREIGN KEY (dx_code_id) " 
                + " REFERENCES public.static_dx_code (combotypecode) "
                + " ON DELETE CASCADE;" ],
    ["emr_hospital_problem", "emr_hospital_problem_combocode_fk",  " FOREIGN KEY (dx_code_id) " 
                + " REFERENCES public.static_dx_code (combotypecode) "
                + " ON DELETE CASCADE;" ],
    ["emr_problem", "emr_problem_combocode_fk",  " FOREIGN KEY (dx_code_id) " 
                + " REFERENCES public.static_dx_code (combotypecode) "
                + " ON DELETE CASCADE;" ],
    ["vaers_diagnosticseventrule_heuristic_defining_codes", "vaers_diag_heuristic_defining_codes_combocode_fk",
                " FOREIGN KEY (dx_code_id) REFERENCES public.static_dx_code (combotypecode) "
                + " ON DELETE CASCADE;" ],
    ["vaers_diagnosticseventrule_heuristic_discarding_codes", "vaers_diag_heuristic_discarding_codes_combocode_fk",
                " FOREIGN KEY (dx_code_id) REFERENCES public.static_dx_code (combotypecode) "
                + " ON DELETE CASCADE;" ] 
]

aasQryDeleteDuplicate = [
    [ ("DELETE FROM public.emr_encounter_dx_codes a "
        + "USING (SELECT MIN(ctid) as ctid, encounter_id, dx_code_id "
        + "FROM public.emr_encounter_dx_codes WHERE dx_code_id ='"),  
       ("' GROUP BY encounter_id, dx_code_id HAVING COUNT(*) > 1 ) b "
        + "WHERE a.encounter_id = b.encounter_id AND a.dx_code_id = b.dx_code_id AND a.ctid <> b.ctid; ") ],
    [ ("DELETE FROM public.conf_reportabledx_code a "
        + "USING (SELECT MIN(ctid) as ctid, condition_id, dx_code_id "
        + "FROM public.conf_reportabledx_code WHERE dx_code_id ='"),  
       ("' GROUP BY condition_id, dx_code_id HAVING COUNT(*) > 1 ) b "
        + "WHERE a.condition_id = b.condition_id AND a.dx_code_id = b.dx_code_id AND a.ctid <> b.ctid; ") ],
    [ ("DELETE FROM public.vaers_diagnosticseventrule_heuristic_defining_codes a "
        + "USING (SELECT MIN(ctid) as ctid, diagnosticseventrule_id, dx_code_id "
        + "FROM public.vaers_diagnosticseventrule_heuristic_defining_codes WHERE dx_code_id ='"),  
       ("' GROUP BY diagnosticseventrule_id, dx_code_id HAVING COUNT(*) > 1 ) b "
        + "WHERE a.diagnosticseventrule_id = b.diagnosticseventrule_id AND a.dx_code_id = b.dx_code_id AND a.ctid <> b.ctid; ") ],
    [ ("DELETE FROM public.vaers_diagnosticseventrule_heuristic_discarding_codes a "
        + "USING (SELECT MIN(ctid) as ctid, diagnosticseventrule_id, dx_code_id "
        + "FROM public.vaers_diagnosticseventrule_heuristic_discarding_codes WHERE dx_code_id ='"),  
       ("' GROUP BY diagnosticseventrule_id, dx_code_id HAVING COUNT(*) > 1 ) b "
        + "WHERE a.diagnosticseventrule_id = b.diagnosticseventrule_id AND a.dx_code_id = b.dx_code_id AND a.ctid <> b.ctid; ") ]
]

def UpdateReferences(sNewCode, sOldCode):

    try:
        for asConstraint in aasConstraint:
            sQry = "UPDATE public." + asConstraint[0] + " SET dx_code_id = '" + sNewCode + "' WHERE dx_code_id= 'icd10:" + sOldCode + "';"
            cursor.execute(sQry)
    
    except Exception as e:
        sys.stderr.write("***Error UpdateReferences() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(1)
        return
        
    conn.commit()
    
def DeleteDuplicates(dx_code_id):

    try:
        for asQry in aasQryDeleteDuplicate:
            sQry = asQry[0] + dx_code_id + asQry[1]
            cursor.execute(sQry)
    
    except Exception as e:
        sys.stderr.write("***Error DeleteDuplicates() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(2)
        return
        
    conn.commit()
   

def FixReferences(sComboCode, sGoodCode, sDescription):
    try:
        print "FixRef good: " + sGoodCode + " bad: " + sComboCode
                
        # fix the references in the tables that use the combocode as a foreign key
        UpdateReferences("icd10:" + sGoodCode, sComboCode)

        # we might have created a duplicate rows in the tables that use combocode as a foreign key
        DeleteDuplicates(sGoodCode)
        
    except Exception as e:
        sys.stderr.write("***" + "Error FixReferences() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(3)
        
def GetCodeDescription(sCode):
    try:
        sQry = "SELECT description FROM public.static_icd10_code WHERE code='" + sCode + "';"
        cursor.execute(sQry)
        conn.commit()
        row = cursor.fetchone()
        if row:
            return row[0]
    except Exception as e:
        sys.stderr.write("***" + "Error GetCodeDescription() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
    return ""

def EnsureCodeInStaticDx(sCode, sDescription):
    try:
        sQry = "SELECT code FROM public.static_dx_code WHERE code ='" + sCode + "';"
        cursor.execute(sQry)
        conn.commit()
        row = cursor.fetchone()
        if row == None:
            sQry = ("INSERT INTO public.static_dx_code(combotypecode, code, type, name, longname) "
                + "VALUES ('icd10:" + sCode + "','" + sCode + "','icd10', '', " + str(adapt(sDescription)) + ");")
            cursor.execute(sQry)

    except Exception as e:
        sys.stderr.write("***" + "Error EnsureCodeInstaticDx() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        con.rollback()
        return
    conn.commit()

def DeleteDuplicateStaticDxRows(sCode):
    try:
        sQry = ("DELETE FROM public.static_dx_code a "
            + "USING (SELECT MIN(ctid) as ctid, code "
            + "FROM public.static_dx_code WHERE code ='" + sCode + "' "
            + "GROUP BY code HAVING COUNT(*) > 1 ) b "
            + "WHERE a.code = b.code AND a.ctid <> b.ctid; ")
        cursor.execute(sQry)
    except Exception as e:
        sys.stderr.write("***" + "Error DeleteDuplicateStaticDx() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        con.rollback()
        return
    conn.commit()

def FixAppendedComma():
    try:
        sQry = ("SELECT combotypecode, code FROM public.static_dx_code WHERE type = 'icd10' AND "
            + " (combotypecode LIKE '%,' OR code LIKE '%,');")
        cursor.execute(sQry)
        conn.commit()
        aRow = cursor.fetchall()

        # For each suspect icd code
        for row in aRow:
        
            print " Comma row: " + str(row)
            
            sComboCode = row[0]
            sCodeFromCombo = row[0][6:]
            sGoodCodeFromCombo = sCodeFromCombo.rstrip(",")
            sCode = row[1]
            sGoodCode = sCode.rstrip(",")
            
            # The real code we are looking for might be either the code (minus the ,) or what is at the end of the combo-type code
            sDescription = ""
            
            # if the code and the code from the combo are not the same
            # we need to make sure we have entries for both in the icd10 table
            if(sGoodCode != sGoodCodeFromCombo):
                # find a description from the master table if the code is there
                sDescription = GetCodeDescription(sGoodCodeFromCombo)
                EnsureCodeInStaticDx(sGoodCodeFromCombo, sDescription)
                DeleteDuplicateStaticDxRows(sGoodCodeFromCombo)
                
            sDescription = GetCodeDescription(sGoodCode)
            EnsureCodeInStaticDx(sGoodCode, sDescription)
            DeleteDuplicateStaticDxRows(sGoodCode)
                
            # fix all the table references that use this combo code to point to the good code
            FixReferences(sComboCode, sGoodCode, sDescription)

            if(sCode != sGoodCode):
                #delete the bogus code entry with a comma appended
                sQry = "DELETE FROM public.static_dx_code WHERE code ='" + sCode + "';"
                cursor.execute(sQry)
                conn.commit()
        
    except Exception as e:
        sys.stderr.write("***" + "Error FixAppendedComma() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(4)
    
def FixComboCodeMismatch():
    try:
        sQry = "SELECT combotypecode, code FROM public.static_dx_code  WHERE type = 'icd10' AND ('icd10:' || code != combotypecode);"
        cursor.execute(sQry)
        conn.commit()
        aRow = cursor.fetchall()

        # For each suspect icd code
        for row in aRow:

            print "Combo row: " + str(row)
            
            sComboCode = row[0]
            sCodeFromCombo = row[0][6:]
            sCode = row[1]
            
            # make sure there is a row in the table for the code in the combotypecode
            sDescription = GetCodeDescription(sCodeFromCombo)
            EnsureCodeInStaticDx(sCodeFromCombo, sDescription)
            
            # fix the given row so the combotypecode and code match
            sDescription = GetCodeDescription(sCode)
            sQry = ("UPDATE public.static_dx_code SET combotypecode = 'icd10:" + sCode + "', name= '', longname=" 
                + str(adapt(sDescription)) + " WHERE code = '" + sCode + "';")
            cursor.execute(sQry)
            conn.commit()
                
            DeleteDuplicateStaticDxRows(sCode)
            DeleteDuplicateStaticDxRows(sCodeFromCombo)
            
            # fix the references in the tables that use the combocode as a foreign key
            FixReferences(sComboCode, sCode, sDescription)
        
    except Exception as e:
        sys.stderr.write("***" + "Error FixComboCodeMismatch() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(5)

def UpdateDescriptions():
    sQry = ("UPDATE public.static_dx_code sdc "
        + "SET name='', longname=sic.description "
        + "FROM public.static_icd10_code sic WHERE sdc.code = sic.code; ")
    try:
        cursor.execute(sQry)
    except Exception as e:
        sys.stderr.write("***Error UpdateDescriptions() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        return
        
    conn.commit()

def DeleteConstraints():

    try:
        for asConstraint in aasConstraint:
            sQry = "ALTER TABLE public." + asConstraint[0] + " DROP CONSTRAINT " + asConstraint[1]
            cursor.execute(sQry)
        sQry = "ALTER TABLE public.static_dx_code DROP CONSTRAINT static_dx_code_pkey;"
        cursor.execute(sQry)
    
    except Exception as e:
        sys.stderr.write("***Error DeleteConstraints() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(6)
        return
    conn.commit()
    

def RestoreConstraints():

    print "Restoring Constraints"
    try:
        sQry = "ALTER TABLE public.static_dx_code ADD PRIMARY KEY (combotypecode);"
        cursor.execute(sQry)

        for asConstraint in aasConstraint:
            sQry = "ALTER TABLE public." + asConstraint[0] + " ADD CONSTRAINT " + asConstraint[1] + asConstraint[2]
            cursor.execute(sQry)
    
    except Exception as e:
        sys.stderr.write("***Error RestoreConstraints() sql: " + sQry + "\n")
        sys.stderr.write("***" + str(e) + "\n")
        conn.rollback()
        sys.exit(7)
        return
    conn.commit()


parser = argparse.ArgumentParser(description="Clean known data issues in the ESP database public.static_dx_code and public.emr_encounter_dx_codes.\n" 
    + "Usage: CleanIcd10.py <database_url> <database_password>\n"
    + "This script relies on the Key Constraints for the following tables having the following names: "
    + "emr_encounter_dx_codes: emr_encounter_dx_codes_unique and emr_encounter_dx_codes_combocode_fk " 
    + "conf_reportabledx_code: conf_reportabledx_code_combocode_fk"
    + "emr_hospital_problem: emr_hospital_problem_combocode_fk "
    + "emr_problem: emr_problem_combocode_fk"
    + "vaers_diagnosticseventrule_heuristic_defining_codes: vaers_diagnosticseventrule_heuristic_defining_codes_combocode_fk"
    + "vaers_diagnosticseventrule_heuristic_discarding_codes: vaers_diagnosticseventrule_heuristic_discarding_codes_combocode_fk " )
parser.add_argument("url")
parser.add_argument("password")
aArg = parser.parse_args()

sUrl = aArg.url
sPassword = aArg.password


try:
    # connect to the database
    fRestoreConstraints = False
    sError = "Could not connect to the specified database."
    conn = psycopg2.connect(host=sUrl, dbname='esp', user='esp', password=sPassword)
    cursor = conn.cursor()
    
    #TODO jboyer
#    RestoreConstraints()
 
    DeleteConstraints()
    fRestoreConstraints = True  

    FixAppendedComma()
    FixComboCodeMismatch()
    UpdateDescriptions()
                
except Exception as e:
    sys.stderr.write("***" + sError + "\n")
    sys.stderr.write("***" + str(e) + "\n")
    sys.exit(1)

finally:
    if conn:
        if fRestoreConstraints:
            #TODO jboyer
            RestoreConstraints()
            conn.commit()

        conn.close()

sys.exit(0)

