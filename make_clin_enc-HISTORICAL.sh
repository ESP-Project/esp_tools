#!/bin/bash
#Modify the following script variables as appropriate.
#This is the name of the esp database
ESPDB=esp
#This is the name of the esp user
ESPUSR=esp
#This is the logfile stem:
LOGFILE=/home/esp/logs/make_clin_enc_historical.log
#This is the path to the sql directory where the sql scripts are kept
#  (and probably also this batch script, but not necessarily).
clin_enc_dir=/home/esp/esp_tools
DATE=$(date +%Y-%m-%d)

if [ $# = 1 ]; then
  pulldate=$(date -d $1 +%Y-%m-%d)
  startdate=$(date -d $pulldate +%s)
else
  pulldate=$(date -d '3 months ago' +%Y-%m-%d)
  startdate=$(date -d $pulldate +%s)
fi

#GET THE DATE ONE YEAR FROM START DATE
END_DATE=$(date +%Y-%m-%d -d "$pulldate + 365 day")
#END_DATE=$(date +%Y-%m-%d -d "$pulldate + 127 day")

exec 5>&1 6>&2 >>$LOGFILE.$DATE 2>&1

#CONVERT TO EPOCH FORMAT
enddate=$(date -d $END_DATE +%s)
echo "pulldate $pulldate"
echo "startdate $startdate"
echo "end_date $END_DATE"
echo "enddate $enddate"

  while [ "$startdate" -le "$enddate" ]; do
    eval psql -d $ESPDB -U $ESPUSR -v pulldate=${pulldate} -f ${clin_enc_dir}/make_clin_enc.pg.sql
        pulldate=$(date -I -d "$pulldate + 1 day")
        startdate=$(date -d $pulldate +%s)
  done
exec 1>&5 2>&6
