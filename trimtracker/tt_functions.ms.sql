IF object_id('gen_pop_tools.downfill2', 'P') IS NOT NULL DROP procedure gen_pop_tools.downfill2;
GO
create procedure gen_pop_tools.downfill2 
 (@intable varchar(100),
  @partby varchar(100),
  @orderby varchar(100),
  @col varchar(100),
  @extlim varchar(100) = '24' )
as
  declare @dynsql nvarchar(max);
  set @dynsql = '
  merge into ' + @intable +
  ' using
  (select ' + @partby + ' partid,  ' + @orderby + ' orderid,  [' + @col + '] sourcecol,
    lead([' + @col + ']) over (partition by ' + @partby + ' order by ' + @orderby + ') nextval,
    case
      when coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), STUFF(left(convert(varchar(max), getdate(),112),6), 5, 0, ''_''))
           <= STUFF(left(convert(varchar(max), dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ',''_'','''')+''01'', 112)),112),6), 5, 0, ''_'')   
           then coalesce(lead(' + @orderby + ') over (partition by ' + @partby + ' order by ' + @orderby + '), STUFF(left(convert(varchar(max), getdate(),112),6), 5, 0, ''_''))
      else STUFF(left(convert(varchar(max), dateadd(month, ' + @extlim + ', convert(datetime, replace(' + @orderby + ',''_'','''')+''01'', 112)),112),6), 5, 0, ''_'')
      end nextdate
  from ' + @intable + '
  where [' + @col + '] > 0) as sourcetab
  on ' + @partby + '=partid and ' + @orderby + ' > orderid and ' + @orderby + ' < nextdate
  when matched then update
  set [' + @col + ']=sourcecol;
  ';
  execute sp_executesql @dynsql;
GO

IF object_id('gen_pop_tools.fluseason2', 'P') IS NOT NULL DROP procedure gen_pop_tools.fluseason2;
GO
create procedure gen_pop_tools.fluseason2 
 (@intable varchar(100),
  @partby varchar(100),
  @orderby varchar(100),
  @col varchar(100))
as
  declare @dynsql nvarchar(max);
  set @dynsql = '
  merge into ' + @intable +
  ' using (select partid, min(orderid) orderid, max(sourcecol) sourcecol, nextaug from
  (select ' + @partby + ' partid,  ' + @orderby + ' orderid,  ' + @col + ' sourcecol,
    lead(' + @col + ') over (partition by ' + @partby + ' order by ' + @orderby + ') nextval,
    left(convert(varchar(max),dateadd(month, 4, convert(datetime, ' + @orderby + '+''01'', 112)),112),4)+''_08'' nextaug
  from ' + @intable + '
  where ' + @col + ' > 0) t0 group by partid, nextaug) as sourcetab
  on ' + @partby + '=partid and ' + @orderby + ' > orderid and ' + @orderby + ' <= nextaug
  when matched then update
  set ' + @col + '=sourcecol;
  ';
  execute sp_executesql @dynsql;
GO
