set search_path=gen_pop_tools,public;
create sequence if not exists gen_pop_tools.mends_qa_seq;
do $$
declare
  x integer := nextval('gen_pop_tools.mends_qa_seq');
begin
  execute 'create table cii_qa.tt_out_valid_' || x ||
           ' as select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''sex''::varchar(25) as field_name,  ab as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''race'' as field_name,  ac as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''age'' as field_name,  ah as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(ai) as counts, field_name, field_level, year ' ||
          ' from (select ai, ''enc1yr'' as field_name,  1 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(aj) as counts, field_name, field_level, year ' ||
          ' from (select aj, ''enc2yr'' as field_name,  2 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''bmi%'' as field_name, al as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''bmi'' as field_name, ak as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''pregnant'' as field_name, am as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''gestDM'' as field_name, an as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''sysbp'' as field_name, ao as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''sysbp%'' as field_name, ap as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''diabp'' as field_name, aq as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''diabp%'' as field_name, ar as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''ldl'' as field_name, "as" as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''trig'' as field_name, at as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''a1c'' as field_name, au as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''prediab'' as field_name, av as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''type1'' as field_name, aw as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''type2'' as field_name, ax as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''insulin'' as field_name, ay as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''metformin'' as field_name, az as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''infvax'' as field_name, ba as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''smoking'' as field_name, bb as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''asthma'' as field_name, bc as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''chlmydscreen'' as field_name, bd as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''depression'' as field_name, be as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''opioid'' as field_name, bf as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''benzo'' as field_name, bg as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''benzoopioid'' as field_name, bh as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''highopioid'' as field_name, bi as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''gonorrscreen'' as field_name, bj as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''hypertension'' as field_name, bk as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''ili'' as field_name, bl as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''hepc'' as field_name, bm as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''lyme'' as field_name, bn as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''syph'' as field_name, bo as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''TDAP'' as field_name, bp as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''TDAPpreg'' as field_name, bq as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''lymeCume'' as field_name, br as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(bs) as counts, field_name, field_level, year ' ||
          ' from (select bs, ''enctot'' as field_name,  3 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''iliCur'' as field_name, bt as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''lymeCur'' as field_name, bu as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''PertusCur'' as field_name, bv as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''PertPrior'' as field_name, bw as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''ASCVD'' as field_name, bx as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''Diaghypert'' as field_name, "by" as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''PrimPayer'' as field_name, bz as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select count(*) as counts, field_name, field_level, year ' ||
          ' from (select ''DiagDiab'' as field_name, ca as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(allpriorbp) as counts, field_name, field_level, year ' ||
          ' from (select allpriorbp, ''allpriorbp'' as field_name,  3 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(prior2bp) as counts, field_name, field_level, year ' ||
          ' from (select prior2bp, ''prior2bp'' as field_name,  3 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
  execute 'insert into cii_qa.tt_out_valid_' || x ||
          ' select sum(prior1bp) as counts, field_name, field_level, year ' ||
          ' from (select prior1bp, ''enctot'' as field_name,  3 as field_level, af as year ' ||
                'from gen_pop_tools.tt_out2) t0 group by field_name, field_level, year;';
end
$$ language plpgsql;
