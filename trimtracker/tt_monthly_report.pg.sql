/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Monthly Report
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;

create or replace function coalesce_r_sfunc(state anyelement, value anyelement)
    returns anyelement
    immutable parallel safe
as
$$
select coalesce(value, state);
$$ language sql;

create or replace aggregate find_last_ignore_nulls(anyelement) (
    sfunc = coalesce_r_sfunc,
    stype = anyelement
);


drop table if exists tt_out;
create table tt_out as
SELECT 
  tt_pat.patient_id as AA
, case
    when tt_pat.gender='M' then 1
	when tt_pat.gender='F' then 2
  end as AB
, case tt_pat.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as AC
, case
    when birth_year<1945 then 0
	when birth_year>=1945 and birth_year<= 1965 then 1
	when birth_year>1965 then 2
  end as AD
, case
    when substring(tt_pat.zip,6,1)='-' then substring(tt_pat.zip,1,5)
    else tt_pat.zip
  end as AE
, substr(tt_pat_seq_enc.year_month,1,4) as AF
, substr(tt_pat_seq_enc.year_month,6,2) as AG
,  case 
    when tt_pat_seq_enc.age<=4 then 1
    when tt_pat_seq_enc.age>=5 and tt_pat_seq_enc.age <=9 then 2
    when tt_pat_seq_enc.age>=10 and tt_pat_seq_enc.age <=14 then 3
    when tt_pat_seq_enc.age>=15 and tt_pat_seq_enc.age <=19 then 4
    when tt_pat_seq_enc.age>=20 and tt_pat_seq_enc.age <=24 then 5
    when tt_pat_seq_enc.age>=25 and tt_pat_seq_enc.age <=29 then 6
    when tt_pat_seq_enc.age>=30 and tt_pat_seq_enc.age <=39 then 7
    when tt_pat_seq_enc.age>=40 and tt_pat_seq_enc.age <=49 then 8
    when tt_pat_seq_enc.age>=50 and tt_pat_seq_enc.age <=59 then 9
    when tt_pat_seq_enc.age>=60 and tt_pat_seq_enc.age <=69 then 10
    when tt_pat_seq_enc.age>=70 and tt_pat_seq_enc.age <=79 then 11
    when tt_pat_seq_enc.age>=80 then 12
  end as AH 
, tt_pat_seq_enc.prior1 as AI
, tt_pat_seq_enc.prior2 as AJ
, case
    when find_last_ignore_nulls(tt_bmi.bmi) over 
            (partition by tt_pat.patient_id 
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) >0 
         then find_last_ignore_nulls(tt_bmi.bmi) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)
    when tt_pat.age>=12 then 0 -- if none of the above, and age 12 or over, then zero
  end as AK
, case 
    when find_last_ignore_nulls(tt_bmi_pct.bmipct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)<=5 then 1
    when find_last_ignore_nulls(tt_bmi_pct.bmipct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 5 and 50 then 2 
    when find_last_ignore_nulls(tt_bmi_pct.bmipct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 50 and 85 then 3
    when find_last_ignore_nulls(tt_bmi_pct.bmipct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 85 and 95 then 4
    when find_last_ignore_nulls(tt_bmi_pct.bmipct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) >95 then 5
    when tt_pat.age<=20 then 0 --if none of the above, and age 20 or less, then zero
  end as AL
, case
    when tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' then 0
  end as AM
, case
    when tt_gdm1.gdm=1 and tt_preg1.recent_pregnancy=1 
	  then 1
	when tt_pat.gender='F' then 0
  end as AN
, case
    when find_last_ignore_nulls(tt_bp1.max_bp_systolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)<=130 then 1
    when find_last_ignore_nulls(tt_bp1.max_bp_systolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 130 and 139 then 2
    when find_last_ignore_nulls(tt_bp1.max_bp_systolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) >139 then 3
    else 0
  end as AO
, case 
    when find_last_ignore_nulls(tt_bp1.syspct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) <=5 then 1
    when find_last_ignore_nulls(tt_bp1.syspct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 5 and 50 then 2
    when find_last_ignore_nulls(tt_bp1.syspct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 50 and 85 then 3
    when find_last_ignore_nulls(tt_bp1.syspct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 85 and 95 then 4
    when find_last_ignore_nulls(tt_bp1.syspct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) > 95 then 5
    else 0
  end as AP
, case
    when find_last_ignore_nulls(tt_bp1.max_bp_diastolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) <= 80 then 1
    when find_last_ignore_nulls(tt_bp1.max_bp_diastolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 80 and 89 then 2
    when find_last_ignore_nulls(tt_bp1.max_bp_diastolic) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) > 89 then 3
    else 0
  end as AQ
, case 
    when find_last_ignore_nulls(tt_bp1.diapct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)<=5 then 1
    when find_last_ignore_nulls(tt_bp1.diapct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 5 and 50 then 2
    when find_last_ignore_nulls(tt_bp1.diapct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 50 and 85 then 3
    when find_last_ignore_nulls(tt_bp1.diapct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 85 and 95 then 4
    when find_last_ignore_nulls(tt_bp1.diapct) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) > 95 then 5
    else 0
  end as AR
, case
    when find_last_ignore_nulls(tt_ldl1.max_ldl1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) <= 100 then 1
    when find_last_ignore_nulls(tt_ldl1.max_ldl1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 100 and 129 then 2
    when find_last_ignore_nulls(tt_ldl1.max_ldl1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 129 and 159 then 3
    when find_last_ignore_nulls(tt_ldl1.max_ldl1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 159 and 189 then 4
    when find_last_ignore_nulls(tt_ldl1.max_ldl1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)>189 then 5
    else 0
  end as AS
, case
    when find_last_ignore_nulls(tt_trig1.max_trig1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) <=199 then 1
    when find_last_ignore_nulls(tt_trig1.max_trig1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 199 and 499 then 2
    when find_last_ignore_nulls(tt_trig1.max_trig1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)>500 then 3
    else 0
  end as AT
, case
    when find_last_ignore_nulls(tt_a1c1.max_a1c1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)<=5.6 then 1
    when find_last_ignore_nulls(tt_a1c1.max_a1c1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 5.6 and 6.4 then 2
    when find_last_ignore_nulls(tt_a1c1.max_a1c1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row) between 6.4 and 8.0 then 3
    when find_last_ignore_nulls(tt_a1c1.max_a1c1) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)>8.0 then 4
    else 0
  end as AU
, case
    when find_last_ignore_nulls(tt_predm.predm) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)=1 then 1
    else 0
  end as AV
, case
    when find_last_ignore_nulls(tt_type1.type_1_diabetes) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)=1 then 1
    else 0
  end as AW
, case
    when find_last_ignore_nulls(tt_type2.type2) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)=1 then 1
    else 0
  end as AX
, case
    when tt_insulin.insulin=1 then 1
    else 0
  end as AY
, case
    when tt_metformin.metformin=1 then 1
    else 0
  end as AZ
, case
    when tt_flu_cur.influenza_vaccine=1 then 1
    when tt_pat_seq_enc.flu_month=1 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 1 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=2 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 2 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=3 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 3 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=4 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 4 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=5 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 5 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=6 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 6 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=7 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 7 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=8 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 8 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=9 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 9 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=10 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 10 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=11 and find_last_ignore_nulls(tt_flu_cur.influenza_vaccine) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 then 1
    else 0
  end as BA
, case 
    when find_last_ignore_nulls(tt_smoking.smoking::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)>0 
        then find_last_ignore_nulls(tt_smoking.smoking::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)
    else 0
  end as BB
, case 
    when tt_asthma.asthma = 1 then 1
    else 0
  end as BC
, case 
    when find_last_ignore_nulls(tt_chlamydia.recent_chlamydia::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row) >= 1 
        then find_last_ignore_nulls(tt_chlamydia.recent_chlamydia::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)
    else 0
  end as BD
, case 
    when find_last_ignore_nulls(tt_depression.depression::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 23 preceding and current row)=1 then 1
    else 0
  end as BE
, case 
    when tt_opi0.opioidrx::integer=1 then 1
    else 0
  end as BF
, case 
    when tt_opi1.benzorx::integer=1  then 1
    else 0
  end as BG
, case 
    when tt_opi2.opibenzorx::integer=1 then 1
    else 0
  end as BH
, case 
    when tt_opi3.highopi::integer=1 then 1
    else 0
  end as BI
, case 
    when find_last_ignore_nulls(tt_gonorrhea.recent_gonorrhea::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row) >= 1 
        then find_last_ignore_nulls(tt_gonorrhea.recent_gonorrhea::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)
    else 0
  end as BJ
, tt_hypertension.hypertension::integer
  as BK
, case 
    when tt_ili_cur.ili::integer=1 then 1
    else 0
  end as BL
, case 
    when find_last_ignore_nulls(tt_hepc.hepc::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between unbounded preceding and current row)=1 then 1
    else 0
  end as BM
, case 
    when tt_lyme.lyme::integer=1 then 1
    else 0
  end as BN
, case 
    when find_last_ignore_nulls(tt_syph.syph::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row) >= 1 
        then find_last_ignore_nulls(tt_syph.syph::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)
    else 0
  end as BO
, case 
    when tt_tdap.tdap_vaccine::integer=1 then 1
    else 0
  end as BP
, case 
    when find_last_ignore_nulls(tt_tdap.tdap_vaccine::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 and tt_preg1.recent_pregnancy=1 and tt_pat.gender='F' then 1
    when tt_pat.gender='F' and tt_preg1.recent_pregnancy=1 then 0
  end as BQ
--
-- lyme 1 yr lag
--
, case 
    when tt_lyme_lag.lyme::integer=1 then 1
    else 0
  end as BR
, tt_pat_seq_enc.allprior as BS
--
--ili
--
, case 
    when tt_ili_cur.ili::integer=1 then 1
    when tt_pat_seq_enc.flu_month=1 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 1 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=2 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 2 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=3 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 3 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=4 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 4 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=5 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 5 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=6 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 6 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=7 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 7 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=8 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 8 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=9 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 9 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=10 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 10 preceding and current row)=1 then 1
    when tt_pat_seq_enc.flu_month=11 and find_last_ignore_nulls(tt_ili_cur.ili::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 then 1
    else 0
  end as BT
--
-- lyme cur yr
--
, case 
    when tt_lyme_cur.lyme::integer=1 then 1
    else 0
  end as BU
--
-- pertussis monthly
--
, case 
    when tt_pertussis.pertussis::integer=1 then 1
    else 0
  end as BV
--
-- pertussis carry forward 1 yr (see below)
--
, case 
    when find_last_ignore_nulls(tt_pertussis.pertussis::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)=1 then 1
    else 0
  end as BW
--
-- CV Risk
--
, tt_cvrisk.cvrisk BX
--diagnosedhypertension
, tt_diaghypert.hypertension::integer
   as BY
--primary_payer
, case
    when find_last_ignore_nulls(tt_primary_payer.primary_payer::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)>=0 
        then find_last_ignore_nulls(tt_primary_payer.primary_payer::integer) over
            (partition by tt_pat.patient_id
             order by tt_pat_seq_enc.year_month
             rows between 11 preceding and current row)
    else 0::int
  end as BZ
--diagnosed diabetes
, case
   when tt_diagdiabetes.diagdiab::integer=0 then 0
      when tt_diagdiabetes.diagdiab::integer=1 then 1
      when tt_diagdiabetes.diagdiab::integer=2 then 2
      when tt_diagdiabetes.diagdiab::integer=3 then 3
      else 4
   end as CA

--census tract
, tt_census.census_tract_id as CB
--ethnicity
, case when tt_pat.ethnicity='HISPANIC' then 1 when tt_pat.ethnicity='NOT HISPANIC' then 2 end as CC
--
-- tables built from tt_weekly_tables.pg.sql
--
FROM gen_pop_tools.tt_pat
--
-- tt_pat_seq_enc  
--
JOIN tt_pat_seq_enc on tt_pat_seq_enc.patient_id=tt_pat.patient_id
--
-- most recent BMI Condition
--
LEFT JOIN gen_pop_tools.tt_bmi
	ON tt_bmi.patient_id = tt_pat_seq_enc.patient_id and tt_bmi.year_month=tt_pat_seq_enc.year_month
--
-- most recent BMI PCT
--
LEFT JOIN gen_pop_tools.tt_bmi_pct
	ON tt_bmi_pct.patient_id = tt_pat_seq_enc.patient_id and tt_bmi_pct.year_month=tt_pat_seq_enc.year_month
--
-- pregnancy
--
LEFT JOIN gen_pop_tools.tt_preg1
	ON tt_preg1.patient_id = tt_pat_seq_enc.patient_id and tt_preg1.year_month=tt_pat_seq_enc.year_month
--
-- Gestational diabetes
--
LEFT JOIN gen_pop_tools.tt_gdm1
	ON tt_gdm1.patient_id = tt_pat_seq_enc.patient_id and tt_gdm1.year_month=tt_pat_seq_enc.year_month
--
-- Blood pressure
--
LEFT JOIN gen_pop_tools.tt_bp1
	ON tt_bp1.patient_id = tt_pat_seq_enc.patient_id and tt_bp1.year_month=tt_pat_seq_enc.year_month
--
-- Max ldl lab result last year
--
LEFT JOIN gen_pop_tools.tt_ldl1
	ON tt_ldl1.patient_id = tt_pat_seq_enc.patient_id and tt_ldl1.year_month=tt_pat_seq_enc.year_month
--
-- Max trig lab result last year
--
LEFT JOIN gen_pop_tools.tt_trig1
	ON tt_trig1.patient_id = tt_pat_seq_enc.patient_id and tt_trig1.year_month=tt_pat_seq_enc.year_month
--
-- Max A1C lab result last year
--
LEFT JOIN gen_pop_tools.tt_a1c1
	ON tt_a1c1.patient_id = tt_pat_seq_enc.patient_id and tt_a1c1.year_month=tt_pat_seq_enc.year_month
--
-- Prediabetes
--
LEFT JOIN gen_pop_tools.tt_predm
	ON tt_predm.patient_id = tt_pat_seq_enc.patient_id and tt_predm.year_month=tt_pat_seq_enc.year_month
--
-- Type 1 Diabetes
--
LEFT JOIN gen_pop_tools.tt_type1
	ON tt_type1.patient_id = tt_pat_seq_enc.patient_id and tt_type1.year_month=tt_pat_seq_enc.year_month
--
-- Type 2 Diabetes
--
LEFT JOIN gen_pop_tools.tt_type2
	ON tt_type2.patient_id = tt_pat_seq_enc.patient_id and tt_type2.year_month=tt_pat_seq_enc.year_month
--
-- Insulin
--    Prescription for insulin within the previous year
--
LEFT JOIN gen_pop_tools.tt_insulin
	ON tt_insulin.patient_id = tt_pat_seq_enc.patient_id and tt_insulin.year_month=tt_pat_seq_enc.year_month
--
-- Metformin
--     Prescription for metformin within the previous year
--
LEFT JOIN gen_pop_tools.tt_metformin
	ON tt_metformin.patient_id = tt_pat_seq_enc.patient_id and tt_metformin.year_month=tt_pat_seq_enc.year_month
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
LEFT JOIN gen_pop_tools.tt_flu_cur
	ON tt_flu_cur.patient_id = tt_pat_seq_enc.patient_id and tt_flu_cur.year_month=tt_pat_seq_enc.year_month
--
-- Smoking
--
LEFT JOIN gen_pop_tools.tt_smoking 
        ON tt_smoking.patient_id = tt_pat_seq_enc.patient_id and tt_smoking.year_month=tt_pat_seq_enc.year_month
--
-- asthma
--
LEFT JOIN gen_pop_tools.tt_asthma
        on tt_asthma.patient_id = tt_pat_seq_enc.patient_id and tt_asthma.year_month=tt_pat_seq_enc.year_month
--
-- Most recent chlamydia test
--
LEFT JOIN gen_pop_tools.tt_chlamydia
	ON tt_chlamydia.patient_id = tt_pat_seq_enc.patient_id and tt_chlamydia.year_month=tt_pat_seq_enc.year_month
--
-- depression
--
LEFT JOIN gen_pop_tools.tt_depression
        on tt_depression.patient_id = tt_pat_seq_enc.patient_id and tt_depression.year_month=tt_pat_seq_enc.year_month
--
-- Opioid -- have to join this 4 times as each of the results are in separate rows.
--
LEFT JOIN gen_pop_tools.tt_opioidrx tt_opi0
        on tt_opi0.patient_id = tt_pat_seq_enc.patient_id and tt_opi0.year_month=tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_benzorx tt_opi1
        on tt_opi1.patient_id = tt_pat_seq_enc.patient_id and tt_opi1.year_month=tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_opibenzorx tt_opi2
        on tt_opi2.patient_id = tt_pat_seq_enc.patient_id and tt_opi2.year_month=tt_pat_seq_enc.year_month
LEFT JOIN gen_pop_tools.tt_highopi tt_opi3
        on tt_opi3.patient_id = tt_pat_seq_enc.patient_id and tt_opi3.year_month=tt_pat_seq_enc.year_month
--
-- Gonorrhea
--
LEFT JOIN gen_pop_tools.tt_gonorrhea
        on tt_gonorrhea.patient_id = tt_pat_seq_enc.patient_id and tt_gonorrhea.year_month=tt_pat_seq_enc.year_month
--
-- Hypertension
--
LEFT JOIN gen_pop_tools.tt_hypertension
        on tt_hypertension.patient_id = tt_pat_seq_enc.patient_id and tt_hypertension.year_month=tt_pat_seq_enc.year_month
--
-- current ili
--
LEFT JOIN gen_pop_tools.tt_ili_cur
        on tt_ili_cur.patient_id = tt_pat_seq_enc.patient_id and tt_ili_cur.year_month=tt_pat_seq_enc.year_month
--
-- Hep C
--
LEFT JOIN gen_pop_tools.tt_hepc
        on tt_hepc.patient_id = tt_pat_seq_enc.patient_id and tt_hepc.year_month=tt_pat_seq_enc.year_month
--
-- lyme
--
LEFT JOIN gen_pop_tools.tt_lyme
        on tt_lyme.patient_id = tt_pat_seq_enc.patient_id and tt_lyme.year_month=tt_pat_seq_enc.year_month
--
-- lyme 1 yr lag
--
LEFT JOIN 
    (select max(lyme) lyme, patient_id, substr(year_month,1,4)::text as year
     from gen_pop_tools.tt_lyme 
     group by patient_id, substr(year_month,1,4)) tt_lyme_lag
        on tt_lyme_lag.patient_id = tt_pat_seq_enc.patient_id 
          and tt_lyme_lag.year=(to_number(substr(tt_pat_seq_enc.year_month,1,4),'9999') - 1)::text
--
-- syphilis 
--
LEFT JOIN gen_pop_tools.tt_syph
        on tt_syph.patient_id = tt_pat_seq_enc.patient_id and tt_syph.year_month=tt_pat_seq_enc.year_month
--
-- tdap
--
LEFT JOIN gen_pop_tools.tt_tdap
        on tt_tdap.patient_id = tt_pat_seq_enc.patient_id and tt_tdap.year_month=tt_pat_seq_enc.year_month
--
-- more lyme.  Calendar year
--
LEFT JOIN 
    (select sum(lyme) over (partition by tt_pat_seq_enc.patient_id, 
                       substr(tt_pat_seq_enc.year_month,1,4) 
                       order by tt_pat_seq_enc.year_month)  lyme, 
	   tt_pat_seq_enc.patient_id, 
	   tt_pat_seq_enc.year_month
     from gen_pop_tools.tt_pat_seq_enc 
      LEFT JOIN gen_pop_tools.tt_lyme
        on tt_lyme.patient_id = tt_pat_seq_enc.patient_id 
          and tt_lyme.year_month=tt_pat_seq_enc.year_month) tt_lyme_cur
on tt_lyme_cur.patient_id = tt_pat_seq_enc.patient_id 
          and tt_lyme_cur.year_month=tt_pat_seq_enc.year_month
--
-- pertussis
--
LEFT JOIN gen_pop_tools.tt_pertussis
        on tt_pertussis.patient_id = tt_pat_seq_enc.patient_id and tt_pertussis.year_month=tt_pat_seq_enc.year_month
--
-- CV Risk
--
LEFT JOIN gen_pop_tools.tt_cvrisk
        on tt_cvrisk.patient_id = tt_pat_seq_enc.patient_id and tt_cvrisk.year_month=tt_pat_seq_enc.year_month
--
-- diagnosedhypertension
--
LEFT JOIN gen_pop_tools.tt_diaghypert
        on tt_diaghypert.patient_id = tt_pat_seq_enc.patient_id and tt_diaghypert.year_month=tt_pat_seq_enc.year_month
--
--
-- diagnoseddiabetes
--
LEFT JOIN gen_pop_tools.tt_diagdiabetes
        on tt_diagdiabetes.patient_id = tt_pat_seq_enc.patient_id and tt_diagdiabetes.year_month=tt_pat_seq_enc.year_month
----       
----
---- primary payer
--
LEFT JOIN gen_pop_tools.tt_primary_payer
        on tt_primary_payer.patient_id=tt_pat_seq_enc.patient_id and tt_primary_payer.year_month=tt_pat_seq_enc.year_month 
--
-- census tract
--
LEFT JOIN gen_pop_tools.patient_census_tract tt_census
       on tt_census.patient_id=tt_pat_seq_enc.patient_id 
       and tt_census.census_tract_id > 0
--
-- Ordering
--
ORDER BY tt_pat_seq_enc.patient_id, tt_pat_seq_enc.year_month
;
alter table gen_pop_tools.tt_out add primary key (AA, AF, AG);
analyze gen_pop_tools.tt_out;

delete from gen_pop_tools.tt_out
where af||ag=to_char(current_date,'yyyymm'); --this of course breaks any partial month results for current month.						  
update gen_pop_tools.tt_out
 set AV=0 where AX=1 and AV is not null;
update gen_pop_tools.tt_out
 set BO=0 where AM = 0 and BO=1;
update gen_pop_tools.tt_out
 set BQ=0 where AM = 0 and BQ=1;

--Uncomment the next two lines and update if site does not approve release of certain results.
--update gen_pop_tools.tt_out
--  set BD=0, BF=0, BG=0, BH=0, BI=0, BJ=0, BO=0, BQ=0, BR=0, BV=0, BW=0; 


