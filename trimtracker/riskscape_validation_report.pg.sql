-- first check patient_id by year, month
-- this should be an empty result set 
-- there should never be more than 1 row per patient (aa) per year (af) per month (ag)
select 'If patient, year, month is not unique, there will be counts here';
select count(*), aa, af, ag
from gen_pop_tools.tt_out2
group by aa, af, ag
having count(*)>1;

select "this looks at total counts of patient records per year and month";
select "If the min max difference is large, this must be investigated";
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount
from (
select count(*) as counts, af, ag
from gen_pop_tools.tt_out2
group by af, ag);

select 'here are counts for sex groups by year.  Same drill';
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount, ab
from (
select count(*), af, ag, ab
from gen_pop_tools.tt_out
group by af, ag, ab)
group by ab
order by ab;

select 'here are counts for race-ethnicity groups by year.';
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount, ac
from (
select count(*), af, ag, ac
from gen_pop_tools.tt_out
group by af, ag, ac)
group by ac
order by ac;

select 'here are counts for age groups by year.';
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount, ah
from (
select count(*), af, ag, ah
from gen_pop_tools.tt_out
group by af, ag, ah)
group by ah
order by ah;

select 'here are counts for hypertension diagnosed and control status groups by year.';
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount, by
from (
select count(*), af, ag, by
from gen_pop_tools.tt_out
group by af, ag, by)
group by by
order by by;

select 'here are counts for clinical hypertension groups by year.';
select avg(counts) as avgcount, min(counts) as mincount, max(counts) as maxcount, bk
from (
select count(*), af, ag, bk
from gen_pop_tools.tt_out
group by af, ag, bk)
group by bk
order by bk;