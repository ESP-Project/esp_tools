set search_path=gen_pop_tools,public;
drop table random_patid;
create table random_patid (id serial primary key, patient_id integer);
insert into random_patid (patient_id)
select patient_id from tt_pat
order by random();

drop table if exists tt_pat_bp_seq_enc;
with bp as (select sum(enc_counts) as enc_counts, patient_id, year_month from tt_bp_encs_amb group by patient_id, year_month)
select seq.patient_id, seq.year_month,
        sum(bp.enc_counts) over (partition by seq.patient_id order by seq.patient_id, seq.year_month range between unbounded preceding and current row) as allpriorbp,
        sum(bp.enc_counts) over (partition by seq.patient_id order by seq.patient_id, seq.year_month rows between 23 preceding and current row) as prior2bp,
        sum(bp.enc_counts) over (partition by seq.patient_id order by seq.patient_id, seq.year_month rows between 11 preceding and current row) as prior1bp
into tt_pat_bp_seq_enc
from tt_pat_seq seq 
left join bp on seq.patient_id=bp.patient_id and seq.year_month=bp.year_month;

drop table if exists gen_pop_tools.tt_out2;
select rp.id as aa_seq, ab, 
    case 
      when tto.cc=1 then 4
      when tto.ac=4 then 5
      when ac=5 then 6
      else ac
    end as ac, ad, ae, af, ag, ah, ai, aj, ak, al, am, an, ao, ap, aq, ar,
    "as", "at", au, av, aw, ax, ay, az, ba, bb, bc, bd, be, bf, bg, bh, bi, bj,
    case
    when bk=0 then 3
	when bk=1 then 1
	when bk=2 then 2
    when bk=3 then 4
    when bk=4 then 5
	else 0
    end bk, bl, bm, bn, bo, bp, bq, br, bs, bt, bu, bv, bw, bx, 
    case
	when "by"=4 then 0 
	else "by"
    end "by", bz, ca, cb,
case
    when pse.age<=4 then 1
    when pse.age>=5 and pse.age <=9 then 2
    when pse.age>=10 and pse.age <=14 then 3
    when pse.age>=15 and pse.age <=19 then 4
    when pse.age>=20 and pse.age <=24 then 5
    when pse.age>=25 and pse.age <=29 then 6
    when pse.age>=30 and pse.age <=34 then 7
    when pse.age>=35 and pse.age <=44 then 8
    when pse.age>=45 and pse.age <=54 then 9
    when pse.age>=55 and pse.age <=64 then 10
    when pse.age>=65 and pse.age <=74 then 11
    when pse.age>=75 and pse.age <=84 then 12
    when pse.age>=85 then 13
end as ah_alt,
bpseq.allpriorbp, bpseq.prior2bp, bpseq.prior1bp, tto.cc as ethnicity, 
  coalesce(cm.code,5) as race, pse.age, mh.mendshypertension as pph
into gen_pop_tools.tt_out2
from tt_out tto
join tt_pat_seq_enc pse on pse.patient_id=tto.aa and pse.year_month=tto.af || '_' || tto.ag
join tt_pat_bp_seq_enc bpseq on bpseq.patient_id=tto.aa and bpseq.year_month=tto.af || '_' || tto.ag
join emr_patient p on p.id=tto.aa
left join rs_conf_mapping cm on cm.src_field='race' and cm.src_value=p.race
left join tt_mendshypertension mh on tt0.aa=mh.patient_id and mh.year_month=tto.af || '_' || tto.ag
join random_patid rp on rp.patient_id=tto.aa ;
