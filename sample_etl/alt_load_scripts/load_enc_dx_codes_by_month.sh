#!/bin/bash
#set start date. This only loads one month of data
d="2021-01-01"
echo "$d"
mstart=$(date -d "$d" +%Y%m%d)
mend=$(date -d "$d + 1 month - 1 day" +%Y%m%d)
psql -h esp.server.name -d esp -U esp_user_name -f '/srv/esp/scripts/alt_load_scripts/load_enc_dx_codes.pg.sql' -v startdt=$mstart -v enddt=$mend
