#!/bin/bash
#run this after the quarterly extract is done
DATE=$(date +%Y-%m-%d)
LOGFILE=/srv/esp/logs/load_temp_tables.log.$DATE
exec 5>&1 6>&2 >>$LOGFILE.$DATE 2>&1

cd /srv/esp/data/epic/archive
rm epic*.esp.*
cd ../incoming/
for F in epicmem.esp.*; do 
   psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_pat from '$F' delimiter '^'" -v ON_ERROR_STOP=1
   if [ $? -ne 0 ];
      then
          echo -e "Error loading file ${F}"
          mv $F ../error/$F
          exit
      else
          echo -e "File copied: ${F}"
          mv $F ../archive/$F
   fi
done
cd vis
for F in epicvis.esp.*; do
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_enc from '$F' delimiter '^'" -v ON_ERROR_STOP=1
  if [ $? -ne 0 ];
    then
      echo -e "Error loading file ${F}"
      mv $F ../../error/$F
      exit
    else
      echo -e "File copied: ${F}"
      mv $F ../../archive/$F
  fi
done
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "select *, natural_key || (row_number() over (partition by natural_key))::bigint  as new_natural_key into new_temp_enc from temp_enc"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "alter table new_temp_enc drop column natural_key"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "alter table new_temp_enc rename column new_natural_key to natural_key"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "drop table temp_enc"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "alter table new_temp_enc rename to temp_enc"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create unique index temp_enc_natural_key_idx on temp_enc (natural_key)"
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create index temp_enc_date_idx on temp_enc (date)"
cd ../res
for F in epicres.esp.*; do
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_res from '$F' delimiter '^'" -v ON_ERROR_STOP=1
  if [ $? -ne 0 ];
    then
      echo -e "Error loading file ${F}"
      mv $F ../../error/$F
      exit
    else
      echo -e "File copied: ${F}"
      mv $F ../../archive/$F
  fi
done
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create index temp_res_date_idx on temp_res (date)" 
cd ../soc
for F in epicsoc.esp.*; do
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_soc from '$F' delimiter '^'" -v ON_ERROR_STOP=1
  if [ $? -ne 0 ];
    then
      echo -e "Error loading file ${F}"
      mv $F ../../error/$F
      exit
    else
      echo -e "File copied: ${F}"
      mv $F ../../archive/$F
  fi
done
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create index temp_soc_date_idx on temp_soc (date_noted)"
cd ../imm
for F in epicimm.esp.*; do
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_imm from '$F' delimiter '^'" -v ON_ERROR_STOP=1
  if [ $? -ne 0 ];
    then
      echo -e "Error loading file ${F}"
      mv $F ../../error/$F
      exit
    else
      echo -e "File copied: ${F}"
      mv $F ../../archive/$F
  fi
done
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create index temp_imm_date_idx on temp_imm (date)"
cd ../med
for F in epicmed.esp.*; do
  psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "\copy temp_med from '$F' delimiter '^'" -v ON_ERROR_STOP=1
  if [ $? -ne 0 ];
    then
      echo -e "Error loading file ${F}"
      mv $F ../../error/$F
      exit
    else
      echo -e "File copied: ${F}"
      mv $F ../../archive/$F
  fi
done
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -c "create index temp_med_date_idx on temp_med (date)"
exec 1>&5 2>&6

