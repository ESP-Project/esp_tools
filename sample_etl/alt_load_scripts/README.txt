These scripts provide an efficient method for truncating an ESP database and loading data from a number of standard sources.  
This has become necessary at a number of MENDS sites because these data partners do not have a 100% stable set of source data providers 
-- the set of patients available at any given cycle is more likely than not to represent a new sample, with a significant number of new patients, a significant number of patients lost, and a number of patients merged (deduplicated).
The complexities of dealing with these changes using an incremental update approach were too great to obtain any efficiency. 
However, the standard Python load_epic approach was not much more efficient in rebuilding the database.  
This set of scripts work quite quickly.

1 - copy files to new alt_load_scripts directory under scripts
  mkdir /srv/esp/scripts/alt_load_scripts
  cp /srv/esp/esp_tools/sample_etl/alt_load_scripts/* /srv/esp/scripts/alt_load_scripts/



1- setup temp tables and truncate old tables
Edit setup_alt_load.sh and run or run these three commands separately:
  psql -d esp -f /srv/esp/scripts/alt_load_scripts/move_esp_to_prior.pg.sql
  psql -d esp -f /srv/esp/scripts/alt_load_scripts/create_tmp_load_tables.pg.sql
  psql -d esp -f /srv/esp/scripts/alt_load_scripts/truncate_esp_tables.pg.sql


2. load new files
Edit and run load_raw_data.sh - set data/epic/incoming and archive diretories and user, etc as needed
  ./load_raw_data.sh


3 - load emr records  - load from temp tables into emr_tables
Edit and run load_to_emr.sh - set data/epic/incoming and archive diretories and user, etc as needed
./load_to_emr.sh - 

OR call individual load_xyz.sh

  /bin/bash /srv/esp/scripts/alt_load_scripts/load_encounter.sh   
  /bin/bash /srv/esp/scripts/alt_load_scripts/load_enc_dx_codes.sh  ??
  /bin/bash /srv/esp/scripts/alt_load_scripts/load_imm.sh
  /bin/bash /srv/esp/scripts/alt_load_scripts/load_labresult.sh
  /bin/bash /srv/esp/scripts/alt_load_scripts/load_prescription.sh
  /bin/bash /srv/esp/scripts/alt_load_scripts/load_sochist.sh
