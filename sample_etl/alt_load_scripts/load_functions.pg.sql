CREATE OR REPLACE FUNCTION gen_pop_tools.heightcm_fromftin(
	raw_height text
    )
    RETURNS decimal
    LANGUAGE 'plpgsql'
AS $BODY$
declare
  err_context text;
  height decimal;
begin
    height :=
	case 
	  when position(E'\'' in raw_height) > 0 and position('"' in raw_height) > 0
	    then (substr(raw_height,1,position(E'\'' in raw_height)-1)::decimal
		 + substr(split_part(raw_height,' ',2),1,
				  position('"' in split_part(raw_height,' ',2))-1)::decimal/12) * 30.48
	  when position(E'\'' in raw_height) > 0
		then (substr(raw_height,1,position(E'\'' in raw_height)-1)::decimal) * 30.48
	  when position('"' in raw_height) > 0
	    then (substr(split_part(raw_height,' ',2),1,
				  position('"' in split_part(raw_height,' ',2))-1)::decimal/12) * 30.48
	  else null
	end;
	if height > 0 and height <= 250 then 
	    return height;
    else 
	    return null;
	end if;
    exception when others then 
	    GET STACKED DIAGNOSTICS err_context = PG_EXCEPTION_CONTEXT;
        RAISE INFO 'Error Name:%',SQLERRM;
        RAISE INFO 'Error State:%', SQLSTATE;
        RAISE INFO 'Error Context:%', err_context;
		return null;
end 
$BODY$;
CREATE OR REPLACE FUNCTION gen_pop_tools.weightkm_fromlboz(
	raw_weight text
    )
    RETURNS decimal
    LANGUAGE 'plpgsql'
AS $BODY$
declare
  err_context text;
  weight decimal;
begin
    weight :=
      case when split_part(raw_weight,' ',2)='lb' and split_part(raw_weight,' ',4)='oz'
          then (split_part(raw_weight,' ',1)::decimal 
		  + split_part(raw_weight,' ',3)::decimal/16)
		  / 2.20462262185
	    when split_part(raw_weight,' ',2)='lb'
          then (split_part(raw_weight,' ',1)::decimal)
		  / 2.20462262185	 
        when split_part(raw_weight,' ',2)='oz'
          then (split_part(raw_weight,' ',1)::decimal / 16)
		  / 2.20462262185	
	    else null
	  end;
	if weight > 0 and weight <= 400 then 
	    return weight;
    else 
	    return null;
	end if;
    exception when others then 
	    GET STACKED DIAGNOSTICS err_context = PG_EXCEPTION_CONTEXT;
        RAISE INFO 'Error Name:%',SQLERRM;
        RAISE INFO 'Error State:%', SQLSTATE;
        RAISE INFO 'Error Context:%', err_context;
		return null;
end 
$BODY$;
CREATE OR REPLACE FUNCTION gen_pop_tools.bmi_fromhtwt(
	ht decimal, wt decimal
    )
    RETURNS decimal
    LANGUAGE 'plpgsql'
AS $BODY$
declare
  err_context text;
  bmi decimal;
begin
    bmi := wt / ((ht / 100)^2);
	if bmi > 0 and bmi <= 200 then 
	    return bmi;
    else 
	    return null;
	end if;
    exception when others then 
	    GET STACKED DIAGNOSTICS err_context = PG_EXCEPTION_CONTEXT;
        RAISE INFO 'Error Name:%',SQLERRM;
        RAISE INFO 'Error State:%', SQLSTATE;
        RAISE INFO 'Error Context:%', err_context;
		return null;
end 
$BODY$;
