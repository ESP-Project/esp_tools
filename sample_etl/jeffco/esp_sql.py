#!/usr/bin/python
"""
Name: esp_sql.py

Purpose:  Container for SQL commands.

Modifications:
    initial version, Bob Zambarano 3/9/2021
"""

import esp_gd

SELECT_ESP_Patient_TABLE="""
SELECT [natural_key]
      ,[mrn]
      ,[last_name]
      ,[first_name]
      ,[middle_name]
      ,[address1]
      ,[address2]
      ,[city]
      ,[state]
      ,[zip]
      ,[country]
      ,[areacode]
      ,[tel]
      ,[tel_ext]
      ,CONVERT (nvarchar(30), [date_of_birth] , 112  ) as BIRTH_DATE
      ,[gender]
      ,[race]
      ,[home_language]
      ,[ssn]
      ,[pcp_id]
      ,[marital_stat]
      --overloading religion with sexual orientation
      ,[sexual_orientation] as religion
      ,[aliases]
      ,[mother_mrn]
      ,[date_of_death]
      ,[center_id]
      ,[ethnicity]
      ,[mother_maiden_name]
      ,[last_update]
      ,[last_update_site]
      ,[suffix]
      ,[title]
      ,[remark]
      ,[income_level]
      ,[housing_status]
      ,substring([insurance_status],1,50)
      ,[birth_country]
      ,[vital_status]
      ,[next_appt_provider_id]
      -- using the next_appt_date field to store the privacy waiver date to associate with sex orientation
      ,CASE WHEN convert(nvarchar(8), [privacy_waiver_date], 112) = '19000101'
           THEN NULL 
           ELSE convert(nvarchar(8), [privacy_waiver_date], 112) END AS next_appt_date
      ,[next_appt_fac_provider_id]
  FROM [dbo].[cfar_member] p
  WHERE exists (select null
        FROM cfar_visit e
        WHERE e.date < %s AND e.date >= %s
        and p.natural_key=e.patient_id)
        or exists (select null
        from cfar_medications m
        WHERE m.date < %s AND m.date >= %s
        and p.natural_key=m.patient_id)
        or exists (select null
        from cfar_lab_results l
        WHERE l.date < %s AND l.date >= %s
        and p.natural_key=l.patient_id)
        or exists (select null
        from cfar_social_history h
        WHERE h.date < %s AND h.date >= %s
        and p.natural_key=h.patient_id)
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday)

ESP_Patient_DICT= {'SELECT':SELECT_ESP_Patient_TABLE}


SELECT_ESP_Provider_TABLE="""
SELECT [natural_key]
      ,[last_name]
      ,[first_name]
      ,[middle_name]
      ,[title]
      ,[dept_natural_key]
      ,[dept]
      ,[dept_address_1]
      ,[dept_address_2]
      ,[dept_city]
      ,[dept_state]
      ,[dept_zip]
      ,[area_code]
      ,[telephone]
      ,[center_id]
      ,[dept_country]
      ,[dept_county_code]
      ,[tel_country_code]
      ,[tel_ext]
      ,[call_info]
      ,[clin_address1]
      ,[clin_address2]
      ,[clin_city]
      ,[clin_state]
      ,[clin_zip]
      ,[clin_country]
      ,[clin_county_code]
      ,[clin_tel_country_code]
      ,[clin_areacode]
      ,[clin_tel]
      ,[clin_tel_ext]
      ,[clin_call_info]
      ,[suffix]
      ,[dept_addr_type]
      ,[clin_addr_type]
      ,[npi]
      ,[provider_type]
  FROM [dbo].[cfar_provider] p
  WHERE exists (select null
        FROM cfar_visit e
        WHERE e.date < %s AND e.date >= %s
        and p.natural_key=e.provider_id)
        or exists (select null
        from cfar_medications m
        WHERE m.date < %s AND m.date >= %s
        and p.natural_key=m.provider_id)
        or exists (select null
        from cfar_lab_results l
        WHERE l.date < %s AND l.date >= %s
        and p.natural_key=l.provider_id)
        or exists (select null
        from cfar_social_history h
        WHERE h.date < %s AND h.date >= %s
        and p.natural_key=h.provider_id)
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday)

ESP_Provider_DICT= {'SELECT':SELECT_ESP_Provider_TABLE}


SELECT_ESP_Visit_TABLE="""
SELECT distinct [patient_id]
      ,[mrn]
      ,concat(natural_key, convert(nvarchar(8), [date], 112)) as natural_key
      ,convert(nvarchar(8), [date], 112) date
      ,null as close_flag
      ,convert(nvarchar(8), [date_closed], 112) date_closed
      ,[provider_id]
      ,[site_natural_key]
      ,[site_name]
      ,[raw_encounter_type]
      ,case when [edd_pregnant]>= '2010-01-01' then convert(nvarchar(8), [edd_pregnant], 112) else [pregnant] end as preg 
      ,[raw_temperature]
      ,[cpt]
      ,[raw_weight]
      ,[raw_height]
      ,[raw_bp_systolic]
      ,[raw_bp_diastolic]
      ,[raw_o2_sat]
      ,null as peak_flow
      ,case when len(ltrim(emr_encounter_dx_codes))>1 then concat('%s',ltrim(replace(emr_encounter_dx_codes,'; ',';%s'))) 
            else emr_encounter_dx_codes end dx_codes
      ,[raw_bmi]
      ,convert(nvarchar(8), [hosp_admit_dt], 112) hosp_admit_dt
      ,convert(nvarchar(8), [hosp_dschrg_dt], 112) hosp_dschrg_dt
      ,[primary_payer]
  FROM [dbo].[cfar_visit]
WHERE date < %s AND date >= %s
"""  % (esp_gd.icd, esp_gd.icd, esp_gd.today, esp_gd.yesterday)

ESP_Visit_DICT= {'SELECT':SELECT_ESP_Visit_TABLE}

SELECT_ESP_Social_Hx_TABLE="""
SELECT distinct [patient_id]
      ,[mrn]
      ,CASE WHEN tobacco_use in ('Attempting to quit using chewing tobacco', 'chewing nicotine-containing substances tobacco', 
                                  'NICOTINE DEPENDENCE CHEWING TOBACCO WITH NICOTINE-INDUCED DISORDER', 'NICOTINE DEPENDENCE TOBACCO PRODUCT UNCOMPLICATED',
				  'NICOTINE DEPENDENCE TOBACCO PRODUCT WITH NICOTINE-INDUCED DISORDER', 'Odor Of Breath Tobacco Smoke') then 'Yes'
            WHEN tobacco_use in ('exposure to environmental tobacco smoke', 'EXPOSURE TO TOBACCO SMOKE IN PERINATAL PERIOD', 
                                  'NOXIOUS INFLUENCE VIA PLACENTA OR BREAST MILK - TOBACCO') then 'Passive'
            WHEN tobacco_use in ('NICOTINE DEPENDENCE CHEWING TOBACCO IN REMISSION', 'Previous history of using chewing tobacco') then 'Quit'
            WHEN tobacco_use in ('Teeth Discoloration Tobacco-Stained') then 'Other'
            WHEN tobacco_use in ('') then NULL
       ELSE tobacco_use END as tobacco_use
      ,CASE WHEN alcohol_use in ('Abstinence From Alcohol') then 'No'
            WHEN alcohol_use in ('alcohol','abdominal pain started/intensified by consumption of alcohol', 'ACUTE PANCREATITIS ALCOHOLIC','ALCOHOL ABUSE',
                                  'ALCOHOL ABUSE - CONTINUOUS', 'ALCOHOL ABUSE - EPISODIC', 'ALCOHOL ABUSE - PERSISTENT', 'ALCOHOL ABUSE WITH INTOXICATION - UNCOMPLICATED',
	         	          'ALCOHOL DISORDERS', 'alcohol use during pregnancy', 'alcohol use interfering with work', 'ALCOHOL-INDUCED ANXIETY DISORDER', 'COMBINED DRUG AND ALCOHOL ABUSE',
				  'ALCOHOL-INDUCED SLEEP DISORDER', 'ALCOHOLIC HEPATITIS', 'ALCOHOLIC HEPATITIS WITHOUT ASCITES', 'ALCOHOLIC LIVER DAMAGE', 'Counseling And Surveillance Of Alcoholic',
				  'CUSHING''S SYNDROME IDIOPATHIC ALCOHOL-INDUCED', 'drinking alcohol frequently before age 18', 'Drug/Alcohol Abuse', 'FATTY LIVER - ALCOHOLIC', 'GASTRITIS ALCOHOLIC',
				  'Guidance: Concerns About Alcohol Use', 'headache precipitated by drinking alcohol', 'heartburn related to certain foods alcohol', 
                                  'heartburn related to certain foods alcohol beer', 'inability to control amount of alcohol', 'Odor Of Breath Alcoholic', 'Patient Education - Alcohol',
                                  'recent decrease in alcohol consumption', 'recent increase in alcohol consumption') 
				   OR alcohol_use like 'ALCOHOL DEPENDENCE%%' 
				   OR alcohol_use like 'CIRRHOSIS ALCOHOLIC%%' then 'Yes'
	     WHEN alcohol_use in ('ALCOHOL ABUSE - IN REMISSION', 'ALCOHOL INTOXICATION - IN REMISSION', 'Alcohol or Drug Use', 
	                           'ALCOHOLIC CEREBELLAR DEGENERATION', 'Avoid drugs and alcohol', 'FATTY LIVER - NONALCOHOLIC', 'FETAL ALCOHOL SYNDROME', 'NONALCOHOLIC STEATOHEPATITIS',
				   'NOXIOUS INFLUENCE VIA PLACENTA OR BREAST MILK - ALCOHOL', 'POISONING ISOPROPYL ALCOHOL ACCIDENTAL', 'prenatal fetal exposure to alcohol', 
                                   'prenatal history of child alcohol use by mother', 'Self-help Group - Alcoholics Anonymous', '')
	                           OR alcohol_use like 'ALCOHOL WITHDRAWAL%%'
				   OR alcohol_use like 'reported family%%' then NULL
	     WHEN alcohol_use in ('never drank alcohol') then 'Never'
	     WHEN alcohol_use in ('stopped drinking alcohol') then 'No'
	     ELSE alcohol_use END as alcohol_use
      ,convert(nvarchar(8), [date], 112) date
      ,LookupKey as [natural_key]
      ,[provider_id]
      ,ltrim(replace(sex_partner_gender,'sexual orientation','')) as sex_partner_gender
      ,[alcohol_oz_per_week]
      ,CASE WHEN ill_drug_use in ('METHAMPHETAMINE ABUSE', 'using ''designer drugs''', 'using ''designer drugs'' - methamphetamines', 'using barbiturates',
                                  'using cocaine', 'using cocaine - alkaloid form', 'using cocaine with heroin', 'using heroin', 'using intravenous drugs', 
				  'using intravenous drugs and sharing needles', 'using PCP (''angel dust'')' ) then 'Yes'
            WHEN ill_drug_use in ('METHAMPHETAMINE ABUSE - IN REMISSION', 'METHAMPHETAMINE DEPENDENCE - IN REMISSION', 'drug use', 'using marijuana', 
	                          'using marijuana daily for a month or more', '') then NULL
            ELSE ill_drug_use END as ill_drug_use
      ,CASE WHEN sexually_active like 'sexually_active%%' then 'Yes'
            ELSE sexually_active END as sexually_active
      ,[birth_control_method]
  FROM [dbo].[cfar_social_history]
WHERE date < %s AND date >= %s
    and (len(ltrim(tobacco_use))>0 
        or len(ltrim(alcohol_use))>0
        or len(ltrim(sex_partner_gender))>0
        or len(ltrim(sexually_active))>0
        or len(ltrim(alcohol_oz_per_week))>0 
        or len(ltrim(ill_drug_use))>0
        or len(ltrim(birth_control_method))>0)
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Social_Hx_DICT= {'SELECT':SELECT_ESP_Social_Hx_TABLE}


SELECT_ESP_Test_Results_TABLE="""
SELECT distinct [patient_id]
      ,[mrn]
      ,[order_natural_key]
      ,convert(nvarchar(8), [date], 112) date
      ,CASE WHEN convert(nvarchar(8), [result_date], 112) = '19000101'
           THEN convert(nvarchar(8), [date], 112)
           ELSE convert(nvarchar(8), [result_date], 112) END AS res_date
      ,[provider_id]
      ,[order_type]
      ,[cpt_native_code]
      ,[component_native_code]
      ,REPLACE(native_name, '^', ' ') as native_name
      ,CASE WHEN result_string = ''
           THEN LEFT(REPLACE(REPLACE(comment, CHAR(13), ''), CHAR(10), ''), 2000)
           ELSE REPLACE(REPLACE(result_string, CHAR(13), ' '), CHAR(10), ' ') END AS result_string
      ,[abnormal_flag]
      ,[ref_low_string]
      ,[ref_high_string]
      ,[ref_unit]
      ,[status]
      ,LEFT(REPLACE(REPLACE(comment, CHAR(13), ''), CHAR(10), ''), 2000) as comment
      ,[specimen_num]
      ,[impression]
      ,[specimen_source]
      ,CASE WHEN convert(nvarchar(8), [collection_date], 112) = '19000101' and convert(nvarchar(8), [result_date], 112) != '19000101'
                 THEN convert(nvarchar(8), [result_date], 112)
       	    WHEN convert(nvarchar(8), [collection_date], 112) = '19000101' and convert(nvarchar(8), [result_date], 112) = '19000101'
     	         THEN convert(nvarchar(8), [date], 112)
            ELSE convert(nvarchar(8), [collection_date], 112) END AS collection_date
      ,[procedure_name]
      ,concat(order_natural_key, natural_key) as new_natural_key
      ,[patient_class]
      ,[patient_status]
      ,[CLIA_ID_id]
      ,CASE WHEN convert(nvarchar(8), [collection_date_end], 112) = '19000101'
           THEN NULL 
           ELSE convert(nvarchar(8), [collection_date_end], 112) END AS collection_end_dt
      ,CASE WHEN convert(nvarchar(8), [status_date], 112) = '19000101'
           THEN NULL 
           ELSE convert(nvarchar(8), [collection_date], 112) END AS status_date
  FROM [dbo].[cfar_lab_results]
WHERE date < %s AND date >= %s
--AND native_name like '%%^%%'
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Test_Results_DICT= {'SELECT':SELECT_ESP_Test_Results_TABLE}


SELECT_ESP_Meds_TABLE="""
SELECT distinct [patient_id]
      ,[mrn]
      ,[order_natural_key]
      ,[provider_id]
      ,convert(nvarchar(8), [date], 112) date
      ,[status]
      ,replace(replace(replace([directions],CHAR(10),' '),CHAR(13),' '),'^',' ') directions
      ,[code]
      ,[name]
      ,[quantity]
      ,[refills]
      ,convert(nvarchar(8), [start_date], 112) start_date
      ,convert(nvarchar(8), [end_date], 112) end_date
      ,[route]
      ,[dose]
      ,[patient_class]
      ,[patient_status]
      ,[managing_provider_id]
      ,[facility_provider_id]
  FROM [dbo].[cfar_medications]
WHERE date < %s AND date >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Meds_DICT= {'SELECT':SELECT_ESP_Meds_TABLE}


SELECT_ESP_Risk_TABLE="""
SELECT distinct [patient_id]
      ,[mrn]
      ,[natural_key]
      ,convert(nvarchar(8), [encounter_date], 112) enc_date
      ,[ever_sex_with_male]
      ,[past12mo_sex_with_male]
      ,[ever_sex_with_female]
      ,[past12mo_sex_with_female]
      ,[ever_sex_with_hiv]
      ,[past12mo_sex_with_hiv]
      ,[ever_sex_with_hepc]
      ,[past12mo_sex_with_hepc]
      ,[ever_sex_with_ivuser]
      ,[past12mo_sex_with_ivuser]
      ,[ever_exchange_sex]
      ,[past12mo_exchange_sex]
      ,[ever_sex_while_ui]
      ,[past12mo_sex_while_ui]
      ,[ever_injected_nonrx_drug]
      ,[past12mo_injected_nonrx_drug]
      ,[ever_nonrx_intranasal_drug_use]
      ,[past12mo_nonrx_intranasal_drug_use]
      ,[ever_nonrx_drug_use]
      ,[past12mo_nonrx_drug_use]
      ,[ever_share_drug_equip]
      ,[past12mo_share_drug_equip]
      ,[ever_incarcerated]
      ,[past12mo_incarcerated]
      ,[ever_homeless]
      ,[past12mo_homeless]
      ,[other_risk_factors]
  FROM [dbo].[cfar_risk_factors]
WHERE encounter_date < %s AND encounter_date >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Risk_DICT= {'SELECT':SELECT_ESP_Risk_TABLE}


if __name__ == '__main__':
    pass
