USE [CLARITY]

DECLARE	@filename varchar(100) = 'epicmed.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 


    /*----------------------------------------------------------------------
    Returns patient medication data required for the ESP medication extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #X_ESP_XTR_ORDER_MED;

    -- For improved performance
    -- Gather up all of the meds to process
    SELECT --TOP 100
      pa.PAT_MRN_ID AS MRN
     --,OM.SERV_AREA_ID
	 , om.*
    INTO        #X_ESP_XTR_ORDER_MED
    FROM [CLARITY].[dbo].ORDER_MED om
    INNER JOIN [CLARITY].[dbo].PAT_ENC pe ON pe.PAT_ENC_CSN_ID = om.PAT_ENC_CSN_ID
	INNER JOIN [CLARITY].[dbo].CLARITY_DEP DEP ON PE.EFFECTIVE_DEPT_ID = DEP.DEPARTMENT_ID
    INNER JOIN [CLARITY].[dbo].PATIENT pa ON om.PAT_ID = pa.PAT_ID                            
    WHERE DEP.SERV_AREA_ID =  115	--Lowell Community Health Center	[LCHC115]
         -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
		AND 
			( 
			  OM.ORDERING_DATE BETWEEN @StartDate AND @EndDate
			  OR OM.UPDATE_DATE BETWEEN @StartDate AND @EndDate
			 )
	    AND om.ORDER_CLASS_C  = 1 --normal orders only
		AND (om.ORDER_STATUS_C <> 4 or om.ORDER_STATUS_C is null) -- Exclude Cancelled
		AND (om.ORDER_STATUS_C <> 1 or om.ORDER_STATUS_C is null) -- Exclude Pending
        /*
		---- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
        AND (om.ORDER_CLASS_C <>3 or om.ORDER_CLASS_C is null) -- Exclude Historical
        AND (om.ORDER_CLASS_C <>10 or om.ORDER_CLASS_C is null)-- Exclude No Print
		AND (om.ORDER_CLASS_C <>6 or om.ORDER_CLASS_C is null) -- Exclude OTC
		AND (om.ORDER_CLASS_C <>7 or om.ORDER_CLASS_C is null) -- Exclude Sample
		*/
		-- Meds can be entered after order date so use the update date
		AND om.UPDATE_DATE < @endDate
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND om.UPDATE_DATE >= DATEADD ( DAY, -0, @startDate) 
        -- ONLY PROCESS UPDATES FOR MEDS ORDERED IN THE LAST 12 MONTHS
        AND (om.ORDERING_DATE >= DATEADD(month,-12,getdate()) or om.ORDER_INST >= DATEADD(month,-12,getdate()) );
		

    CREATE INDEX ix_OrdPatID ON #X_ESP_XTR_ORDER_MED ( PAT_ID );
    CREATE INDEX ix_ordCSN ON #X_ESP_XTR_ORDER_MED ( PAT_ENC_CSN_ID );
    CREATE UNIQUE INDEX ix_ordmedID ON #X_ESP_XTR_ORDER_MED ( ORDER_MED_ID );
    CREATE INDEX ix_mID ON #X_ESP_XTR_ORDER_MED ( MEDICATION_ID );

    DROP TABLE IF EXISTS #X_ESP_XTR_PAT_ENC_MED;

    -- For improved performance
    SELECT DISTINCT
        pe.PAT_ID
      , pe.PAT_ENC_CSN_ID
      , pe.DEPARTMENT_ID
      , pe.CONTACT_DATE
    INTO  #X_ESP_XTR_PAT_ENC_MED
    FROM [CLARITY].[dbo].PAT_ENC pe
    INNER JOIN #X_ESP_XTR_ORDER_MED om  ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;

    CREATE UNIQUE INDEX ix_encCSN ON #X_ESP_XTR_PAT_ENC_MED ( PAT_ENC_CSN_ID );

    DROP TABLE IF EXISTS #OUTPUT;

    -- Final results
    SELECT DISTINCT
        om.PAT_ID
      , om.MRN
      , CAST(om.ORDER_MED_ID as varchar)                              AS ORDER_ID
      , om.MED_PRESC_PROV_ID                         AS RX_PROV_ID
      , CAST(ISNULL (CONVERT( VARCHAR(8), om.ORDERING_DATE, 112 ), CONVERT( VARCHAR(8), om.ORDER_INST, 112 )) as varchar)  AS ORDER_DATE
      --,OM.ORDER_CLASS_C
	  --,om.IS_PENDING_ORD_YN
	  --,om.MODIFY_TRACK_C
	  --,om.ORDER_STATUS_C
	  , os.TITLE                           AS ORDER_STATUS
      , REPLACE( om.SIG, '^', ' ' )                  AS DIRECTIONS
      , REPLACE( ndc.NDC_CODE, '-', '' )             AS NDC_CODE
      , med.NAME                                     AS MEDICATION_NAME
      , om.QUANTITY
      , om.REFILLS
      , CAST(CONVERT( VARCHAR(8), om.START_DATE, 112 ) as varchar)    AS START_DATE
      , CAST(CONVERT( VARCHAR(8), om.END_DATE, 112 ) as varchar)      AS END_DATE
      , rt.NAME                                      AS ROUTE
      , om.HV_DISCRETE_DOSE + ' ' + mu.NAME          AS DOSE
      , CASE
            WHEN om.ORDER_CLASS_C is not null
            THEN CONVERT(varchar,om.ORDERING_MODE_C) + '.' + convert(varchar,om.ORDER_CLASS_C)
            ELSE convert(varchar,om.ORDERING_MODE_C)
        END                                          AS PATIENT_CLASS
      , CAST(NULL as varchar)                        AS PAT_STATUS_HOSP
      , COALESCE(om.AUTHRZING_PROV_ID,om.ORD_PROV_ID) AS MANAGING_PROV_ID
      -- Add a preceding "D" to designate departments/facilities
      -- Provider Extract must also include preceding "D"
      , concat('D', coalesce(om.PAT_LOC_ID, pe.DEPARTMENT_ID))    AS FACILITY_PROV_ID
    --INTO  #OUTPUT
    FROM #X_ESP_XTR_ORDER_MED om
    LEFT JOIN #X_ESP_XTR_PAT_ENC_MED         pe  ON om.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
    INNER JOIN [CLARITY].[dbo].ZC_ORDER_STATUS    os  ON os.ORDER_STATUS_C = om.ORDER_STATUS_C
    LEFT JOIN [CLARITY].[dbo].CLARITY_UCL        ucl ON om.ORDER_MED_ID = ucl.ORDER_ID
    LEFT JOIN [CLARITY].[dbo].CLARITY_MEDICATION med ON med.MEDICATION_ID = om.MEDICATION_ID
    LEFT JOIN [CLARITY].[dbo].ZC_ADMIN_ROUTE     rt  ON om.MED_ROUTE_C = rt.MED_ROUTE_C
    LEFT JOIN [CLARITY].[dbo].ZC_MED_UNIT        mu  ON om.DOSE_UNIT_C = mu.DISP_QTYUNIT_C
    LEFT JOIN [CLARITY].[dbo].RX_MED_NDC_CODE    ndc ON om.MEDICATION_ID = ndc.MEDICATION_ID     AND ndc.LINE = 1--;
