USE [CLARITY]


DECLARE	@filename varchar(100) = 'epicpro.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 

	
    /*--------------------------------------------------------
    Returns provider data required for the ESP provider extract
    ----------------------------------------------------------*/

    
    DROP TABLE IF EXISTS #OUTPUT;
        
    SELECT All_Providers_And_Depts.* 
      -- INTO #OUTPUT
    FROM (

    SELECT  DISTINCT 
       ser.PROV_ID                                             AS PROVIDER_ID
       , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                 THEN SUBSTRING(
                     ser.PROV_NAME
                     , 1
                     , IIF(CHARINDEX( ',', ser.PROV_NAME ) = 0, 1, CHARINDEX( ',', ser.PROV_NAME )) - 1 )
                 ELSE
                     SUBSTRING(
                     ser.PROV_NAME
                   , CHARINDEX( ' ', ser.PROV_NAME ) + 1
                   , LEN( ser.PROV_NAME ) - CHARINDEX( ' ', ser.PROV_NAME ) + 1 )
        END                                                       AS PROV_LAST_NAME
        
      , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                  AND CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) > 0
                 THEN -- strip off middle name/initial when existing
                     SUBSTRING(
                         ser.PROV_NAME
                         , CHARINDEX( ',', ser.PROV_NAME ) + 2
                         , CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                         - CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME )))
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                 THEN -- format is last_name, first_name
                     SUBSTRING(
                        ser.PROV_NAME
                        , CHARINDEX( ',', ser.PROV_NAME ) + 2
                        , LEN( ser.PROV_NAME ) - CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                 ELSE -- format is first_name <space> last_name
                     SUBSTRING( ser.PROV_NAME, 1, CHARINDEX( ' ', ser.PROV_NAME ))
        END                                                     AS PROV_FIRST_NAME
        

      , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                  AND CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) > 0
                 THEN SUBSTRING(
                     ser.PROV_NAME
                     , CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) + 1
                     , LEN( ser.PROV_NAME ) - CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                     + 1 )
                 ELSE ''
        END                                                     AS PROV_MIDDLE_NAME
      , ser.DOCTORS_DEGREE									    AS CLINICIAN_TITLE
      , CAST(COALESCE( emp.MR_LOGON_DEPT_ID, serdept.DEPARTMENT_ID ) as varchar) AS PRIM_DEPT_ID
      , COALESCE( dep.EXTERNAL_NAME, dep2.EXTERNAL_NAME )       AS DEPARTMENT_NAME
      , prim_addr.ADDR_LINE_1                                   AS PRIM_ADDR1
      , prim_addr.ADDR_LINE_2                                   AS PRIM_ADDR2
      , prim_addr.CITY                                          AS PRIM_CITY
      , st1.ABBR                                                AS PRIM_STATE
      , CAST(prim_addr.ZIP as varchar)                          AS PRIM_ZIP
      , CAST(LEFT(prim_addr.PHONE, 3) as varchar)               AS PRIM_PHONE_AC
      , CAST(RIGHT(prim_addr.PHONE, 8) as varchar)              AS PRIM_PHONE
      , CAST(NULL as varchar)                                   AS CENTER_ID
      ,ctry.TITLE						                        AS PRIM_COUNTRY
      , CAST(NULL as varchar) /*cty1.ABBR*/                     AS PRIM_COUNTY
      , CAST(NULL as varchar)                                   AS PRIM_CTRY_CODE
      , CAST(NULL as varchar)                                   AS PRIM_EXT
      , CAST(NULL as varchar)                                   AS PRIM_DEPT_COMMENT
      , CAST(NULL as varchar)                                   AS CLIN_ADDR1
      , CAST(NULL as varchar)                                   AS CLIN_ADDR2
      , CAST(NULL as varchar)                                   AS CLIN_CITY
      , CAST(NULL as varchar)                                   AS CLIN_STATE
      , CAST(NULL as varchar)                                   AS CLIN_ZIP
      , CAST(NULL as varchar)                                   AS CLIN_COUNTRY
      , CAST(NULL as varchar)                                   AS CLIN_COUNTY
      , CAST(NULL as varchar)                                   AS CLIN_CTRY_CODE
      , CAST(NULL as varchar)                                   AS CLIN_PHONE_AC
      , CAST(NULL as varchar)                                   AS CLIN_PHONE
      , CAST(NULL as varchar)                                   AS CLIN_EXT
      , CAST(NULL as varchar)                                   AS CLIN_DEPT_COMMENT
      , CAST(NULL as varchar)                                   AS CLIN_SUFFIX
      , CAST(NULL as varchar)                                   AS DEPT_ID
      , CAST(NULL as varchar)                                   AS ADDRESS_CODE
      , ser2.NPI                                                AS NPI
      , '1'                                                     AS PROVIDER_TYPE
    FROM  [CLARITY].[dbo].CLARITY_SER ser
    LEFT JOIN [CLARITY].[dbo].CLARITY_SER_ADDR prim_addr ON prim_addr.PROV_ID = ser.PROV_ID AND prim_addr.PRIMARY_ADDR_YN = 'Y'
    LEFT JOIN [CLARITY].[dbo].CLARITY_SER_2    ser2      ON ser2.PROV_ID = ser.PROV_ID
    LEFT JOIN [CLARITY].[dbo].CLARITY_EMP      emp       ON ser.USER_ID = emp.USER_ID
    LEFT JOIN [CLARITY].[dbo].CLARITY_DEP      dep       ON emp.MR_LOGON_DEPT_ID = dep.DEPARTMENT_ID
    LEFT JOIN [CLARITY].[dbo].ZC_STATE         st1       ON st1.STATE_C = prim_addr.STATE_C
    LEFT JOIN [CLARITY].[dbo].ZC_COUNTY        cty1      ON cty1.COUNTY_C = prim_addr.COUNTY_C
    LEFT JOIN [CLARITY].[dbo].ZC_COUNTRY       ctry      ON ctry.COUNTRY_C = prim_addr.COUNTRY_C
    LEFT JOIN [CLARITY].[dbo].CLARITY_SER_DEPT serdept   ON ser.PROV_ID = serdept.PROV_ID AND serdept.LINE = 1
    LEFT JOIN [CLARITY].[dbo].CLARITY_DEP      dep2      ON serdept.DEPARTMENT_ID = dep2.DEPARTMENT_ID AND serdept.LINE = 1
    
	WHERE dep2.SERV_AREA_ID = 115 --Lowell Community Health Center [LCHC115]
        AND ser.PROV_ID is not NULL
      --and ser.PROV_TYPE <> 'Resource' --exclude resources
	   -- PROVIDERS SHOULD BE UPDATED WITHIN THE LAST YEAR
        -- FOR HISTORICAL RUNS THIS MAY NEED TO BE EXTENDED        
        --AND (SER2.INSTANT_OF_UPDATE_DTTM > DATEADD(yyyy,-1,getdate()) or SER2.INSTANT_OF_UPDATE_DTTM is NULL)
        
    -- Bring in the departments/facilities    
    -- Departments/Facilities are considered to be a "place provider" vs a "people provider"
    UNION
    SELECT DISTINCT
       -- Add a preceding "D" to designate departments/facilities
       -- Eliminates conflict between provider_id and dept_id
      cast(concat('D', dep.DEPARTMENT_ID) AS varchar) AS PROVIDER_ID
      , CAST(NULL as varchar)      AS PROV_LAST_NAME
      , CAST(NULL as varchar)      AS PROV_FIRST_NAME
      , CAST(NULL as varchar)      AS PROV_MIDDLE_NAME
      , CAST(NULL as varchar)      AS CLINICIAN_TITLE
      , CAST(CONCAT('D',dep.DEPARTMENT_ID) as varchar)          AS PRIM_DEPT_ID
      , coalesce(dep.EXTERNAL_NAME, dep.DEPARTMENT_NAME)          AS DEPARTMENT_NAME
      , addr1.ADDRESS              AS PRIM_ADDR1
      , addr2.ADDRESS              AS PRIM_ADDR2
      , dep2.ADDRESS_CITY          AS PRIM_CITY
      , st.ABBR                    AS PRIM_STATE
      , dep2.ADDRESS_ZIP_CODE      AS PRIM_ZIP
      , LEFT(dep.PHONE_NUMBER, 3)  AS PRIM_PHONE_AC
      , RIGHT(dep.PHONE_NUMBER, 8) AS PRIM_PHONE
      , CAST(NULL as varchar)      AS CENTER_ID
      , ctry.TITLE AS PRIM_COUNTRY
      , CAST(NULL as varchar)/*cty.ABBR*/                   AS PRIM_COUNTY
      , CAST(NULL as varchar)      AS PRIM_CTRY_CODE
      , CAST(NULL as varchar)      AS PRIM_EXT
      , CAST(NULL as varchar)      AS PRIM_DEPT_COMMENT
      , CAST(NULL as varchar)      AS CLIN_ADDR1
      , CAST(NULL as varchar)      AS CLIN_ADDR2
      , CAST(NULL as varchar)      AS CLIN_CITY
      , CAST(NULL as varchar)      AS CLIN_STATE
      , CAST(NULL as varchar)      AS CLIN_ZIP
      , CAST(NULL as varchar)      AS CLIN_COUNTRY
      , CAST(NULL as varchar)      AS CLIN_COUNTY
      , CAST(NULL as varchar)      AS CLIN_CTRY_CODE
      , CAST(NULL as varchar)      AS CLIN_PHONE_AC
      , CAST(NULL as varchar)      AS CLIN_PHONE
      , CAST(NULL as varchar)      AS CLIN_EXT
      , CAST(NULL as varchar)      AS CLIN_DEPT_COMMENT
      , CAST(NULL as varchar)      AS CLIN_SUFFIX
      , CAST(NULL as varchar)      AS DEPT_ID
      , CAST(NULL as varchar)      AS ADDRESS_CODE
      , CAST(NULL as varchar)      AS NPI
      , '2'                        AS PROVIDER_TYPE
    FROM [CLARITY].[dbo].CLARITY_DEP dep
        LEFT JOIN [CLARITY].[dbo].CLARITY_DEP_2    dep2  ON dep.DEPARTMENT_ID = dep2.DEPARTMENT_ID
        LEFT JOIN [CLARITY].[dbo].CLARITY_DEP_ADDR addr1 ON dep.DEPARTMENT_ID = addr1.DEPARTMENT_ID             AND addr1.LINE = 1
        LEFT JOIN [CLARITY].[dbo].CLARITY_DEP_ADDR addr2 ON dep.DEPARTMENT_ID = addr1.DEPARTMENT_ID             AND addr1.LINE = 2
        LEFT JOIN [CLARITY].[dbo].ZC_STATE         st    ON dep2.ADDRESS_STATE_C = st.STATE_C
        LEFT JOIN [CLARITY].[dbo].ZC_COUNTRY       ctry  ON dep2.ADDRESS_COUNTRY_C = ctry.COUNTRY_C
        LEFT JOIN [CLARITY].[dbo].ZC_COUNTY        cty   ON dep2.ADDRESS_COUNTY_C = cty.COUNTY_C
    WHERE 
        dep.SERV_AREA_ID = 115 --Lowell Community Health Center [LCHC115]
		) All_Providers_And_Depts
         
