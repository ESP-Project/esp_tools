USE [CLARITY]

DECLARE	@filename varchar(100) = 'epicres.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 



 /*----------------------------------------------------------------------
    Returns lab results data required for the ESP results extract
    ------------------------------------------------------------------------*/

    --DROP TABLE IF EXISTS #OUTPUT;

    -- Final results
    SELECT DISTINCT
        op.PAT_ID
      , pa.PAT_MRN_Id
      , CAST(res.ORDER_PROC_ID as varchar) AS LAB_ORDER_ID
      , CAST(FORMAT(op.ORDERING_DATE,'yyyyMMdd') as varchar)  AS ORDERING_DATE
      , CAST(FORMAT(op.RESULT_TIME,'yyyyMMdd') as varchar)    AS RESULT_DATE
      , op.AUTHRZING_PROV_ID                               AS ORDERING_PROVIDER_ID
      , ot.NAME                                            AS ORDER_TYPE
      , eap.PROC_CODE                                      AS CPT
      , CAST(res.COMPONENT_ID as varchar)                  AS COMPONENT_ID
      , cc.NAME                                            AS TEST_NAME
      , res.ORD_VALUE                                      AS RESULT
      , LEFT(rf.TITLE, 20)                                 AS RESULT_FLAG
      , res.REFERENCE_LOW
      , res.REFERENCE_HIGH
      , REPLACE( res.REFERENCE_UNIT, '^', ' ' )            AS REFERENCE_UNIT
      , rs.TITLE                                           AS RESULT_STATUS
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS COMPONENT_COMMENT
      , CAST(NULL as varchar)							   AS SPECIMEN_ID
      , REPLACE( res.COMPONENT_COMMENT, '^', ' ' )         AS IMPRESSION_COMMENT
      , ss.TITLE                                           AS SPECIMEN_SRC
      , CAST(FORMAT(op2.SPECIMN_TAKEN_DATE,'yyyyMMdd') as varchar)  AS COLLECTION_DATE
      , eap.PROC_NAME                                      AS PROCEDURE_NAME
      , CAST(NULL as varchar)							   AS LAB_RESULT_ID
      , CAST(NULL as varchar)							   AS PATIENT_CLASS
      , CAST(NULL as varchar)							   AS PATIENT_STATUS
      -- REPLACE THE FOLLOWING AND REMOVE NULL LINE WITH CLIA COLUMN
      , poi.PERFORMING_ORG_CLIA_NUM                        AS CLIA_NUM
      --, CAST(NULL as varchar)                              AS CLIA_NUM
      , CONVERT( VARCHAR(8), op2.SPECIMEN_RECV_DATE, 112 ) AS COLLECTION_END_DATE
      , CAST(NULL as varchar)                              AS INTERPRETER
      , CAST(NULL as varchar)                              AS STATUS_DATE
      , CAST(NULL as varchar)                              AS INTERPRETER_ID
      , CAST(NULL as varchar)                              AS INTERPRETER_ID_TYPE
      , CAST(NULL as varchar)                              AS INTERPRETER_UID
      , CAST(NULL as varchar)                              AS OBS_METHOD
      , CAST(NULL as varchar)                              AS REF_TEXT
      -- Add a preceding "D" to designate departments/facilities
      -- Provider Extract must also include preceding "D"
      , concat('D', coalesce(pe.DEPARTMENT_ID, pe.EFFECTIVE_DEPT_ID))    AS FACILITY_PROVIDER_ID
   --,'--'
   --,cc.*
   --INTO  #OUTPUT
    FROM  [CLARITY].[dbo].ORDER_RESULTS           res
    INNER JOIN [CLARITY].[dbo].Order_Proc         op  ON res.ORDER_PROC_ID = op.ORDER_PROC_ID
    INNER JOIN [CLARITY].[dbo].PATIENT            pa  ON op.PAT_ID = pa.PAT_ID
    LEFT JOIN [CLARITY].[dbo].ORDER_PROC_2        op2 ON op2.ORDER_PROC_ID = op.ORDER_PROC_ID
    LEFT JOIN [CLARITY].[dbo].ZC_ORDER_TYPE       ot  ON ot.ORDER_TYPE_C = op.ORDER_TYPE_C
    LEFT JOIN [CLARITY].[dbo].CLARITY_EAP         eap ON eap.PROC_ID = op.PROC_ID
    LEFT JOIN [CLARITY].[dbo].ZC_RESULT_FLAG      rf  ON rf.RESULT_FLAG_C = res.RESULT_FLAG_C
    LEFT JOIN [CLARITY].[dbo].ZC_RESULT_STATUS    rs  ON rs.RESULT_STATUS_C = res.RESULT_STATUS_C
    LEFT JOIN [CLARITY].[dbo].ZC_SPECIMEN_SOURCE  ss  ON ss.SPECIMEN_SOURCE_C = op.SPECIMEN_SOURCE_C
    -- REPLACE THE FOLLOWING WITH A JOIN TO THE TABLE THAT STORES THE LAB CLIA  
    LEFT JOIN [CLARITY].[dbo].PERFORMING_ORG_INFO AS poi ON op.ORDER_PROC_ID = poi.ORDER_ID
   
   LEFT JOIN [CLARITY].[dbo].CLARITY_COMPONENT   cc ON cc.COMPONENT_ID = res.COMPONENT_ID
    LEFT JOIN [CLARITY].[dbo].PAT_ENC             pe ON res.PAT_ENC_CSN_ID=pe.PAT_ENC_CSN_ID
    LEFT JOIN [CLARITY].[dbo].CLARITY_DEP                     DEP ON PE.EFFECTIVE_DEPT_ID = DEP.DEPARTMENT_ID      
     WHERE DEP.SERV_AREA_ID =  115	--Lowell Community Health Center	[LCHC115]
        -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
        AND (res.RESULT_STATUS_C IN ( 3, 4 ) or res.RESULT_STATUS_C IS NULL) -- INCLUDE Final and Corrected
        AND (op.ORDER_STATUS_C NOT IN ( 1, 4 ) or op.ORDER_STATUS_C IS NULL) -- EXCLUDE Pending and Cancelled orders

		
        AND op.RESULT_TIME < @endDate
        
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND (  op.RESULT_TIME >=  DATEADD ( DAY, -0, @startDate)
               OR
              (OP2.LAST_RESULT_UPD_TM >= DATEADD ( DAY, -0, @startDate) 
                   and OP2.LAST_RESULT_UPD_TM < @endDate)
            )
        /*-- FOR TESTING
		and eap.PROC_CODE in
		(--'LAB4098'
		 'LAB40991'
		,'LAB40980'
		,'LAB60310'
		)
		*/
		order by [LAB_ORDER_ID]
		-- IF RESULTS SHOULD BE RESTRICTED TO PARTICULAR ORDER TYPES UNCOMMENT
        -- AND CUSTOMIZE AS NEEDED
        -- AND op.order_type_c in (
        -- 1,         -- Procedures
        -- 5,         -- Radiology
        -- 6,         -- Immunization/Injection
        -- 7,         -- Lab
        -- 10,        -- NURSING
        -- 14,        -- Blood Bank
        -- 16,        -- Microbiology
        -- 21,        -- Respiratory Care
        -- 26,        -- Point of Care Testing
        -- 41,        -- PR Charge
        -- 60,        -- Nursing Transfusion
        -- 67,        -- Point of Care External Testing
        -- 1005,      -- CPVL - PULMONARY PROCEDURES
        -- 1007,      -- CPVL - VASCULAR PROCEDURES
        -- 1014       -- Cytology
        -- )    