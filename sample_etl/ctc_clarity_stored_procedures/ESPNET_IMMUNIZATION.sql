-- Create extract of all immunizations records that have been entered within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.
-- For example, startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 

USE [CLARITY]

-- ADJUST LOOKBACK HERE
DECLARE	@filename varchar(100) = 'epicimm.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 
		
		
    /*----------------------------------------------------------------------
    Returns immunization data required for the ESP extract
    ------------------------------------------------------------------------*/

    --DROP TABLE IF EXISTS #OUTPUT;

    SELECT DISTINCT
        pa.PAT_ID                                        AS PATIENT_ID
	  , imm.IMMUNZATN_ID                                 AS IMM_TYPE
	  , imm_dict.NAME                                    AS IMM_NAME
	  , imm.IMMUNE_DATE                                  AS IMM_DATE
	  , imm.DOSE                                         AS IMM_DOSE
	  , zc_mfg.NAME                                      AS MFG_NAME
	  , imm.LOT                                          AS IMM_LOT
	  , imm.IMMUNE_ID                                    AS IMM_ID --natural_key
      , pa.PAT_MRN_ID                                    AS MRN
	  , prov.PROV_ID                                     AS PROVIDER_ID
	  , NULL                                             AS VISIT_DATE
	  , zc_imm_status.TITLE                              AS IMM_STATUS
    --INTO  #OUTPUT
    FROM
        [CLARITY].[dbo].IMMUNE                               imm 
		INNER JOIN [CLARITY].[dbo].PATIENT                   pa            ON pa.Pat_ID = imm.Pat_ID
		INNER JOIN [CLARITY].[dbo].CLARITY_IMMUNZATN         imm_dict      ON imm.IMMUNZATN_ID = imm_dict.IMMUNZATN_ID
		LEFT JOIN  [CLARITY].[dbo].ZC_MFG                    zc_mfg        ON imm.MFG_C = zc_mfg.MFG_C
		LEFT JOIN  (SELECT USER_ID, PROV_ID from [CLARITY].[dbo].CLARITY_EMP WHERE PROV_ID IS NOT NULL ) prov ON imm.ENTRY_USER_ID = prov.USER_ID
		LEFT JOIN  [CLARITY].[dbo].ZC_IMMNZTN_STATUS         zc_imm_status ON imm.IMMNZTN_STATUS_C = zc_imm_status.IMMNZTN_STATUS_C
		LEFT JOIN  [CLARITY].[dbo].PAT_ENC                   pe            ON imm.IMM_CSN = pe.PAT_ENC_CSN_ID
		LEFT JOIN  [CLARITY].[dbo].CLARITY_DEP               dep           ON pe.EFFECTIVE_DEPT_ID = dep.DEPARTMENT_ID
		   
    WHERE
	    imm.ENTRY_DATE <= @endDate
        AND imm.ENTRY_DATE >=  DATEADD ( DAY, -0, @startDate)
		AND DEP.SERV_AREA_ID =  XXX	--ENTER HEALTH CENTER ID HERE	
		
		

    
