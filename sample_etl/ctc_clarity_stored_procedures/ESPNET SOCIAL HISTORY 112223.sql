USE [CLARITY]

DECLARE	@filename varchar(100) = 'epicsoc.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 


	
    /*----------------------------------------------------------------------
    Returns social history data required for the ESP social history extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #OUTPUT;

    SELECT DISTINCT
        pa.PAT_ID                                        AS PATIENT_ID
      , pa.Pat_MRN_ID                                    AS MRN
      , tob.TITLE                                        AS TOBACCO_USE
      , alc.TITLE                                        AS ALCOHOL_USE
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 )      AS DATE_ENTERED
      , CAST(coalesce(soc.HX_LNK_ENC_CSN, pe.PAT_ENC_CSN_ID) as varchar)  AS SOCIAL_HX_ID
      , CAST(pe.VISIT_PROV_ID as varchar)                                 AS PROVIDER_ID
      , CASE
            WHEN (soc.FEMALE_PARTNER_YN = 'Y' and soc.MALE_PARTNER_YN = 'Y') THEN 'MALE + FEMALE'
            WHEN soc.FEMALE_PARTNER_YN = 'Y' THEN 'FEMALE'
            WHEN soc.MALE_PARTNER_YN = 'Y' THEN 'MALE'
        END                                           AS SEX_PARTNER_GENDER
      , CAST(soc.ALCOHOL_OZ_PER_WK as varchar)        AS ALC_OZ_PER_WEEK
      , ill_drug.TITLE                                   AS ILL_DRUG_USE
      , sex_active.TITLE                                 AS SEXUALLY_ACTIVE
      , LTRIM(concat(
             (CASE WHEN soc.condom_yn = 'Y' then ' CONDOM +' ELSE NULL END),
             (CASE WHEN soc.pill_yn = 'Y' then ' PILL +' ELSE NULL END),
             (CASE WHEN soc.DIAPHRAGM_YN = 'Y' then ' DIAPHRAGM +' ELSE NULL END),
             (CASE WHEN soc.IUD_YN = 'Y' then ' IUD +' ELSE NULL END),
             (CASE WHEN soc.SURGICAL_YN = 'Y' then ' SURGICAL +' ELSE NULL END),
             (CASE WHEN soc.SPERMICIDE_YN = 'Y' then ' SPERMICIDE +' ELSE NULL END),
             (CASE WHEN soc.IMPLANT_YN = 'Y' then ' IMPLANT +' ELSE NULL END),
             (CASE WHEN soc.RHYTHM_YN = 'Y' then ' RHYTHM +' ELSE NULL END),
             (CASE WHEN soc.INJECTION_YN = 'Y' then ' INJECTION +' ELSE NULL END),
             (CASE WHEN soc.SPONGE_YN = 'Y' then ' SPONGE_YN +' ELSE NULL END),
             (CASE WHEN soc.INSERTS_YN = 'Y' then ' INSERTS +' ELSE NULL END),
             (CASE WHEN soc.ABSTINENCE_YN = 'Y' then ' ABSTINENCE +' ELSE NULL END))) AS BIRTH_CONTROL_METHOD
    --INTO #OUTPUT
    FROM
        [CLARITY].[dbo].PATIENT pa
		LEFT JOIN [CLARITY].[dbo].Social_Hx          soc        ON soc.PAT_ID = pa.PAT_ID
        INNER JOIN [CLARITY].[dbo].PAT_ENC           pe         ON pe.PAT_ENC_CSN_ID = soc.PAT_ENC_CSN_ID
		LEFT JOIN [CLARITY].[dbo].CLARITY_DEP                    dep        ON PE.EFFECTIVE_DEPT_ID = DEP.DEPARTMENT_ID
        LEFT JOIN [CLARITY].[dbo].ZC_TOBACCO_USER    tob        ON tob.TOBACCO_USER_C = soc.TOBACCO_USER_C
        LEFT JOIN [CLARITY].[dbo].ZC_ALCOHOL_USE     alc        ON alc.ALCOHOL_USE_C = soc.ALCOHOL_USE_C
        LEFT JOIN [CLARITY].[dbo].ZC_ILL_DRUG_USER   ill_drug   ON soc.ILL_DRUG_USER_C = ill_drug.ILL_DRUG_USER_C
        LEFT JOIN [CLARITY].[dbo].ZC_SEXUALLY_ACTIVE sex_active ON soc.SEXUALLY_ACTIVE_C = sex_active.SEXUALLY_ACTIVE_C
    WHERE DEP.SERV_AREA_ID =  115 --Lowell Community Health Center [LCHC115]
         AND PE.EFFECTIVE_DATE_DTTM BETWEEN @startDate AND @endDate
		 -- don't include rows that don't have one of these values
         AND
		 ( COALESCE(tob.TITLE, alc.title, ill_drug.title, sex_active.title) is not null
            OR
           COALESCE(soc.MALE_PARTNER_YN, soc.FEMALE_PARTNER_YN) <> ('N')
          )
        ORDER BY [PATIENT_ID]
		/*AND pe.CONTACT_DATE < @endDate
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND ( pe.CONTACT_DATE >=  DATEADD ( DAY, -0, @startDate)
              OR
            pe.UPDATE_DATE >=  DATEADD ( DAY, -0, @startDate) )
         -- only process updates for visits that happened in the week prior to the start date
         -- this prevents overloading system with updates but ensures recently updated records are captured
        AND pe.CONTACT_DATE >= DATEADD(DAY, -7, @startDate)
       */
	--WHERE PE.CONTACT_DATE BETWEEN @StartDate and @EndDate

