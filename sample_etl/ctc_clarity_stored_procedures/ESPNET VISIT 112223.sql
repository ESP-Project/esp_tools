USE [CLARITY]
GO

DECLARE	@filename varchar(100) = 'epicvis.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 


    /*------------------------------------------------------------------
    Returns patient encounter data required for the ESP visit extract
    --------------------------------------------------------------------*/

    DECLARE
        @csn NUMERIC(18, 0)
      , @code VARCHAR(25)
      , @last_csn NUMERIC(18, 0)
      , @code_str VARCHAR(1000)
      , @Insert BIT;


    DROP TABLE IF EXISTS #X_ESP_XTR_Pat_Enc_VIS;

    SELECT
        pa.Pat_MRN_ID AS MRN
		, pe.*
    INTO
        #X_ESP_XTR_Pat_Enc_VIS
    FROM
        [CLARITY].[dbo].PAT_ENC pe
        INNER JOIN [CLARITY].[dbo].PATIENT pa  ON pe.Pat_ID = pa.Pat_ID
		LEFT JOIN [CLARITY].[dbo].CLARITY_DEP          DEP ON PE.EFFECTIVE_DEPT_ID = DEP.DEPARTMENT_ID
    WHERE DEP.SERV_AREA_ID =  115 --Lowell Community Health Center [LCHC115]
		AND PE.EFFECTIVE_DATE_DTTM BETWEEN @StartDate and @EndDate
		AND PE.ENC_TYPE_C <> '40' --EXCLUDE WAIT LIST
		/*
		and pe.CONTACT_DATE < @endDate 
        
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND ( pe.CONTACT_DATE >=  DATEADD ( DAY, -0, @startDate)
                 OR
              pe.UPDATE_DATE >=  DATEADD ( DAY, -0, @startDate) )
              
        -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE  
         
        -- only process updates for visits that happened in the week prior to the start date
        -- this prevents overloading system with updates but ensures recently updated records are captured
        AND pe.CONTACT_DATE >= DATEADD(DAY, -7, @startDate);
		*/
              

    CREATE INDEX ix_EncPatID ON #X_ESP_XTR_Pat_Enc_VIS ( PAT_ID );
    CREATE UNIQUE INDEX ix_peCSN ON #X_ESP_XTR_Pat_Enc_VIS ( PAT_ENC_CSN_ID );

    DROP TABLE IF EXISTS #X_ESP_XTR_Patient_VIS;

    -- get distinct patients
    SELECT DISTINCT
        pa.PAT_ID
      , pa.SEX_C
      , pa.EDD_DT
    INTO
        #X_ESP_XTR_Patient_VIS
    FROM
        [CLARITY].[dbo].PATIENT AS pa
        INNER JOIN #X_ESP_XTR_Pat_Enc_VIS AS pe
           ON pe.PAT_ID = pa.PAT_ID;

    CREATE UNIQUE INDEX ix_PatID ON #X_ESP_XTR_Patient_VIS ( PAT_ID );


    DROP TABLE IF EXISTS #X_ESP_XTR_ICD;

    DROP TABLE IF EXISTS #X_ESP_XTR_ICD_STR;

    -- get all DX codes by encounter

    CREATE TABLE #X_ESP_XTR_ICD_STR
    (
        PAT_ENC_CSN_ID NUMERIC(18, 0)
      , ICD_STR VARCHAR(1000)
    );

    SELECT DISTINCT
        pe.PAT_ENC_CSN_ID
      , icd.CODE
    INTO
        #X_ESP_XTR_ICD
    FROM
        #X_ESP_XTR_Pat_Enc_VIS                   AS pe
        INNER JOIN [CLARITY].[dbo].PAT_ENC_DX        AS dx
           ON dx.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
        INNER JOIN [CLARITY].[dbo].EDG_CURRENT_ICD10 AS icd
           ON icd.DX_ID = dx.DX_ID
		 WHERE PE.ENC_TYPE_C <> 40	-- Exclude Wait List
		 ;

    DECLARE CURSOR_icd CURSOR FOR
        SELECT
            PAT_ENC_CSN_ID
          , CODE
        FROM
            #X_ESP_XTR_ICD
        ORDER BY
            PAT_ENC_CSN_ID;

    OPEN CURSOR_icd;
    FETCH NEXT FROM CURSOR_icd
    INTO
        @csn
      , @code;

    SET @code_str = 'icd10:' + @code;
    SET @last_csn = @csn;
    SET @Insert = 0;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        FETCH NEXT FROM CURSOR_icd
        INTO
            @csn
          , @code;

        IF @csn = @last_csn
            SET @code_str = @code_str + ';icd10:' + @code;
        ELSE
            SET @Insert = 1;

        IF @Insert = 1
        BEGIN
            INSERT INTO
                #X_ESP_XTR_ICD_STR
            VALUES
            (
                @last_csn, @code_str
            );

            SET @code_str = 'icd10:' + @code;
        END;

        SET @last_csn = @csn;
        SET @Insert = 0;
    END;

    CREATE UNIQUE INDEX ix_CSN ON #X_ESP_XTR_ICD_STR ( PAT_ENC_CSN_ID );

    CLOSE CURSOR_icd;
    DEALLOCATE CURSOR_icd;

    --DROP TABLE IF EXISTS #OUTPUT;

    -- Final results
    SELECT DISTINCT
        pe.PAT_ID
      , CAST(pe.MRN as varchar) as MRN 
      , CAST(pe.PAT_ENC_CSN_ID as varchar) as PAT_ENC_CSN_ID
      , FORMAT(pe.CONTACT_DATE,'yyyyMMdd')                              AS CONTACT_DATE
      , pe.ENC_CLOSED_YN                                                         AS ENC_CLOSED
      , FORMAT(pe.ENC_CLOSE_DATE,'yyyyMMdd')                            AS ENC_CLOSE_DATE
      , pe.VISIT_PROV_ID
      , CAST(CONCAT('D',pe.EFFECTIVE_DEPT_ID) as varchar)		as DEPARTMENT_ID
      , coalesce(dep.EXTERNAL_NAME, dep.DEPARTMENT_NAME)		AS DEPARTMENT_NAME
      , enc_type.NAME                                                            AS ENC_TYPE
      , FORMAT(pa.EDD_DT,'yyyyMMdd')   as EDD_DT                                                    
      , cast(pe.TEMPERATURE as varchar) as TEMPERATURE
      , eap.PROC_CODE                                                            AS CPT
      , CONVERT( VARCHAR(10), CONVERT( INT, ROUND( pe.WEIGHT, 0 )) / 16 ) + ' lb '
        + CONVERT( VARCHAR(10), CONVERT( INT, ROUND( WEIGHT, 0 )) % 16 ) + ' oz' AS WEIGHT
      , pe.HEIGHT
      , cast(pe.BP_SYSTOLIC	 as varchar) as BP_SYSTOLIC
      , cast(pe.BP_DIASTOLIC as varchar) as BP_DIASTOLIC
      , cast(pe2.PHYS_SPO2 as varchar)                                           AS SPO2
      , CAST(pe2.PHYS_PEAK_FLOW as varchar)                                      AS PEAK_FLOW
      , icd.ICD_STR                                                              AS DX_CODES
      , CAST(pe.BMI as varchar) as BMI
      , CAST(pe.HOSP_ADMSN_TIME as varchar)                                      AS HOSP_ADM_DATETIME
      , CAST(pe.HOSP_DISCHRG_TIME as varchar)                                    AS HOSP_DISCH_DATETIME
      , cast(NULL as varchar)                                                    AS PRIMARY_PAYER
    --INTO        #OUTPUT
    FROM #X_ESP_XTR_Pat_Enc_VIS pe
    INNER JOIN #X_ESP_XTR_Patient_VIS      pa       ON pa.PAT_ID = pe.PAT_ID
    LEFT JOIN [CLARITY].[dbo].CLARITY_DEP      dep      ON dep.DEPARTMENT_ID = pe.EFFECTIVE_DEPT_ID
    LEFT JOIN [CLARITY].[dbo].ZC_DISP_ENC_TYPE enc_type ON pe.ENC_TYPE_C = enc_type.DISP_ENC_TYPE_C
    LEFT JOIN [CLARITY].[dbo].CLARITY_EAP      eap      ON pe.LOS_PRIME_PROC_ID = eap.PROC_ID
    LEFT JOIN [CLARITY].[dbo].PAT_ENC_2        pe2      ON pe2.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID
    LEFT JOIN #X_ESP_XTR_ICD_STR           icd      ON icd.PAT_ENC_CSN_ID = pe.PAT_ENC_CSN_ID;
	
