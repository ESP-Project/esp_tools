USE [CLARITY]

DECLARE	@filename varchar(100) = 'epicmem.esp.' + FORMAT(GETDATE(),'MMddyyyy') + '.txt'
		,@StartDate as datetime = CAST(CONVERT( varchar(10), DATEADD(dd, -10, getdate() ), 120 ) + ' 00:00' AS DATETIME)
		,@EndDate as datetime   =  CAST(CONVERT( varchar(10), DATEADD(dd, -1, getdate() ), 120 ) + ' 23:59' AS DATETIME) 


	IF Object_id ('tempdb..#OUTPUT')  IS NOT NULL	 
	DROP table #OUTPUT
	
	/*--------------------------------------------------------
    Returns patient data required for the ESP member extract
    ---------------------------------------------------------*/
    DECLARE
        @Pat_ID VARCHAR(100)
      , @Line SMALLINT
      , @MaxLine SMALLINT
      , @Insert BIT;

    SET NOCOUNT ON;


    DROP TABLE IF EXISTS #X_ESP_XTR_Pat_Enc_MEM;

    -- all patient encounter/contact for past x days
    SELECT        PAT_ID
    INTO        #X_ESP_XTR_Pat_Enc_MEM
    FROM          [CLARITY].[dbo].PATIENT
    WHERE CUR_PRIM_LOC_ID =  115 --Lowell Community Health Center [LCHC115]
	 AND UPDATE_DATE BETWEEN @startDate AND @endDate
	 --AND UPDATE_DATE >= DATEADD ( DAY, -0, @startDate)  AND UPDATE_DATE < @endDate
     
     UNION
     
    SELECT           PAT_ID 
    FROM      [CLARITY].[dbo].PAT_ENC
	LEFT JOIN [CLARITY].[dbo].CLARITY_DEP DEP ON PAT_ENC.EFFECTIVE_DEPT_ID = DEP.DEPARTMENT_ID
    WHERE DEP.SERV_AREA_ID =  115 --Lowell Community Health Center [LCHC115]
	AND EFFECTIVE_DATE_DTTM BETWEEN @startDate AND @endDate
	--AND CONTACT_DATE >= DATEADD ( DAY, -0, @startDate) AND CONTACT_DATE < @endDate
     
     UNION
     
    SELECT        PAT_ID
    FROM          [CLARITY].[dbo].ORDER_MED
    WHERE SERV_AREA_ID =  115 --Lowell Community Health Center [LCHC115]
	 AND UPDATE_DATE BETWEEN @startDate AND @endDate
	 --AND UPDATE_DATE >= DATEADD ( DAY, -0, @startDate)  AND UPDATE_DATE < @endDate     
     
	 UNION
     
    SELECT        PAT_ID
    FROM          [CLARITY].[dbo].ORDER_PROC
    WHERE SERV_AREA_ID  =  115 --Lowell Community Health Center [LCHC115]
	AND UPDATE_DATE BETWEEN @startDate AND @endDate
	--AND UPDATE_DATE >= DATEADD ( DAY, -0, @startDate)  AND UPDATE_DATE < @endDate 
    
	UNION
     
    SELECT        PAT_ID
    FROM          [CLARITY].[dbo].ORDER_RESULTS
    WHERE  SERV_AREA_ID =  115	--Lowell Community Health Center	[LCHC115]
	AND RESULT_DATE BETWEEN @startDate AND @endDate
	 --AND RESULT_DATE >= DATEADD ( DAY, -0, @startDate)     AND RESULT_DATE < @endDate
          
     ;
     

    DROP TABLE IF EXISTS #X_ESP_XTR_Patient_MEM;

    -- Get patient details for patients identified
    SELECT DISTINCT
        pa.PAT_MRN_ID AS MRN
      , pa.*
    INTO        #X_ESP_XTR_Patient_MEM
    FROM [CLARITY].[dbo].PATIENT pa
    INNER JOIN #X_ESP_XTR_Pat_Enc_MEM pe ON pe.PAT_ID = pa.PAT_ID
	INNER JOIN VALID_PATIENT VP ON PA.PAT_ID = VP.PAT_ID
	INNER JOIN PATIENT_3 P3 ON PA.PAT_ID = P3.PAT_ID
    WHERE -- exclude test patients
        P3.IS_TEST_PAT_YN = 'N'
		AND VP.IS_VALID_PAT_YN = 'Y';

    CREATE UNIQUE INDEX ix_PATID ON #X_ESP_XTR_Patient_MEM ( PAT_ID );

     
    --DROP TABLE IF EXISTS #OUTPUT;

    SELECT DISTINCT
        pa.PAT_ID
      , pa.MRN
      , pa.PAT_LAST_NAME
      , pa.PAT_FIRST_NAME
      , pa.PAT_MIDDLE_NAME
      , addr1.[ADDRESS]                               AS ADDRESS1
      , CASE WHEN CHARINDEX ( '^', ISNULL (  addr2.[ADDRESS], '' )) = 0  THEN '' 
	    ELSE addr2.[ADDRESS] END AS ADDRESS2
      , pa.CITY                                     AS CITY
      , st.ABBR                                     AS PAT_STATE
      , CAST(pa.ZIP as varchar)                     AS ZIP
      , ctry1.TITLE                                 AS COUNTRY
      , CAST(LEFT(pa.HOME_PHONE, 3) as varchar)     AS AREA_CODE
      , RIGHT(pa.HOME_PHONE, 8)                     AS HOME_PHONE
      , CAST(NULL as varchar)                       AS HOME_PHONE_EXT
      , CONVERT ( VARCHAR(8), pa.BIRTH_DATE, 112 )  AS DOB
       -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
             WHEN pa.SEX_C IN ( '1', '2', '3' ) THEN sex.ABBR
             WHEN pa.SEX_C IN ( '4', '5' ) THEN 'T'
             ELSE 'U'
        END                                         AS GENDER
       -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
             WHEN prace1.PATIENT_RACE_C IS NOT NULL 
                  --IF ONE OF THE RACES IS UNKNOWN OR DECLINED DON'T CONSIDER IT
                  AND prace1.PATIENT_RACE_C NOT IN (8, 6)
                  AND prace2.PATIENT_RACE_C IS NOT NULL
                  -- RACES NEED TO BE DIFFERENT
                  AND prace1.PATIENT_RACE_C <> prace2.PATIENT_RACE_C
                  THEN 'MULTIRACIAL' 
             -- 1 AMERICAN INDIAN/ALASKAN NATIVE
             -- 2 ASIAN
             -- 3 BLACK
             -- 4 HISPANIC
             -- 5 OTHER
             -- 6 UNKNOWN
             -- 7 WHITE
             -- 9 PACIFIC ISLANDER/HAWAIIAN
             WHEN prace1.PATIENT_RACE_C IN ( 1, 2, 3, 4, 5, 6, 7) THEN zrace.TITLE
             WHEN prace1.PATIENT_RACE_C = 9 THEN 'ASIAN'-- Chinese
			 -- 8 REFORMAT TEXT FOR DECLINED TO ANSWER      
             WHEN prace1.PATIENT_RACE_C = 8 THEN 'DECLINED/REFUSED'
             -- 10 ASHKENAZI JEWISH
             WHEN prace1.PATIENT_RACE_C = 10 THEN 'OTHER'
             ELSE 'UNKNOWN'
        END                                         AS RACE
      , lang.TITLE                                  AS HOME_LANGUAGE
      , pa.SSN
      , pa.CUR_PCP_PROV_ID                          AS PCP_ID
      , mari.TITLE                                  AS MARITAL_STATUS
      , reli.TITLE                                  AS RELIGION
      , CAST(NULL as varchar)                       AS ALIAS
      , mother.MRN                                  AS MOTHER_MRN
      , CONVERT ( VARCHAR(8), pa.DEATH_DATE, 112 )  AS DEATH_DATE
      , CAST(NULL as varchar)                       AS CENTER_ID
      -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      -- ETHNICITY DATA MAY BE STORED ELSEWHERE
      , CASE
             WHEN pa.ETHNIC_GROUP_C = 1 THEN 'NOT HISPANIC'
             -- 2 UNKNOWN
             -- 3 DECLINED TO ANSWER
             -- 8 BLANK
             WHEN pa.ETHNIC_GROUP_C IN (2, 3, 8) THEN 'OTHER OR UNDETERMINED'
             -- HISPANIC GROUPS
             WHEN pa.ETHNIC_GROUP_C IN (4, 5, 6 ,7) THEN 'HISPANIC OR LATINO'
             ELSE 'OTHER OR UNDETERMINED'
        END                                         AS ETHNICITY
      , mother.PAT_NAME                             AS MOTHER_MAIDEN_NAME
      , CONVERT ( VARCHAR(8), pa.UPDATE_DATE, 112 ) AS LAST_UPDATE
      , CAST(NULL as varchar)                       AS SITE_ID_LAST_UPD
      , sufx.TITLE                                  AS SUFFIX
      , titl.TITLE
      , CAST(NULL as varchar)                       AS REMARK
      , CAST(NULL as varchar)                       AS INCOME_LEVEL
      , CAST(NULL as varchar)                       AS HOUSING_STATUS
      , fin.TITLE                                   AS INSURANCE_STATUS
      , ctry2.TITLE                                 AS COUNTRY_OF_BIRTH
      -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
           WHEN pa.PAT_STATUS_C = 1 THEN 'ALIVE'
           WHEN pa.PAT_STATUS_C = 2 THEN 'DECEASED'
        END                                         AS VITAL_STATUS
      , CAST(NULL as varchar)                       AS NEXT_APPT_PROV_ID
      , CAST(NULL as varchar)                       AS NEXT_APPT_DATE
      , CAST(NULL as varchar)                       AS NEXT_APPT_FAC_PROV_ID
      , zgend_iden.TITLE                            AS GENDER_IDENTITY
      , zsexor.TITLE                                AS SEXUAL_ORIENTATION
      , birth_sex.TITLE                             AS SEX_AT_BIRTH
    --INTO        #OUTPUT
    FROM        #X_ESP_XTR_Patient_MEM pa
	LEFT JOIN [CLARITY].[dbo].PAT_ADDRESS        addr1  ON addr1.PAT_ID = pa.PAT_ID             AND addr1.LINE = 1
    LEFT JOIN [CLARITY].[dbo].PAT_ADDRESS        addr2  ON addr2.PAT_ID = pa.PAT_ID             AND addr2.LINE = 2
    LEFT JOIN [CLARITY].[dbo].ZC_STATE           st     ON st.STATE_C = pa.STATE_C
    LEFT JOIN [CLARITY].[dbo].ZC_COUNTRY         ctry1  ON ctry1.COUNTRY_C = pa.COUNTRY_C
    LEFT JOIN [CLARITY].[dbo].ZC_COUNTRY         ctry2  ON ctry2.COUNTRY_C = pa.COUNTRY_OF_ORIG_C
    LEFT JOIN [CLARITY].[dbo].ZC_SEX             sex    ON pa.SEX_C = sex.RCPT_MEM_SEX_C
    LEFT JOIN [CLARITY].[dbo].PATIENT_RACE       prace1 ON pa.PAT_ID = prace1.PAT_ID             AND prace1.LINE = 1
    LEFT JOIN [CLARITY].[dbo].PATIENT_RACE       prace2 ON pa.PAT_ID = prace2.PAT_ID             AND prace2.LINE = 2
    LEFT JOIN [CLARITY].[dbo].ZC_PATIENT_RACE    zrace  ON prace1.PATIENT_RACE_C = zrace.PATIENT_RACE_C
    LEFT JOIN [CLARITY].[dbo].ZC_LANGUAGE        lang   ON pa.LANGUAGE_C = lang.LANGUAGE_C
    LEFT JOIN [CLARITY].[dbo].ZC_MARITAL_STATUS  mari   ON pa.MARITAL_STATUS_C = mari.MARITAL_STATUS_C
    LEFT JOIN [CLARITY].[dbo].ZC_RELIGION        reli   ON pa.RELIGION_C = reli.RELIGION_C
    LEFT JOIN
       ( SELECT pa.PAT_ID, pa.MRN, pa.PAT_NAME FROM #X_ESP_XTR_Patient_MEM AS pa ) AS mother          ON mother.PAT_ID = pa.MOTHER_PAT_ID
    LEFT JOIN [CLARITY].[dbo].ZC_ETHNIC_GROUP        ethn       ON ethn.ETHNIC_GROUP_C = pa.ETHNIC_GROUP_C
    LEFT JOIN [CLARITY].[dbo].ZC_PAT_NAME_SUFFIX     sufx       ON sufx.PAT_NAME_SUFFIX_C = pa.PAT_NAME_SUFFIX_C
    LEFT JOIN [CLARITY].[dbo].ZC_PAT_TITLE           titl       ON pa.PAT_TITLE_C = titl.PAT_TITLE_C
    LEFT JOIN [CLARITY].[dbo].PAT_ACCT_CVG           pac        ON pac.PAT_ID = pa.PAT_ID             AND pac.LINE = 1
    LEFT JOIN [CLARITY].[dbo].ZC_FIN_CLASS           fin        ON pac.FIN_CLASS = fin.FIN_CLASS_C
    LEFT JOIN [CLARITY].[dbo].PATIENT_4              pat4       ON pat4.PAT_ID = pa.PAT_ID
	LEFT JOIN [CLARITY].[dbo].ZC_GENDER_IDENTITY     zgend_iden ON zgend_iden.GENDER_IDENTITY_C = pat4.GENDER_IDENTITY_C
    LEFT JOIN [CLARITY].[dbo].PAT_SEXUAL_ORIENTATION sexor      ON sexor.PAT_ID = pa.PAT_ID
    LEFT JOIN [CLARITY].[dbo].ZC_SEXUAL_ORIENTATION  zsexor     ON zsexor.SEXUAL_ORIENTATION_C = sexor.SEXUAL_ORIENTATN_C
    LEFT JOIN [CLARITY].[dbo].ZC_SEX_ASGN_AT_BIRTH   birth_sex  ON birth_sex.SEX_ASGN_AT_BIRTH_C = pat4.SEX_ASGN_AT_BIRTH_C
    WHERE
        CHARINDEX ( '^', pa.PAT_LAST_NAME ) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_FIRST_NAME, '' )) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_MIDDLE_NAME, '' )) = 0
     	--AND	PA.PAT_MRN_ID = '2006199'
	--WHERE PE.CONTACT_DATE BETWEEN @StartDate and @EndDate
