--this is a preliminary set of SQL code that will take individual patient level FHIR data and load to ESP
--this uses MITRE Synthea data at https://synthea.mitre.org/downloads.  
--I used the FHIR R4 1K data, but there are some large sets there if you want a lot of data
--This script is written for JSON fhir data structures, not XML.
--load the FHIR files to JSONB column in a raw data table
drop table if exists fhir_files;
create table fhir_files (filename text);
--the initial copy command expects a linux file system.
--you'll want to change this path, of course, to where you're storing the FHIR resource files.  
COPY fhir_files FROM PROGRAM 'find /home/bobz/vm_shared/syntheaFHIR/fhir/ -maxdepth 1 -type f -printf "%f\n"';
drop table if exists fhir_txt;
create table fhir_txt (id SERIAL PRIMARY KEY NOT NULL, jrow text);
drop table if exists fhir_json;
create table fhir_json (source varchar(260), patrec jsonb);
--and here is another local path that needs changing.  Probably worth parameterizing this
do $$
declare ff record;
begin
  for ff in select filename from fhir_files
  loop
    execute 'copy fhir_txt(jrow) from ''/home/bobz/vm_shared/syntheaFHIR/fhir/' || ff.filename || 
	 '''  csv quote e''\x01'' delimiter e''\x02'' ';
    execute 'insert into fhir_json (source, patrec) select ''' ||
	        ff.filename || ''', string_agg(jrow, E''\n'' order by id)::json from fhir_txt';
	execute 'truncate fhir_txt restart identity';
  end loop;
end$$;

--load the provenance table
insert into emr_provenance (timestamp, source, hostname, status, valid_rec_count, error_count)
select now(), source, 'https://synthea.mitre.org/downloads', '', -1, -1
from fhir_json;

--load the patient table
with t0 as ( 
  select patrec -> 'entry' ->0-> 'resource' as patresource, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select (patresource ->> 'id')::varchar(128) as pid,
          (patresource -> 'extension') as patext
  from t0)
,   t10 as (
  select pid, i.raceitem
  from t1
  cross join lateral jsonb_array_elements(t1.patext) i(raceitem)
  where i.raceitem ->> 'url' = 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-race')
,   t11 as (
  select pid, i.ethnitem
  from t1
  cross join lateral jsonb_array_elements(t1.patext) i(ethnitem)
  where i.ethnitem ->> 'url' = 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity')
insert into emr_patient (natural_key, created_timestamp, updated_timestamp, state, zip, zip5, date_of_birth, cdate_of_birth, race, ethnicity, provenance_id)
select t1.pid as natural_key, now() as created_timestamp, now() as updated_timestamp, 
  (t0.patresource -> 'address' ->0->> 'state') as state,
  (t0.patresource -> 'address' ->0->> 'postalCode') as zip,  
  substr((t0.patresource -> 'address' ->0->> 'postalCode'),1,5) as zip5,  
  (t0.patresource ->> 'birthDate')::timestamp as date_of_birth,
  (t0.patresource ->> 'birthDate')::varchar(100) as cdate_of_birth,
  (t10.raceitem -> 'extension' ->0-> 'valueCoding' ->> 'display')::varchar(100) race,
  (t11.ethnitem -> 'extension' ->0-> 'valueCoding' ->> 'display')::varchar(100) ethnicity,
  p.provenance_id
from t0
join t1 on (t0.patresource ->> 'id')::varchar(128)=t1.pid
join t10 on t10.pid=t1.pid
join t11 on t11.pid=t1.pid
join emr_provenance p on p.source=t0.source;

--load the encounter table
with t0 as ( 
  select (patrec -> 'entry' ->0-> 'resource' ->> 'id')::varchar(128) as pid, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select fj.source, i.encitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(encitem)
  where i.encitem -> 'resource' ->> 'resourceType' = 'Encounter' )
, t2 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
    and (i.obsitem -> 'resource' -> 'code' ->> 'text') = 'Body Height')
, t3 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
    and (i.obsitem -> 'resource' -> 'code' ->> 'text') = 'Body Weight')
, t4 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
    and (i.obsitem -> 'resource' -> 'code' ->> 'text') = 'Body Mass Index')
, t5 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
    and (i.obsitem -> 'resource' -> 'code' ->> 'text') = 'Blood Pressure')
, t50 as (
  select (t5.obsitem -> 'resource' -> 'encounter' ->> 'reference') as fullURL,
          i.compitem
  from t5
  cross join lateral jsonb_array_elements(obsitem -> 'resource' -> 'component' ) i(compitem)
  where i.compitem -> 'code' ->> 'text' = 'Diastolic Blood Pressure'  )
, t51 as (
  select (t5.obsitem -> 'resource' -> 'encounter' ->> 'reference') as fullURL,
          i.compitem
  from t5
  cross join lateral jsonb_array_elements(obsitem -> 'resource' -> 'component' ) i(compitem)
  where i.compitem -> 'code' ->> 'text' = 'Systolic Blood Pressure'  )
insert into emr_encounter (natural_key, created_timestamp, updated_timestamp, date, raw_encounter_type,
						   height, weight, bmi, bp_diastolic, bp_systolic, provider_id, provenance_id, patient_id, priority)
select (t1.encitem-> 'resource' ->> 'id')::varchar(128) || ':' ||
  trim(to_char(row_number() over (partition by (t1.encitem-> 'resource' ->> 'id')::varchar(128)),'999999')) as natural_key, 
  now() as created_timestamp, now() as updated_timestamp,
  (t1.encitem -> 'resource' -> 'period' ->> 'start')::date date,
  ((t1.encitem -> 'resource' -> 'class' ->> 'code') || ': ' ||
   (t1.encitem -> 'resource' -> 'type' ->0-> 'coding' ->0->> 'display'))::varchar(100) as raw_encounter_type,
  case
    when (t2.obsitem -> 'resource' -> 'valueQuantity' ->> 'unit') = 'cm'
      then (t2.obsitem -> 'resource' -> 'valueQuantity' ->> 'value')::numeric
	when (t2.obsitem -> 'resource' -> 'valueQuantity' ->> 'unit') <> 'cm'
	  then -1
  end as height,
  case
    when (t3.obsitem -> 'resource' -> 'valueQuantity' ->> 'unit') = 'kg'
      then (t3.obsitem -> 'resource' -> 'valueQuantity' ->> 'value')::numeric
	when (t3.obsitem -> 'resource' -> 'valueQuantity' ->> 'unit') <> 'kg'
	  then -1
  end as weight,
  (t4.obsitem -> 'resource' -> 'valueQuantity' ->> 'value')::numeric as bmi,
  (t50.compitem -> 'valueQuantity' ->> 'value')::numeric as bp_diastolic,
  (t51.compitem -> 'valueQuantity' ->> 'value')::numeric as bp_systolic,
   1 as provider_id, pr.provenance_id, pa.id as patient_id, '' as priority
from t1
join t0 on t0.source=t1.source
join emr_provenance pr on pr.source=t1.source
join emr_patient pa on pa.natural_key=t0.pid
left join t2 on (t2.obsitem -> 'resource' -> 'encounter' ->> 'reference') = (t1.encitem ->> 'fullUrl')
left join t3 on (t3.obsitem -> 'resource' -> 'encounter' ->> 'reference') = (t1.encitem ->> 'fullUrl')
left join t4 on (t4.obsitem -> 'resource' -> 'encounter' ->> 'reference') = (t1.encitem ->> 'fullUrl')
left join t50 on t50.fullURL = (t1.encitem ->> 'fullUrl')
left join t51 on t51.fullURL = (t1.encitem ->> 'fullUrl');

--load static_dx_codes table with unknown icd10 codes
with t0 as ( 
  select fj.source, i.conditem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(conditem)
  where i.conditem -> 'resource' ->> 'resourceType' = 'Condition' )
, t1 as (
  select split_part((t0.conditem -> 'resource' -> 'encounter' ->> 'reference'),':',3) as enc_natural_key, i.snomed, source
  from t0
  cross join lateral jsonb_array_elements(t0.conditem -> 'resource' -> 'code' -> 'coding') i(snomed)
  where i.snomed ->> 'system' like '%snomed%')
  insert into static_dx_code (combotypecode, code, type, name, longname)
select 'icd10:' || trim(xwlk.maptarget) as combotypecode, trim(xwlk.maptarget) as code, 'icd10' as type, 'added by load process' as name, 'added by load process' as longname
from t1
join snomed2icd10_full xwlk on xwlk.referencedcomponentid = (t1.snomed ->> 'code')::bigint --and xwlk.maprule='TRUE'
left join static_dx_code dx on 'icd10:' || trim(xwlk.maptarget) = dx.combotypecode
where xwlk.maptarget is not null and dx.combotypecode is null
group by trim(xwlk.maptarget)

--load encounter_dx_codes
with t0 as ( 
  select fj.source, i.conditem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(conditem)
  where i.conditem -> 'resource' ->> 'resourceType' = 'Condition' )
, t1 as (
  select split_part((t0.conditem -> 'resource' -> 'encounter' ->> 'reference'),':',3) as enc_natural_key, i.snomed, source
  from t0
  cross join lateral jsonb_array_elements(t0.conditem -> 'resource' -> 'code' -> 'coding') i(snomed)
  where i.snomed ->> 'system' like '%snomed%')
insert into emr_encounter_dx_codes (encounter_id, dx_code_id)
select e.id as encounter_id--, t1.enc_natural_key, t1.source
, 'icd10:' || trim(xwlk.maptarget) as dx_code_id
from t1
join emr_encounter e on split_part(e.natural_key,':',1) = t1.enc_natural_key
left join snomed2icd10_full xwlk on xwlk.referencedcomponentid = (t1.snomed ->> 'code')::bigint --and xwlk.maprule='TRUE'
where xwlk.maptarget is not null
group by e.id, 'icd10:' || trim(xwlk.maptarget);

--load labresults
with t0 as ( 
  select (patrec -> 'entry' ->0-> 'resource' ->> 'id')::varchar(128) as pid, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
      and i.obsitem -> 'resource' -> 'category' ->0-> 'coding' ->0->> 'code' = 'laboratory')
--The MITRE data was checked for component lab results, but none were found.  This is unlikely to be
-- true for other FHIR sources of lab results.  obsitem -> resource -> component contents should be checked.
-- Similarly the various value components were checked (see case statement below). MITRE FHIR has no valueInteger,
--  valueRange, valueBoolean or valueRatio.  This should b checked with other FHIR sources.
insert into emr_labresult (natural_key, created_timestamp, updated_timestamp, date, order_natural_key,
						   native_code, native_name, result_date, cresult_date, collection_date, ccollection_date,
                         result_string, ref_unit, result_float, provider_id, provenance_id, patient_id )
select (t1.obsitem-> 'resource' ->> 'id')::varchar(128) as natural_key, 
  now() as created_timestamp, now() as updated_timestamp,
  (t1.obsitem -> 'resource' ->> 'effectiveDateTime')::date date,
  split_part((t1.obsitem -> 'resource' -> 'encounter' ->> 'reference'),':',3)::varchar(128) as order_natural_key,
  (t1.obsitem -> 'resource' -> 'code' -> 'coding' ->0->> 'code') as native_code,
  (t1.obsitem -> 'resource' -> 'code' ->> 'text') as native_name,
  (t1.obsitem -> 'resource' ->> 'effectiveDateTime')::timestamp result_date,
  (t1.obsitem -> 'resource' ->> 'effectiveDateTime')::varchar(100) cresult_date,
  (t1.obsitem -> 'resource' ->> 'issued')::timestamp collection_date,
  (t1.obsitem -> 'resource' ->> 'issued')::varchar(100) ccollection_date,
  case 
    when (t1.obsitem -> 'resource' ->> 'valueQuantity') is not null 
	  then (t1.obsitem -> 'resource' -> 'valueQuantity' ->> 'value')::varchar(2000)  --unit is element 'unit'
    when (t1.obsitem -> 'resource' ->> 'valueCodeableConcept') is not null 
	  then (t1.obsitem -> 'resource' -> 'valueCodeableConcept' ->> 'text')::varchar(2000) 
    when (t1.obsitem -> 'resource' ->> 'valueString') is not null 
	  then (t1.obsitem -> 'resource' ->> 'valueString')::varchar(2000)  
  end as result_string,
  case 
    when (t1.obsitem -> 'resource' ->> 'valueQuantity') is not null 
	  then (t1.obsitem -> 'resource' -> 'valueQuantity' ->> 'unit')::varchar(100)  
  end as ref_unit,
  case 
    when (t1.obsitem -> 'resource' ->> 'valueQuantity') is not null 
	  then (t1.obsitem -> 'resource' -> 'valueQuantity' ->> 'value')::numeric 
  end as result_float,
  1 as provider_id, pr.provenance_id, pa.id as patient_id
from t1
join t0 on t0.source=t1.source
join emr_provenance pr on pr.source=t1.source
join emr_patient pa on pa.natural_key=t0.pid;

--just using resourceType MedicationRequest, but this may not be appropriate for other FHIR sources
with t0 as ( 
  select (patrec -> 'entry' ->0-> 'resource' ->> 'id')::varchar(128) as pid, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select fj.source, i.medreqitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(medreqitem)
  where i.medreqitem -> 'resource' ->> 'resourceType' = 'MedicationRequest' )
insert into emr_prescription (natural_key, created_timestamp, updated_timestamp, date, order_natural_key,
						   name, code, dose, 
                         provider_id, provenance_id, patient_id )
select (t1.medreqitem-> 'resource' ->> 'id')::varchar(128) as natural_key, 
  now() as created_timestamp, now() as updated_timestamp,
  (t1.medreqitem -> 'resource' ->> 'authoredOn')::date date,
  split_part((t1.medreqitem -> 'resource' -> 'encounter' ->> 'reference'),':',3)::varchar(128) as order_natural_key,
  (t1.medreqitem -> 'resource' -> 'medicationCodeableConcept' ->> 'text') as name,
  (t1.medreqitem -> 'resource' -> 'medicationCodeableConcept' -> 'coding' ->0->> 'code') as code,
  (t1.medreqitem -> 'resource' -> 'dosageInstruction' ->0-> 'doseAndRate' ->0-> 'doseQuantity' ->> 'value') as dose,
  1 as provider_id, pr.provenance_id, pa.id as patient_id
from t1
join t0 on t0.source=t1.source
join emr_provenance pr on pr.source=t1.source
join emr_patient pa on pa.natural_key=t0.pid
where (t1.medreqitem -> 'resource' -> 'medicationCodeableConcept' ->> 'text') is not null;

--immunization
with t0 as ( 
  select (patrec -> 'entry' ->0-> 'resource' ->> 'id')::varchar(128) as pid, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select fj.source, i.immitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(immitem)
  where i.immitem -> 'resource' ->> 'resourceType' = 'Immunization' )
insert into emr_immunization (natural_key, created_timestamp, updated_timestamp, date, 
						   name, imm_type, 
                         provider_id, provenance_id, patient_id, isvaccine )
select (t1.immitem-> 'resource' ->> 'id')::varchar(128) as natural_key, 
  now() as created_timestamp, now() as updated_timestamp,
  (t1.immitem -> 'resource' ->> 'occurrenceDateTime')::date date,
  (t1.immitem -> 'resource' -> 'vaccineCode' ->> 'text') as name,
  (t1.immitem -> 'resource' -> 'vaccineCode' -> 'coding' ->0->> 'code') as imm_type,
  1 as provider_id, pr.provenance_id, pa.id as patient_id, TRUE as isvaccine
from t1
join t0 on t0.source=t1.source
join emr_provenance pr on pr.source=t1.source
join emr_patient pa on pa.natural_key=t0.pid;

--smoking status
with t0 as ( 
  select (patrec -> 'entry' ->0-> 'resource' ->> 'id')::varchar(128) as pid, source
  from fhir_json 
  where patrec -> 'entry' ->0-> 'resource' @> '{"resourceType": "Patient"}'::jsonb )
,    t1 as (
  select fj.source, i.obsitem
  from fhir_json fj
  cross join lateral jsonb_array_elements(patrec -> 'entry') i(obsitem)
  where i.obsitem -> 'resource' ->> 'resourceType' = 'Observation'
     and i.obsitem -> 'resource' -> 'code' ->> 'text' = 'Tobacco smoking status NHIS')
insert into emr_socialhistory (natural_key, created_timestamp, updated_timestamp, date, 
						   tobacco_use, 
                         provider_id, provenance_id, patient_id )
select (t1.obsitem-> 'resource' ->> 'id')::varchar(128) as natural_key, 
  now() as created_timestamp, now() as updated_timestamp,
  (t1.obsitem -> 'resource' ->> 'effectiveDateTime')::date date,
  (t1.obsitem -> 'resource' -> 'valueCodeableConcept' ->> 'text') as tobacco_use,
  1 as provider_id, pr.provenance_id, pa.id as patient_id
from t1
join t0 on t0.source=t1.source
join emr_provenance pr on pr.source=t1.source
join emr_patient pa on pa.natural_key=t0.pid;