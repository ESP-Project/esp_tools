-- Create extract of all patients that have updated info within the x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.

-- Typically startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but it is recommended to set this to 5 days if feasible.


USE [clarity_production_temp]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Member] ( @startDate DATETIME2(3), @endDate DATETIME2(3))
AS
BEGIN
    /*--------------------------------------------------------
    Returns patient data required for the ESP member extract
    ---------------------------------------------------------*/
    DECLARE
        @Pat_ID VARCHAR(100)
      , @Line SMALLINT
      , @MaxLine SMALLINT
      , @Insert BIT;

    SET NOCOUNT ON;


    DROP TABLE IF EXISTS #X_ESP_XTR_Pat_Enc_MEM;

    -- all patient encounter/contact for past x days
    SELECT
        PAT_ID
    INTO
        #X_ESP_XTR_Pat_Enc_MEM
    FROM
          Clarity.dbo.PATIENT
    WHERE 
          UPDATE_DATE >= DATEADD ( DAY, -0, @startDate)
     AND UPDATE_DATE < @endDate
     
     UNION
     
     SELECT 
          PAT_ID 
     FROM
          Clarity.dbo.PAT_ENC 
    WHERE
          CONTACT_DATE >= DATEADD ( DAY, -0, @startDate)
     AND CONTACT_DATE < @endDate
     
     UNION
     
     SELECT
        PAT_ID
    FROM
          Clarity.dbo.ORDER_MED
    WHERE 
          UPDATE_DATE >= DATEADD ( DAY, -0, @startDate) 
     AND UPDATE_DATE < @endDate
     
     UNION
     
     SELECT
        PAT_ID
    FROM
          Clarity.dbo.ORDER_PROC
    WHERE 
          UPDATE_DATE >= DATEADD ( DAY, -0, @startDate) 
     AND UPDATE_DATE < @endDate
     
    UNION
     
     SELECT
        PAT_ID
    FROM
          Clarity.dbo.ORDER_RESULTS
    WHERE 
          RESULT_DATE >= DATEADD ( DAY, -0, @startDate) 
     AND RESULT_DATE < @endDate
          
     ;
     

    DROP TABLE IF EXISTS #X_ESP_XTR_Patient_MEM;

    -- Get patient details for patients identified
    SELECT DISTINCT
        pa.PAT_MRN_ID AS MRN
      , pa.*
    INTO
        #X_ESP_XTR_Patient_MEM
    FROM
        Clarity.dbo.PATIENT                   AS pa
        INNER JOIN #X_ESP_XTR_Pat_Enc_MEM     AS pe
           ON pe.PAT_ID = pa.PAT_ID
    WHERE
        -- exclude test patients
        pa.PAT_NAME NOT LIKE 'ZZ%';

    CREATE UNIQUE INDEX ix_PATID ON #X_ESP_XTR_Patient_MEM ( PAT_ID );

     
    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_MEM;

    SELECT DISTINCT
        pa.PAT_ID
      , pa.MRN
      , pa.PAT_LAST_NAME
      , pa.PAT_FIRST_NAME
      , pa.PAT_MIDDLE_NAME
      , addr1.ADDRESS                               AS ADDRESS1
      , addr2.ADDRESS                               AS ADDRESS2
      , pa.CITY                                     AS CITY
      , st.ABBR                                     AS PAT_STATE
      , pa.ZIP                                      AS ZIP
      , ctry1.TITLE                                 AS COUNTRY
      , LEFT(pa.HOME_PHONE, 3)                      AS AREA_CODE
      , RIGHT(pa.HOME_PHONE, 8)                     AS HOME_PHONE
      , NULL                                        AS HOME_PHONE_EXT
      , CONVERT ( VARCHAR(8), pa.BIRTH_DATE, 112 )  AS DOB
       -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
             WHEN pa.SEX_C IN ( '1', '2', '3' ) THEN sex.ABBR
             WHEN pa.SEX_C IN ( '4', '5' ) THEN 'T'
             ELSE 'U'
        END                                         AS GENDER
       -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
             WHEN prace1.PATIENT_RACE_C IS NOT NULL 
                  --IF ONE OF THE RACES IS UNKNOWN OR DECLINED DON'T CONSIDER IT
                  AND prace1.PATIENT_RACE_C NOT IN (8, 6)
                  AND prace2.PATIENT_RACE_C IS NOT NULL
                  -- RACES NEED TO BE DIFFERENT
                  AND prace1.PATIENT_RACE_C <> prace2.PATIENT_RACE_C
                  THEN 'MULTIRACIAL' 
             -- 1 AMERICAN INDIAN/ALASKAN NATIVE
             -- 2 ASIAN
             -- 3 BLACK
             -- 4 HISPANIC
             -- 5 OTHER
             -- 6 UNKNOWN
             -- 7 WHITE
             -- 9 PACIFIC ISLANDER/HAWAIIAN
             WHEN prace1.PATIENT_RACE_C IN ( 1, 2, 3, 4, 5, 6, 7, 9) THEN zrace.TITLE
             -- 8 REFORMAT TEXT FOR DECLINED TO ANSWER      
             WHEN prace1.PATIENT_RACE_C = 8 THEN 'DECLINED/REFUSED'
             -- 10 ASHKENAZI JEWISH
             WHEN prace1.PATIENT_RACE_C = 10 THEN 'OTHER'
             ELSE 'UNKNOWN'
        END                                         AS RACE
      , lang.TITLE                                  AS HOME_LANGUAGE
      , pa.SSN
      , pa.CUR_PCP_PROV_ID                          AS PCP_ID
      , mari.TITLE                                  AS MARITAL_STATUS
      , reli.TITLE                                  AS RELIGION
      , NULL                                        AS ALIAS
      , mother.MRN                                  AS MOTHER_MRN
      , CONVERT ( VARCHAR(8), pa.DEATH_DATE, 112 )  AS DEATH_DATE
      , NULL                                        AS CENTER_ID
      -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      -- ETHNICITY DATA MAY BE STORED ELSEWHERE
      , CASE
             WHEN pa.ETHNIC_GROUP_C = 1 THEN 'NOT HISPANIC'
             -- 2 UNKNOWN
             -- 3 DECLINED TO ANSWER
             -- 8 BLANK
             WHEN pa.ETHNIC_GROUP_C IN (2, 3, 8) THEN 'OTHER OR UNDETERMINED'
             -- HISPANIC GROUPS
             WHEN pa.ETHNIC_GROUP_C IN (4, 5, 6 ,7) THEN 'HISPANIC OR LATINO'
             ELSE 'OTHER OR UNDETERMINED'
        END                                         AS ETHNICITY
      , mother.PAT_NAME                             AS MOTHER_MAIDEN_NAME
      , CONVERT ( VARCHAR(8), pa.UPDATE_DATE, 112 ) AS LAST_UPDATE
      , NULL                                        AS SITE_ID_LAST_UPD
      , sufx.TITLE                                  AS SUFFIX
      , titl.TITLE
      , NULL                                        AS REMARK
      , NULL                                        AS INCOME_LEVEL
      , NULL                                        AS HOUSING_STATUS
      , fin.TITLE                                   AS INSURANCE_STATUS
      , ctry2.TITLE                                 AS COUNTRY_OF_BIRTH
      -- MAPPINGS SHOULD BE UPDATED TO MATCH THE SITE
      , CASE
           WHEN pa.PAT_STATUS_C = 1 THEN 'ALIVE'
           WHEN pa.PAT_STATUS_C = 2 THEN 'DECEASED'
        END                                         AS VITAL_STATUS
      , NULL                                        AS NEXT_APPT_PROV_ID
      , NULL                                        AS NEXT_APPT_DATE
      , NULL                                        AS NEXT_APPT_FAC_PROV_ID
      , zgend_iden.TITLE                            AS GENDER_IDENTITY
      , zsexor.TITLE                                AS SEXUAL_ORIENTATION
      , birth_sex.TITLE                             AS SEX_AT_BIRTH
    INTO
        #X_ESP_XTR_Final_Results_MEM
    FROM
        #X_ESP_XTR_Patient_MEM                                           AS pa
        LEFT JOIN Clarity.dbo.PAT_ADDRESS                                AS addr1
          ON addr1.PAT_ID = pa.PAT_ID
             AND addr1.LINE = 1
        LEFT JOIN Clarity.dbo.PAT_ADDRESS                                AS addr2
          ON addr2.PAT_ID = pa.PAT_ID
             AND addr2.LINE = 2
        LEFT JOIN Clarity.dbo.ZC_STATE                                   AS st
          ON st.STATE_C = pa.STATE_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY                                 AS ctry1
          ON ctry1.COUNTRY_C = pa.COUNTRY_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY                                 AS ctry2
          ON ctry2.COUNTRY_C = pa.COUNTRY_OF_ORIG_C
        LEFT JOIN Clarity.dbo.ZC_SEX                                     AS sex
          ON pa.SEX_C = sex.RCPT_MEM_SEX_C
        LEFT JOIN Clarity.dbo.PATIENT_RACE                               AS prace1
          ON pa.PAT_ID = prace1.PAT_ID
             AND prace1.LINE = 1
        LEFT JOIN Clarity.dbo.PATIENT_RACE                               AS prace2
          ON pa.PAT_ID = prace2.PAT_ID
             AND prace2.LINE = 2
        LEFT JOIN Clarity.dbo.ZC_PATIENT_RACE                            AS zrace
          ON prace1.PATIENT_RACE_C = zrace.PATIENT_RACE_C
        LEFT JOIN Clarity.dbo.ZC_LANGUAGE                                AS lang
          ON pa.LANGUAGE_C = lang.LANGUAGE_C
        LEFT JOIN Clarity.dbo.ZC_MARITAL_STATUS                          AS mari
          ON pa.MARITAL_STATUS_C = mari.MARITAL_STATUS_C
        LEFT JOIN Clarity.dbo.ZC_RELIGION                                AS reli
          ON pa.RELIGION_C = reli.RELIGION_C
        LEFT JOIN
           ( SELECT pa.PAT_ID, pa.MRN, pa.PAT_NAME FROM #X_ESP_XTR_Patient_MEM AS pa ) AS mother
          ON mother.PAT_ID = pa.MOTHER_PAT_ID
        LEFT JOIN Clarity.dbo.ZC_ETHNIC_GROUP                            AS ethn
          ON ethn.ETHNIC_GROUP_C = pa.ETHNIC_GROUP_C
        LEFT JOIN Clarity.dbo.ZC_PAT_NAME_SUFFIX                         AS sufx
          ON sufx.PAT_NAME_SUFFIX_C = pa.PAT_NAME_SUFFIX_C
        LEFT JOIN Clarity.dbo.ZC_PAT_TITLE                               AS titl
          ON pa.PAT_TITLE_C = titl.PAT_TITLE_C
        LEFT JOIN Clarity.dbo.PAT_ACCT_CVG                               AS pac
          ON pac.PAT_ID = pa.PAT_ID
             AND pac.LINE = 1
        LEFT JOIN Clarity.dbo.ZC_FIN_CLASS                               AS fin
          ON pac.FIN_CLASS = fin.FIN_CLASS_C
        LEFT JOIN Clarity.dbo.PATIENT_4                                  AS pat4
          ON pat4.PAT_ID = pa.PAT_ID
        LEFT JOIN Clarity.dbo.ZC_GENDER_IDENTITY                         AS zgend_iden
          ON zgend_iden.GENDER_IDENTITY_C = pat4.GENDER_IDENTITY_C
        LEFT JOIN Clarity.dbo.PAT_SEXUAL_ORIENTATION                     AS sexor
          ON sexor.PAT_ID = pa.PAT_ID
        LEFT JOIN Clarity.dbo.ZC_SEXUAL_ORIENTATION                      AS zsexor
          ON zsexor.SEXUAL_ORIENTATION_C = sexor.SEXUAL_ORIENTATN_C
        LEFT JOIN Clarity.dbo.ZC_SEX_ASGN_AT_BIRTH                       AS birth_sex
          ON birth_sex.SEX_ASGN_AT_BIRTH_C = pat4.SEX_ASGN_AT_BIRTH_C
    WHERE
        CHARINDEX ( '^', pa.PAT_LAST_NAME ) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_FIRST_NAME, '' )) = 0
        AND CHARINDEX ( '^', ISNULL ( pa.PAT_MIDDLE_NAME, '' )) = 0
        
        
    SELECT
        *
    FROM
        #X_ESP_XTR_Final_Results_MEM


END;
GO





