-- Create extract of all immunizations records that have been entered within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.
-- For example, startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but IT IS RECOMMENDED to set this to 5 days if feasible.

USE [clarity_production_temp]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Immunization]
(
    @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns immunization data required for the ESP extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_Immunization;

    SELECT DISTINCT
        pa.PAT_ID                                        AS PATIENT_ID
	  , imm.IMMUNZATN_ID                                 AS IMM_TYPE
	  , imm_dict.NAME                                    AS IMM_NAME
	  , imm.IMMUNE_DATE                                  AS IMM_DATE
	  , imm.DOSE                                         AS IMM_DOSE
	  , zc_mfg.NAME                                      AS MFG_NAME
	  , imm.LOT                                          AS IMM_LOT
	  , imm.IMMUNE_ID                                    AS IMM_ID --natural_key
      , pa.PAT_MRN_ID                                    AS MRN
	  , prov.PROV_ID                                     AS PROVIDER_ID
	  , NULL                                             AS VISIT_DATE
	  , zc_imm_status.TITLE                              AS IMM_STATUS
    INTO
        #X_ESP_XTR_Final_Results_Immunization
    FROM
        Clarity.dbo.IMMUNE                               AS imm 
		INNER JOIN Clarity.dbo.PATIENT                   AS pa
		   ON pa.Pat_ID = imm.Pat_ID
		INNER JOIN Clarity.dbo.Clarity_Immunzatn         AS imm_dict 
		   ON imm.IMMUNZATN_ID = imm_dict.IMMUNZATN_ID
		LEFT JOIN ZC_MFG                                 AS zc_mfg
		   ON imm.MFG_C = zc_mfg.MFG_C
		LEFT JOIN (SELECT USER_ID, PROV_ID from Clarity.dbo.CLARITY_EMP WHERE PROV_ID IS NOT NULL )  AS prov
		   ON imm.ENTRY_USER_ID = prov.USER_ID
		LEFT JOIN ZC_IMMNZTN_STATUS                      AS zc_imm_status
		   ON imm.IMMNZTN_STATUS_C = zc_imm_status.IMMNZTN_STATUS_C
    WHERE
        imm.ENTRY_DATE < @endDate
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND imm.ENTRY_DATE >=  DATEADD ( DAY, -5, @startDate);
    
     SELECT
         *
     FROM
         #X_ESP_XTR_Final_Results_Immunization

END;
GO
