-- Create extract of all social history records that have updated info within x days 
-- prior to the startDate passed in to the script and prior to the endDate passed in to the script.

-- Typically startDate would be "yesterday" and endDate would be "today"

-- Modify as needed to meet your requirements. 
-- Some sites may wish to increase the number of "lookback" days 
-- to allow for automatic recovery if in extract does not run for a day.
-- It is set to "0" for NO lookback, but it is recommended to set this to 5 days if feasible.

USE [clarity_production_temp]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Social_History]
(
    @startDate DATETIME2(3)
  , @endDate DATETIME2(3)
)
AS
BEGIN

    SET NOCOUNT ON;
    /*----------------------------------------------------------------------
    Returns social history data required for the ESP social history extract
    ------------------------------------------------------------------------*/

    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_SOC;

    SELECT DISTINCT
        pa.PAT_ID                                        AS PATIENT_ID
      , pa.Pat_MRN_ID                                    AS MRN
      , tob.TITLE                                        AS TOBACCO_USE
      , alc.TITLE                                        AS ALCOHOL_USE
      , CONVERT( VARCHAR(8), pe.CONTACT_DATE, 112 )      AS DATE_ENTERED
      , coalesce(soc.HX_LNK_ENC_CSN, pe.PAT_ENC_CSN_ID)  AS SOCIAL_HX_ID
      , pe.VISIT_PROV_ID                                 AS PROVIDER_ID
      , CASE
            WHEN (soc.FEMALE_PARTNER_YN = 'Y' and soc.MALE_PARTNER_YN = 'Y') THEN 'MALE + FEMALE'
            WHEN soc.FEMALE_PARTNER_YN = 'Y' THEN 'FEMALE'
            WHEN soc.MALE_PARTNER_YN = 'Y' THEN 'MALE'
        END                                           AS SEX_PARTNER_GENDER
      , soc.ALCOHOL_OZ_PER_WK                            AS ALC_OZ_PER_WEEK
      , ill_drug.TITLE                                   AS ILL_DRUG_USE
      , sex_active.TITLE                                 AS SEXUALLY_ACTIVE
      , LTRIM(concat(
             (CASE WHEN soc.condom_yn = 'Y' then ' CONDOM +' ELSE NULL END),
             (CASE WHEN soc.pill_yn = 'Y' then ' PILL +' ELSE NULL END),
             (CASE WHEN soc.DIAPHRAGM_YN = 'Y' then ' DIAPHRAGM +' ELSE NULL END),
             (CASE WHEN soc.IUD_YN = 'Y' then ' IUD +' ELSE NULL END),
             (CASE WHEN soc.SURGICAL_YN = 'Y' then ' SURGICAL +' ELSE NULL END),
             (CASE WHEN soc.SPERMICIDE_YN = 'Y' then ' SPERMICIDE +' ELSE NULL END),
             (CASE WHEN soc.IMPLANT_YN = 'Y' then ' IMPLANT +' ELSE NULL END),
             (CASE WHEN soc.RHYTHM_YN = 'Y' then ' RHYTHM +' ELSE NULL END),
             (CASE WHEN soc.INJECTION_YN = 'Y' then ' INJECTION +' ELSE NULL END),
             (CASE WHEN soc.SPONGE_YN = 'Y' then ' SPONGE_YN +' ELSE NULL END),
             (CASE WHEN soc.INSERTS_YN = 'Y' then ' INSERTS +' ELSE NULL END),
             (CASE WHEN soc.ABSTINENCE_YN = 'Y' then ' ABSTINENCE +' ELSE NULL END))) AS BIRTH_CONTROL_METHOD
    INTO
        #X_ESP_XTR_Final_Results_SOC
    FROM
        Clarity.dbo.PATIENT                                    AS pa
        LEFT JOIN Clarity.dbo.Social_Hx                        AS soc
           ON soc.PAT_ID = pa.PAT_ID
        INNER JOIN Clarity.dbo.PAT_ENC                         AS pe
           ON pe.PAT_ENC_CSN_ID = soc.PAT_ENC_CSN_ID
        LEFT JOIN Clarity.dbo.ZC_TOBACCO_USER                  AS tob
          ON tob.TOBACCO_USER_C = soc.TOBACCO_USER_C
        LEFT JOIN Clarity.dbo.ZC_ALCOHOL_USE                   AS alc
          ON alc.ALCOHOL_USE_C = soc.ALCOHOL_USE_C
        LEFT JOIN Clarity.dbo.ZC_ILL_DRUG_USER                 AS ill_drug
           ON soc.ILL_DRUG_USER_C = ill_drug.ILL_DRUG_USER_C
        LEFT JOIN Clarity.dbo.ZC_SEXUALLY_ACTIVE               AS sex_active
          ON soc.SEXUALLY_ACTIVE_C = sex_active.SEXUALLY_ACTIVE_C
    WHERE
         -- don't include rows that don't have one of these values
         ( COALESCE(tob.TITLE, alc.title, ill_drug.title, sex_active.title) is not null
            OR
           COALESCE(soc.MALE_PARTNER_YN, soc.FEMALE_PARTNER_YN) <> ('N')
          )
        AND pe.CONTACT_DATE < @endDate
        -- SITES CAN ADJUST "LOOKBACK" DAYS HERE
        AND ( pe.CONTACT_DATE >=  DATEADD ( DAY, -0, @startDate)
              OR
            pe.UPDATE_DATE >=  DATEADD ( DAY, -0, @startDate) )
         -- only process updates for visits that happened in the week prior to the start date
         -- this prevents overloading system with updates but ensures recently updated records are captured
        AND pe.CONTACT_DATE >= DATEADD(DAY, -7, @startDate);
    
     SELECT
         *
     FROM
         #X_ESP_XTR_Final_Results_SOC

END;
GO
