USE [clarity_production_temp]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[X_ESP_XTR_Assemble_ESP_Provider]
AS
BEGIN
    /*--------------------------------------------------------
    Returns provider data required for the ESP provider extract
    ----------------------------------------------------------*/
    SET NOCOUNT ON;
    
    DROP TABLE IF EXISTS #X_ESP_XTR_Final_Results_PROV;
        
    SELECT All_Providers_And_Depts.* 
       INTO #X_ESP_XTR_Final_Results_PROV
    FROM (

    SELECT  DISTINCT 
        ser.PROV_ID                                             AS PROVIDER_ID
       , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                 THEN SUBSTRING(
                     ser.PROV_NAME
                     , 1
                     , IIF(CHARINDEX( ',', ser.PROV_NAME ) = 0, 1, CHARINDEX( ',', ser.PROV_NAME )) - 1 )
                 ELSE
                     SUBSTRING(
                     ser.PROV_NAME
                   , CHARINDEX( ' ', ser.PROV_NAME ) + 1
                   , LEN( ser.PROV_NAME ) - CHARINDEX( ' ', ser.PROV_NAME ) + 1 )
        END                                                       AS PROV_LAST_NAME
        
      , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                  AND CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) > 0
                 THEN -- strip off middle name/initial when existing
                     SUBSTRING(
                         ser.PROV_NAME
                         , CHARINDEX( ',', ser.PROV_NAME ) + 2
                         , CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                         - CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME )))
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                 THEN -- format is last_name, first_name
                     SUBSTRING(
                        ser.PROV_NAME
                        , CHARINDEX( ',', ser.PROV_NAME ) + 2
                        , LEN( ser.PROV_NAME ) - CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                 ELSE -- format is first_name <space> last_name
                     SUBSTRING( ser.PROV_NAME, 1, CHARINDEX( ' ', ser.PROV_NAME ))
        END                                                     AS PROV_FIRST_NAME
        

      , CASE
             WHEN CHARINDEX( ',', ser.PROV_NAME ) > 0
                  AND CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) > 0
                 THEN SUBSTRING(
                     ser.PROV_NAME
                     , CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 ) + 1
                     , LEN( ser.PROV_NAME ) - CHARINDEX( ' ', ser.PROV_NAME, CHARINDEX( ',', ser.PROV_NAME ) + 2 )
                     + 1 )
                 ELSE ''
        END                                                     AS PROV_MIDDLE_NAME
      , ser.CLINICIAN_TITLE
      , COALESCE( emp.MR_LOGON_DEPT_ID, serdept.DEPARTMENT_ID ) AS PRIM_DEPT_ID
      , COALESCE( dep.EXTERNAL_NAME, dep2.EXTERNAL_NAME )       AS DEPARTMENT_NAME
      , prim_addr.ADDR_LINE_1                                   AS PRIM_ADDR1
      , prim_addr.ADDR_LINE_2                                   AS PRIM_ADDR2
      , prim_addr.CITY                                          AS PRIM_CITY
      , st1.ABBR                                                AS PRIM_STATE
      , prim_addr.ZIP                                           AS PRIM_ZIP
      , LEFT(prim_addr.PHONE, 3)                                AS PRIM_PHONE_AC
      , RIGHT(prim_addr.PHONE, 8)                               AS PRIM_PHONE
      , NULL                                                    AS CENTER_ID
      , ctry.NAME                                               AS PRIM_COUNTRY
      , cty1.ABBR                                               AS PRIM_COUNTY
      , NULL                                                    AS PRIM_CTRY_CODE
      , NULL                                                    AS PRIM_EXT
      , NULL                                                    AS PRIM_DEPT_COMMENT
      , NULL                                                    AS CLIN_ADDR1
      , NULL                                                    AS CLIN_ADDR2
      , NULL                                                    AS CLIN_CITY
      , NULL                                                    AS CLIN_STATE
      , NULL                                                    AS CLIN_ZIP
      , NULL                                                    AS CLIN_COUNTRY
      , NULL                                                    AS CLIN_COUNTY
      , NULL                                                    AS CLIN_CTRY_CODE
      , NULL                                                    AS CLIN_PHONE_AC
      , NULL                                                    AS CLIN_PHONE
      , NULL                                                    AS CLIN_EXT
      , NULL                                                    AS CLIN_DEPT_COMMENT
      , NULL                                                    AS CLIN_SUFFIX
      , NULL                                                    AS DEPT_ID
      , NULL                                                    AS ADDRESS_CODE
      , ser2.NPI                                                AS NPI
      , '1'                                                     AS PROVIDER_TYPE
    FROM
        Clarity.dbo.CLARITY_SER                AS ser
        LEFT JOIN Clarity.dbo.CLARITY_SER_ADDR AS prim_addr
          ON prim_addr.PROV_ID = ser.PROV_ID
             AND prim_addr.PRIMARY_ADDR_YN = 'Y'
        LEFT JOIN Clarity.dbo.CLARITY_SER_2    AS ser2
          ON ser2.PROV_ID = ser.PROV_ID
        LEFT JOIN Clarity.dbo.CLARITY_EMP      AS emp
          ON ser.USER_ID = emp.USER_ID
        LEFT JOIN Clarity.dbo.CLARITY_DEP      AS dep
          ON emp.MR_LOGON_DEPT_ID = dep.DEPARTMENT_ID
        LEFT JOIN Clarity.dbo.ZC_STATE         AS st1
          ON st1.STATE_C = prim_addr.STATE_C
        LEFT JOIN Clarity.dbo.ZC_COUNTY        AS cty1
          ON cty1.COUNTY_C = prim_addr.COUNTY_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY       AS ctry
          ON ctry.COUNTRY_C = prim_addr.COUNTRY_C
        LEFT JOIN Clarity.dbo.CLARITY_SER_DEPT AS serdept
          ON ser.PROV_ID = serdept.PROV_ID
             AND serdept.LINE = 1
        LEFT JOIN Clarity.dbo.CLARITY_DEP      AS dep2
          ON serdept.DEPARTMENT_ID = dep2.DEPARTMENT_ID
             AND serdept.LINE = 1
    WHERE
        ser.PROV_ID is not NULL
        -- PROVIDERS SHOULD BE UPDATED WITHIN THE LAST YEAR
        -- FOR HISTORICAL RUNS THIS MAY NEED TO BE EXTENDED        
        AND (SER2.INSTANT_OF_UPDATE_DTTM > DATEADD(yyyy,-1,getdate()) or SER2.INSTANT_OF_UPDATE_DTTM is NULL)
        
    -- Bring in the departments/facilities    
    -- Departments/Facilities are considered to be a "place provider" vs a "people provider"
    UNION
    SELECT DISTINCT
       -- Add a preceding "D" to designate departments/facilities
       -- Eliminates conflict between provider_id and dept_id
       cast(concat('D', dep.DEPARTMENT_ID) AS varchar) AS PROVIDER_ID
      , NULL                       AS PROV_LAST_NAME
      , NULL                       AS PROV_FIRST_NAME
      , NULL                       AS PROV_MIDDLE_NAME
      , NULL                       AS CLINICIAN_TITLE
      , dep.DEPARTMENT_ID          AS PRIM_DEPT_ID
      , coalesce(dep.EXTERNAL_NAME, dep.DEPARTMENT_NAME)          AS DEPARTMENT_NAME
      , addr1.ADDRESS              AS PRIM_ADDR1
      , addr2.ADDRESS              AS PRIM_ADDR2
      , dep2.ADDRESS_CITY          AS PRIM_CITY
      , st.ABBR                    AS PRIM_STATE
      , dep2.ADDRESS_ZIP_CODE      AS PRIM_ZIP
      , LEFT(dep.PHONE_NUMBER, 3)  AS PRIM_PHONE_AC
      , RIGHT(dep.PHONE_NUMBER, 8) AS PRIM_PHONE
      , NULL                       AS CENTER_ID
      , ctry.NAME                  AS PRIM_COUNTRY
      , cty.ABBR                   AS PRIM_COUNTY
      , NULL                       AS PRIM_CTRY_CODE
      , NULL                       AS PRIM_EXT
      , NULL                       AS PRIM_DEPT_COMMENT
      , NULL                       AS CLIN_ADDR1
      , NULL                       AS CLIN_ADDR2
      , NULL                       AS CLIN_CITY
      , NULL                       AS CLIN_STATE
      , NULL                       AS CLIN_ZIP
      , NULL                       AS CLIN_COUNTRY
      , NULL                       AS CLIN_COUNTY
      , NULL                       AS CLIN_CTRY_CODE
      , NULL                       AS CLIN_PHONE_AC
      , NULL                       AS CLIN_PHONE
      , NULL                       AS CLIN_EXT
      , NULL                       AS CLIN_DEPT_COMMENT
      , NULL                       AS CLIN_SUFFIX
      , NULL                       AS DEPT_ID
      , NULL                       AS ADDRESS_CODE
      , NULL                       AS NPI
      , '2'                        AS PROVIDER_TYPE
    FROM
        Clarity.dbo.CLARITY_DEP                    AS dep
        LEFT JOIN Clarity.dbo.CLARITY_DEP_2        AS dep2
          ON dep.DEPARTMENT_ID = dep2.DEPARTMENT_ID
        LEFT JOIN Clarity.dbo.CLARITY_DEP_ADDR     AS addr1
          ON dep.DEPARTMENT_ID = addr1.DEPARTMENT_ID
             AND addr1.LINE = 1
        LEFT JOIN Clarity.dbo.CLARITY_DEP_ADDR     AS addr2
          ON dep.DEPARTMENT_ID = addr1.DEPARTMENT_ID
             AND addr1.LINE = 2
        LEFT JOIN Clarity.dbo.ZC_STATE             AS st
          ON dep2.ADDRESS_STATE_C = st.STATE_C
        LEFT JOIN Clarity.dbo.ZC_COUNTRY           AS ctry
          ON dep2.ADDRESS_COUNTRY_C = ctry.COUNTRY_C
        LEFT JOIN Clarity.dbo.ZC_COUNTY            AS cty
          ON dep2.ADDRESS_COUNTY_C = cty.COUNTY_C
    WHERE 
        dep.DEPARTMENT_ID IS NOT NULL ) All_Providers_And_Depts
         

   SELECT
      *
   FROM
      #X_ESP_XTR_Final_Results_PROV

END;
GO

