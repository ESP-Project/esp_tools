/* Fenway notes to GLFHC: Depending ON the circumstances of treatment, medications for GC/CT may be stored in one of three tables: OBS, Orders, or Medicate (tied to Prescrib).
** This file contains 3 logically separate queries Union-ed together to collate all relevant medications.
** This frequently duplicates a single medication, but DPH has indicated that they can deal with that ON their END.
** No data is reported here for the last 4 columns: route, dose, patient class, or patient status. These default to null in ESP.*/

SET NOCOUNT ON

DECLARE @StartDt date
DECLARE @EndDt date

--SET @StartDt = DATEADD(d,-3,GETDATE())
--SET @EndDt = GETDATE()

SET @StartDt = getdate() - 5
SET @EndDt = getdate()

/*Meds FROM Medicate*/
SELECT pp.pid 'PatientId'
	  ,pp.patientid 'MRN'
	  ,pre.ptid 'OrderId'
	  ,u.doctorfacilityid 'RXProviderId'
	  --,ISNULL(CONVERT(VARCHAR(8), pre.clinicaldate, 112),'') 'OrderDt'
	  ,COALESCE(CONVERT(VARCHAR(8), pre.clinicaldate, 112), CONVERT(VARCHAR(8), med.startdate, 112)) 'OrderDt'
	  ,'' 'OrderStatus'
	  ,REPLACE(REPLACE(med.instructions, CHAR(13), ''), CHAR(10), '') as 'MedDirections' /*Remove erroneous newline characters FROM prescription directions*/
	  ,ISNULL(med.ndcLabProd + med.ndcPackage, '') 'NDC_Code'
	  ,med.description 'RxMedName'
	  ,pre.quantity 'RxQuantity'
	  ,pre.refills 'RxRefills'
	  ,ISNULL(CONVERT(VARCHAR(8), med.startdate, 112),'') 'RxStartDt'
	  ,CASE WHEN med.stopdate = '4700-12-31 00:00:00.000' THEN '' 
			ELSE ISNULL(CONVERT(VARCHAR(8), med.stopdate, 112),'')
	   END 'RxENDDt'
	  ,ISNULL(med.Route,'') 'Route'
	  ,ISNULL(CAST(med.Dose AS VARCHAR),'') 'Dose'

  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.prescrib pre ON pp.pid = pre.pid
  INNER JOIN centricityps.dbo.medicate med ON med.pid = pre.pid AND med.mid = pre.mid
  LEFT JOIN centricityps.dbo.usr u ON u.pvid = pre.pvid
  LEFT JOIN centricityps.dbo.locreg lr ON u.homelocation = lr.locid

 --UNCOMMENT AND REPLACE THE FOLLOWING LINE FOR DAILY PROCESSING
 --WHERE pre.db_updated_date BETWEEN @StartDt AND @EndDt
 --NEXT LINE IS FOR HISTORICAL PROCESSING
   WHERE ((pre.clinicaldate BETWEEN @StartDt AND @EndDt) OR (med.startdate BETWEEN @StartDt AND @EndDt))
   AND pre.change not in (0,4,10) /*Excluded deleted data.*/
   AND med.xid = 1000000000000000000
   --AND PatientStatusMId <> '-903'
   /*Exclude test patients*/            
   AND PatientId NOT LIKE '9999%'


   --AND PatientStatusMId = '-900'
   --AND pp.DoctorId <> 569
   
   
UNION

SELECT pp.pid 'PatientId'
      ,pp.patientid 'MRN'
	  ,obs.obsid 'OrderId'
	  ,CASE WHEN df.DoctorFacilityId = -1002 
	        THEN CAST(dfp.DoctorFacilityId AS VARCHAR)
            ELSE ISNULL(CAST(df.DoctorFacilityId AS VARCHAR),'')
       END 'RXProviderId'
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'OrderDt'
	  ,'' 'OrderStatus'
	  ,'' 'MedDirections'
	  ,'' 'NDC_Code',
	  ,ISNULL(oh.description,'') 'ObsDescription' /*Using Obs Description as Med Name*/
	  ,'' 'RxQuantity'
	  ,'' 'RxRefills'
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'StartDate' /*As these are admin on site use obs date for start and end*/
	  ,ISNULL(CONVERT(VARCHAR(8), obs.obsdate, 112),'') 'EndDate' /*As these are admin on site use obs date for start and end*/
	  ,'' 'Route'
	  ,'' 'Dose'
  FROM centricityps.dbo.patientprofile pp
  INNER JOIN centricityps.dbo.obs obs ON pp.PId = obs.PID
  LEFT JOIN centricityps.dbo.DOCUMENT doc ON obs.SDID = doc.SDID
  LEFT JOIN centricityps.dbo.DoctorFacility df ON doc.USRID = df.pvid
  LEFT JOIN centricityps.dbo.DoctorFacility dfp ON pp.DoctorId = dfp.DoctorFacilityId
  LEFT JOIN centricityps.dbo.obshead oh ON obs.hdid = oh.hdid
  JOIN centricityps.dbo.DOCUMENT d ON obs.SDID = d.DID
            AND d.DOCTYPE in (2,7,8,9,27,28,32,44,45,1631149916156120)

--UNCOMMENT AND REPLACE THE FOLLOWING LINE FOR DAILY PROCESSING
   WHERE ((obs.db_updated_date BETWEEN @StartDt AND @EndDt) or (obs.obsdate BETWEEN @StartDt AND @EndDt))
--NEXT LINE IS FOR HISTORICAL PROCESSING
   --WHERE obs.obsdate BETWEEN @StartDt AND @EndDt
   AND obs.xid = 1000000000000000000
   AND obs.change NOT IN (0,4,10,11,12) /*Excluded deleted data.*/
   /*Exclude recently updated entries for results over 45 days old*/
   --AND obs.obsdate >= dateadd(dd,-30,@StartDt)
   --AND pp.PatientStatusMId <> '-903'
   /*Exclude test patients*/            
   AND pp.PatientId NOT LIKE '9999%'
   AND obs.OBSVALUE NOT LIKE  'Pending'
   AND obs.HDID <> -1
   AND oh.MLCODE IN ('MLI-109296', 'MLI-15579', 'MLI-17215', 'MLI-17216', 'MLI-62498.7', 'MLI-62498.8', 'MLI-25903', 'MLI-62498.10', 'MLI-62498.11', 'MLI-62498.12', 'LOC-2033.0')

	  

   
   
   
 
