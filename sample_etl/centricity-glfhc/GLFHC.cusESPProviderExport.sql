USE [Centricityps]
GO

/****** Object:  StoredProcedure [GLFHC].[cusESPProviderExport]    Script Date: 9/28/2018 9:32:32 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [GLFHC].[cusESPProviderExport]

AS

BEGIN

/* Fenway notes to GLFHC: We report all providers in the practice. The file rarely changes, but it's small so we pull everyone every day.
** No data is reported here for the last 2 columns:department code, address code. These default to null in ESP.*/

SET NOCOUNT ON

SELECT DISTINCT df.doctorfacilityid AS 'natural_key'
	,df.last AS 'last_name'
	,df.first AS 'first_name'
	,ISNULL( substring(df.middle,1,1),'') AS 'middle_name'
	,ISNULL (df.prefix, '') AS 'title'
	,ISNULL (lr.facilityid, '') AS 'dept_natural_key'
	,ISNULL (lr.name, '') AS 'dept'
	,ISNULL (lr.address1, '') AS 'dept_address_1'
	,ISNULL (lr.address2, '') AS 'dept_address_2'
	,ISNULL (lr.city, '') AS 'dept_city'
	,ISNULL (lr.state, '') AS 'dept_state'
	,ISNULL (lr.zip, '') AS 'dept_zip'
	,left(lr.primphone,3) AS 'area_code'
	,substring(lr.primphone,4,3) + '-' + substring(lr.primphone,7,4) AS 'telephone'
	/*Center ID is 1 for Fenway*/
	,1 AS 'center_id'
	,ISNULL(lr.country, '') AS 'dept_country'
	,'' AS 'dept_country_code'
	,'' AS 'tel_country_code'
	,'' AS 'tel_ext' /*Fenway does not use telephone extensions in primary dept numbers.*/
	,'' AS 'call_info' /*Fenway does not record comments on how to contact departments.*/
	,ISNULL(df.address1, '') AS 'clin_address1'
	,ISNULL(df.address2, '') AS 'clin_address2'
	,ISNULL(df.city, '') AS 'clin_city'
	,ISNULL(df.state, '') AS 'clin_state'
	,ISNULL(df.zip, '') AS 'clin_zip'
	,ISNULL (df.country, '') AS 'clin_country'
	,'' AS 'clin_county_code'
	,'' AS 'clin_tel_country_code'
	,left(df.phone1,3) AS 'clin_areacode'
	,substring(df.phone1,4,3) + '-' + substring(df.phone1,7,4) AS 'clin_tel'
	,'' AS 'clin_tel_ext' /*Fenway does not use telephone extensions in clinician phone numbers.*/
	,'' AS 'clin_call_info' /*Fenway does not record comments on how to contact clinicians.*/
	,ISNULL (df.suffix,'') AS 'suffix'
FROM doctorfacility df
LEFT JOIN locreg lr 
	ON df.LocationId = lr.LOCID

END
GO


