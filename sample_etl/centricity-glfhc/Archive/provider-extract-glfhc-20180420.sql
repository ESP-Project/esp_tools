/* Fenway notes to GLFHC: We report all providers in the practice. The file rarely changes, but it's small so we pull everyone every day.
** No data is reported here for the last 2 columns:department code, address code. These default to null in ESP.*/

SET NOCOUNT ON

select distinct doctorfacility.doctorfacilityid as 'Provider ID',
doctorfacility.last as 'Clinician Last Name',
doctorfacility.first as 'Clinician First Name',
ISNULL( substring(doctorfacility.middle,1,1),'') as 'Clinician Middle Initial',
ISNULL (doctorfacility.prefix, '') as 'Clinician Title',
ISNULL (locreg.facilityid, '')as 'Primary Department ID',
locreg.name as 'Primary Department Name',
locreg.address1 as 'Primary Dept address line 1',
locreg.address2 as 'Primary Dept address line 2',
locreg.city as 'Primary Dept city',
locreg.state as 'Primary Dept state',
locreg.zip as 'Primary Dept zip',
left(locreg.primphone,3) as 'Primary Dept phone area code',
substring(locreg.primphone,4,3) + '-' + substring(locreg.primphone,7,4) as 'Primary Dept phone',
/*Center ID is 1 for Fenway*/
1 as Center,
locreg.country as 'Primary Dept country',
'' as dept_country_code,
 ''as tel_country_code,
'' as tel_ext, /*Fenway does not use telephone extensions in primary dept numbers.*/
'' as call_info, /*Fenway does not record comments on how to contact departments.*/
doctorfacility.address1 as 'Clinician Address Line 1',
doctorfacility.address2 as 'Clinician Address Line 2',
doctorfacility.city as 'Clinician City',
doctorfacility.state as 'Clinician State',
doctorfacility.zip as 'Clinician Zip Code',
ISNULL (doctorfacility.country, '') as 'Clinician Country',
'' as 'Clinician County code',
'' as 'Clinician Telephone Country Code',
left(doctorfacility.phone1,3) as 'Clinician Phone Area Code',
substring(doctorfacility.phone1,4,3) + '-' + substring(doctorfacility.phone1,7,4) as 'Clinician Phone number',
'' as 'Clinician Extension', /*Fenway does not use telephone extensions in clinician phone numbers.*/
'' as 'Clinician Call Info/Comment', /*Fenway does not record comments on how to contact clinicians.*/
ISNULL (doctorfacility.suffix,'') as 'Clinician name suffix'

from doctorfacility
left join locreg on DoctorFacility.LocationId = LOCREG.LOCID

where doctorfacility.inactive = 0
and doctorfacility.DEA <>'NULL' --in (1,7,8) /*Include only providers, not other types of staff.*/