USE [Centricityps]
GO

/****** Object:  StoredProcedure [GLFHC].[cusESPMedicationExport]    Script Date: 9/28/2018 9:31:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [GLFHC].[cusESPMedicationExport]

@StartDt as datetime = NULL,
@EndDt datetime = NULL

AS

BEGIN

/* Fenway notes to GLFHC: Depending ON the circumstances of treatment, medications for GC/CT may be stored in one of three tables: OBS, Orders, or Medicate (tied to Prescrib).
** This file contains 3 logically separate queries Union-ed together to collate all relevant medications.
** This frequently duplicates a single medication, but DPH has indicated that they can deal with that ON their END.
** No data is reported here for the last 4 columns: route, dose, patient class, or patient status. These default to null in ESP.*/

SET NOCOUNT ON

IF @StartDt IS NULL
	SET @StartDt = CAST(GETDATE() AS date)
ELSE
	SET @StartDt = CAST(@StartDt AS date)

IF @EndDt IS NULL
	SET @EndDt = CAST(GETDATE() AS date)
ELSE
	SET @EndDt = CAST(@EndDt AS date)

/*Meds FROM Medicate*/
SELECT pp.pid AS 'patient_id'
	  ,pp.patientid AS 'mrn'
	  ,pre.ptid AS 'order_natural_key'
	  ,u.doctorfacilityid AS 'provider_id'
	  ,COALESCE(CONVERT(VARCHAR(8), pre.clinicaldate, 112), CONVERT(VARCHAR(8), med.startdate, 112)) AS 'date'
	  ,'' AS 'status'
	  ,REPLACE(REPLACE(med.instructions, CHAR(13), ''), CHAR(10), '') AS 'directions' /*Remove erroneous newline characters FROM prescription directions*/
	  ,ISNULL(med.ndcLabProd + med.ndcPackage, '') AS 'code'
	  ,med.description AS 'name'
	  ,pre.quantity AS 'quantity'
	  ,pre.refills AS 'refills'
	  ,ISNULL(CONVERT(VARCHAR(8), med.startdate, 112),'') AS 'start_date'
	  ,CASE WHEN med.stopdate = '4700-12-31 00:00:00.000' THEN '' 
			ELSE ISNULL(CONVERT(VARCHAR(8), med.stopdate, 112),'')
	   END AS 'end_date'
	  ,ISNULL(med.Route,'') AS 'route'
	  ,ISNULL(CAST(med.Dose AS VARCHAR),'') AS 'dose'

FROM centricityps.dbo.patientprofile AS pp
INNER JOIN centricityps.dbo.prescrib AS pre 
	ON pp.pid = pre.pid
INNER JOIN centricityps.dbo.medicate AS med 
	ON med.pid = pre.pid AND med.mid = pre.mid
LEFT JOIN centricityps.dbo.usr AS u 
	ON u.pvid = pre.pvid
LEFT JOIN centricityps.dbo.locreg AS lr 
	ON u.homelocation = lr.locid

--UNCOMMENT AND REPLACE THE FOLLOWING LINE FOR DAILY PROCESSING
WHERE ((pre.db_updated_date BETWEEN @StartDt AND @EndDt) OR (med.startdate BETWEEN @StartDt AND @EndDt))
--NEXT LINE IS FOR HISTORICAL PROCESSING
--WHERE ((pre.clinicaldate BETWEEN @StartDt AND @EndDt) OR (med.startdate BETWEEN @StartDt AND @EndDt))
	/*Excluded deleted data.*/
	AND pre.change not in (0,4,10) 
	AND med.xid = 1000000000000000000
	/*Exclude test patients*/            
	AND PatientId NOT LIKE '9999%'
	/*NULL pid or patientid will cause ESP load to fail */
	AND pp.pid IS NOT NULL
	AND pp.PatientId IS NOT NULL

END
GO


