USE [Centricityps]
GO

/****** Object:  StoredProcedure [GLFHC].[cusESPMemberExport]    Script Date: 9/28/2018 9:32:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [GLFHC].[cusESPMemberExport]

@StartDt as datetime = NULL,
@EndDt datetime = NULL

AS

BEGIN

SET NOCOUNT ON

IF @StartDt IS NULL
	SET @StartDt = CAST(GETDATE() AS date)
ELSE
	SET @StartDt = CAST(@StartDt AS date)

IF @EndDt IS NULL
	SET @EndDt = CAST(GETDATE() AS date)
ELSE
	SET @EndDt = CAST(@EndDt AS date)

SELECT DISTINCT pp.PId AS 'natural_key'
	  ,pp.PatientId AS 'mrn'
	  ,ISNULL(pp.Last,'') AS 'last_name'
	  ,ISNULL(pp.First,'') AS 'first_name'
	  ,ISNULL(pp.Middle,'') AS 'middle_name'
	  ,ISNULL(pp.Address1,'') AS 'address1'
	  ,ISNULL (pp.Address2,'') AS 'address2'
	  ,ISNULL(pp.City,'') AS 'city'
	  ,ISNULL(pp.State,'') AS 'state'
	  -- Replace carriage returns in zip codes
      ,ISNULL (replace(replace(replace(pp.zip, CHAR(13), ''), CHAR(10), ''),char(9),''), '') AS 'zip'
	  ,ISNULL (pp.Country,'') AS 'country'
	  ,CASE WHEN pp.phone1 = '9999999999' THEN ''
			ELSE ISNULL(left(pp.phone1,3),'') 
	   END AS 'areacode'
	  ,CASE WHEN pp.phone1 = '9999999999' THEN ''
			ELSE ISNULL(substring(pp.phone1,4,3) + '-' + substring(pp.phone1,7,4),'') 
	   END AS 'tel'
	  ,'' AS 'tel_ext'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.birthdate, 112),'') AS 'date_of_birth'
	  ,ISNULL(pp.Sex,'') AS 'gender'
	  ,ISNULL(re.Race,'') AS 'race'
	  ,ISNULL(re.PatientPrefLanguage,'') AS 'home_language'
	  ,ISNULL(substring(pp.ssn,1,3) + '-' + substring(pp.ssn,4,2) + '-' + substring(pp.ssn,6,4),'') AS 'ssn'
	  ,ISNULL(pp.DoctorId,'') AS 'pcp_id'
	  ,ISNULL (ml.Description,'') AS 'marital_stat'
	  ,'' AS 'religion'
	  ,ISNULL(pp.Nickname,'') AS 'aliases'
	  ,'' AS 'mother_mrn'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.DeathDate, 112),'') AS 'date_of_death'
	  ,ISNULL(pp.FacilityId,'') AS 'center_id'
	  ,CASE WHEN re.UDSEthnicityCode = 'H' THEN 'Hispanic'
            WHEN re.UDSEthnicityCode = 'N' THEN 'Non Hispanic'
            WHEN re.UDSEthnicityCode = 'NR' THEN 'Not Reported'
			ELSE ''
       END AS 'ethnicity'
	  ,ISNULL(pp.MothersMaiden, '') AS 'mother_maiden_name'
	  ,ISNULL(CONVERT(VARCHAR(8), pp.lastmodified, 112),'') AS 'last_update'
      ,ISNULL(pp.FacilityId,'') AS 'last_update_site'
	  ,ISNULL(pp.Suffix,'') AS 'suffix'
	  ,'' AS 'title'
	  ,'' AS 'remark'
	  ,'' AS 'income_level'
	  ,'' AS 'housing_status'
	  ,'' AS 'insurance_status'
	  ,'' AS 'birth_country'

 FROM centricityps.dbo.PatientProfile pp
 LEFT JOIN centricityps.dbo.Appointments a 
	ON pp.PatientProfileId = a.OwnerId
    AND a.ApptStart <= @EndDt AND a.ApptStart >= @StartDt
    AND a.Canceled is null
    AND a.ApptStart = (SELECT MAX(b.ApptStart)
                       FROM centricityps.dbo.Appointments b
                       WHERE a.OwnerId = b.OwnerId
           		           AND b.ApptStart <= @EndDt AND b.ApptStart >= @StartDt
                           AND b.Canceled IS NULL
                       GROUP BY b.OwnerId)
 LEFT JOIN centricityps.dbo.cusUDSPatientRaceEthnicity_2017 re 
	ON pp.PatientProfileId = re.PatientProfileID
 LEFT OUTER JOIN centricityps.dbo.MedLists ml 
	ON pp.MaritalStatusMId = ml.MedListsId
 LEFT JOIN centricityps.dbo.obs obs 
	ON pp.PId = obs.PID AND obs.obsdate BETWEEN @StartDt AND @EndDt
 LEFT JOIN centricityps.dbo.prescrib pre 
	ON pp.PId = pre.PID AND pre.clinicaldate BETWEEN @StartDt AND @EndDt
 WHERE pp.PatientId NOT LIKE '9999%'
 -- bad patient records to be excluded
	AND pp.PId NOT IN ('1765191591195540')

 END
GO


