#!/usr/bin/python
"""
Name: esp_sql.py

Purpose:  Container for SQL commands.

Modifications:
    17-Apr-2020 Created.  MC
	29-May-2020 Update queries. MC
"""

import esp_gd

SELECT_ESP_Patient_TABLE="""
SELECT	Demographic.GPID AS natural_key,
		Demographic.GPID AS mrn,
		NULL AS last_name,
		NULL AS first_name,
		NULL AS middle_name,
		NULL AS address1,
		NULL AS address2,
		NULL AS city,
		Lds_Address_History.ADDRESS_STATE AS state,
		Lds_Address_History.ADDRESS_ZIP5 AS zip,
		NULL AS country,
		NULL AS areacode,
		NULL AS tel,
		NULL AS tel_ext,
		FORMAT(Demographic.BIRTH_DATE, 'yyyyMMdd') AS date_of_birth,
		LEFT(Demographic.SEX, 1) AS gender,
		CASE 
			WHEN Demographic.RACE = '01' THEN 'American Indian or Alaska Native'
			WHEN Demographic.RACE = '02' THEN 'Asian'
			WHEN Demographic.RACE = '03' THEN 'Black or African American'
			WHEN Demographic.RACE = '04' THEN 'Native Hawaiian or Other Pacific Islander'
			WHEN Demographic.RACE = '05' THEN 'White'
			WHEN Demographic.RACE = '06' THEN 'Multiple race'
			WHEN Demographic.RACE = '07' THEN 'Refuse to answer'
			WHEN Demographic.RACE = 'NI' THEN 'No Information'
			WHEN Demographic.RACE = 'UN' THEN 'Unknown'
			WHEN Demographic.RACE = 'OT' THEN 'Other'
		END AS race,
		NULL AS home_language,
		NULL AS ssn,
		NULL AS pcp_id,
		NULL AS marital_stat,
		NULL AS religion,
		NULL AS aliases,
		NULL AS mother_mrn,
		NULL AS date_of_death,
		NULL AS center_id,
		LEFT(Demographic.HISPANIC, 1) AS ethnicity
FROM REACHNET.DEMOGRAPHIC Demographic
OUTER APPLY (
	SELECT TOP(1) lah.ADDRESS_STATE, lah.ADDRESS_ZIP5 
	FROM REACHNET.LDS_ADDRESS_HISTORY lah
	WHERE Demographic.GPID = lah.GPID
	AND (lah.ADDRESS_PERIOD_END IS NULL OR lah.ADDRESS_PREFERRED = 'Y')
) AS Lds_Address_History
WHERE Demographic.GPID IN (
	SELECT GPID
	FROM REACHNET.ENCOUNTER
	WHERE ADMIT_DATE < %s AND ADMIT_DATE >= %s
	UNION
	SELECT GPID
	FROM REACHNET.PRESCRIBING
	WHERE RX_ORDER_DATE < %s AND RX_ORDER_DATE >= %s
	UNION
	SELECT GPID
	FROM REACHNET.LAB_RESULT_CM
	WHERE RESULT_DATE < %s AND RESULT_DATE >= %s
	UNION
	SELECT GPID
	FROM REACHNET.VITAL
	WHERE MEASURE_DATE < %s AND MEASURE_DATE >= %s
	UNION
	SELECT GPID
	FROM REACHNET.IMMUNIZATION
	WHERE VX_RECORD_DATE < %s AND VX_RECORD_DATE >= %s)
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today, esp_gd.yesterday)

ESP_Patient_DICT= {'SELECT':SELECT_ESP_Patient_TABLE}

# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Visit_TABLE="""
SELECT	DISTINCT Encounter.GPID AS patient_id,
		Encounter.GPID AS mrn,
		CONCAT(Encounter.ENCOUNTERID, '-', Vital.VITALID) AS natural_key,
		FORMAT(Encounter.ADMIT_DATE, 'yyyyMMdd') AS date,
		NULL AS is_closed,
		NULL AS date_closed,
		NULL AS provider_id,
		NULL AS site_natural_key,
		NULL AS site_name,
		Encounter.ENC_TYPE AS raw_encounter_type,
		NULL AS edd,
		NULL AS raw_temperature,
		NULL AS cpt,
		Vital.WT AS raw_weight,
		Vital.HT AS raw_height,
		Vital.SYSTOLIC AS raw_bp_systolic,
		Vital.DIASTOLIC AS raw_bp_diastolic,
		NULL AS raw_o2_stat,
		NULL AS raw_peak_flow,
		(SELECT
				CASE
					WHEN dx.DX_TYPE = '10' THEN 'icd10:' + dx.DX
					WHEN dx.DX_TYPE = '09' THEN 'icd9:' + dx.DX
					ELSE NULL
				END + ';'
			FROM REACHNET.DIAGNOSIS dx
			WHERE dx.ENCOUNTERID = Diagnosis.ENCOUNTERID
			FOR XML PATH('')) AS dx_code_id,
		CAST(Vital.ORIGINAL_BMI AS DECIMAL(15,4)) AS raw_bmi,
		NULL AS hosp_admit_dt,
		NULL AS nosp_dschrg_dt,
		Encounter.RAW_PAYER_NAME_PRIMARY AS primary_payer
FROM REACHNET.ENCOUNTER Encounter
LEFT OUTER JOIN REACHNET.VITAL Vital ON Encounter.GPID = Vital.GPID AND Encounter.ENCOUNTERID = Vital.ENCOUNTERID
LEFT OUTER JOIN REACHNET.DIAGNOSIS Diagnosis ON Encounter.GPID = Diagnosis.GPID AND Encounter.ENCOUNTERID = Diagnosis.ENCOUNTERID
WHERE Encounter.ADMIT_DATE < %s AND Encounter.ADMIT_DATE >= %s
"""  % (esp_gd.today, esp_gd.yesterday)

ESP_Visit_DICT= {'SELECT':SELECT_ESP_Visit_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Immune_TABLE="""
SELECT	Immunization.GPID AS patient_id,
		NULL AS imm_type,
		Immunization.RAW_VX_NAME AS name,
		FORMAT(Immunization.VX_ADMIN_DATE, 'yyyyMMdd') AS date
FROM REACHNET.IMMUNIZATION Immunization
WHERE Immunization.VX_RECORD_DATE < %s AND Immunization.VX_RECORD_DATE >= %s
"""  % (esp_gd.today, esp_gd.yesterday)

ESP_Immune_DICT= {'SELECT':SELECT_ESP_Immune_TABLE}


SELECT_ESP_Social_Hx_TABLE="""
SELECT	Vital.GPID AS patient_id,
		Vital.GPID AS mrn,
		CASE
			WHEN Vital.TOBACCO = '01' THEN 'Current user'
			WHEN Vital.TOBACCO = '02' THEN 'Never'
			WHEN Vital.TOBACCO = '03' THEN 'Quit/former user'
			WHEN Vital.TOBACCO = '04' THEN 'Passive or environmental'
			WHEN Vital.TOBACCO = '06' THEN 'Not asked'
			WHEN Vital.TOBACCO = 'NI' THEN 'No information'
			WHEN Vital.TOBACCO = 'UN' THEN 'Unknown'
			WHEN Vital.TOBACCO = 'OT' THEN 'Other'
		END AS tobacco_use,
		NULL AS alcohol_use,
		FORMAT(Vital.MEASURE_DATE, 'yyyyMMdd') AS date_noted,
		Vital.VITALID AS natural_key
FROM REACHNET.VITAL Vital
WHERE Vital.MEASURE_DATE < %s AND Vital.MEASURE_DATE >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Social_Hx_DICT= {'SELECT':SELECT_ESP_Social_Hx_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Test_Results_TABLE="""
SELECT	Lab_Result_Cm.GPID AS patient_id,
		Lab_Result_Cm.GPID AS mrn,
		NULL AS order_natural_key,
		FORMAT(Lab_Result_Cm.LAB_ORDER_DATE, 'yyyyMMdd') AS date,
		FORMAT(Lab_Result_Cm.RESULT_DATE, 'yyyyMMdd') AS result_date,
		NULL AS provider_id,
		NULL AS order_type,
		Lab_Result_Cm.LAB_PX AS cpt_native_code,
		Lab_Result_Cm.RAW_LAB_CODE AS component_native_code,
		Lab_Result_Cm.RAW_LAB_NAME AS native_name,
		Lab_Result_Cm.RAW_RESULT AS result_string,
		Lab_Result_Cm.ABN_IND AS abnormal_flag,
		Lab_Result_Cm.NORM_RANGE_LOW AS ref_low_string,
		Lab_Result_Cm.NORM_RANGE_HIGH AS ref_high_string
FROM REACHNET.LAB_RESULT_CM Lab_Result_Cm
WHERE Lab_Result_Cm.RESULT_DATE < %s AND Lab_Result_Cm.RESULT_DATE >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Test_Results_DICT= {'SELECT':SELECT_ESP_Test_Results_TABLE}


# Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Meds_TABLE="""
SELECT	Prescribing.PATID AS patient_id,
		Prescribing.PATID AS mrn,
		Prescribing.PRESCRIBINGID AS order_natural_key,
		NULL AS provider_id,
		FORMAT(Prescribing.RX_ORDER_DATE, 'yyyyMMdd') AS date,
		NULL AS status,
		NULL AS directions,
		NULL as code,
		Prescribing.RAW_RX_MED_NAME AS name,
		Prescribing.RX_QUANTITY AS quantity,
		Prescribing.RX_REFILLS AS refills,
		FORMAT(Prescribing.RX_START_DATE, 'yyyyMMdd') AS start_date,
		FORMAT(Prescribing.RX_END_DATE, 'yyyyMMdd') AS end_date,
		Prescribing.RX_ROUTE AS route,
		CONCAT(Prescribing.RX_DOSE_ORDERED, ' ', Prescribing.RX_DOSE_ORDERED_UNIT) AS dose
FROM REACHNET.PRESCRIBING Prescribing
WHERE Prescribing.RX_ORDER_DATE < %s AND Prescribing.RX_ORDER_DATE >= %s
""" % (esp_gd.today, esp_gd.yesterday)

ESP_Meds_DICT= {'SELECT':SELECT_ESP_Meds_TABLE}


if __name__ == '__main__':
    pass
