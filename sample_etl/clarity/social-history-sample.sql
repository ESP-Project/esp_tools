SELECT pat.Pat_ID,
       pat.Pat_MRN_ID,
       zc.TITLE as TOBACCO_USER,
       zca.title as IS_Alcohol_user,
       convert(varchar, Pat_enc.contact_date,112) as date_noted,
       coalesce(sh.HX_LNK_ENC_CSN, sh.pat_enc_csn_id) as natural_key,
       pat_enc.VISIT_PROV_ID as provider_ID,
       case when sh.FEMALE_PARTNER_YN = 'Y' and sh.MALE_PARTNER_YN = 'Y' then 'MALE + FEMALE'
            when sh.FEMALE_PARTNER_YN = 'Y' then 'FEMALE'
            when sh.MALE_PARTNER_YN = 'Y' then 'MALE' end as gender_of_sex_partners,
       sh.ALCOHOL_OZ_PER_WK as alcohol_oz_per_week,
       zcill.title as illegal_drug_use,
       zcsexa.title as sexually_active,
       LTRIM(concat (
            (case when condom_yn = 'Y' then ' CONDOM +' else null end),
            (case when pill_yn = 'Y' then ' PILL +' else null end),
            (case when DIAPHRAGM_YN = 'Y' then ' DIAPHRAGM +' else null end),
            (case when IUD_YN = 'Y' then ' IUD +' else null end),
            (case when SURGICAL_YN = 'Y' then ' SURGICAL +' else null end),
            (case when SPERMICIDE_YN = 'Y' then ' SPERMICIDE +' else null end),
            (case when IMPLANT_YN = 'Y' then ' IMPLANT +' else null end),
            (case when RHYTHM_YN = 'Y' then ' RHYTHM +' else null end),
            (case when INJECTION_YN = 'Y' then ' INJECTION +' else null end),
            (case when SPONGE_YN = 'Y' then ' SPONGE_YN +' else null end),
            (case when INSERTS_YN = 'Y' then ' INSERTS +' else null end),
            (case when ABSTINENCE_YN = 'Y' then ' ABSTINENCE +' else null end))) as birth_control_method
FROM Patient pat
             left join Social_Hx sh on pat.Pat_ID = sh.Pat_ID
             inner join PAT_ENC on sh.pat_ENC_CSN_ID = pat_enc.PAT_ENC_CSN_ID
             left join ZC_TOBACCO_USER zc on sh.TOBACCO_USER_C=zc.TOBACCO_USER_C
             left join ZC_OB_GPTPAL zca on sh.ALCOHOL_USE_C=zca.OB_GPTPAL_C
             left join ZC_ILL_DRUG_USER zcill on sh.ILL_DRUG_USER_C = zcill.ILL_DRUG_USER_C
             left join ZC_SEXUALLY_ACTIVE zcsexa on sh.SEXUALLY_ACTIVE_C = zcsexa.SEXUALLY_ACTIVE_C
WHERE coalesce(zc.TITLE, zca.title, sh.MALE_PARTNER_YN, sh.FEMALE_PARTNER_YN) is not null
AND pat_enc.Contact_Date < %s and pat_enc.contact_date >= %s;