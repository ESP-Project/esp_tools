--CREATE_ESP_Meds_TABLE="""
SELECT 	pat.Pat_ID, 
     	pat.Pat_MRN_ID,
        Order_Med_ID,
        Med_Presc_Prov_ID,
        isnull (Ordering_Date, Order_Inst) as Ordering_Date,
        zc.Title as Order_Status_Title,
        cl.Name as Order_Med_Name,
        cast (Null as nvarchar)as NDC_Code,
        Sig,       
        Quantity,
        Refills,
        Start_Date,
        End_Date,
        cl.Route,
        CAST (null as nvarchar) as Dose,
        case 
          when med.order_class_c is not null then 
            convert(varchar,ordering_mode_c) + '.' + convert(varchar,med.order_class_c)
          else convert(varchar,ordering_mode_c) 
        end as patient_class
INTO clarity_production_temp.dbo.X_ESP_Meds
FROM clarity_production_temp.dbo.X_ESP_Patient pat inner join Order_Med med on pat.Pat_ID = med.Pat_ID	
		inner join zc_Order_Status zc on med.Order_Status_C = zc.Order_Status_C
		inner join Clarity_Medication cl on med.Medication_ID = cl.Medication_ID
		inner join ZC_ORDER_CLASS zco on med.ORDER_CLASS_C =zco.ORDER_CLASS_C
WHERE UPDATE_DATE < %s and UPDATE_DATE >= %s and zc.order_Status_c <>4 and zco.ORDER_CLASS_C <>3
""" % (esp_gd.today, esp_gd.yesterday \
       ,esp_gd.today)

--UPDATE_ESP_Meds_NDC_Code="""	
UPDATE clarity_production_temp.dbo.X_ESP_Meds
SET  NDC_Code	= rx.NDC_Code 
FROM Rx_Med_NDC_Code rx  inner join Order_Med med on med.Medication_ID = rx.Medication_ID
WHERE LINE = 1
"""

# Casts are needed because linux pymssql returns decimal - windows verison does not.
--SELECT_ESP_Meds_TABLE="""
SELECT  Pat_ID,
        Pat_MRN_ID,
        cast(Order_Med_ID as int),
        Med_Presc_Prov_ID,
        Ordering_Date,
        Order_Status_Title,
        Sig,
        NDC_Code,
        Order_Med_Name,
        Quantity,
        Refills,
        Start_Date,
        End_Date,
        Route,
        dose,
        patient_class
FROM clarity_production_temp.dbo.X_ESP_Meds
"""