SELECT  Distinct vis.VISIT_PROV_ID as provider_id_num,
        LEFT(ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME + ',') -1) as last_name,
        LEFT(
                SUBSTRING (ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME)+2, LEN(PROV_NAME)),
                CHARINDEX(' ',
                SUBSTRING (ser.PROV_NAME, CHARINDEX(',', ser.PROV_NAME)+2, LEN(PROV_NAME))
                        + ' ')) as first_name,
        dbo.fnSplitName(ser.PROV_NAME,'Middle') as middle_name,
        ser.CLINICIAN_TITLE as title,
        serdept.DEPARTMENT_ID as department_id,
        coalesce(dept.EXTERNAL_NAME, dept.DEPARTMENT_NAME) as department_name,
        seraddr.ADDR_LINE_1 as dept_address_1,
        seraddr.ADDR_LINE_2 as dept_address_2,
        seraddr.CITY as dept_city,
        case when seraddr.STATE_C = 22 then 'MA' else NULL end as dept_state,
        seraddr.zip as dept_zip,
        isnull (SUBSTRING(ser.OFFICE_PHONE_NUM,1,3), '617') as office_area,
        isnull (SUBSTRING(ser.OFFICE_PHONE_NUM,5,9) , '665-1000')as office_phone,
        cast (Null as varchar) as centerid,
        null as dept_country,
        null dept_county_code,
        null tel_country_code,
        null as tel_ext,
        null as call_info,
        null as clin_address1,
        null as clin_address2,
        null as clin_CITY,
        null as clin_state,
        null as clin_zip,
        null as clin_country,
        null as clin_county_code,
        null as clin_tel_country_code,
        null as clin_areacode,
        null as clin_tel,
        null as clin_tel_ext,
        null as clin_call_info,
        null as suffix,
        null as dept_addr_type,
        null as clin_addr_type,
        ser2.npi as npi,
        1 as provider_type
FROM INSERT_YOUR_TABLE_NAME_HERE vis
left join Clarity_SER ser on vis.VISIT_PROV_ID = ser.Prov_ID
left join Clarity_SER_2 ser2 on vis.VISIT_PROV_ID = ser2.Prov_ID
left join (select PROV_ID, ADDR_LINE_1, ADDR_LINE_2, CITY, STATE_C, zip from CLARITY_SER_ADDR where line = 1) seraddr on vis.VISIT_PROV_ID = seraddr.Prov_ID
left join CLARITY_SER_DEPT serdept on vis.VISIT_PROV_ID = serdept.Prov_ID
left join CLARITY_DEP dept on dept.DEPARTMENT_ID = serdept.DEPARTMENT_ID
WHERE
vis.VISIT_PROV_ID is not NULL
--physicians may serve at mulitple locations we just one the first one
AND (serdept.LINE is null or serdept.LINE = 1)

UNION

-- ADD IN DEPARTMENTS AS FACILITY PROVIDERS
SELECT  Distinct dep.DEPARTMENT_ID as provider_id_num,
        null as last_name,
        null as first_name,
        null as middle_name,
        null as title,
        dep.DEPARTMENT_ID as department_id,
        coalesce(dep.EXTERNAL_NAME, dep.DEPARTMENT_NAME) as department_name,
        addr1.ADDRESS as dept_address_1,
        addr2.ADDRESS as dept_address_2,
        dep2.ADDRESS_CITY as dept_city,
        case when dep2.ADDRESS_STATE_C = 22 then 'MA' else NULL end as dept_state,
        dep2.ADDRESS_ZIP_CODE as dept_zip,
        isnull (SUBSTRING(dep.PHONE_NUMBER,1,3), '617') as office_area,
        isnull (SUBSTRING(dep.PHONE_NUMBER,5,9) , '665-1000')as office_phone,
        cast (Null as varchar) as centerid,
        null as dept_country,
        null dept_county_code,
        null tel_country_code,
        null as tel_ext,
        null as call_info,
        null as clin_address1,
        null as clin_address2,
        null as clin_CITY,
        null as clin_state,
        null as clin_zip,
        null as clin_country,
        null as clin_county_code,
        null as clin_tel_country_code,
        null as clin_areacode,
        null as clin_tel,
        null as clin_tel_ext,
        null as clin_call_info,
        null as suffix,
        null as dept_addr_type,
        null as clin_addr_type,
        null as npi,
        2 as provider_type
FROM CLARITY_DEP dep
left join CLARITY_DEP_2 dep2 on dep2.DEPARTMENT_ID = dep.DEPARTMENT_ID
left join (select department_id, address from CLARITY_DEP_ADDR where line = 1) addr1 ON addr1.department_id = dep.DEPARTMENT_ID
left join (select department_id, address from CLARITY_DEP_ADDR where line = 2) addr2 ON addr2.DEPARTMENT_ID = dep.DEPARTMENT_ID
WHERE
Dep.DEPARTMENT_ID is not NULL;
