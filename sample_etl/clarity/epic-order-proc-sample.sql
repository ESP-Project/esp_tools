-- Note: Column order doesn't match spec.  It matches load_epic.py order.
CREATE_ESP_OrderProcs_TABLE=
SELECT distinct
    pat.Pat_ID,
    pat.Pat_MRN_ID,
    pro.Order_Proc_ID,
    EAP.PROC_CODE,
    cast (NULL as nvarchar) Modifier,
    cast (NULL as nvarchar) as specimen_id /*Health accession number no CHA value*/,
    pro.Ordering_Date,
    ord_type.TITLE as order_type,
    pro.Authrzing_Prov_ID,
    pro.Description as procedure_name,
    zc.Title as specimen_source,
    ord_status.TITLE as order_status,
    cast (NULL as nvarchar) as patient_class,
    cast (NULL as nvarchar) as patient_status,
    pro.PAT_ENC_CSN_ID as encounter_id,
    cast (NULL as nvarchar) as reason_code,
    ord_priority.TITLE as order_priority,
    cast (NULL as nvarchar) as clinical_info,
    pro.PROC_START_TIME,
    pro.PROC_ENDING_TIME
INTO clarity_production_temp.dbo.X_ESP_OrderProcs
FROM Patient pat
        inner join clarity.dbo.Order_Proc pro on pat.Pat_ID = pro.Pat_ID
        left join clarity.dbo.order_proc_2 op2 on pro.order_proc_id =op2.order_proc_id
        left join clarity.dbo.ZC_Specimen_Source zc on pro.Specimen_Source_C = zc.Specimen_Source_C
        left join Clarity.dbo.CLARITY_EAP EAP ON PRO.PROC_ID=EAP.PROC_ID
        left join clarity.dbo.ZC_ORDER_TYPE ord_type on pro.order_type_c = ord_type.order_type_c
        left join clarity.dbo.ZC_ORDER_STATUS ord_status on pro.order_status_c = ord_status.order_status_c
        left join clarity.dbo.ZC_ORDER_PRIORITY ord_priority on pro.ORDER_PRIORITY_C = ord_priority.ORDER_PRIORITY_C
WHERE
-- this clause is designed to eliminate duplicate order rows
((pro.INSTANTIATED_TIME is not null and pro.future_or_stand is null) or pro.result_time is not null)
-- if the order status is null then lab status cannot be null
-- if order status is not null it cannot be cancelled
and  (  (pro.ORDER_STATUS_C is null and pro.LAB_STATUS_C is not null) or (pro.ORDER_STATUS_C <> 4) ) --cancelled
and pro.order_type_c in (
        1,         -- Procedures
        5,         -- Radiology
        6,         -- Immunization/Injection
        7,         -- Lab
        10,        -- NURSING
        14,        -- Blood Bank
        16,        -- Microbiology
        21,        -- Respiratory Care
        26,        -- Point of Care Testing
        41,        -- PR Charge
        60,        -- Nursing Transfusion
        67,        -- Point of Care External Testing
        1005,      -- CPVL - PULMONARY PROCEDURES
        1007,      -- CPVL - VASCULAR PROCEDURES
        1014       -- Cytology
        )
and pro.ordering_date <= %s and pro.ordering_DATE >= dateadd(DAY,-5,%s);
--variable substitution % (esp_gd.today, esp_gd.yesterday)


-- Casts are needed because linux pymssql returns decimal - windows verison does not.
-- To create the file in the proper order.
SELECT_ESP_OrderProcs_TABLE=
SELECT  pat_id,
        pat_mrn_id,
        cast(Order_Proc_ID as int), --natural_key
        PROC_CODE,
        Modifier,
        specimen_id,
        Ordering_Date,
        order_type,
        Authrzing_Prov_ID,
        procedure_name,
        specimen_source,
        order_status,
        patient_class,
        patient_status,
        encounter_id,
        reason_code,
        order_priority,
        clinical_info,
        proc_start_time,
        proc_ending_time
FROM clarity_production_temp.dbo.X_ESP_OrderProcs;


