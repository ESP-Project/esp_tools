--CREATE_ESP_Visit_TABLE="""
SELECT DISTINCT enc.Pat_ID,
        pat.Pat_MRN_ID,
        enc.Pat_Enc_CSN_ID,
        enc.Contact_Date,
        enc.Enc_Closed_YN,
        enc.Enc_Close_Date,
        Visit_Prov_ID,
        enc.Department_ID,
        cast (Null as varchar(255)) as Department_Name,
        Enc_Type_C,
        CASE
          WHEN hsp.IP_EPISODE_ID is null THEN CONCAT('O-', enc_type.title)
          WHEN hsp.IP_EPISODE_ID is not null THEN CONCAT('I-', enc_type.title)
          ELSE CONCAT('U-', enc_type.title)
        END Enc_Type_Name,  /*Flag for inpatient outpatient visit combined with encounter type*/
        pat.EDD_DT,
        Temperature,
        Los_Proc_Code,
        Left(Cast((CONVERT(NUMERIC(8,2), (weight*.0625))) as varchar ), 3) +' lb' + ' ' +  cast(convert(numeric(3,2),right( (CONVERT(NUMERIC(8,2), (weight*.0625))), 3)) * 16 as varchar )+  ' oz'  AS WEIGHT,
	LEFT(height,(CHARINDEX('"',HEIGHT))) as Height,
        BP_Systolic,
        BP_Diastolic,
        enc2.PHYS_SPO2 as O2_Stat,
        enc.SITE_PEAK_FLOW as peak_flow,
        dx.dx_list as ICD9,
        bmi
INTO clarity_production_temp.dbo.X_ESP_Visit	
FROM PAT_ENC enc 
left join PATIENT pat on enc.Pat_ID = pat.Pat_ID  
left join PAT_ENC_2 enc2 on enc.Pat_enc_csn_id = enc2.Pat_enc_csn_id
left join clarity.dbo.pat_enc_hsp hsp on enc.pat_enc_csn_id=hsp.pat_enc_csn_id  
left join ZC_DISP_ENC_TYPE enc_type on enc.enc_type_c = enc_type.internal_id
left outer join (
                 select t1.pat_enc_csn_id,
                    (select codwthtxt + '; '
                     FROM
                           ( select dx.pat_enc_csn_id,
                             case when edg10.code is not null then 'icd10:' + edg10.code
                                  when edg9.code is not null then 'icd9:' + edg9.code
                                  else null
                                  end AS codwthtxt
                             FROM PAT_ENC_DX dx
                             left join EDG_CURRENT_ICD10 edg10 on edg10.dx_id = dx.dx_id
                             left join EDG_CURRENT_ICD9 edg9 on edg9.dx_id = dx.dx_id
                                )  as t2
                     WHERE t2.pat_enc_csn_id=t1.pat_enc_csn_id
                     GROUP BY codwthtxt
                     ORDER BY codwthtxt
                     FOR XML PATH('') ) as dx_list
                FROM PAT_ENC_DX as t1
                GROUP BY pat_enc_csn_id
                ) dx  on dx.Pat_enc_csn_id = enc.Pat_enc_csn_id
WHERE enc.Contact_Date < %s and enc.Contact_Date >= %s
"""  % (esp_gd.today, esp_gd.yesterday)

UPDATE_ESP_Visit_Department_Name="""
UPDATE clarity_production_temp.dbo.X_ESP_Visit	
SET Department_Name = dic.Department_Name
FROM clarity_production_temp.dbo.X_ESP_visit esp inner join Clarity_Dep dic on esp.Department_ID = dic.Department_ID
"""

SELECT_ESP_Visit_TABLE="""
SELECT  Pat_ID, 
        Pat_MRN_ID,
        cast(Pat_Enc_CSN_ID as int),
        Contact_Date,
        Enc_Closed_YN,
        Enc_Close_Date,
        Visit_Prov_ID,
        cast(Department_ID as int),
        Department_Name,
        Enc_Type_Name,
        EDD_DT, 
        Temperature,
        Los_Proc_Code,
        rtrim(Weight),
        rtrim(Height),
        BP_Systolic,
        BP_Diastolic,
        O2_Stat,
        peak_flow,
        cast(replace(replace(Icd9,'&lt;' ,'<'),'&gt;' ,'>') as text) as Icd9,
        bmi
FROM clarity_production_temp.dbo.X_ESP_Visit
"""