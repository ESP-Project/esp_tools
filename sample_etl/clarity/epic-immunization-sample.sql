CREATE_ESP_Immune_TABLE=
SELECT  pat.Pat_id,
        imm.Immunzatn_id,
        dic.Name  as Immunization_Name,
        Immune_Date,
        Dose,
        imm.MFG_C,
        mfg.Name as MFG_Name,
        Lot,
        Immune_ID,
        patient.PAT_MRN_ID as mrn,
        t.PROV_ID as provider_id,
        cast (Null as varchar(255)) as encounter_date,
        imm.IMMNZTN_STATUS_C
INTO clarity_production_temp.dbo.X_ESP_Immune
FROM  clarity_production_temp.dbo.X_ESP_Patient pat inner join Immune imm on pat.Pat_ID = imm.Pat_ID
                inner join Clarity_Immunzatn dic on imm.Immunzatn_ID = dic.Immunzatn_ID
                inner join ZC_MFG mfg on imm.MFG_C = mfg.MFG_C
                left join (Select User_id, prov_id from Clarity_EMP where PROV_ID is not null) t  on ENTRY_USER_ID=t.USER_ID
                left join PATIENT on pat.Pat_ID = PATIENT.PAT_ID
WHERE imm.entry_date >= %s or imm.update_date >= %s;
--variable substituion % (esp_gd.yesterday, esp_gd.yesterday)


--Casts are needed because linux pymssql returns decimal - windows verison does not.
SELECT_ESP_Immune_TABLE=
SELECT  pat_id,
        cast(Immunzatn_id as int),
        Immunization_Name,
        Immune_Date,
        Dose,
        MFG_Name,
        Lot,
        cast(Immune_ID as int),
        cast (mrn as varchar(255)) as mrn,
        cast (provider_id as varchar(255)) as provider_id,
        cast (encounter_date as varchar(255)) as encounter_date,
        cast(immnztn_status_c as int)
FROM clarity_production_temp.dbo.X_ESP_Immune;
