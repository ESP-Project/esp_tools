#!/bin/bash

##
## Set Local Parameters Here
##
DATA_DIR=/srv/esp/data
ESP_DIR=/srv/esp/prod
LOG_DIR=/srv/esp/logs
DB_NAME=esp
MAX_SQL_PROC=15
ESP_CUSTOM_SCRIPTS_DIR=/srv/esp/scripts
# The dupe_pat_script is not applicable for most sites. Do not set path if not used.
DUPE_PAT_SCRIPT=/path/to/dupe-patient-clean-up.sql

#define the specific conditions for nodis **OR** comment out to run nodis for all conditions
NODIS_NOTIFIABLE_CONDITIONS=(gonorrhea chlamydia)


## Generally the following parameters will be correct by default, but should be verified and confirmed
runtime=`date -u +%Y-%m-%dT%H:%M:%S`
LOGFILE=$LOG_DIR/daily_cron.log.$runtime
ESP_SCRIPT=$ESP_DIR/bin/esp
SQL_PROC_QUERY="select count(*) from pg_stat_activity where state<>'idle' and datname='$DB_NAME'"
SQL_STATUS_SET="update nodis_case set status = 'Q-OLD-CASE-REVIEW' where created_timestamp >= now() - INTERVAL '2 days' and created_timestamp::date - date > 183 and status = 'Q'"
SQL_NONAME_STATUS_SET="UPDATE nodis_case T1 SET status = concat(status, '-MISSING PAT_NAME') FROM emr_patient T2 WHERE T1.patient_id = T2.id AND (first_name is null or last_name is null) AND status in ('Q', 'RQ')"
CLIN_ENC_CHECK_SQL="select 1 as success from pg_stat_all_tables where schemaname = 'gen_pop_tools' and relname = 'clin_enc' and last_analyze >= now() - INTERVAL '30 hours'"
INCOMING_DIR=$DATA_DIR/epic/incoming
INCOMING_FILES=$INCOMING_DIR/epic*
INCOMING_MEM_FILES=$INCOMING_DIR/epicmem*
INCOMING_VIS_FILES=$INCOMING_DIR/epicvis*
INCOMING_PRO_FILES=$INCOMING_DIR/epicpro*
INCOMING_MED_FILES=$INCOMING_DIR/epicmed*
INCOMING_LAB_FILES=$INCOMING_DIR/epicres*
ARCHIVE_DIR=$DATA_DIR/epic/archive


# This file will be created by the script
ALL_HEF_FILE=$ESP_CUSTOM_SCRIPTS_DIR/all_hef.txt
# This file will be created by the script
SUB_HEF_FILE=$ESP_CUSTOM_SCRIPTS_DIR/sub_hef.txt
# This file will be created by the script **if** has not already been created and configured
NOTIFIABLE_HEF_FILE=$ESP_CUSTOM_SCRIPTS_DIR/queued_conditions_hef.txt

## When reporting is split between notifiable and non-notifiable or testing conditions
## Define them here. Corresponds with "run_hef_and_nodis_for_nonprod_conditions" function.
LOWER_MAX_SQL_PROC=10
NODIS_OTHER_CONDITIONS=(insert conditions here)


##
## MDPHnet/Riskscape scripts, files, and parameters (not applicable for most sites)
##
OPIOID_SCRIPT=/srv/esp/esp_tools/opioid-daily.sql
OBESITY_SCRIPT=/srv/esp/esp_tools/obesity-daily.sql
PREG_HEF_FILE=$ESP_CUSTOM_SCRIPTS_DIR/preg_hef.txt
NO_PREG_HEF_FILE=$ESP_CUSTOM_SCRIPTS_DIR/no_preg_hef.txt
UPDATE_MDPHNET_SCRIPT=/path/to/update_mdphnet.sh




# FUNCTIONS
set_date_variables() {
    if [ $# = 1 ]
    then
        DATE=`date -d $1 +%Y%m%d`
        DAY_OF_WEEK=`date -d $1 +%A`
    else
        DATE=`date -d "-1 day" +%Y%m%d`
        DAY_OF_WEEK=`date -d "-1 day" +%A`
    fi
}


# Modify this function with code to pull the extract files as needed by the site
get_extract_files() {
    $ESP_SCRIPT download_sftp
}


core_file_check() {
    # Check that we have 1 of all the core file types that we expect
    if [ ! -f $INCOMING_LAB_FILES ] || [ ! -f $INCOMING_MEM_FILES ] || [ ! -f $INCOMING_VIS_FILES ] || [ ! -f $INCOMING_PRO_FILES ] || [ ! -f $INCOMING_MED_FILES ] ; then
        echo "ERROR: A core extract file (res/mem/vis/pro/med) was not found in the incoming dir. Please check for a missing file. Here are the files obtained: `ls -C $INCOMING_DIR` "
    fi

    # Do a quick row count test on the lab results file if there is one
    if [ -n "$(ls -A $INCOMING_LAB_FILES 2>/dev/null)" ]; then
       (for lab_file in $(ls -1tr $INCOMING_LAB_FILES | sort -g); do
           ROW_COUNT=`grep -c ^ $lab_file`
               if [ $ROW_COUNT -lt 1000 ]
               then
                   echo "ERROR: LOW ROW COUNT DETECTED: $lab_file has a row count of: $ROW_COUNT Please investigate for potential problems."
               fi
       done
       wait)
    fi
}

preprocess_lab_files() {
    # This is an example only and must be customixed by site.
    # For labs with mixed log and copies results within the same native code, overwrite the natvive code to include the reference unit.
    if [ "$(ls -A $INCOMING_LAB_FILES)" ]; then
          (for lab_file in $(ls -1tr $INCOMING_LAB_FILES | sort -g); do
               echo "Pre-Processing $lab_file file for LOG Results..."
               source_name="${lab_file##*/}"
               #echo "Source Name is $source_name"

               already_loaded=$(psql $DB_NAME -c "SELECT true from emr_provenance where source='$source_name'" -t | tr -d ' \n')

               #echo "The value of already_loaded is $already_loaded"

               if [ "$already_loaded" != "t" ];
               then
                    echo "File $source_name has NOT been previously loaded. Updating the native code to include the Log Reference Unit"
                    awk -i inplace -v INPLACE_SUFFIX=.bak -F '^' -v OFS='^' '$15 ~ /LOG CPS\/ML/ || /Log IU\/mL/ || /LOG IU\/ML/ || /logIU\/mL/ || /Log cps\/mL/ || /log IU\mL/ { $9 = $9 " "  $15 }1' $lab_file
                    mv $lab_file.bak $ARCHIVE_DIR

                    # update for one pesky test that has a number of results without a ref_unit(these are all log)
                    # The copies values do have ref_unit
                    awk -i inplace -v INPLACE_SUFFIX=.bak2 -F '^' -v OFS='^' '$15 ~ /Copies\/mL/ || /copies\/mL/ || /COPIES\/ML/ && $8 == "HIV-1 RNA, QUANTITATIVE, PCR, SERUM OR PLASMA" && $9 == "HIV 1 RNA PCR" { $9 = $9 " "  $15 }1' $lab_file
                    mv $lab_file.bak2 $ARCHIVE_DIR
               fi


            done
            wait)
    fi
}

# REMOVE ALL MEDS PRIOR TO START DATE
pre_process_meds() {
    if [ "$(ls -A $INCOMING_MED_FILES)" ]; then
          (for med_file in $(ls -1tr $INCOMING_MED_FILES | sort -g); do
               echo "Pre-Processing $med_file file..."
               /usr/local/bin/awk -i inplace -v INPLACE_SUFFIX=.bak -F '^' -v start='20221008' '$5>start' $med_file
               mv $med_file.bak $ARCHIVE_DIR
           done
           wait)
    fi
}




load_epic() {
    if [ -n "$(ls -A $INCOMING_FILES 2>/dev/null)" ]; then
        set -e
       (
        ## Load Provider Files First
        if [ -n "$(ls -A $INCOMING_PRO_FILES 2>/dev/null)" ]; then
           (for pro_file in $(ls -1tr $INCOMING_PRO_FILES | sort -g); do
               echo "Processing $pro_file file..."
               # load_epic
               ( $ESP_SCRIPT load_epic --file $pro_file )
               done
               wait)
        fi
        ## Load Patient Files Second
        if [ -n "$(ls -A $INCOMING_MEM_FILES 2>/dev/null)" ]; then
           ( for patient_file in $(ls -1tr $INCOMING_MEM_FILES | sort -g); do
               echo "Processing $patient_file file..."
               # load_epic
               ( $ESP_SCRIPT load_epic --file $patient_file )
               done
               wait)
        fi

        ## Load Visit Files Third
        if [ -n "$(ls -A $INCOMING_VIS_FILES 2>/dev/null)" ]; then
           ( for visit_file in $(ls -1tr $INCOMING_VIS_FILES | sort -g); do
               echo "Processing $visit_file file..."
               # load_epic
               ( $ESP_SCRIPT load_epic --file $visit_file )
               done
               wait)
        fi


        ## Load All Other Files
        if [ -n "$(ls -A $INCOMING_FILES --ignore="epicmem*" --ignore="epicpro*" --ignore="epicvis*" 2>/dev/null)" ]; then
           ( for other_file in $(ls -1tr $INCOMING_FILES --ignore="epicmem*" --ignore="epicpro*" --ignore="epicvis*" | sort -g); do
               wait
               echo "Processing $other_file file..."
               # load_epic
               ( $ESP_SCRIPT load_epic --file $other_file )
               done
               wait)
        fi
       )
    fi
}

merge_patients() {
    echo "Merge / Clean Up Duplicate Patients Before Creating New Events and Cases..."
    DUPE_PAT_UPDATE=$(psql esp -f $DUPE_PAT_SCRIPT)
}

build_unmapped_labs() {
    echo "Starting concordance run..."
    $ESP_SCRIPT concordance
}


run_hef_for_notifiable_conditions() {

    #Create a text file with all hef heuristics that are installed
    $ESP_SCRIPT hef --list > $ALL_HEF_FILE

    # If a queued conditions file does not exist create one that contains everything.
    # Maintain the file by manually adding hef events to this file for priority processing of notifiable conditions
    if [ ! -f "$NOTIFIABLE_HEF_FILE" ]; then
       $ESP_SCRIPT hef --list > $NOTIFIABLE_HEF_FILE
    fi

    #if the all and queued files are different create a sub_hef.txt file
    cmp --silent $ALL_HEF_FILE $NOTIFIABLE_HEF_FILE  || grep -v -x -f $NOTIFIABLE_HEF_FILE $ALL_HEF_FILE > $SUB_HEF_FILE

    ( while read line
      do
        sleep 1
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        echo 'Now running HEF heuristic:'
        echo $line
        ( $ESP_SCRIPT hef "$line" ) &
      done < $NOTIFIABLE_HEF_FILE
      wait )
}

run_nodis_for_notifiable_conditions() {
    if [ ${#NODIS_NOTIFIABLE_CONDITIONS[@]} -eq 0 ]; then
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run for all conditions"
        ( $ESP_SCRIPT nodis )
    else ( for nodisval in "${NODIS_NOTIFIABLE_CONDITIONS[@]}"; do
        ## If the database is running many processes, wait until the load drops before proceeding
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run for $nodisval"
        ( $ESP_SCRIPT nodis $nodisval ) &
          done
          wait )
    fi
}

run_requeue() {
    $ESP_SCRIPT case_requeue --status=RS
    $ESP_SCRIPT case_requeue --status=S

}

prevent_old_case_sending() {
   # Merged patients and older data entering the extracts can cause old cases to be created. 
   # Update these cases with a special status so they can be reviewed and then fake transmitted
   # or set to not be sent. This applies only to status Q where cases are more
   # than 6 months old and created in the last 2 days
   echo "Setting Status for Old Cases..."
   sql_status_set_for_merge=$(psql $DB_NAME -c "$SQL_STATUS_SET" -t | tr -d ' \n')
}


prevent_noname_case_sending() {
   # If a case is linked to a patient without a first name or last name
   # generate an error and update the case status so it can be reviewed
   echo "Setting Status for No Name Cases..."
   sql_status_set_for_no_name=$(psql $DB_NAME -c "$SQL_NONAME_STATUS_SET" -t | tr -d ' \n')
   if [ $sql_status_set_for_no_name != "UPDATE0" ]
   then
      echo "ERROR: A case without a patient name has been prevented from sending to DPH. Resolve missing patient information and update the status of the case. The status has been set to RQ-MISSING PAT_NAME or Q-MISSING_PAT_NAME."
   else
      echo "No cases with missing names were identified."
   fi
}



run_case_report() {
    $ESP_SCRIPT case_report --mdph --transmit --status 'Q'
    $ESP_SCRIPT case_report --mdph --transmit --status 'RQ'
}

send_status_report() {
    echo "Starting status_report..."
    $ESP_SCRIPT status_report --send-mail
}


update_opioid() {
    #update esp_condition table for opioid project
    OPIOID_UPDATE=$(psql $DB_NAME -f $OPIOID_SCRIPT)
}

update_obesity() {
    #update esp condition table for obesity conditions
    OBESITY_UPDATE=$(psql $DB_NAME -f $OBESITY_SCRIPT)
}

run_ili_reports() {
    ## Run the weekly ILI reports on the day we proces the "Sunday" and "Saturday" data extract.
    if [ $DAY_OF_WEEK = "Sunday" -o $DAY_OF_WEEK = "Saturday" ] ; then
        echo 'ili_report.sh submitted'
        $ESP_CUSTOM_SCRIPTS_DIR/ili_report.sh $DATE
        echo 'ili_report.sh completed'
    fi
}


run_hef_and_nodis_for_nonprod_conditions() {
    # If a preg_hef file does not exist create one that contains no heuristics
    if [ ! -f "$PREG_HEF_FILE" ]; then
        touch $PREG_HEF_FILE
    fi

    #now we pull the preg heuristics out of the larger list
    grep -v -x -f $PREG_HEF_FILE $SUB_HEF_FILE > $NO_PREG_HEF_FILE

    (set -e
    (
    flock -e -w 10 201 || return 0

    ( while read line
      do
        sleep 1
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$LOWER_MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        echo 'Now running HEF heuristic:'
        echo $line
        ( $ESP_SCRIPT hef "$line" ) &
      done < $NO_PREG_HEF_FILE
      wait ) && \
    if [ ${#NODIS_OTHER_CONDITIONS[@]} -eq 0 ]; then
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$LOWER_MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run for all conditions"
        ( $ESP_SCRIPT nodis )
    else ( for nodisval in "${NODIS_OTHER_CONDITIONS[@]}"; do
        ## If the database is running many processes, wait until the load drops before proceeding
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "$LOWER_MAX_SQL_PROC" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run for $nodisval"
        ( $ESP_SCRIPT nodis $nodisval ) &
          done
          wait )
    fi
    ) 201>./OTHER_HEF_NODIS_LCK..esp )
}

run_pregnancy_hef_timespan() {
    (set -e
    (
      flock -e -w 10 202 || return 0
      # Run preg heuristics
      while read line ; do
          echo 'Now running HEF heuristic:'
          echo $line
          $ESP_SCRIPT hef "$line"
        done < $PREG_HEF_FILE
    ) 202>./PREG_HEF_LCK..esp)
}

run_diag_hypert_nodis() {
    (set -e
    (
      flock -e -w 10 203 || return 0
      # Run diagnosed hypertension, which takes a while...
      $ESP_SCRIPT nodis diagnosedhypertension
    ) 203>./DIAG_HT_NODIS_LCK..esp)
}

update_mdphnet() {
    (set -e
    (
      flock -e -w 10 204 || return 0
      # Run weekly mdphnet rebuild, but skip it if a previously submitted update is still running
      $UPDATE_MDPHNET_SCRIPT
    ) 204>./MDPHNET_LCK..esp)
}

generate_clinencs() {
    #generate/update clinical encounters table
    (set -e
    (
      flock -e -w 10 205 || return 0
      $CLIN_ENCS_SCRIPT
      echo "RUNNING CLIN_ENC_SCRIPT"
    ) 205>./CLINENCS_LCK..esp)
}

check_clin_enc_analyze_time() {
     clin_enc_check=$(psql $DB_NAME -c "$CLIN_ENC_CHECK_SQL" -t | tr -d ' \n')
     if [ "$clin_enc_check" != 1 ] ; then
        echo "ERROR: clin_enc table has not been analyzed in 30 hours. Please check to see if there is an issue with clinical encounter table creation."
     else
        echo "SUCCESS: Clinical Encounters table was analyzed less than 30 hours ago."
     fi
}



##
## PROCESSING STARTS HERE
##

exec 5>&1 6>&2 >>$LOGFILE 2>&1
echo "starting script"
set_date_variables
check_clin_enc_analyze_time
get_extract_files
core_file_check
#preprocess_lab_files
#pre_process_meds
load_epic
#merge_patients
build_unmapped_labs
run_hef_for_notifiable_conditions
run_nodis_for_notifiable_conditions
run_requeue
prevent_old_case_sending
#prevent_noname_case_sending
run_case_report
send_status_report
generate_clinencs

echo "Processing for notifiable conditions completed at `date`"

##
## The following functions only apply to specific sites!!
## These should remain commented out unless explicitly instructed to modify
##

#update_opioid
#update_obesity
#run_ili_reports
#run_hef_and_nodis_for_nonprod_conditions
#run_pregnancy_hef_timespan
#run_diag_hypert_nodis
#update_mdphnet

echo "Full Daily Batch Processing Completed At `date`"

exec 1>&5 2>&6