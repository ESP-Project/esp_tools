
-- 1. Gender
select count(*), gender 
from emr_patient
group by gender
order by gender;

-- 2. Marital Status
select count(*), marital_stat
from emr_patient
group by marital_stat
order by marital_stat;

-- 3. Alcohol Use
select count(*), alcohol_use
from emr_socialhistory
group by alcohol_use
order by alcohol_use;

-- 4. Sex Partner Gender
select count(*), sex_partner_gender
from emr_socialhistory
group by sex_partner_gender
order by sex_partner_gender;


-- 5. Sexual Orientation
select count(*), sex_orientation
from emr_patient
group by sex_orientation
order by sex_orientation;

-- 6. Sexually Active
select count(*), sexually_active
from emr_socialhistory
group by sexually_active
order by sexually_active;

-- 7. BCM
select count(*), birth_control_method
from emr_socialhistory
group by birth_control_method
order by birth_control_method;


