#!/bin/bash

DATA_DIR=/srv/esp/data
ESP_DIR=/srv/esp/prod
INCOMING_FILES=$DATA_DIR/epic/incoming
PROVIDER_FILES=$DATA_DIR/epic/incoming/epicpro*
PATIENT_FILES=$DATA_DIR/epic/incoming/epicmem*
VISIT_FILES=$DATA_DIR/epic/incoming/epicvis*
ESP_SCRIPT=$ESP_DIR/bin/esp
LOGFILE=/srv/esp/logs/daily_cron.log.$$
DB_NAME=esp
SQL_PROC_QUERY="select count(*) from pg_stat_activity"

HEF_CONDITIONS=(labresult:gonorrhea:positive labresult:chlamydia:positive)
NODIS_CONDITIONS=(gonorrhea chlamydia)
#CASE_CONDITIONS=(gonorrhea chlamydia)

exec 5>&1 6>&2 >>$LOGFILE 2>&1
echo "starting script"


if [ "$(ls -A $INCOMING_FILES)" ]; then
    set -e
   (
    ## Load Provider Files First
    if [ "$(ls -A $PROVIDER_FILES)" ]; then
       (for pro_file in $(ls -1tr $PROVIDER_FILES | sort -g); do
	       echo "Removing Double Quotes From File"
		   sed -i 's/\"//g' $pro_file
		   wait
           echo "Processing $pro_file file..."
           # load_epic
           ( $ESP_SCRIPT load_epic --file $pro_file )
           done
           wait)
    fi
    ## Load Patient Files Second
    if [ "$(ls -A $PATIENT_FILES)" ]; then
       ( for patient_file in $(ls -1tr $PATIENT_FILES | sort -g); do
	       echo "Removing Double Quotes From File"
		   sed -i 's/\"//g' $patient_file
		   wait
           echo "Processing $patient_file file..."
           # load_epic
           ( $ESP_SCRIPT load_epic --file $patient_file )
           done
           wait)
    fi

    ## Load Visit Files Third 
    if [ "$(ls -A $VISIT_FILES)" ]; then
       ( for visit_file in $(ls -1tr $VISIT_FILES | sort -g); do
	   	   echo "Removing Double Quotes From File"
		   sed -i 's/\"//g' $visit_file
		   wait
	       echo "Processing $visit_file file..."
           # load_epic
           ( $ESP_SCRIPT load_epic --file $visit_file )
           done
           wait)
    fi


    ## Load All Other Files
    if [ "$(ls -A $INCOMING_FILES --ignore="epicmem*" --ignore="epicpro*" --ignore="epicvis*")" ]; then
       ( for other_file in $(ls -1tr $INCOMING_FILES --ignore="epicmem*" --ignore="epicpro*" --ignore="epicvis*" | sort -g); do
	       echo "Removing Double Quotes From File"
		   sed -i 's/\"//g' $other_file
		   wait
           echo "Processing $other_file file..."
           #load_epic
           ( $ESP_SCRIPT load_epic --file $INCOMING_FILES/$other_file )
           done
           wait)
    fi
   )
fi 

    echo "Starting concordance run..."
    $ESP_SCRIPT concordance

    ## If specific hef conditions are not defined run hef for all
    if [ ${#HEF_CONDITIONS[@]} -eq 0 ]; then
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute hef
        echo "Starting hef run..."
        ( $ESP_SCRIPT hef )
    else ( for hefval in "${HEF_CONDITIONS[@]}"; do
        ## If the database is running many processes, wait until the load drops before proceeding
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute hef
        echo "Starting hef run..."
        ( $ESP_SCRIPT hef $hefval ) &
           done
           wait )
    fi
    wait && \
    ## If specific hef conditions are not defined run nodis for all
    if [ ${#NODIS_CONDITIONS[@]} -eq 0 ]; then
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run..."
        ( $ESP_SCRIPT nodis )
    else ( for nodisval in "${NODIS_CONDITIONS[@]}"; do
        ## If the database is running many processes, wait until the load drops before proceeding
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute nodis
        echo "Starting nodis run..."
        ( $ESP_SCRIPT nodis $nodisval ) &
          done
          wait )
    fi
    wait && \
    ## Run case_requeue for status of sent and re-sent
    sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
    ## Execute case_requeue
    echo "Starting case_requeue for status = SENT"
    $ESP_SCRIPT case_requeue --status=S
    wait && \
    sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
    ## Execute case_requeue
    echo "Starting case_requeue for status = RESENT"
    $ESP_SCRIPT case_requeue --status=RS
    wait && \
    ## If specific case conditions are not defined run case_report for all
    if [ ${#CASE_CONDITIONS[@]} -eq 0 ]; then
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute case_report
        echo "Starting case_report..."
        ( $ESP_SCRIPT case_report --status=Q --mdph --transmit )
        wait
        ( $ESP_SCRIPT case_report --status=RQ --mdph --transmit )
    else ( for caseval in "${CASE_CONDITIONS[@]}"; do
        ## If the database is running many processes, wait until the load drops before proceeding
        sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        while [ "$sqlProc" -ge "15" ]; do
            sleep 5
            sqlProc=$(psql $DB_NAME -c "$SQL_PROC_QUERY" -t | tr -d ' \n')
        done
        ## Execute case_report
        echo "Starting case_report..."
        ( $ESP_SCRIPT case_report $caseval --status=Q --mdph --transmit )
        wait
        ( $ESP_SCRIPT case_report $caseval --status=RQ --mdph --transmit ) &
          done
          wait )
    fi
    wait && \
    echo "Starting status_report..."
    $ESP_SCRIPT status_report --send-mail 
