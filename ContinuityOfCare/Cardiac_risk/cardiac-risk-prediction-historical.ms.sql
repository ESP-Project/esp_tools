--set client_min_messages to error;
IF object_id('gen_pop_tools.cr_hist_coefficients', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_coefficients;
GO
SELECT 
CAST(-29.799 AS REAL) f_ln_age,
CAST(17.114 AS REAL) f_b_ln_age,
CAST(12.344 AS REAL) m_ln_age,
CAST(2.469 AS REAL) m_b_ln_age,
CAST(4.884 AS REAL) f_ln_age2,
CAST(13.54 AS REAL) f_ln_chol,
CAST(0.94 AS REAL) f_b_ln_chol,
CAST(11.853 AS REAL) m_ln_chol,
CAST(0.302 AS REAL) m_b_ln_chol,
CAST(-3.114 AS REAL) f_ln_ageXchol,
CAST(-2.664 AS REAL) m_ln_ageXchol,
CAST(-13.578 AS REAL) f_ln_hdl_C,
CAST(-18.92 AS REAL) f_b_ln_hdl_C,
CAST(-7.99 AS REAL) m_ln_hdl_C,
CAST(-0.307 AS REAL) m_b_ln_hdl_C,
CAST(3.149 AS REAL) f_ln_ageXhdl_C,
CAST(4.475 AS REAL) f_b_ln_ageXhdl_C,
CAST(1.769 AS REAL) m_ln_ageXhdl_C,
CAST(2.019 AS REAL) f_ln_tr_sys_bp,
CAST(29.291 AS REAL) f_b_ln_tr_sys_bp,
CAST(1.797 AS REAL) m_ln_tr_sys_bp,
CAST(1.916 AS REAL) m_b_ln_tr_sys_bp,
CAST(-6.432 AS REAL) f_b_ln_ageXtr_sys_bp,
CAST(1.957 AS REAL) f_ln_u_sys_bp,
CAST(27.82 AS REAL) f_b_ln_u_sys_bp,
CAST(1.764 AS REAL) m_ln_u_sys_bp,
CAST(1.809 AS REAL) m_b_ln_u_sys_bp,
CAST(-6.087 AS REAL) f_b_ln_ageXu_sys_bp,
CAST(7.574 AS REAL) f_cur_smk,
CAST(0.691 AS REAL) f_b_cur_smk,
CAST(7.837 AS REAL) m_cur_smk,
CAST(0.549 AS REAL) m_b_cur_smk,
CAST(-1.665 AS REAL) f_cur_smkXln_age,
CAST(-1.795 AS REAL) m_cur_smkXln_age,
CAST(0.661 AS REAL) f_diab,
CAST(0.874 AS REAL) f_b_diab,
CAST(0.658 AS REAL) m_diab,
CAST(0.645 AS REAL) m_b_diab,
CAST(0.9665 AS REAL) f_bsline,
CAST(0.9533 AS REAL) f_b_bsline,
CAST(0.9144 AS REAL) m_bsline,
CAST(0.8954 AS REAL) m_b_bsline
INTO gen_pop_tools.cr_hist_coefficients;
GO

IF object_id('gen_pop_tools.cr_hist_icd', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_icd;
GO
CREATE TABLE gen_pop_tools.cr_hist_icd ( dx_code_id varchar(20) );
insert into gen_pop_tools.cr_hist_icd (dx_code_id)
  values ('icd9:410%'),
         ('icd9:411%'),
         ('icd9:412%'),
         ('icd9:413%'),
         ('icd9:414.0%'),
         ('icd9:414.2%'),
         ('icd9:414.3%'),
         ('icd9:414.8%'),
         ('icd9:414.9%'),
         ('icd9:433%'),
         ('icd9:434%'),
         ('icd9:437.0%'),
         ('icd9:437.1%'),
         ('icd9:437.9%'),
         ('icd9:438%'),
         ('icd9:440%'),
         ('icd9:443,9%'),
         ('icd10:I20%'),
         ('icd10:I21%'),
         ('icd10:I22%'),
         ('icd10:I23%'),
         ('icd10:I24%'),
         ('icd10:I25%'),
         ('icd10:I63%'),
         ('icd10:I65%'),
         ('icd10:I66%'),
         ('icd10:I67.2%'),
         ('icd10:I67.8%'),
         ('icd10:I69.3%'),
         ('icd10:I69.4%'),
         ('icd10:I70%'),
         ('icd10:I73.9%');
GO

IF object_id('gen_pop_tools.cr_hist_antihypertensives', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_antihypertensives;
GO
CREATE TABLE gen_pop_tools.cr_hist_antihypertensives ( generic_name varchar(100) );
insert into gen_pop_tools.cr_hist_antihypertensives (generic_name)
  values ('hydrochlorothiazide'),
         ('chlorthalidone'),
         ('indapamide'),
         ('amlodipine'),
         ('clevidipine'),
         ('felodipine'),
         ('isradipine'),
         ('nicardipine'),
         ('nifedipine'),
         ('nisoldipine'),
         ('diltiazem'),
         ('verapamil'),
         ('acebutolol'),
         ('atenolol'),
         ('betaxolol'),
         ('bisoprolol'),
         ('carvedilol'),
         ('labetalol'),
         ('metoprolol'),
         ('nadolol'),
         ('nebivolol'),
         ('pindolol'),
         ('propranolol'),
         ('benazepril'),
         ('catopril'),
         ('enalapril'),
         ('fosinopril'),
         ('lisinopril'),
         ('moexipril'),
         ('perindopril'),
         ('quinapril'),
         ('ramipril'),
         ('trandolapril'),
         ('candesartan'),
         ('eprosartan'),
         ('irbesartan'),
         ('losartan'),
         ('olmesartan'),
         ('telmisartan'),
         ('valsartan'),
         ('clonidine'),
         ('doxazosin'),
         ('guanfacine'),
         ('methyldopa'),
         ('prazosin'),
         ('terazosin'),
         ('eplerenone'),
         ('spironolactone'),
         ('aliskiren'),
         ('hydralazine');       
GO

--here are patients who can be scored
IF object_id('gen_pop_tools.cr_hist_index_patients', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_index_patients;
GO
SELECT distinct 
       t1.id as patient_id,
       t1.date_of_birth,
       case when upper(substring(t1.gender,1,1)) = 'F' then 'Female'
         when upper(substring(t1.gender,1,1)) = 'M' then 'Male'
       end sex,
       case 
         when (select mapped_value from gen_pop_tools.rs_conf_mapping t00 
		       where t00.src_table='emr_patient' and
		          t00.src_field='race' and t00.src_value=t1.race and t00.mapped_value='BLACK') is not null
           then 'BLACK'
         else 'OTHER'
       end race
INTO gen_pop_tools.cr_hist_index_patients
FROM dbo.emr_patient t1
WHERE upper(substring(t1.gender,1,1)) in ('M', 'F')
AND t1.last_name not in ('TEST', 'TEST**')
AND lower(t1.last_name) NOT LIKE '%ssmctest%' 
AND lower(t1.last_name) NOT LIKE '% test%' 
AND t1.last_name NOT LIKE 'XB%' 
AND t1.last_name NOT LIKE 'XX%';
GO
alter table gen_pop_tools.cr_hist_index_patients alter column patient_id int NOT NULL;
alter table gen_pop_tools.cr_hist_index_patients add primary key (patient_id);
GO
UPDATE STATISTICS gen_pop_tools.cr_hist_index_patients;
GO

--these are the patients with cvdisease diagnosis encounters
IF object_id('gen_pop_tools.cr_dx_encs', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_dx_encs;
select enc.patient_id, gen_pop_tools.dateToYyyyMm(min(enc.date)) as year_month
INTO gen_pop_tools.cr_dx_encs
from dbo.emr_encounter enc
join dbo.emr_encounter_dx_codes dx on dx.encounter_id=enc.id
join gen_pop_tools.cr_hist_icd crdx on dx.dx_code_id like crdx.dx_code_id
group by enc.patient_id;
GO
alter table gen_pop_tools.cr_dx_encs alter column patient_id int NOT NULL;
alter table gen_pop_tools.cr_dx_encs alter column year_month varchar(10) NOT NULL;
GO
alter table gen_pop_tools.cr_dx_encs add primary key (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.cr_dx_encs;
GO

--these are the qualifying encounters for risk assessment
IF object_id('gen_pop_tools.cr_enc_pat', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_enc_pat;
GO
select enc.patient_id, 
       gen_pop_tools.dateToYyyyMm(enc.date) as year_month
INTO gen_pop_tools.cr_enc_pat
from dbo.emr_encounter enc 
JOIN gen_pop_tools.clin_enc t2 on t2.patient_id=enc.patient_id and t2.date=enc.date and t2.source='enc'
where not exists (select null from gen_pop_tools.cr_dx_encs dxe 
                     where dxe.patient_id=enc.patient_id 
                     and gen_pop_tools.dateToYyyyMm(enc.date)>=dxe.year_month)
group by enc.patient_id, gen_pop_tools.dateToYyyyMm(enc.date);
GO
alter table gen_pop_tools.cr_enc_pat alter column patient_id int not null;
alter table gen_pop_tools.cr_enc_pat alter column year_month varchar(10) not null;
GO
alter table gen_pop_tools.cr_enc_pat add primary key (patient_id, year_month);
UPDATE STATISTICS gen_pop_tools.cr_enc_pat;
GO

-- Gather up all of the cr lab tests
IF object_id('gen_pop_tools.cr_hist_labs', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_labs;
GO
SELECT * 
INTO gen_pop_tools.cr_hist_labs
FROM (SELECT t1.patient_id, t2.test_name, gen_pop_tools.dateToYyyyMm(t1.date) year_month, 
       t1.result_float as result, 
	   row_number() over (partition by t1.patient_id, t2.test_name, gen_pop_tools.dateToYyyyMm(t1.date) 
	     order by t1.patient_id, t2.test_name, gen_pop_tools.dateToYyyyMm(t1.date)) rownum
FROM dbo.emr_labresult t1 
INNER JOIN conf_labtestmap t2 ON ((t1.native_code = t2.native_code)) 
WHERE test_name in ('cholesterol-total', 'cholesterol-hdl')
and t1.result_float>0
AND t1.date >= CAST('01-01-2017' AS DATE)) t3
WHERE rownum = 1;
GO

IF object_id('gen_pop_tools.cr_hist_tchol', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_tchol;
GO
select t0.patient_id, t0.year_month, t1.result
INTO gen_pop_tools.cr_hist_tchol
from gen_pop_tools.tt_pat_seq t0 
left join (select * from gen_pop_tools.cr_hist_labs where test_name='cholesterol-total') 
     t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
GO
alter table gen_pop_tools.cr_hist_tchol alter column patient_id int NOT NULL;
alter table gen_pop_tools.cr_hist_tchol alter column year_month varchar(10) NOT NULL;
GO
alter table gen_pop_tools.cr_hist_tchol add primary key (patient_id, year_month);
GO
CREATE INDEX cr_hist_tchol_idx
  ON gen_pop_tools.cr_hist_tchol (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.cr_hist_tchol;
GO
--downfill total chol 23 months
exec gen_pop_tools.downfill_dub 'gen_pop_tools.cr_hist_tchol',
                              'patient_id',
                              'year_month',
                              'result',
							  '23';
GO

IF object_id('gen_pop_tools.cr_hist_hdlc', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_hdlc;
GO
select t0.patient_id, t0.year_month, t1.result
INTO gen_pop_tools.cr_hist_hdlc
from gen_pop_tools.tt_pat_seq t0 
left join (select * from gen_pop_tools.cr_hist_labs where test_name='cholesterol-hdl') 
     t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
GO
alter table gen_pop_tools.cr_hist_hdlc alter column patient_id int NOT NULL;
alter table gen_pop_tools.cr_hist_hdlc alter column year_month varchar(10) NOT NULL;
GO
alter table gen_pop_tools.cr_hist_hdlc add primary key (patient_id, year_month);
GO
CREATE INDEX cr_hist_hdlc_idx
  ON gen_pop_tools.cr_hist_hdlc
  (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.cr_hist_hdlc;
GO

--downfil hdl chol 23 months
exec gen_pop_tools.downfill_dub 'gen_pop_tools.cr_hist_hdlc',
                                'patient_id',
                                'year_month',
                                'result',
								'23';
GO

--Find the anterhypertensives
IF object_id('gen_pop_tools.cr_hist_antihypert_rx', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_antihypert_rx;
GO
SELECT distinct p.patient_id, p.date
INTO gen_pop_tools.cr_hist_antihypert_rx
FROM dbo.emr_prescription p
join static_drugsynonym ds on lower(p.name) like '%' + ds.other_name + '%'
join gen_pop_tools.cr_hist_antihypertensives ah on lower(ds.generic_name) = ah.generic_name
AND p.date >= CAST('01-01-2017' AS DATE);
GO
alter table gen_pop_tools.cr_hist_antihypert_rx alter column patient_id int not null;
alter table gen_pop_tools.cr_hist_antihypert_rx alter column date DATE not null;
GO
alter table gen_pop_tools.cr_hist_antihypert_rx add primary key (patient_id, date);
UPDATE STATISTICS gen_pop_tools.cr_hist_antihypert_rx;
GO

-- Gather up all the systolic BP data and figure out treated or not
IF object_id('gen_pop_tools.cr_hist_bp_raw', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_bp_raw;
GO
SELECT t1.patient_id, t1.date, t1.bp_systolic
INTO gen_pop_tools.cr_hist_bp_raw
FROM dbo.emr_encounter t1 
WHERE t1.bp_systolic between 50 and 500 -- reality, please.
AND t1.date >= CAST('01-01-2017' AS DATE);
GO
CREATE INDEX cr_hist_bp_raw_idx
  ON gen_pop_tools.cr_hist_bp_raw
  (patient_id, date);
GO

IF object_id('gen_pop_tools.cr_hist_bp_treated', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_bp_treated;
GO
select t1.*,  
   case when exists (select null from gen_pop_tools.cr_hist_antihypert_rx rx where rx.patient_id=t1.patient_id 
   and rx.date between DATEADD(year, -2, t1.date) and DATEADD(day, -1, t1.date)) then 1 else 0 end as treated
INTO gen_pop_tools.cr_hist_bp_treated
from gen_pop_tools.cr_hist_bp_raw t1;
GO

IF object_id('gen_pop_tools.cr_hist_bp', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_bp;
GO
SELECT patient_id, year_month, treated, bp_systolic 
INTO gen_pop_tools.cr_hist_bp
FROM (SELECT distinct patient_id, gen_pop_tools.dateToYyyyMm(date) year_month, treated, bp_systolic, ROW_NUMBER() OVER 
    (PARTITION BY patient_id, gen_pop_tools.dateToYyyyMm(date) ORDER BY patient_id, gen_pop_tools.dateToYyyyMm(date)) AS rownumber
    FROM gen_pop_tools.cr_hist_bp_treated WHERE bp_systolic between 50 and 500 AND date >= CAST('01-01-2017' AS DATE)) AS dummyalias304
WHERE rownumber=1
order by patient_id, year_month desc, bp_systolic desc;
GO

IF object_id('gen_pop_tools.cr_hist_bp_seq', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_bp_seq;
GO
SELECT t0.patient_id, t0.year_month, t1.treated, t1.bp_systolic
INTO gen_pop_tools.cr_hist_bp_seq
FROM gen_pop_tools.tt_pat_seq t0
left join gen_pop_tools.cr_hist_bp t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
GO
alter table gen_pop_tools.cr_hist_bp_seq alter column patient_id int NOT NULL;
alter table gen_pop_tools.cr_hist_bp_seq alter column year_month varchar(10) NOT NULL;
GO
alter table gen_pop_tools.cr_hist_bp_seq add primary key (patient_id, year_month);
GO
CREATE INDEX cr_hist_bp_seq_idx
  ON gen_pop_tools.cr_hist_bp_seq
  (patient_id, year_month);
GO
select 'Now running downfil_bool on cr_hist_bp_seq';
GO
exec gen_pop_tools.downfill_bool 'gen_pop_tools.cr_hist_bp_seq' ,
                                 'patient_id',
                                 'year_month',
                                 'bp_systolic',
                                 'treated';
GO
UPDATE STATISTICS gen_pop_tools.cr_hist_bp_seq;
GO

IF object_id('gen_pop_tools.cr_smoke_seq', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_smoke_seq;
GO
select t0.patient_id, t0.year_month, 
case 
    when CAST(t1.smoking AS integer)>0 then CAST(t1.smoking AS integer)
    else 0 end as smoking 
INTO gen_pop_tools.cr_smoke_seq
FROM gen_pop_tools.tt_pat_seq t0
left join gen_pop_tools.tt_smoking t1 on t0.patient_id=t1.patient_id and t0.year_month=t1.year_month;
GO
CREATE INDEX cr_smoke_seq_idx
  ON gen_pop_tools.cr_smoke_seq
  (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.cr_smoke_seq;
select 'Now running downfill2 on cr_smoke_seq';
GO
exec gen_pop_tools.downfill2 'gen_pop_tools.cr_smoke_seq',
                               'patient_id',
                               'year_month',
                               'smoking',
							   '500';
GO

IF object_id('gen_pop_tools.cr_diab_seq', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_diab_seq;
GO
select 'starting to build gen_pop_tools.cr_diab_seq';
GO
select distinct t0.patient_id, t0.year_month, 
       case when t1.type_1_diabetes=1 or t2.type2=1 then 1 else 0 end as diabetes
INTO gen_pop_tools.cr_diab_seq
FROM gen_pop_tools.tt_pat_seq t0
left join gen_pop_tools.tt_type1 t1 on t0.patient_id=t1.patient_id and t0.year_month >= t1.year_month
left join gen_pop_tools.tt_type2 t2 on t0.patient_id=t2.patient_id and t0.year_month=t2.year_month;
GO
alter table gen_pop_tools.cr_diab_seq alter column patient_id int NOT NULL;
--alter table gen_pop_tools.cr_diab_seq alter column year_month int NOT NULL;
CREATE INDEX cr_diab_seq_idx
  ON gen_pop_tools.cr_diab_seq
  (patient_id, year_month);
GO
UPDATE STATISTICS gen_pop_tools.cr_diab_seq;
GO
alter table gen_pop_tools.cr_diab_seq alter column patient_id int NOT NULL;
GO

-- Bring together all of the fields and values
select 'starting to build gen_pop_tools.cr_hist_index_data';
GO

IF object_id('gen_pop_tools.cr_hist_index_data', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_index_data;
GO
SELECT t0.year_month,
  t1.patient_id,
  t1.sex,
  t1.race,
  case t10.race
    when 'CAUCASIAN' then 1
    when 'ASIAN' then 2
    when 'BLACK' then 3
    when 'HISPANIC' then 4
    when 'OTHER' then 5
    else 6
  end as fullrace,
  gen_pop_tools.age_2(CONVERT(DATE, REPLACE(t0.year_month, '_', '')+'01',112),CAST(t1.date_of_birth AS DATE)) as age,
  case when t2.patient_id is not null then 1 else 0 end as cvd,
  t4.result as t_chol,
  t5.result as hdl_chol,
  t6.bp_systolic, t6.treated,
  t7.smoking,
  t8.diabetes,
  case when t1.race='BLACK' and t1.sex='Female' then 'fb'
       when t1.race='OTHER' and t1.sex='Female' then 'fo'
       when t1.race='BLACK' and t1.sex='Male' then 'mb'
       when t1.race='OTHER' and t1.sex='Male' then 'mo'
  end as rs_type       
INTO gen_pop_tools.cr_hist_index_data
FROM gen_pop_tools.tt_pat_seq t0
left Join gen_pop_tools.cr_hist_index_patients t1 on t1.patient_id=t0.patient_id
left join gen_pop_tools.cr_dx_encs t2 on t0.patient_id=t2.patient_id and t0.year_month>=t2.year_month
left join gen_pop_tools.cr_hist_tchol t4 on t0.patient_id=t4.patient_id and t0.year_month=t4.year_month
left join gen_pop_tools.cr_hist_hdlc t5 on t0.patient_id=t5.patient_id and t0.year_month=t5.year_month
left join gen_pop_tools.cr_hist_bp_seq t6 on t0.patient_id=t6.patient_id and t0.year_month=t6.year_month
left join gen_pop_tools.cr_smoke_seq t7 on t0.patient_id=t7.patient_id and t0.year_month=t7.year_month
left join gen_pop_tools.cr_diab_seq t8 on t0.patient_id=t8.patient_id and t0.year_month=t8.year_month
left join gen_pop_tools.tt_pat t10 on t0.patient_id=t10.patient_id
where exists (select null from gen_pop_tools.cr_enc_pat t3 
              where t0.patient_id=t3.patient_id 
                    and CONVERT(DATE, REPLACE(t3.year_month, '_', '')+'01',112) 
                    between DATEADD(year, -2, CONVERT(DATE, REPLACE(t0.year_month, '_', '')+'01',112))
                    and DATEADD(month, -1, CONVERT(DATE, REPLACE(t0.year_month, '_', '')+'01',112))) ;
GO

-- Compute the values to be used in the risk score calculation
IF object_id('gen_pop_tools.cr_hist_pat_sum_x_value', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cr_hist_pat_sum_x_value;
GO
SELECT patient_id, year_month, fullrace as race, sex, age, rs_type, smoking,
(log(age) * case when i.rs_type='fb' then c.f_b_ln_age 
                when i.rs_type='fo' then c.f_ln_age 
                when i.rs_type='mb' then c.m_b_ln_age 
                when i.rs_type='mo' then c.m_ln_age end) ln_age,
(square(log(age)) * case when i.rs_type='fo' then c.f_ln_age2 else null end) ln_age2,
(log(t_chol) * case when i.rs_type='fb' then c.f_b_ln_chol 
                   when i.rs_type='fo' then c.f_ln_chol 
                   when i.rs_type='mb' then c.m_b_ln_chol 
                   when i.rs_type='mo' then c.m_ln_chol end) ln_t_chol,
(log(t_chol) * log(age) * case when i.rs_type='fo' then c.f_ln_ageXchol when i.rs_type='mo' then c.m_ln_ageXchol else null end) ln_ageXt_chol,
(log(hdl_chol) * case when i.rs_type='fb' then c.f_b_ln_hdl_C 
                     when i.rs_type='fo' then c.f_ln_hdl_C 
                     when i.rs_type='mb' then c.m_b_ln_hdl_C 
                     when i.rs_type='mo' then c.m_ln_hdl_C end) ln_hdl_c,
(log(hdl_chol) * log(age) * case when i.rs_type='fb' then c.f_b_ln_ageXhdl_C 
                               when i.rs_type='fo' then c.f_ln_ageXhdl_C 
                               when i.rs_type='mo' then c.m_ln_ageXhdl_C else null end) ln_ageXhdl_c,
(case 
  when treated=1 then
    (log(bp_systolic) * case when i.rs_type='fb' then c.f_b_ln_tr_sys_bp 
                            when i.rs_type='fo' then c.f_ln_tr_sys_bp 
                            when i.rs_type='mb' then c.m_b_ln_tr_sys_bp 
                            when i.rs_type='mo' then c.m_ln_tr_sys_bp end)
  when treated=0 then 
    (log(bp_systolic) * case when i.rs_type='fb' then c.f_b_ln_u_sys_bp 
                            when i.rs_type='fo' then c.f_ln_u_sys_bp 
                            when i.rs_type='mb' then c.m_b_ln_u_sys_bp 
                            when i.rs_type='mo' then c.m_ln_u_sys_bp end)
 end) ln_bp,
(case 
  when treated=1 then
    (log(bp_systolic) * log(age) * case when i.rs_type='fb' then c.f_b_ln_ageXtr_sys_bp end)
  when treated=0 then 
    (log(bp_systolic) * log(age) * case when i.rs_type='fb' then c.f_b_ln_ageXu_sys_bp end) 
 end) ln_ageXbp,
(case when smoking=4 then (case when i.rs_type='fb' then c.f_b_cur_smk 
                                when i.rs_type='fo' then c.f_cur_smk 
                                when i.rs_type='mb' then c.m_b_cur_smk 
                                when i.rs_type='mo' then c.m_cur_smk end)
      when smoking<4 then 0 end) cur_smk,
(case when smoking=4 then (log(age) * case when i.rs_type='fo' then c.f_cur_smkXln_age when i.rs_type='mo' then c.m_cur_smkXln_age end) 
      when smoking<4 then 0 end) cur_smkXln_age,
(case when diabetes=1 then (case when i.rs_type='fb' then c.f_b_diab 
                                 when i.rs_type='fo' then c.f_diab 
                                 when i.rs_type='mb' then c.m_b_diab 
                                 when i.rs_type='mo' then c.m_diab end)
      when diabetes=0 then 0 end) diab,
(case when i.rs_type='fb' then c.f_b_bsline 
      when i.rs_type='fo' then c.f_bsline 
      when i.rs_type='mb' then c.m_b_bsline 
      when i.rs_type='mo' then c.m_bsline end) baseline,
(case when i.rs_type='fb' then 86.61 
      when i.rs_type='fo' then -29.18 
      when i.rs_type='mb' then 19.54 
      when i.rs_type='mo' then 61.18 end) mean,
 cvd
INTO gen_pop_tools.cr_hist_pat_sum_x_value
FROM gen_pop_tools.cr_hist_index_data i,
  gen_pop_tools.cr_hist_coefficients c
where age between 20 and 79;
GO

--this table gets used by Riskscape
IF object_id('gen_pop_tools.cc_cr_risk_score', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_risk_score;
GO
select patient_id, year_month, race, sex, age, cur_smk, smoking,
  case
     when cvd=1 then 1
     else case rs_type
       when 'fb' 
       then power(1-baseline,exp((ln_age + ln_t_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + ln_ageXbp + cur_smk + diab) - mean))
       when 'fo'
       then power(1-baseline,exp((ln_age + ln_age2 + ln_t_chol + ln_ageXt_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + cur_smk + cur_smkXln_age + diab ) - mean))
       when 'mb' 
       then power(1-baseline,exp((ln_age + ln_t_chol + ln_hdl_c + ln_bp + cur_smk + diab ) - mean))
       when 'mo'
       then power(1-baseline,exp((ln_age + ln_t_chol + ln_ageXt_chol + ln_hdl_c + ln_ageXhdl_c + ln_bp + cur_smk + cur_smkXln_age + diab ) - mean))
     end
  end cv_risk 
INTO gen_pop_tools.cc_cr_risk_score
from gen_pop_tools.cr_hist_pat_sum_x_value;
GO

IF object_id('gen_pop_tools.cc_cr_yearly_risk_score', 'U') IS NOT NULL DROP TABLE gen_pop_tools.cc_cr_yearly_risk_score;
GO
select patient_id, substring(year_month,1,4) rpt_yr,
  first_value(race) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) race, 
  first_value(sex) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) sex, 
  first_value(age) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) age, 
  first_value(cur_smk) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) cur_smk, 
  first_value(smoking) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) smoking, 
  first_value(cv_risk) over (partition by patient_id, substring(year_month,1,4) 
                            order by patient_id, substring(year_month,1,4), year_month desc) cv_risk 
INTO gen_pop_tools.cc_cr_yearly_risk_score
from gen_pop_tools.cc_cr_risk_score
order by patient_id, substring(year_month,1,4), year_month desc;
GO
