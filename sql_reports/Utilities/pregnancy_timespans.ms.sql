DECLARE @TimespanLimit INT = 310;
DECLARE @NormalLimit INT = 280;
DECLARE @EndRule2G INT = 14
DECLARE @EndRuleClearance INT = 30
DECLARE @StartRule INT = -30;

---Assumes no pregnancy timespans currently exist
-- Basic setup, using hef_event to find pregnancy-related events and put them in temp tables
drop table if exists #tmp_preg_onset
Select distinct patient_id, date, id as event_id 
into #tmp_preg_onset
from hef_event where name='dx:pregnancy:onset';

drop table if exists #tmp_preg_complications
Select distinct patient_id, date, id as event_id 
into #tmp_preg_complications
from hef_event where name='dx:pregnancy:complications';

drop table if exists #tmp_preg_end_provis
Select distinct patient_id, date, id as event_id 
into #tmp_preg_end_provis
from hef_event where name='dx:pregnancy:end-of-pregnancy-provisional-icd10';

drop table if exists #tmp_preg_end
Select distinct patient_id, date, id as event_id 
into #tmp_preg_end
from hef_event where name='dx:pregnancy:end-of-pregnancy-icd10';

drop table if exists tmp_preg_edd_start;
select patient_id, DATEADD(DAY, -280, edd) as date, id as event_id
into #tmp_preg_edd_start
from emr_encounter 
where edd is not null;

drop table if exists tmp_preg_edd_end;
select patient_id, edd as date, id as event_id
into #tmp_preg_edd_end
from emr_encounter
where edd is not null;

drop table if exists #tmp_preg_gest_age
-- TODO: make an adjustment to handle the special case of single digit Z3A.* code
SELECT distinct e.patient_id, e.id as event_id, 
	CASE WHEN ISNUMERIC(SUBSTRING(dx.dx_code_id , charindex('.', dx.dx_code_id) + 1, 2)) = 1 
		THEN DATEADD(WEEK, -CAST(SUBSTRING(dx.dx_code_id, charindex('.', dx.dx_code_id) + 1, 2) AS int), e.date) 
		ELSE NULL END AS date
into #tmp_preg_gest_age
FROM hef_event e
	JOIN emr_encounter enc on enc.id=e.object_id and e.name='dx:pregnancy:gestational-age'
	JOIN emr_encounter_dx_codes dx on dx.encounter_id=enc.id and dx.dx_code_id like 'icd10:Z3A%';

-- End basic setup

-- Secondary setup. Merging basic tables into onset (start of pregnancy) and eop (end of pregnancy tables)
-- and back-dating the events according to the documented algorithm. Then merging these into the first overall event table (tmp_preg_seq1a).

drop table if exists #tmp_onset_grp
select distinct patient_id, date, event_id, which
into #tmp_onset_grp
from
(select patient_id, DATEADD(DAY, -30, date) AS date, event_id, 'start' as which from #tmp_preg_onset
 union 
 select patient_id, DATEADD(DAY, -30, date), event_id, 'comp' as which from #tmp_preg_complications 
 union 
 select patient_id, date, event_id, 'ga' as which from #tmp_preg_gest_age
 union 
 select patient_id, date, event_id, 'start'::varchar(5) as which from tmp_preg_edd_start) t0;

drop table if exists #tmp_eop_grp
select distinct patient_id, date, event_id, 'end' as which
into #tmp_eop_grp
from
(select patient_id, date, event_id from #tmp_preg_end_provis
 union 
 select patient_id, date, event_id from #tmp_preg_end
 union
 select patient_id, date, event_id from tmp_preg_edd_end) t0;
 
drop table if exists #tmp_preg_seq1a
select *, lag(date) over (partition by patient_id order by patient_id, date, which) as prior_dt,
          lead(date) over (partition by patient_id order by patient_id, date, which) as next_dt,
          lag(which) over (partition by patient_id order by patient_id, date, which) as prior_which,
          lead(which) over (partition by patient_id order by patient_id, date, which) as next_which
into #tmp_preg_seq1a
from (
 select * from #tmp_onset_grp
 union
 select * from #tmp_eop_grp
 ) t0;

CREATE INDEX pid_idx ON #tmp_preg_seq1a (patient_id);
CREATE INDEX dt_idx ON #tmp_preg_seq1a (date);
CREATE INDEX which_idx ON #tmp_preg_seq1a (which);
CREATE INDEX pwhich_idx ON #tmp_preg_seq1a (prior_which);

 --If there is no EoP event within 310 days of an SoP event we create a fake end event to terminate the timespan.
 --Per rule 2g the end of the timespan should be 14 days after the last start or complication event. 
 --This should be followed by a 30 day gap before the next pregnancy.
 --If an SoP doesn't have a matching EoP within 310 days the fake end event should be added 14 days after the last start/complication event, 
 --unless this would fall into the 30 day gap between pregnancies. In that case the fake end should be 30 days prior to the start of the next timespan.

drop table if exists #tmp_fake_end_setup;
drop table if exists #tmp_fake_end_setup2;
drop table if exists #tmp_fake_end;

-- Adding candidate end dates for a "normal" end (14 days after the SoP) and one based on rule 2g (14 days after the last start/comp event).
-- The fend candidate events are those which can count as SoP and do not have a normal ending. For SoP the specific criteria are:
-- 1. A start/comp event that is at least 30 (@EndRuleClearance) days after an end event.
-- 2. A start/comp event that has no prior event.
-- 3. A start/comp event where the prior event is more than 310 (@TimespanLimit) days prior.
-- 4. A GA event where the prior event is at least 30 (@EndRuleClearance) days prior.
-- For having no normal ending, the criteria is that the even has no end event within @TimespanLimit (310) days, 
-- unless there is a start event after the end event and still within @TimespanLimit days.

WITH EOPs AS (
	SELECT t0.patient_id, t0.date
	FROM #tmp_preg_seq1a t0
		LEFT JOIN #tmp_preg_seq1a t1 ON t1.patient_id = t0.patient_id AND t1.which='start'
	WHERE t0.which='end' 
		AND (t1.patient_id IS NULL OR NOT t1.date BETWEEN t0.date AND DATEADD(DAY, @EndRuleClearance, t0.date))
	GROUP BY t0.patient_id, t0.date
)
select *
	, DATEADD(DAY, @EndRule2G, date) as normal_end
	, (select top 1 DATEADD(DAY, @EndRule2G, t1.date)
		from #tmp_preg_seq1a t1 
		where t1.patient_id = t0.patient_id 
			and (t1.which='start' or t1.which='comp') 
			and t1.date > t0.date 
			and t1.date <= DATEADD(DAY, @NormalLimit, t0.date)
		order by date desc) as end_rule_2g
into #tmp_fake_end_setup
from #tmp_preg_seq1a t0
where (((which='start' or which='comp') and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance)
			 or (which='comp' and prior_which='end')
             or ((which='start' or which='comp' or which='ga') and prior_which is null)
             or ((which='start' or which='comp') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
             or ((which='start' or which='comp') and next_which is null and DATEDIFF(DAY, date, GETDATE()) > @TimespanLimit)
			 or (which='ga' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance))
	 and not exists (select null from EOPs e WHERE e.patient_id = t0.patient_id and e.date >= t0.date and e.date <= DATEADD(DAY, @TimespanLimit, t0.date))
; 

-- Adding a candidate end date specified by needing 30 days of clearance between timespans
select *
	,(select top 1 DATEADD(DAY, -@EndRuleClearance, t1.date) 
		from #tmp_preg_seq1a t1 
		where t1.patient_id = t0.patient_id 
			and t1.which in ('start', 'ga') 
			and t1.date > t0.end_rule_2g 
			and t1.date <= DATEADD(DAY, @EndRuleClearance, t0.end_rule_2g)
		order by t1.date asc
	) as end_clearance_rule
into #tmp_fake_end_setup2
from #tmp_fake_end_setup t0

-- Since the clearance and rule 2g dates are only non-null if the criteria for them is met, we chose those preferentially
-- and only use the "normal" end date if the other two are null.
select patient_id,
		case when end_clearance_rule is not null 
			then end_clearance_rule
		else
			case when end_rule_2g is not null
				then end_rule_2g
			else
				normal_end
			end
		end as date,
		event_id, 'fend' as which
into #tmp_fake_end
from #tmp_fake_end_setup2 t0;

-- If there is no SoP event within 310 days prior to an EoP we create a fake start event to initiate the timespan.
-- The fake SoP is 30 days prior to the EoP, unless this intersects the 30 day clearance period after a previous timespan.

drop table if exists #tmp_fake_start;
drop table if exists #tmp_fake_start_setup;
select *
	, DATEADD(DAY, @StartRule, date) as normal_start_date
	, DATEADD(DAY, @EndRuleClearance, prior_dt) as clearance_start_date
into #tmp_fake_start_setup
from #tmp_preg_seq1a t0
where ((which='end' and (prior_which='start' or prior_which='comp' or prior_which='ga') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
             or (which='end' and prior_which is null) 
			 or (which='end' and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance));

-- If the clearance start date is non-null we prefer that over the "normal" start date, similar to the fake end date selection.

select patient_id,
	case when DATEDIFF(DAY, prior_dt, normal_start_date) < @EndRuleClearance AND clearance_start_date IS NOT NULL
		then clearance_start_date
	else
		normal_start_date
	end as date, 
	event_id, 'fstrt' as which
into #tmp_fake_start
from #tmp_fake_start_setup;

-- Merging the fake EoP and SoP events into the real events table
-- TODO: use GROUP BY event_id to eliminate duplicate fake events created from the same real event 
drop table if exists #tmp_preg_seq1b;
select patient_id, date, event_id, which,
          lag(date) over (partition by patient_id order by patient_id, date, which, event_id) as prior_dt, 
          lead(date) over (partition by patient_id order by patient_id, date, which, event_id) as next_dt,
          lag(which) over (partition by patient_id order by patient_id, date, which, event_id) as prior_which,
          lead(which) over (partition by patient_id order by patient_id, date, which, event_id) as next_which,
          row_number() over (partition by patient_id order by patient_id, date, which, event_id) as i
into #tmp_preg_seq1b
from (
select patient_id, date, event_id, which from #tmp_preg_seq1a
union
select patient_id, date, event_id, which from #tmp_fake_end
union
select patient_id, date, event_id, which from #tmp_fake_start
) t0;

CREATE INDEX pid_idx_1b ON #tmp_preg_seq1b (patient_id);
CREATE INDEX dt_idx_1b ON #tmp_preg_seq1b (date);
CREATE INDEX which_idx_1b ON #tmp_preg_seq1b (which);
CREATE INDEX pwhich_idx_1b ON #tmp_preg_seq1b (prior_which);

-- In the case of very long intervals with no EoP, inserting a fake end event will create one valid timespan 
-- and one that is still longer than the limit due to not having an EoP.
-- We use the same criteria for fake EoP creation in a loop to close off timespans that are too long, until there are no more "candidate" timespans.

-- We count candidates using the same criteria as the standalone fend creation above, except we also add fstrt and fend as potential events.
-- Since the criteria for creating an fend event rules out those followed closely by start/comp events, we can assume any start/comp/fstrt 
-- event subsequent to fend is the start of a new timespan.


DROP TABLE IF EXISTS #tmp_preg_seq1c;
SELECT * 
INTO #tmp_preg_seq1c
FROM #tmp_preg_seq1b
WHERE 1=2;

DECLARE @FendCandidateCount INT;
DECLARE @PreviousCount INT = -1;
DECLARE @OlderCount INT = -1;



WITH EOPs AS (
	SELECT t0.patient_id, t0.date
	FROM #tmp_preg_seq1b t0
		LEFT JOIN #tmp_preg_seq1b t1 ON t1.patient_id = t0.patient_id AND t1.which='start'
	WHERE t0.which='end' 
		AND (t1.patient_id IS NULL OR NOT t1.date BETWEEN t0.date AND DATEADD(DAY, @EndRuleClearance, t0.date))
	GROUP BY t0.patient_id, t0.date
)
SELECT @FendCandidateCount = COUNT(1) FROM #tmp_preg_seq1b t0
	where (((which IN ('start', 'comp', 'fstrt')) and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance)
			or (which='comp' and prior_which='end')
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which='fend')
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which is null)
			or ((which IN ('start', 'comp', 'fstrt')) and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and next_which is null and DATEDIFF(DAY, date, GETDATE()) > @TimespanLimit)
			or (which='ga' and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance))
			and not exists (select null from EOPs e where e.patient_id = t0.patient_id and e.date >= t0.date and e.date <= DATEADD(DAY, @TimespanLimit, t0.date))
;			

WHILE @FendCandidateCount > 0
BEGIN
	delete from #tmp_fake_end_setup;
	WITH EOPs AS (
		SELECT t0.patient_id, t0.date
		FROM #tmp_preg_seq1b t0
			LEFT JOIN #tmp_preg_seq1b t1 ON t1.patient_id = t0.patient_id AND t1.which='start'
		WHERE t0.which='end' 
			AND (t1.patient_id IS NULL OR NOT t1.date BETWEEN t0.date AND DATEADD(DAY, @EndRuleClearance, t0.date))
		GROUP BY t0.patient_id, t0.date
	)
	INSERT INTO #tmp_fake_end_setup (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, normal_end, end_rule_2g)
	select patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which
		, DATEADD(DAY, 14, date) as normal_end
		, (select top 1 DATEADD(DAY, @EndRule2G, t1.date) 
			from #tmp_preg_seq1b t1 
			where t1.patient_id = t0.patient_id 
				and (t1.which='start' or t1.which='comp') 
				and t1.date > t0.date 
				and t1.date <= DATEADD(DAY, @NormalLimit, t0.date) 
			order by date desc
			) as end_rule_2g
	from #tmp_preg_seq1b t0
	where (((which IN ('start', 'comp', 'fstrt')) and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance)
			or (which='comp' and prior_which='end')
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which='fend')
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which is null)
			or ((which IN ('start', 'comp', 'fstrt')) and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
			or ((which IN ('start', 'comp', 'fstrt', 'ga')) and next_which is null and DATEDIFF(DAY, date, GETDATE()) > @TimespanLimit)
			or (which='ga' and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance))
			and not exists (select null from EOPs e where e.patient_id = t0.patient_id and e.date >= t0.date and e.date <= DATEADD(DAY, @TimespanLimit, t0.date))
	;

	delete from #tmp_fake_end_setup2;
	INSERT INTO #tmp_fake_end_setup2 (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, normal_end, end_rule_2g, end_clearance_rule)
	select *
		,(select top 1 DATEADD(DAY, -@EndRuleClearance, t1.date) 
			from #tmp_preg_seq1b t1 
			where t1.patient_id = t0.patient_id 
				and t1.which in ('start', 'ga') 
				and t1.date > t0.end_rule_2g 
				and t1.date <= DATEADD(DAY, @EndRuleClearance, t0.end_rule_2g)
			order by t1.date asc
		) as end_clearance_rule
	from #tmp_fake_end_setup t0;

	delete from #tmp_fake_end;
	INSERT INTO #tmp_fake_end (patient_id, date, event_id, which)
	SELECT patient_id,
			case when end_clearance_rule is not null 
				then end_clearance_rule
			else
				case when end_rule_2g is not null
					then end_rule_2g
				else
					normal_end
				end
			end as date,
			event_id, 'fend' as which
	FROM #tmp_fake_end_setup2 t0;

	DELETE FROM #tmp_preg_seq1c;
	INSERT INTO #tmp_preg_seq1c
	select patient_id, date, event_id, which,
			  lag(date) over (partition by patient_id order by patient_id, date, which, event_id) as prior_dt, 
			  lead(date) over (partition by patient_id order by patient_id, date, which, event_id) as next_dt,
			  lag(which) over (partition by patient_id order by patient_id, date, which, event_id) as prior_which,
			  lead(which) over (partition by patient_id order by patient_id, date, which, event_id) as next_which,
			  row_number() over (partition by patient_id order by patient_id, date, which, event_id) as i
	from (
		select patient_id, date, event_id, which from #tmp_preg_seq1b
		union
		select patient_id, date, event_id, which from #tmp_fake_end
	) t0;

	DELETE FROM #tmp_preg_seq1b;
	INSERT INTO #tmp_preg_seq1b SELECT * FROM #tmp_preg_seq1c;

	SET @OlderCount = @PreviousCount
	SET @PreviousCount = @FendCandidateCount;
	

	WITH EOPs AS (
		SELECT t0.patient_id, t0.date
		FROM #tmp_preg_seq1b t0
			LEFT JOIN #tmp_preg_seq1b t1 ON t1.patient_id = t0.patient_id AND t1.which='start'
		WHERE t0.which='end' 
			AND (t1.patient_id IS NULL OR NOT t1.date BETWEEN t0.date AND DATEADD(DAY, @EndRuleClearance, t0.date))
		GROUP BY t0.patient_id, t0.date
	)
	SELECT @FendCandidateCount = COUNT(1) FROM #tmp_preg_seq1b t0
	where (((which IN ('start', 'comp', 'fstrt')) and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance)
				or (which='comp' and prior_which='end')
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which='fend')
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which is null)
				or ((which IN ('start', 'comp', 'fstrt')) and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and next_which is null and DATEDIFF(DAY, date, GETDATE()) > @TimespanLimit)
				or (which='ga' and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance))
			and not exists (select null from EOPs e where e.patient_id = t0.patient_id and e.date >= t0.date and e.date <= DATEADD(DAY, @TimespanLimit, t0.date))
	;

	-- End the loop if the previous two counts match each other and the current count
	IF @PreviousCount = @FendCandidateCount AND @OlderCount = @PreviousCount
		BREAK;

END;



-- Labeling events as SoP and EoP
-- An event is SoP if it is:
-- 1. A start/comp/GA event that is at least 1 month after and end/fend event.
-- 2. A start/comp/GA event that has no prior event.
-- 3. A start/comp/GA event for which the prior event is over 310 (@TimespanLimit) days prior.
-- 4. A GA event for which the prior event is start, comp, end, or fend and is more than 30 (@EndRuleClearance) days prior, 
--    or the prior event is more than 310 (@TimespanLimit) days prior.
-- 5. Any fstrt event.
-- An event is EoP if it is:
-- 1. An end event followed by a start or fstrt after at least 1 month.
-- 2. An end event followed by another end event within 1 month.
-- 3. An end event followed by an fend event after at least a month.
-- 4. An end event with no subsequent events.
-- 5. Any fend event.
drop table if exists #tmp_preg_seq2;
select *,
   case when (which='start' or which='comp' or which='ga') and prior_which in ('end','fend') and DATEDIFF(MONTH, prior_dt, date) > 0 then 'sop'
        when (which='start' or which='comp' or which='ga') and prior_which is null then 'sop'
        when (which='start' or which='comp' or which='ga') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit then 'sop'
		when which='ga' and ((prior_which <> 'ga' and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
								or (prior_which IN ('start', 'comp', 'end', 'fend') and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance)
								or (DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
							)
			then 'sop'
        when which='end' and next_which in ('start','fstrt') and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
		when which='end' and next_which='end' and DATEDIFF(MONTH, date, next_dt) = 0 then 'eop'
		when which='end' and next_which='fend' and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
        when which='end' and (next_which = 'comp' or next_which = 'ga') then 'eop'
        when which='end' and next_which is null then 'eop'
		when which='start' and prior_which='end' and date=prior_dt then 'eop'
        when which='fend' then 'eop'
        when which='fstrt' then 'sop'
   end as preg        
into #tmp_preg_seq2
from #tmp_preg_seq1b;

drop table if exists #tmp_preg_seq3;
select *,
    sum(case when preg='sop' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as timespan_grps
into #tmp_preg_seq3
from #tmp_preg_seq2;

drop table if exists #tmp_preg_spans;
select patient_id, min(date) start_date, min(case when preg='eop' then date else null end) end_date, timespan_grps
into #tmp_preg_spans
from #tmp_preg_seq3
group by patient_id, timespan_grps;

-- creating 'fendf' events to terminate timespans that exceed 310 days

drop table if exists #tmp_fake_end2; 
select seq.patient_id, DATEADD(DAY, @TimespanLimit, seq.date) as date, seq.event_id, 'fendf' as which
into #tmp_fake_end2
from #tmp_preg_seq3 seq join #tmp_preg_spans sp on seq.patient_id = sp.patient_id and seq.timespan_grps = sp.timespan_grps 
where (preg = 'sop' or which = 'start' or which = 'comp' or which = 'ga') 
	AND (DATEDIFF(DAY, sp.start_date, sp.end_date) >= @TimespanLimit OR (sp.end_date IS NULL AND DATEDIFF(DAY, sp.start_date, GETDATE()) >= @TimespanLimit));

drop table if exists #tmp_fake_end3;
SELECT patient_id, min(date) as date, event_id, max(which) as which 
INTO #tmp_fake_end3
FROM #tmp_fake_end2 
GROUP BY patient_id, event_id 
ORDER BY patient_id, event_id;

drop table if exists #tmp_fendf_enumerated
select patient_id, date, event_id, which,
          lag(date) over (partition by patient_id order by patient_id, date, event_id) as prior_dt, 
          lead(date) over (partition by patient_id order by patient_id, date, event_id) as next_dt,
          row_number() over (partition by patient_id order by patient_id, date, event_id) as i
into #tmp_fendf_enumerated
from #tmp_fake_end3;

drop table if exists #tmp_fendf_borders
select *
	, CASE WHEN ((next_dt IS NULL OR DATEDIFF(DAY, date, next_dt) >= @EndRuleClearance) AND (prior_dt IS NULL OR DATEDIFF(DAY, prior_dt, date) < @EndRuleClearance)) THEN 'end'
		WHEN (prior_dt IS NULL OR DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance) THEN 'start'
		ELSE NULL END AS border_status
into #tmp_fendf_borders
from #tmp_fendf_enumerated
order by patient_id, date;

drop table if exists #tmp_fendf_groups
select *,
    sum(case when border_status='start' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as group_count
into #tmp_fendf_groups
from #tmp_fendf_borders;

drop table if exists #tmp_preg_seq4;
select patient_id, date, event_id, which,
          lag(date) over (partition by patient_id order by patient_id, date, which) as prior_dt, 
          lead(date) over (partition by patient_id order by patient_id, date, which) as next_dt,
          lag(which) over (partition by patient_id order by patient_id, date, which) as prior_which,
          lead(which) over (partition by patient_id order by patient_id, date, which) as next_which,
          row_number() over (partition by patient_id order by patient_id, date, which) as i
into #tmp_preg_seq4
from (
select patient_id, date, event_id, which from #tmp_preg_seq1b
union
select patient_id, min(date), min(event_id), max(which) from #tmp_fendf_groups group by patient_id, group_count
) t0;

drop table if exists #tmp_preg_seq5;
select *,
   case when which='fendf' then 'eop'
		when which='ga' and ((prior_which <> 'ga' and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
								or (prior_which IN ('start', 'comp', 'end', 'fend') and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance)
								or (DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
							)
			then 'sop'
		when (which='start' or which='comp' or which='ga') and prior_which in ('end','fend','fendf') and DATEDIFF(MONTH, prior_dt, date) > 0 then 'sop'
        when (which='start' or which='comp' or which='ga') and prior_which is null then 'sop'
        when (which='start' or which='comp' or which='ga') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit then 'sop'
        when which='end' and next_which in ('start','fstrt') and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
		when which='end' and next_which='end' and DATEDIFF(MONTH, date, next_dt) = 0 then 'eop'
		when which='end' and next_which='fend' and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
        when which='end' and (next_which = 'comp' or next_which = 'ga') then 'eop'
        when which='end' and next_which is null then 'eop'
		when which='start' and prior_which='end' and date=prior_dt then 'eop'
		when which='fend' then 'eop'
        when which='fstrt' then 'sop'
   end as preg        
into #tmp_preg_seq5
from #tmp_preg_seq4;

drop table if exists #tmp_preg_seq6;
select *,
    sum(case when preg='sop' or prior_which='fendf' or prior_which='fend' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as timespan_grps
into #tmp_preg_seq6
from #tmp_preg_seq5;

drop table if exists #tmp_preg_spans2;
select patient_id, min(date) start_date, max(case when which='fendf' then date else null end) fake_end_date, min(case when preg='eop' then date else null end) end_date, timespan_grps
into #tmp_preg_spans2
from #tmp_preg_seq6
group by patient_id, timespan_grps;

drop table if exists #tmp_preg_spans3;
select patient_id, start_date, case when fake_end_date is not null and fake_end_date < end_date then fake_end_date else end_date end end_date, timespan_grps
into #tmp_preg_spans3
from #tmp_preg_spans2;

-- end fendf event creation

-- fendf loop

DECLARE @LongTimespanCount INT;





SELECT @LongTimespanCount = COUNT(1) 
FROM #tmp_preg_spans3 
WHERE DATEDIFF(DAY, start_date, end_date) > @TimespanLimit

WHILE @LongTimespanCount > 0
BEGIN
	DELETE FROM #tmp_fake_end2;
	INSERT INTO #tmp_fake_end2 (patient_id, date, event_id, which)
	select seq.patient_id, DATEADD(DAY, @TimespanLimit, seq.date) as date, seq.event_id, 'fendf' as which
	from #tmp_preg_seq6 seq join #tmp_preg_spans3 sp on seq.patient_id = sp.patient_id and seq.timespan_grps = sp.timespan_grps 
	where (preg = 'sop' or which = 'start' or which = 'comp' or which = 'ga') 
		AND (DATEDIFF(DAY, sp.start_date, sp.end_date) > @TimespanLimit 
			OR (sp.end_date IS NULL AND DATEDIFF(DAY, sp.start_date, GETDATE()) > @TimespanLimit));
	
	DELETE #tmp_fake_end3;
	INSERT INTO #tmp_fake_end3 (patient_id, date, event_id, which)
	SELECT patient_id, min(date) as date, event_id, max(which) as which 
	FROM #tmp_fake_end2
	GROUP BY patient_id, event_id 
	ORDER BY patient_id, event_id;
     
	DELETE FROM #tmp_fendf_enumerated
	INSERT INTO #tmp_fendf_enumerated (patient_id, date, event_id, which, prior_dt, next_dt, i)
	SELECT patient_id, date, event_id, which,
			  lag(date) over (partition by patient_id order by patient_id, date, event_id) as prior_dt, 
			  lead(date) over (partition by patient_id order by patient_id, date, event_id) as next_dt,
			  row_number() over (partition by patient_id order by patient_id, date, event_id) as i
	from #tmp_fake_end3;

	DELETE FROM #tmp_fendf_borders
	INSERT INTO #tmp_fendf_borders (patient_id, date, event_id, which, prior_dt, next_dt, i, border_status)
	SELECT *
		, CASE WHEN ((next_dt IS NULL OR DATEDIFF(DAY, date, next_dt) >= @EndRuleClearance) AND (prior_dt IS NULL OR DATEDIFF(DAY, prior_dt, date) < @EndRuleClearance)) THEN 'end'
			WHEN (prior_dt IS NULL OR DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance) THEN 'start'
			ELSE NULL END AS border_status
	FROM #tmp_fendf_enumerated
	ORDER BY patient_id, date;

	DELETE FROM #tmp_fendf_groups
	INSERT INTO #tmp_fendf_groups (patient_id, date, event_id, which, prior_dt, next_dt, i, border_status, group_count)
	SELECT *,
		sum(case when border_status='start' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as group_count
	FROM #tmp_fendf_borders;

	DELETE FROM #tmp_preg_seq4;
	INSERT INTO #tmp_preg_seq4 (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, i)
	select patient_id, date, event_id, which,
			  lag(date) over (partition by patient_id order by patient_id, date, which) as prior_dt, 
			  lead(date) over (partition by patient_id order by patient_id, date, which) as next_dt,
			  lag(which) over (partition by patient_id order by patient_id, date, which) as prior_which,
			  lead(which) over (partition by patient_id order by patient_id, date, which) as next_which,
			  row_number() over (partition by patient_id order by patient_id, date, which) as i
	from (
	select patient_id, date, event_id, which from #tmp_preg_seq6
	union
	select patient_id, date, event_id, which from #tmp_fake_end
	union
	select patient_id, date, event_id, which from #tmp_fake_start
	union
	select patient_id, min(date), min(event_id), max(which) from #tmp_fendf_groups group by patient_id, group_count
	) t0;

	DELETE FROM #tmp_preg_seq5;
	INSERT INTO #tmp_preg_seq5 (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, i, preg)
	select *,
	   case when which='fendf' then 'eop'
		when which='ga' and ((prior_which <> 'ga' and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
								or (prior_which IN ('start', 'comp', 'end', 'fend') and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance)
								or (DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
							)
			then 'sop'
			when (which='start' or which='comp' or which='ga') and prior_which in ('end','fend','fendf') and DATEDIFF(MONTH, prior_dt, date) > 0 then 'sop'
			when (which='start' or which='comp' or which='ga') and prior_which is null then 'sop'
			when (which='start' or which='comp' or which='ga') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit then 'sop'
			when which='end' and next_which in ('start','fstrt') and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
			when which='end' and next_which='end' and DATEDIFF(MONTH, date, next_dt) = 0 then 'eop'
			when which='end' and next_which='fend' and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
			when which='end' and (next_which = 'comp' or next_which = 'ga') then 'eop'
			when which='end' and next_which is null then 'eop'
			when which='start' and prior_which='end' and date=prior_dt then 'eop'
			when which='fend' then 'eop'
			when which='fstrt' then 'sop'
	   end as preg        
	from #tmp_preg_seq4;

	DELETE FROM #tmp_preg_seq6;
	INSERT INTO #tmp_preg_seq6 (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, i, preg, timespan_grps)
	select *,
		sum(case when preg='sop' or prior_which='fendf' or prior_which='fend' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as timespan_grps
	from #tmp_preg_seq5;

	DELETE FROM #tmp_preg_spans2;
	INSERT INTO #tmp_preg_spans2 (patient_id, start_date, fake_end_date, end_date, timespan_grps)
	select patient_id
			, min(case when which IN ('start', 'ga', 'comp', 'fstrt') then date else null end) start_date
			, max(case when which='fendf' then date else null end) fake_end_date
			, min(case when preg='eop' then date else null end) end_date, timespan_grps
	from #tmp_preg_seq6
	group by patient_id, timespan_grps;

	DELETE FROM #tmp_preg_spans3;
	INSERT INTO #tmp_preg_spans3 (patient_id, start_date, end_date, timespan_grps)
	select patient_id, start_date, case when fake_end_date is not null and fake_end_date < end_date then fake_end_date else end_date end end_date, timespan_grps
	from #tmp_preg_spans2;
	
	SET @PreviousCount = @LongTimespanCount
	SELECT @LongTimespanCount = COUNT(1) FROM #tmp_preg_spans3 WHERE DATEDIFF(DAY, start_date, end_date) > @TimespanLimit
	IF @PreviousCount = @LongTimespanCount
		BREAK



END

-- end fendf loop

--BEGIN one final fend creation step to pick up any new timespans

CREATE INDEX pid_idx ON #tmp_preg_seq6 (patient_id);
CREATE INDEX dt_idx ON #tmp_preg_seq6 (date);
CREATE INDEX which_idx ON #tmp_preg_seq6 (which);
CREATE INDEX pwhich_idx ON #tmp_preg_seq6 (prior_which);

delete from #tmp_fake_end_setup;
WITH EOPs AS (
	SELECT t0.patient_id, t0.date
	FROM #tmp_preg_seq6 t0
		LEFT JOIN #tmp_preg_seq6 t1 ON t1.patient_id = t0.patient_id AND t1.which='start'
	WHERE t0.which='end' 
		AND (t1.patient_id IS NULL OR NOT t1.date BETWEEN t0.date AND DATEADD(DAY, @EndRuleClearance, t0.date))
	GROUP BY t0.patient_id, t0.date
)
INSERT INTO #tmp_fake_end_setup (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, normal_end, end_rule_2g)
select patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which
	, DATEADD(DAY, 14, date) as normal_end
	, (select top 1 DATEADD(DAY, @EndRule2G, t1.date) 
		from #tmp_preg_seq6 t1 
		where t1.patient_id = t0.patient_id 
			and (t1.which='start' or t1.which='comp') 
			and t1.date > t0.date 
			and t1.date <= DATEADD(DAY, @NormalLimit, t0.date) 
		order by date desc
		) as end_rule_2g
from #tmp_preg_seq6 t0
where (((which IN ('start', 'comp', 'fstrt')) and prior_which='end' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance)
				or (which='comp' and prior_which='end')
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which='fend')
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and prior_which is null)
				or ((which IN ('start', 'comp', 'fstrt')) and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit)
				or ((which IN ('start', 'comp', 'fstrt', 'ga')) and next_which is null and DATEDIFF(DAY, date, GETDATE()) > @TimespanLimit)
				or (which='ga' and DATEDIFF(DAY, prior_dt, date) >= @EndRuleClearance))
			 and not exists (select null from EOPs e WHERE e.patient_id = t0.patient_id and e.date >= t0.date and e.date <= DATEADD(DAY, @TimespanLimit, t0.date))
;

delete from #tmp_fake_end_setup2;
INSERT INTO #tmp_fake_end_setup2 (patient_id, date, event_id, which, prior_dt, next_dt, prior_which, next_which, normal_end, end_rule_2g, end_clearance_rule)
select *
	,(select top 1 DATEADD(DAY, -@EndRuleClearance, t1.date) 
		from #tmp_preg_seq6 t1 
		where t1.patient_id = t0.patient_id 
			and t1.which in ('start', 'ga') 
			and t1.date > t0.end_rule_2g 
			and t1.date <= DATEADD(DAY, @EndRuleClearance, t0.end_rule_2g)
		order by t1.date asc
	) as end_clearance_rule
from #tmp_fake_end_setup t0;

delete from #tmp_fake_end;
INSERT INTO #tmp_fake_end (patient_id, date, event_id, which)
SELECT patient_id,
		case when end_clearance_rule is not null 
			then end_clearance_rule
		else
			case when end_rule_2g is not null
				then end_rule_2g
			else
				normal_end
			end
		end as date,
		event_id, 'fend' AS which
FROM #tmp_fake_end_setup2 t0;

DROP TABLE IF EXISTS #tmp_preg_seq7;
select patient_id, date, event_id, which,
			lag(date) over (partition by patient_id order by patient_id, date, which, event_id) as prior_dt, 
			lead(date) over (partition by patient_id order by patient_id, date, which, event_id) as next_dt,
			lag(which) over (partition by patient_id order by patient_id, date, which, event_id) as prior_which,
			lead(which) over (partition by patient_id order by patient_id, date, which, event_id) as next_which,
			row_number() over (partition by patient_id order by patient_id, date, which, event_id) as i
into #tmp_preg_seq7
from (
	select patient_id, date, event_id, which from #tmp_preg_seq6
	union
	select patient_id, date, event_id, which from #tmp_fake_end
) t0;

DROP TABLE IF EXISTS #tmp_preg_seq8;
select *,
	case when which='fendf' then 'eop'
	when which='ga' and ((prior_which <> 'ga' and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
							or (prior_which IN ('start', 'comp', 'end', 'fend') and DATEDIFF(DAY, prior_dt, date) > @EndRuleClearance)
							or (DATEDIFF(DAY, prior_dt, date) > @TimespanLimit) 
						)
		then 'sop'
		when (which='start' or which='comp' or which='ga') and prior_which in ('end','fend','fendf') and DATEDIFF(MONTH, prior_dt, date) > 0 then 'sop' 
		when (which='start' or which='comp' or which='ga') and prior_which is null then 'sop'
		when (which='start' or which='comp' or which='ga') and DATEDIFF(DAY, prior_dt, date) > @TimespanLimit then 'sop'
		when which='end' and next_which in ('start','fstrt') and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
		when which='end' and next_which='end' and DATEDIFF(MONTH, date, next_dt) = 0 then 'eop'
		when which='end' and next_which='fend' and DATEDIFF(MONTH, date, next_dt) > 0 then 'eop'
		when which='end' and (next_which = 'comp' or next_which = 'ga') then 'eop'
		when which='end' and next_which is null then 'eop'
		when which='fend' then 'eop'
		when which='fstrt' then 'sop'
		when which='start' and prior_which='end' and date=prior_dt then 'eop' -- TODO: document this special case
		when which='end' and next_which='fendf' then 'eop' -- TODO: document this special case
	end as preg    
into #tmp_preg_seq8
from #tmp_preg_seq7;

DROP TABLE IF EXISTS #tmp_preg_seq9;
select *,
	sum(case when preg='sop' or prior_which='fendf' or prior_which='fend' then 1 else 0 end) over (partition by patient_id order by patient_id, i) as timespan_grps
into #tmp_preg_seq9
from #tmp_preg_seq8;

DELETE FROM #tmp_preg_spans2;
INSERT INTO #tmp_preg_spans2 (patient_id, start_date, fake_end_date, end_date, timespan_grps)
select patient_id
		, min(case when which IN ('start', 'ga', 'comp', 'fstrt') then date else null end) start_date
		, max(case when which='fendf' then date else null end) fake_end_date
		, min(case when preg='eop' then date else null end) end_date, timespan_grps
from #tmp_preg_seq9
group by patient_id, timespan_grps;

DELETE FROM #tmp_preg_spans3;
INSERT INTO #tmp_preg_spans3 (patient_id, start_date, end_date, timespan_grps)
select patient_id, start_date
		, case 
			when fake_end_date is not null and fake_end_date < end_date then fake_end_date 
			else end_date end end_date
		, timespan_grps
from #tmp_preg_spans2;

-- END one final fend creation step to pick up any new timespans

drop table if exists #tmp_preg_spans4;
select ROW_NUMBER() OVER (ORDER BY start_date) as id, patient_id, start_date, end_date
into #tmp_preg_spans4
from #tmp_preg_spans3;

drop table if exists #tmp_seq_spans
create table #tmp_seq_spans
(
	event_id int not null,
	span_id int not null,
	constraint tmp_seq_spans_unique unique(event_id, span_id) WITH (IGNORE_DUP_KEY=ON)
);

INSERT INTO #tmp_seq_spans (event_id, span_id)
SELECT t0.event_id as event_id, t1.id as span_id
FROM #tmp_preg_seq9 t0 JOIN #tmp_preg_spans4 t1 ON t0.patient_id = t1.patient_id 
	AND t0.date >= t1.start_date AND t0.date <= t1.end_date


drop table if exists #tmp_preg_seq10;
select * 
into #tmp_preg_seq10
from #tmp_preg_seq9
where not (which='fendf' and event_id in (
	select seq.event_id
	from #tmp_preg_seq9 seq join #tmp_seq_spans hte on hte.event_id=seq.event_id join #tmp_preg_spans4 ht on ht.id=hte.span_id
	where which = 'fendf' and not seq.date between ht.start_date and ht.end_date));


delete from hef_timespan_events te where exists (select null from hef_timespan t where te.timespan_id=t.id and t.name='pregnancy');
delete from hef_timespan where name='pregnancy';

insert into hef_timespan (name, source, patient_id, start_date, end_date, timestamp, pattern)
select 'pregnancy' as name, 'urn:x-esphealth:heuristic:commoninf:timespan:pregnancy:v1' as source, patient_id, start_date, end_date,
       current_timestamp as timestamp, 'onset:dx_code eop:dx_code' as pattern
from #tmp_preg_spans;

drop table if exists #tmp_preg_spans_w_id;
select id, patient_id, start_date, end_date, pattern
into #tmp_preg_spans_w_id
from hef_timespan
where name='pregnancy';

insert into hef_timespan_events (event_id, timespan_id)
select distinct t0.event_id, t1.id as timespan_id
from #tmp_preg_seq10 t0
join #tmp_preg_spans_w_id t1 on t0.patient_id=t1.patient_id 
  and t0.date>=t1.start_date and t0.date<=t1.end_date
  where not exists (select null from hef_timespan_events te where te.timespan_id=t1.id and te.event_id=t0.event_id);
