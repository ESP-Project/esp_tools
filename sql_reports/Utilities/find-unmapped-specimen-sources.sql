-- Modify for desired test_names

select count(*), c.test_name, l.specimen_source, l.native_code
from emr_labresult l,
conf_labtestmap c
where l.native_code = c.native_code
and test_name in ('chlamydia', 'gonorrhea')
and ((lower(specimen_source) not in (select name from static_specimensourcesnomed)) or specimen_source is null or specimen_source = '')
group by test_name, specimen_source, l.native_code
order by test_name, count(*) desc, native_code, specimen_source;


--If you find you are getting a huge list and just want to get the specimen_source names (and you don't care about the native_codes) so you can map them in the UI, just run this:


select count(*), c.test_name, l.specimen_source
from emr_labresult l,
conf_labtestmap c
where l.native_code = c.native_code
and test_name in ('chlamydia', 'gonorrhea')
and ((lower(specimen_source) not in (select name from static_specimensourcesnomed)) or specimen_source is null or specimen_source = '')
group by  test_name, specimen_source
order by test_name, count(*) desc, specimen_source;



