﻿SET client_min_messages TO WARNING;

DROP TABLE IF EXISTS kre_report.tmp_unmapped_gonorrhea;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_chlamydia;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_a;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_b;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hiv;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_lyme;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_rpr;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tp;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tppa;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_vdrl;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_pertussis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tuberculosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_a1c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_alt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ast;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_bilirubin;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ogtt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_fta;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_islet_cell_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_c_peptide;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_cd4;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_gad65;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_glucose;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ica512;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_insulin_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid19;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_influenza;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid_suspect;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_anaplasmosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_babesiosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_all;

DROP TABLE IF EXISTS kre_report.unmapped_result_strings_report;


DROP TABLE IF EXISTS kre_report.tmp_unmapped_filter;
CREATE TABLE kre_report.tmp_unmapped_filter
(
  filter_result text
)
WITH (
  OIDS=FALSE
);


-- exact string matches
INSERT INTO kre_report.tmp_unmapped_filter(filter_result)
VALUES 
( '--'),
( '? INTERFERING SUBSTANCE'),
( '0.00 Comment'),
( '(617)-983-6600'),
( '617-983-6940 to obtain a case report form.'), 
( '69 FIRST AVENUE'), 
( 'Abbott BinaxNOW COVID-19 Antigen Self Test'),
( 'Access Bio CareStart COVID-19 Antigen Home Test'),
( 'ACON/Flowflex COVID-19 Antigen Home Test Kit'),
( 'ADDED IN ERROR'), 
( 'Added Test'),
( 'Additional Testing Ordered%'),
( 'Admission Surveillance'),
( 'Anal: Unsatisfactory'),
( 'Analyzer down. Changed to alternate analyzer.'),
( 'and Isolation and Quarantine Requirements (105 CMR'), 
( 'ADD ON STABILITY'),
( '(Added) SEE TEXT'),
( 'ALTERNATE TEST PERFORMED.'),
( 'Alternative testing done at Quest Diagnostics reference lab 06-2023'),
( 'AN SWAB'),
( 'Analyzer down. Converted to alternate testing platform to expedite for ER patient.'), 
( 'ANALYZER DOWN, SWITCHED TO ALTRENATE INSTRUMENT'), 
( 'ARS COV 2 RNA PANEL, PROBE, RESPIRATORY SPECIMEN ('),
( 'ARTERIAL'),
( 'Bad derived test component'),
( 'BATCHING ERROR. REORDERED.'),
( 'BD Veritor At-Home COVID-19 Test'),
( 'broad institute'),
( 'Broad test not performed%'),
( 'CALLED RESULT%'), 
( 'Cancel%'),
( 'CANDUP'),
( 'CAP'),
( 'Cerner ver%'),
( 'CERVIX%'),
( 'CHANGED TO CCVD19'),
( 'CHANGE PER CR'),
( 'CLEAN CATCH%'), 
( 'CLEAN VOIDED%'),
( 'CODE CHANGED'),
( 'Collection method not acceptable.'), 
( 'COLONIES TOO SMALL TO IDENTIFY. REINCUBATED'), 
( 'COMBINED TESTS'),
( 'Comment'),
( 'COMMENT TEST%'),
( 'Complete'),
( 'Container leaked during transport; no specimen remains for testing.'), 
( 'Container leaked during transport; no specimen rem'),
( 'CONTAMINANT'),
( 'CONTAMINATED'), 
( 'CONTAMINATED SPECIMEN'), 
( 'Contaminated specimen. Please disregard result and draw new specimen.'),
( 'Contamination suspected'), 
( 'CORONAVIRUS DISEASE 2019 (COVID-19)- EXT'),
( 'COULD NOT VOID%'),
( 'COV2 BROAD INSTITUTE 9/28/2021'),
( 'Covid PCR result - Broad Lab'),
( 'covid 19 result'), 
( 'covid result'),
( 'CRDUP'),
( 'Credit%'), 
( 'CRHEMO'),
( 'CRORD'), 
( 'CRRX'), 
( 'CRYPTOSPORIDIU...'),
( 'CT GC NAAT'),
( 'CX PER CR'),
( 'Culture in Process'),
( 'Culture in progress%'),
( 'Decline%'),
( 'DELETED'),
( 'Direct bilirubin greater than total bilirubin, suspect assay'), 
( 'Direct bilirubin is less than the measureable limit. Therefore, indirect'),
( 'discard%'), 
( 'DISREGARD%'),
( 'Done%'),
( 'donor'),
( 'DNR'), 
( 'DOUP'), 
( 'DRUG SUSCEPTIBILITY%'),
( 'Due to instrument%'), 
( 'DUPLICATE%'), 
( 'DUPO'), 
( 'Ellume COVID-19 Home Test Kit'),
( 'Endocervical'),
( 'ENDOCX'), 
( 'ENTERED IN ERROR - WRONG PATIENT'), 
( 'endo cx'),
( 'Err code 6'), 
( 'error'), 
( 'Erroneous Encounter'), 
( 'Ex.'),
( 'FIA antigen test not performed%'),
( 'FINAL'),
( 'Final Report'),
( 'FIXED TO CLCVD'), 
( 'fixed to clcvd'),
( 'fixed to pcvd19'), 
( 'Footnote%'),
( 'FOR RESULTS, PLEASE SEE OUTSIDE LAB IN EPIC MEDIA'),
( 'GEL CLUMPS PRESENT'),
( 'GROSS HEMOLYSIS%'),
( 'GROSSLY HEMOLYZED'), 
( 'Has been added%'),
( 'HEALTH_MAINTENANCE_COMPLETED'),
( 'Health in accordance with Disease Reporting,Survei'),
( 'HEM'),
( 'HEMOLY'),
( 'Hemolysis detected%'),
( 'HEMOLYSIS PRESENT IN SAMPLE'), 
( 'HEMOLYTIC'), 
( 'HEMOLYZED%'), 
( 'HepC Ab CMIA'),
( 'hbsui'),
( 'hbsu1'),
( 'hbsul'),
( 'HIDE'), 
( 'HIV Ag/Ab CMIA'),
( 'HIV COUNSEL - DONE'),
( 'https://wihlaboratory.testcatalog.org'),
( 'iHealth COVID-19 Antigen Rapid Test'),
( 'IMPROPER TRANSPORT MEDIA'), 
( 'In-house testing being performed'),
( 'INCORRECT ORDER CODE'),
( 'incorrect swab used%'),
( 'Instrument malfunction, QNS for retesting.'),
( 'INTERFERING SUBSTANCE'),
( 'INTERFERING SUBSTANCE, NO INTERPRETATION.'), 
( 'INTERPRET WITH CAUTION, SPECIMEN HEMOLYZED'), 
( 'INSTRUMENT DOWN'),
( 'INSTRUMENT ERROR%'),
( 'Invalid%'),
( 'Inhibited'),
( 'Lab Accident at Reference Lab, Test not done.'), 
( 'LAB ACCIDENT: Unable to test. Resubmission requested.'),
( 'Lab Cancel%'),
( 'Lab Error'),
( 'Lab were not drawn or obtained'),
( 'LABORATORY LOG IN ERROR'),
( 'Laboratory, Saints Campus, 1 Hospital Drive%'),
( 'lab not done%'),
( 'Lipemic Specimen'), 
( 'LIPEMIC, GROSSLY'),
( 'LOG value not%'),
( 'Long wait for analyzer. Converted to alternate testing platform to expedite for ER patient.'),
( 'Long wait time. Converted to alternate platform to expedite for ER patient.'),
( 'LOST IN TRANSIT TO REF LAB'),
( 'LOST SPECIMEN. DENISE ANGERANE, ED CHARGE RN NOTIFIED.'), 
( 'Luminex PCR not performed. PCR sent to Broad Institute, result pending'),
( 'MACHINE DOWN'),
( 'MD AND/OR CARE UNIT NOTIFIED'), 
( 'MGLAB'),
( 'MIC'), 
( 'MICROSCOPY NOT DONE'),
( 'MISLABELED%'), 
( 'MIXED ORGANISMS RESEMBLING OROPHARYNGEAL FLORA'),
( 'neh'),
( 'N/A'),
( 'NASAL'), 
( 'Nasopharyngeal'),
( 'NASOPHARYNGEAL SWAB'),
( 'Nasopharynx'),
( 'ND'), 
( 'Neisseria species'),
( 'NEISSERIA GONORRHOEAE NEGATIVE FOR PENICILLINASE PRODUCTION'), 
( 'NEISSERIA MENINGITIDIS'), 
( 'NG'),
( 'Nina Lowery'),
( 'NO ADD%'),
( 'NOADD%'), 
( 'NO CHARGE%'), 
( 'NO CHARGE CREDITED: DUPLICATE SPECIMEN/ORDER.'),
( 'NO CHARGE CREDITED: NO SPECIMEN RECEIVED'), 
( 'NO CHARGE CREDITED: WRONG ORDER CODE USED.'), 
( 'NO COLLECTION%'),
( 'NO KIT, REORDER AS CCVD19'),
( 'NO RESULT%'), 
( 'No result (prediction failure)'),
( 'NO SPECIMEN RECEIVED'), 
( 'NO SPECIMEN RECEIVED,CALLED TO U6 AMANDA'), 
( 'NORMAL BACTERIAL VAGINAL FLORA'), 
( 'NORMOCHROMIC NORMOCYTIC ANEMIA WITHOUT SCHISTOCYTES, SPHEROCYTES OR BITE CELLS.'),
( 'NOT AVAIL.'),
( 'Not Applicable'),
( 'NOT CALCULATED'), 
( '%NOT COLLECTED%'),
( 'not completed%'),
( 'Not Done'),
( 'Not Drawn. Patient Refused.'),
( 'NOT GIVEN'), 
( 'NOT INDICATED'),
( 'NOT NEEDED'),
( 'Not Perf'),
( 'Not performed'),
( 'NOT_PERFORMED'),
( 'NO SPEC REC''D'),
( 'Not tested'),
( '(NOTE)'), 
( 'NOT VALID'),
( 'NOTIFIED RN'),
( 'NP'),
( 'NULL'),
( 'O.4'), 
( 'OLAB'), 
( 'OLD'), 
( 'OLD SPECIMEN'), 
( 'ORDER ENTRY ERROR'),
( 'OPERATOR ERROR'),
( 'Orasure InteliSwab COVID-19 Rapid Test'),
( 'order error'),
( 'ORDER ERROR'),
( 'Order expired%'),
( 'order sent to lab'),
( 'ORDERED AND RESULTED ON WRONG PATIENT'), 
( 'ordered in error'),
( 'ORDERED INCORRECTLY'),
( 'Ordered on new accession.'), 
( 'ORDERED BY%'),
( 'Other (see comments)'),
( 'PATIENT ID IN QUESTION'), 
( 'PATIENT DID NOT ARRIVE FOR SCHEDULED TESTING.'), 
( 'PCR sent to in-house platform'),
( 'PCR test not performed. Sent for FIA antigen test in house, result pending'),
( 'Pending%'),
( 'PND'),
( 'pt decline%'),
( 'PATIENT DISCHARGED'),
( 'Patient Did Not Return%'),
( 'PATIENT DOC%'),
( 'Patient left%'),
( 'Patient never went%'),
( 'Patient not available'),
( 'PT LEFT'),
( 'PT TO RETURN WITH URINE'),
( 'PATIENT REFUSED%'),
( 'PCR for B. burgdorferi is only available on synovial fluid, synovial tissue specimens, or skin biopsies.  The sensitivity of this assay, when applied to other specimen types, is unacceptably low.'), 
( 'Performed'),
( 'Performed at MAYO MEDICAL LABORATORY, 200 First Street Southwest, Rochester, MN 55905'),
( 'Performed at NORTHWESTERN REPRODUCTIVE GENETICS, INC., 680 North Lake Shore Drive, Suite 1230, Chicago, IL 60611'),
( 'Performed at NSMC'),
( 'PLASMA SENT IN ERROR%'),
( 'PLASMODIUM SPECIES IDENTIFIED. DEFINITIVE SPECIATION REQUIRES FURTHER STUDIES.'),
( 'PLASMODIUM FALCIPARUM'),
( 'Please contact the Division of STD Prevention at'), 
( 'PLEASE DISREGARD%'), 
( 'Please refer to COVID-19, NAA result displayed on the All Results Tab'),
( 'PLEASE REFER TO SPECIMEN NUMBER:'), 
( 'PLEASE SEE%'),
( 'POSSIBLE CONTAMINATION%'), 
( 'PREF'),
( 'Processed and/or performed%'),
( 'pt ref'),
( 'QNS%'),
( 'QUANTITY NOT SUFFICIENT%'),
( 'QUANTITY NOT SUFICIENT%'),
( 'QUESTION REACTIVE'),
( 'Quidel Quickvue At-Home OTC COVID-19 Test'), 
( 'R'),
( 'RAPID MIC METHOD'), 
( 'RAPID SARS COV + SARS COV 2 AG, QL IA, RESPIRATORY'),
( 'RARITAN, NJ 08869'), 
( 'REACCESSIONED FOR RAPID IN HOUSE TESTING.'), 
( 'reaction'),
( 'Re-accessioned due to Billing Error. Verified by'),
( 'REFER TO CCVD19'),
( 'REFER TO FOLLOWING FREE TEXT COMMENT'),
( 'Referred'),
( 'Reflexed to PCR Assay'),
( 'Refused%'),
( 'REORDER%'),
( 'Replaced'),
( 'REPORTED TO%'),
( 'REQUEST CREDITED'), 
( 'Result Invalid. The presence or absence of COVID-19 Viral RNAs cannot be determined. At a low frequency, clinical samples can contain inhibitors that may generate invalid results.'), 
( 'Results do not belong to this patient. Laboratory'),
( 'Result may be falsely elevated%'),
( 'Result not cal... mg/dL (calc)'), 
( 'Results not reliable with moderate/severe specimen hemolysis. A repeat'),
( 'RESULT NOT REPORTED, HEMOLYSIS'), 
( 'RESULT NOT REPORTED, ICTERUS'),
( 'RESULT NOT REPORTED, LIPEMIA'), 
( 'Result NOT VALID. Recommend collection of a new specimen.'), 
( 'Result Scanned'),
( '(Results below)'),
( 'Results Scanned'),
( 'RESULT NOT CALCULATED'),
( 'Results Viewable in Attached Link:'),
( 'RETEST'),
( '(Revised) SEE TEXT'),
( '(Revised) TNP'),
( 'RPR: RAECTIVE'),
( 'RPR:Reactive'),
( 'SAMPLE CONTAMINATED'),
( 'Sample not labeled according to JCAHO standards.'),
( 'Sample sent to reference lab. Test results to be sent to ordering provider by reference lab.'),
( 'Sample sent to reference lab. Results communicated to provider directly.'), 
( 'sars cov 2 Broad Institute 9/27/2021'), 
( 'SARS COV 2 9/27/2021 BROAD INSTITUTE'),
( 'SARS COV 2 9/28/2021 BROAD INSTITUTE'),
( 'SARS COV 2 RNA PANEL, PROBE, RESPIRATORY SPECIMEN'),
( 'SCANNED'),
( 'See '),
( '(see '),
( 'SEE ATTACH%'), 
( 'SEE BELOW%'),
( 'See Comment%'),
( 'See Dilution%'),
( 'SEE FAXED%'),
( 'SEE FINAL%'),
( 'See HBSAG Confirmation%'),
( 'See Hepatitis B Surface Antigen confirmation%'),
( 'SEE MANUAL%'), 
( 'SEE MEDICAL%'),
( 'See%module%'),
( 'SEE NOTE%'),
( '(SEE NOTE)'),
( 'seenote'),
( '**SEE NOTE**'),
( 'see result%'),
( 'see same%'),
( 'See Scan%'),
( 'See text%'),
( 'SEE WESTERN BLOT%'), 
( 'Seen'),
( 'SEND OUT TO CAPE COD HOSPITAL'), 
( 'SENDING TO BCH'),
( 'SENDING TO MGH UNDER CODE SCVGM'), 
( 'SENDOUT TO MAYO MEDICAL LABORATORY ID PERFORMED BY MAYO MEDICAL LABORATORIES'), 
( 'Sent for HIV Confirm'),
( 'SENT TO%'),
( 'SERUM'),
( 'Spec. integrity issue, redraw suggested'),
( 'SPECIMEN APPEARS GROSSLY LIPEMIC'), 
( 'Specimen collected and sent to Broad Institute for testing. Results to follow.'), 
( 'Specimen %hemolyzed%'),
( 'SPECIMEN LEAKED IN TRANSIT TO REFERENCE LAB'), 
( 'SPECIMEN MISLABELED, RESULTS DO NOT BELONG TO THIS PATIENT'), 
( 'SPECIMEN NOT APPROPRIATE%'),
( 'Specimen Not Received%'),
( 'SPECIMEN RECEIVED%'),
( 'SPECIMEN RELOGGED'),
( 'Specimen sent to State lab.'),
( 'Specimen slightly hemolyzed.  Result may be falsely elevated.'), 
( 'Specimen slightly hemolyzed.  Result may be affected, interpret with caution.'),
( 'Specimen testing being performed in-house.'),
( 'Specimen too hemolyzed to test.'), 
( 'Specimen too old to process'), 
( 'SPECIMEN TYPE NOT APPROPRIATE FOR TEST REQUESTED'), 
( 'SPECIMEN TYPE HAS NOT BEEN VALIDATED FOR TEST REQUESTED'),
( 'SPECIMEN UNACCEPTABLE%'),
( 'Specimen unsatisfactory for evaluation'),
( 'Specimen was received in laboratory with incorrect patient identification. The requested testing will not be performed. Healthcare provider notified:'), 
( 'Specimen was received in the Microbiology Laboratory and being processed to send out to the Massachusetts State Lab, results to follow.'),
( 'SPRCS'), 
( 'SST TUBE LEFT UNSPUN AT OFFICE OVERNIGHT, CANNOT BE USED FOR TESTING'),
( 'STABILITY LIMIT EXCEEDED%'),
( 'SUBMIT NEW SPEC/ORIGINAL UNSATIS'),
( 'SUSPECTED I.V. CONTAMINATION'),
( 'SWAB'),
( 'SWAB COLLECTED IN MEDIA NWH PLATFORMS ARE NOT VALIDATED FOR. ON CALL PHYSICIAN PAGED AND NOTIFIED.'), 
( 'Swab Not Received'), 
( 'Symptomatic'),
( 'Syphilis Ab CMIA'),
( 'SYPHILIS ANTIBODY CASCADING REFLEX'),
( 'TEST'),
( 'Test cancelled%'),
( 'Test component not applicable or not reported.'), 
( 'TEST CREDITED%'), 
( 'Test currently unavailable due to manufacturer reagent shortage.'), 
( 'TEST INCORRECTLY%'), 
( 'Test not done'), 
( 'Test not indicated%'),
( 'Test not performed%'),
( 'TEST ORDERED IN ERROR%'), 
( 'TEST PERFORMED%'),
( 'TESTING'),
( 'TESTING.'),
( 'Testing performed in NWH Microbiology'),
( 'Test received-See reflex to IDDL test SARS CoV2 (COVID-19) Virus RT-PCR'),
( 'Test reordered%'),
( 'Test replaced'),
( 'Testing sent to Quest'),
( 'Test sent to reference laboratory.'),
( 'Test(s) cancelled by Care Unit.'), 
( 'Test(s) cancelled by M.D.'), 
( 'Test with result pen'),
( 'Tested'),
( 'Testing CANCELLED per Microbiology Director due to COVID 19 workflow management.'), 
( 'TESTING CREDIT'), 
( 'TESTING DONE IN HOUSE'),
( 'TESTING INCOMPLETE DUE TO LABORATORY ACCIDENT'), 
( 'testing moved to inhouse test'), 
( 'Testing performed at Quest'),
( 'Testing platform%'),
( 'The BioFire Diagnostics respiratory panel is targeted  for Coronavirus 229E, HKU1, NL63, and OC43. BioFire Diagnostics has indicated that reactivity in this assay does NOT predict cross-reactivity with the 2019-nCoV Coronavirus.'),
( 'THE BUFFER TO PREVERVE'),
( 'The ordering clinician or clinical facility shoul'), 
( 'these results to the Massachusetts Department of P'), 
( 'This specimen was received without adequate identification and cannot be replaced. After consultation with the responsible provider, processing was deemed to be in the best interest of patient care, in accordance with hospital policy.'),
( 'There is non-specific interference by antibody scr'),
( 'THIS CULTURE.'),
( 'THIS SPECIMEN WAS RECEIVED UNLABELLED. TEST(S) CANCELLED. NOTIFIED:'),
( 'This test has not been reviewed by the FDA.'),
( 'THIS RESULT IS INCORRECT FOR THIS PATIENT%'),
( 'TND'),
( 'TNP%'),
( 'TO OLD'),
( 'TOO OLD'),
( 'TOO OLD RECOLLECT'),
( 'Total bilirubin is less than the measureable limit. Therefore, indirect'),
( 'Totally hemolyzed specimen. Unable to perform test'), 
( 'Too Young to Interpret, Final Result to Follow.'), 
( 'TP-PA Testing not performed. This patient has been tested by TP-PA at least twice in the past.'), 
( 'TRANSPORT DELAY TO REFERENCE LAB'), 
( 'TUBE WAS MISLABELLED'), 
( 'UNABLE TO%'),
( 'UNINTERPRETABLE%'), 
( 'Unknown'), 
( 'Unlabeled specimen'),
( 'Unsatisfactory%'),
( 'unsatifactory%'),
( 'URINE'), 
( 'Urine CT Order%'),
( 'Urine GC Order%'),
( 'VAGINAL SPECIMEN'),
( 'Vaginal: Unsatisfactory'),
( 'Valid'),
( 'VOID'),
( 'WARNING: This nonreactive result:'),
( 'When testing is completed, results will be available in Epic Chart Review under the Media tab.'), 
( 'WRONG%'), 
( 'x'),
( 'XWT')
;


create table kre_report.tmp_unmapped_gonorrhea AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name = 'gonorrhea'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:gonorrhea%'; 



CREATE TABLE kre_report.tmp_unmapped_chlamydia AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name = 'chlamydia'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')


	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:chlamydia%';

CREATE TABLE kre_report.tmp_unmapped_hepatitis_a AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_a%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_a%';


CREATE TABLE kre_report.tmp_unmapped_hepatitis_b AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_b%'
	AND test_name not in ('hepatitis_b_core_antigen_general_antibody', 'hepatitis_b_core_ab')
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_b%';

CREATE TABLE kre_report.tmp_unmapped_hepatitis_c AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hepatitis_c%'
	AND c.test_name != 'hepatitis_c_genotype'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_c%';

CREATE TABLE kre_report.tmp_unmapped_hiv AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	--LEFT JOIN (select array_agg(filter_result || '%') filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'hiv%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	AND c.native_code not in ('MDPH-293')
	AND result_string not in ('<30 Detected', '< 20', '< 1.301', 'HIV-1 RNA detected, <20 cp/mL', '<30 DETECTED', '<40')
	AND result_string not ilike '%copies/mL%'
	AND result_string not ilike '<20%'
	AND result_string not ilike '<1.30%'

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hiv%' 
	AND c.native_code not in ('MDPH-293');

-- FOR LYME - UPDATING TO 30 DAYS FOR NOW SINCE IT IS NOT BEING REPORTED YET
CREATE TABLE kre_report.tmp_unmapped_lyme AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'lyme%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '30 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	-- only check if hef events for lyme have been created in the last 2 years
	-- this is so result strings won't be triggered for sites that have labs mapped but are not running hef for the condition
	and exists (select * from hef_event where name ilike 'lx:lyme%' and date >= (CURRENT_DATE - INTERVAL '1 years')::date)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:lyme%';

CREATE TABLE kre_report.tmp_unmapped_rpr AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'rpr%'
	AND c.test_name not ilike 'rpr_riskscape%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	-- rpr excluded positive strings
	AND result_string not in ('REACTIVE', 'WEAKLY REACTIVE', 'POSITIVE', 'Positive', 'Reactivre', 'Reactive', 'reactive', 'Weakly Reactive', 'RPR: REACTIVE', 'Immune', 'TP-PA: REACTIVE', 'TPPA: Reactive', 'laboratory evidence of syphilis infection. Resubmit in 2-4 weeks if clinically indicated.')

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:rpr%';

CREATE TABLE kre_report.tmp_unmapped_tp AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tp-%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tp-%';

CREATE TABLE kre_report.tmp_unmapped_tppa AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tppa%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tppa%';

CREATE TABLE kre_report.tmp_unmapped_vdrl AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'vdrl%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:vdrl%';


CREATE TABLE kre_report.tmp_unmapped_tuberculosis AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'tb_%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tb_%';

CREATE TABLE kre_report.tmp_unmapped_alt AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'alt%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	-- ignore those beginning with a less than symbol as these are expected to not be interpreted
	AND result_string !~ ('^<*')
	AND result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:alt%';

CREATE TABLE kre_report.tmp_unmapped_ast AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'ast%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT', 'Normal')
	AND result_string !~  ('^\d+\.?,?\d?,?\d*$') 
	AND ((result_float >= 100 and ref_high_float is null) or (result_float >= ref_high_float*2) or result_float is null)
	-- no provision for handling non-numeric results for ast
	AND result_string !~ '^(<|>)'
	AND result_string not in ('Above linear range')

	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:ast%';

CREATE TABLE kre_report.tmp_unmapped_bilirubin AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE 
	   (
	   (c.test_name ilike 'bilirubin_direct' and result_string not ilike '>%' and result_string not ilike '<%')
	   or (c.test_name = 'bilirubin_indirect' and result_string not ilike '>%' and result_string not ilike '<%' and result_string not ilike '%MG/DL (CALC)%')
  		--bilirubin_total does not create negative_events so these need to be filtered out
	   or (c.test_name = 'bilirubin_total' and result_float >= 1.5)
			--and result_string not ilike '<%'
			--and result_string not ilike '>%'
			--and upper(result_string) not in ('BELOW LINEAR RANGE','UL', 'DL', 'NEGATIVE','NEG','N/A')
			--and result_string not ilike '%below assay range%'
			--and (result_float is not null and result_float != ''))
			--and (result_float > 1.5 or result_float is not null)) 
	   )
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:bilirubin%';


CREATE TABLE kre_report.tmp_unmapped_fta AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'fta%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:fta%';

CREATE TABLE kre_report.tmp_unmapped_cd4 AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'cd4%'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	and result_string !~  ('^\d+\.?,?\d?,?\d*$') 

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:cd4%';



CREATE TABLE kre_report.tmp_unmapped_covid19 AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE ( c.test_name = 'covid19_pcr' 
		   -- rogue PCR result
		   AND result_string not in ('2')
	        OR c.test_name IN ('covid19_igg', 'covid19_igm', 'covid19_iga', 'covid19_ab_total', 'covid19_ag')
	      )
        AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT', 'NASOPHARYNX')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:covid%';
	
	
	
CREATE TABLE kre_report.tmp_unmapped_influenza AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
        WHERE c.test_name in  ('influenza', 'rapid_flu', 'influenza_culture')
        AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:influenza%' or name ilike 'lx:rapid_flu%');
	
	
	
CREATE TABLE kre_report.tmp_unmapped_covid_suspect AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
        FROM emr_labresult l
        JOIN conf_labtestmap c on (l.native_code = c.native_code)
        LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
        WHERE c.test_name in  ('h_metapneumovirus', 'parainfluenza', 'adenovirus', 'rhino_entero_virus', 'coronavirus_non19', 'm_pneumoniae_igm',
		      'm_pneumoniae_pcr', 'c_pneumoniae_igm', 'c_pneumoniae_pcr', 'rsv', 'parapertussis')
        AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
        and result_string is not null and result_string != ''
        and result_string NOT ILIKE ALL(filter_array)
        and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')

        EXCEPT

        select h.object_id, l.native_code, result_string, c.test_name, l.date
        from hef_event h
        JOIN emr_labresult l on (h.object_id = l.id)
        JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:h_metapneumovirus%' or name ilike 'lx:parainfluenza%' or name ilike 'lx:adenovirus%' or name ilike 'lx:rhino_entero_virus%' or name ilike 'lx:coronavirus_non19%' or name ilike 'lx:m_pneumoniae_igm%'
	       or name ilike 'lx:m_pneumoniae_pcr%' or name ilike 'lx:c_pneumoniae_igm%' or name ilike 'lx:c_pneumoniae_pcr%' or name ilike 'lx:rsv%' or name ilike 'lx:parapertussis%');	
		   
		   
create table kre_report.tmp_unmapped_anaplasmosis AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE c.test_name ilike 'anaplasmosis%'
	AND c.test_name != 'anaplasmosis not a test'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE name ilike 'lx:anaplasmosis%'; 
	
create table kre_report.tmp_unmapped_babesiosis AS
SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l
	JOIN conf_labtestmap c on (l.native_code = c.native_code)
	LEFT JOIN (select array_agg(filter_result) filter_array from kre_report.tmp_unmapped_filter) f on (1=1)
	WHERE (c.test_name ilike 'babesiosis%' or c.test_name = 'tick_bloodsmear')
	AND c.test_name != 'babesiosis not a test'
	AND date >= (CURRENT_DATE - INTERVAL '10 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	and result_string is not null and result_string != ''
	and result_string NOT ILIKE ALL(filter_array)
	and result_string not in ('.', '*', '-', 'n', 'na', 'NA', 'nd', 'REPORT')
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h
	JOIN emr_labresult l on (h.object_id = l.id)
	JOIN conf_labtestmap c  on (l.native_code = c.native_code)
    WHERE (name ilike 'lx:babesiosis%' or name ilike 'lx:tick_bloodsmear%'); 
	
	

CREATE TABLE kre_report.tmp_unmapped_all AS
SELECT * from kre_report.tmp_unmapped_gonorrhea
UNION
SELECT * from kre_report.tmp_unmapped_chlamydia
UNION 
SELECT * from kre_report.tmp_unmapped_hepatitis_a
UNION 
SELECT * from kre_report.tmp_unmapped_hepatitis_b
UNION
SELECT * FROM kre_report.tmp_unmapped_hepatitis_c
UNION
SELECT * FROM kre_report.tmp_unmapped_hiv
UNION
SELECT * FROM kre_report.tmp_unmapped_lyme
UNION
SELECT * FROM kre_report.tmp_unmapped_rpr
UNION
SELECT * FROM kre_report.tmp_unmapped_tp
UNION
SELECT * FROM kre_report.tmp_unmapped_tppa
UNION
SELECT * FROM kre_report.tmp_unmapped_vdrl
UNION
SELECT * FROM kre_report.tmp_unmapped_tuberculosis
UNION
SELECT * FROM kre_report.tmp_unmapped_alt
UNION
SELECT * FROM kre_report.tmp_unmapped_ast
UNION
SELECT * FROM kre_report.tmp_unmapped_bilirubin
UNION
SELECT * FROM kre_report.tmp_unmapped_fta
UNION
SELECT * FROM kre_report.tmp_unmapped_cd4
UNION
SELECT * FROM kre_report.tmp_unmapped_covid19
UNION
SELECT * FROM kre_report.tmp_unmapped_influenza
UNION
SELECT * FROM kre_report.tmp_unmapped_covid_suspect
UNION
SELECT * FROM kre_report.tmp_unmapped_anaplasmosis
UNION
SELECT * FROM kre_report.tmp_unmapped_babesiosis;


CREATE TABLE kre_report.unmapped_result_strings_report AS
select count(*), native_code, result_string, test_name
from kre_report.tmp_unmapped_all
group by result_string, test_name, native_code
having count(*) >= 2
ORDER BY test_name, native_code, count(*) desc;

SELECT * FROM kre_report.unmapped_result_strings_report;


\COPY kre_report.unmapped_result_strings_report  TO '/tmp/unmapped_result_strings_report.csv' DELIMITER ',' CSV HEADER;


DROP TABLE IF EXISTS kre_report.tmp_unmapped_gonorrhea;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_chlamydia;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_a;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_b;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hepatitis_c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_hiv;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_lyme;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_rpr;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tp;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tppa;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_vdrl;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_pertussis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_tuberculosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_a1c;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_alt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ast;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_bilirubin;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ogtt;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_fta;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_islet_cell_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_c_peptide;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_cd4;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_gad65;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_glucose;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_ica512;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_insulin_antibody;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid19;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_influenza;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_covid_suspect;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_anaplasmosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_babesiosis;
DROP TABLE IF EXISTS kre_report.tmp_unmapped_all;

	


