-- create a table with the hef events and case information

create table kre_temp as
select l.id as lab_id, coalesce(c.id, null) as case_id, h.name as hef_name, h.id as hef_id
from hef_event h
LEFT JOIN nodis_case c ON (c.patient_id = h.patient_id and condition = 'hepatitis_c')
INNER JOIN emr_labresult l ON (h.patient_id = l.patient_id and h.object_id = l.id)
where l.native_code in ('2001033--612--', 'LAB1033--612--' );

-- delete from "sent" case reports
-- More cleanup may be required here if you want to fully delete the cases!
delete from nodis_case_events where event_id in (select hef_id from kre_temp);

-- only if you want to fully delete the cases
delete from nodis_case_events where case_id in (select case_id from kre_temp);

-- only run this if you want to fully delete the cases
delete from nodis_case where id in (select case_id from kre_temp);

-- delete the hef events
delete from hef_event where id in (select hef_id from kre_temp);

-- Make sure to remap and run hef again
-- Now run nodis
-- Now run case_requeue (for S and RS)
-- 