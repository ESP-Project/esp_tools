SET client_min_messages TO WARNING;

DROP TABLE IF EXISTS tmp_unmapped_gonorrhea_numeric;
DROP TABLE IF EXISTS tmp_unmapped_chlamydia_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_a_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_b_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_c_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hiv_numeric;
DROP TABLE IF EXISTS tmp_unmapped_lyme_numeric;
DROP TABLE IF EXISTS tmp_unmapped_rpr_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tp_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tppa_numeric;
DROP TABLE IF EXISTS tmp_unmapped_vdrl_numeric;
DROP TABLE IF EXISTS tmp_unmapped_pertussis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tuberculosis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_a1c_numeric;
DROP TABLE IF EXISTS tmp_unmapped_alt_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ast_numeric;
DROP TABLE IF EXISTS tmp_unmapped_bilirubin_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ogtt_numeric;
DROP TABLE IF EXISTS tmp_unmapped_fta_numeric;
DROP TABLE IF EXISTS tmp_unmapped_islet_cell_antibody_numeric;
DROP TABLE IF EXISTS tmp_unmapped_c_peptide_numeric;
DROP TABLE IF EXISTS tmp_unmapped_cd4_numeric;
DROP TABLE IF EXISTS tmp_unmapped_gad65_numeric;
DROP TABLE IF EXISTS tmp_unmapped_giardiasis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_glucose_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ica512_numeric;
DROP TABLE IF EXISTS tmp_unmapped_insulin_antibody_numeric;
DROP TABLE IF EXISTS tmp_unmapped_all_numeric;

DROP TABLE IF EXISTS unmapped_result_strings_report_numeric;

CREATE TABLE tmp_unmapped_gonorrhea_numeric AS 
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name = 'gonorrhea'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:gonorrhea%';


CREATE TABLE tmp_unmapped_chlamydia_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name = 'chlamydia'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)
	
	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:chlamydia%'; 

CREATE TABLE tmp_unmapped_hepatitis_a_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'hepatitis_a%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_a%';

CREATE TABLE tmp_unmapped_hepatitis_b_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'hepatitis_b%'
	--AND (test_name  = 'hepatitis_b_viral_dna' and result_string not in ('5', '1')
	--	OR (test_name =  'hepatitis_b_surface_antigen' and result_string not in ('3', '1'))
	--	OR (test_name = 'hepatitis_b_core_antigen_igm_antibody' and result_string not in ('2', '0.2', '66'))
	--	)
	--AND test_name NOT IN ('hepatitis_b_core_antigen_general_antibody', 'hepatitis_b_e_antigen')
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_b%'; 


CREATE TABLE tmp_unmapped_hepatitis_c_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'hepatitis_c%'
	--AND (test_name = 'hepatitis_c_signal_cutoff'
	--	OR test_name = 'hepatitis_c_elisa'
	--	OR (test_name = 'hepatitis_c_riba' and result_string not in ('0.1', '0.03', '2', '0.03'))
	--	OR test_name = 'hepatitis_c_genotype'
	--	OR test_name = 'hepatitis_c_rna')
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	--AND result_string not in ('12')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hepatitis_c%' 
	AND ref_high_float is null;

-- hiv_rna_viral has a threshold within the plugin
CREATE TABLE tmp_unmapped_hiv_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND test_name ilike 'hiv%'
	AND (c.test_name in ('hiv_wb','hiv_pcr', 'hiv_elisa', 'hiv_ag_ab') or (c.test_name = 'hiv_rna_viral' and result_float > 200 or result_float is null))
	--	c.test_name = 'hiv_wb'
	--	OR (c.test_name = 'hiv_pcr' and result_string not in ('1'))
	--	OR c.test_name = 'hiv_elisa'
	--	OR (c.test_name = 'hiv_rna_viral' and (result_float > 200 or result_float is null))
	--	OR (c.test_name = 'hiv_ag_ab' and result_string not in ('0.14'))
	--	)
	--AND c.test_name not in ('hiv_rna_viral')
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:hiv%'; 
	


CREATE TABLE tmp_unmapped_lyme_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'lyme%'
	--AND c.threshold is null
	--AND ref_high_float is null
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:lyme%';

CREATE TABLE tmp_unmapped_rpr_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'rpr%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)
	--AND result_string not in ('6')

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:rpr%'; 


CREATE TABLE tmp_unmapped_tp_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'tp-%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tp-%';


CREATE TABLE tmp_unmapped_tppa_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'tppa%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tppa%'; 

CREATE TABLE tmp_unmapped_vdrl_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'vdrl%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:vdrl%'; 

CREATE TABLE tmp_unmapped_tuberculosis_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'tuberculosis%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:tb%'; 

CREATE TABLE tmp_unmapped_alt_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'alt%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)
	AND ((result_float >= 200 and ref_high_float is null) or (result_float >= ref_high_float*2) or result_float is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:alt%'; 

CREATE TABLE tmp_unmapped_ast_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'ast%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)
	AND ((result_float >= 100 and ref_high_float is null) or (result_float >= ref_high_float*2) or result_float is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:ast%'; 

CREATE TABLE tmp_unmapped_bilirubin_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'bilirubin%'
	AND (c.test_name in ('bilirubin_direct', 'bilirubin_indirect') or (c.test_name = 'bilirubin_total' and result_float > 1.5 or result_float is null))
	--AND 
	--(
	--c.test_name in ('bilirubin_direct', 'bilirubin_indirect')
	--OR (c.test_name = 'bilirubin_total' and (result_float > 1.5 or result_float is null)
	--	and result_string not ilike '<%' 
	--	and result_string not in ('Below linear range','UL', 'DL', 'NEGATIVE','Negative', 'NEG', 'NEGATIVE', 'N/A')
	 --  )
	--)																			
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:bilirubin%'; 
	
CREATE TABLE tmp_unmapped_fta_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'fta%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:fta%';

CREATE TABLE tmp_unmapped_cd4_numeric AS
	SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	FROM emr_labresult l, conf_labtestmap c
	WHERE l.native_code = c.native_code
	AND c.test_name ilike 'cd4%'
	AND date >= (CURRENT_DATE - INTERVAL '2 years')::date AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	AND result_string is not null and result_string != ''
	AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	AND (ref_high_float is null or threshold is null)

	EXCEPT

	select h.object_id, l.native_code, result_string, c.test_name, l.date
	from hef_event h, emr_labresult l, conf_labtestmap c 
	where h.object_id = l.id
	AND l.native_code = c.native_code
	AND h.object_id = l.id
	AND name ilike 'lx:cd4%';
	
	
-- CREATE TABLE tmp_unmapped_pertussis_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'pertussis%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
--	AND name ilike 'lx:pertussis%';

-- CREATE TABLE tmp_unmapped_a1c_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'a1c%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)
	-- and (result_float >= 5.7 or result_float is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:a1c%';
	
-- CREATE TABLE tmp_unmapped_ogtt_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND ((c.test_name in (
		-- 'ogtt100-fasting-urine',
		-- 'ogtt50-random',		
		-- 'ogtt75-1hr',
		-- 'ogtt75-2hr',
		-- 'ogtt75-30min',
		-- 'ogtt75-90min',
		-- 'ogtt75-fasting-urine',
		-- 'ogtt75-fasting',
		-- 'ogtt75-order'))
		-- OR
			-- (c.test_name = 'ogtt100-1hr' and (result_float > 180 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-2hr' and (result_float >= 155 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-30min' and (result_float >= 200 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-3hr' and (result_float >= 140 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-4hr' and (result_float >= 140 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-5hr' and (result_float >= 140 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-90min' and (result_float >= 180 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt100-fasting' and (result_float >= 95 or result_float is null))
		-- OR
			-- (c.test_name = 'ogtt50-fasting' and (result_float >= 100 or result_float is null))
		-- OR 
			-- (c.test_name = 'ogtt50-1hr' and (result_float > 190 or result_float is null))
		-- )
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:ogtt%';
	
	
-- CREATE TABLE tmp_unmapped_islet_cell_antibody_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'islet-cell-antibody%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:islet-cell-antibody%';

-- CREATE TABLE tmp_unmapped_c_peptide_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'c-peptide%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (result_float < .8 or result_float is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:c-peptide%';



-- CREATE TABLE tmp_unmapped_gad65_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'gad65%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:gad65%' 
	-- and result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$');

-- CREATE TABLE tmp_unmapped_giardiasis_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'giardiasis%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:giardiasis%' 
	-- and l.date >= '01-01-2017' 
	-- and l.date <= '12-31-2017'
	-- and result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$');


-- CREATE TABLE tmp_unmapped_glucose_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND (
		-- (c.test_name = 'glucose-random' and (result_float >= 140 or result_float is null) and result_string not in ('See Fasting Glucose', 't') and result_string !~* 'neg|trace')  
		 -- OR (c.test_name = 'glucose_fasting' and (result_float >= 100 or result_float is null) )
		-- )
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- --AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:glucose%';

-- CREATE TABLE tmp_unmapped_ica512_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'ica512%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- AND (ref_high_float is null or threshold is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:ica512%';

-- CREATE TABLE tmp_unmapped_insulin_antibody_numeric AS
	-- SELECT l.id, l.native_code, l.result_string, c.test_name, l.date
	-- FROM emr_labresult l, conf_labtestmap c
	-- WHERE l.native_code = c.native_code
	-- AND c.test_name ilike 'insulin-antibody%'
	-- AND date >= '01-01-2006' AND date < (CURRENT_DATE - INTERVAL '3 days')::date
	-- AND result_string is not null and result_string != ''
	-- AND result_string ~ ('^\d+\.?,?\d*,?\d*$')
	-- --AND (ref_high_float is null or threshold is null)
	-- AND (result_float > .8 or result_float is null)

	-- EXCEPT

	-- select h.object_id, l.native_code, result_string, c.test_name, l.date
	-- from hef_event h, emr_labresult l, conf_labtestmap c 
	-- where h.object_id = l.id
	-- AND l.native_code = c.native_code
	-- AND h.object_id = l.id
	-- AND name ilike 'lx:insulin-antibody%';

		-- PAUSED HERE ON UPDATING!!!

	

CREATE TABLE tmp_unmapped_all_numeric AS
SELECT * from tmp_unmapped_gonorrhea_numeric
UNION
SELECT * from tmp_unmapped_chlamydia_numeric
UNION 
SELECT * from tmp_unmapped_hepatitis_a_numeric
UNION 
SELECT * from tmp_unmapped_hepatitis_b_numeric
UNION
SELECT * FROM tmp_unmapped_hepatitis_c_numeric
UNION
SELECT * FROM tmp_unmapped_hiv_numeric
UNION
SELECT * FROM tmp_unmapped_lyme_numeric
UNION
SELECT * FROM tmp_unmapped_rpr_numeric
UNION
SELECT * FROM tmp_unmapped_tp_numeric
UNION
SELECT * FROM tmp_unmapped_tppa_numeric
UNION
SELECT * FROM tmp_unmapped_vdrl_numeric
UNION
SELECT * FROM tmp_unmapped_tuberculosis_numeric
UNION
SELECT * FROM tmp_unmapped_alt_numeric
UNION
SELECT * FROM tmp_unmapped_ast_numeric
UNION
SELECT * FROM tmp_unmapped_bilirubin_numeric
UNION
SELECT * FROM tmp_unmapped_fta_numeric
UNION
SELECT * FROM tmp_unmapped_cd4_numeric;

--SELECT * FROM tmp_unmapped_pertussis_numeric
--UNION
--SELECT * FROM tmp_unmapped_a1c_numeric
--UNION
--SELECT * FROM tmp_unmapped_ogtt_numeric
--UNION
--SELECT * FROM tmp_unmapped_islet_cell_antibody_numeric
--UNION
--SELECT * FROM tmp_unmapped_c_peptide_numeric
--UNION
--SELECT * FROM tmp_unmapped_gad65_numeric
--UNION
--SELECT * FROM tmp_unmapped_giardiasis_numeric
--UNION
--SELECT * FROM tmp_unmapped_glucose_numeric
--UNION
--SELECT * FROM tmp_unmapped_ica512_numeric
--UNION
--SELECT * FROM tmp_unmapped_insulin_antibody_numeric;

CREATE TABLE unmapped_result_strings_report_numeric AS
select count(*), native_code, result_string, test_name
from tmp_unmapped_all_numeric
group by result_string, test_name, native_code
having count(*) > 1
ORDER BY test_name, native_code, count(*) desc;

SELECT * FROM unmapped_result_strings_report_numeric;



DROP TABLE IF EXISTS tmp_unmapped_gonorrhea_numeric;
DROP TABLE IF EXISTS tmp_unmapped_chlamydia_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_a_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_b_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hepatitis_c_numeric;
DROP TABLE IF EXISTS tmp_unmapped_hiv_numeric;
DROP TABLE IF EXISTS tmp_unmapped_lyme_numeric;
DROP TABLE IF EXISTS tmp_unmapped_rpr_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tp_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tppa_numeric;
DROP TABLE IF EXISTS tmp_unmapped_vdrl_numeric;
DROP TABLE IF EXISTS tmp_unmapped_pertussis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_tuberculosis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_a1c_numeric;
DROP TABLE IF EXISTS tmp_unmapped_alt_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ast_numeric;
DROP TABLE IF EXISTS tmp_unmapped_bilirubin_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ogtt_numeric;
DROP TABLE IF EXISTS tmp_unmapped_fta_numeric;
DROP TABLE IF EXISTS tmp_unmapped_islet_cell_antibody_numeric;
DROP TABLE IF EXISTS tmp_unmapped_c_peptide_numeric;
DROP TABLE IF EXISTS tmp_unmapped_cd4_numeric;
DROP TABLE IF EXISTS tmp_unmapped_gad65_numeric;
DROP TABLE IF EXISTS tmp_unmapped_giardiasis_numeric;
DROP TABLE IF EXISTS tmp_unmapped_glucose_numeric;
DROP TABLE IF EXISTS tmp_unmapped_ica512_numeric;
DROP TABLE IF EXISTS tmp_unmapped_insulin_antibody_numeric;
DROP TABLE IF EXISTS tmp_unmapped_all_numeric;

--TEST
