DROP TABLE IF EXISTS kre_report.rpr_confirm_result_to_hef;
CREATE TABLE kre_report.rpr_confirm_result_to_hef as
SELECT count(*), c.test_name, c.threshold, l.native_code, l.native_name, l.result_string, h.name as hef_name
FROM emr_labresult l, hef_event h, conf_labtestmap c
WHERE l.id = h.object_id
AND l.native_code = c. native_code
AND h.name ilike 'lx:%'
AND c.test_name = 'rpr'
AND l.date >= '01-01-2019' AND l.date < (CURRENT_DATE - INTERVAL '3 days')::date
GROUP by l.native_code, l.native_name, c.threshold, l.result_string, c.test_name, h.name;

SELECT * from kre_report.rpr_confirm_result_to_hef order by result_string;