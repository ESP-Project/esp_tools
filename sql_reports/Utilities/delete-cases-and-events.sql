
-- Define the cases to be deleted
create table tmp_cases_to_delete as
select id as nodis_case
from nodis_case
where condition = 'syphilis';

-- First you need to delete all of the nodis_case_events for the case
delete from nodis_case_events 
where case_id in (select nodis_case from tmp_cases_to_delete);

-- Next delete the nodis_report_cases
delete from nodis_report_cases where case_id in (select nodis_case from tmp_cases_to_delete);

-- Next delete from nodis_reported
delete from nodis_reported where case_reported_id in 
	(select id from nodis_casereportreported  where case_report_id in 
		(select id from nodis_casereport T1, tmp_cases_to_delete T2 
	 	where T1.case_id = T2.nodis_case));

-- Next delete from nodis_casereportreported
delete from nodis_casereportreported 
where case_report_id in 
	(select id from nodis_casereport T1, tmp_cases_to_delete T2 
	 where T1.case_id = T2.nodis_case);

-- Next delete from nodis_report
delete from nodis_casereport 
where case_id in (select nodis_case from tmp_cases_to_delete);

-- Delete from nodis_caseactivehistory
-- Seeing 0 deletes is often normal
delete from nodis_caseactivehistory
where case_id in (select nodis_case from tmp_cases_to_delete);

-- Delete from nodis_casetimespans
-- Seeing 0 deletes is often normal
delete from nodis_case_timespans
where case_id in (select nodis_case from tmp_cases_to_delete);

-- Delete from nodis_casestatushistory
-- Seeing 0 deletes is often normal
delete from nodis_casestatushistory
where case_id in (select nodis_case from tmp_cases_to_delete);

-- Next delete the cases
delete from nodis_case 
where id in (select nodis_case from tmp_cases_to_delete);


-- Next delete ALL the hef_events. Not just ones associated to cases.
delete from hef_event 
where name ilike 'lx:rpr:%'
or name ilike 'lx:vdrl%'
or name ilike 'lx:tppa%'
or name ilike 'lx:fta-abs%'
or name ilike 'lx:tp-%';

-- delete the temporary table
drop table tmp_cases_to_delete;

-- Now run hef again
-- Now run nodis again for the condition 