--SQL to Create a listing of labs useful for assigning output and snomed values in the conf_labtestmap table
-- update the date of the file generated
-- added max(date) and min(date) 

--chlamydia
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('chlamydia') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/labmappings-ESP-chlamydia-2021-06-01.csv' WITH CSV HEADER;

--gonorrhea
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('gonorrhea') group by c.id, l.native_code, native_name, procedure_name, test_name, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-gonorrhea-2021-06-01.csv' WITH CSV HEADER;

--syphilis
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('rpr', 'vdrl', 'tppa', 'fta-abs', 'tp-igg', 'tp-cia', 'tp-igm', 'vdrl-csf', 'tppa-csf', 'fta-abs-csf') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-syphilis-2021-06-01.csv' WITH CSV HEADER;


--tuberculosis
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('tb_afb', 'tb_igra') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-tuberculosis-2021-06-01.csv' WITH CSV HEADER;

--hepatits_a
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('hepatitis_a_igm_antibody', 'alt', 'ast') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepA-2021-06-01.csv' WITH CSV HEADER;

--hepatitis_b
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and (test_name ilike 'hepatitis_b%' or test_name in ('ast', 'bilirubin_total', 'alt')) group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepB-2021-06-01.csv' WITH CSV HEADER;

--hepatitis_c
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and (test_name ilike 'hepatitis_c%' or test_name in ('bilirubin_total', 'alt')) group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hepC-2021-06-01.csv' WITH CSV HEADER;

--tuberculosis
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('tb_afb', 'tb_igra') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-tuberculosis-2021-06-01.csv' WITH CSV HEADER;

--hiv
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name ilike '%hiv_%' group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/lab-mappings-ESP-hiv-2021-08-27.csv' WITH CSV HEADER;

--covid_confirmed
--hiv
\COPY (select count(*), min(date) as min_date, max(date) as max_date, c.id as test_id, l.native_code, native_name, procedure_name, test_name, threshold, output_code as loinc, snomed_pos, snomed_neg, snomed_ind from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name in ('covid19_pcr', 'covid19_ag') group by c.id, l.native_code, native_name, procedure_name, test_name, threshold, output_code, snomed_pos, snomed_neg order by test_name, native_code) TO '/tmp/covid-confirmed-esp-lab-mappings.csv' WITH CSV HEADER;

