select * 
into temp_hold_caseactivehistory
from nodis_caseactivehistory;
truncate nodis_caseactivehistory restart identity;
insert into nodis_caseactivehistory (status, date, change_reason, latest_event_date, object_id, case_id, content_type_id)
(select  status, date, change_reason, latest_event_date, object_id, case_id, content_type_id
from temp_hold_caseactivehistory);
--drop table temp_hold_caseactivehistory;