-- IDENTIFY POS AND NEG ON THE SAME DATE
select T5.id as case_id, T5.date as case_date, T1.patient_id, T1.name as neg_hef, T2.name as pos_hef, T1.date as neg_date, T2.date as pos_date, T3.native_code as neg_lab_code, T3.result_string as neg_result,
T3.id as neg_labid, T4.native_code as pos_lab_code, T4.result_string as pos_result, T4.id as pos_labid
FROM hef_event T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
LEFT JOIN emr_labresult T3 ON (T1.patient_id = T3.patient_id and T3.id = T1.object_id)
LEFT JOIN emr_labresult T4 ON (T2.patient_id = T4.patient_id and T4.id = T2.object_id)
LEFT JOIN nodis_case T5 ON (T1.patient_id = T5.patient_id and T5.condition = 'hepatitis_b')
WHERE T1.name = 'lx:hepatitis_b_viral_dna:negative'
AND T2.name = 'lx:hepatitis_b_viral_dna:positive'
AND T1.date > T5.date --neg events on case date are not attached

CODES TO FIX
---------------
'LAB556--22115'
'87517--8037'
'87517--8036'
'87517--8038'
'87517--7905'

STRING FIXES
-----------
87517--2413
'87517--2414'



-- IDENTIFY POS AND NEG ON THE SAME DATE
select T5.id as case_id, T5.date as case_date, T1.patient_id, T1.name as neg_hef, T2.name as pos_hef, T1.date as neg_date, T2.date as pos_date, T3.native_code as neg_lab_code, T3.result_string as neg_result,
T3.id as neg_labid, T4.native_code as pos_lab_code, T4.result_string as pos_result, T4.id as pos_labid
FROM hef_event T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
LEFT JOIN emr_labresult T3 ON (T1.patient_id = T3.patient_id and T3.id = T1.object_id)
LEFT JOIN emr_labresult T4 ON (T2.patient_id = T4.patient_id and T4.id = T2.object_id)
LEFT JOIN nodis_case T5 ON (T1.patient_id = T5.patient_id and T5.condition = 'hepatitis_c')
WHERE T1.name = 'lx:hepatitis_c_rna:negative'
AND T2.name = 'lx:hepatitis_c_rna:positive'
AND T1.date > T5.date --neg events on case date are not attached




select count(*) 
FROM (
select patient_id, date as case_date, date_part('year', age(now(), date_of_birth)) current_age, date_part('year', age(T1.date, date_of_birth)) age_on_case_date, gender
from nodis_case T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE status = 'AR_PENDING_TO_DPH'
AND condition = 'hepatitis_b'
AND gender in ('F') ) foo
where current_age >= 14
and current_age <= 49



drop table if exists kre_report.hep_b_remap_hef;
create table kre_report.hep_b_remap_hef as
select T2.result_string, T1.id as hef_id, T1.name, T1.patient_id, T1.date as hef_date
from hef_event T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id and T1.object_id = T2.id)
where T2.native_code = '87517--8037'
and T1.name = 'lx:hepatitis_b_viral_dna:negative'
and (T2.result_float >= 20 or result_string in ('<20 DETECTED'));

delete from hef_event
where id in (select hef_id from kre_report.hep_b_remap_hef);

--now add the string mapping to positive
--rerun event creation
--rerun nodis
--rerun requeue


drop table if exists kre_report.hep_b_remap_nce;
create table kre_report.hep_b_remap_nce as
select * from nodis_case_events
where event_id in (select hef_id from kre_report.hep_b_remap_hef)

delete from nodis_case_events where id in (select id from kre_report.hep_b_remap_nce )




------------------
------------------

select * from conf_labtestmap 
where output_code = '5009-6'
and id not in (select labtestmap_id from conf_labtestmap_extra_negative_strings)
order by id 