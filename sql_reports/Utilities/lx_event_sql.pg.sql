--long running lx events via sql
--labresult:glucose-random:range:gte:140:lt:200
drop table if exists temp_known_lx_evnt; 
Select object_id 
into temporary temp_known_lx_evnt
from hef_event
where name = 'lx:glucose-random:range:gte:140:lt:200';
drop table if exists temp_new_lx;
select distinct lx.id as object_id , lx.provider_id, lx.patient_id, lx.date
into temporary temp_new_lx
from emr_labresult lx
join conf_labtestmap ltm on ltm.native_code=lx.native_code and ltm.test_name='glucose-random'
where not exists (select null from temp_known_lx_evnt klx where klx.object_id=lx.id)
and lx.result_float >= 140 and lx.result_float < 200;
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'lx:glucose-random:range:gte:140:lt:200'::varchar(128) as name, 
  'urn:x-esphealth:heuristic:commoninf:random-glucose:v1'::varchar(250) as source,
  lx.date, current_timestamp as "timestamp", 'raw sql method'::text as note, lx.object_id, ct.id as content_type_id, 
  lx.patient_id, lx.provider_id
from temp_new_lx lx 
join django_content_type ct on app_label='emr' and model='labresult';
--labresult:glucose-random:threshold:gte:200
drop table if exists temp_known_lx_evnt2; 
Select object_id 
into temporary temp_known_lx_evnt2
from hef_event
where name = 'lx:glucose-random:threshold:gte:200';
drop table if exists temp_new_lx2;
select distinct lx.id as object_id , lx.provider_id, lx.patient_id, lx.date
into temporary temp_new_lx2
from emr_labresult lx
join conf_labtestmap ltm on ltm.native_code=lx.native_code and ltm.test_name='glucose-random'
where not exists (select null from temp_known_lx_evnt2 klx where klx.object_id=lx.id)
and lx.result_float >= 200;
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'lx:glucose-random:threshold:gte:200'::varchar(128) as name, 
  'urn:x-esphealth:heuristic:commoninf:random-glucose:v1'::varchar(250) as source,
  lx.date, current_timestamp as "timestamp", 'raw sql method'::text as note, lx.object_id, ct.id as content_type_id, 
  lx.patient_id, lx.provider_id
from temp_new_lx2 lx 
join django_content_type ct on app_label='emr' and model='labresult';
--a1c entries
drop table if exists temp_known_lx_evnt2; 
Select object_id 
into temporary temp_known_lx_evnt2
from hef_event
where name = 'lx:a1c:threshold:lt:7';
drop table if exists temp_new_lx2;
select distinct lx.id as object_id , lx.provider_id, lx.patient_id, lx.date
into temporary temp_new_lx2
from emr_labresult lx
join conf_labtestmap ltm on ltm.native_code=lx.native_code and ltm.test_name='a1c'
where not exists (select null from temp_known_lx_evnt2 klx where klx.object_id=lx.id)
and lx.result_float < 7;
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id)
select 'lx:a1c:threshold:lt:7'::varchar(128) as name, 
  'urn:x-esphealth:heuristic:commoninf:a1c:v1'::varchar(250) as source,
  lx.date, current_timestamp as "timestamp", 'raw sql method'::text as note, lx.object_id, ct.id as content_type_id, 
  lx.patient_id, lx.provider_id
from temp_new_lx2 lx 
join django_content_type ct on app_label='emr' and model='labresult';

