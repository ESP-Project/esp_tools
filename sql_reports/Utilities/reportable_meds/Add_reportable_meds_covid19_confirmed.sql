-- conf_reportablemedication
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'remdesivir', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'prednisone', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'prednisolone', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'methylprednisolone', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'dexamethasone', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'hydrocortisone iv', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'tocilizumab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'sarilumab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'anakinra', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'casirivimab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'bamlanivimab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'nirmatrelvir', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'molnupiravir', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'tixagevimab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'sotrovimab', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'baricitinib', null, 'covid19_confirmed') ON CONFLICT DO NOTHING;


-- static drug_synonym
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Remdesivir', 'Remdesivir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisone', 'Prednisone', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisone', 'Rayos', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisone', 'Sterapred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisone', 'Deltasone', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Prednisolone', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Flo-Pred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Millipred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Orapred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Pediapred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Veripred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Prelone', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Hydeltra', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Hydeltrasol', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Key-Pred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Cotolone', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predicort', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predalone', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predacort', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predate', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predaject', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Pred-ject', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Medicort', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Pri-cortin', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Predcor', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Bubbli-Pred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Prednisolone', 'Asmalpred', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Methylprednisolone', 'Methylprednisolone', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Methylprednisolone', 'Medrol', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Dexamethasone', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Baycadron', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Decadron', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Dexpack', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Taperdex', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Zema-Pak', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Zodex', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dexamethasone', 'Zonacort', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Hydrocortisone IV', 'Hydrocortisone IV', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Hydrocortisone IV', 'Cortef', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tocilizumab', 'Tocilizumab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tocilizumab', 'Actemra', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sarilumab', 'Sarilumab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sarilumab', 'Kevzara', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Anakinra', 'Anakinra', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Anakinra', 'Kineret', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Casirivimab', 'Casirivimab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Casirivimab', 'REGEN-COV', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Bamlanivimab', 'Bamlanivimab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Nirmatrelvir', 'Nirmatrelvir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Nirmatrelvir', 'Paxlovid', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Molnupiravir', 'Molnupiravir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tixagevimab', 'Tixagevimab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Tixagevimab', 'Evusheld', null) ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sotrovimab', 'Sotrovimab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Baricitinib', 'Baricitinib', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Baricitinib', 'Olumiant', null) ON CONFLICT DO NOTHING;















