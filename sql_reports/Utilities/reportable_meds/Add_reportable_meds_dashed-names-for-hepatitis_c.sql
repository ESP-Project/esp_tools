-- conf_reportablemedication
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'dasab-ombitas-paritapre-ritona', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'elbasvir-grazoprevir', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'glecaprevir-pibrentasvir', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'interferon alfa-2a', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'interferon alfa-2b', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'interferon alfacon-1', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'ledipasvir-sofosbuvir', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'ombitas-paritapre-ritona-dasab', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'ombitasvir-paritaprev-ritonav', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'peginterferon alfa-2a', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'peginterferon alfa-2b', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'ribavirin-interferon a-2b', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'ribavirin/interferon a-2b', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'sofosbuvir-velpatasvir', null, 'hepatitis_c') ON CONFLICT DO NOTHING;
INSERT INTO conf_reportablemedication (id, drug_name, notes, condition_id) VALUES (default, 'sofosbuv-velpatasv-voxilaprev', null, 'hepatitis_c') ON CONFLICT DO NOTHING;


-- static drug_synonym
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Dasab-Ombitas-Paritapre-Ritona', 'Dasab-Ombitas-Paritapre-Ritona', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Elbasvir-Grazoprevir', 'Zepatier', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Elbasvir-Grazoprevir', 'Elbasvir-Grazoprevir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Glecaprevir-Pibrentasvir', 'Glecaprevir-Pibrentasvir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Glecaprevir-Pibrentasvir', 'Mavyret', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2a', 'Roferon', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2a', 'Interferon alfa-2a', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2b', 'Interferon alfa-2b', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfa-2b', 'Intron A', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfacon-1', 'Infergen', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Interferon alfacon-1', 'Interferon alfacon-1', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ledipasvir-Sofosbuvir', 'Ledipasvir-Sofosbuvir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ledipasvir-Sofosbuvir', 'Harvoni', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ombitas-Paritapre-Ritona-Dasab', 'Ombitas-Paritapre-Ritona-Dasab', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ombitas-Paritapre-Ritona-Dasab', 'Viekira Pak', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ombitasvir-Paritaprev-Ritonav', 'Ombitasvir-Paritaprev-Ritonav', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ombitasvir-Paritaprev-Ritonav', 'Technivie', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2a', 'Peginterferon alfa-2a', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2a', 'Pegasys', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2b', 'Peginterferon alfa-2b', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2b', 'Pegintron', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Peginterferon alfa-2b', 'Sylatron', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ribavirin-Interferon a-2b', 'Ribavirin-Interferon a-2b', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ribavirin-Interferon a-2b', 'Rebetron', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ribavirin/Interferon a-2b', 'Rebetron', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Ribavirin/Interferon a-2b', 'Ribavirin/Interferon a-2b', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sofosbuvir-Velpatasvir', 'Sofosbuvir-Velpatasvir', 'Self') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sofosbuvir-Velpatasvir', 'Epclusa', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sofosbuv-Velpatasv-Voxilaprev', 'Vosevi', '') ON CONFLICT DO NOTHING;
INSERT INTO static_drugsynonym (drugynonym_id, generic_name, other_name, comment) VALUES (default, 'Sofosbuv-Velpatasv-Voxilaprev', 'Sofosbuv-Velpatasv-Voxilaprev', 'Self') ON CONFLICT DO NOTHING;
