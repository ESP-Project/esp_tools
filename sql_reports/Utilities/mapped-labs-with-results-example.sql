drop table if exists kre_report.flu_mapped;
create table kre_report.flu_mapped as
select count(*), native_name, c.native_code, procedure_name, test_name
from conf_labtestmap c,
emr_labresult l
where c.native_code = l.native_code
and c.test_name in ('influenza', 'rapid_flu', 'influenza_culture')
group by native_name, c.native_code, procedure_name, test_name;

DROP TABLE IF EXISTS kre_report.flu_mapped_result_strings;
CREATE TABLE kre_report.flu_mapped_result_strings AS
SELECT T3.count, T1.native_code, T3.test_name, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(*),
 S1.native_code,
 result_string,
   RANK () OVER (
      PARTITION BY S1.native_code
      ORDER BY count(result_string) DESC
   ) rank
FROM
   emr_labresult S1,
   kre_report.flu_mapped S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string) T1,
emr_labresult T2,
kre_report.flu_mapped T3
WHERE rank <= 5
AND T3.native_code = T1.native_code
AND T3.native_code = T2.native_code
--AND T2.native_code = 'LAB475--17973'
AND T1.native_code = T2.native_code
GROUP BY T3.count, T1.native_code, T3.test_name, T2.native_name, T2.procedure_name;

