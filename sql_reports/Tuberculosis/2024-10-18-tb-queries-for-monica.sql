WITH  tb_screen AS (

SELECT count(distinct(T1.patient_id)) as tb_screen_pats
FROM
(SELECT patient_id, date FROM hef_event 
       WHERE name like 'lx:tb_igra:%'
       UNION
       SELECT patient_id, date FROM emr_prescription
       WHERE name ilike '%ppd%'
       UNION 
       SELECT patient_id, date FROM emr_immunization
       WHERE name ilike '%ppd%'
	   UNION
	   SELECT patient_id, date FROM emr_labresult
	   WHERE native_name ilike 'ppd%' or native_name ilike '%tuber%ppd%') T1
JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE date >= '2023-10-01'
AND date < '2024-10-01'
AND state ilike 'MA'), 

latent_tb as (
SELECT count(distinct(patient_id)) as latent_tb_pats
FROM
(SELECT distinct on (c.patient_id, to_char(cah.date,'yyyy_mm')) 
      c.patient_id,
	  to_char(cah.date,'yyyy_mm') year_month,
	  case (cah.status) when 'TB-L' then 1 when 'TB-A' then 2 end as tb
     FROM nodis_case c
     JOIN nodis_caseactivehistory cah on cah.case_id=c.id
     JOIN emr_patient pat on (c.patient_id = pat.id)
     WHERE c.condition='tuberculosis'
     AND cah.date >= '2023-10-01'
     AND cah.date < '2024-10-01'
     AND state ilike 'MA'
     ORDER BY patient_id, to_char(cah.date,'yyyy_mm'), cah.date desc) T1
WHERE tb = 1),

clin_enc_1yr as (
--Clin Enc >=1 in last one year
SELECT count(distinct(patient_id)) as clin_enc_pats
FROM gen_pop_tools.clin_enc T1
JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE date >= '2023-10-01'
AND date < '2024-10-01'
AND state ilike 'MA')

SELECT clin_enc_pats, tb_screen_pats, latent_tb_pats
FROM tb_screen T1
JOIN latent_tb T2 ON (1=1)
JOIN clin_enc_1yr T3 ON (1=1)