--CREATE SCHEMA covid19_report;

DROP TABLE IF EXISTS covid19_report.covid_wk_all_pats;
DROP TABLE IF EXISTS covid19_report.covid_wk_cases;
DROP TABLE IF EXISTS covid19_report.covid_wk_cases_tests_no_res;
DROP TABLE IF EXISTS covid19_report.covid_cumu_cases;
DROP TABLE IF EXISTS covid19_report.covid_wk_cases_within;
DROP TABLE IF EXISTS covid19_report.covid_pcr_testing_result_order_only_wk;
DROP TABLE IF EXISTS covid19_report.covid_pcr_testing;
DROP TABLE IF EXISTS covid19_report.covid_cumu_pcr_testing;
DROP TABLE IF EXISTS covid19_report.resp_wk_cases_within;
DROP TABLE IF EXISTS covid19_report.resp_testing;
DROP TABLE IF EXISTS covid19_report.covid_demog_groups;
DROP TABLE IF EXISTS covid19_report.covid_tot_full_strat;
DROP TABLE IF EXISTS covid19_report.covid_output;
DROP TABLE IF EXISTS covid19_report.covid_output_formatted;


-- Assumes script is running on a Saturday/Sunday
CREATE TABLE covid19_report.covid_wk_all_pats AS
SELECT count(distinct(patient_id)) total_pats_wk, week_end_date, covid19_report.mmwrwkn(T1.date) as week_number, --zip5, 
CASE 
      WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) < 11 then '0-10'
      WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 11 and 14 then '11-14'
      WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 15 and 24 then '15-24'
      WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 25 and 34 then '25-34'
	  WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 35 and 44 then '35-44'
	  WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 45 and 54 then '45-54'
	  WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 55 and 64 then '55-64'
	  WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 65 and 74 then '65-74'
	  WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) between 75 and 84 then '75-84'
      WHEN extract(YEAR FROM age(week_end_date::date, T4.date_of_birth::date)) > 84 then '85+'
    END as age_group,
CASE
     WHEN gender in ('F', 'FEMALE') then 'F'
     WHEN gender in ('M', 'MALE') then 'M'
	 ELSE 'U'
    END gender,
CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
     WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
     WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
     WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
     WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
     WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
     WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
     WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race_ethnicity,
coalesce(county, 'UNKNOWN COUNTY'::text) as county
FROM gen_pop_tools.clin_enc T1
JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T2 on (1=1)
JOIN (select :run_date::date as week_end_date) T3 on (1=1)
INNER JOIN emr_patient T4 on (T1.patient_id = T4.id)
LEFT JOIN covid19_report.ma_zip_to_county T5 on (T4.zip5 = T5.zip)
WHERE date >= '2020-02-01'
AND T1.date >= week_start_date
AND T1.date <= week_end_date
AND T4.date_of_birth is not null
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;

-- COVID CASES WITH CASE ASSOCIATED TESTS
CREATE TABLE covid19_report.covid_wk_cases AS
SELECT 
COUNT(DISTINCT id) as total_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 1' then id end) as cat1_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 2' then id end) as cat2_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 3' then id end) as cat3_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 4' then id end) as cat4_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 5' then id end) as cat5_covid19_cases_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 6' then id end) as cat6_covid19_cases_wk,
--CONFIRMED OK -- CHECK TO SEE IF MULTIPLE POS & NEGS IN SAME WEEK ARE COUNTED HERE -- BELOW
COUNT(CASE WHEN criteria = 'Category 1' and hef_name ilike '%positive%' then 1 end ) as cat1_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 1' and hef_name ilike '%negative%' then 1 end ) as cat1_covid19_neg_test_wk,
COUNT(CASE WHEN criteria = 'Category 2' and hef_name ilike '%positive%' then 1 end ) as cat2_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 2' and hef_name ilike '%negative%' then 1 end ) as cat2_covid19_neg_test_wk,
COUNT(CASE WHEN criteria = 'Category 3' and hef_name ilike '%positive%' then 1 end ) as cat3_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 3' and hef_name ilike '%negative%' then 1 end ) as cat3_covid19_neg_test_wk,
COUNT(CASE WHEN criteria = 'Category 4' and hef_name ilike '%positive%' then 1 end ) as cat4_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 4' and hef_name ilike '%negative%' then 1 end ) as cat4_covid19_neg_test_wk,
COUNT(CASE WHEN criteria = 'Category 5' and hef_name ilike '%positive%' then 1 end ) as cat5_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 5' and hef_name ilike '%negative%' then 1 end ) as cat5_covid19_neg_test_wk,
COUNT(CASE WHEN criteria = 'Category 6' and hef_name ilike '%positive%' then 1 end ) as cat6_covid19_pos_test_wk,
COUNT(CASE WHEN criteria = 'Category 6' and hef_name ilike '%negative%' then 1 end ) as cat6_covid19_neg_test_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.criteria, T1.id, T2.hef_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
 		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM nodis_case T1
	LEFT JOIN (	SELECT T1.patient_id, name as hef_name, T2.id as case_id
			FROM hef_event T1
			INNER JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
			INNER JOIN nodis_case_events T3 on (T2.id = T3.case_id and T1.id = T3.event_id)
			WHERE name ilike 'lx:covid19_pcr:%'
			AND condition = 'covid19'
			AND T2.date >= '2020-02-01'
			-- only include events prior to end of reporting week
			AND T1.date <= :run_date::date
		GROUP BY T1.patient_id, hef_name, T2.id) T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.case_id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE condition = 'covid19'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- THESE ARE THE PATIENTS WITH NO TEST RESULT YET
CREATE TABLE covid19_report.covid_wk_cases_tests_no_res AS
SELECT 
count(CASE WHEN criteria = 'Category 1' then 1 end) as cat1_covid19_no_result_test_wk,
count(CASE WHEN criteria = 'Category 2' then 1 end) as cat2_covid19_no_result_test_wk,
count(CASE WHEN criteria = 'Category 3' then 1 end) as cat3_covid19_no_result_test_wk,
count(CASE WHEN criteria = 'Category 4' then 1 end) as cat4_covid19_no_result_test_wk,
count(CASE WHEN criteria = 'Category 5' then 1 end) as cat5_covid19_no_result_test_wk,
count(CASE WHEN criteria = 'Category 6' then 1 end) as cat6_covid19_no_result_test_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T2.criteria, week_end_date, covid19_report.mmwrwkn(T2.date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T2.date::date, T5.date_of_birth::date)) > 84 then '85+'
	 END as age_group,
	CASE
	     WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		 END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM 
		-- Pats with laborder but no result yet
		(SELECT DISTINCT T1.patient_id, T2.natural_key
	         FROM hef_event T1,
	         emr_laborder T2
	         WHERE T1.patient_id = T2.patient_id
	         AND T1.object_id = T2.id
	         AND T1.name = 'lx:covid19_pcr_order:order'
	         AND T2.date >= '2020-02-01'
        EXCEPT
        SELECT DISTINCT patient_id, order_natural_key
		    FROM emr_labresult
		    WHERE date >= '2020-02-01'
			-- test needs to be resulted before report week end date
			AND date <= :run_date::date) T1
	INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE T2.date >= '2020-02-01'
	AND T2.date >= week_start_date
	AND T2.date <= week_end_date
	AND T5.date_of_birth is not null
	AND condition = 'covid19') T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- COVID CASES WITH TESTS(AND ORDERS) IN THE SAME WEEK
-- INCLUDING THE ORDERS WILL PICK UP ANY NON-RESULTED TESTS
-- AS LONG AS WE ARE USING UNIQUE PATIENT DATA THIS SHOULD WORK
CREATE TABLE covid19_report.covid_wk_cases_within AS
SELECT 
COUNT(DISTINCT CASE WHEN hef_name is not null then patient_id end ) as cat_any_covid19_any_test_within_wk,
-- CONFIRMED OK -- multiple positives counted as one
COUNT(CASE WHEN hef_name ilike '%positive%' then 1 end ) as cat_any_covid19_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 1' and hef_name is not null then id end ) as cat1_covid19_any_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 2' and hef_name is not null then id end ) as cat2_covid19_any_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 3' and hef_name is not null then id end ) as cat3_covid19_any_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 4' and hef_name is not null then id end ) as cat4_covid19_any_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 5' and hef_name is not null then id end ) as cat5_covid19_any_test_within_wk,
COUNT(DISTINCT CASE WHEN criteria = 'Category 6' and hef_name is not null then id end ) as cat6_covid19_any_test_within_wk,


week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.criteria, T1.id, T2.hef_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
 		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM nodis_case T1
	LEFT JOIN (	SELECT T1.patient_id, name as hef_name, T2.id as case_id
			FROM hef_event T1
			INNER JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
			INNER JOIN nodis_case_events T3 on (T2.id = T3.case_id and T1.id = T3.event_id)
			WHERE (name ilike 'lx:covid19_pcr:%' or name = 'lx:covid19_pcr_order:order')
			AND condition = 'covid19'
			AND T2.date >= '2020-02-01'
			-- only include events within the reporting week
			AND T1.date >=  (:run_date::date - INTERVAL '6 days')::date
			AND T1.date <= :run_date::date
		GROUP BY T1.patient_id, hef_name, T2.id) T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.case_id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE condition = 'covid19'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;


-- CUMULATIVE COUNTS
CREATE TABLE covid19_report.covid_cumu_cases AS
SELECT 
COUNT(DISTINCT id) as covid19_cumu_total,
COUNT(DISTINCT CASE WHEN criteria = 'Category 1' then id end) as cat1_covid19_cumu,
COUNT(DISTINCT CASE WHEN criteria = 'Category 2' then id end) as cat2_covid19_cumu,
COUNT(DISTINCT CASE WHEN criteria = 'Category 3' then id end) as cat3_covid19_cumu,
COUNT(DISTINCT CASE WHEN criteria = 'Category 4' then id end) as cat4_covid19_cumu,
COUNT(DISTINCT CASE WHEN criteria = 'Category 5' then id end) as cat5_covid19_cumu,
COUNT(DISTINCT CASE WHEN criteria = 'Category 6' then id end) as cat6_covid19_cumu,
COUNT(CASE WHEN criteria = 'Category 1' and hef_name ilike '%positive%' then 1 end ) as cat1_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 1' and hef_name ilike '%negative%' then 1 end ) as cat1_covid19_cumu_neg_test,
COUNT(CASE WHEN criteria = 'Category 2' and hef_name ilike '%positive%' then 1 end ) as cat2_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 2' and hef_name ilike '%negative%' then 1 end ) as cat2_covid19_cumu_neg_test,
COUNT(CASE WHEN criteria = 'Category 3' and hef_name ilike '%positive%' then 1 end ) as cat3_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 3' and hef_name ilike '%negative%' then 1 end ) as cat3_covid19_cumu_neg_test,
COUNT(CASE WHEN criteria = 'Category 4' and hef_name ilike '%positive%' then 1 end ) as cat4_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 4' and hef_name ilike '%negative%' then 1 end ) as cat4_covid19_cumu_neg_test,
COUNT(CASE WHEN criteria = 'Category 5' and hef_name ilike '%positive%' then 1 end ) as cat5_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 5' and hef_name ilike '%negative%' then 1 end ) as cat5_covid19_cumu_neg_test,
COUNT(CASE WHEN criteria = 'Category 6' and hef_name ilike '%positive%' then 1 end ) as cat6_covid19_cumu_pos_test,
COUNT(CASE WHEN criteria = 'Category 6' and hef_name ilike '%negative%' then 1 end ) as cat6_covid19_cumu_neg_test,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.criteria, T1.id, T2.hef_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM nodis_case T1
	LEFT JOIN (	SELECT T1.patient_id, name as hef_name, T2.id as case_id
			FROM hef_event T1
			INNER JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
			INNER JOIN nodis_case_events T3 on (T2.id = T3.case_id and T1.id = T3.event_id)
			WHERE name ilike 'lx:covid19_pcr:%'
			AND condition = 'covid19'
			AND T2.date >= '2020-02-01'
			-- only include events prior to end of reporting week
			AND T1.date <= :run_date::date
		GROUP BY T1.patient_id, hef_name, T2.id) T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.case_id)
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE condition = 'covid19'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;


-- COVID CASES WITH RESPIRATORY TESTS IN THE SAME WEEK
-- AS LONG AS WE ARE USING UNIQUE PATIENT DATA THIS SHOULD WORK
CREATE TABLE covid19_report.resp_wk_cases_within AS
SELECT 
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:rsv:%' then patient_id end ) as cat_any_rsv_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:rsv:positive%' then 1 end ) as cat_any_rsv_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:rapid_flu:%' or hef_name ilike 'lx:influenza_culture:%' or hef_name ilike 'lx:influenza:%' then patient_id end ) as cat_any_influenza_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:rapid_flu:positive%' or hef_name ilike 'lx:influenza_culture:positive%' or hef_name ilike 'lx:influenza:positive%' then 1 end ) as cat_any_influenza_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:h_metapneumovirus:%' then patient_id end ) as cat_any_h_metapneumovirus_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:h_metapneumovirus:positive%' then 1 end ) as cat_any_h_metapneumovirus_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:parainfluenza:%' then patient_id end ) as cat_any_parainfluenza_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:parainfluenza:positive%' then 1 end ) as cat_any_parainfluenza_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:adenovirus:%' then patient_id end ) as cat_any_adenovirus_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:adenovirus:positive%' then 1 end ) as cat_any_adenovirus_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:rhino_entero_virus:%' then patient_id end ) as cat_any_rhino_entero_virus_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:rhino_entero_virus:positive%' then 1 end ) as cat_any_rhino_entero_virus_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:coronavirus_non19:%' then patient_id end ) as cat_any_coronavirus_non19_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:coronavirus_non19:positive%' then 1 end ) as cat_any_coronavirus_non19_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx::m_pneumoniae_igm:%' or hef_name ilike 'lx:m_pneumoniae_pcr:%' then patient_id end ) as cat_any_m_pneumoniae_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx::m_pneumoniae_igm:positive%' or hef_name ilike 'lx:m_pneumoniae_pcr:positive%' then 1 end ) as cat_any_m_pneumoniae_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx::c_pneumoniae_igm:%' or hef_name ilike 'lx:c_pneumoniae_pcr:%' then patient_id end ) as cat_any_c_pneumoniae_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx::c_pneumoniae_igm:positive%' or hef_name ilike 'lx:c_pneumoniae_pcr:positive%' then 1 end ) as cat_any_c_pneumoniae_pos_test_within_wk,
COUNT(DISTINCT CASE WHEN hef_name ilike 'lx:parapertussis:%' then patient_id end ) as cat_any_parapertussis_any_test_within_wk,
COUNT(CASE WHEN hef_name ilike 'lx:parapertussis:positive%' then 1 end ) as cat_any_parapertussis_pos_test_within_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.criteria, T1.id, T2.hef_name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
		CASE 
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
			  WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
			END as age_group,
		CASE
			 WHEN gender in ('F', 'FEMALE') then 'F'
			 WHEN gender in ('M', 'MALE') then 'M'
			 ELSE 'U'
			END gender,
		CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
			 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
			 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
			 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
			 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
			 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
			 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
			 WHEN race is null or race = '' then 'UNKNOWN' 
		ELSE 'UNKNOWN' END as race_ethnicity,
		coalesce(county, 'UNKNOWN COUNTY'::text) as county
		FROM nodis_case T1
		INNER JOIN (	SELECT T1.patient_id, name as hef_name, T2.id as case_id
				FROM hef_event T1
				INNER JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
				WHERE (   name ilike 'lx:rsv:%' 
					   or name ilike 'lx:parapertussis:%' 
					   or name ilike 'lx:rapid_flu:%'
					   or name ilike 'lx:influenza:%'
					   or name ilike 'lx:influenza_culture:%'
					   or name ilike 'lx:parainfluenza:%'
					   or name ilike 'lx:h_metapneumovirus:%'
					   or name ilike 'lx:adenovirus:%'
					   or name ilike 'lx:rhino_entero_virus:%'
					   or name ilike 'lx:m_pneumoniae_igm:%'
					   or name ilike 'lx:m_pneumoniae_pcr:%'
					   or name ilike 'lx:c_pneumoniae_igm:%'
					   or name ilike 'lx:c_pneumoniae_pcr:%'
					   or name ilike 'lx:coronavirus_non19:%')
				AND condition = 'covid19'
				AND T2.date >= '2020-02-01'
				-- only include events within the reporting week
				AND T1.date >=  (:run_date::date - INTERVAL '6 days')::date
				AND T1.date <= :run_date::date
		GROUP BY T1.patient_id, hef_name, T2.id) T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.case_id)
		JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
		JOIN (select :run_date::date as week_end_date) T4 on (1=1)
		INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
		LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
		WHERE condition = 'covid19'
		AND T5.date_of_birth is not null
		AND T1.date >= '2020-02-01'
		AND T1.date >= week_start_date
		AND T1.date <= week_end_date
		) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- WEEKLY COVID TESTING REGARDLESS IF SOMEONE HAS A CASE OR NOT
CREATE TABLE covid19_report.covid_pcr_testing AS
SELECT 
COUNT(DISTINCT patient_id) as pats_tested_wk,
COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pos_tests_wk,
COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as neg_tests_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name ilike 'lx:covid19_pcr:%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- POSITIVE OVERRIDE WEEKLY TESTING REGARDLESS IF SOMEONE HAS A CASE OR NOT
-- THIS IS GETTING THE COUNTS OF RESULTS BUT NOT COUNTING IF PATIENT HAS A POS OR OTHER RESULT IN THE SAME WEEK
-- HERE WE ARE JUST LOOKING AT THE ORDER EVENT ITSELF -- NO MATCH ON NATURAL KEY
CREATE TABLE covid19_report.covid_pcr_testing_result_order_only_wk AS
SELECT 
COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as neg_only_tests_wk,
COUNT(CASE WHEN name ilike '%indeterminate%' then 1 end ) as ind_only_tests_wk,
COUNT(CASE WHEN name ilike '%order%' then 1 end ) as order_no_res_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	(SELECT DISTINCT T1.patient_id, 'negative' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name ilike 'lx:covid19_pcr:negative%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
	
	EXCEPT
	
	SELECT DISTINCT T1.patient_id, 'negative' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_pcr:indeterminate')
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date)
	
	UNION
	
	(SELECT DISTINCT T1.patient_id, 'indeterminate' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name ilike 'lx:covid19_pcr:indeterminate%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
	
	EXCEPT
	
	SELECT DISTINCT T1.patient_id, 'indeterminate' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_pcr:negative')
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date)
	
	UNION
	
	(SELECT DISTINCT T1.patient_id, 'order' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE T1.name = 'lx:covid19_pcr_order:order'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
	
	EXCEPT
	
	SELECT DISTINCT T1.patient_id, 'order' as name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_pcr:indeterminate', 'lx:covid19_pcr:negative')
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date)	
	
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;


-- WEEKLY RESPIRATORY TESTING REGARDLESS IF SOMEONE HAS A CASE OR NOT
CREATE TABLE covid19_report.resp_testing AS
SELECT 
COUNT(DISTINCT CASE WHEN name ilike 'lx:rsv:%' then patient_id end ) as rsv_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:rsv:positive%' then patient_id end ) as rsv_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:rapid_flu:%' or name ilike 'lx:influenza_culture:%' or name ilike 'lx:influenza:%' then patient_id end ) as influenza_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:rapid_flu:positive%' or name ilike 'lx:influenza_culture:positive%' or name ilike 'lx:influenza:positive%' then patient_id end ) as influenza_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:h_metapneumovirus:%' then patient_id end ) as h_metapneumovirus_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:h_metapneumovirus:positive%' then patient_id end ) as h_metapneumovirus_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:parainfluenza:%' then patient_id end ) as parainfluenza_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:parainfluenza:positive%' then patient_id end ) as parainfluenza_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:adenovirus:%' then patient_id end ) as adenovirus_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:adenovirus:positive%' then patient_id end ) as adenovirus_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:rhino_entero_virus:%' then patient_id end ) as rhino_entero_virus_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:rhino_entero_virus:positive%' then patient_id end ) as rhino_entero_virus_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:coronavirus_non19:%' then patient_id end ) as coronavirus_non19_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:coronavirus_non19:positive%' then patient_id end ) as coronavirus_non19_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx::m_pneumoniae_igm:%' or name ilike 'lx:m_pneumoniae_pcr:%' then patient_id end ) as m_pneumoniae_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx::m_pneumoniae_igm:positive%' or name ilike 'lx:m_pneumoniae_pcr:positive%' then patient_id end ) as m_pneumoniae_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx::c_pneumoniae_igm:%' or name ilike 'lx:c_pneumoniae_pcr:%' then patient_id end ) as c_pneumoniae_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx::c_pneumoniae_igm:positive%' or name ilike 'lx:c_pneumoniae_pcr:positive%' then patient_id end ) as c_pneumoniae_pos_tests_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:parapertussis:%' then patient_id end ) as parapertussis_pats_tested_wk,
COUNT(DISTINCT CASE WHEN name ilike 'lx:parapertussis:positive%' then patient_id end ) as parapertussis_pos_tests_wk,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE (   name ilike 'lx:rsv:%' 
					   or name ilike 'lx:parapertussis:%' 
					   or name ilike 'lx:rapid_flu:%'
					   or name ilike 'lx:influenza:%'
					   or name ilike 'lx:influenza_culture:%'
					   or name ilike 'lx:parainfluenza:%'
					   or name ilike 'lx:h_metapneumovirus:%'
					   or name ilike 'lx:adenovirus:%'
					   or name ilike 'lx:rhino_entero_virus:%'
					   or name ilike 'lx:m_pneumoniae_igm:%'
					   or name ilike 'lx:m_pneumoniae_pcr:%'
					   or name ilike 'lx:c_pneumoniae_igm:%'
					   or name ilike 'lx:c_pneumoniae_pcr:%'
					   or name ilike 'lx:coronavirus_non19:%')
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
	AND T1.date >= week_start_date
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- CUMULATIVE TESTING REGARDLESS IF SOMEONE HAS A CASE OR NOT
CREATE TABLE covid19_report.covid_cumu_pcr_testing AS
SELECT 
COUNT(DISTINCT patient_id) as pats_tested_cumu,
COUNT(CASE WHEN name ilike '%positive%' then 1 end ) as pos_tests_cumu,
COUNT(CASE WHEN name ilike '%negative%' then 1 end ) as neg_tests_cumu,
week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county
FROM (
	SELECT DISTINCT T1.patient_id, T1.name, week_end_date, covid19_report.mmwrwkn(week_end_date) as week_number, --zip5,
	CASE 
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) < 11 then '0-10'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 11 and 14 then '11-14'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 15 and 24 then '15-24'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 25 and 34 then '25-34'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 35 and 44 then '35-44'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 45 and 54 then '45-54'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 55 and 64 then '55-64'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 65 and 74 then '65-74'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) between 75 and 84 then '75-84'
		 WHEN extract(YEAR FROM age(T1.date::date, T5.date_of_birth::date)) > 84 then '85+'
		END as age_group,
	CASE
		 WHEN gender in ('F', 'FEMALE') then 'F'
		 WHEN gender in ('M', 'MALE') then 'M'
		 ELSE 'U'
		END gender,
	CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC'
		 WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
		 WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
		 WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
		 WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
		 WHEN UPPER(race) in ('ASIAN', 'A', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
		 WHEN UPPER(race) in  ('NATIVE HAWAI', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
		 WHEN race is null or race = '' then 'UNKNOWN' 
	ELSE 'UNKNOWN' END as race_ethnicity,
	coalesce(county, 'UNKNOWN COUNTY'::text) as county
	FROM hef_event T1
	JOIN (select (:run_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
    JOIN (select :run_date::date as week_end_date) T4 on (1=1)
	INNER JOIN emr_patient T5 on (T1.patient_id = T5.id)
	LEFT JOIN covid19_report.ma_zip_to_county T6 on (T5.zip5 = T6.zip)
	WHERE name ilike 'lx:covid19_pcr:%'
	AND T5.date_of_birth is not null
	AND T1.date >= '2020-02-01'
    AND T1.date <= week_end_date
    ) T1
GROUP BY week_end_date, week_number, --zip5, 
	age_group, gender, race_ethnicity, county;



-- GENERATE THE GROUPS
CREATE TABLE covid19_report.covid_demog_groups AS
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_wk_all_pats
UNION 
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_wk_cases
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_wk_cases_tests_no_res
UNION 
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_cumu_cases 
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_pcr_testing
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_cumu_pcr_testing
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_wk_cases_within
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.covid_pcr_testing_result_order_only_wk
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.resp_wk_cases_within
UNION
SELECT week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county FROM covid19_report.resp_testing
GROUP BY week_end_date, week_number, --zip5, 
age_group, gender, race_ethnicity, county;



-- GENERATE THE FULLY STRATIFIED OUTPUT
CREATE TABLE covid19_report.covid_tot_full_strat AS
select T1.week_end_date, T1.week_number, --T1.zip5, 
T1.age_group, T1.gender, T1.race_ethnicity, T1.county,
coalesce(total_pats_wk, 0) as total_pats_wk, 
coalesce(total_covid19_cases_wk, 0) as total_covid19_cases_wk, 
coalesce(cat1_covid19_cases_wk, 0) as cat1_covid19_cases_wk,
coalesce(cat1_covid19_pos_test_wk, 0) as cat1_covid19_pos_test_wk,
coalesce(cat1_covid19_neg_test_wk, 0) as cat1_covid19_neg_test_wk,
coalesce(cat1_covid19_no_result_test_wk, 0) as cat1_covid19_no_result_test_wk,
coalesce(cat2_covid19_cases_wk, 0) as cat2_covid19_cases_wk,
coalesce(cat2_covid19_pos_test_wk, 0) as cat2_covid19_pos_test_wk,
coalesce(cat2_covid19_neg_test_wk, 0) as cat2_covid19_neg_test_wk,
coalesce(cat2_covid19_no_result_test_wk, 0) as cat2_covid19_no_result_test_wk,
coalesce(cat3_covid19_cases_wk, 0) as cat3_covid19_cases_wk,
coalesce(cat3_covid19_pos_test_wk, 0) as cat3_covid19_pos_test_wk,
coalesce(cat3_covid19_neg_test_wk, 0) as cat3_covid19_neg_test_wk,
coalesce(cat3_covid19_no_result_test_wk, 0) as cat3_covid19_no_result_test_wk,

coalesce(cat4_covid19_cases_wk, 0) as cat4_covid19_cases_wk,
coalesce(cat4_covid19_pos_test_wk, 0) as cat4_covid19_pos_test_wk,
coalesce(cat4_covid19_neg_test_wk, 0) as cat4_covid19_neg_test_wk,
coalesce(cat4_covid19_no_result_test_wk, 0) as cat4_covid19_no_result_test_wk,


coalesce(cat5_covid19_cases_wk, 0) as cat5_covid19_cases_wk,
coalesce(cat5_covid19_pos_test_wk, 0) as cat5_covid19_pos_test_wk,
coalesce(cat5_covid19_neg_test_wk, 0) as cat5_covid19_neg_test_wk,
coalesce(cat5_covid19_no_result_test_wk, 0) as cat5_covid19_no_result_test_wk,

coalesce(cat6_covid19_cases_wk, 0) as cat6_covid19_cases_wk,
coalesce(cat6_covid19_pos_test_wk, 0) as cat6_covid19_pos_test_wk,
coalesce(cat6_covid19_neg_test_wk, 0) as cat6_covid19_neg_test_wk,
coalesce(cat6_covid19_no_result_test_wk, 0) as cat6_covid19_no_result_test_wk,


coalesce(round(((total_covid19_cases_wk::integer / total_pats_wk::float)*100)::numeric, 2), 0) as pct_pats_w_cases_wk,
coalesce(cat_any_covid19_any_test_within_wk, 0) as cat_any_covid19_any_test_within_wk,
coalesce(cat_any_covid19_pos_test_within_wk, 0) as cat_any_covid19_pos_test_within_wk,
coalesce(cat1_covid19_any_test_within_wk, 0) as cat1_covid19_any_test_within_wk,
coalesce(cat2_covid19_any_test_within_wk, 0) as cat2_covid19_any_test_within_wk,
coalesce(cat3_covid19_any_test_within_wk, 0) as cat3_covid19_any_test_within_wk,

coalesce(cat4_covid19_any_test_within_wk, 0) as cat4_covid19_any_test_within_wk,
coalesce(cat5_covid19_any_test_within_wk, 0) as cat5_covid19_any_test_within_wk,
coalesce(cat6_covid19_any_test_within_wk, 0) as cat6_covid19_any_test_within_wk,

coalesce(covid19_cumu_total, 0) as covid19_cumu_total,
coalesce(cat1_covid19_cumu, 0) as cat1_covid19_cumu,
coalesce(cat2_covid19_cumu, 0) as cat2_covid19_cumu,
coalesce(cat3_covid19_cumu, 0) as cat3_covid19_cumu,
coalesce(cat4_covid19_cumu, 0) as cat4_covid19_cumu,
coalesce(cat5_covid19_cumu, 0) as cat5_covid19_cumu,
coalesce(cat6_covid19_cumu, 0) as cat6_covid19_cumu,
coalesce(cat1_covid19_cumu_pos_test, 0) as cat1_covid19_cumu_pos_test,
coalesce(cat1_covid19_cumu_neg_test, 0) as cat1_covid19_cumu_neg_test,
coalesce(cat2_covid19_cumu_pos_test, 0) as cat2_covid19_cumu_pos_test,
coalesce(cat2_covid19_cumu_neg_test, 0) as cat2_covid19_cumu_neg_test,
coalesce(cat3_covid19_cumu_pos_test, 0) as cat3_covid19_cumu_pos_test,
coalesce(cat3_covid19_cumu_neg_test, 0) as cat3_covid19_cumu_neg_test,
coalesce(cat4_covid19_cumu_pos_test, 0) as cat4_covid19_cumu_pos_test,
coalesce(cat4_covid19_cumu_neg_test, 0) as cat4_covid19_cumu_neg_test,
coalesce(cat5_covid19_cumu_pos_test, 0) as cat5_covid19_cumu_pos_test,
coalesce(cat5_covid19_cumu_neg_test, 0) as cat5_covid19_cumu_neg_test,
coalesce(cat6_covid19_cumu_pos_test, 0) as cat6_covid19_cumu_pos_test,
coalesce(cat6_covid19_cumu_neg_test, 0) as cat6_covid19_cumu_neg_test,
coalesce(pats_tested_wk, 0) as pats_tested_wk,
coalesce(pos_tests_wk, 0) as pos_tests_wk,
coalesce(neg_tests_wk, 0) as neg_tests_wk,
coalesce(neg_only_tests_wk, 0) as neg_only_tests_wk,
coalesce(ind_only_tests_wk, 0) as ind_only_tests_wk,
coalesce(order_no_res_wk, 0) as order_no_res_wk,
coalesce(pats_tested_cumu, 0) as pats_tested_cumu,
coalesce(pos_tests_cumu, 0) as pos_tests_cumu,
coalesce(neg_tests_cumu, 0) as neg_tests_cumu,
coalesce(cat_any_rsv_any_test_within_wk, 0) as cat_any_rsv_any_test_within_wk,
coalesce(cat_any_rsv_pos_test_within_wk, 0) as cat_any_rsv_pos_test_within_wk,
coalesce(rsv_pats_tested_wk, 0) as rsv_pats_tested_wk,
coalesce(rsv_pos_tests_wk, 0) as rsv_pos_tests_wk,
coalesce(cat_any_influenza_any_test_within_wk, 0) as cat_any_influenza_any_test_within_wk,
coalesce(cat_any_influenza_pos_test_within_wk, 0) as cat_any_influenza_pos_test_within_wk,
coalesce(influenza_pats_tested_wk, 0) as influenza_pats_tested_wk,
coalesce(influenza_pos_tests_wk, 0) as influenza_pos_tests_wk,
coalesce(cat_any_h_metapneumovirus_any_test_within_wk, 0) as cat_any_h_metapneumovirus_any_test_within_wk,
coalesce(cat_any_h_metapneumovirus_pos_test_within_wk, 0) as cat_any_h_metapneumovirus_pos_test_within_wk,
coalesce(h_metapneumovirus_pats_tested_wk, 0) as h_metapneumovirus_pats_tested_wk,
coalesce(h_metapneumovirus_pos_tests_wk, 0) as h_metapneumovirus_pos_tests_wk,
coalesce(cat_any_parainfluenza_any_test_within_wk, 0) as cat_any_parainfluenza_any_test_within_wk,
coalesce(cat_any_parainfluenza_pos_test_within_wk, 0) as cat_any_parainfluenza_pos_test_within_wk,
coalesce(parainfluenza_pats_tested_wk, 0) as parainfluenza_pats_tested_wk,
coalesce(parainfluenza_pos_tests_wk, 0) as parainfluenza_pos_tests_wk,
coalesce(cat_any_adenovirus_any_test_within_wk, 0) as cat_any_adenovirus_any_test_within_wk,
coalesce(cat_any_adenovirus_pos_test_within_wk, 0) as cat_any_adenovirus_pos_test_within_wk,
coalesce(adenovirus_pats_tested_wk, 0) as adenovirus_pats_tested_wk,
coalesce(adenovirus_pos_tests_wk, 0) as adenovirus_pos_tests_wk,
coalesce(cat_any_rhino_entero_virus_any_test_within_wk, 0) as cat_any_rhino_entero_virus_any_test_within_wk,
coalesce(cat_any_rhino_entero_virus_pos_test_within_wk, 0) as cat_any_rhino_entero_virus_pos_test_within_wk,
coalesce(rhino_entero_virus_pats_tested_wk, 0) as rhino_entero_virus_pats_tested_wk,
coalesce(rhino_entero_virus_pos_tests_wk, 0) as rhino_entero_virus_pos_tests_wk,
coalesce(cat_any_coronavirus_non19_any_test_within_wk, 0) as cat_any_coronavirus_non19_any_test_within_wk,
coalesce(cat_any_coronavirus_non19_pos_test_within_wk, 0) as cat_any_coronavirus_non19_pos_test_within_wk,
coalesce(coronavirus_non19_pats_tested_wk, 0) as coronavirus_non19_pats_tested_wk,
coalesce(coronavirus_non19_pos_tests_wk, 0) as coronavirus_non19_pos_tests_wk,
coalesce(cat_any_m_pneumoniae_any_test_within_wk, 0) as cat_any_m_pneumoniae_any_test_within_wk,
coalesce(cat_any_m_pneumoniae_pos_test_within_wk, 0) as cat_any_m_pneumoniae_pos_test_within_wk,
coalesce(m_pneumoniae_pats_tested_wk, 0) as m_pneumoniae_pats_tested_wk,
coalesce(m_pneumoniae_pos_tests_wk, 0) as m_pneumoniae_pos_tests_wk,
coalesce(cat_any_c_pneumoniae_any_test_within_wk, 0) as cat_any_c_pneumoniae_any_test_within_wk,
coalesce(cat_any_c_pneumoniae_pos_test_within_wk, 0) as cat_any_c_pneumoniae_pos_test_within_wk,
coalesce(c_pneumoniae_pats_tested_wk, 0) as c_pneumoniae_pats_tested_wk,
coalesce(c_pneumoniae_pos_tests_wk, 0) as c_pneumoniae_pos_tests_wk,
coalesce(cat_any_parapertussis_any_test_within_wk, 0) as cat_any_parapertussis_any_test_within_wk,
coalesce(cat_any_parapertussis_pos_test_within_wk, 0) as cat_any_parapertussis_pos_test_within_wk,
coalesce(parapertussis_pats_tested_wk, 0) as parapertussis_pats_tested_wk,
coalesce(parapertussis_pos_tests_wk, 0) as parapertussis_pos_tests_wk
FROM covid19_report.covid_demog_groups T1
LEFT JOIN covid19_report.covid_wk_all_pats T2 on (T1.week_end_date = T2.week_end_date and T1.week_number = T2.week_number --and T1.zip5 = T2.zip5 
	and T1.age_group = T2.age_group and T1.gender = T2.gender and T1.race_ethnicity = T2.race_ethnicity and T1.county = T2.county)
LEFT JOIN covid19_report.covid_wk_cases T3 on (T1.week_end_date = T3.week_end_date and T1.week_number = T3.week_number --and T1.zip5 = T3.zip5 
	and T1.age_group = T3.age_group and T1.gender = T3.gender and T1.race_ethnicity = T3.race_ethnicity and T1.county = T3.county)
LEFT JOIN covid19_report.covid_wk_cases_tests_no_res T5 on (T1.week_end_date = T5.week_end_date and T1.week_number = T5.week_number --and T1.zip5 = T5.zip5 
	and T1.age_group = T5.age_group and T1.gender = T5.gender and T1.race_ethnicity = T5.race_ethnicity and T1.county = T5.county)
LEFT JOIN covid19_report.covid_cumu_cases T6 on (T1.week_end_date = T6.week_end_date and T1.week_number = T6.week_number --and T1.zip5 = T6.zip5 
	and T1.age_group = T6.age_group and T1.gender = T6.gender and T1.race_ethnicity = T6.race_ethnicity and T1.county = T6.county)
LEFT JOIN covid19_report.covid_pcr_testing T7 on (T1.week_end_date = T7.week_end_date and T1.week_number = T7.week_number --and T1.zip5 = T7.zip5 
	and T1.age_group = T7.age_group and T1.gender = T7.gender and T1.race_ethnicity = T7.race_ethnicity and T1.county = T7.county)
LEFT JOIN covid19_report.covid_cumu_pcr_testing T8 on (T1.week_end_date = T8.week_end_date and T1.week_number = T8.week_number --and T1.zip5 = T8.zip5 
	and T1.age_group = T8.age_group and T1.gender = T8.gender and T1.race_ethnicity = T8.race_ethnicity and T1.county = T8.county)
LEFT JOIN covid19_report.covid_wk_cases_within T9 on (T1.week_end_date = T9.week_end_date and T1.week_number = T9.week_number --and T1.zip5 = T9.zip5 
	and T1.age_group = T9.age_group and T1.gender = T9.gender and T1.race_ethnicity = T9.race_ethnicity and T1.county = T9.county)
LEFT JOIN covid19_report.covid_pcr_testing_result_order_only_wk T10 on (T1.week_end_date = T10.week_end_date and T1.week_number = T10.week_number --and T1.zip5 = T10.zip5 
	and T1.age_group = T10.age_group and T1.gender = T10.gender and T1.race_ethnicity = T10.race_ethnicity and T1.county = T10.county)
LEFT JOIN covid19_report.resp_wk_cases_within T11 on (T1.week_end_date = T11.week_end_date and T1.week_number = T11.week_number --and T1.zip5 = T11.zip5 
	and T1.age_group = T11.age_group and T1.gender = T11.gender and T1.race_ethnicity = T11.race_ethnicity and T1.county = T11.county)
LEFT JOIN covid19_report.resp_testing T12 on (T1.week_end_date = T12.week_end_date and T1.week_number = T12.week_number --and T1.zip5 = T12.zip5 
	and T1.age_group = T12.age_group and T1.gender = T12.gender and T1.race_ethnicity = T12.race_ethnicity and T1.county = T12.county)
order by T1.week_end_date, T1.week_number, T1.age_group, T1.gender, --T1.zip5, 
T1.race_ethnicity, T1.county;


-- ADD IN THE "ALL" VALUES
CREATE TABLE covid19_report.covid_output AS
SELECT *, '9999' as position
FROM covid19_report.covid_tot_full_strat
UNION
SELECT 
week_end_date, week_number, --'ALL' as zip5, 
'ALL' as age_group, 'ALL' as gender, 'ALL' as race_ethnicity, 'ALL' as county,
sum(total_pats_wk) total_pats_wk, 
sum(total_covid19_cases_wk) total_covid19_cases_wk, 
sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,


sum(cat4_covid19_cases_wk) cat4_covid19_cases_wk, 
sum(cat4_covid19_pos_test_wk) cat4_covid19_pos_test_wk,
sum(cat4_covid19_neg_test_wk) cat4_covid19_neg_test_wk,
sum(cat4_covid19_no_result_test_wk) cat4_covid19_no_result_test_wk,


sum(cat5_covid19_cases_wk) cat5_covid19_cases_wk, 
sum(cat5_covid19_pos_test_wk) cat5_covid19_pos_test_wk,
sum(cat5_covid19_neg_test_wk) cat5_covid19_neg_test_wk,
sum(cat5_covid19_no_result_test_wk) cat5_covid19_no_result_test_wk,

sum(cat6_covid19_cases_wk) cat6_covid19_cases_wk, 
sum(cat6_covid19_pos_test_wk) cat6_covid19_pos_test_wk,
sum(cat6_covid19_neg_test_wk) cat6_covid19_neg_test_wk,
sum(cat6_covid19_no_result_test_wk) cat6_covid19_no_result_test_wk,





case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
sum(cat_any_covid19_any_test_within_wk) cat_any_covid19_any_test_within_wk,
sum(cat_any_covid19_pos_test_within_wk) cat_any_covid19_pos_test_within_wk,
sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,

sum(cat4_covid19_any_test_within_wk) cat4_covid19_any_test_within_wk,
sum(cat5_covid19_any_test_within_wk) cat5_covid19_any_test_within_wk,
sum(cat6_covid19_any_test_within_wk) cat6_covid19_any_test_within_wk,

sum(covid19_cumu_total) covid19_cumu_total,
sum(cat1_covid19_cumu) cat1_covid19_cumu,
sum(cat2_covid19_cumu) cat2_covid19_cumu,
sum(cat3_covid19_cumu) cat3_covid19_cumu,
sum(cat4_covid19_cumu) cat4_covid19_cumu,
sum(cat5_covid19_cumu) cat5_covid19_cumu,
sum(cat6_covid19_cumu) cat6_covid19_cumu,

sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,

sum(cat4_covid19_cumu_pos_test) cat4_covid19_cumu_pos_test,
sum(cat4_covid19_cumu_neg_test) cat4_covid19_cumu_neg_test,
sum(cat5_covid19_cumu_pos_test) cat5_covid19_cumu_pos_test,
sum(cat5_covid19_cumu_neg_test) cat5_covid19_cumu_neg_test,
sum(cat6_covid19_cumu_pos_test) cat6_covid19_cumu_pos_test,
sum(cat6_covid19_cumu_neg_test) cat6_covid19_cumu_neg_test,





sum(pats_tested_wk) pats_tested_wk,
sum(pos_tests_wk) pos_tests_wk,
sum(neg_tests_wk) neg_tests_wk,
sum(neg_only_tests_wk) neg_only_tests_wk,
sum(ind_only_tests_wk) ind_only_tests_wk,
sum(order_no_res_wk) order_no_res_wk,
sum(pats_tested_cumu) pats_tested_cumu,
sum(pos_tests_cumu) as pos_tests_cumu,
sum(neg_tests_cumu) as neg_tests_cumu,
sum(cat_any_rsv_any_test_within_wk) as cat_any_rsv_any_test_within_wk,
sum(cat_any_rsv_pos_test_within_wk) as cat_any_rsv_pos_test_within_wk,
sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
sum(cat_any_influenza_any_test_within_wk) as cat_any_influenza_any_test_within_wk,
sum(cat_any_influenza_pos_test_within_wk) as cat_any_influenza_pos_test_within_wk,
sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
sum(cat_any_h_metapneumovirus_any_test_within_wk) as cat_any_h_metapneumovirus_any_test_within_wk,
sum(cat_any_h_metapneumovirus_pos_test_within_wk) as cat_any_h_metapneumovirus_pos_test_within_wk,
sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
sum(cat_any_parainfluenza_any_test_within_wk) as cat_any_parainfluenza_any_test_within_wk,
sum(cat_any_parainfluenza_pos_test_within_wk) as cat_any_parainfluenza_pos_test_within_wk,
sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
sum(cat_any_adenovirus_any_test_within_wk) as cat_any_adenovirus_any_test_within_wk,
sum(cat_any_adenovirus_pos_test_within_wk) as cat_any_adenovirus_pos_test_within_wk,
sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
sum(cat_any_rhino_entero_virus_any_test_within_wk) as cat_any_rhino_entero_virus_any_test_within_wk,
sum(cat_any_rhino_entero_virus_pos_test_within_wk) as cat_any_rhino_entero_virus_pos_test_within_wk,
sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
sum(cat_any_coronavirus_non19_any_test_within_wk) as cat_any_coronavirus_non19_any_test_within_wk,
sum(cat_any_coronavirus_non19_pos_test_within_wk) as cat_any_coronavirus_non19_pos_test_within_wk,
sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
sum(cat_any_m_pneumoniae_any_test_within_wk) as cat_any_m_pneumoniae_any_test_within_wk,
sum(cat_any_m_pneumoniae_pos_test_within_wk) as cat_any_m_pneumoniae_pos_test_within_wk,
sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
sum(cat_any_c_pneumoniae_any_test_within_wk) as cat_any_c_pneumoniae_any_test_within_wk,
sum(cat_any_c_pneumoniae_pos_test_within_wk) as cat_any_c_pneumoniae_pos_test_within_wk,
sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
sum(cat_any_parapertussis_any_test_within_wk) as cat_any_parapertussis_any_test_within_wk,
sum(cat_any_parapertussis_pos_test_within_wk) as cat_any_parapertussis_pos_test_within_wk,
sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
'1' as position
FROM covid19_report.covid_tot_full_strat
GROUP BY week_end_date, week_number
UNION
SELECT 
week_end_date, week_number, --'ALL' as zip5, 
'ALL' as age_group, gender, 'ALL' as race_ethnicity, 'ALL' as county,
sum(total_pats_wk) total_pats_wk, 
sum(total_covid19_cases_wk) total_covid19_cases_wk, 
sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,

sum(cat4_covid19_cases_wk) cat4_covid19_cases_wk, 
sum(cat4_covid19_pos_test_wk) cat4_covid19_pos_test_wk,
sum(cat4_covid19_neg_test_wk) cat4_covid19_neg_test_wk,
sum(cat4_covid19_no_result_test_wk) cat4_covid19_no_result_test_wk,

sum(cat5_covid19_cases_wk) cat5_covid19_cases_wk, 
sum(cat5_covid19_pos_test_wk) cat5_covid19_pos_test_wk,
sum(cat5_covid19_neg_test_wk) cat5_covid19_neg_test_wk,
sum(cat5_covid19_no_result_test_wk) cat5_covid19_no_result_test_wk,

sum(cat6_covid19_cases_wk) cat6_covid19_cases_wk, 
sum(cat6_covid19_pos_test_wk) cat6_covid19_pos_test_wk,
sum(cat6_covid19_neg_test_wk) cat6_covid19_neg_test_wk,
sum(cat6_covid19_no_result_test_wk) cat6_covid19_no_result_test_wk,



case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
sum(cat_any_covid19_any_test_within_wk) cat_any_covid19_any_test_within_wk,
sum(cat_any_covid19_pos_test_within_wk) cat_any_covid19_pos_test_within_wk,
sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,

sum(cat4_covid19_any_test_within_wk) cat4_covid19_any_test_within_wk,
sum(cat5_covid19_any_test_within_wk) cat5_covid19_any_test_within_wk,
sum(cat6_covid19_any_test_within_wk) cat6_covid19_any_test_within_wk,

sum(covid19_cumu_total) covid19_cumu_total,
sum(cat1_covid19_cumu) cat1_covid19_cumu,
sum(cat2_covid19_cumu) cat2_covid19_cumu,
sum(cat3_covid19_cumu) cat3_covid19_cumu,
sum(cat4_covid19_cumu) cat4_covid19_cumu,
sum(cat5_covid19_cumu) cat5_covid19_cumu,
sum(cat6_covid19_cumu) cat6_covid19_cumu,
sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,
sum(cat4_covid19_cumu_pos_test) cat4_covid19_cumu_pos_test,
sum(cat4_covid19_cumu_neg_test) cat4_covid19_cumu_neg_test,
sum(cat5_covid19_cumu_pos_test) cat5_covid19_cumu_pos_test,
sum(cat5_covid19_cumu_neg_test) cat5_covid19_cumu_neg_test,
sum(cat6_covid19_cumu_pos_test) cat6_covid19_cumu_pos_test,
sum(cat6_covid19_cumu_neg_test) cat6_covid19_cumu_neg_test,

sum(pats_tested_wk) pats_tested_wk,
sum(pos_tests_wk) pos_tests_wk,
sum(neg_tests_wk) neg_tests_wk,
sum(neg_only_tests_wk) neg_only_tests_wk,
sum(ind_only_tests_wk) ind_only_tests_wk,
sum(order_no_res_wk) order_no_res_wk,
sum(pats_tested_cumu) pats_tested_cumu,
sum(pos_tests_cumu) as pos_tests_cumu,
sum(neg_tests_cumu) as neg_tests_cumu,
sum(cat_any_rsv_any_test_within_wk) as cat_any_rsv_any_test_within_wk,
sum(cat_any_rsv_pos_test_within_wk) as cat_any_rsv_pos_test_within_wk,
sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
sum(cat_any_influenza_any_test_within_wk) as cat_any_influenza_any_test_within_wk,
sum(cat_any_influenza_pos_test_within_wk) as cat_any_influenza_pos_test_within_wk,
sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
sum(cat_any_h_metapneumovirus_any_test_within_wk) as cat_any_h_metapneumovirus_any_test_within_wk,
sum(cat_any_h_metapneumovirus_pos_test_within_wk) as cat_any_h_metapneumovirus_pos_test_within_wk,
sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
sum(cat_any_parainfluenza_any_test_within_wk) as cat_any_parainfluenza_any_test_within_wk,
sum(cat_any_parainfluenza_pos_test_within_wk) as cat_any_parainfluenza_pos_test_within_wk,
sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
sum(cat_any_adenovirus_any_test_within_wk) as cat_any_adenovirus_any_test_within_wk,
sum(cat_any_adenovirus_pos_test_within_wk) as cat_any_adenovirus_pos_test_within_wk,
sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
sum(cat_any_rhino_entero_virus_any_test_within_wk) as cat_any_rhino_entero_virus_any_test_within_wk,
sum(cat_any_rhino_entero_virus_pos_test_within_wk) as cat_any_rhino_entero_virus_pos_test_within_wk,
sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
sum(cat_any_coronavirus_non19_any_test_within_wk) as cat_any_coronavirus_non19_any_test_within_wk,
sum(cat_any_coronavirus_non19_pos_test_within_wk) as cat_any_coronavirus_non19_pos_test_within_wk,
sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
sum(cat_any_m_pneumoniae_any_test_within_wk) as cat_any_m_pneumoniae_any_test_within_wk,
sum(cat_any_m_pneumoniae_pos_test_within_wk) as cat_any_m_pneumoniae_pos_test_within_wk,
sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
sum(cat_any_c_pneumoniae_any_test_within_wk) as cat_any_c_pneumoniae_any_test_within_wk,
sum(cat_any_c_pneumoniae_pos_test_within_wk) as cat_any_c_pneumoniae_pos_test_within_wk,
sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
sum(cat_any_parapertussis_any_test_within_wk) as cat_any_parapertussis_any_test_within_wk,
sum(cat_any_parapertussis_pos_test_within_wk) as cat_any_parapertussis_pos_test_within_wk,
sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
'2' as position
FROM covid19_report.covid_tot_full_strat
GROUP BY week_end_date, week_number, gender
UNION
SELECT 
week_end_date, week_number, --'ALL' as zip5, 
age_group, 'ALL' as gender, 'ALL' as race_ethnicity, 'ALL' as county,
sum(total_pats_wk) total_pats_wk, 
sum(total_covid19_cases_wk) total_covid19_cases_wk, 
sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,

sum(cat4_covid19_cases_wk) cat4_covid19_cases_wk, 
sum(cat4_covid19_pos_test_wk) cat4_covid19_pos_test_wk,
sum(cat4_covid19_neg_test_wk) cat4_covid19_neg_test_wk,
sum(cat4_covid19_no_result_test_wk) cat4_covid19_no_result_test_wk,

sum(cat5_covid19_cases_wk) cat5_covid19_cases_wk, 
sum(cat5_covid19_pos_test_wk) cat5_covid19_pos_test_wk,
sum(cat5_covid19_neg_test_wk) cat5_covid19_neg_test_wk,
sum(cat5_covid19_no_result_test_wk) cat5_covid19_no_result_test_wk,

sum(cat6_covid19_cases_wk) cat6_covid19_cases_wk, 
sum(cat6_covid19_pos_test_wk) cat6_covid19_pos_test_wk,
sum(cat6_covid19_neg_test_wk) cat6_covid19_neg_test_wk,
sum(cat6_covid19_no_result_test_wk) cat6_covid19_no_result_test_wk,


case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
sum(cat_any_covid19_any_test_within_wk) cat_any_covid19_any_test_within_wk,
sum(cat_any_covid19_pos_test_within_wk) cat_any_covid19_pos_test_within_wk,
sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,

sum(cat4_covid19_any_test_within_wk) cat4_covid19_any_test_within_wk,
sum(cat5_covid19_any_test_within_wk) cat5_covid19_any_test_within_wk,
sum(cat6_covid19_any_test_within_wk) cat6_covid19_any_test_within_wk,

sum(covid19_cumu_total) covid19_cumu_total,
sum(cat1_covid19_cumu) cat1_covid19_cumu,
sum(cat2_covid19_cumu) cat2_covid19_cumu,
sum(cat3_covid19_cumu) cat3_covid19_cumu,
sum(cat4_covid19_cumu) cat4_covid19_cumu,
sum(cat5_covid19_cumu) cat5_covid19_cumu,
sum(cat6_covid19_cumu) cat6_covid19_cumu,
sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,
sum(cat4_covid19_cumu_pos_test) cat4_covid19_cumu_pos_test,
sum(cat4_covid19_cumu_neg_test) cat4_covid19_cumu_neg_test,
sum(cat5_covid19_cumu_pos_test) cat5_covid19_cumu_pos_test,
sum(cat5_covid19_cumu_neg_test) cat5_covid19_cumu_neg_test,
sum(cat6_covid19_cumu_pos_test) cat6_covid19_cumu_pos_test,
sum(cat6_covid19_cumu_neg_test) cat6_covid19_cumu_neg_test,


sum(pats_tested_wk) pats_tested_wk,
sum(pos_tests_wk) pos_tests_wk,
sum(neg_tests_wk) neg_tests_wk,
sum(neg_only_tests_wk) neg_only_tests_wk,
sum(ind_only_tests_wk) ind_only_tests_wk,
sum(order_no_res_wk) order_no_res_wk,
sum(pats_tested_cumu) pats_tested_cumu,
sum(pos_tests_cumu) as pos_tests_cumu,
sum(neg_tests_cumu) as neg_tests_cumu,
sum(cat_any_rsv_any_test_within_wk) as cat_any_rsv_any_test_within_wk,
sum(cat_any_rsv_pos_test_within_wk) as cat_any_rsv_pos_test_within_wk,
sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
sum(cat_any_influenza_any_test_within_wk) as cat_any_influenza_any_test_within_wk,
sum(cat_any_influenza_pos_test_within_wk) as cat_any_influenza_pos_test_within_wk,
sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
sum(cat_any_h_metapneumovirus_any_test_within_wk) as cat_any_h_metapneumovirus_any_test_within_wk,
sum(cat_any_h_metapneumovirus_pos_test_within_wk) as cat_any_h_metapneumovirus_pos_test_within_wk,
sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
sum(cat_any_parainfluenza_any_test_within_wk) as cat_any_parainfluenza_any_test_within_wk,
sum(cat_any_parainfluenza_pos_test_within_wk) as cat_any_parainfluenza_pos_test_within_wk,
sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
sum(cat_any_adenovirus_any_test_within_wk) as cat_any_adenovirus_any_test_within_wk,
sum(cat_any_adenovirus_pos_test_within_wk) as cat_any_adenovirus_pos_test_within_wk,
sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
sum(cat_any_rhino_entero_virus_any_test_within_wk) as cat_any_rhino_entero_virus_any_test_within_wk,
sum(cat_any_rhino_entero_virus_pos_test_within_wk) as cat_any_rhino_entero_virus_pos_test_within_wk,
sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
sum(cat_any_coronavirus_non19_any_test_within_wk) as cat_any_coronavirus_non19_any_test_within_wk,
sum(cat_any_coronavirus_non19_pos_test_within_wk) as cat_any_coronavirus_non19_pos_test_within_wk,
sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
sum(cat_any_m_pneumoniae_any_test_within_wk) as cat_any_m_pneumoniae_any_test_within_wk,
sum(cat_any_m_pneumoniae_pos_test_within_wk) as cat_any_m_pneumoniae_pos_test_within_wk,
sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
sum(cat_any_c_pneumoniae_any_test_within_wk) as cat_any_c_pneumoniae_any_test_within_wk,
sum(cat_any_c_pneumoniae_pos_test_within_wk) as cat_any_c_pneumoniae_pos_test_within_wk,
sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
sum(cat_any_parapertussis_any_test_within_wk) as cat_any_parapertussis_any_test_within_wk,
sum(cat_any_parapertussis_pos_test_within_wk) as cat_any_parapertussis_pos_test_within_wk,
sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
'3' as position
FROM covid19_report.covid_tot_full_strat
GROUP BY week_end_date, week_number, age_group
UNION
SELECT 
week_end_date, week_number, --'ALL' as zip5, 
'ALL' as age_group, 'ALL' as gender, race_ethnicity, 'ALL' as county,
sum(total_pats_wk) total_pats_wk, 
sum(total_covid19_cases_wk) total_covid19_cases_wk, 
sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,

sum(cat4_covid19_cases_wk) cat4_covid19_cases_wk, 
sum(cat4_covid19_pos_test_wk) cat4_covid19_pos_test_wk,
sum(cat4_covid19_neg_test_wk) cat4_covid19_neg_test_wk,
sum(cat4_covid19_no_result_test_wk) cat4_covid19_no_result_test_wk,

sum(cat5_covid19_cases_wk) cat5_covid19_cases_wk, 
sum(cat5_covid19_pos_test_wk) cat5_covid19_pos_test_wk,
sum(cat5_covid19_neg_test_wk) cat5_covid19_neg_test_wk,
sum(cat5_covid19_no_result_test_wk) cat5_covid19_no_result_test_wk,

sum(cat6_covid19_cases_wk) cat6_covid19_cases_wk, 
sum(cat6_covid19_pos_test_wk) cat6_covid19_pos_test_wk,
sum(cat6_covid19_neg_test_wk) cat6_covid19_neg_test_wk,
sum(cat6_covid19_no_result_test_wk) cat6_covid19_no_result_test_wk,



case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
sum(cat_any_covid19_any_test_within_wk) cat_any_covid19_any_test_within_wk,
sum(cat_any_covid19_pos_test_within_wk) cat_any_covid19_pos_test_within_wk,
sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,

sum(cat4_covid19_any_test_within_wk) cat4_covid19_any_test_within_wk,
sum(cat5_covid19_any_test_within_wk) cat5_covid19_any_test_within_wk,
sum(cat6_covid19_any_test_within_wk) cat6_covid19_any_test_within_wk,


sum(covid19_cumu_total) covid19_cumu_total,
sum(cat1_covid19_cumu) cat1_covid19_cumu,
sum(cat2_covid19_cumu) cat2_covid19_cumu,
sum(cat3_covid19_cumu) cat3_covid19_cumu,
sum(cat4_covid19_cumu) cat4_covid19_cumu,
sum(cat5_covid19_cumu) cat5_covid19_cumu,
sum(cat6_covid19_cumu) cat6_covid19_cumu,
sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,
sum(cat4_covid19_cumu_pos_test) cat4_covid19_cumu_pos_test,
sum(cat4_covid19_cumu_neg_test) cat4_covid19_cumu_neg_test,
sum(cat5_covid19_cumu_pos_test) cat5_covid19_cumu_pos_test,
sum(cat5_covid19_cumu_neg_test) cat5_covid19_cumu_neg_test,
sum(cat6_covid19_cumu_pos_test) cat6_covid19_cumu_pos_test,
sum(cat6_covid19_cumu_neg_test) cat6_covid19_cumu_neg_test,


sum(pats_tested_wk) pats_tested_wk,
sum(pos_tests_wk) pos_tests_wk,
sum(neg_tests_wk) neg_tests_wk,
sum(neg_only_tests_wk) neg_only_tests_wk,
sum(ind_only_tests_wk) ind_only_tests_wk,
sum(order_no_res_wk) order_no_res_wk,
sum(pats_tested_cumu) pats_tested_cumu,
sum(pos_tests_cumu) as pos_tests_cumu,
sum(neg_tests_cumu) as neg_tests_cumu,
sum(cat_any_rsv_any_test_within_wk) as cat_any_rsv_any_test_within_wk,
sum(cat_any_rsv_pos_test_within_wk) as cat_any_rsv_pos_test_within_wk,
sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
sum(cat_any_influenza_any_test_within_wk) as cat_any_influenza_any_test_within_wk,
sum(cat_any_influenza_pos_test_within_wk) as cat_any_influenza_pos_test_within_wk,
sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
sum(cat_any_h_metapneumovirus_any_test_within_wk) as cat_any_h_metapneumovirus_any_test_within_wk,
sum(cat_any_h_metapneumovirus_pos_test_within_wk) as cat_any_h_metapneumovirus_pos_test_within_wk,
sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
sum(cat_any_parainfluenza_any_test_within_wk) as cat_any_parainfluenza_any_test_within_wk,
sum(cat_any_parainfluenza_pos_test_within_wk) as cat_any_parainfluenza_pos_test_within_wk,
sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
sum(cat_any_adenovirus_any_test_within_wk) as cat_any_adenovirus_any_test_within_wk,
sum(cat_any_adenovirus_pos_test_within_wk) as cat_any_adenovirus_pos_test_within_wk,
sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
sum(cat_any_rhino_entero_virus_any_test_within_wk) as cat_any_rhino_entero_virus_any_test_within_wk,
sum(cat_any_rhino_entero_virus_pos_test_within_wk) as cat_any_rhino_entero_virus_pos_test_within_wk,
sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
sum(cat_any_coronavirus_non19_any_test_within_wk) as cat_any_coronavirus_non19_any_test_within_wk,
sum(cat_any_coronavirus_non19_pos_test_within_wk) as cat_any_coronavirus_non19_pos_test_within_wk,
sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
sum(cat_any_m_pneumoniae_any_test_within_wk) as cat_any_m_pneumoniae_any_test_within_wk,
sum(cat_any_m_pneumoniae_pos_test_within_wk) as cat_any_m_pneumoniae_pos_test_within_wk,
sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
sum(cat_any_c_pneumoniae_any_test_within_wk) as cat_any_c_pneumoniae_any_test_within_wk,
sum(cat_any_c_pneumoniae_pos_test_within_wk) as cat_any_c_pneumoniae_pos_test_within_wk,
sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
sum(cat_any_parapertussis_any_test_within_wk) as cat_any_parapertussis_any_test_within_wk,
sum(cat_any_parapertussis_pos_test_within_wk) as cat_any_parapertussis_pos_test_within_wk,
sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
'4' as position
FROM covid19_report.covid_tot_full_strat
GROUP BY week_end_date, week_number, race_ethnicity
UNION
SELECT 
week_end_date, week_number, --'ALL' as zip5, 
'ALL' as age_group, 'ALL' as gender, 'ALL' as race_ethnicity, county,
sum(total_pats_wk) total_pats_wk, 
sum(total_covid19_cases_wk) total_covid19_cases_wk, 
sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,


sum(cat4_covid19_cases_wk) cat4_covid19_cases_wk, 
sum(cat4_covid19_pos_test_wk) cat4_covid19_pos_test_wk,
sum(cat4_covid19_neg_test_wk) cat4_covid19_neg_test_wk,
sum(cat4_covid19_no_result_test_wk) cat4_covid19_no_result_test_wk,

sum(cat5_covid19_cases_wk) cat5_covid19_cases_wk, 
sum(cat5_covid19_pos_test_wk) cat5_covid19_pos_test_wk,
sum(cat5_covid19_neg_test_wk) cat5_covid19_neg_test_wk,
sum(cat5_covid19_no_result_test_wk) cat5_covid19_no_result_test_wk,

sum(cat6_covid19_cases_wk) cat6_covid19_cases_wk, 
sum(cat6_covid19_pos_test_wk) cat6_covid19_pos_test_wk,
sum(cat6_covid19_neg_test_wk) cat6_covid19_neg_test_wk,
sum(cat6_covid19_no_result_test_wk) cat6_covid19_no_result_test_wk,


case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
sum(cat_any_covid19_any_test_within_wk) cat_any_covid19_any_test_within_wk,
sum(cat_any_covid19_pos_test_within_wk) cat_any_covid19_pos_test_within_wk,
sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,

sum(cat4_covid19_any_test_within_wk) cat4_covid19_any_test_within_wk,
sum(cat5_covid19_any_test_within_wk) cat5_covid19_any_test_within_wk,
sum(cat6_covid19_any_test_within_wk) cat6_covid19_any_test_within_wk,


sum(covid19_cumu_total) covid19_cumu_total,
sum(cat1_covid19_cumu) cat1_covid19_cumu,
sum(cat2_covid19_cumu) cat2_covid19_cumu,
sum(cat3_covid19_cumu) cat3_covid19_cumu,
sum(cat4_covid19_cumu) cat4_covid19_cumu,
sum(cat5_covid19_cumu) cat5_covid19_cumu,
sum(cat6_covid19_cumu) cat6_covid19_cumu,
sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,
sum(cat4_covid19_cumu_pos_test) cat4_covid19_cumu_pos_test,
sum(cat4_covid19_cumu_neg_test) cat4_covid19_cumu_neg_test,
sum(cat5_covid19_cumu_pos_test) cat5_covid19_cumu_pos_test,
sum(cat5_covid19_cumu_neg_test) cat5_covid19_cumu_neg_test,
sum(cat6_covid19_cumu_pos_test) cat6_covid19_cumu_pos_test,
sum(cat6_covid19_cumu_neg_test) cat6_covid19_cumu_neg_test,


sum(pats_tested_wk) pats_tested_wk,
sum(pos_tests_wk) pos_tests_wk,
sum(neg_tests_wk) neg_tests_wk,
sum(neg_only_tests_wk) neg_only_tests_wk,
sum(ind_only_tests_wk) ind_only_tests_wk,
sum(order_no_res_wk) order_no_res_wk,
sum(pats_tested_cumu) pats_tested_cumu,
sum(pos_tests_cumu) as pos_tests_cumu,
sum(neg_tests_cumu) as neg_tests_cumu,
sum(cat_any_rsv_any_test_within_wk) as cat_any_rsv_any_test_within_wk,
sum(cat_any_rsv_pos_test_within_wk) as cat_any_rsv_pos_test_within_wk,
sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
sum(cat_any_influenza_any_test_within_wk) as cat_any_influenza_any_test_within_wk,
sum(cat_any_influenza_pos_test_within_wk) as cat_any_influenza_pos_test_within_wk,
sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
sum(cat_any_h_metapneumovirus_any_test_within_wk) as cat_any_h_metapneumovirus_any_test_within_wk,
sum(cat_any_h_metapneumovirus_pos_test_within_wk) as cat_any_h_metapneumovirus_pos_test_within_wk,
sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
sum(cat_any_parainfluenza_any_test_within_wk) as cat_any_parainfluenza_any_test_within_wk,
sum(cat_any_parainfluenza_pos_test_within_wk) as cat_any_parainfluenza_pos_test_within_wk,
sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
sum(cat_any_adenovirus_any_test_within_wk) as cat_any_adenovirus_any_test_within_wk,
sum(cat_any_adenovirus_pos_test_within_wk) as cat_any_adenovirus_pos_test_within_wk,
sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
sum(cat_any_rhino_entero_virus_any_test_within_wk) as cat_any_rhino_entero_virus_any_test_within_wk,
sum(cat_any_rhino_entero_virus_pos_test_within_wk) as cat_any_rhino_entero_virus_pos_test_within_wk,
sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
sum(cat_any_coronavirus_non19_any_test_within_wk) as cat_any_coronavirus_non19_any_test_within_wk,
sum(cat_any_coronavirus_non19_pos_test_within_wk) as cat_any_coronavirus_non19_pos_test_within_wk,
sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
sum(cat_any_m_pneumoniae_any_test_within_wk) as cat_any_m_pneumoniae_any_test_within_wk,
sum(cat_any_m_pneumoniae_pos_test_within_wk) as cat_any_m_pneumoniae_pos_test_within_wk,
sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
sum(cat_any_c_pneumoniae_any_test_within_wk) as cat_any_c_pneumoniae_any_test_within_wk,
sum(cat_any_c_pneumoniae_pos_test_within_wk) as cat_any_c_pneumoniae_pos_test_within_wk,
sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
sum(cat_any_parapertussis_any_test_within_wk) as cat_any_parapertussis_any_test_within_wk,
sum(cat_any_parapertussis_pos_test_within_wk) as cat_any_parapertussis_pos_test_within_wk,
sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
'5' as position
FROM covid19_report.covid_tot_full_strat
GROUP BY week_end_date, week_number, county
-- UNION
-- SELECT 
-- week_end_date, week_number, zip5::text, 'ALL' as age_group, 'ALL' as gender, 'ALL' as race_ethnicity, 'ALL' as county,
-- sum(total_pats_wk) total_pats_wk, 
-- sum(total_covid19_cases_wk) total_covid19_cases_wk, 
-- sum(cat1_covid19_cases_wk) cat1_covid19_cases_wk,
-- sum(cat1_covid19_pos_test_wk) cat1_covid19_pos_test_wk,
-- sum(cat1_covid19_neg_test_wk) cat1_covid19_neg_test_wk,
-- sum(cat1_covid19_no_result_test_wk) cat1_covid19_no_result_test_wk,
-- sum(cat2_covid19_cases_wk) cat2_covid19_cases_wk, 
-- sum(cat2_covid19_pos_test_wk) cat2_covid19_pos_test_wk,
-- sum(cat2_covid19_neg_test_wk) cat2_covid19_neg_test_wk,
-- sum(cat2_covid19_no_result_test_wk) cat2_covid19_no_result_test_wk,
-- sum(cat3_covid19_cases_wk) cat3_covid19_cases_wk, 
-- sum(cat3_covid19_pos_test_wk) cat3_covid19_pos_test_wk,
-- sum(cat3_covid19_neg_test_wk) cat3_covid19_neg_test_wk,
-- sum(cat3_covid19_no_result_test_wk) cat3_covid19_no_result_test_wk,
-- case when (sum(total_covid19_cases_wk)) > 0 then coalesce(round(((sum(total_covid19_cases_wk)::integer / sum(total_pats_wk)::float)*100)::numeric, 2), 0) else 0 end as pct_pats_w_cases_wk,
-- sum(cat123_covid19_any_test_within_wk) cat123_covid19_any_test_within_wk,
-- sum(cat123_covid19_pos_test_within_wk) cat123_covid19_pos_test_within_wk,
-- sum(cat1_covid19_any_test_within_wk) cat1_covid19_any_test_within_wk,
-- sum(cat2_covid19_any_test_within_wk) cat2_covid19_any_test_within_wk,
-- sum(cat3_covid19_any_test_within_wk) cat3_covid19_any_test_within_wk,
-- sum(covid19_cumu_total) covid19_cumu_total,
-- sum(cat1_covid19_cumu) cat1_covid19_cumu,
-- sum(cat2_covid19_cumu) cat2_covid19_cumu,
-- sum(cat3_covid19_cumu) cat3_covid19_cumu,
-- sum(cat1_covid19_cumu_pos_test) cat1_covid19_cumu_pos_test,
-- sum(cat1_covid19_cumu_neg_test) cat1_covid19_cumu_neg_test,
-- sum(cat2_covid19_cumu_pos_test) cat2_covid19_cumu_pos_test,
-- sum(cat2_covid19_cumu_neg_test) cat2_covid19_cumu_neg_test,
-- sum(cat3_covid19_cumu_pos_test) cat3_covid19_cumu_pos_test,
-- sum(cat3_covid19_cumu_neg_test) cat3_covid19_cumu_neg_test,
-- sum(pats_tested_wk) pats_tested_wk,
-- sum(pos_tests_wk) pos_tests_wk,
-- sum(neg_tests_wk) neg_tests_wk,
-- sum(neg_only_tests_wk) neg_only_tests_wk,
-- sum(ind_only_tests_wk) ind_only_tests_wk,
-- sum(order_no_res_wk) order_no_res_wk,
-- sum(pats_tested_cumu) pats_tested_cumu,
-- sum(pos_tests_cumu) as pos_tests_cumu,
-- sum(neg_tests_cumu) as neg_tests_cumu,
-- sum(cat123_rsv_any_test_within_wk) as cat123_rsv_any_test_within_wk,
-- sum(cat123_rsv_pos_test_within_wk) as cat123_rsv_pos_test_within_wk,
-- sum(rsv_pats_tested_wk) as rsv_pats_tested_wk,
-- sum(rsv_pos_tests_wk) as rsv_pos_tests_wk,
-- sum(cat123_influenza_any_test_within_wk) as cat123_influenza_any_test_within_wk,
-- sum(cat123_influenza_pos_test_within_wk) as cat123_influenza_pos_test_within_wk,
-- sum(influenza_pats_tested_wk) as influenza_pats_tested_wk,
-- sum(influenza_pos_tests_wk) as influenza_pos_tests_wk,
-- sum(cat123_h_metapneumovirus_any_test_within_wk) as cat123_h_metapneumovirus_any_test_within_wk,
-- sum(cat123_h_metapneumovirus_pos_test_within_wk) as cat123_h_metapneumovirus_pos_test_within_wk,
-- sum(h_metapneumovirus_pats_tested_wk) as h_metapneumovirus_pats_tested_wk,
-- sum(h_metapneumovirus_pos_tests_wk) as h_metapneumovirus_pos_tests_wk,
-- sum(cat123_parainfluenza_any_test_within_wk) as cat123_parainfluenza_any_test_within_wk,
-- sum(cat123_parainfluenza_pos_test_within_wk) as cat123_parainfluenza_pos_test_within_wk,
-- sum(parainfluenza_pats_tested_wk) as parainfluenza_pats_tested_wk,
-- sum(parainfluenza_pos_tests_wk) as parainfluenza_pos_tests_wk,
-- sum(cat123_adenovirus_any_test_within_wk) as cat123_adenovirus_any_test_within_wk,
-- sum(cat123_adenovirus_pos_test_within_wk) as cat123_adenovirus_pos_test_within_wk,
-- sum(adenovirus_pats_tested_wk) as adenovirus_pats_tested_wk,
-- sum(adenovirus_pos_tests_wk) as adenovirus_pos_tests_wk,
-- sum(cat123_rhino_entero_virus_any_test_within_wk) as cat123_rhino_entero_virus_any_test_within_wk,
-- sum(cat123_rhino_entero_virus_pos_test_within_wk) as cat123_rhino_entero_virus_pos_test_within_wk,
-- sum(rhino_entero_virus_pats_tested_wk) as rhino_entero_virus_pats_tested_wk,
-- sum(rhino_entero_virus_pos_tests_wk) as rhino_entero_virus_pos_tests_wk,
-- sum(cat123_coronavirus_non19_any_test_within_wk) as cat123_coronavirus_non19_any_test_within_wk,
-- sum(cat123_coronavirus_non19_pos_test_within_wk) as cat123_coronavirus_non19_pos_test_within_wk,
-- sum(coronavirus_non19_pats_tested_wk) as coronavirus_non19_pats_tested_wk,
-- sum(coronavirus_non19_pos_tests_wk) as coronavirus_non19_pos_tests_wk,
-- sum(cat123_m_pneumoniae_any_test_within_wk) as cat123_m_pneumoniae_any_test_within_wk,
-- sum(cat123_m_pneumoniae_pos_test_within_wk) as cat123_m_pneumoniae_pos_test_within_wk,
-- sum(m_pneumoniae_pats_tested_wk) as m_pneumoniae_pats_tested_wk,
-- sum(m_pneumoniae_pos_tests_wk) as m_pneumoniae_pos_tests_wk,
-- sum(cat123_c_pneumoniae_any_test_within_wk) as cat123_c_pneumoniae_any_test_within_wk,
-- sum(cat123_c_pneumoniae_pos_test_within_wk) as cat123_c_pneumoniae_pos_test_within_wk,
-- sum(c_pneumoniae_pats_tested_wk) as c_pneumoniae_pats_tested_wk,
-- sum(c_pneumoniae_pos_tests_wk) as c_pneumoniae_pos_tests_wk,
-- sum(cat123_parapertussis_any_test_within_wk) as cat123_parapertussis_any_test_within_wk,
-- sum(cat123_parapertussis_pos_test_within_wk) as cat123_parapertussis_pos_test_within_wk,
-- sum(parapertussis_pats_tested_wk) as parapertussis_pats_tested_wk,
-- sum(parapertussis_pos_tests_wk) as parapertussis_pos_tests_wk,
-- '6' as position
-- FROM covid19_report.covid_tot_full_strat
-- GROUP BY week_end_date, week_number, zip5
ORDER BY position, age_group, gender, race_ethnicity, county; --zip5;

CREATE TABLE covid19_report.covid_output_formatted AS
SELECT
week_end_date,
week_number,
--zip5,
age_group,
gender,
race_ethnicity,
county,
total_pats_wk,
total_covid19_cases_wk,
cat1_covid19_cases_wk,
cat1_covid19_pos_test_wk,
cat1_covid19_neg_test_wk,
cat1_covid19_no_result_test_wk,
cat2_covid19_cases_wk,
cat2_covid19_pos_test_wk,
cat2_covid19_neg_test_wk,
cat2_covid19_no_result_test_wk,
cat3_covid19_cases_wk,
cat3_covid19_pos_test_wk,
cat3_covid19_neg_test_wk,
cat3_covid19_no_result_test_wk,

cat4_covid19_cases_wk,
cat4_covid19_pos_test_wk,
cat4_covid19_neg_test_wk,
cat4_covid19_no_result_test_wk,

cat5_covid19_cases_wk,
cat5_covid19_pos_test_wk,
cat5_covid19_neg_test_wk,
cat5_covid19_no_result_test_wk,

cat6_covid19_cases_wk,
cat6_covid19_pos_test_wk,
cat6_covid19_neg_test_wk,
cat6_covid19_no_result_test_wk,


pct_pats_w_cases_wk,
cat_any_covid19_any_test_within_wk,
cat_any_covid19_pos_test_within_wk,
cat1_covid19_any_test_within_wk,
cat2_covid19_any_test_within_wk,
cat3_covid19_any_test_within_wk,

cat4_covid19_any_test_within_wk,
cat5_covid19_any_test_within_wk,
cat6_covid19_any_test_within_wk,

covid19_cumu_total,
cat1_covid19_cumu,
cat1_covid19_cumu_pos_test,
cat1_covid19_cumu_neg_test,
cat2_covid19_cumu,
cat2_covid19_cumu_pos_test,
cat2_covid19_cumu_neg_test,
cat3_covid19_cumu,
cat3_covid19_cumu_pos_test,
cat3_covid19_cumu_neg_test,
cat4_covid19_cumu,
cat4_covid19_cumu_pos_test,
cat4_covid19_cumu_neg_test,
cat5_covid19_cumu,
cat5_covid19_cumu_pos_test,
cat5_covid19_cumu_neg_test,
cat6_covid19_cumu,
cat6_covid19_cumu_pos_test,
cat6_covid19_cumu_neg_test,
pats_tested_wk,
pos_tests_wk,
neg_tests_wk,
neg_only_tests_wk,
ind_only_tests_wk,
order_no_res_wk,
pats_tested_cumu,
pos_tests_cumu,
neg_tests_cumu,
cat_any_rsv_any_test_within_wk,
cat_any_rsv_pos_test_within_wk,
rsv_pats_tested_wk,
rsv_pos_tests_wk,
cat_any_influenza_any_test_within_wk,
cat_any_influenza_pos_test_within_wk,
influenza_pats_tested_wk,
influenza_pos_tests_wk,
cat_any_h_metapneumovirus_any_test_within_wk,
cat_any_h_metapneumovirus_pos_test_within_wk,
h_metapneumovirus_pats_tested_wk,
h_metapneumovirus_pos_tests_wk,
cat_any_parainfluenza_any_test_within_wk,
cat_any_parainfluenza_pos_test_within_wk,
parainfluenza_pats_tested_wk,
parainfluenza_pos_tests_wk,
cat_any_adenovirus_any_test_within_wk,
cat_any_adenovirus_pos_test_within_wk,
adenovirus_pats_tested_wk,
adenovirus_pos_tests_wk,
cat_any_rhino_entero_virus_any_test_within_wk,
cat_any_rhino_entero_virus_pos_test_within_wk,
rhino_entero_virus_pats_tested_wk,
rhino_entero_virus_pos_tests_wk,
cat_any_coronavirus_non19_any_test_within_wk,
cat_any_coronavirus_non19_pos_test_within_wk,
coronavirus_non19_pats_tested_wk,
coronavirus_non19_pos_tests_wk,
cat_any_m_pneumoniae_any_test_within_wk,
cat_any_m_pneumoniae_pos_test_within_wk,
m_pneumoniae_pats_tested_wk,
m_pneumoniae_pos_tests_wk,
cat_any_c_pneumoniae_any_test_within_wk,
cat_any_c_pneumoniae_pos_test_within_wk,
c_pneumoniae_pats_tested_wk,
c_pneumoniae_pos_tests_wk,
cat_any_parapertussis_any_test_within_wk,
cat_any_parapertussis_pos_test_within_wk,
parapertussis_pats_tested_wk,
parapertussis_pos_tests_wk
FROM covid19_report.covid_output;

-- Need to add check that all the data has loaded?
-- But it is the week before so should be fine right?










