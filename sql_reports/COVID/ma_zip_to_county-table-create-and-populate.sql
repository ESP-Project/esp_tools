CREATE TABLE covid19_report.ma_zip_to_county
(
    zip character varying(10) COLLATE pg_catalog."default" NOT NULL,
    city character varying(50) COLLATE pg_catalog."default" NOT NULL,
    county character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT ma_zip_to_county_pkey PRIMARY KEY (zip, city)

)
WITH (
    OIDS = FALSE
);


ALTER TABLE covid19_report.ma_zip_to_county
    OWNER to esp;


sudo su postgres
psql -d esp	
	
COPY covid19_report.ma_zip_to_county("zip", "city", "county")
	FROM '/tmp/ma-zip-codes-with-county-ESP.csv'
	WITH DELIMITER '^'
	CSV HEADER;
	
	
	

