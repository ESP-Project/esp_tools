DROP TABLE IF EXISTS covid19_report.suspect_combo_symptom;
DROP TABLE IF EXISTS covid19_report.suspect_combo_symptom_final;
DROP TABLE IF EXISTS covid19_report.suspect_combo_lag_compute;
DROP TABLE IF EXISTS covid19_report.suspect_combo_lag_events;
DROP TABLE IF EXISTS covid19_report.suspect_combo_primary_events;
DROP TABLE IF EXISTS covid19_report.suspect_index_cases_prep;
DROP TABLE IF EXISTS covid19_report.suspect_index_cases;


--SYMPTOMS WHERE A PATIENT NEEDS AT LEAST 2
CREATE TABLE covid19_report.suspect_combo_symptom AS
SELECT T1.patient_id, date, 
--normalize the ways fever can appear so they look like the same event
CASE WHEN name in ('enc:fever','dx:covid_suspect:fever') then 'fever' else name end as name 
FROM hef_event T1
LEFT JOIN (SELECT patient_id, max(date) max_date from covid19_report.suspect_nodis_case where condition = 'covid19_suspect' and date >= '2020-01-01' group by patient_id) T2 on (T1.patient_id = T2.patient_id)
WHERE name in ('enc:fever', 
               'dx:covid_suspect:fever', 
			   'dx:chills', 
			   'dx:covid_suspect:myalgia',
			   'dx:headache',
			   'dx:sore-throat',
			   'dx:olfactory-taste-disorders')
AND T1.date >= '2020-01-01'	
AND ( (T1.date > T2.max_date + INTERVAL '42 days') or T2.max_date is null)
GROUP BY T1.patient_id, date, name;  

-- Drop 'dx:olfactory-taste-disorders' if a patient has had a diagnosis code for this more than 14 days before
CREATE TABLE covid19_report.suspect_combo_symptom_final AS
SELECT * FROM covid19_report.suspect_combo_symptom
EXCEPT
SELECT DISTINCT T1.*
from covid19_report.suspect_combo_symptom T1
JOIN (SELECT patient_id, date from hef_event where name ='dx:olfactory-taste-disorders') T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.name = 'dx:olfactory-taste-disorders'
AND T2.date < T1.date - INTERVAL '14 days';


-- Compute lag interval for all qualifying symptom events
CREATE TABLE covid19_report.suspect_combo_lag_compute AS
SELECT patient_id, name,
	  date, 
	  date - lag(date, 1) OVER (partition by patient_id ORDER BY date) delta
	FROM covid19_report.suspect_combo_symptom_final
	ORDER BY patient_id,date;

-- These are events that are within 3 days of another qualifying event for the patient.	
CREATE TABLE covid19_report.suspect_combo_lag_events AS
select * from covid19_report.suspect_combo_lag_compute where delta <= 3;

-- The primary event needs to differ from lag event 
-- The symptoms need to be different
CREATE TABLE covid19_report.suspect_combo_primary_events AS
SELECT T1.*
FROM covid19_report.suspect_combo_lag_compute T1,
covid19_report.suspect_combo_lag_events T2
WHERE T1.patient_id = T2.patient_id 
AND (T1.date = (T2.date - T2.delta) AND T1.name != T2.name);


-- Cases to Create
-- Handle recurrence interval
CREATE TABLE covid19_report.suspect_index_cases_prep as
-- single diag code
	SELECT T1.patient_id, min(T1.date) min_date, 'single_symptom'::text as criteria
	FROM hef_event T1
	LEFT JOIN (SELECT patient_id, max(date) max_date from covid19_report.suspect_nodis_case where condition = 'covid19_suspect' and date >= '2020-01-01' group by patient_id) T2 on (T1.patient_id = T2.patient_id)
	WHERE name in ( 'dx:covid19',
					'dx:contact-communicable-disease',
					'dx:covid_suspect:cough',
					'dx:shortofbreath',
					'dx:difficulty_breathing',
					'dx:covid_suspect:pneumonia',
					'dx:ards')
	AND T1.date >= '2020-01-01'
	AND ( (T1.date > T2.max_date + INTERVAL '42 days') or T2.max_date is null)
	GROUP BY T1.patient_id, criteria
UNION
-- multiple specific symptoms within 3 days of one another
	SELECT patient_id, min(date) as min_date, 'multi_symptom' as criteria
	from ( select * from covid19_report.suspect_combo_lag_events 
		where patient_id in (select patient_id from covid19_report.suspect_combo_primary_events)
		UNION
		select * from covid19_report.suspect_combo_primary_events 
		order by patient_id, date) S1
	GROUP BY patient_id, criteria
UNION
-- COVID lab test of any type
	SELECT T1.patient_id, min(T1.date) as min_date, 'covid_lab'::text as criteria
	FROM emr_labresult T1
	JOIN conf_labtestmap T2 on (T1.native_code = T2.native_code)
	LEFT JOIN (SELECT patient_id, max(date) max_date from covid19_report.suspect_nodis_case where condition = 'covid19_suspect' and date >= '2020-01-01' group by patient_id) T3 on (T1.patient_id = T3.patient_id)
	WHERE T2.test_name in ('covid19_pcr', 'covid19_igg', 'covid19_igm', 'covid19_iga', 'covid19_ab_total')
	AND T1.date >= '2020-01-01'
	AND ( (T1.date > T3.max_date + INTERVAL '42 days') or T3.max_date is null)
	GROUP BY T1.patient_id, criteria;

CREATE TABLE covid19_report.suspect_index_cases as 
SELECT T1.patient_id, min(case_date) case_date, array_agg(criteria) as criteria		
FROM (SELECT patient_id, min(min_date) as case_date FROM covid19_report.suspect_index_cases_prep GROUP BY patient_id) T1
JOIN (SELECT patient_id, min_date, criteria from covid19_report.suspect_index_cases_prep GROUP BY patient_id, min_date, criteria) T2 ON (T1.case_date = T2.min_date and T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id;


-- Add the cases to the table
INSERT INTO covid19_report.suspect_nodis_case
 SELECT NEXTVAL('covid19_report.suspect_nodis_case_id_seq'), 'covid19_suspect'::text as condition, case_date, patient_id,
 criteria, 'covid-suspect.sql'::text as source, 'AR'::text as status, 
 null as notes, now() as created_timestamp, 
 now() as updated_timestamp, null as sent_timestamp 
 FROM covid19_report.suspect_index_cases;
 
 
-- DROP TABLE IF EXISTS covid19_report.suspect_combo_symptom;
-- DROP TABLE IF EXISTS covid19_report.suspect_combo_symptom_final;
-- DROP TABLE IF EXISTS covid19_report.suspect_combo_lag_compute;
-- DROP TABLE IF EXISTS covid19_report.suspect_combo_lag_events;
-- DROP TABLE IF EXISTS covid19_report.suspect_combo_primary_events;
-- DROP TABLE IF EXISTS covid19_report.suspect_index_cases;







