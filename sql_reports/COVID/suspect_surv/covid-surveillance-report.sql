

DROP TABLE IF EXISTS covid19_report.surv_index_pats;
DROP TABLE IF EXISTS covid19_report.surv_demographics;
DROP TABLE IF EXISTS covid19_report.surv_lab_results;
DROP TABLE IF EXISTS covid19_report.surv_lab_orders;
DROP TABLE IF EXISTS covid19_report.surv_encounters_with_codes;
DROP TABLE IF EXISTS covid19_report.surv_encounters_with_code_arrays;
DROP TABLE IF EXISTS covid19_report.surv_encounters_details;
DROP TABLE IF EXISTS covid19_report.surv_medications;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_1yr;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_1yr_arrays;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_details_1yr;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_2yr;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_2yr_arrays;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_details_2yr;
DROP TABLE IF EXISTS covid19_report.surv_health_cond_details_all;
DROP TABLE IF EXISTS covid19_report.surv_immuno_drugs_1yr;


DROP TABLE IF EXISTS covid19_report.surv_rpt_denom_output;
DROP TABLE IF EXISTS covid19_report.surv_demog_ouput;
DROP TABLE IF EXISTS covid19_report.surv_labresult_ouput;
DROP TABLE IF EXISTS covid19_report.surv_laborder_ouput;
DROP TABLE IF EXISTS covid19_report.surv_enc_ouput;
DROP TABLE IF EXISTS covid19_report.surv_med_ouput;
DROP TABLE IF EXISTS covid19_report.surv_hlthcond_ouput;



-- CLINICAL ENCOUNTERS FOR THE WEEK
CREATE TABLE covid19_report.surv_rpt_denom_output as
SELECT count(distinct(patient_id)) total_pats_w_clin_enc, week_start_date::date, week_end_date::date
FROM gen_pop_tools.clin_enc T1
JOIN (select :run_date::timestamp as week_end_date) T2 on (1=1)
JOIN (select :run_date::date - INTERVAL '6 days' as week_start_date) T3 on (1=1)
WHERE date>= week_start_date
AND date <= week_end_date
GROUP BY week_end_date, week_start_date;

	 
CREATE TABLE covid19_report.surv_index_pats as
SELECT patient_id, date as index_date
FROM covid19_report.suspect_nodis_case 
WHERE 
(
     (date >= (:run_date::date - INTERVAL '6 days')::date
     AND date <= :run_date::date)
     -- send cases created after weekly run for prior weeks
     OR (date <= :run_date::date and sent_timestamp is null)
)
-- HOLD FOR 14 DAYS FROM INDEX DATE BEFORE SENDING
AND date <= (now() - INTERVAL '14 days')::date;


-- GATHER DEMOGRAPHICS DATA
CREATE TABLE covid19_report.surv_demographics as
SELECT DISTINCT
T1.id as patient_id,
upper(T1.last_name) as last_name,
upper(T1.first_name) as first_name,
upper(T1.middle_name) as middle_name,
concat(T1.areacode, '-', tel) as phone_number,
T1.address1,
T1.address2,
T1.city,
T1.state,
T1.zip5,
T1.country,
date_of_birth::date,
case when upper(gender) in ('M', 'MALE') then 'MALE'
	 when upper(gender) in ('F', 'FEMALE') then 'FEMALE'
	else 'UNKNOWN' end as gender,
null as gender_identity,
CASE WHEN upper(gender) in ('M', 'MALE') and T5.sex_partner_gender = 'FEMALE' then 'HETEROSEXUAL'
     WHEN upper(gender) in ('F', 'FEMALE')  and T5.sex_partner_gender = 'MALE' then 'HETEROSEXUAL'
	 WHEN upper(gender) in ('M', 'MALE') and T5.sex_partner_gender = 'MALE' then 'HOMOSEXUAL'
	 WHEN upper(gender) in ('F', 'FEMALE')  and T5.sex_partner_gender = 'FEMALE' then 'HOMOSEXUAL'
	 WHEN T5.sex_partner_gender in ('MALE + FEMALE', 'FEMALE + MALE') then 'BISEXUAL'
	 ELSE 'UNKNOWN' end as sexual_orientation,
CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC' 
     WHEN UPPER(ethnicity) in ('NON HISPANIC', 'NOT HISPANIC OR LATINO') then 'NON-HISPANIC'
	 ELSE 'UNKNOWN' end as ethnicity,
CASE WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
     WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
     WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
     WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
     WHEN UPPER(race) in ('ASIAN', 'A', 'CHINESE', 'ASIAN INDIAN', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
     WHEN UPPER(race) in  ('NATIVE HAWAI', 'NATIVE HAWAII', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'PAC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
     WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race,
concat(T3.last_name, ', ', T3.first_name) as pcp_name,
T3.npi as pcp_npi
FROM emr_patient T1
JOIN covid19_report.surv_index_pats T2 on (T1.id = T2.patient_id)
LEFT JOIN emr_provider T3 on (T1.pcp_id = T3.id)
LEFT JOIN (SELECT patient_id, max(date) soc_date from emr_socialhistory where sex_partner_gender is not null group by patient_id) T4 on (T1.id = T4.patient_id)
LEFT JOIN emr_socialhistory T5 on (T1.id = T5.patient_id and T4.soc_date = T5.date and T5.sex_partner_gender is not null);

-- GATHER LAB RESULTS DATA
CREATE TABLE covid19_report.surv_lab_results AS 
SELECT DISTINCT T1.patient_id, T1.index_date, T3.test_name as test_type, 
T2.result_date::date,
T2.collection_date::date,
T2.result_string, 
CASE WHEN T4.name ilike '%pos%' then 'POSITIVE'
     WHEN T4.name ilike '%neg%' then 'NEGATIVE'
	 WHEN T4.name ilike '%ind%' then 'INDETERMINATE'
	 ELSE T4.name END as result_interpretation,
T2.specimen_source,
case when T5.last_name is not null and T5.last_name != '' then concat(T5.last_name, ', ', T5.first_name)
   else 'UNKNOWN, UNKNOWN'::text end as lab_provider_name,
T5.npi as lab_provider_npi,
CASE WHEN (T6.dept not ilike '%lab%' and T6.dept != 'UNKNOWN') or T6.dept ilike '%labor &%' or T6.dept ilike '%collaboration%' then T6.dept 
     WHEN T6.dept in ('UNKNOWN') or T6.dept is null then T5.dept
	 WHEN T5.dept is null then 'UNKNOWN'
     ELSE T5.dept END as pat_dept_loc_or_provider_dept
FROM covid19_report.surv_index_pats T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
LEFT JOIN emr_provider T5 ON (T5.id = T2.provider_id )
LEFT JOIN emr_provider T6 ON (T6.id = T2.facility_provider_id)
WHERE T2.date >= '01-01-2020'
AND T2.date >= T1.index_date - INTERVAL '14 days'
AND T2.date <= T1.index_date + INTERVAL '14 days'
AND T3.test_name in ('covid19_pcr','covid19_igg','covid19_igm','covid19_iga','covid19_ab_total','covid19_ag','wbc',
                     'hematocrit','platelet_count','lymphocytes', 'influenza', 'influenza_culture', 'rapid_flu',
					 'po2_art', 'po2_ven', 'd_dimer', 'crp', 'h_metapneumovirus', 'parainfluenza', 'adenovirus',
					 'rhino_entero_virus', 'coronavirus_non19', 'm_pneumoniae_igm', 'm_pneumoniae_pcr',
					 'c_pneumoniae_igm', 'c_pneumoniae_pcr', 'rsv', 'parapertussis');

-- GATHER LAB ORDER DATA
CREATE TABLE covid19_report.surv_lab_orders AS
SELECT DISTINCT T1.patient_id, T1.index_date, 
T2.date as lab_order_date, 
T3.test_name as order_type,
case when T5.last_name is not null and T5.last_name != '' then concat(T5.last_name, ', ', T5.first_name)
   else 'UNKNOWN, UNKNOWN'::text end as lab_order_provider_name,
T5.npi lab_order_provider_npi,
T5.dept as lab_order_provider_dept
FROM covid19_report.surv_index_pats T1
INNER JOIN emr_laborder T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T3.native_code = T2.procedure_code)
--LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
LEFT JOIN emr_provider T5 ON (T5.id = T2.provider_id )
--LEFT JOIN emr_provider T6 ON (T6.id = T2.facility_provider_id)
WHERE T2.date >= '01-01-2020'
AND T2.date >= T1.index_date - INTERVAL '14 days'
AND T2.date <= T1.index_date + INTERVAL '14 days'
AND T3.test_name in ('chestxray', 'chestct', 'covid19_igm_order', 'covid19_iga_order', 'covid19_igg_order', 'covid19_pcr_order', 'covid19_ab_total_order', 'conva_plasma', 'mechanical_vent');


-- ENCOUNTERS
CREATE TABLE covid19_report.surv_encounters_with_codes AS 
SELECT T3.*, T2.patient_id, T1.index_date, T2.date,
CASE WHEN dx_code_id ~ '^(icd10:icd10:R50.81|icd10:R50.9|icd10:R56.00)' then 1 else 0 end fever_yn,
CASE WHEN dx_code_id ~ '^(icd10:icd10:R50.81|icd10:R50.9|icd10:R56.00)' then dx_code_id else null end fever_dx,
CASE WHEN dx_code_id ~ '^(icd10:J12.8|icd10:J12.9|icd10:J18.8|icd10:J18.9)' then 1 else 0 end pneumonia_yn,
CASE WHEN dx_code_id ~ '^(icd10:J12.8|icd10:J12.9|icd10:J18.8|icd10:J18.9)' then dx_code_id else null end pneumonia_dx,
CASE WHEN dx_code_id ~ '^(icd10:J20.8|icd10:J20.9|J21.8|icd10:J40)' then 1 else 0 end bronchitis_yn,
CASE WHEN dx_code_id ~ '^(icd10:J20.8|icd10:J20.9|J21.8|icd10:J40)' then dx_code_id else null end bronchitis_dx,
CASE WHEN dx_code_id ~ '^(icd10:J22|icd10:J98.8)' then 1 else 0 end lri_yn,
CASE WHEN dx_code_id ~ '^(icd10:J22|icd10:J98.8)' then dx_code_id else null end lri_dx,
CASE WHEN dx_code_id ~ '^(icd10:J80)' then 1 else 0 end ards_yn,
CASE WHEN dx_code_id ~ '^(icd10:J80)' then dx_code_id else null end ards_dx,
CASE WHEN dx_code_id ~ '^(icd10:J00|icd10:J02.9|icd10:J04.0|icd10:J04.10|icd10:J04.11|icd10:J04.2|icd10:J05.0|icd10:J06.0|icd10:J06.9|icd10:J39.8|icd10:J39.9)' then 1 else 0 end uri_yn,
CASE WHEN dx_code_id ~ '^(icd10:J00|icd10:J02.9|icd10:J04.0|icd10:J04.10|icd10:J04.11|icd10:J04.2|icd10:J05.0|icd10:J06.0|icd10:J06.9|icd10:J39.8|icd10:J39.9)' then dx_code_id else null end uri_dx,
CASE WHEN dx_code_id ~ '^(icd10:R05)' then 1 else 0 end cough_yn,
CASE WHEN dx_code_id ~ '^(icd10:R05)' then dx_code_id else null end cough_dx,
CASE WHEN dx_code_id ~ '^(icd10:R06.02)' then 1 else 0 end short_of_breath_yn,
CASE WHEN dx_code_id ~ '^(icd10:R06.02)' then dx_code_id else null end short_of_breath_dx,
CASE WHEN dx_code_id ~ '^(icd10:B33.8|icd10:B34.8|icd10:B97.89)' then 1 else 0 end viral_inf_yn,
CASE WHEN dx_code_id ~ '^(icd10:B33.8|icd10:B34.8|icd10:B97.89)' then dx_code_id else null end viral_inf_dx,
CASE WHEN dx_code_id ~ '^(icd10:B34.2|icd10:B97.29|icd10:U07.1|icd10:Z20.828)' then 1 else 0 end covid19_yn,
CASE WHEN dx_code_id ~ '^(icd10:B34.2|icd10:B97.29|icd10:U07.1|icd10:Z20.828)' then dx_code_id else null end covid19_dx,
CASE WHEN dx_code_id ~ '^(icd10:R68.83)' then 1 else 0 end chills_yn,
CASE WHEN dx_code_id ~ '^(icd10:R68.83)' then dx_code_id else null end chills_dx,
CASE WHEN dx_code_id ~ '^(icd10:M79.10|icd10:M79.18)' or dx_code_id in ('icd10:M79.1') then 1 else 0 end myalgia_yn,
CASE WHEN dx_code_id ~ '^(icd10:M79.10|icd10:M79.18)' or dx_code_id in ('icd10:M79.1') then dx_code_id else null end myalgia_dx,
CASE WHEN dx_code_id ~ '^(icd10:R51)' then 1 else 0 end headache_yn,
CASE WHEN dx_code_id ~ '^(icd10:R51)' then dx_code_id else null end headache_dx,
CASE WHEN dx_code_id ~ '^(icd10:R07.0)' then 1 else 0 end sore_throat_yn,
CASE WHEN dx_code_id ~ '^(icd10:R07.0)' then dx_code_id else null end sore_throat_dx,
CASE WHEN dx_code_id ~ '^(icd10:R43.0|icd10:R43.1|icd10:R43.2|icd10:R43.8|icd10:R43.9)' then 1 else 0 end olf_taste_dis_yn,
CASE WHEN dx_code_id ~ '^(icd10:R43.0|icd10:R43.1|icd10:R43.2|icd10:R43.8|icd10:R43.9)' then dx_code_id else null end olf_taste_dis_dx,
CASE WHEN dx_code_id ~ '^(icd10:R06.89|icd10:R06.9|icd10:R07.1)' then 1 else 0 end diff_breathing_yn,
CASE WHEN dx_code_id ~ '^(icd10:R06.89|icd10:R06.9|icd10:R07.1)' then dx_code_id else null end diff_breathing_dx
FROM 
covid19_report.surv_index_pats T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date >= '01-01-2020'
AND T2.date >= T1.index_date - INTERVAL '14 days'
AND T2.date <= T1.index_date + INTERVAL '14 days'
AND (dx_code_id ~
'^(icd10:icd10:R50.81|icd10:R50.9|icd10:R56.00|icd10:J12.8|icd10:J12.9|icd10:J18.8|icd10:J18.9|icd10:J20.8|icd10:J20.9|J21.8|icd10:J40|icd10:J22|icd10:J98.8|icd10:J80|icd10:J00|icd10:J02.9|icd10:J04.0|icd10:J04.10|icd10:J04.11|icd10:J04.2|icd10:J05.0|icd10:J06.0|icd10:J06.9|icd10:J39.8|icd10:J39.9|icd10:R05|icd10:R06.02|icd10:B33.8|icd10:B34.8|icd10:B97.89|icd10:B34.2|icd10:B97.29|icd10:U07.1|icd10:Z20.828|icd10:R68.83|icd10:M79.10|icd10:M79.18|icd10:R51|icd10:R07.0|icd10:R43.0|icd10:R43.1|icd10:R43.2|icd10:R43.8|icd10:R43.9|icd10:R06.89|icd10:R06.9|icd10:R07.1)'
or dx_code_id in ('icd10:M79.1'));


CREATE TABLE covid19_report.surv_encounters_with_code_arrays as
select 
    patient_id,
	encounter_id,
    (select array_agg(a) from unnest(T1.fever_dx_array) a where a is not null) fever_dx_array,
	(select array_agg(a) from unnest(T1.pneumonia_dx_array) a where a is not null) pneumonia_dx_array,
	(select array_agg(a) from unnest(T1.bronchitis_dx_array) a where a is not null) bronchitis_dx_array,
	(select array_agg(a) from unnest(T1.lri_dx_array) a where a is not null) lri_dx_array,
	(select array_agg(a) from unnest(T1.ards_dx_array) a where a is not null) ards_dx_array,
	(select array_agg(a) from unnest(T1.uri_dx_array) a where a is not null) uri_dx_array,
    (select array_agg(a) from unnest(T1.cough_dx_array) a where a is not null) cough_dx_array,
	(select array_agg(a) from unnest(T1.short_of_breath_dx_array) a where a is not null) short_of_breath_dx_array,
	(select array_agg(a) from unnest(T1.viral_inf_dx_array) a where a is not null) viral_inf_dx_array,
	(select array_agg(a) from unnest(T1.covid19_dx_array) a where a is not null) covid19_dx_array,
	(select array_agg(a) from unnest(T1.chills_dx_array) a where a is not null) chills_dx_array,
	(select array_agg(a) from unnest(T1.myalgia_dx_array) a where a is not null) myalgia_dx_array,
	(select array_agg(a) from unnest(T1.headache_dx_array) a where a is not null) headache_dx_array,
	(select array_agg(a) from unnest(T1.sore_throat_dx_array) a where a is not null) sore_throat_dx_array,
	(select array_agg(a) from unnest(T1.olf_taste_dis_dx_array) a where a is not null) olf_taste_dis_dx_array,
	(select array_agg(a) from unnest(T1.diff_breathing_dx_array) a where a is not null) diff_breathing_dx_array
from (
    SELECT patient_id, encounter_id,
           array_agg(fever_dx) fever_dx_array,
		   array_agg(pneumonia_dx) pneumonia_dx_array,
		   array_agg(bronchitis_dx) bronchitis_dx_array,
		   array_agg(lri_dx) lri_dx_array,
		   array_agg(ards_dx) ards_dx_array,
		   array_agg(uri_dx) uri_dx_array,
           array_agg(cough_dx) cough_dx_array,
		   array_agg(short_of_breath_dx) short_of_breath_dx_array,
		   array_agg(viral_inf_dx) viral_inf_dx_array,
		   array_agg(covid19_dx) covid19_dx_array,
		   array_agg(chills_dx) chills_dx_array,
		   array_agg(myalgia_dx) myalgia_dx_array,
		   array_agg(headache_dx) headache_dx_array,
		   array_agg(sore_throat_dx) sore_throat_dx_array,
		   array_agg(olf_taste_dis_dx) olf_taste_dis_dx_array,
		   array_agg(diff_breathing_dx) diff_breathing_dx_array
    FROM covid19_report.surv_encounters_with_codes
    GROUP BY patient_id, encounter_id
	) T1;


CREATE TABLE covid19_report.surv_encounters_details as
SELECT T1.patient_id,
--T1.encounter_id,
T1.index_date,
case when last_name is not null and last_name != '' then concat(T4.last_name, ', ', T4.first_name) 
   else 'UNKNOWN, UNKNOWN'::text end as provider_name,
T4.npi as provider_npi,
T4.dept as provider_primary_dept,
T3.date as visit_date,
hosp_admit_dt,
hosp_dschrg_dt,
case when edd is not null or pregnant = true then 1 end pregnant,
raw_encounter_type as encounter_type,
case when raw_encounter_type ~* ('TELEVISIT|TELEHEALTH|TELEMEDICINE|TELEPHONE|VIDEOVISIT') then 1 else 0 end as telehealth_yn,
o2_sat,
site_name,
case when T5.name = 'enc:fever' then 1
    else max(fever_yn) end as fever_yn,
--max(fever_yn) fever_yn,
replace(array_to_string(fever_dx_array, ' | '), 'icd10:', '') as fever_dx_codes,
max(pneumonia_yn) pneumonia_yn,
replace(array_to_string(pneumonia_dx_array, ' | '), 'icd10:', '') as pneumonia_dx_codes,
max(bronchitis_yn) bronchitis_yn,
replace(array_to_string(bronchitis_dx_array, ' | '), 'icd10:', '') as bronchitis_dx_codes,
max(lri_yn) lri_yn,
replace(array_to_string(lri_dx_array, ' | '), 'icd10:', '') as lri_dx_codes,
max(ards_yn) ards_yn,
replace(array_to_string(ards_dx_array, ' | '), 'icd10:', '') as ards_dx_codes,
max(uri_yn) uri_yn,
replace(array_to_string(uri_dx_array, ' | '), 'icd10:', '') as uri_dx_codes,
max(cough_yn) cough_yn,
replace(array_to_string(cough_dx_array, ' | '), 'icd10:', '') as cough_dx_codes,
max(short_of_breath_yn) short_of_breath_yn,
replace(array_to_string(short_of_breath_dx_array, ' | '), 'icd10:', '') as short_of_breath_dx_codes,
max(viral_inf_yn) viral_inf_yn,
replace(array_to_string(viral_inf_dx_array, ' | '), 'icd10:', '') as viral_inf_dx_codes,
max(covid19_yn) covid19_yn,
replace(array_to_string(covid19_dx_array, ' | '), 'icd10:', '') as covid19_dx_codes,
max(chills_yn) chills_yn,
replace(array_to_string(chills_dx_array, ' | '), 'icd10:', '') as chills_dx_codes,
max(myalgia_yn) myalgia_yn,
replace(array_to_string(myalgia_dx_array, ' | '), 'icd10:', '') as myalgia_dx_codes,
max(headache_yn) headache_yn,
replace(array_to_string(headache_dx_array, ' | '), 'icd10:', '') as headache_dx_codes,
max(sore_throat_yn) sore_throat_yn,
replace(array_to_string(sore_throat_dx_array, ' | '), 'icd10:', '') as sore_throat_dx_codes,
max(olf_taste_dis_yn) olf_taste_dis_yn,
replace(array_to_string(olf_taste_dis_dx_array, ' | '), 'icd10:', '') as olf_taste_dis_dx_codes,
max(diff_breathing_yn) diff_breathing_yn,
replace(array_to_string(diff_breathing_dx_array, ' | '), 'icd10:', '') as diff_breathing_dx_codes
FROM covid19_report.surv_encounters_with_codes T1
LEFT JOIN covid19_report.surv_encounters_with_code_arrays T2 ON (T1.patient_id = T2.patient_id and T1.encounter_id = T2.encounter_id)
INNER JOIN emr_encounter T3 on (T1.patient_id = T3.patient_id and T1.encounter_id = T3.id)
LEFT JOIN emr_provider T4 ON (T4.id = T3.provider_id )
LEFT JOIN hef_event T5 ON (T5.patient_id = T1.patient_id and T5.object_id = T1.encounter_id and T5.name = 'enc:fever')
GROUP BY T1.patient_id, T1.index_date, T1.encounter_id, fever_dx_array, pneumonia_dx_codes, bronchitis_dx_codes, 
lri_dx_codes, ards_dx_codes, uri_dx_codes, cough_dx_codes, short_of_breath_dx_codes, covid19_dx_codes, viral_inf_dx_codes,chills_dx_codes,
myalgia_dx_codes, headache_dx_codes, sore_throat_dx_codes, olf_taste_dis_dx_codes, diff_breathing_dx_codes,
T4.last_name, T4.first_name, T4.npi, T4.dept, T3.date, hosp_admit_dt, hosp_dschrg_dt, raw_encounter_type, 
o2_sat, site_name, T5.name, edd, pregnant;


--MEDICATIONS

CREATE TABLE covid19_report.surv_medications as
SELECT DISTINCT T1.patient_id,
T1.index_date,
case when T3.last_name is not null and T3.last_name != '' then concat(T3.last_name, ', ', T3.first_name)
   else 'UNKNOWN, UNKNOWN'::text end as med_provider_name,
T3.npi as med_provider_npi,
T2.date as med_date,
CASE WHEN (T4.dept not ilike '%lab%' and T4.dept != 'UNKNOWN') or T4.dept ilike '%labor &%' or T4.dept ilike '%collaboration%' then T4.dept 
     WHEN T4.dept in ('UNKNOWN') or T4.dept is null then T3.dept
	 WHEN T3.dept is null then 'UNKNOWN'
     ELSE T3.dept END as pat_dept_loc_or_provider_dept,
T2.name as drug_name,
CASE 
     WHEN name ~* ('azithromycin|zithromax|zmax') then 'AZITHROMYCIN'
	 WHEN name ~* ('chloroquine|aralen') and name not ilike '%hydroxychloroquine%' then 'CHLOROQUINE'
	 WHEN name ~* ('hydroxychloroquine|plaquenil|quineprox') then 'HYDROXYCHLOROQUINE'
	 WHEN name ~* ('remdesivir') then 'REMDESIVIR' 
	 WHEN name ~* ('interferon alfa-n3|alferon') then 'INTERFERON ALFA-N3'
     WHEN name ~* ('interferon beta-1a|avonex|rebif') then 'INTERFERON BETA-1A' 
	 WHEN name ~* ('interferon beta-1b|betaseron|extavia') then 'INTERFERON BETA-1B'
	 WHEN name ~* ('interferon gamma-1b|actimmune') then 'INTERFERON GAMMA-1B'
	 WHEN name ~* ('interferon alfa-2a|roferon') then 'INTERFERON ALFA-2A'
	 WHEN name ~* ('interferon alfa-2b|intron a') and name not ilike '%PEGINTERFERON ALFA-2B%' then 'INTERFERON ALFA-2B'
	 WHEN name ~* ('peginterferon alfa-2b|pegintron|sylatron') then 'PEGINTERFERON ALFA-2B'
	 WHEN name ~* ('tocilizumab|actemra') then 'TOCILIZUMAB'
	 WHEN name ~* ('sarilumab|kevzara') then 'SARILUMAB'
	 WHEN name ~* ('anakinra|kineret') then 'ANAKINRA'
	 WHEN name ~* ('prednisone|rayos|sterapred|deltasone') or name ilike 'orasone%' then 'PREDNISONE'
	 WHEN name ~* ('prednisolone|flo-pred|millipred|orapred|pediapred|veripred|prelone|hydeltra|hydeltrasol|key-pred|cotolone|predicort|predalone|predacort|predate|predaject|pred-ject|medicort|pri-cortin|predcor|bubbli-pred|asmalpred') and name not ilike '%METHYLPREDNISOLONE%' then 'PREDNISOLONE'
	 WHEN name ~* ('methylprednisolone|medrol') then 'METHYLPREDNISOLONE'
	 WHEN name ~* ('dexamethasone|baycadron|decadron|dexpak|taperdex|zema-pak|zodex|zonacort') then 'DEXAMETHASONE'
	 WHEN name ~* ('hydrocortisone|cortef') then 'HYDROCORTISONE'
	 ELSE null END as drug_type
from covid19_report.surv_index_pats T1
INNER JOIN emr_prescription T2 on (T1.patient_id = T2.patient_id)
LEFT JOIN emr_provider T3 ON (T3.id = T2.provider_id )
LEFT JOIN emr_provider T4 ON (T4.id = T2.facility_provider_id)
WHERE 
(    T2.name ilike '%azithromycin%'
     or T2.name ilike '%zithromax%'
     or T2.name ilike '%zmax%'
     or T2.name ilike '%chloroquine%'
     or T2.name ilike '%aralen%'
     or T2.name ilike '%plaquenil%'
     or T2.name ilike '%quineprox%'
	 or T2.name ilike '%remdesivir%'
	 or T2.name ilike '%interferon alfa-n3%'
	 or T2.name ilike '%alferon%'
	 or T2.name ilike '%interferon beta-1a%'
	 or T2.name ilike '%avonex%'
	 or T2.name ilike '%rebif%'
	 or T2.name ilike '%interferon beta-1b%'
	 or T2.name ilike '%betaseron%'
	 or T2.name ilike '%extavia%'
	 or T2.name ilike '%interferon gamma-1b%'
	 or T2.name ilike '%actimmune%'
	 or T2.name ilike '%interferon alfa-2a%'
	 or T2.name ilike '%roferon%'
	 or T2.name ilike '%interferon alfa-2b%'
	 or T2.name ilike '%intron a%'
	 or T2.name ilike '%peginterferon alfa-2b%'
	 or T2.name ilike '%pegintron%'
	 or T2.name ilike '%sylatron%'	 
	 or T2.name ilike '%tocilizumab%'
	 or T2.name ilike '%actemra%'
	 or T2.name ilike '%sarilumab%'
	 or T2.name ilike '%kevzara%'
	 or T2.name ilike '%anakinra%'
	 or T2.name ilike '%kineret%'
	 or T2.name ilike '%prednisone%'
	 or T2.name ilike '%rayos%'
	 or T2.name ilike '%sterapred%'
	 or T2.name ilike '%deltasone%'
	 --starts with match
	 or T2.name ilike 'orasone %'
	 or T2.name ilike '%prednisolone%'
	 or T2.name ilike '%flo-pred%'
	 or T2.name ilike '%millipred%'
	 or T2.name ilike '%orapred%'
	 or T2.name ilike '%pediapred%'
	 or T2.name ilike '%veripred%'
	 or T2.name ilike '%prelone%'
	 or T2.name ilike '%hydeltra%'
	 or T2.name ilike '%hydeltrasol%'
	 or T2.name ilike '%key-pred%'
	 or T2.name ilike '%cotolone%'
	 or T2.name ilike '%predicort%'
	 or T2.name ilike '%predalone%'
	 or T2.name ilike '%predacort%'
	 or T2.name ilike '%predate%'
	 or T2.name ilike '%predaject%'
	 or T2.name ilike '%pred-ject%'
	 or T2.name ilike '%medicort%'
	 or T2.name ilike '%pri-cortin%'
	 or T2.name ilike '%predcor%'
	 or T2.name ilike '%bubbli-pred%'
	 or T2.name ilike '%asmalpred%'
	 or T2.name ilike '%methylprednisolone'
	 or T2.name ilike '%medrol%'
	 or T2.name ilike '%dexamethasone%'
	 or T2.name ilike '%baycadron%'
	 or T2.name ilike '%decadron%'
	 or T2.name ilike '%dexpak%'
	 or T2.name ilike '%taperdex%'
	 or T2.name ilike '%zema-pak%'
	 or T2.name ilike '%zodex%'
	 or T2.name ilike '%zonacort%'
	 or T2.name ilike '%hydrocortisone%'
	 or T2.name ilike '%cortef%'	 
	 )
AND T2.name not ilike '%PEGINTERFERON BETA-1A%'
AND T2.name not ilike '%PEGINTERFERON ALFA-2A%'
AND T2.name not ilike '%RIBAVIRIN%'
AND T2.name not ilike '%AZASITE%'
AND T2.name not ilike '%EYE DROPS%'
AND T2.name not ilike '%EAR DROPS%'
AND T2.name not ilike '%DROPS%OT%'
AND T2.name not ilike '%OINTMENT%'
AND T2.name not ilike '%TOPICAL%'
AND T2.name not ilike '%LOTION%'
AND T2.name not ilike '%CREAM%'
AND T2.name not ilike '%ENEMA%'
AND T2.name not ilike '%RECTAL%'
AND T2.name not ilike '%TOBRAMYCIN%'
AND T2.name not ilike '%OP SOLN%'
AND T2.name not ilike '%POWD%'
AND T2.name not ilike '%OT SUSP%'
AND T2.name not ilike '%OP SUSP%'
AND T2.name not ilike '%OP OINT%'
AND T2.name not ilike '%EX CREA%'
AND T2.name not ilike '%PR CREA%'
AND T2.name not ilike '%EX OINT%'
AND T2.name not ilike '%EX LOTN%'
AND T2.name not ilike '%PR LOTN%'
AND T2.name not ilike '%EX GEL%'
AND T2.name not ilike '%PR GEL%'
AND T2.name not ilike '%PR ENEM%'
AND T2.name not ilike '%EX FOAM%'
AND T2.name not ilike '%PR FOAM%'
AND T2.name not ilike '%EX SOLN%'
AND T2.name not ilike '%PR SUPP%'
AND T2.name not ilike '%RE CREA%'
AND T2.name not ilike '%OT SOLN%'
AND T2.name not ilike '%EX KIT%'
AND T2.name not ilike '%PR KIT%'
AND T2.name not ilike '%SUBCONJUNCTIVAL%'
AND T2.name not ilike '%TRANSDERMAL%'
AND T2.name not ilike '%INTRAVITREAL%'
AND T2.name not ilike '%GEL PACKET%'
AND T2.name not ilike '%OPHT%'
AND T2.name not ilike '%INTRAOCULAR%'
AND T2.name not ilike '%SUPPOSITORY%'
AND T2.name not ilike '%HYDROCORTISONE%RECT%'
--needs to end with TOP
AND T2.name not ilike '%HYDROCORTISONE%TOP'
AND T2.name not ilike '%HYDROCORTISONE%VAG%'
AND T2.name not ilike '%EARSOL%'
AND T2.date >= '01-01-2020'
AND T2.date >= T1.index_date - INTERVAL '14 days'
AND T2.date <= T1.index_date + INTERVAL '14 days';


-- DECIDED NOT TO USE DRUG SYNONYM TABLE FOR THIS SINCE IT SEEMS LIKE NOT ALL SITES ARE THE SAME RIGHT NOW
-- SAVING CODE IN CASE I WANT THIS FOR LATER.
-- CREATE TABLE covid19_report.surv_medications_new as
-- SELECT DISTINCT T1.patient_id,
-- T1.index_date,
-- case when T3.last_name is not null and T3.last_name != '' then concat(T3.last_name, ', ', T3.first_name)
   -- else 'UNKNOWN, UNKNOWN'::text end as med_provider_name,
-- T3.npi as med_provider_npi,
-- T2.date as med_date,
-- CASE WHEN (T4.dept not ilike '%lab%' and T4.dept != 'UNKNOWN') or T4.dept ilike '%labor &%' or T4.dept ilike '%collaboration%' then T4.dept 
     -- WHEN T4.dept in ('UNKNOWN') or T4.dept is null then T3.dept
	 -- WHEN T3.dept is null then 'UNKNOWN'
     -- ELSE T3.dept END as pat_dept_loc_or_provider_dept,
-- T2.name as drug_name,
-- upper(T5.generic_name) as drug_type
-- from covid19_report.surv_index_pats T1
-- INNER JOIN emr_prescription T2 on (T1.patient_id = T2.patient_id)
-- LEFT JOIN emr_provider T3 ON (T3.id = T2.provider_id )
-- LEFT JOIN emr_provider T4 ON (T4.id = T2.facility_provider_id)
-- INNER JOIN static_drugsynonym T5 on (T2.name ilike '%' || T5.other_name || '%')
-- WHERE (T5.generic_name ilike '%Azithromycin%' or
       -- T5.generic_name ilike 'Interferon alfa-2a%' or
	   -- T5.generic_name ilike 'Interferon alfa-2b%' or
	   -- T5.generic_name ilike 'Peginterferon alfa-2b%')
-- AND T2.name not ilike '%PEGINTERFERON ALFA-2A%'
-- AND T2.name not ilike '%RIBAVIRIN%'
-- AND T2.date >= '01-01-2020'
-- AND T2.date >= T1.index_date - INTERVAL '14 days'
-- AND T2.date <= T1.index_date + INTERVAL '14 days';

	 
	 

-- ASSOCIATED HEALTH CONDITIONS
CREATE TABLE covid19_report.surv_health_cond_dx_codes_1yr AS 
SELECT T1.patient_id, 
CASE WHEN dx_code_id ~ '^(icd10:J45)' then 1 else 0 end asthma_1yr,
CASE WHEN dx_code_id ~ '^(icd10:J45)' then dx_code_id else null end asthma_1yr_dx,
CASE WHEN dx_code_id in ('icd10:J40','icd10:J41.0','icd10:J41.1','icd10:J41.8','icd10:J42','icd10:J43.0','icd10:J43.1','icd10:J43.2','icd10:J43.8','icd10:J43.9',
                         'icd10:J44.0','icd10:J44.1','icd10:J44.9', 'icd10:J47.0','icd10:J47.1','icd10:J47.9','icd10:J67.0','icd10:J67.1','icd10:J67.2','icd10:J67.3',
						 'icd10:J67.4','icd10:J67.5','icd10:J67.6','icd10:J67.7','icd10:J67.8','icd10:J67.9') then 1 else 0 end chronic_lung_1yr,
CASE WHEN dx_code_id in ('icd10:J40','icd10:J41.0','icd10:J41.1','icd10:J41.8','icd10:J42','icd10:J43.0','icd10:J43.1','icd10:J43.2','icd10:J43.8','icd10:J43.9',
                         'icd10:J44.0','icd10:J44.1','icd10:J44.9', 'icd10:J47.0','icd10:J47.1','icd10:J47.9','icd10:J67.0','icd10:J67.1','icd10:J67.2','icd10:J67.3',
						 'icd10:J67.4','icd10:J67.5','icd10:J67.6','icd10:J67.7','icd10:J67.8','icd10:J67.9') then dx_code_id else null end chronic_lung_1yr_dx,
CASE WHEN dx_code_id in ('icd10:E66.01','icd10:E66.09','icd10:E66.1','icd10:E66.8','icd10:E66.9','icd10:K95.01','icd10:K95.09','icd10:K95.81','icd10:K95.89','icd10:O99.210','icd10:O99.211','icd10:O99.212',
                   'icd10:O99.213','icd10:O99.214','icd10:O99.215','icd10:O99.840','icd10:O99.841','icd10:O99.842','icd10:O99.843','icd10:O99.844','icd10:O99.845','icd10:Z68.30','icd10:Z68.31',
                   'icd10:Z68.32','icd10:Z68.33','icd10:Z68.34','icd10:Z68.35','icd10:Z68.36','icd10:Z68.37','icd10:Z68.38','icd10:Z68.39','icd10:Z68.41','icd10:Z68.42','icd10:Z68.43','icd10:Z68.44',
                   'icd10:Z68.45', 'icd10:278.03', 'icd10:E66.2') then 1 else 0 end obesity_1yr,
CASE WHEN dx_code_id in ('icd10:E66.01','icd10:E66.09','icd10:E66.1','icd10:E66.8','icd10:E66.9','icd10:K95.01','icd10:K95.09','icd10:K95.81','icd10:K95.89','icd10:O99.210','icd10:O99.211','icd10:O99.212',
                   'icd10:O99.213','icd10:O99.214','icd10:O99.215','icd10:O99.840','icd10:O99.841','icd10:O99.842','icd10:O99.843','icd10:O99.844','icd10:O99.845','icd10:Z68.30','icd10:Z68.31',
                   'icd10:Z68.32','icd10:Z68.33','icd10:Z68.34','icd10:Z68.35','icd10:Z68.36','icd10:Z68.37','icd10:Z68.38','icd10:Z68.39','icd10:Z68.41','icd10:Z68.42','icd10:Z68.43','icd10:Z68.44',
                   'icd10:Z68.45', 'icd10:278.03', 'icd10:E66.2') then dx_code_id else null end obesity_1yr_dx,
CASE WHEN dx_code_id in ('icd10:K70.0','icd10:K70.10','icd10:K70.11','icd10:K70.2','icd10:K70.30','icd10:K70.31','icd10:K70.40','icd10:K70.41','icd10:K70.9','icd10:K73.0','icd10:K73.1','icd10:K73.2','icd10:K73.8',
                   'icd10:K73.9','icd10:K74.0','icd10:K74.1','icd10:K74.2','icd10:K74.3','icd10:K74.4','icd10:K74.5','icd10:K74.60','icd10:K74.69','icd10:K75.4','icd10:K75.81','icd10:K76.0','icd10:K76.89',
                   'icd10:K76.9') then 1 else 0 end hepatic_disorder_1yr,
CASE WHEN dx_code_id in ('icd10:K70.0','icd10:K70.10','icd10:K70.11','icd10:K70.2','icd10:K70.30','icd10:K70.31','icd10:K70.40','icd10:K70.41','icd10:K70.9','icd10:K73.0','icd10:K73.1','icd10:K73.2','icd10:K73.8',
                   'icd10:K73.9','icd10:K74.0','icd10:K74.1','icd10:K74.2','icd10:K74.3','icd10:K74.4','icd10:K74.5','icd10:K74.60','icd10:K74.69','icd10:K75.4','icd10:K75.81','icd10:K76.0','icd10:K76.89',
                   'icd10:K76.9') then dx_code_id else null end hepatic_disorder_1yr_dx,
CASE WHEN dx_code_id in ('icd10:E09.21','icd10:E09.22','icd10:E09.29','icd10:M32.14','icd10:M32.15','icd10:M35.04','icd10:N00.0','icd10:N00.1','icd10:N00.2','icd10:N00.3','icd10:N00.4','icd10:N00.5','icd10:N00.6',
                   'icd10:N00.7','icd10:N00.8','icd10:N00.9','icd10:N01.0','icd10:N01.1','icd10:N01.2','icd10:N01.3','icd10:N01.4','icd10:N01.5','icd10:N01.6','icd10:N01.7','icd10:N01.8','icd10:N01.9',
                    'icd10:N02.0','icd10:N02.1','icd10:N02.2','icd10:N02.3','icd10:N02.4','icd10:N02.5','icd10:N02.6','icd10:N02.7','icd10:N02.8','icd10:N02.9','icd10:N03.0','icd10:N03.1','icd10:N03.2',
                    'icd10:N03.3','icd10:N03.4','icd10:N03.5','icd10:N03.6','icd10:N03.7','icd10:N03.8','icd10:N03.9','icd10:N04.0','icd10:N04.1','icd10:N04.2','icd10:N04.3','icd10:N04.4','icd10:N04.5',
                    'icd10:N04.6','icd10:N04.7','icd10:N04.8','icd10:N04.9','icd10:N05.0','icd10:N05.1','icd10:N05.2','icd10:N05.3','icd10:N05.4','icd10:N05.5','icd10:N05.6','icd10:N05.7','icd10:N05.8',
                    'icd10:N05.9','icd10:N07.0','icd10:N07.1','icd10:N07.2','icd10:N07.3','icd10:N07.4','icd10:N07.5','icd10:N07.6','icd10:N07.7','icd10:N07.8','icd10:N07.9','icd10:N08','icd10:N10',
                    'icd10:N11.0','icd10:N11.1','icd10:N11.8','icd10:N11.9','icd10:N12','icd10:N13.0','icd10:N13.1','icd10:N13.2','icd10:N13.30','icd10:N13.39','icd10:N13.4','icd10:N13.5','icd10:N13.6',
                    'icd10:N13.70','icd10:N13.71','icd10:N13.721','icd10:N13.722','icd10:N13.729','icd10:N13.731','icd10:N13.732','icd10:N13.739','icd10:N13.8','icd10:N13.9','icd10:N14.0','icd10:N14.1',
                    'icd10:N14.2','icd10:N14.3','icd10:N14.4','icd10:N15.0','icd10:N15.1','icd10:N15.8','icd10:N15.9','icd10:N16','icd10:N25.89','icd10:N25.9','icd10:N28.89','icd10:N28.9','icd10:N29',
                    'icd10:N18.1','icd10:N18.2','icd10:N18.3','icd10:N18.4','icd10:N18.5') then 1 else 0 end renal_disorder_1yr,
CASE WHEN dx_code_id in ('icd10:E09.21','icd10:E09.22','icd10:E09.29','icd10:M32.14','icd10:M32.15','icd10:M35.04','icd10:N00.0','icd10:N00.1','icd10:N00.2','icd10:N00.3','icd10:N00.4','icd10:N00.5','icd10:N00.6',
                   'icd10:N00.7','icd10:N00.8','icd10:N00.9','icd10:N01.0','icd10:N01.1','icd10:N01.2','icd10:N01.3','icd10:N01.4','icd10:N01.5','icd10:N01.6','icd10:N01.7','icd10:N01.8','icd10:N01.9',
                    'icd10:N02.0','icd10:N02.1','icd10:N02.2','icd10:N02.3','icd10:N02.4','icd10:N02.5','icd10:N02.6','icd10:N02.7','icd10:N02.8','icd10:N02.9','icd10:N03.0','icd10:N03.1','icd10:N03.2',
                    'icd10:N03.3','icd10:N03.4','icd10:N03.5','icd10:N03.6','icd10:N03.7','icd10:N03.8','icd10:N03.9','icd10:N04.0','icd10:N04.1','icd10:N04.2','icd10:N04.3','icd10:N04.4','icd10:N04.5',
                    'icd10:N04.6','icd10:N04.7','icd10:N04.8','icd10:N04.9','icd10:N05.0','icd10:N05.1','icd10:N05.2','icd10:N05.3','icd10:N05.4','icd10:N05.5','icd10:N05.6','icd10:N05.7','icd10:N05.8',
                    'icd10:N05.9','icd10:N07.0','icd10:N07.1','icd10:N07.2','icd10:N07.3','icd10:N07.4','icd10:N07.5','icd10:N07.6','icd10:N07.7','icd10:N07.8','icd10:N07.9','icd10:N08','icd10:N10',
                    'icd10:N11.0','icd10:N11.1','icd10:N11.8','icd10:N11.9','icd10:N12','icd10:N13.0','icd10:N13.1','icd10:N13.2','icd10:N13.30','icd10:N13.39','icd10:N13.4','icd10:N13.5','icd10:N13.6',
                    'icd10:N13.70','icd10:N13.71','icd10:N13.721','icd10:N13.722','icd10:N13.729','icd10:N13.731','icd10:N13.732','icd10:N13.739','icd10:N13.8','icd10:N13.9','icd10:N14.0','icd10:N14.1',
                    'icd10:N14.2','icd10:N14.3','icd10:N14.4','icd10:N15.0','icd10:N15.1','icd10:N15.8','icd10:N15.9','icd10:N16','icd10:N25.89','icd10:N25.9','icd10:N28.89','icd10:N28.9','icd10:N29',
                    'icd10:N18.1','icd10:N18.2','icd10:N18.3','icd10:N18.4','icd10:N18.5') then dx_code_id else null end renal_disorder_1yr_dx,
CASE WHEN dx_code_id in ('icd10:F17.200','icd10:F17.201','icd10:F17.210','icd10:F17.211','icd10:F17.290','icd10:F17.291','icd10:O99.330','icd10:O99.331','icd10:O99.332','icd10:O99.333','icd10:O99.334',
	               'icd10:O99.335','icd10:T65.221A','icd10:T65.222A','icd10:T65.223A','icd10:T65.224A','icd10:T65.291A','icd10:T65.292A','icd10:T65.293A','icd10:T65.294A','icd10:F17.203',
				   'icd10:F17.208','icd10:F17.209','icd10:F17.213','icd10:F17.218','icd10:F17.219','icd10:F17.291','icd10:F17.293','icd10:F17.298','icd10:F17.299','icd10:Z72.0') then 1 else 0 end smoking_1yr,
CASE WHEN dx_code_id in ('icd10:F17.200','icd10:F17.201','icd10:F17.210','icd10:F17.211','icd10:F17.290','icd10:F17.291','icd10:O99.330','icd10:O99.331','icd10:O99.332','icd10:O99.333','icd10:O99.334',
	               'icd10:O99.335','icd10:T65.221A','icd10:T65.222A','icd10:T65.223A','icd10:T65.224A','icd10:T65.291A','icd10:T65.292A','icd10:T65.293A','icd10:T65.294A','icd10:F17.203',
				   'icd10:F17.208','icd10:F17.209','icd10:F17.213','icd10:F17.218','icd10:F17.219','icd10:F17.291','icd10:F17.293','icd10:F17.298','icd10:F17.299','icd10:Z72.0') then dx_code_id else null end smoking_1yr_dx,
CASE WHEN dx_code_id in ('icd10:C88.0','icd10:C96.5','icd10:C96.6','icd10:D47.2','icd10:D80.0','icd10:D80.1','icd10:D80.2','icd10:D80.3','icd10:D80.4','icd10:D80.5','icd10:D80.6','icd10:D80.7',
                  'icd10:D80.8','icd10:D80.9','icd10:D81.0','icd10:D81.1','icd10:D81.2','icd10:D81.3','icd10:D81.4','icd10:D81.5','icd10:D81.6','icd10:D81.7','icd10:D81.810','icd10:D81.818',
				  'icd10:D81.819','icd10:D81.89','icd10:D81.9','icd10:D82.0','icd10:D82.1','icd10:D82.2','icd10:D82.3','icd10:D82.4','icd10:D82.8','icd10:D82.9','icd10:D83.0','icd10:D83.1',
				  'icd10:D83.2','icd10:D83.8','icd10:D83.9','icd10:D84.0','icd10:D84.1','icd10:D84.8','icd10:D84.9','icd10:D89.0','icd10:D89.1','icd10:D89.2','icd10:D89.3','icd10:D89.40',
				  'icd10:D89.41','icd10:D89.42','icd10:D89.43','icd10:D89.49','icd10:D89.810','icd10:D89.811','icd10:D89.812','icd10:D89.813','icd10:D89.82','icd10:D89.89','icd10:D89.9',
				  'icd10:D70.1','icd10:D70.2','icd10:D70.3','icd10:D70.4','icd10:D70.8','icd10:D70.9','icd10:D72.819','icd10:T86.91','icd10:Z94','icd10:Z94.0','icd10:Z94.1','icd10:Z94.2',
				  'icd10:Z94.3','icd10:Z94.4','icd10:Z94.5','icd10:Z94.6','icd10:Z94.7','icd10:Z94.8','icd10:Z94.81','icd10:Z94.82','icd10:Z94.83','icd10:Z94.84','icd10:Z94.89','icd10:Z94.9',
				  'icd10:B20','icd10:B97.35','icd10:Z21') then 1 else 0 end immune_disorder_1yr,
CASE WHEN dx_code_id in ('icd10:C88.0','icd10:C96.5','icd10:C96.6','icd10:D47.2','icd10:D80.0','icd10:D80.1','icd10:D80.2','icd10:D80.3','icd10:D80.4','icd10:D80.5','icd10:D80.6','icd10:D80.7',
                  'icd10:D80.8','icd10:D80.9','icd10:D81.0','icd10:D81.1','icd10:D81.2','icd10:D81.3','icd10:D81.4','icd10:D81.5','icd10:D81.6','icd10:D81.7','icd10:D81.810','icd10:D81.818',
				  'icd10:D81.819','icd10:D81.89','icd10:D81.9','icd10:D82.0','icd10:D82.1','icd10:D82.2','icd10:D82.3','icd10:D82.4','icd10:D82.8','icd10:D82.9','icd10:D83.0','icd10:D83.1',
				  'icd10:D83.2','icd10:D83.8','icd10:D83.9','icd10:D84.0','icd10:D84.1','icd10:D84.8','icd10:D84.9','icd10:D89.0','icd10:D89.1','icd10:D89.2','icd10:D89.3','icd10:D89.40',
				  'icd10:D89.41','icd10:D89.42','icd10:D89.43','icd10:D89.49','icd10:D89.810','icd10:D89.811','icd10:D89.812','icd10:D89.813','icd10:D89.82','icd10:D89.89','icd10:D89.9',
				  'icd10:D70.1','icd10:D70.2','icd10:D70.3','icd10:D70.4','icd10:D70.8','icd10:D70.9','icd10:D72.819','icd10:T86.91','icd10:Z94','icd10:Z94.0','icd10:Z94.1','icd10:Z94.2',
				  'icd10:Z94.3','icd10:Z94.4','icd10:Z94.5','icd10:Z94.6','icd10:Z94.7','icd10:Z94.8','icd10:Z94.81','icd10:Z94.82','icd10:Z94.83','icd10:Z94.84','icd10:Z94.89','icd10:Z94.9',
				  'icd10:B20','icd10:B97.35','icd10:Z21') then dx_code_id else null end immune_disorder_1yr_dx,
CASE WHEN  dx_code_id in ('icd10:R41.82') then 1 else 0 end alt_mental_status_1yr,
CASE WHEN  dx_code_id in ('icd10:R41.82') then dx_code_id else null end alt_mental_status_1yr_dx,
CASE WHEN  dx_code_id in ('icd10:N17.0', 'icd10:N17.1', 'icd10:N17.2', 'icd10:N17.8', 'icd10:N17.9', 'icd10:N18.6', 'icd10:N18.9', 'icd10:N19') then 1 else 0 end renal_failure_1yr,
CASE WHEN  dx_code_id in ('icd10:N17.0', 'icd10:N17.1', 'icd10:N17.2', 'icd10:N17.8', 'icd10:N17.9', 'icd10:N18.6', 'icd10:N18.9', 'icd10:N19') then dx_code_id else null end renal_failure_1yr_dx,
CASE WHEN dx_code_id ~ '^(icd10:C00|icd10:C01|icd10:C02|icd10:C03|icd10:C04|icd10:C05|icd10:C06|icd10:C07|icd10:C08|icd10:C09|icd10:C10|icd10:C11|icd10:C12|icd10:C13|icd10:C14|icd10:C15|icd10:C16|icd10:C17|icd10:C18|icd10:C19|icd10:C20|icd10:C21|icd10:C22|icd10:C23|icd10:C24|icd10:C25|icd10:C26|icd10:C30|icd10:C31|icd10:C32|icd10:C33|icd10:C34|icd10:C37|icd10:C38|icd10:C39|icd10:C40|icd10:C41|icd10:C43|icd10:C45|icd10:C46|icd10:C47|icd10:C48|icd10:C49|icd10:C50|icd10:C51|icd10:C52|icd10:C53|icd10:C54|icd10:C55|icd10:C56|icd10:C57|icd10:C58|icd10:C60|icd10:C61|icd10:C62|icd10:C63|icd10:C64|icd10:C65|icd10:C66|icd10:C67|icd10:C68|icd10:C69|icd10:C70|icd10:C71|icd10:C72|icd10:C73|icd10:C74|icd10:C75|icd10:C76|icd10:C77|icd10:C78|icd10:C79|icd10:C7A|icd10:C7B|icd10:C80|icd10:C81|icd10:C82|icd10:C83|icd10:C84|icd10:C85|icd10:C86|icd10:C88|icd10:C90|icd10:C91|icd10:C92|icd10:C93|icd10:C94|icd10:C95|icd10:C96|icd10:C97|icd10:D03|icd10:D45|icd10:O9A.1|icd10:R97.21|icd10:D37|icd10:D38|icd10:D39|icd10:D40|icd10:D41|icd10:D42|icd10:D43|icd10:D44|icd10:D47.0|icd10:D47.Z9|icd10:D48)' then 1 else 0 end cancer_1yr,
CASE WHEN dx_code_id ~ '^(icd10:C00|icd10:C01|icd10:C02|icd10:C03|icd10:C04|icd10:C05|icd10:C06|icd10:C07|icd10:C08|icd10:C09|icd10:C10|icd10:C11|icd10:C12|icd10:C13|icd10:C14|icd10:C15|icd10:C16|icd10:C17|icd10:C18|icd10:C19|icd10:C20|icd10:C21|icd10:C22|icd10:C23|icd10:C24|icd10:C25|icd10:C26|icd10:C30|icd10:C31|icd10:C32|icd10:C33|icd10:C34|icd10:C37|icd10:C38|icd10:C39|icd10:C40|icd10:C41|icd10:C43|icd10:C45|icd10:C46|icd10:C47|icd10:C48|icd10:C49|icd10:C50|icd10:C51|icd10:C52|icd10:C53|icd10:C54|icd10:C55|icd10:C56|icd10:C57|icd10:C58|icd10:C60|icd10:C61|icd10:C62|icd10:C63|icd10:C64|icd10:C65|icd10:C66|icd10:C67|icd10:C68|icd10:C69|icd10:C70|icd10:C71|icd10:C72|icd10:C73|icd10:C74|icd10:C75|icd10:C76|icd10:C77|icd10:C78|icd10:C79|icd10:C7A|icd10:C7B|icd10:C80|icd10:C81|icd10:C82|icd10:C83|icd10:C84|icd10:C85|icd10:C86|icd10:C88|icd10:C90|icd10:C91|icd10:C92|icd10:C93|icd10:C94|icd10:C95|icd10:C96|icd10:C97|icd10:D03|icd10:D45|icd10:O9A.1|icd10:R97.21|icd10:D37|icd10:D38|icd10:D39|icd10:D40|icd10:D41|icd10:D42|icd10:D43|icd10:D44|icd10:D47.0|icd10:D47.Z9|icd10:D48)' then dx_code_id else null end cancer_1yr_dx,
CASE WHEN dx_code_id ~ '^(icd10:E10|icd10:E11|icd10:E13)' then 1 else 0 end diabetes_1yr,
CASE WHEN dx_code_id ~ '^(icd10:E10|icd10:E11|icd10:E13)' then dx_code_id else null end diabetes_1yr_dx,
CASE WHEN dx_code_id in ('icd10:I20.0','icd10:I20.1','icd10:I20.8','icd10:I20.9','icd10:I21.01','icd10:I21.02','icd10:I21.09','icd10:I21.11','icd10:I21.19','icd10:I21.21','icd10:I21.29','icd10:I21.3','icd10:I21.4','icd10:I21.9','icd10:I21.A1','icd10:I21.A9','icd10:I22.0','icd10:I22.1','icd10:I22.2','icd10:I22.8','icd10:I22.9','icd10:I24.0','icd10:I24.1','icd10:I24.8','icd10:I24.9','icd10:I25.10','icd10:I25.110','icd10:I25.111','icd10:I25.118','icd10:I25.119','icd10:I25.2','icd10:I25.3','icd10:I25.41','icd10:I25.42','icd10:I25.5','icd10:I25.6','icd10:I25.700','icd10:I25.701','icd10:I25.708','icd10:I25.709','icd10:I25.710','icd10:I25.711','icd10:I25.718','icd10:I25.719','icd10:I25.720','icd10:I25.721','icd10:I25.728','icd10:I25.729','icd10:I25.730','icd10:I25.731','icd10:I25.738','icd10:I25.739','icd10:I25.750','icd10:I25.751','icd10:I25.758','icd10:I25.759','icd10:I25.760','icd10:I25.761','icd10:I25.768','icd10:I25.769','icd10:I25.790','icd10:I25.791','icd10:I25.798','icd10:I25.799','icd10:I25.810','icd10:I25.811','icd10:I25.812','icd10:I25.82','icd10:I25.83','icd10:I25.84','icd10:I25.89','icd10:I25.9','icd10:I23.0','icd10:I23.1','icd10:I23.2','icd10:I23.3','icd10:I23.4','icd10:I23.5','icd10:I23.6','icd10:I23.7','icd10:I23.8','icd10:I25.10','icd10:I30.0','icd10:I30.1','icd10:I30.8','icd10:I30.9','icd10:I31.0','icd10:I31.1','icd10:I31.2','icd10:I31.3','icd10:I31.4','icd10:I31.8','icd10:I31.9','icd10:I32','icd10:I33.0','icd10:I33.9','icd10:I34.0','icd10:I34.1','icd10:I34.2','icd10:I34.8','icd10:I34.9','icd10:I35.0','icd10:I35.1','icd10:I35.2','icd10:I35.8','icd10:I35.9','icd10:I36.0','icd10:I36.1','icd10:I36.2','icd10:I36.8','icd10:I36.9','icd10:I37.0','icd10:I37.1','icd10:I37.2','icd10:I37.8','icd10:I37.9','icd10:I38','icd10:I39','icd10:I40.0','icd10:I40.1','icd10:I40.8','icd10:I40.9','icd10:I41','icd10:I42.0','icd10:I42.1','icd10:I42.2','icd10:I42.3','icd10:I42.4','icd10:I42.5','icd10:I42.6','icd10:I42.7','icd10:I42.8','icd10:I42.9','icd10:I43','icd10:I44.0','icd10:I44.1','icd10:I44.2','icd10:I44.30','icd10:I44.39','icd10:I44.4','icd10:I44.5','icd10:I44.60','icd10:I44.69','icd10:I44.7','icd10:I45.0','icd10:I45.10','icd10:I45.19','icd10:I45.2','icd10:I45.3','icd10:I45.4','icd10:I45.5','icd10:I45.6','icd10:I45.81','icd10:I45.89','icd10:I45.9','icd10:I46.2','icd10:I46.8','icd10:I46.9','icd10:I47.0','icd10:I47.1','icd10:I47.2','icd10:I47.9','icd10:I48.0','icd10:I48.1','icd10:I48.2','icd10:I48.3','icd10:I48.4','icd10:I48.91','icd10:I48.92','icd10:I49.01','icd10:I49.02','icd10:I49.1','icd10:I49.2','icd10:I49.3','icd10:I49.40','icd10:I49.49','icd10:I49.5','icd10:I49.8','icd10:I49.9','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9','icd10:I51.0','icd10:I51.1','icd10:I51.2','icd10:I51.3','icd10:I51.4','icd10:I51.5','icd10:I51.7','icd10:I51.81','icd10:I51.89','icd10:I51.9','icd10:I52','icd10:I97.0','icd10:I97.110','icd10:I97.111','icd10:I97.120','icd10:I97.121','icd10:I97.130','icd10:I97.131','icd10:I97.190','icd10:I97.191','icd10:M32.11','icd10:M32.12','icd10:R00.1','icd10:I26.01','icd10:I26.01','icd10:I26.02','icd10:I26.09','icd10:I26.90','icd10:I26.92','icd10:I26.99','icd10:I27.0','icd10:I27.1','icd10:I27.20','icd10:I27.21','icd10:I27.22','icd10:I27.23','icd10:I27.24','icd10:I27.29','icd10:I27.81','icd10:I27.82','icd10:I27.89','icd10:I27.9','icd10:I28.0','icd10:I28.1','icd10:I28.8','icd10:I28.9','icd10:T80.0XXA','icd10:T81.718A','icd10:T81.72XA','icd10:T82.817A','icd10:T82.818A','icd10:J84','icd10:J84.0','icd10:J84.01','icd10:J84.02','icd10:J84.03','icd10:J84.09','icd10:J84.1','icd10:J84.10','icd10:J84.11','icd10:J84.111','icd10:J84.112','icd10:J84.113','icd10:J84.114','icd10:J84.115','icd10:J84.116','icd10:J84.117','icd10:J84.17','icd10:J84.2','icd10:J84.8','icd10:J84.81','icd10:J84.82','icd10:J84.83','icd10:J84.84','icd10:J84.841','icd10:J84.842','icd10:J84.843','icd10:J84.848','icd10:J84.89','icd10:J84.9','icd10:J61','icd10:J68.4','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9', 'icd10:I05.0','icd10:I05.1','icd10:I05.2','icd10:I05.8','icd10:I05.9','icd10:I06.0','icd10:I06.1','icd10:I06.2','icd10:I06.8','icd10:I06.9','icd10:I07.0','icd10:I07.1','icd10:I07.2','icd10:I07.8','icd10:I07.9','icd10:I08.0','icd10:I08.1','icd10:I08.2','icd10:I08.3','icd10:I08.8','icd10:I08.9','icd10:I09.0','icd10:I09.1','icd10:I09.2','icd10:I09.81','icd10:I09.89','icd10:I09.9','icd10:Q24.2','icd10:Q24.3','icd10:Q24.4','icd10:Q24.5','icd10:Q24.6','icd10:Q24.8','icd10:Q24.9') then 1 else 0 end cardiovascular_disease_1yr,
CASE WHEN dx_code_id in ('icd10:I20.0','icd10:I20.1','icd10:I20.8','icd10:I20.9','icd10:I21.01','icd10:I21.02','icd10:I21.09','icd10:I21.11','icd10:I21.19','icd10:I21.21','icd10:I21.29','icd10:I21.3','icd10:I21.4','icd10:I21.9','icd10:I21.A1','icd10:I21.A9','icd10:I22.0','icd10:I22.1','icd10:I22.2','icd10:I22.8','icd10:I22.9','icd10:I24.0','icd10:I24.1','icd10:I24.8','icd10:I24.9','icd10:I25.10','icd10:I25.110','icd10:I25.111','icd10:I25.118','icd10:I25.119','icd10:I25.2','icd10:I25.3','icd10:I25.41','icd10:I25.42','icd10:I25.5','icd10:I25.6','icd10:I25.700','icd10:I25.701','icd10:I25.708','icd10:I25.709','icd10:I25.710','icd10:I25.711','icd10:I25.718','icd10:I25.719','icd10:I25.720','icd10:I25.721','icd10:I25.728','icd10:I25.729','icd10:I25.730','icd10:I25.731','icd10:I25.738','icd10:I25.739','icd10:I25.750','icd10:I25.751','icd10:I25.758','icd10:I25.759','icd10:I25.760','icd10:I25.761','icd10:I25.768','icd10:I25.769','icd10:I25.790','icd10:I25.791','icd10:I25.798','icd10:I25.799','icd10:I25.810','icd10:I25.811','icd10:I25.812','icd10:I25.82','icd10:I25.83','icd10:I25.84','icd10:I25.89','icd10:I25.9','icd10:I23.0','icd10:I23.1','icd10:I23.2','icd10:I23.3','icd10:I23.4','icd10:I23.5','icd10:I23.6','icd10:I23.7','icd10:I23.8','icd10:I25.10','icd10:I30.0','icd10:I30.1','icd10:I30.8','icd10:I30.9','icd10:I31.0','icd10:I31.1','icd10:I31.2','icd10:I31.3','icd10:I31.4','icd10:I31.8','icd10:I31.9','icd10:I32','icd10:I33.0','icd10:I33.9','icd10:I34.0','icd10:I34.1','icd10:I34.2','icd10:I34.8','icd10:I34.9','icd10:I35.0','icd10:I35.1','icd10:I35.2','icd10:I35.8','icd10:I35.9','icd10:I36.0','icd10:I36.1','icd10:I36.2','icd10:I36.8','icd10:I36.9','icd10:I37.0','icd10:I37.1','icd10:I37.2','icd10:I37.8','icd10:I37.9','icd10:I38','icd10:I39','icd10:I40.0','icd10:I40.1','icd10:I40.8','icd10:I40.9','icd10:I41','icd10:I42.0','icd10:I42.1','icd10:I42.2','icd10:I42.3','icd10:I42.4','icd10:I42.5','icd10:I42.6','icd10:I42.7','icd10:I42.8','icd10:I42.9','icd10:I43','icd10:I44.0','icd10:I44.1','icd10:I44.2','icd10:I44.30','icd10:I44.39','icd10:I44.4','icd10:I44.5','icd10:I44.60','icd10:I44.69','icd10:I44.7','icd10:I45.0','icd10:I45.10','icd10:I45.19','icd10:I45.2','icd10:I45.3','icd10:I45.4','icd10:I45.5','icd10:I45.6','icd10:I45.81','icd10:I45.89','icd10:I45.9','icd10:I46.2','icd10:I46.8','icd10:I46.9','icd10:I47.0','icd10:I47.1','icd10:I47.2','icd10:I47.9','icd10:I48.0','icd10:I48.1','icd10:I48.2','icd10:I48.3','icd10:I48.4','icd10:I48.91','icd10:I48.92','icd10:I49.01','icd10:I49.02','icd10:I49.1','icd10:I49.2','icd10:I49.3','icd10:I49.40','icd10:I49.49','icd10:I49.5','icd10:I49.8','icd10:I49.9','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9','icd10:I51.0','icd10:I51.1','icd10:I51.2','icd10:I51.3','icd10:I51.4','icd10:I51.5','icd10:I51.7','icd10:I51.81','icd10:I51.89','icd10:I51.9','icd10:I52','icd10:I97.0','icd10:I97.110','icd10:I97.111','icd10:I97.120','icd10:I97.121','icd10:I97.130','icd10:I97.131','icd10:I97.190','icd10:I97.191','icd10:M32.11','icd10:M32.12','icd10:R00.1','icd10:I26.01','icd10:I26.01','icd10:I26.02','icd10:I26.09','icd10:I26.90','icd10:I26.92','icd10:I26.99','icd10:I27.0','icd10:I27.1','icd10:I27.20','icd10:I27.21','icd10:I27.22','icd10:I27.23','icd10:I27.24','icd10:I27.29','icd10:I27.81','icd10:I27.82','icd10:I27.89','icd10:I27.9','icd10:I28.0','icd10:I28.1','icd10:I28.8','icd10:I28.9','icd10:T80.0XXA','icd10:T81.718A','icd10:T81.72XA','icd10:T82.817A','icd10:T82.818A','icd10:J84','icd10:J84.0','icd10:J84.01','icd10:J84.02','icd10:J84.03','icd10:J84.09','icd10:J84.1','icd10:J84.10','icd10:J84.11','icd10:J84.111','icd10:J84.112','icd10:J84.113','icd10:J84.114','icd10:J84.115','icd10:J84.116','icd10:J84.117','icd10:J84.17','icd10:J84.2','icd10:J84.8','icd10:J84.81','icd10:J84.82','icd10:J84.83','icd10:J84.84','icd10:J84.841','icd10:J84.842','icd10:J84.843','icd10:J84.848','icd10:J84.89','icd10:J84.9','icd10:J61','icd10:J68.4','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9', 'icd10:I05.0','icd10:I05.1','icd10:I05.2','icd10:I05.8','icd10:I05.9','icd10:I06.0','icd10:I06.1','icd10:I06.2','icd10:I06.8','icd10:I06.9','icd10:I07.0','icd10:I07.1','icd10:I07.2','icd10:I07.8','icd10:I07.9','icd10:I08.0','icd10:I08.1','icd10:I08.2','icd10:I08.3','icd10:I08.8','icd10:I08.9','icd10:I09.0','icd10:I09.1','icd10:I09.2','icd10:I09.81','icd10:I09.89','icd10:I09.9','icd10:Q24.2','icd10:Q24.3','icd10:Q24.4','icd10:Q24.5','icd10:Q24.6','icd10:Q24.8','icd10:Q24.9') then dx_code_id else null end cardiovascular_disease_1yr_dx
FROM 
covid19_report.surv_index_pats T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date >= T1.index_date - INTERVAL '1 year'
AND T2.date <= T1.index_date 
AND (
	(dx_code_id ~'^(icd10:J45)')
or
	--chronic lung
	dx_code_id in ('icd10:J40','icd10:J41.0','icd10:J41.1','icd10:J41.8','icd10:J42','icd10:J43.0','icd10:J43.1','icd10:J43.2','icd10:J43.8','icd10:J43.9','icd10:J44.0','icd10:J44.1','icd10:J44.9',
                   'icd10:J47.0','icd10:J47.1','icd10:J47.9','icd10:J67.0','icd10:J67.1','icd10:J67.2','icd10:J67.3','icd10:J67.4','icd10:J67.5','icd10:J67.6','icd10:J67.7','icd10:J67.8','icd10:J67.9')
or 
    --obesity
	dx_code_id in ('icd10:E66.01','icd10:E66.09','icd10:E66.1','icd10:E66.8','icd10:E66.9','icd10:K95.01','icd10:K95.09','icd10:K95.81','icd10:K95.89','icd10:O99.210','icd10:O99.211','icd10:O99.212',
                   'icd10:O99.213','icd10:O99.214','icd10:O99.215','icd10:O99.840','icd10:O99.841','icd10:O99.842','icd10:O99.843','icd10:O99.844','icd10:O99.845','icd10:Z68.30','icd10:Z68.31',
                   'icd10:Z68.32','icd10:Z68.33','icd10:Z68.34','icd10:Z68.35','icd10:Z68.36','icd10:Z68.37','icd10:Z68.38','icd10:Z68.39','icd10:Z68.41','icd10:Z68.42','icd10:Z68.43','icd10:Z68.44',
                   'icd10:Z68.45', 'icd10:278.03', 'icd10:E66.2')
or
	--hepatic disorders
	dx_code_id in ('icd10:K70.0','icd10:K70.10','icd10:K70.11','icd10:K70.2','icd10:K70.30','icd10:K70.31','icd10:K70.40','icd10:K70.41','icd10:K70.9','icd10:K73.0','icd10:K73.1','icd10:K73.2','icd10:K73.8',
                   'icd10:K73.9','icd10:K74.0','icd10:K74.1','icd10:K74.2','icd10:K74.3','icd10:K74.4','icd10:K74.5','icd10:K74.60','icd10:K74.69','icd10:K75.4','icd10:K75.81','icd10:K76.0','icd10:K76.89',
                   'icd10:K76.9')
or
    --renal disorders
	dx_code_id in ('icd10:E09.21','icd10:E09.22','icd10:E09.29','icd10:M32.14','icd10:M32.15','icd10:M35.04','icd10:N00.0','icd10:N00.1','icd10:N00.2','icd10:N00.3','icd10:N00.4','icd10:N00.5','icd10:N00.6',
                   'icd10:N00.7','icd10:N00.8','icd10:N00.9','icd10:N01.0','icd10:N01.1','icd10:N01.2','icd10:N01.3','icd10:N01.4','icd10:N01.5','icd10:N01.6','icd10:N01.7','icd10:N01.8','icd10:N01.9',
                    'icd10:N02.0','icd10:N02.1','icd10:N02.2','icd10:N02.3','icd10:N02.4','icd10:N02.5','icd10:N02.6','icd10:N02.7','icd10:N02.8','icd10:N02.9','icd10:N03.0','icd10:N03.1','icd10:N03.2',
                    'icd10:N03.3','icd10:N03.4','icd10:N03.5','icd10:N03.6','icd10:N03.7','icd10:N03.8','icd10:N03.9','icd10:N04.0','icd10:N04.1','icd10:N04.2','icd10:N04.3','icd10:N04.4','icd10:N04.5',
                    'icd10:N04.6','icd10:N04.7','icd10:N04.8','icd10:N04.9','icd10:N05.0','icd10:N05.1','icd10:N05.2','icd10:N05.3','icd10:N05.4','icd10:N05.5','icd10:N05.6','icd10:N05.7','icd10:N05.8',
                    'icd10:N05.9','icd10:N07.0','icd10:N07.1','icd10:N07.2','icd10:N07.3','icd10:N07.4','icd10:N07.5','icd10:N07.6','icd10:N07.7','icd10:N07.8','icd10:N07.9','icd10:N08','icd10:N10',
                    'icd10:N11.0','icd10:N11.1','icd10:N11.8','icd10:N11.9','icd10:N12','icd10:N13.0','icd10:N13.1','icd10:N13.2','icd10:N13.30','icd10:N13.39','icd10:N13.4','icd10:N13.5','icd10:N13.6',
                    'icd10:N13.70','icd10:N13.71','icd10:N13.721','icd10:N13.722','icd10:N13.729','icd10:N13.731','icd10:N13.732','icd10:N13.739','icd10:N13.8','icd10:N13.9','icd10:N14.0','icd10:N14.1',
                    'icd10:N14.2','icd10:N14.3','icd10:N14.4','icd10:N15.0','icd10:N15.1','icd10:N15.8','icd10:N15.9','icd10:N16','icd10:N25.89','icd10:N25.9','icd10:N28.89','icd10:N28.9','icd10:N29',
                    'icd10:N18.1','icd10:N18.2','icd10:N18.3','icd10:N18.4','icd10:N18.5')
or 
    --smoking
	dx_code_id in ('icd10:F17.200','icd10:F17.201','icd10:F17.210','icd10:F17.211','icd10:F17.290','icd10:F17.291','icd10:O99.330','icd10:O99.331','icd10:O99.332','icd10:O99.333','icd10:O99.334',
	               'icd10:O99.335','icd10:T65.221A','icd10:T65.222A','icd10:T65.223A','icd10:T65.224A','icd10:T65.291A','icd10:T65.292A','icd10:T65.293A','icd10:T65.294A','icd10:F17.203',
				   'icd10:F17.208','icd10:F17.209','icd10:F17.213','icd10:F17.218','icd10:F17.219','icd10:F17.291','icd10:F17.293','icd10:F17.298','icd10:F17.299','icd10:Z72.0')
or
   --immune disorders
   dx_code_id in ('icd10:C88.0','icd10:C96.5','icd10:C96.6','icd10:D47.2','icd10:D80.0','icd10:D80.1','icd10:D80.2','icd10:D80.3','icd10:D80.4','icd10:D80.5','icd10:D80.6','icd10:D80.7',
                  'icd10:D80.8','icd10:D80.9','icd10:D81.0','icd10:D81.1','icd10:D81.2','icd10:D81.3','icd10:D81.4','icd10:D81.5','icd10:D81.6','icd10:D81.7','icd10:D81.810','icd10:D81.818',
				  'icd10:D81.819','icd10:D81.89','icd10:D81.9','icd10:D82.0','icd10:D82.1','icd10:D82.2','icd10:D82.3','icd10:D82.4','icd10:D82.8','icd10:D82.9','icd10:D83.0','icd10:D83.1',
				  'icd10:D83.2','icd10:D83.8','icd10:D83.9','icd10:D84.0','icd10:D84.1','icd10:D84.8','icd10:D84.9','icd10:D89.0','icd10:D89.1','icd10:D89.2','icd10:D89.3','icd10:D89.40',
				  'icd10:D89.41','icd10:D89.42','icd10:D89.43','icd10:D89.49','icd10:D89.810','icd10:D89.811','icd10:D89.812','icd10:D89.813','icd10:D89.82','icd10:D89.89','icd10:D89.9',
				  'icd10:D70.1','icd10:D70.2','icd10:D70.3','icd10:D70.4','icd10:D70.8','icd10:D70.9','icd10:D72.819','icd10:T86.91','icd10:Z94','icd10:Z94.0','icd10:Z94.1','icd10:Z94.2',
				  'icd10:Z94.3','icd10:Z94.4','icd10:Z94.5','icd10:Z94.6','icd10:Z94.7','icd10:Z94.8','icd10:Z94.81','icd10:Z94.82','icd10:Z94.83','icd10:Z94.84','icd10:Z94.89','icd10:Z94.9',
				  'icd10:B20','icd10:B97.35','icd10:Z21')
or
   --altered mental status
   dx_code_id in ('icd10:R41.82')
or
   --renal failure
   dx_code_id in ('icd10:N17.0', 'icd10:N17.1', 'icd10:N17.2', 'icd10:N17.8', 'icd10:N17.9', 'icd10:N18.6', 'icd10:N18.9', 'icd10:N19')
or
   --cancer
   dx_code_id ~ '^(icd10:C00|icd10:C01|icd10:C02|icd10:C03|icd10:C04|icd10:C05|icd10:C06|icd10:C07|icd10:C08|icd10:C09|icd10:C10|icd10:C11|icd10:C12|icd10:C13|icd10:C14|icd10:C15|icd10:C16|icd10:C17|icd10:C18|icd10:C19|icd10:C20|icd10:C21|icd10:C22|icd10:C23|icd10:C24|icd10:C25|icd10:C26|icd10:C30|icd10:C31|icd10:C32|icd10:C33|icd10:C34|icd10:C37|icd10:C38|icd10:C39|icd10:C40|icd10:C41|icd10:C43|icd10:C45|icd10:C46|icd10:C47|icd10:C48|icd10:C49|icd10:C50|icd10:C51|icd10:C52|icd10:C53|icd10:C54|icd10:C55|icd10:C56|icd10:C57|icd10:C58|icd10:C60|icd10:C61|icd10:C62|icd10:C63|icd10:C64|icd10:C65|icd10:C66|icd10:C67|icd10:C68|icd10:C69|icd10:C70|icd10:C71|icd10:C72|icd10:C73|icd10:C74|icd10:C75|icd10:C76|icd10:C77|icd10:C78|icd10:C79|icd10:C7A|icd10:C7B|icd10:C80|icd10:C81|icd10:C82|icd10:C83|icd10:C84|icd10:C85|icd10:C86|icd10:C88|icd10:C90|icd10:C91|icd10:C92|icd10:C93|icd10:C94|icd10:C95|icd10:C96|icd10:C97|icd10:D03|icd10:D45|icd10:O9A.1|icd10:R97.21|icd10:D37|icd10:D38|icd10:D39|icd10:D40|icd10:D41|icd10:D42|icd10:D43|icd10:D44|icd10:D47.0|icd10:D47.Z9|icd10:D48)'
or
   --diabetes
   dx_code_id ~ '^(icd10:E10|icd10:E11|icd10:E13)'
   
or --cardiovascular disease
   dx_code_id in ('icd10:I20.0','icd10:I20.1','icd10:I20.8','icd10:I20.9','icd10:I21.01','icd10:I21.02','icd10:I21.09','icd10:I21.11','icd10:I21.19','icd10:I21.21','icd10:I21.29','icd10:I21.3','icd10:I21.4','icd10:I21.9','icd10:I21.A1','icd10:I21.A9','icd10:I22.0','icd10:I22.1','icd10:I22.2','icd10:I22.8','icd10:I22.9','icd10:I24.0','icd10:I24.1','icd10:I24.8','icd10:I24.9','icd10:I25.10','icd10:I25.110','icd10:I25.111','icd10:I25.118','icd10:I25.119','icd10:I25.2','icd10:I25.3','icd10:I25.41','icd10:I25.42','icd10:I25.5','icd10:I25.6','icd10:I25.700','icd10:I25.701','icd10:I25.708','icd10:I25.709','icd10:I25.710','icd10:I25.711','icd10:I25.718','icd10:I25.719','icd10:I25.720','icd10:I25.721','icd10:I25.728','icd10:I25.729','icd10:I25.730','icd10:I25.731','icd10:I25.738','icd10:I25.739','icd10:I25.750','icd10:I25.751','icd10:I25.758','icd10:I25.759','icd10:I25.760','icd10:I25.761','icd10:I25.768','icd10:I25.769','icd10:I25.790','icd10:I25.791','icd10:I25.798','icd10:I25.799','icd10:I25.810','icd10:I25.811','icd10:I25.812','icd10:I25.82','icd10:I25.83','icd10:I25.84','icd10:I25.89','icd10:I25.9','icd10:I23.0','icd10:I23.1','icd10:I23.2','icd10:I23.3','icd10:I23.4','icd10:I23.5','icd10:I23.6','icd10:I23.7','icd10:I23.8','icd10:I25.10','icd10:I30.0','icd10:I30.1','icd10:I30.8','icd10:I30.9','icd10:I31.0','icd10:I31.1','icd10:I31.2','icd10:I31.3','icd10:I31.4','icd10:I31.8','icd10:I31.9','icd10:I32','icd10:I33.0','icd10:I33.9','icd10:I34.0','icd10:I34.1','icd10:I34.2','icd10:I34.8','icd10:I34.9','icd10:I35.0','icd10:I35.1','icd10:I35.2','icd10:I35.8','icd10:I35.9','icd10:I36.0','icd10:I36.1','icd10:I36.2','icd10:I36.8','icd10:I36.9','icd10:I37.0','icd10:I37.1','icd10:I37.2','icd10:I37.8','icd10:I37.9','icd10:I38','icd10:I39','icd10:I40.0','icd10:I40.1','icd10:I40.8','icd10:I40.9','icd10:I41','icd10:I42.0','icd10:I42.1','icd10:I42.2','icd10:I42.3','icd10:I42.4','icd10:I42.5','icd10:I42.6','icd10:I42.7','icd10:I42.8','icd10:I42.9','icd10:I43','icd10:I44.0','icd10:I44.1','icd10:I44.2','icd10:I44.30','icd10:I44.39','icd10:I44.4','icd10:I44.5','icd10:I44.60','icd10:I44.69','icd10:I44.7','icd10:I45.0','icd10:I45.10','icd10:I45.19','icd10:I45.2','icd10:I45.3','icd10:I45.4','icd10:I45.5','icd10:I45.6','icd10:I45.81','icd10:I45.89','icd10:I45.9','icd10:I46.2','icd10:I46.8','icd10:I46.9','icd10:I47.0','icd10:I47.1','icd10:I47.2','icd10:I47.9','icd10:I48.0','icd10:I48.1','icd10:I48.2','icd10:I48.3','icd10:I48.4','icd10:I48.91','icd10:I48.92','icd10:I49.01','icd10:I49.02','icd10:I49.1','icd10:I49.2','icd10:I49.3','icd10:I49.40','icd10:I49.49','icd10:I49.5','icd10:I49.8','icd10:I49.9','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9','icd10:I51.0','icd10:I51.1','icd10:I51.2','icd10:I51.3','icd10:I51.4','icd10:I51.5','icd10:I51.7','icd10:I51.81','icd10:I51.89','icd10:I51.9','icd10:I52','icd10:I97.0','icd10:I97.110','icd10:I97.111','icd10:I97.120','icd10:I97.121','icd10:I97.130','icd10:I97.131','icd10:I97.190','icd10:I97.191','icd10:M32.11','icd10:M32.12','icd10:R00.1','icd10:I26.01','icd10:I26.01','icd10:I26.02','icd10:I26.09','icd10:I26.90','icd10:I26.92','icd10:I26.99','icd10:I27.0','icd10:I27.1','icd10:I27.20','icd10:I27.21','icd10:I27.22','icd10:I27.23','icd10:I27.24','icd10:I27.29','icd10:I27.81','icd10:I27.82','icd10:I27.89','icd10:I27.9','icd10:I28.0','icd10:I28.1','icd10:I28.8','icd10:I28.9','icd10:T80.0XXA','icd10:T81.718A','icd10:T81.72XA','icd10:T82.817A','icd10:T82.818A','icd10:J84','icd10:J84.0','icd10:J84.01','icd10:J84.02','icd10:J84.03','icd10:J84.09','icd10:J84.1','icd10:J84.10','icd10:J84.11','icd10:J84.111','icd10:J84.112','icd10:J84.113','icd10:J84.114','icd10:J84.115','icd10:J84.116','icd10:J84.117','icd10:J84.17','icd10:J84.2','icd10:J84.8','icd10:J84.81','icd10:J84.82','icd10:J84.83','icd10:J84.84','icd10:J84.841','icd10:J84.842','icd10:J84.843','icd10:J84.848','icd10:J84.89','icd10:J84.9','icd10:J61','icd10:J68.4','icd10:I50.1','icd10:I50.20','icd10:I50.21','icd10:I50.22','icd10:I50.23','icd10:I50.30','icd10:I50.31','icd10:I50.32','icd10:I50.33','icd10:I50.40','icd10:I50.41','icd10:I50.42','icd10:I50.43','icd10:I50.810','icd10:I50.811','icd10:I50.812','icd10:I50.813','icd10:I50.814','icd10:I50.82','icd10:I50.83','icd10:I50.84','icd10:I50.89','icd10:I50.9', 'icd10:I05.0','icd10:I05.1','icd10:I05.2','icd10:I05.8','icd10:I05.9','icd10:I06.0','icd10:I06.1','icd10:I06.2','icd10:I06.8','icd10:I06.9','icd10:I07.0','icd10:I07.1','icd10:I07.2','icd10:I07.8','icd10:I07.9','icd10:I08.0','icd10:I08.1','icd10:I08.2','icd10:I08.3','icd10:I08.8','icd10:I08.9','icd10:I09.0','icd10:I09.1','icd10:I09.2','icd10:I09.81','icd10:I09.89','icd10:I09.9','icd10:Q24.2','icd10:Q24.3','icd10:Q24.4','icd10:Q24.5','icd10:Q24.6','icd10:Q24.8','icd10:Q24.9')

)
GROUP BY T1.patient_id, dx_code_id;




CREATE TABLE covid19_report.surv_health_cond_dx_codes_1yr_arrays as
select 
    patient_id,
    (select array_agg(a) from unnest(T1.asthma_1yr_dx_array) a where a is not null) asthma_1yr_dx_array,
	(select array_agg(a) from unnest(T1.chronic_lung_1yr_dx_array) a where a is not null) chronic_lung_1yr_dx_array,
	(select array_agg(a) from unnest(T1.obesity_1yr_dx_array) a where a is not null) obesity_1yr_dx_array,
	(select array_agg(a) from unnest(T1.hepatic_disorder_1yr_dx_array) a where a is not null) hepatic_disorder_1yr_dx_array,
	(select array_agg(a) from unnest(T1.renal_disorder_1yr_dx_array) a where a is not null) renal_disorder_1yr_dx_array,
	(select array_agg(a) from unnest(T1.smoking_1yr_dx_array) a where a is not null) smoking_1yr_dx_array,
    (select array_agg(a) from unnest(T1.immune_disorder_1yr_dx_array) a where a is not null) immune_disorder_1yr_dx_array,
	(select array_agg(a) from unnest(T1.alt_mental_status_1yr_dx_array) a where a is not null) alt_mental_status_1yr_dx_array,
	(select array_agg(a) from unnest(T1.renal_failure_1yr_dx_array) a where a is not null) renal_failure_1yr_dx_array,
	(select array_agg(a) from unnest(T1.cancer_1yr_dx_array) a where a is not null) cancer_1yr_dx_array,
	(select array_agg(a) from unnest(T1.diabetes_1yr_dx_array) a where a is not null) diabetes_1yr_dx_array,
	(select array_agg(a) from unnest(T1.cardiovascular_disease_1yr_dx_array) a where a is not null) cardiovascular_disease_1yr_dx_array
from (
    SELECT patient_id,
           array_agg(asthma_1yr_dx) asthma_1yr_dx_array,
		   array_agg(chronic_lung_1yr_dx) chronic_lung_1yr_dx_array,
		   array_agg(obesity_1yr_dx) obesity_1yr_dx_array,
		   array_agg(hepatic_disorder_1yr_dx) hepatic_disorder_1yr_dx_array,
		   array_agg(renal_disorder_1yr_dx) renal_disorder_1yr_dx_array,
		   array_agg(smoking_1yr_dx) smoking_1yr_dx_array,
           array_agg(immune_disorder_1yr_dx) immune_disorder_1yr_dx_array,
		   array_agg(alt_mental_status_1yr_dx) alt_mental_status_1yr_dx_array,
		   array_agg(renal_failure_1yr_dx) renal_failure_1yr_dx_array,
		   array_agg(cancer_1yr_dx) cancer_1yr_dx_array,
		   array_agg(diabetes_1yr_dx) diabetes_1yr_dx_array,
		   array_agg(cardiovascular_disease_1yr_dx) cardiovascular_disease_1yr_dx_array
    FROM covid19_report.surv_health_cond_dx_codes_1yr
    GROUP BY patient_id
	) T1;


CREATE TABLE covid19_report.surv_health_cond_details_1yr as
SELECT T1.patient_id,
max(asthma_1yr) as asthma_1yr,
array_to_string(asthma_1yr_dx_array, ' | ') as asthma_1yr_dx_codes,
max(chronic_lung_1yr) as chronic_lung_1yr,
array_to_string(chronic_lung_1yr_dx_array, ' | ') as chronic_lung_1yr_dx_codes,
max(obesity_1yr)  as obesity_1yr,
array_to_string(obesity_1yr_dx_array, ' | ') as obesity_1yr_dx_codes,
max(hepatic_disorder_1yr) as hepatic_disorder_1yr,
array_to_string(hepatic_disorder_1yr_dx_array, ' | ') as hepatic_disorder_1yr_dx_codes,
max(renal_disorder_1yr) as renal_disorder_1yr,
array_to_string(renal_disorder_1yr_dx_array, ' | ') as renal_disorder_1yr_dx_codes,
max(smoking_1yr) as smoking_1yr,
array_to_string(smoking_1yr_dx_array, ' | ') as smoking_1yr_dx_codes,
max(immune_disorder_1yr) as immune_disorder_1yr,
array_to_string(immune_disorder_1yr_dx_array, ' | ') as immune_disorder_1yr_dx_codes,
max(alt_mental_status_1yr) as alt_mental_status_1yr,
array_to_string(alt_mental_status_1yr_dx_array, ' | ') as alt_mental_status_1yr_dx_codes,
max(renal_failure_1yr) as renal_failure_1yr,
array_to_string(renal_failure_1yr_dx_array, ' | ') as renal_failure_1yr_dx_codes,
max(cancer_1yr) as cancer_1yr,
array_to_string(cancer_1yr_dx_array, ' | ') as cancer_1yr_dx_codes,
max(diabetes_1yr) as diabetes_1yr,
array_to_string(diabetes_1yr_dx_array, ' | ') as diabetes_1yr_dx_codes,
max(cardiovascular_disease_1yr) as cardiovascular_disease_1yr,
array_to_string(cardiovascular_disease_1yr_dx_array, ' | ') as cardiovascular_disease_1yr_dx_codes
FROM covid19_report.surv_health_cond_dx_codes_1yr T1
LEFT JOIN covid19_report.surv_health_cond_dx_codes_1yr_arrays T2 on (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id, asthma_1yr_dx_array, chronic_lung_1yr_dx_array, obesity_1yr_dx_array, hepatic_disorder_1yr_dx_array, renal_disorder_1yr_dx_array,
         smoking_1yr_dx_array, immune_disorder_1yr_dx_array, alt_mental_status_1yr_dx_array, renal_failure_1yr_dx_array, cancer_1yr_dx_array, diabetes_1yr_dx_array,
		 cardiovascular_disease_1yr_dx_array;


-- HYPERTENSION IS 2 YEARS

CREATE TABLE covid19_report.surv_health_cond_dx_codes_2yr AS 
SELECT T1.patient_id, 
CASE WHEN dx_code_id in ('icd10:I10', 'icd10:I15.0', 'icd10:I15.1', 'icd10:I15.2', 'icd10:I15.8', 'icd10:I15.9') then 1 else 0 end hypertension_2yr,
CASE WHEN dx_code_id in ('icd10:I10', 'icd10:I15.0', 'icd10:I15.1', 'icd10:I15.2', 'icd10:I15.8', 'icd10:I15.9') then dx_code_id else null end hypertension_2yr_dx
FROM 
covid19_report.surv_index_pats T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date >= T1.index_date - INTERVAL '2 years'
AND T2.date <= T1.index_date 
AND (
	--hypertension
	dx_code_id in ('icd10:I10', 'icd10:I15.0', 'icd10:I15.1', 'icd10:I15.2', 'icd10:I15.8', 'icd10:I15.9')
)
GROUP BY T1.patient_id, dx_code_id;


CREATE TABLE covid19_report.surv_health_cond_dx_codes_2yr_arrays as
select 
    patient_id,
    (select array_agg(a) from unnest(T1.hypertension_2yr_dx_array) a where a is not null) hypertension_2yr_dx_array
from (
    SELECT patient_id,
           array_agg(hypertension_2yr_dx) hypertension_2yr_dx_array
    FROM covid19_report.surv_health_cond_dx_codes_2yr
    GROUP BY patient_id
	) T1;

CREATE TABLE covid19_report.surv_health_cond_details_2yr as
SELECT T1.patient_id,
max(hypertension_2yr) as hypertension_2yr,
array_to_string(hypertension_2yr_dx_array, ' | ') as hypertension_2yr_dx_codes
FROM covid19_report.surv_health_cond_dx_codes_2yr T1
LEFT JOIN covid19_report.surv_health_cond_dx_codes_2yr_arrays T2 on (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id, hypertension_2yr_dx_codes;


--IMMUNOSUPPRESSIVE DRUGS

CREATE TABLE covid19_report.surv_immuno_drugs_1yr as
SELECT T1.patient_id,
1 as immunosuppressive_drugs_1yr
FROM covid19_report.surv_index_pats T1
INNER JOIN emr_prescription T2 on (T1.patient_id = T2.patient_id)
WHERE (name ~* ('abatacept|abraxane|actemra|adalimumab|adrucil|alemtuzumab|anakinra|arzerra|astagraf|aubagio|azasan|azathioprine|basiliximab|belatacept|bortezomib|campath|canakinumab|cellcept|certolizumab pegol|cimzia|cisplatin|copaxone|cyclophosphamide|cyclosporine|cytoxan|deltasone|dimethyl fumarate|enbrel|envarsus|etanercept|etoposide|fingolimod|fludara|fludarabine|fluorouracil|gengraf|gilenya|glatiramer|glatopa|golimumab|hecoria|humira|ifosfamide|ilaris|imuran|inflectra|infliximab|kineret|lemtrada|lenalidomide|meclorethamine|methotrexate|mitoxantrone|mycophenolate|myfortic|natalizumab|neoral|novantrone|ofatumumab|orencia|otrexup|paclitaxel|pomalidomide|pomalyst|prednisone|prograf|rapamune|rasuvo|rayos|remicade|renflexis|revlimid|rheumarex|rilonacept|rituxan|rituximab|sandimmune|simponi|simulect|sirolimus|stelara|sterapred|tacrolimus|tecfidera|temsirolimus|teriflunomide|thalidomide|thalomid|tocilizumab|tofacitinib|toposar|torisel|trexall|tysabri|ustekinumab|velcade|vepesid|vincristine|xatmep|xeljanz')
    --starts with matches for brands that match other generics
	OR name ilike 'orasone %'
	OR name ilike 'taxol %'
)	
AND name not ilike '%eye drop%'
AND name not ilike '%topical%'
AND name not ilike '%ointment%'
AND name not ilike '%opht%'
AND name not ilike '%cream%'
--ends with top
AND name not ilike '%top'
AND name not ilike '%OP EMUL%'
AND name not ilike '%EX CREA%'
AND name not ilike '%EX SOLN%'
AND name not ilike '%LIDOCAINE%'
AND name not ilike '%EX OINT%'
AND T2.date >= T1.index_date - INTERVAL '1 year'
AND T2.date <= T1.index_date
GROUP BY T1.patient_id, immunosuppressive_drugs_1yr;  




-- BRING ALL HEALTH CONDITIONS TOGETHER
-- REPORT ROW FOR ALL PATIENTS ON THE REPORT

CREATE TABLE covid19_report.surv_health_cond_details_all as
SELECT
DISTINCT T1.patient_id,
coalesce(asthma_1yr, 0) as asthma_1yr,
replace(asthma_1yr_dx_codes, 'icd10:', '') asthma_1yr_dx_codes,
coalesce(chronic_lung_1yr, 0) as chronic_lung_1yr,
replace(chronic_lung_1yr_dx_codes, 'icd10:', '') chronic_lung_1yr_dx_codes,
coalesce(obesity_1yr, 0) as obesity_1yr,
replace(obesity_1yr_dx_codes, 'icd10:', '') obesity_1yr_dx_codes,
coalesce(hepatic_disorder_1yr, 0) as hepatic_disorder_1yr,
replace(hepatic_disorder_1yr_dx_codes, 'icd10:', '') hepatic_disorder_1yr_dx_codes,
coalesce(renal_disorder_1yr, 0) as renal_disorder_1yr,
replace(renal_disorder_1yr_dx_codes, 'icd10:', '') renal_disorder_1yr_dx_codes,
coalesce(smoking_1yr, 0) as smoking_1yr,
replace(smoking_1yr_dx_codes, 'icd10:', '') smoking_1yr_dx_codes,
coalesce(immune_disorder_1yr, 0) as immune_disorder_1yr,
replace(immune_disorder_1yr_dx_codes, 'icd10:', '') immune_disorder_1yr_dx_codes,
coalesce(alt_mental_status_1yr, 0) as alt_mental_status_1yr,
replace(alt_mental_status_1yr_dx_codes, 'icd10:', '') alt_mental_status_1yr_dx_codes,
coalesce(renal_failure_1yr, 0) as renal_failure_1yr,
replace(renal_failure_1yr_dx_codes, 'icd10:', '') renal_failure_1yr_dx_codes,
coalesce(cancer_1yr, 0) as cancer_1yr,
replace(cancer_1yr_dx_codes, 'icd10:', '') cancer_1yr_dx_codes,
coalesce(diabetes_1yr, 0) as diabetes_1yr,
replace(diabetes_1yr_dx_codes, 'icd10:', '') diabetes_1yr_dx_codes,
coalesce(cardiovascular_disease_1yr, 0) as cardiovascular_disease_1yr,
replace(cardiovascular_disease_1yr_dx_codes, 'icd10:', '') cardiovascular_disease_1yr_dx_codes,
coalesce(hypertension_2yr, 0) as hypertension_2yr,
replace(hypertension_2yr_dx_codes, 'icd10:', '') hypertension_2yr_dx_codes,
coalesce(immunosuppressive_drugs_1yr, 0) as immunosuppressive_drugs_1yr
FROM covid19_report.surv_index_pats T1
LEFT JOIN covid19_report.surv_health_cond_details_1yr T2 on (T1.patient_id = T2.patient_id)
LEFT JOIN covid19_report.surv_health_cond_details_2yr T3 on (T1.patient_id = T3.patient_id)
LEFT JOIN covid19_report.surv_immuno_drugs_1yr T4 on (T1.patient_id = T4.patient_id);
--GROUP BY T1.patient_id;


-- THESE ARE PATIENTS WITH ACTIVE ASTHMA ACCORDING TO ESP
-- UPDATE THE asthma_1yr VALUE FOR THESE PATIENTS 
-- IN THE ASBSENCE OF DX CODES

UPDATE covid19_report.surv_health_cond_details_all
SET asthma_1yr = 1 
WHERE patient_id in ( 
	select T1.patient_id
	FROM covid19_report.surv_index_pats T1
	JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
	WHERE id in (select T1.case_id 
				 from nodis_caseactivehistory T1
				 JOIN (select case_id, max(date) max_ch_date from nodis_caseactivehistory group by case_id) T2 
																				  on (T1.case_id = T2.case_id and T1.date=max_ch_date)
				 WHERE status in ('I', 'R') )
	AND condition = 'asthma'
	--THE NODIS CASE MUST EXIST ON/BEFORE THE COVID CASE DATE
	AND T1.index_date >= T2.date
	AND T1.patient_id not in (select patient_id from covid19_report.surv_health_cond_details_all where asthma_1yr = 1)
);

-- DO THE SAME THING FOR HYPERTENSION

UPDATE covid19_report.surv_health_cond_details_all
SET hypertension_2yr = 1 
WHERE patient_id in ( 
	select T1.patient_id
	FROM covid19_report.surv_index_pats T1
	JOIN nodis_case T2 on (T1.patient_id = T2.patient_id)
	WHERE id in (select T1.case_id 
				 from nodis_caseactivehistory T1
				 JOIN (select case_id, max(date) max_ch_date from nodis_caseactivehistory group by case_id) T2 
																				  on (T1.case_id = T2.case_id and T1.date=max_ch_date)
				 WHERE status in ('I', 'R') )
	AND condition = 'hypertension'
	--THE NODIS CASE MUST EXIST ON/BEFORE THE COVID CASE DATE
	AND T1.index_date >= T2.date
	AND T1.patient_id not in (select patient_id from covid19_report.surv_health_cond_details_all where hypertension_2yr = 1)
);


-- IF A PATIENT HAS HAD A BMI OF >= 30 AT ANY TIME IN THE LAST YEAR
-- FLAG THEM FOR OBESITY

UPDATE covid19_report.surv_health_cond_details_all
SET obesity_1yr = 1 
WHERE patient_id in ( 
	SELECT patient_id FROM 
	covid19_report.surv_index_pats 
	WHERE patient_id not in (select patient_id from covid19_report.surv_health_cond_details_all where obesity_1yr = 1)
	AND patient_id in (select patient_id from emr_encounter where date >= (:run_date::date - INTERVAL '1 year')::date and bmi >= 30 AND date <= (:run_date::date + INTERVAL '14 days'))
);


-- IF A PATIENT HAS REPORTED BEING A SMOKER AT ANY TIME IN THE LAST YEAR
-- FLAG THEM FOR SMOKING

UPDATE covid19_report.surv_health_cond_details_all
SET smoking_1yr = 1 
WHERE patient_id in (
	SELECT patient_id FROM 
		covid19_report.surv_index_pats 
		WHERE patient_id not in (select patient_id from covid19_report.surv_health_cond_details_all where smoking_1yr = 1)
		AND patient_id in (select patient_id from emr_socialhistory where date >= (:run_date::date - INTERVAL '1 year')::date 
		                   AND date <= (:run_date::date + INTERVAL '14 days')
						   and upper(tobacco_use) in ('YES', 'CURRENT'))
);


-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST

INSERT INTO covid19_report.surv_pats_masked_patients
SELECT 
DISTINCT patient_id,
CONCAT(:site_abbr, NEXTVAL('covid19_report.surv_pats_masked_id_seq')) masked_patient_id
FROM covid19_report.surv_index_pats
WHERE patient_id not in (select patient_id from covid19_report.surv_pats_masked_patients)
GROUP by patient_id;



-- CREATE THE OUTPUT FILES
--DEMOGRAPHICS
CREATE TABLE covid19_report.surv_demog_ouput AS
SELECT masked_patient_id,
last_name,
first_name,
middle_name,
phone_number,
address1,
address2,
city,
state,
zip5,
country,
date_of_birth,
gender,
sexual_orientation,
ethnicity,
race,
pcp_name,
pcp_npi
from covid19_report.surv_demographics T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;


--LAB RESULTS
CREATE TABLE covid19_report.surv_labresult_ouput AS
SELECT masked_patient_id,
index_date,
test_type,
result_date,
collection_date,
result_string,
result_interpretation,
specimen_source,
lab_provider_name,
lab_provider_npi,
pat_dept_loc_or_provider_dept
from covid19_report.surv_lab_results T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;

-- LAB ORDERS
CREATE TABLE covid19_report.surv_laborder_ouput AS
SELECT masked_patient_id,
index_date,
lab_order_date,
order_type,
lab_order_provider_name,
lab_order_provider_npi,
lab_order_provider_dept
from covid19_report.surv_lab_orders T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;


--ENCOUNTERS
CREATE TABLE covid19_report.surv_enc_ouput AS
SELECT masked_patient_id,
index_date,
provider_name,
provider_npi,
provider_primary_dept,
visit_date,
hosp_admit_dt,
hosp_dschrg_dt,
pregnant,
encounter_type,
telehealth_yn,
o2_sat,
site_name,
fever_yn,
fever_dx_codes,
pneumonia_yn,
pneumonia_dx_codes,
bronchitis_yn,
bronchitis_dx_codes,
lri_yn,
lri_dx_codes,
ards_yn,
ards_dx_codes,
uri_yn,
uri_dx_codes,
cough_yn,
cough_dx_codes,
short_of_breath_yn,
short_of_breath_dx_codes,
viral_inf_yn,
viral_inf_dx_codes,
covid19_yn,
covid19_dx_codes,
chills_yn,
chills_dx_codes,
myalgia_yn,
myalgia_dx_codes,
headache_yn,
headache_dx_codes,
sore_throat_yn,
sore_throat_dx_codes,
olf_taste_dis_yn,
olf_taste_dis_dx_codes,
diff_breathing_yn,
diff_breathing_dx_codes
from covid19_report.surv_encounters_details T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;

-- MEDICATIONS
CREATE TABLE covid19_report.surv_med_ouput AS
SELECT masked_patient_id,
index_date,
med_provider_name,
med_provider_npi,
med_date,
pat_dept_loc_or_provider_dept,
drug_name,
drug_type
from covid19_report.surv_medications T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;



--ASSOCIATED HEALTH CONDITIONS
CREATE TABLE covid19_report.surv_hlthcond_ouput AS
SELECT masked_patient_id,
asthma_1yr,
asthma_1yr_dx_codes,
chronic_lung_1yr,
chronic_lung_1yr_dx_codes,
obesity_1yr,
obesity_1yr_dx_codes,
hepatic_disorder_1yr,
hepatic_disorder_1yr_dx_codes,
renal_disorder_1yr,
renal_disorder_1yr_dx_codes,
smoking_1yr,
smoking_1yr_dx_codes,
immune_disorder_1yr,
immune_disorder_1yr_dx_codes,
alt_mental_status_1yr,
alt_mental_status_1yr_dx_codes,
renal_failure_1yr,
renal_failure_1yr_dx_codes,
cancer_1yr,
cancer_1yr_dx_codes,
diabetes_1yr,
diabetes_1yr_dx_codes,
cardiovascular_disease_1yr,
cardiovascular_disease_1yr_dx_codes,
hypertension_2yr,
hypertension_2yr_dx_codes,
immunosuppressive_drugs_1yr
FROM covid19_report.surv_health_cond_details_all T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;


-- DROP TABLE IF EXISTS covid19_report.surv_demographics;
-- DROP TABLE IF EXISTS covid19_report.surv_lab_results;
-- DROP TABLE IF EXISTS covid19_report.surv_lab_orders;
-- DROP TABLE IF EXISTS covid19_report.surv_encounters_with_codes;
-- DROP TABLE IF EXISTS covid19_report.surv_encounters_with_code_arrays;
-- DROP TABLE IF EXISTS covid19_report.surv_encounters_details;
-- DROP TABLE IF EXISTS covid19_report.surv_medications;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_1yr;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_1yr_arrays;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_details_1yr;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_2yr;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_dx_codes_2yr_arrays;
-- DROP TABLE IF EXISTS covid19_report.surv_health_cond_details_all;
-- DROP TABLE IF EXISTS covid19_report.surv_immuno_drugs_1yr;

-- Test ID: 1699201




