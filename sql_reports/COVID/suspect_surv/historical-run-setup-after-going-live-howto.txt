1. Comment out the current weekly reporting script from crontab

  # COVID Suspect Reports - Run on Tuesday Mornings
  #00 09 *  *  2    /srv/esp/esp_tools/sql_reports/COVID/suspect_surv/covid-suspect-surv-report.sh

2. Update the esp_tools repository

3. Make a new HISTORICAL directory to store the new zip files. 
   If desired the old zip files can be removed after successful creation of the historical files.
   
   mkdir /srv/esp/data/covid_suspect_surv_reports/HISTORICAL

** IF THE NEW HISTORICAL SCRIPT HAS PREVIOUSLY BEEN RUN AT A SITE
4. Update covid-suspect-surv-report-historical.sh script
	A) input_start=2020-01-04 #set this to the first Saturday you do want to run
    B) input_end=2022-01-01   #set this to the next Saturday that you DON'T want to run
    C) make sure sent timestamp update is not commented out
	D) Make sure SFTP_RESULT section is left justified
	E) Change the REPORT_DIR path to match the HISTORICAL directory created above.
	F) Make sure COVID_WEEKLY_SQL is pointed to $DBNAME and not $DB_NAME
    G) FOR MGB MAKE SURE AUDIT ENTRY CODE IS IN THE SHELL SCRIPT
	
	
** IF THE NEW HISTORICAL SCRIPT HAS **NOT** PREVIOUSLY BEEN RUN AT A SITE
4. Create covid-suspect-surv-report-historical.sh script
	A) cp covid-suspect-surv-report-historical.sh.template covid-suspect-surv-report-historical.sh
	B) Set the parameters to match the weekly script
	C) Change the REPORT_DIR path to match the HISTORICAL directory created above.
	D) input_start=2020-01-04 #set this to the first Saturday you do want to run
    E) input_end=2022-01-01   #set this to the next Saturday that you DON'T want to run
	F) chmod 750 covid-suspect-surv-report-historical.sh
	
	
5. Clear out the sent_timestamp on all the existing cases.
    update covid19_report.suspect_nodis_case T1 SET sent_timestamp = null;

6. Kick off the script
   nohup ./covid-suspect-surv-report-historical.sh &
   
   
7. Once script finishes and files are confirmed enable normal weekly script via crontab (and delete old files in non-HISTORICAL directory if desired)

