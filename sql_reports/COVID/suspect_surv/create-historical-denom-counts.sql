drop table if exists kre_report.temp_covid_start_and_end_dates;
create table kre_report.temp_covid_start_and_end_dates as
select 
(d - interval '6 days')::date as week_start_date,
d::date as week_end_date
from
generate_series('2020-01-04', '2020-01-04'::date + interval '4.2 years', interval '1 weeks') d;

drop table if exists kre_report.temp_covid_denom_all;
create table kre_report.temp_covid_denom_all as 
SELECT count(distinct(patient_id)) total_pats_w_clin_enc, week_start_date::date, week_end_date::date
FROM gen_pop_tools.clin_enc T1
JOIN kre_report.temp_covid_start_and_end_dates T2 on (1=1)
WHERE date>= week_start_date
AND date <= week_end_date
GROUP BY week_end_date, week_start_date;



\COPY (SELECT * From kre_report.temp_covid_denom_all) To '/tmp/2024-03-07-covid-suspect-full-denom-file.csv' With CSV DELIMITER ',' HEADER;