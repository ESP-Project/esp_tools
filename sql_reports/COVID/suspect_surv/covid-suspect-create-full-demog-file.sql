DROP TABLE IF EXISTS covid19_report.surv_index_pats_all;
DROP TABLE IF EXISTS covid19_report.surv_demographics_all;
DROP TABLE IF EXISTS covid19_report.surv_demog_ouput_all;


CREATE TABLE covid19_report.surv_index_pats_all as
SELECT patient_id, date as index_date
FROM covid19_report.suspect_nodis_case 
WHERE (date >= '2020-01-01'
AND date <= '2021-09-03')
and sent_timestamp is not null;


-- GATHER DEMOGRAPHICS DATA
CREATE TABLE covid19_report.surv_demographics_all; as
SELECT DISTINCT
T1.id as patient_id,
upper(T1.last_name) as last_name,
upper(T1.first_name) as first_name,
upper(T1.middle_name) as middle_name,
concat(T1.areacode, '-', tel) as phone_number,
T1.address1,
T1.address2,
T1.city,
T1.state,
T1.zip5,
T1.country,
date_of_birth::date,
case when upper(gender) in ('M', 'MALE') then 'MALE'
	 when upper(gender) in ('F', 'FEMALE') then 'FEMALE'
	else 'UNKNOWN' end as gender,
null as gender_identity,
CASE WHEN upper(gender) in ('M', 'MALE') and T5.sex_partner_gender = 'FEMALE' then 'HETEROSEXUAL'
     WHEN upper(gender) in ('F', 'FEMALE')  and T5.sex_partner_gender = 'MALE' then 'HETEROSEXUAL'
	 WHEN upper(gender) in ('M', 'MALE') and T5.sex_partner_gender = 'MALE' then 'HOMOSEXUAL'
	 WHEN upper(gender) in ('F', 'FEMALE')  and T5.sex_partner_gender = 'FEMALE' then 'HOMOSEXUAL'
	 WHEN T5.sex_partner_gender in ('MALE + FEMALE', 'FEMALE + MALE') then 'BISEXUAL'
	 ELSE 'UNKNOWN' end as sexual_orientation,
CASE WHEN UPPER(ethnicity) in ('HISPANIC', 'HISPANIC/LATINO', 'HISPANIC OR LATINO') or UPPER(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') then 'HISPANIC' 
     WHEN UPPER(ethnicity) in ('NON HISPANIC', 'NOT HISPANIC OR LATINO') then 'NON-HISPANIC'
	 ELSE 'UNKNOWN' end as ethnicity,
CASE WHEN UPPER(race) in ('CAUCASIAN', 'WHITE', 'W') then 'WHITE'
     WHEN UPPER(race) in ('BLACK', 'B') or UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' then 'BLACK'
     WHEN UPPER(race) in ('OTHER','MULTIRACIAL', 'O', 'ASHKENAZI JEWISH', 'MORE THAN ONE RACE') then 'OTHER'
     WHEN UPPER(race) in ('INDIAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'NAT AMERICAN', 'I', 'ALASKAN', 'AM INDIAN', 'AMERICAN IND') or UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE'  then 'INDIAN'
     WHEN UPPER(race) in ('ASIAN', 'A', 'CHINESE', 'ASIAN INDIAN', 'FILIPINO', 'JAPANESE', 'KOREAN', 'MIDDLE EAST', 'VIETNAMESE') then 'ASIAN'
     WHEN UPPER(race) in  ('NATIVE HAWAI', 'NATIVE HAWAII', 'PACIFIC ISLANDER/HAWAIIAN', 'P', 'NATIVE HAWAIIAN', 'OTHER PACIFIC ISLANDER', 'PACIFIC ISLANDER', 'PAC ISLANDER', 'GUAM/CHAMORR', 'OTHER PACIFI', 'SAMOAN', 'PACIFIC ISLA') then 'PACIFIC_ISLANDER'
     WHEN race is null or race = '' then 'UNKNOWN' 
     ELSE 'UNKNOWN' END as race,
concat(T3.last_name, ', ', T3.first_name) as pcp_name,
T3.npi as pcp_npi
FROM emr_patient T1
JOIN covid19_report.surv_index_pats_all T2 on (T1.id = T2.patient_id)
LEFT JOIN emr_provider T3 on (T1.pcp_id = T3.id)
LEFT JOIN (SELECT patient_id, max(date) soc_date from emr_socialhistory where sex_partner_gender is not null group by patient_id) T4 on (T1.id = T4.patient_id)
LEFT JOIN emr_socialhistory T5 on (T1.id = T5.patient_id and T4.soc_date = T5.date and T5.sex_partner_gender is not null);


-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST
INSERT INTO covid19_report.surv_pats_masked_patients
SELECT 
DISTINCT patient_id,
CONCAT(:site_abbr, NEXTVAL('covid19_report.surv_pats_masked_id_seq')) masked_patient_id
FROM covid19_report.surv_index_pats_all
WHERE patient_id not in (select patient_id from covid19_report.surv_pats_masked_patients)
GROUP by patient_id;


-- CREATE THE OUTPUT FILES
--DEMOGRAPHICS
CREATE TABLE covid19_report.surv_demog_ouput_all AS
SELECT masked_patient_id,
last_name,
first_name,
middle_name,
phone_number,
address1,
address2,
city,
state,
zip5,
country,
date_of_birth,
gender,
sexual_orientation,
ethnicity,
race,
pcp_name,
pcp_npi
from covid19_report.surv_demographics_all T1,
covid19_report.surv_pats_masked_patients T2
WHERE T1.patient_id = T2.patient_id;


-- DEMOG_QUERY="SELECT * FROM covid19_report.surv_demog_ouput_all ORDER BY masked_patient_id"
-- DEMOG_FILE_CREATE=$(psql --dbname=$DBNAME -c "\copy ($DEMOG_QUERY) to '$REPORT_DIR/$DEMOG_FILE' with csv header;" -t)
-- 

--psql -d esp -c "\copy SELECT * FROM covid19_report.surv_demog_ouput_all ORDER BY masked_patient_id to /tmp/mgb-covid-demog-all.csv with csv header;" -t

--INVOKE SSH-AGENT
--eval `$KEYCHAIN -q --noask --agents ssh --eval $DPH_CERT`

--#UPLOAD THE ZIP FILE
--SFTP_RESULT=$($SFTP $DPH_SFTP_USERNAME"@"$DPH_SFTP_SERVER <<EOF
--put $REPORT_DIR/$ZIP_FILE
--exit
--EOF
--)
