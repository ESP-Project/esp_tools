-- For a broadstroke analysis, I simply took all patients that did not have a case since 02/01/2020 (of any category type)

-- CATEGORY 2 THAT WOULD GET PICKED UP IN THE WEEK IF WE DROPPED FEVER 
--196
select distinct(patient_id) from hef_event
where name in (
'dx:pneumonia','dx:bronchitis','dx:lri','dx:ards'
)
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01');

-- CATEGORY 3 THAT WOULD GET PICKED UP IN THE WEEK IF WE DROPPED FEVER 
--1522
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01');
	

-- CATEGORY 3 THAT WOULD GET PICKED UP IF WE DROPPED FEVER FROM BOTH CAT2 and CAT3
-- ELIMINATES THOSE THAT WOULD NOW GET PICKED UP BY CAT2 FIRST
--1452
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01')
and patient_id not in (
	select patient_id from hef_event
    where name in ('dx:pneumonia','dx:bronchitis','dx:lri','dx:ards') and date >= '2020-04-19' and date <= '2020-04-25');


-- CATEGORY 3 THAT WOULD GET PICKED UP IF WE INCLUDED COVID DIAGNOSIS CODE AS AN "OR" FOR FEVER
-- I AM JUST LOOKING FOR COVID DX IN THE SAME WEEK AS OTHER DIAG TO CUT DOWN ON OVERLAPPING PATIENTS BETWEEN WEEKS.
-- ALGORITHM CHANGE COULD POSSIBLY PICK UP MORE OR TRIGGER EARLIER AS IT WOULD BE LOOKING WITHIN 14 DAYS
-- 142
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id in (select patient_id from hef_event where name = 'dx:covid19' and date >= '2020-04-19' and date <= '2020-04-25')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' and date >= '2020-02-01');
	
	
-- CATEGORY 2 and 3 CASES THAT WOULD GET DROPPED IF FLU WAS A NEGATING DIAG CODE
-- 3	
select distinct(T1.patient_id), dx_code_id, T1.date flu_dx_date, T3.date case_date, T3.criteria as category
from emr_encounter T1
JOIN emr_encounter_dx_codes T2 on T1.id = T2.encounter_id
JOIN nodis_case T3 on T1.patient_id = T3.patient_id
WHERE
dx_code_id ~ '^(icd10:J09|icd10:J10|icd10:J11|icd9:487.0|icd9:487.1|icd9:487.8|icd9:488.0|icd9:488.1|icd9:488.8)'
and T1.date >= '2020-03-22'
and T1.date <= '2020-03-28'
and condition = 'covid19'
and criteria in ('Category 2', 'Category 3')
-- case date after start period
and T3.date >= '2020-02-01'
-- case date before week end
and T3.date <= '2020-03-28'
-- flu dx should be within 14 days before case date
and T1.date >= T3.date - INTERVAL '14 days' 
-- flu dx should be on/before case date
and T1.date <= T3.date;
	
	
	
	
	
	
Search the CCDD Category field and Also apply to Discharge Diagnosis: ^;Fever and Cough-Sob-DiffBr neg Influenza DD v1;^,or,^;CDC Coronavirus-DD v1;^,or,(,(,^[;/ ]R50.9^,or,^[;/ ]R509^,),and,(,^[;/ ]R05^,or,^[;/ ]R06.02^,or,^[;/ ]R0602^,or,^[;/ ]R07.0^,or,^[;/ ]R070^,),),

andnot,(,^[;/ ]J09^,or,^[;/ ]J10^,or,^[;/ ]J11^,or,^[;/ ]487.[018][;/ ]^,or,^[;/ ]487[018][;/ ]^,or,^[;/ ]488.[018][19][;/ ]^,or,^[;/ ]488[018][19][;/ ]^,or,^[;/ ]442696006[;/ ]^,or,^[;/ ]442438000[;/ ]^,or,^[;/ ]6142004[;/ ]^,or,^[;/ ]195878008[;/ ]^,),

or,^[;/ ]J98.8^,or,^[;/ ]J988^,or,^[;/ ]J22^,or,^[;/ ]J80^
	
	J09 - FLU
	J10
	J11
	
	ICD9:
	487.0
	487.1
	487.8
	488.0
	488.1
	488.8
	
	select count(*), dx_code_id
from emr_encounter_dx_codes
where dx_code_id ~ '^(icd10:J09|icd10:J10|icd10:J11|icd9:487.0|icd9:487.1|icd9:487.8|icd9:488.0|icd9:488.1|icd9:488.8)'
	
	
	