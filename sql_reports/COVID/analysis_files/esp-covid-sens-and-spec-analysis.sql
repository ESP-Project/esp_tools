-- case and pos pcr test in same week
select count(distinct T1.patient_id) algpos_pcrpos_week
from nodis_case T1
JOIN hef_event T2 on (T1.patient_id = T2.patient_id)
where T1.date >= '2020-04-19'
and T1.date <= '2020-04-25'
and condition = 'covid19'
and T2.name = 'lx:covid19_pcr:positive'
and T2.date >= '2020-04-19'
and T2.date <= '2020-04-25';



-- case and pos pcr test since 02/01
select count(distinct T1.patient_id) algpos_pcrpos_all
from nodis_case T1
JOIN hef_event T2 on (T1.patient_id = T2.patient_id)
where T1.date >= '2020-02-01'
and T1.date <= '2020-04-25'
and condition = 'covid19'
and T2.name = 'lx:covid19_pcr:positive'
and T2.date >= '2020-02-01'
and T2.date <= '2020-04-25';



-- case and neg test in same week (and no pos in the week)
select count(distinct T1.patient_id) algpos_pcrneg_week
from nodis_case T1
JOIN hef_event T2 on (T1.patient_id = T2.patient_id)
where T1.date >= '2020-04-19'
and T1.date <= '2020-04-25'
and condition = 'covid19'
and T2.name = 'lx:covid19_pcr:negative'
and T2.date >= '2020-04-19'
and T2.date <= '2020-04-25'
and T1.patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-04-19' and date <= '2020-04-25');


-- case and neg pcr test since 02/01 and patient has not had a pos pcr test
select count(distinct T1.patient_id) algpos_pcrneg_all
from nodis_case T1
JOIN hef_event T2 on (T1.patient_id = T2.patient_id)
where T1.date >= '2020-02-01'
and T1.date <= '2020-04-25'
and condition = 'covid19'
and T2.name = 'lx:covid19_pcr:negative'
and T1.patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-02-01' and date <= '2020-04-25')
and T2.date >= '2020-02-01'
and T2.date <= '2020-04-25';


-- no case but pos pcr test in the week
select count(distinct T1.patient_id) algneg_pcrpos_week
from hef_event T1
where name = 'lx:covid19_pcr:positive'
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id not in (select patient_id from nodis_case where condition = 'covid19' and date >= '2020-04-19' and date <= '2020-04-25');



-- no case but pos pcr test since 02/01
select count(distinct T1.patient_id) algneg_pcrpos_all
from hef_event T1
where name = 'lx:covid19_pcr:positive'
and date >= '2020-02-01'
and date <= '2020-04-25'
and patient_id not in (select patient_id from nodis_case where condition = 'covid19' and date >= '2020-02-01' and date <= '2020-04-25');



-- no case but neg test in the week (and no pos test in same week)
select count(distinct T1.patient_id) algneg_pcrneg_week
from hef_event T1
where name = 'lx:covid19_pcr:negative'
and date >= '2020-04-19'
and date <= '2020-04-25'
and patient_id not in (select patient_id from nodis_case where condition = 'covid19' and date >= '2020-04-19' and date <= '2020-04-25')
and patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-04-19' and date <= '2020-04-25');


-- no case but neg test since 02/01 (and no prior positive)
select count(distinct T1.patient_id) algneg_pcrneg_all
from hef_event T1
where name = 'lx:covid19_pcr:negative'
and date >= '2020-02-01'
and date <= '2020-04-25'
and patient_id not in (select patient_id from nodis_case where condition = 'covid19' and date >= '2020-02-01' and date <= '2020-04-25')
and patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-02-01' and date <= '2020-04-25');


-- pos tests for the week
select count(distinct patient_id) pcrpos_week
from hef_event 
where name = 'lx:covid19_pcr:positive'
and date >= '2020-04-19'
and date <= '2020-04-25'

-- neg tests for week but don't count patients with a positive as well
select count(distinct patient_id) pcrneg_week
from hef_event 
where name = 'lx:covid19_pcr:negative'
and patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-04-19' and date <= '2020-04-25')
and date >= '2020-04-19'
and date <= '2020-04-25'


select count(distinct patient_id) pcrpos_all
from hef_event 
where name = 'lx:covid19_pcr:positive'
and date >= '2020-02-01'
and date <= '2020-04-25';


select count(distinct patient_id) pcrneg_all
from hef_event 
where name = 'lx:covid19_pcr:negative'
and patient_id not in (select patient_id from hef_event where name = 'lx:covid19_pcr:positive' and date >= '2020-02-01' and date <= '2020-04-25')
and date >= '2020-02-01'
and date <= '2020-04-25';

