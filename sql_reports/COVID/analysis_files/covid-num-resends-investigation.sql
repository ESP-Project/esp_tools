drop table if exists kre_report.tmp_cov_pos_pcr;
create table kre_report.tmp_cov_pos_pcr AS
select T1.patient_id, T1.name, min(T1.date) first_pcr_pos
from hef_event T1
where T1.name = 'lx:covid19_pcr:positive'
--90 day recurrence just using 3 months
and T1.date >= '2021-06-01'
and T1.date < '2021-09-01'
group by T1.patient_id, T1.name;


-- eliminate patients that had a prior pos in the 90 days before our dates
drop table IF EXISTS kre_report.tmp_cov_pos_pcr_filtered;
create table kre_report.tmp_cov_pos_pcr_filtered AS
select *
from kre_report.tmp_cov_pos_pcr
WHERE patient_id not in (
select T1.patient_id
	--,T1.name, T1.date, T2.first_pcr_pos
from hef_event T1
INNER JOIN kre_report.tmp_cov_pos_pcr T2 ON (T1.patient_id = T2.patient_id and T1.date != T2.first_pcr_pos)
WHERE T1.date + INTERVAL '90 days' >= T2.first_pcr_pos
AND T1.name = 'lx:covid19_pcr:positive');



drop table if exists kre_report.tmp_cov_fu_tests;
create table kre_report.tmp_cov_fu_tests AS
select DISTINCT T1.patient_id, T1.first_pcr_pos, T2.date, T2.name,
case when T2.name is null then 0 else 1 end as fu_test
from kre_report.tmp_cov_pos_pcr_filtered T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and (T2.name ilike 'lx:covid19_pcr%' or T2.name ilike 'lx:covid19_ag%')
						   AND T2.name not ilike 'lx:%order%'
						   AND T2.date > T1.first_pcr_pos
						   AND T2.date <= T1.first_pcr_pos + INTERVAL '90 days');
						   
						   
select pat_count, total_resends, pats_with_resends
FROM (select count(distinct(patient_id)) as pat_count from kre_report.tmp_cov_fu_tests ) T1,
(select sum(fu_test) total_resends from kre_report.tmp_cov_fu_tests ) T2,
(select count(distinct(patient_id)) as pats_with_resends from kre_report.tmp_cov_fu_tests where fu_test != 0) T3;



select count(*), patient_id, date
from kre_report.tmp_cov_fu_tests
group by patient_id, date
having count(*) > 1

--------------------------------------
TESTING
------------------------------------------

drop table if exists kre_report.tmp_cov_fu_tests;
create table kre_report.tmp_cov_fu_tests AS
select DISTINCT T1.patient_id, T1.first_pcr_pos, T2.date, T2.name,
case when T2.date is null then 0 
     when T2.date - T1.first_pcr_pos <= 90 then 1 
	 else 0 end as fu_test_90days,
case when T2.date is null then 0 
     when T2.date - T1.first_pcr_pos <= 14 then 1 
	 else 0 end as fu_test_14days,
case when T2.date is null then 0 
     when T2.date - T1.first_pcr_pos <= 10 then 1 
	 else 0 end as fu_test_10days,	 
case when T2.date is null then 0 
     when T2.date - T1.first_pcr_pos <= 5 then 1 
	 else 0 end as fu_test_5days	 
from kre_report.tmp_cov_pos_pcr_filtered T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and (T2.name ilike 'lx:covid19_pcr%' or T2.name ilike 'lx:covid19_ag%')
						   AND T2.name not ilike 'lx:%order%'
						   AND T2.date > T1.first_pcr_pos
						   AND T2.date <= T1.first_pcr_pos + INTERVAL '90 days');
						   
						   
						   
select total_new_cases, cases_with_fu_testing, resends_for_tests_90_days, resends_for_tests_5_days, resends_for_tests_10_days, resends_for_tests_14_days
FROM
(select count(distinct(patient_id)) total_new_cases from kre_report.tmp_cov_fu_tests) T1,
(select count(distinct(patient_id)) cases_with_fu_testing from kre_report.tmp_cov_fu_tests where fu_test_90days != 0) T2,
(select sum(fu_test_90days) resends_for_tests_90_days from kre_report.tmp_cov_fu_tests ) T3,
(select sum(fu_test_5days) resends_for_tests_5_days from kre_report.tmp_cov_fu_tests ) T4,
(select sum(fu_test_10days) resends_for_tests_10_days from kre_report.tmp_cov_fu_tests ) T5,
(select sum(fu_test_14days) resends_for_tests_14_days from kre_report.tmp_cov_fu_tests ) T6;
						   
						   
select pat_count, test_resend_90_days, test_resend_14_days, test_resend_10_days, test_resend_5_days, 
pats_with_resends_90, pats_with_resends_14, pats_with_resends_10, pats_with_resends_5, pats_with_resends_5_only, pats_with_resends_10_only, pats_with_resends_14_only
FROM (select count(distinct(patient_id)) as pat_count from kre_report.tmp_cov_fu_tests ) T1,
(select sum(fu_test_90days) test_resend_90_days from kre_report.tmp_cov_fu_tests ) T2,
(select sum(fu_test_14days) test_resend_14_days from kre_report.tmp_cov_fu_tests ) T4,
(select sum(fu_test_10days) test_resend_10_days from kre_report.tmp_cov_fu_tests ) T5,
(select sum(fu_test_5days) test_resend_5_days from kre_report.tmp_cov_fu_tests ) T6,
(select count(distinct(patient_id)) as pats_with_resends_90 from kre_report.tmp_cov_fu_tests where fu_test_90days != 0) T3,
(select count(distinct(patient_id)) as pats_with_resends_14 from kre_report.tmp_cov_fu_tests where fu_test_14days != 0) T7,
(select count(distinct(patient_id)) as pats_with_resends_10 from kre_report.tmp_cov_fu_tests where fu_test_10days != 0) T8,
(select count(distinct(patient_id)) as pats_with_resends_5 from kre_report.tmp_cov_fu_tests where fu_test_5days != 0) T9,
(select count(distinct(patient_id)) as pats_with_resends_5_only from kre_report.tmp_cov_fu_tests 
      where fu_test_5days != 0 
	  and patient_id not in (select patient_id from kre_report.tmp_cov_fu_tests where fu_test_10days = 0  ) )T10,
(select count(distinct(patient_id)) as pats_with_resends_10_only from kre_report.tmp_cov_fu_tests 
      where fu_test_10days != 0 
	  and patient_id not in (select patient_id from kre_report.tmp_cov_fu_tests where fu_test_14days = 0 
	  and patient_id not in (select patient_id from kre_report.tmp_cov_fu_tests where fu_test_5days = 1)) )T11,
(select count(distinct(patient_id)) as pats_with_resends_14_only from kre_report.tmp_cov_fu_tests 
      where fu_test_14days != 0 
	  and patient_id not in (select patient_id from kre_report.tmp_cov_fu_tests where fu_test_10days = 1 
	  and patient_id not in (select patient_id from kre_report.tmp_cov_fu_tests where fu_test_5days = 1)) )T12
						   
			