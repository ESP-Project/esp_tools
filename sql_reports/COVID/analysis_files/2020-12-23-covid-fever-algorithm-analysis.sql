-- CATEGORY 2 THAT WOULD GET PICKED UP IN THE WEEK IF WE DROPPED FEVER 
-- ONLY LOOK FOR CASE (ANY CATEGORY) IN PREVIOUS 42 DAYS

select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:pneumonia','dx:bronchitis','dx:lri','dx:ards'
)
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;

	
select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:pneumonia','dx:bronchitis','dx:lri','dx:ards'
)
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;
	
	

-- CATEGORY 3 THAT WOULD GET PICKED UP IN THE WEEK IF WE DROPPED FEVER 
-- ONLY LOOK FOR CASE (ANY CATEGORY) IN PREVIOUS 42 DAYS

select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;

select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;
	
	
-- WOULD GET PICKED UP if we added olfactory and taste disorder and dropped fever
select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:olfactory-taste-disorders'
)
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;

	
select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:olfactory-taste-disorders'
)
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;
	
-- FOR MLCHC we have to use ICD codes since we don't have covid suspect implemented

select count(*) from (
select distinct(patient_id) 
from emr_encounter T1,
emr_encounter_dx_codes T2
WHERE T1.id = T2.encounter_id
and (T2.dx_code_id ilike 'icd10:R43.0'
	or T2.dx_code_id ilike 'icd10:R43.1'
	or T2.dx_code_id ilike 'icd10:R43.2'
	or T2.dx_code_id ilike 'icd10:R43.8'
	or T2.dx_code_id ilike 'icd10:R43.9')
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;

select count(*) from (	
select distinct(patient_id) 
from emr_encounter T1,
emr_encounter_dx_codes T2
WHERE T1.id = T2.encounter_id
and (T2.dx_code_id ilike 'icd10:R43.0'
	or T2.dx_code_id ilike 'icd10:R43.1'
	or T2.dx_code_id ilike 'icd10:R43.2'
	or T2.dx_code_id ilike 'icd10:R43.8'
	or T2.dx_code_id ilike 'icd10:R43.9')
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;


-- WOULD GET PICKED UP if we added olfactory and taste disorder and KEPT fever
select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:olfactory-taste-disorders'
)
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id in (
	select patient_id from hef_event
	where name in ('dx:fever', 'enc:fever')
	and date >= '2020-12-13'
	and date <= '2020-12-19')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;

select count(distinct(patient_id)) from (
select distinct(patient_id) from hef_event
where name in (
'dx:olfactory-taste-disorders'
)
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id in (
	select patient_id from hef_event
	where name in ('dx:fever', 'enc:fever')
	and date >= '2020-12-06'
	and date <= '2020-12-12')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;
	
	
select count(*) from (
select distinct(patient_id) 
from emr_encounter T1,
emr_encounter_dx_codes T2
WHERE T1.id = T2.encounter_id
and (T2.dx_code_id ilike 'icd10:R43.0'
	or T2.dx_code_id ilike 'icd10:R43.1'
	or T2.dx_code_id ilike 'icd10:R43.2'
	or T2.dx_code_id ilike 'icd10:R43.8'
	or T2.dx_code_id ilike 'icd10:R43.9')
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id in (
	select patient_id from hef_event
	where name in ('dx:fever', 'enc:fever')
	and date >= '2020-12-13'
	and date <= '2020-12-19')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;
	
	
	
select count(*) from (	
select distinct(patient_id) 
from emr_encounter T1,
emr_encounter_dx_codes T2
WHERE T1.id = T2.encounter_id
and (T2.dx_code_id ilike 'icd10:R43.0'
	or T2.dx_code_id ilike 'icd10:R43.1'
	or T2.dx_code_id ilike 'icd10:R43.2'
	or T2.dx_code_id ilike 'icd10:R43.8'
	or T2.dx_code_id ilike 'icd10:R43.9')
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id in (
	select patient_id from hef_event
	where name in ('dx:fever', 'enc:fever')
	and date >= '2020-12-06'
	and date <= '2020-12-12')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;
	
	
--CATEGORY 3 THAT WOULD GET PICKED UP IF WE INCLUDED COVID DIAGNOSIS CODE AS AN "OR" FOR FEVER
select count(*) from (
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-12-13'
and date <= '2020-12-19'
and patient_id in (select patient_id from hef_event where name = 'dx:covid19' and date >= '2020-12-13' and date <= '2020-12-19')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-11-07'
    and date <= '2020-12-19')  ) T1;
	
select count(*) from (
select distinct(patient_id) from hef_event
where name in (
'dx:viralinfection','dx:urespinfection','dx:cough','dx:shortofbreath'
)
and date >= '2020-12-06'
and date <= '2020-12-12'
and patient_id in (select patient_id from hef_event where name = 'dx:covid19' and date >= '2020-12-06' and date <= '2020-12-12')
and patient_id not in (
	select patient_id from nodis_case 
	where condition = 'covid19' 
	and date >= '2020-10-31'
    and date <= '2020-12-12')  ) T1;

