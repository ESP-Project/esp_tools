
-- all of the patients that had a COVID lab (any lab).

drop table if exists kre_report.denomval_covid_lab_pats;
create table kre_report.denomval_covid_lab_pats AS
select patient_id, date covid_lab_date, '1' as covid_test
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and extract(year from date) = '2020'
and T2.test_name ilike '%covid%'
GROUP by patient_id, date, covid_test;

-- Find all encounters for all patients that had a COVID lab and compute the number of days 
-- between EVERY COVID lab and EVERY encounter for the patient for the year.
drop table if exists kre_report.denomval_covid_pat_encs;
create table kre_report.denomval_covid_pat_encs AS
select T1.patient_id, covid_lab_date, date as enc_date, raw_encounter_type, abs(covid_lab_date - date) diff_lab_enc
from kre_report.denomval_covid_lab_pats T1
LEFT JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
WHERE (date is null or extract(year from date) = '2020')
AND (raw_encounter_type not in ('O-RECONCILED OUTSIDE DATA') or raw_encounter_type is null);


-- Find the minimum between each encounter and a covid_lab
-- This is the gap between an encounter and a COVID test
-- assign -1 if there are no encounters
drop table if exists kre_report.denomval_covid_lab_enc_gap;
create table kre_report.denomval_covid_lab_enc_gap AS
select patient_id, 
case when min(diff_lab_enc) is null then -1 
     else min(diff_lab_enc) end as min_diff_between_covid_lab_and_enc,
enc_date
from kre_report.denomval_covid_pat_encs
group by patient_id,enc_date;


-- Identify the max number of days between the min COVID lab and any encounter 
drop table if exists kre_report.denomval_covid_diff_max;
create table kre_report.denomval_covid_diff_max AS
select patient_id,
max(min_diff_between_covid_lab_and_enc) as max_date_diff
from kre_report.denomval_covid_lab_enc_gap
group by patient_id;


-- Identify "COVID Only" patients 
-- See how many patients have an encounter and lab less than 15 days apart. 
-- Add the patients to the exclusion list

INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT patient_id, '2020', 'all', 'COVID Only'
FROM kre_report.denomval_covid_diff_max
WHERE max_date_diff <= 15
ON CONFLICT DO NOTHING;


drop table if exists kre_report.denomval_covid_lab_pats;
drop table if exists kre_report.denomval_covid_pat_encs;
drop table if exists kre_report.denomval_covid_diff_max;

--------------------------------------------------------------------------------
--
--  2021
--
------------------------------------------------------------------------------
-- all of the patients that had a COVID lab (any lab).

drop table if exists kre_report.denomval_covid_lab_pats_2021;
create table kre_report.denomval_covid_lab_pats_2021 AS
select patient_id, date covid_lab_date, '1' as covid_test
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and extract(year from date) = '2021'
and T2.test_name ilike '%covid%'
GROUP by patient_id, date, covid_test;

-- Find all encounters for all patients that had a COVID lab and compute the number of days 
-- between EVERY COVID lab and EVERY encounter for the patient for the year.
drop table if exists kre_report.denomval_covid_pat_encs_2021;
create table kre_report.denomval_covid_pat_encs_2021 AS
select T1.patient_id, covid_lab_date, date as enc_date, raw_encounter_type, abs(covid_lab_date - date) diff_lab_enc
from kre_report.denomval_covid_lab_pats_2021 T1
LEFT JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
WHERE (date is null or extract(year from date) = '2021')
AND (raw_encounter_type not in ('O-RECONCILED OUTSIDE DATA') or raw_encounter_type is null);



-- Find the minimum between each encounter and a covid_lab
-- This is the gap between an encounter and a COVID test
-- assign -1 if there are no encounters
drop table if exists kre_report.denomval_covid_lab_enc_gap_2021;
create table kre_report.denomval_covid_lab_enc_gap_2021 AS
select patient_id, 
case when min(diff_lab_enc) is null then -1 
     else min(diff_lab_enc) end as min_diff_between_covid_lab_and_enc,
enc_date
from kre_report.denomval_covid_pat_encs_2021
group by patient_id,enc_date;


-- Identify the max number of days between the min COVID lab and any encounter 
drop table if exists kre_report.denomval_covid_diff_max_2021;
create table kre_report.denomval_covid_diff_max_2021 AS
select patient_id,
max(min_diff_between_covid_lab_and_enc) as max_date_diff
from kre_report.denomval_covid_lab_enc_gap_2021
group by patient_id;

-- Identify "COVID Only" patients 
-- See how many patients have an encounter and lab less than 15 days apart. 
-- Add the patients to the exclusion list

INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT patient_id, '2021', 'all', 'COVID Only'
FROM kre_report.denomval_covid_diff_max_2021
WHERE max_date_diff <= 15
ON CONFLICT DO NOTHING;


drop table if exists kre_report.denomval_covid_lab_pats_2021;
drop table if exists kre_report.denomval_covid_pat_encs_2021;
drop table if exists kre_report.denomval_covid_diff_max_2021;


----------------------------------------------------------------------------
-- 
-- COVID Vaccine
--
---------------------------------------------------------------------------

-- all people with a covid_19 vaccine
create table kre_report.covid_imm_pats AS
select patient_id, date
from emr_immunization
where name ilike '%COVID-19%'
OR (name is null and (dose ilike '%JANSSEN%' or manufacturer ilike '%JANSSEN%' or dose ilike '%covid%' or manufacturer ilike '%covid%'))
group by patient_id, date;


-- all people with nothing else in the month in clin_enc 
-- aside from the vaccine
-- these can be added to the exclusions

create table kre_report.covid_imm_only_exclude as 
select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
from gen_pop_tools.clin_enc T1
JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE 
T1.date = T2.date
and source = 'imu'

EXCEPT

-- clin_enc non-imu entries that are in the same month as the imunization BUT are not imu entries
-- also exclude rx clin_enc RX entries that are for the COVID vaccine
select patient_id, exclude_month, exclude_year
FROM (
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T1.date, source
		from gen_pop_tools.clin_enc T1
		JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
		WHERE 
		-- first day of the month
		T1.date >= date_trunc('month', T2.date)
		-- last day of the month
		and T1.date <= (date_trunc('month', T2.date) + interval '1 month' - interval '1 day')::date
		and source != 'imu'
		
	-- Don't count vaccines as prescriptions
	EXCEPT
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T2.date, source
		from gen_pop_tools.clin_enc T1
		JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
		where T2.name ilike '%COVID%vacc%'
		and source = 'rx'
		and T1.date = T2.date
) non_vax_clin_encs;

-- add them to the table
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT DISTINCT patient_id, exclude_year, exclude_month, 'COVID Immunization Only'
FROM kre_report.covid_imm_only_exclude
ON CONFLICT DO NOTHING;


--30592 example of 2 types (enc and imu)


-- -- all people with nothing else in the month in clin_enc 
-- -- aside from the vaccine
-- -- these can be added to the exclusions
-- create table kre_report.covid_imm_only_exclude as 
-- -- clin_enc entries that match the immunization date
-- select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
-- from gen_pop_tools.clin_enc T1
-- JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
-- WHERE 
-- T1.date = T2.date
-- and source = 'imu'

-- EXCEPT
	
-- -- clin_enc non-imu entries that are in the same month as the imunization BUT are not imu entries
-- select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
-- from gen_pop_tools.clin_enc T1
-- JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
-- WHERE 
-- -- first day of the month
-- T1.date >= date_trunc('month', T2.date)
-- -- last day of the month
-- and T1.date <= (date_trunc('month', T2.date) + interval '1 month' - interval '1 day')::date
-- and source != 'imu';








-----------------------

select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
from gen_pop_tools.clin_enc T1
JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE 
T1.date = T2.date
--AND T1.patient_id in (34260487)
and source = 'imu'

EXCEPT

select patient_id, exclude_month, exclude_year
FROM (
select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
,T1.date, source
from gen_pop_tools.clin_enc T1
JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE 
-- first day of the month
T1.date >= date_trunc('month', T2.date)
-- last day of the month
and T1.date <= (date_trunc('month', T2.date) + interval '1 month' - interval '1 day')::date
and source != 'imu'
--and T1.patient_id in (35045134, 35053830)
--and T1.patient_id in (34260487)
	
-- Don't count vaccines as prescriptions
EXCEPT
select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
,T2.date, source
from gen_pop_tools.clin_enc T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
where T2.name ilike '%COVID%vacc%'
and source = 'rx'
--and T1.patient_id in (35045134, 35053830)
--and T1.patient_id in (34260487)
and T1.date = T2.date) non_vax_clin_encs

LIMIT 100;

---------------------------





--- SKIP ALL OF THE REST OF THIS AS IT NEEDS WORK!!!!!

-- Remove excluded patients from our initial list
CREATE TABLE kre_report.covid_imm_pats_post_first_exclude as
SELECT * FROM kre_report.covid_imm_pats
EXCEPT
SELECT T1.patient_id, T2.date
FROM kre_report.covid_imm_only_exclude T1
JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE lpad(extract(month from date)::text, 2, '0') = T1.exclude_month
AND extract(year from date) = T1.exclude_year;

-- If patient had an rx or lx on the same day as immunization 
-- we don't need to exclude them
CREATE TABLE kre_report.covid_imm_pats_not_excluded as
select * 
from (
select T1.*
from gen_pop_tools.clin_enc T1
INNER JOIN kre_report.covid_imm_pats_post_first_exclude T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
order by patient_id) T1
where source in ('lx', 'rx');

-- Remove from our list
CREATE TABLE kre_report.covid_imm_pats_post_second_exclude as
SELECT * FROM kre_report.covid_imm_pats_post_first_exclude
EXCEPT
SELECT patient_id, date FROM kre_report.covid_imm_pats_not_excluded;


-- now we have enc and imu on the same date
-- these are patients with something other than an immunization ICD code on the date.
-- we won't exclude these records.
create table kre_report.covid_imm_pats_with_other_dx AS
select count(distinct(dx_code_id)), patient_id, date
from (
select T1.*, T3.raw_encounter_type, T4.dx_code_id
from gen_pop_tools.clin_enc T1
INNER JOIN kre_report.covid_imm_pats_post_second_exclude  T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN emr_encounter T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date)
INNER JOIN emr_encounter_dx_codes T4 ON (T3.id = T4.encounter_id and dx_code_id ilike 'icd10:%')
and raw_encounter_type not in ('NO SHOW')
and raw_encounter_type not ilike 'N-%'
--and raw_encounter_type = 'Y-IMM'
and source = 'enc'
order by patient_id, date desc
) S1
where dx_code_id != 'icd10:Z23'
group by patient_id, date;

-- Remove from our list
CREATE TABLE kre_report.covid_imm_pats_post_third_exclude as
SELECT * FROM kre_report.covid_imm_pats_post_second_exclude
EXCEPT
SELECT patient_id, date FROM kre_report.covid_imm_pats_with_other_dx;

-- These are immunization only visits. Add them to the exclusions
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT DISTINCT T1.patient_id, extract(year from T1.date) as exclude_year, lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, 'COVID Immunization Only'
--select T1.*, T3.raw_encounter_type, T4.dx_code_id
from gen_pop_tools.clin_enc T1
INNER JOIN kre_report.covid_imm_pats_post_third_exclude  T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN emr_encounter T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date)
INNER JOIN emr_encounter_dx_codes T4 ON (T3.id = T4.encounter_id and dx_code_id ilike 'icd10:%')
and raw_encounter_type not in ('NO SHOW')
and raw_encounter_type not ilike 'N-%'
--and raw_encounter_type = 'Y-IMM'
and source = 'enc'
ON CONFLICT DO NOTHING;


-- now what is left are a bunch of patients that had a covid vaccine 
-- but aren't in the clinical encounters table for whatever reason
SELECT * FROM kre_report.covid_imm_pats_post_third_exclude
EXCEPT
SELECT patient_id, date FROM kre_report.covid_imm_enc_only;




