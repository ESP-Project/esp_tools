-- Find all the clin_enc entries based on a covid lab
drop table if exists kre_report.covidrs_covid_lab_pats;
create table kre_report.covidrs_covid_lab_pats AS
select T3.patient_id, T3.date covid_lab_date, T3.source,
lpad(extract(month from T3.date)::text, 2, '0') as exclude_month, extract(year from T3.date) as exclude_year
from emr_labresult T1
LEFT JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN gen_pop_tools.clin_enc T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T3.source = 'lx')
WHERE T1.date >= '2020-01-01'
-- REPLACE WITH VARIABLE
AND T1.date < :cutoff_date::date
AND (T2.test_name ilike '%covid%')
GROUP by T3.patient_id, T3.date, T3.source;



-- Keep patients that have other labs in the same month that are NOT covid labs & are NOT care everywhere labs
-- Keep patients with an encounter in the same month as COVID lab but greater than 15 days from lab and have an ICD not COVID related
drop table if exists kre_report.covidrs_covid_lab_pats_exclude;
create table kre_report.covidrs_covid_lab_pats_exclude AS
SELECT * from kre_report.covidrs_covid_lab_pats
EXCEPT
	-- non covid labs in the same month as covid lab (but not on the same date as a covid lab)
	SELECT T1.patient_id, covid_lab_date, T1.source, 
	lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
	from gen_pop_tools.clin_enc T1 
	INNER JOIN kre_report.covidrs_covid_lab_pats T2 ON (T1.patient_id = T2.patient_id and T1.date != T2.covid_lab_date)
	INNER JOIN emr_labresult T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T3.date != T2.covid_lab_date)
	LEFT JOIN conf_labtestmap T4 ON (T3.native_code = T4.native_code)
	WHERE T1.source = 'lx'
	AND lpad(extract(month from T1.date)::text, 2, '0') = T2.exclude_month
	AND extract(year from T1.date) = T2.exclude_year
	AND (T4.test_name not ilike '%covid%' or T4.test_name is null)
	-- some labs are not mapped but are covid related (add more strings here as needed)
	AND T3.native_name not ilike '%covid%'
	AND T3.native_name not ilike 'sars%'
	AND T3.native_name not ilike '%coronavirus%'
	AND (T3.procedure_name not ilike '%covid%' or T3.procedure_name is null)
	AND (T3.procedure_name not ilike 'SARS%' or T3.procedure_name is null)
	AND (T3.procedure_name not ilike '%coronavirus%' or T3.procedure_name is null)
	-- don't count CARE EVERYWHERE labs
	AND T3.native_name not ilike '%CARE EVERYWHERE%'
EXCEPT
	-- Patients that had an encounter within the month but greater than 15 days from covid test
	-- Encounter must have ICD codes that aren't COVID related
	SELECT T1.patient_id, covid_lab_date, T2.source,
	lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
	from gen_pop_tools.clin_enc T1 
	INNER JOIN kre_report.covidrs_covid_lab_pats T2 ON (T1.patient_id = T2.patient_id and T1.date != T2.covid_lab_date)
	INNER JOIN emr_encounter T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date)
	INNER JOIN emr_encounter_dx_codes T4 ON (T3.id = T4.encounter_id)
	WHERE T1.source = 'enc'
	AND lpad(extract(month from T1.date)::text, 2, '0') = T2.exclude_month
	AND extract(year from T1.date) = T2.exclude_year
	AND abs(T2.covid_lab_date - T1.date) > 15
	-- encounter does not count if it is one of known COVID screening encounters
	AND dx_code_id not in ('icd10:Z11.59', 'icd10:Z20.828', 'icd9:799.9', 'icd10:U07.1', 'icd10:Z11.52', 'icd10:Z20.822', 'icd10:Z03.818');


-- Add patients ABOVE from kre_report.covidrs_covid_lab_pats_exclude to the exclusion list.

-- add them to the table
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT DISTINCT patient_id, exclude_year::text, exclude_month::text, 'COVID Lab Only'
FROM kre_report.covidrs_covid_lab_pats_exclude
EXCEPT 
SELECT patient_id, year, month, reason
FROM gen_pop_tools.clin_enc_exclusions
ON CONFLICT DO NOTHING;


-- Identify patients where the only thing they had in the month is an encounter with an ICD of interest.
-- 'icd10:Z11.59' - Encounter for screening for other viral diseases
-- 'icd10:Z20.828' - Contact with and (suspected) exposure to other viral communicable diseases
-- 'icd10:U07.1' - COVID19

DROP TABLE IF EXISTS kre_report.covidrs_covid_enc_pats_exclude;
CREATE TABLE kre_report.covidrs_covid_enc_pats_exclude AS
-- Patients with only encounters and have ICD of interest
    SELECT  T1.patient_id, lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
		from gen_pop_tools.clin_enc T1
		INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
		INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
		WHERE source = 'enc'
		AND T1.date >= '2020-01-01'
		-- REPLACE WITH VARIABLE
        AND T1.date < :cutoff_date::date
		AND dx_code_id in ('icd10:Z11.59', 'icd10:Z20.828', 'icd10:U07.1', 'icd10:Z11.52', 'icd10:Z20.822', 'icd10:Z03.818')
		
	EXCEPT
	    --patients have another type of clin_enc source in the month
		select T1.patient_id, lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
		from gen_pop_tools.clin_enc T1
		WHERE source != 'enc'
		AND date >= '2020-01-01'
		-- REPLACE WITH VARIABLE
        AND T1.date < :cutoff_date::date
		
	   -- patients that have other ICD codes during the month
	EXCEPT	
	    SELECT  T1.patient_id, lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
		from gen_pop_tools.clin_enc T1
		INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
		INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
		WHERE source = 'enc'
		AND T1.date >= '2020-01-01'
		-- REPLACE WITH VARIABLE
        AND T1.date < :cutoff_date::date
		AND dx_code_id not in ('icd10:Z11.59', 'icd10:Z20.828', 'icd9:799.9', 'icd10:U07.1', 'icd10:Z11.52', 'icd10:Z20.822', 'icd10:Z03.818');
		
		
-- Add patients ABOVE from kre_report.covidrs_covid_enc_pats_exclude to the exclusion list.	

-- add them to the table
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT DISTINCT patient_id, exclude_year::text, exclude_month::text, 'COVID Enc Only'
FROM kre_report.covidrs_covid_enc_pats_exclude
EXCEPT 
SELECT patient_id, year, month, reason
FROM gen_pop_tools.clin_enc_exclusions
ON CONFLICT DO NOTHING;




----------------------------------------------------------------------------
-- 
-- COVID Vaccine
--
---------------------------------------------------------------------------

-- all people with a covid_19 vaccine
drop table if exists kre_report.covid_imm_pats;
create table kre_report.covid_imm_pats AS
select patient_id, date
from emr_immunization
where ( name ilike '%COVID-19%'
OR (name is null and (dose ilike '%JANSSEN%' or manufacturer ilike '%JANSSEN%' or dose ilike '%covid%' or manufacturer ilike '%covid%')) )
AND date >= '2020-01-01'
-- REPLACE WITH VARIABLE
AND date < :cutoff_date::date
group by patient_id, date;


-- all people with nothing else in the month in clin_enc 
-- aside from the vaccine
-- these can be added to the exclusions

drop table if exists kre_report.covid_imm_only_exclude;
create table kre_report.covid_imm_only_exclude as 
select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
from gen_pop_tools.clin_enc T1
JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE 
T1.date = T2.date
and source = 'imu'

EXCEPT

-- clin_enc non-imu entries that are in the same month as the imunization BUT are not imu entries
-- also exclude rx clin_enc RX entries that are for the COVID vaccine
select patient_id, exclude_month, exclude_year
FROM (
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T1.date, source
		from gen_pop_tools.clin_enc T1
		JOIN kre_report.covid_imm_pats T2 ON (T1.patient_id = T2.patient_id)
		WHERE 
		-- first day of the month
		T1.date >= date_trunc('month', T2.date)
		-- last day of the month
		and T1.date <= (date_trunc('month', T2.date) + interval '1 month' - interval '1 day')::date
		and source != 'imu'
		
	-- Don't count vaccines as prescriptions
	EXCEPT
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T2.date, source
		from gen_pop_tools.clin_enc T1
		JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
		where T2.name ilike '%COVID%vacc%'
		and source = 'rx'
		and T1.date = T2.date
) non_vax_clin_encs;

-- add them to the table
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT DISTINCT patient_id, exclude_year::text, exclude_month::text, 'COVID Immunization Only'
FROM kre_report.covid_imm_only_exclude
EXCEPT
SELECT patient_id, year, month, reason
FROM gen_pop_tools.clin_enc_exclusions
ON CONFLICT DO NOTHING;



--THINGS TO DO!!!!!

-- delete temp tables





