good test patient = 74904048


-- All people with a COVID mapped lab

drop table if exists kre_report.covid_lab_pats;
create table kre_report.covid_lab_pats AS
select patient_id, date
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and date >= '2020-01-01'
and T2.test_name ilike '%covid%'
group by patient_id, date;



create table kre_report.covid_lab_only_exclude as 
select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
from gen_pop_tools.clin_enc T1
JOIN kre_report.covid_lab_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE 
T1.date = T2.date
and source = 'lx'


EXCEPT

-- clin_enc non-lx entries that are in the same month as the covid lab BUT are not lx entries
-- also exclude  clin_enc "enc" entries that are within 15 days of the COVID lab
select patient_id, exclude_month, exclude_year
FROM (
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T1.date, source
		from gen_pop_tools.clin_enc T1
		JOIN kre_report.covid_lab_pats T2 ON (T1.patient_id = T2.patient_id)
		WHERE 
		-- first day of the month
		T1.date >= date_trunc('month', T2.date)
		-- last day of the month
		and T1.date <= (date_trunc('month', T2.date) + interval '1 month' - interval '1 day')::date
		and source != 'lx'
		
	-- Don't count vaccines as prescriptions
	EXCEPT
		select T1.patient_id, lpad(extract(month from T2.date)::text, 2, '0') as exclude_month, extract(year from T2.date) as exclude_year
		,T2.date, source
		from gen_pop_tools.clin_enc T1
		JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
		where T2.name ilike '%COVID%vacc%'
		and source = 'rx'
		and T1.date = T2.date
) non_vax_clin_encs;


-- exclude all covid labs
-- exclude encounters within 15 days of covid labs
-- exclude encounters that only have certain ICD codes and nothing else in the month.


'icd10:Z11.59' - Encounter for screening for other viral diseases
'icd10:Z20.828' - Contact with and (suspected) exposure to other viral communicable diseases

good test patient = 4811550

encounter is the only thing and one of these codes
encounter outside of lab window and encounter with these codes the only thing


-- Encounters in the same month but more than 15 days from the lab date
create table kre_report.covidrs_covid_enc_pats_exclude AS
SELECT * from kre_report.covidrs_covid_lab_pats_exclude
EXCEPT
SELECT T1.patient_id, T1.date as non_covid_lab_date, T1.source, 
lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
from gen_pop_tools.clin_enc T1 
INNER JOIN kre_report.covidrs_covid_lab_pats_exclude T2 ON (T1.patient_id = T2.patient_id and T1.date != T2.covid_lab_date)
INNER JOIN emr_encounter T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T3.date != T2.covid_lab_date)
WHERE T1.source = 'enc'
AND lpad(extract(month from T1.date)::text, 2, '0') = T2.exclude_month
AND extract(year from T1.date) = T2.exclude_year
AND abs(T2.covid_lab_date - T3.date) >= 15


select * from kre_report.covidrs_covid_lab_pats
except select * from kre_report.covidrs_covid_lab_pats_exclude

patient has non covid lab on diff date -- why still in temp2?
select * from kre_report.covidrs_covid_lab_pats_exclude
where patient_id = 74872943

select * from kre_report.covidrs_covid_lab_pats
where patient_id = 74873363



LEFT JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN gen_pop_tools.clin_enc T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T3.source = 'lx')
WHERE T1.date >= '2020-01-01'
and (T2.test_name ilike '%covid%')
and T3.patient_id in (74904048, 298)
GROUP by T3.patient_id, T3.date, T3.source;






-- pats without encounters??
-- Encounters within 15 days
SELECT T1.patient_id, covid_lab_date, T1.source, T2.date as enc_date, T2.source, abs(T1.covid_lab_date - T2.date) as date_diff,
lpad(extract(month from covid_lab_date)::text, 2, '0') as exclude_month, extract(year from covid_lab_date) as exclude_year
FROM kre_report.covidrs_covid_lab_pats T1
LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.date >= '2020-01-01'
AND (T2.source = 'enc' or T2.source is null)
--AND ( abs(T1.covid_lab_date - T2.date) <= 15 or T2.date is null)


select * from gen_pop_tools.clin_enc_exclusions
where patient_id = 74904048

select * from gen_pop_tools.clin_enc
where patient_id = 298
and date >= '2020-01-01'

select * from emr_labresult where patient_id = 298
and date = '2020-04-16'

select * from emr_prescription where patient_id = 298
and date = '2020-04-17'

select * from emr_encounter where patient_id = 298
and date = '2020-03-23'


(74904048, 298)

only covid lab -- no encounters (74876397)


SELECT T1.patient_id, covid_lab_date, T2.source,
lpad(extract(month from T1.date)::text, 2, '0') as exclude_month, extract(year from T1.date) as exclude_year
from gen_pop_tools.clin_enc T1 
INNER JOIN kre_report.covidrs_covid_lab_pats_exclude T2 ON (T1.patient_id = T2.patient_id and T1.date != T2.covid_lab_date)
WHERE T1.source = 'enc'
AND lpad(extract(month from T1.date)::text, 2, '0') = T2.exclude_month
AND extract(year from T1.date) = T2.exclude_year
AND abs(T2.covid_lab_date - T1.date) > 15











