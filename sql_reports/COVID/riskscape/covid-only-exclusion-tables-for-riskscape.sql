DROP TABLE IF EXISTS gen_pop_tools.clin_enc_exclusions;

CREATE TABLE gen_pop_tools.clin_enc_exclusions
(
    patient_id integer NOT NULL,
    year character varying(10) COLLATE pg_catalog."default" NOT NULL,
    month character varying(10) COLLATE pg_catalog."default" NOT NULL,
    reason character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT clin_enc_exclusions_pkey PRIMARY KEY (patient_id, year, month)
);



changes within tt_tables.pg.sql

after line 57

This is what we need to add

-- Exclude specified encounters
DELETE FROM gen_pop_tools.tt_enc_pat pat_encs
USING gen_pop_tools.clin_enc_exclusions exclude
WHERE pat_encs.patient_id = exclude.patient_id
AND (split_part(pat_encs.year_month, '_', 1) = exclude.year )
AND ( (split_part(pat_encs.year_month, '_', 2) = exclude.month) OR (exclude.month = 'all') );

-- Continue build of patient_encounter list



SELECT *
FROM gen_pop_tools.tt_enc_pat pat_encs
INNER JOIN gen_pop_tools.clin_enc_exclusions exclude ON (pat_encs.patient_id = exclude.patient_id)
WHERE pat_encs.year_month ilike concat(exclude.year, '_%')
AND (split_part(pat_encs.year_month, '_', 2) = exclude.month OR exclude.month = 'all')


populate the table:
INSERT INTO gen_pop_tools.clin_enc_exclusions
SELECT patient_id, '2020', 'all', 'COVID Only'
FROM kre_report.denomval_covid_diff_max
WHERE max_date_diff <= 15;