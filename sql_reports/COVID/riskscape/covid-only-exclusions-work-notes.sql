	 --218482 2020_04
	 --219128 2020_05
	 --221629 2020_06
	 --223393 2020_07
select count(distinct(patient_id))
from gen_pop_tools.tt_pat_seq_enc
where year_month = '2020_07'
and prior2 is not null
	 
	 
	 -- this gives me patients to investigate!
	 
	 select distinct(patient_id)
	 from gen_pop_tools.tt_pat_seq_enc
where year_month = '2020_06'
and prior2 is not null
	 and patient_id not in (select patient_id from gen_pop_tools.tt_pat_seq_enc where year_month = '2020_05'
and prior2 is not null )



======


select count(*), extract(year from date) enc_year, extract(month from date) enc_month, raw_encounter_type
from emr_encounter
where date between '2020-01-01' and '2020-12-31'
and raw_encounter_type in (
'I-HOSPITAL ENCOUNTER',
'O-APPOINTMENT',
'O-BH TELEVISIT',
'O-HISTORY',
'O-PATIENT MESSAGE',
'O-REFILL',
'O-TELEPHONE',
'O-TELEVISIT',
'O-TRAVEL',
'O-RECONCILED OUTSIDE DATA'
)
group by enc_year, enc_month, raw_encounter_type
order by enc_year, enc_month, raw_encounter_type



-- Atrius seeing pre-procedure covid testing
-- also has "RECDATA"


===============================================

-- I think this would get the gap between each covid test and encounter
-- this would be better for handling re-testing

select patient_id, min(diff_lab_enc) new_min, enc_date
from kre_report.denomval_covid_pat_encs
where patient_id = 74889941
group by patient_id,enc_date

select patient_id, min(diff_lab_enc) min_diff_between_covid_lab_and_enc, enc_date
from kre_report.denomval_covid_pat_encs
where patient_id = 10295
group by patient_id,enc_date


========================================================================

MLCHC
-- 20468 THIS IS THE NUMBER TO COMPARE AGAINST
-- 26062 NEW
select count(*) 
FROM kre_report.denomval_covid_diff_max_2021
WHERE max_date_diff <= 15

-- 53731 THIS IS THE NUMBER TO COMPARE AGAINST FOR 2020
-- NEW: 63151
select count(*) from gen_pop_tools.clin_enc_exclusions
where year = '2020'

ATRIUS
--3782
--THIS IS THE NUMBER TO COMPARE AGAINST FOR 2020
-- NEW 4589
select count(*) from gen_pop_tools.clin_enc_exclusions
where year = '2020'

--4807
--NEW: 6015
--THIS IS THE NUMBER TO COMPARE AGAINST FOR 2021
select count(*) from gen_pop_tools.clin_enc_exclusions
where year = '2021'


CHA
--8709 This is the number to compare against
-- 11508 NEW
-- 23096 / 22742
-- 29075 NEW 
select count(*) from gen_pop_tools.clin_enc_exclusions
where year = '2021'

============================================================

-- this is a good one
-- run at cha on mystery months

select patient_id from gen_pop_tools.tt_pat_seq_enc
where year_month = '2021_04'
and prior2 is not null
except
select patient_id from gen_pop_tools.tt_pat_seq_enc
where year_month = '2021_03'
and prior2 is not null
limit 500






