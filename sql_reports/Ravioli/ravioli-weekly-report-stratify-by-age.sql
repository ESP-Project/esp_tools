SET client_min_messages TO WARNING;

-- -- SETUP STEPS
-- -- ONLY RUN ONCE


-- -- Table: public.ravioli_rpt_dx_codes

-- -- DROP TABLE IF EXISTS public.ravioli_rpt_dx_codes;

-- CREATE TABLE IF NOT EXISTS public.ravioli_rpt_dx_codes
-- (
    -- dx_code character varying(25) COLLATE pg_catalog."default" NOT NULL,
    -- condition character varying(50) COLLATE pg_catalog."default" NOT NULL,
    -- CONSTRAINT ravioli_rpt_dx_codes_pk PRIMARY KEY (dx_code, condition)
-- );

-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:A08.2', 'adenovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'adenovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.0', 'adenovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.0', 'adenovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'coronavirus_non19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.2', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.29', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.82', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.89', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J80', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R05.1', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R48.1', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:U07.1', 'covid19');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.81', 'h_metapneumovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.3', 'h_metapneumovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.1', 'h_metapneumovirus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J09.X1', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J09.X2', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J10.00', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J10.1', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J11.00', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J11.1', 'influenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B96.0', 'm_pneumoniae');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B33.8', 'parainfluenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'parainfluenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.4', 'parainfluenza');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.0', 'rhino_entero_virus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B34.8', 'rhino_entero_virus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.10', 'rhino_entero_virus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.6', 'rhino_entero_virus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J45.902', 'rhino_entero_virus');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:B97.4', 'rsv');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.1', 'rsv');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J20.5', 'rsv');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.0', 'rsv');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J21.8', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R06.03', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:P81.9', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J12.9', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R50.81', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.90', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R05.9', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.91', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:J96.92', 'ravioli_other');
-- INSERT INTO public.ravioli_rpt_dx_codes VALUES('icd10:R57.9', 'ravioli_other');



-- -- Table: public.ravioli_rpt_full_output_w_ageagroup

-- -- DROP TABLE IF EXISTS public.ravioli_rpt_full_output_w_ageagroup;

-- CREATE TABLE IF NOT EXISTS public.ravioli_rpt_full_output_w_ageagroup
-- (
    -- week_end_date date NOT NULL,
	-- sex character varying (20) NOT NULL,
    -- age_group character varying(20) NOT NULL,
	-- race character varying(100) NOT NULL,
	-- ethnicity character varying(100) NOT NULL,
	-- clin_enc_pats_l2yr bigint,
	-- clin_enc_pats_week bigint,
	-- ravioli_pats_week bigint,
    -- adenovirus_pats bigint,
    -- coronavirus_non19_pats bigint,
    -- covid19_pats bigint,
    -- h_metapneumovirus_pats bigint,
    -- influenza_pats bigint,
	-- m_pneumoniae_pats bigint,
    -- parainfluenza_pats bigint,
    -- rhino_entero_pats bigint,
    -- rsv_pats bigint,
    -- ravioli_other_pats bigint,
	-- ili_cases bigint,
	-- pertussis_pats bigint,
    -- CONSTRAINT ravioli_rpt_full_output_w_ageagro_week_end_date_age_group_key UNIQUE (week_end_date, sex, age_group, race, ethnicity)
-- )

-- ALTER TABLE public.ravioli_rpt_full_output_w_ageagroup
-- ADD pertussis_pats BIGINT; 

---------------------------------------------------------------------------------------------------------------------


-- Get all labs for the conditions/pathogens of interest
DROP TABLE IF EXISTS public.ravioli_rpt_pos_tests;
create table public.ravioli_rpt_pos_tests AS
select DISTINCT patient_id, date as index_test_date, 
CASE WHEN name = 'lx:rsv:positive' THEN 'rsv'
     WHEN name = 'lx:parainfluenza:positive' THEN 'parainfluenza'
	 WHEN name = 'lx:adenovirus:positive' THEN 'adenovirus'
	 WHEN name = 'lx:rhino_entero_virus:positive' THEN 'rhino_entero_virus'
	 WHEN name = 'lx:coronavirus_non19:positive' THEN 'coronavirus_non19'
	 WHEN name in ('lx:influenza:positive', 'lx:influenza_culture:positive', 'lx:rapid_flu:positive') THEN 'influenza'
	 WHEN name = 'lx:h_metapneumovirus:positive' THEN 'h_metapneumovirus'
	 WHEN name in ('lx:m_pneumoniae_igm:positive', 'lx:m_pneumoniae_pcr:positive') THEN 'm_pneumoniae'
	 WHEN name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive') THEN 'covid19'
ELSE name END as condition
from hef_event
where name in ('lx:rsv:positive',
			   'lx:parainfluenza:positive',
			   'lx:adenovirus:positive',
			   'lx:rhino_entero_virus:positive',
			   'lx:coronavirus_non19:positive',
			   'lx:influenza:positive', 
			   'lx:influenza_culture:positive', 
			   'lx:rapid_flu:positive',
			   'lx:h_metapneumovirus:positive',
			   'lx:m_pneumoniae_igm:positive', 
			   'lx:m_pneumoniae_pcr:positive',
			   'lx:covid19_pcr:positive', 
			   'lx:covid19_ag:positive'
			   )
and date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;


-- Get all encounters that match one of the specified dx codes
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx;
create table public.ravioli_rpt_wk_all_dx AS
select DISTINCT T2.patient_id, T2.date as encounter_date, T3.dx_code, T3.condition
from emr_encounter_dx_codes T1
INNER JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
INNER JOIN public.ravioli_rpt_dx_codes T3 ON (T1.dx_code_id = T3.dx_code)
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and T1.dx_code_id != 'icd9:799.9';

-- Treat pertussis cases as a Ravioli condition
-- Gather all the pertussis cases
DROP TABLE IF EXISTS public.ravioli_rpt_pertussis_cases;
create table public.ravioli_rpt_pertussis_cases AS
select DISTINCT patient_id, date as index_case_date, condition
from nodis_case
where condition = 'pertussis'
and date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;


-- Union together the lab patients and the dx patients and the pertussis patients
-- Exclude ravioli_other condition
-- This is to identify pathogen SPECIFIC patients
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec;
CREATE TABLE public.ravioli_rpt_pathogen_spec AS
select patient_id, condition
from public.ravioli_rpt_pos_tests
UNION
select patient_id, condition
from public.ravioli_rpt_wk_all_dx
where condition != 'ravioli_other'
UNION
select patient_id, condition
from public.ravioli_rpt_pertussis_cases
ORDER BY patient_id;

-- Identify measured fever patients as this is a match for ravioli_other
DROP TABLE IF EXISTS public.ravioli_rpt_meas_fever;
CREATE TABLE public.ravioli_rpt_meas_fever AS
select DISTINCT patient_id, date as encounter_date, 'MEASURED_FEVER' as dx_code, 'ravioli_other' as condition
from hef_event
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and name = 'enc:fever';

-- Incorporate measured fever patients into full dx patient listing
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_and_fever;
CREATE TABLE public.ravioli_rpt_wk_all_dx_and_fever AS
SELECT * from public.ravioli_rpt_wk_all_dx
UNION
SELECT * from public.ravioli_rpt_meas_fever;

-- Identify all "ravioli other" patients
-- Don't count pats in "ravioli other" if already in pathogen specific
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli;
CREATE TABLE public.ravioli_rpt_other_ravioli AS
select DISTINCT patient_id, condition
from public.ravioli_rpt_wk_all_dx_and_fever
where condition = 'ravioli_other'
and patient_id not in (select patient_id from public.ravioli_rpt_pathogen_spec);


-- Create one table with pathogen specific and other patients
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo;
CREATE TABLE public.ravioli_rpt_pathogen_other_combo AS
SELECT patient_id, condition 
FROM public.ravioli_rpt_pathogen_spec
UNION
SELECT patient_id, condition
FROM public.ravioli_rpt_other_ravioli;

-- Identify age group, race, and ethnicity for the patients.
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_w_agegroup;
CREATE TABLE public.ravioli_rpt_pathogen_other_combo_w_agegroup as 
select T1.patient_id, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when date_part('year', age(:week_end_date::date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group,
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'ISLANDER/HAWAIIAN', 'PACIFIC') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC OR LATINO/A' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
condition
FROM public.ravioli_rpt_pathogen_other_combo T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id);

-- Get the patient counts by condition
-- If patient matches multiple pathogens, they will be counted in each category.
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_counts_w_agegroup;
CREATE TABLE public.ravioli_rpt_pathogen_other_combo_counts_w_agegroup AS
select count(distinct(patient_id)) enc_count, sex, age_group, race_group, ethnicity_group, condition
from public.ravioli_rpt_pathogen_other_combo_w_agegroup
group by condition, sex, age_group, race_group, ethnicity_group;

-- Use clinical encounters to count all encounters for all patients for the week.
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_w_agegroup;
CREATE TABLE public.ravioli_rpt_total_encs_w_agegroup AS
SELECT count(distinct(T1.patient_id)) total_encounters,
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group,
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'ISLANDER/HAWAIIAN', 'PACIFIC') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC OR LATINO/A' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group
FROM gen_pop_tools.clin_enc T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
GROUP BY age_group, race_group, ethnicity_group, sex;


-- Use clinical encounters to count all encounters for all patients for with a clinical encounter in the previous 2 years
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_2_yr_w_agegroup;
CREATE TABLE public.ravioli_rpt_total_encs_2_yr_w_agegroup AS
SELECT count(distinct(T1.patient_id)) clin_enc_pats_l2yr,
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group,
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'ISLANDER/HAWAIIAN', 'PACIFIC') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC OR LATINO/A' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group
FROM gen_pop_tools.clin_enc T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE 
date >= (:week_end_date::date - INTERVAL '2 years')::date
and date <= :week_end_date::date
GROUP BY age_group, race_group, ethnicity_group, sex;



-- Add ILI cases to the report. This is standalone from everything else (not related to Ravioli)
DROP TABLE IF EXISTS public.ravioli_rpt_ili_cases;
CREATE TABLE public.ravioli_rpt_ili_cases AS
SELECT DISTINCT patient_id, date as index_test_date, condition,
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,
CASE
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(:week_end_date::date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group,
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'ISLANDER/HAWAIIAN', 'PACIFIC') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC OR LATINO/A' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group
from nodis_case T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
where condition = 'ili'
and date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;


-- Prepare the weekly output.
DROP TABLE IF EXISTS public.ravioli_rpt_single_week_output_w_agegroup;
CREATE TABLE public.ravioli_rpt_single_week_output_w_agegroup AS
SELECT 
:week_end_date::date as week_end_date,
T0.sex,
T0.age_group,
T0.race_group as race,
T0.ethnicity_group as ethnicity,
max(T0.clin_enc_pats_l2yr) as clin_enc_pats_l2yr,
max(T1.total_encounters) as clin_enc_pats_week,
count(distinct(T2.patient_id)) as ravioli_pats_week,
max(case when T4.condition = 'adenovirus' then enc_count else 0 end) as adenovirus_pats,
max(case when T4.condition = 'coronavirus_non19' then enc_count else 0 end) as coronavirus_non19_pats,
max(case when T4.condition = 'covid19' then enc_count else 0 end) as covid19_pats,
max(case when T4.condition = 'h_metapneumovirus' then enc_count else 0 end) as h_metapneumovirus_pats,
max(case when T4.condition = 'influenza' then enc_count else 0 end) as influenza_pats,
max(case when T4.condition = 'm_pneumoniae' then enc_count else 0 end) as m_pneumoniae_pats,
max(case when T4.condition = 'parainfluenza' then enc_count else 0 end) as parainfluenza_pats,
max(case when T4.condition = 'rhino_entero_virus' then enc_count else 0 end) as rhino_entero_pats,
max(case when T4.condition = 'rsv' then enc_count else 0 end) as rsv_pats,
max(case when T4.condition = 'ravioli_other' then enc_count else 0 end) as ravioli_other_pats,
count(distinct(T5.patient_id)) as ili_cases,
max(case when T4.condition = 'pertussis' then enc_count else 0 end) as pertussis_pats
FROM public.ravioli_rpt_total_encs_2_yr_w_agegroup T0
LEFT JOIN public.ravioli_rpt_total_encs_w_agegroup T1 ON (T0.age_group = T1.age_group and T0.race_group = T1.race_group and T0.ethnicity_group = T1.ethnicity_group and T0.sex = T1.sex)
LEFT JOIN public.ravioli_rpt_pathogen_other_combo_w_agegroup T2 ON (T0.age_group=T2.age_group and T0.race_group = T2.race_group and T0.ethnicity_group = T2.ethnicity_group and T0.sex = T2.sex)
--JOIN (select (:week_end_date::date - INTERVAL '6 days')::date as week_start_date) T3 on (1=1)
LEFT JOIN public.ravioli_rpt_pathogen_other_combo_counts_w_agegroup T4 on (T0.age_group = T4.age_group and T0.race_group = T4.race_group and T0.ethnicity_group = T4.ethnicity_group and T0.sex = T4.sex)
LEFT JOIN public.ravioli_rpt_ili_cases T5 on (T0.age_group = T5.age_group and T0.race_group = T5.race_group and T0.ethnicity_group = T5.ethnicity_group and T0.sex = T5.sex)
GROUP BY week_end_date, T0.age_group, T0.race_group, T0.ethnicity_group, T0.sex;



-- Clear out any results for this week from the full table
DELETE FROM public.ravioli_rpt_full_output_w_ageagroup where week_end_date = (select distinct(week_end_date) from public.ravioli_rpt_single_week_output_w_agegroup);

-- Populate the table with the details from this week's run
INSERT INTO public.ravioli_rpt_full_output_w_ageagroup
SELECT * from public.ravioli_rpt_single_week_output_w_agegroup;



DROP TABLE IF EXISTS public.ravioli_rpt_pos_tests;
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec;
DROP TABLE IF EXISTS public.ravioli_rpt_meas_fever;
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_and_fever;
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli;
DROP TABLE IF EXISTS public.ravioli_rpt_pertussis_cases;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_w_agegroup;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_counts_w_agegroup;
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_w_agegroup;
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_2_yr_w_agegroup;
DROP TABLE IF EXISTS public.ravioli_rpt_ili_cases;
DROP TABLE IF EXISTS public.ravioli_rpt_single_week_output_w_agegroup;



--select 'BMC'::text as site, * from public.ravioli_rpt_full_output_w_ageagroup;
--select 'CHA'::text as site, * from public.ravioli_rpt_full_output_w_ageagroup;
--select 'ATR'::text as site, * from public.ravioli_rpt_full_output_w_ageagroup;






