-- -- Table: public.ravioli_rpt_full_output_manu

-- -- DROP TABLE IF EXISTS public.ravioli_rpt_full_output_manu;

-- CREATE TABLE IF NOT EXISTS public.ravioli_rpt_full_output_manu
-- (
    -- week_start_date date NOT NULL,
	-- clin_enc_pats_l2yr bigint,
	-- clin_enc_pats_week bigint,
	-- ravioli_pats_week bigint,
    -- adenovirus_pats bigint,
	-- adenovirus_lx bigint,
	-- adenovirus_dx bigint,
    -- coronavirus_non19_pats bigint,
	-- coronavirus_non19_lx bigint,
	-- coronavirus_non19_dx bigint,
    -- covid19_pats bigint,
	-- covid19_lx bigint,
	-- covid19_dx bigint,
    -- h_metapneumovirus_pats bigint,
	-- h_metapneumovirus_lx bigint,
	-- h_metapneumovirus_dx bigint,
    -- influenza_pats bigint,
	-- influenza_lx bigint,
	-- influenza_dx bigint,
	-- m_pneumoniae_pats bigint,
	-- m_pneumoniae_lx bigint,
	-- m_pneumoniae_dx bigint,
    -- parainfluenza_pats bigint,
	-- parainfluenza_lx bigint,
	-- parainfluenza_dx bigint,
    -- rhino_entero_pats bigint,
	-- rhino_entero_virus_lx bigint,
	-- rhino_entero_virus_dx bigint,
    -- rsv_pats bigint,
	-- rsv_lx bigint,
	-- rsv_dx bigint,
    -- ravioli_other_pats bigint,
	-- ravioli_other_fever bigint,
	-- ravioli_other_dx_wo_fever bigint,
    -- CONSTRAINT ravioli_rpt_full_output_manu_week_start_date_key UNIQUE (week_start_date)
-- )

-- Get all labs for the conditions/pathogens of interest
DROP TABLE IF EXISTS public.ravioli_rpt_pos_tests_manu;
create table public.ravioli_rpt_pos_tests_manu AS
select DISTINCT patient_id, date as index_test_date, 
CASE WHEN name = 'lx:rsv:positive' THEN 'rsv'
     WHEN name = 'lx:parainfluenza:positive' THEN 'parainfluenza'
	 WHEN name = 'lx:adenovirus:positive' THEN 'adenovirus'
	 WHEN name = 'lx:rhino_entero_virus:positive' THEN 'rhino_entero_virus'
	 WHEN name = 'lx:coronavirus_non19:positive' THEN 'coronavirus_non19'
	 WHEN name in ('lx:influenza:positive', 'lx:influenza_culture:positive', 'lx:rapid_flu:positive') THEN 'influenza'
	 WHEN name = 'lx:h_metapneumovirus:positive' THEN 'h_metapneumovirus'
	 WHEN name in ('lx:m_pneumoniae_igm:positive', 'lx:m_pneumoniae_pcr:positive') THEN 'm_pneumoniae'
	 WHEN name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive') THEN 'covid19'
ELSE name END as condition
from hef_event
where name in ('lx:rsv:positive',
			   'lx:parainfluenza:positive',
			   'lx:adenovirus:positive',
			   'lx:rhino_entero_virus:positive',
			   'lx:coronavirus_non19:positive',
			   'lx:influenza:positive', 
			   'lx:influenza_culture:positive', 
			   'lx:rapid_flu:positive',
			   'lx:h_metapneumovirus:positive',
			   'lx:m_pneumoniae_igm:positive', 
			   'lx:m_pneumoniae_pcr:positive',
			   'lx:covid19_pcr:positive', 
			   'lx:covid19_ag:positive'
			   )
and date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;


-- Get all encounters that match one of the specified dx codes
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_manu;
create table public.ravioli_rpt_wk_all_dx_manu AS
select DISTINCT T2.patient_id, T2.date as encounter_date, T3.dx_code, T3.condition
from emr_encounter_dx_codes T1
INNER JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
INNER JOIN public.ravioli_rpt_dx_codes T3 ON (T1.dx_code_id = T3.dx_code)
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and T1.dx_code_id != 'icd9:799.9';

-- Union together the lab patients and the dx patients
-- Exclude ravioli_other condition
-- This is to identify pathogen SPECIFIC patients
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_all_manu;
CREATE TABLE public.ravioli_rpt_pathogen_spec_all_manu AS
select patient_id, condition
from public.ravioli_rpt_pos_tests_manu
UNION
select patient_id, condition
from public.ravioli_rpt_wk_all_dx_manu
where condition != 'ravioli_other'
ORDER BY patient_id;


-- RAVIOLI MEASURED FEVER Identify measured fever patients as this is a match for ravioli_other
DROP TABLE IF EXISTS public.ravioli_rpt_meas_fever_manu;
CREATE TABLE public.ravioli_rpt_meas_fever_manu AS
select DISTINCT patient_id, date as encounter_date, 'MEASURED_FEVER' as dx_code, 'ravioli_other' as condition
from hef_event
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date
and name = 'enc:fever';

-- Incorporate measured fever patients into full dx patient listing
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_and_fever_manu;
CREATE TABLE public.ravioli_rpt_wk_all_dx_and_fever_manu AS
SELECT * from public.ravioli_rpt_wk_all_dx_manu
UNION
SELECT * from public.ravioli_rpt_meas_fever_manu;


-- Identify all "ravioli other" patients
-- Don't count pats in "ravioli other" if already in pathogen specific
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_manu;
CREATE TABLE public.ravioli_rpt_other_ravioli_manu AS
select DISTINCT patient_id, condition
from public.ravioli_rpt_wk_all_dx_and_fever_manu
where condition = 'ravioli_other'
and patient_id not in (select patient_id from public.ravioli_rpt_pathogen_spec_all_manu);

-- Create one table with pathogen specific and other patients
-- USE THIS FOR THE STANDARD REPORT COUNTS
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_manu;
CREATE TABLE public.ravioli_rpt_pathogen_other_combo_manu AS
SELECT patient_id, condition 
FROM public.ravioli_rpt_pathogen_spec_all_manu
UNION
SELECT patient_id, condition
FROM public.ravioli_rpt_other_ravioli_manu;


-- Get the patient counts by condition
-- If patient matches multiple pathogens, they will be counted in each category.
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_counts_manu;
CREATE TABLE public.ravioli_rpt_pathogen_other_combo_counts_manu AS
select count(distinct(patient_id)) enc_count, condition
from public.ravioli_rpt_pathogen_other_combo_manu
group by condition;


-- PATHOGEN SPECIFIC LX 
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_lx_manu;
CREATE TABLE public.ravioli_rpt_pathogen_spec_lx_manu AS
SELECT count(distinct(patient_id)) as lx_count, condition
FROM public.ravioli_rpt_pos_tests_manu
GROUP BY condition;

-- PATHOGEN SPECIFIC DX
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_dx_manu;
CREATE TABLE public.ravioli_rpt_pathogen_spec_dx_manu AS
SELECT count(DISTINCT(patient_id)) as dx_count, condition
FROM public.ravioli_rpt_wk_all_dx_manu
where condition != 'ravioli_other'
GROUP BY condition;

-- RAVIOLI_OTHER FEVER MEASURED & FEVER DX
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_all_fever_manu;
CREATE TABLE public.ravioli_rpt_other_ravioli_all_fever_manu AS
SELECT count(DISTINCT(patient_id)) as fever_count, condition
from public.ravioli_rpt_wk_all_dx_and_fever_manu
where condition = 'ravioli_other'
and dx_code in ('icd10:R50.81', 'MEASURED_FEVER')
and patient_id not in (select patient_id from public.ravioli_rpt_pathogen_spec_all_manu)
GROUP BY condition;

-- RAVIOLI_OTHER DX ONLY
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_all_dx_manu;
CREATE TABLE public.ravioli_rpt_other_ravioli_all_dx_manu AS
SELECT count(DISTINCT(patient_id)) as no_fever_dx_count, condition
from public.ravioli_rpt_wk_all_dx_and_fever_manu
where condition = 'ravioli_other'
and dx_code not in ('icd10:R50.81', 'MEASURED_FEVER')
and patient_id not in (select patient_id from public.ravioli_rpt_pathogen_spec_all_manu)
GROUP BY condition;



DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_manu;
CREATE TABLE public.ravioli_rpt_total_encs_manu AS
SELECT count(distinct(T1.patient_id)) total_encounters
FROM gen_pop_tools.clin_enc T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE 
date >= (:week_end_date::date - INTERVAL '6 days')::date
and date <= :week_end_date::date;

-- Use clinical encounters to count all encounters for all patients for with a clinical encounter in the previous 2 years
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_2_yr_manu;
CREATE TABLE public.ravioli_rpt_total_encs_2_yr_manu AS
SELECT count(distinct(T1.patient_id)) clin_enc_pats_l2yr
FROM gen_pop_tools.clin_enc T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE 
date >= (:week_end_date::date - INTERVAL '2 years')::date
and date <= :week_end_date::date;


-- PATHOGEN SPECIFIC LX AND DX COUNTS
-- RAVIOLI OTHER FEVER AND NON-FEVER
DROP TABLE IF EXISTS ravioli_rpt_single_week_output_manu;
CREATE TABLE ravioli_rpt_single_week_output_manu AS
SELECT
week_start_date,
max(T9.clin_enc_pats_l2yr) as clin_enc_pats_l2yr,
max(T8.total_encounters) as clin_enc_pats_week,
count(distinct(T5.patient_id)) as ravioli_pats_week,
max(case when T6.condition = 'adenovirus' then enc_count else 0 end) as adenovirus_pats,
max(case when T1.condition = 'adenovirus' then lx_count else 0 end) as adenovirus_lx,
max(case when T2.condition = 'adenovirus' then dx_count else 0 end) as adenovirus_dx,
max(case when T6.condition = 'coronavirus_non19' then enc_count else 0 end) as coronavirus_non19_pats,
max(case when T1.condition = 'coronavirus_non19' then lx_count else 0 end) as coronavirus_non19_lx,
max(case when T2.condition = 'coronavirus_non19' then dx_count else 0 end) as coronavirus_non19_dx,
max(case when T6.condition = 'covid19' then enc_count else 0 end) as covid19_pats,
max(case when T1.condition = 'covid19' then lx_count else 0 end) as covid19_lx,
max(case when T2.condition = 'covid19' then dx_count else 0 end) as covid19_dx,
max(case when T6.condition = 'h_metapneumovirus' then enc_count else 0 end) as h_metapneumovirus_pats,
max(case when T1.condition = 'h_metapneumovirus' then lx_count else 0 end) as h_metapneumovirus_lx,
max(case when T2.condition = 'h_metapneumovirus' then dx_count else 0 end) as h_metapneumovirus_dx,
max(case when T6.condition = 'influenza' then enc_count else 0 end) as influenza_pats,
max(case when T1.condition = 'influenza' then lx_count else 0 end) as influenza_lx,
max(case when T2.condition = 'influenza' then dx_count else 0 end) as influenza_dx,
max(case when T6.condition = 'm_pneumoniae' then enc_count else 0 end) as m_pneumoniae_pats,
max(case when T1.condition = 'm_pneumoniae' then lx_count else 0 end) as m_pneumoniae_lx,
max(case when T2.condition = 'm_pneumoniae' then dx_count else 0 end) as m_pneumoniae_dx,
max(case when T6.condition = 'parainfluenza' then enc_count else 0 end) as parainfluenza_pats,
max(case when T1.condition = 'parainfluenza' then lx_count else 0 end) as parainfluenza_lx,
max(case when T2.condition = 'parainfluenza' then dx_count else 0 end) as parainfluenza_dx,
max(case when T6.condition = 'rhino_entero_virus' then enc_count else 0 end) as rhino_entero_pats,
max(case when T1.condition = 'rhino_entero_virus' then lx_count else 0 end) as rhino_entero_virus_lx,
max(case when T2.condition = 'rhino_entero_virus' then dx_count else 0 end) as rhino_entero_virus_dx,
max(case when T6.condition = 'rsv' then enc_count else 0 end) as rsv_pats,
max(case when T1.condition = 'rsv' then lx_count else 0 end) as rsv_lx,
max(case when T2.condition = 'rsv' then dx_count else 0 end) as rsv_dx,
max(case when T6.condition = 'ravioli_other' then enc_count else 0 end) as ravioli_other_pats,
max(case when T3.condition = 'ravioli_other' then  fever_count else 0 end) as ravioli_other_fever,
max(case when T4.condition = 'ravioli_other' then no_fever_dx_count else 0 end) as ravioli_other_dx_wo_fever
FROM public.ravioli_rpt_total_encs_2_yr_manu T9 
LEFT JOIN public.ravioli_rpt_pathogen_spec_lx_manu T1 ON (1=1)
LEFT JOIN public.ravioli_rpt_pathogen_spec_dx_manu T2 ON (1=1)
LEFT JOIN public.ravioli_rpt_other_ravioli_all_fever_manu T3 ON (1=1)
LEFT JOIN public.ravioli_rpt_other_ravioli_all_dx_manu T4 ON (1=1)
LEFT JOIN public.ravioli_rpt_pathogen_other_combo_manu T5 ON (1=1) 
LEFT JOIN public.ravioli_rpt_pathogen_other_combo_counts_manu T6 ON (1=1)
LEFT JOIN (select (:week_end_date::date - INTERVAL '6 days')::date as week_start_date) T7 on (1=1)
LEFT JOIN public.ravioli_rpt_total_encs_manu T8 ON (1=1)
GROUP BY week_start_date;

-- Clear out any results for this week from the full table
DELETE FROM public.ravioli_rpt_full_output_manu where week_start_date = (select distinct(week_start_date) from public.ravioli_rpt_single_week_output_manu);

-- Populate the table with the details from this week's run
INSERT INTO public.ravioli_rpt_full_output_manu
SELECT * from public.ravioli_rpt_single_week_output_manu;

DROP TABLE IF EXISTS public.ravioli_rpt_pos_tests_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_all_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_meas_fever_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_wk_all_dx_and_fever_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_other_combo_counts_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_lx_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_pathogen_spec_dx_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_all_fever_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_other_ravioli_all_dx_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_total_encs_2_yr_manu;
DROP TABLE IF EXISTS public.ravioli_rpt_single_week_output_manu;



