-- check for unmapped races for RAVIOLI report
select distinct upper(race) as unmapped_value, 'race_or_race_rpt_1' as target
from emr_patient p
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='race'
	and mapped_value in ('ASIAN', 'CAUCASIAN', 'BLACK', 'HISPANIC')
    and cm.src_value=upper(p.race))
and not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='race_rpt_1'
	and mapped_value in ('AMERICAN INDIAN/ALASKAN NATIVE', 'DECLINED TO ANSWER', 'OTHER', 'PACIFIC ISLANDER/HAWAIIAN', 'UNKNOWN')
    and cm.src_value=upper(p.race));
	
-- check for unmapped ethnicity for RAVIOLI report

select distinct upper(ethnicity) as unmapped_value, 'ethnicity_or_ethnicity_rpt_1' as target
from emr_patient p
where not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='ethnicity'
	and mapped_value in ('HISPANIC', 'NOT HISPANIC')
    and cm.src_value=upper(p.ethnicity))
and not exists (
  select null from gen_pop_tools.rs_conf_mapping cm 
  where cm.src_table='emr_patient'
    and cm.src_field='ethnicity_rpt_1'
	and mapped_value in ('DECLINED TO ANSWER', 'UNKNOWN')
    and cm.src_value=upper(p.ethnicity));

	

select patient_id, race,
CASE
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'UNKNOWN') or race is null then 'UNKNOWN'
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'AMERICAN INDIAN/ALASKAN NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'BLACK') then 'BLACK' 
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'ASIAN') then 'ASIAN' 
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'PACIFIC ISLANDER/HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'CAUCASIAN') then 'WHITE' 
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'HISPANIC') then 'HISPANIC' 
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'DECLINED TO ANSWER') then 'DECLINED TO ANSWER'
when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'OTHER') then 'OTHER'
ELSE race END race_group
FROM public.ravioli_rpt_pathogen_other_combo T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id);