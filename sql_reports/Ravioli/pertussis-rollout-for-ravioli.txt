-- install plug-in
. bin/activate
pip3.6 install -e git+https://gitlab.com/ESP-Project/plugins/esp-plugin_pertussis.git@v1.7#egg=esp-plugin-pertussis

-- HEF EVENTS
cp all_hef.txt all_hef.txt.OLD
/srv/esp/prod/bin/esp hef --list > all_hef.txt

Add these to queued conditions if that is being used
diagnosis:pertussis
diagnosis:whooping_cough
labresult:pertussis_culture:any-result
labresult:pertussis_culture:positive
labresult:pertussis_pcr:any-result
labresult:pertussis_pcr:positive
labresult:pertussis_serology:any-result
labresult:pertussis_serology:positive
prescription:pertussis_med

-- RESTART HTTPD
sudo service httpd restart

-- RUN CONCORDANCE
nohup /srv/esp/prod/bin/esp concordance &

-- FIND IGNORED LABS

select count(*), T1.native_code, native_name, procedure_name, max(date) most_recent
from emr_labresult T1 
JOIN conf_ignoredcode T2 ON (T1.native_code = T2.native_code)
WHERE native_name ilike '%pert%'
AND date >= '2015-10-01'
GROUP BY T1.native_code, native_name, procedure_name
ORDER BY native_name


select count(*), T1.native_code, native_name, procedure_name, max(date) most_recent
from emr_labresult T1 
JOIN conf_ignoredcode T2 ON (T1.native_code = T2.native_code)
WHERE native_name ilike '%bord%'
AND date >= '2015-10-01'
GROUP BY T1.native_code, native_name, procedure_name
ORDER BY native_name

===========================================
-- MAP FOUND LABS
 Use UI to do this
 
====================================
-- CONFIRM MEDS
Should return 22

select * from static_drugsynonym
where generic_name in ('Erythromycin', 'Clarithromycin', 'Azithromycin', 'Trimethoprim-sulfamethoxazole')
order by generic_name

=========================================

 vi pertussis_hef.txt
 -- add the hef events
  
 -- create one off script
 vi run_pertussis_hef.sh
 
#!/bin/bash


( while read line
    do
    sleep 5
    sqlProc=$(psql esp -c "select count(*) from pg_stat_activity where state<>'idle' and datname='esp'" -t | tr -d ' \n')
    while [ "$sqlProc" -ge "15" ]; do
           sleep 5
           sqlProc=$(psql esp -c "select count(*) from pg_stat_activity where state<>'idle' and datname='esp'" -t | tr -d ' \n')
    done
    echo 'Now running HEF heuristic:'
    echo $line
    ( /srv/esp/prod/bin/esp hef "$line" ) &
    done < /srv/esp/scripts/pertussis_hef.txt
    wait )
	
	
chmod 755 run_pertussis_hef.sh
nohup ./run_pertussis_hef.sh & 
 
==========================================================
RUN NODIS
cd /srv/esp/prod/
nohup ./bin/esp nodis pertussis & 
 
-- ADD pertussis TO DAILY BATCH