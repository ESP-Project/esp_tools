-- Table: kre_report.ravioli_rpt_full_output_combo

-- DROP TABLE kre_report.ravioli_rpt_full_output_combo;

CREATE TABLE kre_report.ravioli_rpt_full_output_combo
(
    site character varying(10) COLLATE pg_catalog."default" NOT NULL,
    week_start_date date NOT NULL,
    total_encs bigint,
    total_ravioli_encs bigint,
    adenovirus_encs bigint,
    c_pneumoniae_encs bigint,
    coronavirus_non19_encs bigint,
    covid19_encs bigint,
    h_metapneumovirus_encs bigint,
    influenza_encs bigint,
    m_pneumoniae_encs bigint,
    parainfluenza_encs bigint,
    parapertussis_encs bigint,
    rhino_entero_encs bigint,
    rsv_encs bigint,
    ravioli_other_encs bigint,
	ili_cases bigint
)
	
	
-- Table: kre_report.ravioli_rpt_full_output_combo

-- DROP TABLE kre_report.ravioli_rpt_full_output_combo;

CREATE TABLE kre_report.ravioli_rpt_full_output_agegroup_combo
(
    site character varying(10) COLLATE pg_catalog."default" NOT NULL,
    week_start_date date NOT NULL,
	age_group character varying(20) COLLATE pg_catalog."default" NOT NULL,
    total_encs bigint,
    total_ravioli_encs bigint,
    adenovirus_encs bigint,
    c_pneumoniae_encs bigint,
    coronavirus_non19_encs bigint,
    covid19_encs bigint,
    h_metapneumovirus_encs bigint,
    influenza_encs bigint,
    m_pneumoniae_encs bigint,
    parainfluenza_encs bigint,
    parapertussis_encs bigint,
    rhino_entero_encs bigint,
    rsv_encs bigint,
    ravioli_other_encs bigint,
	ili_cases bigint
)




--delete from kre_report.ravioli_rpt_full_output_combo

-- manually load the files into the table

-- select 'CHA' as site, *  FROM kre_report.ravioli_rpt_full_output ORDER BY week_start_date;
-- select 'BMC' as site, *  FROM kre_report.ravioli_rpt_full_output ORDER BY week_start_date;
-- select 'ATR' as site, *  FROM kre_report.ravioli_rpt_full_output ORDER BY week_start_date;

select week_start_date, 
sum(total_encs) as total_encs_all,
sum(total_ravioli_encs) as total_ravioli_encs_all,
sum(adenovirus_encs) as adenovirus_encs_all,
sum(c_pneumoniae_encs) as c_pneumoniae_encs_all,
sum(coronavirus_non19_encs) as coronavirus_non19_encs_all,
sum(covid19_encs) as covid19_encs_all,
sum(h_metapneumovirus_encs) as h_metapneumovirus_encs_all,
sum(influenza_encs) as influenza_encs_all,
sum(m_pneumoniae_encs) as m_pneumoniae_encs_all,
sum(parainfluenza_encs) as parainfluenza_encs_all,
sum(parapertussis_encs) as parapertussis_encs_all,
sum(rhino_entero_encs) as rhino_entero_encs_all,
sum(rsv_encs) as rsv_encs_all,
sum(ravioli_other_encs) as ravioli_other_encs_all
sum(ili_cases) as ili_cases_all
from kre_report.ravioli_rpt_full_output_combo
group by week_start_date
order by week_start_date;

--- 
--- AGE GROUP
---


delete from kre_report.ravioli_rpt_full_output_agegroup_combo;

--select 'BMC'::text as site, * from kre_report.ravioli_rpt_full_output_w_ageagroup;
--select 'CHA'::text as site, * from kre_report.ravioli_rpt_full_output_w_ageagroup;
--select 'ATR'::text as site, * from kre_report.ravioli_rpt_full_output_w_ageagroup;


select week_start_date, 
age_group,
sum(total_encs) as total_encs_all,
sum(total_ravioli_encs) as total_ravioli_encs_all,
sum(adenovirus_encs) as adenovirus_encs_all,
sum(c_pneumoniae_encs) as c_pneumoniae_encs_all,
sum(coronavirus_non19_encs) as coronavirus_non19_encs_all,
sum(covid19_encs) as covid19_encs_all,
sum(h_metapneumovirus_encs) as h_metapneumovirus_encs_all,
sum(influenza_encs) as influenza_encs_all,
sum(m_pneumoniae_encs) as m_pneumoniae_encs_all,
sum(parainfluenza_encs) as parainfluenza_encs_all,
sum(parapertussis_encs) as parapertussis_encs_all,
sum(rhino_entero_encs) as rhino_entero_encs_all,
sum(rsv_encs) as rsv_encs_all,
sum(ravioli_other_encs) as ravioli_other_encs_all,
sum(ili_cases) as ili_cases_all
from kre_report.ravioli_rpt_full_output_agegroup_combo
group by week_start_date, age_group
order by week_start_date, age_group;

