update static_dx_code set name = 'Cough, unspecified', longname = 'Cough, unspecified'
where combotypecode = 'icd10:R05.9'
and name ilike '%added by load_epic.py%';


update static_dx_code set name = 'Dependence on other enabling machines and devices', longname = 'Dependence on other enabling machines and devices' 
where combotypecode = 'icd10:Z99.89' 
and name ilike '%added by load_epic.py%';





update static_dx_code set name = 'Contact with and (suspected) exposure to COVID-19', longname = 'Contact with and (suspected) exposure to COVID-19'
where combotypecode = 'icd10:Z20.822'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Acute respiratory distress', longname = 'Acute respiratory distress'
where combotypecode = 'icd10:R06.03'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Encounter for prophylactic fluoride administration', longname = 'Encounter for prophylactic fluoride administration'
where combotypecode = 'icd10:Z29.3'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Other specified disorders of eye and adnexa', longname = 'Other specified disorders of eye and adnexa'
where combotypecode = 'icd10:H57.89'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Cough', longname = 'Cough'
where combotypecode = 'icd10:R05.8'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Encounter for screening for COVID-19', longname = 'Encounter for screening for COVID-19'
where combotypecode = 'icd10:Z11.52'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Encounter for screening for global developmental delays (milestones)', longname = 'Encounter for screening for global developmental delays (milestones)'
where combotypecode = 'icd10:Z13.42'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Malabsorption due to intolerance, not elsewhere classified', longname = 'Malabsorption due to intolerance, not elsewhere classified'
where combotypecode = 'icd10:K90.49'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Chronic cough', longname = 'Chronic cough'
where combotypecode = 'icd10:R05.3'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Bilateral inguinal testes', longname = 'Bilateral inguinal testes'
where combotypecode = 'icd10:Q53.212'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Gastro-esophageal reflux disease with esophagitis, without bleeding', longname = 'Gastro-esophageal reflux disease with esophagitis, without bleeding'
where combotypecode = 'icd10:K21.00'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Abnormal findings on diagnostic imaging of other specified body structures', longname = 'Abnormal findings on diagnostic imaging of other specified body structures'
where combotypecode = 'icd10:R93.89'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Infection and inflammatory reaction due to indwelling urethral catheter, sequela', longname = 'Infection and inflammatory reaction due to indwelling urethral catheter, sequela'
where combotypecode = 'icd10:T83.511S'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Encounter for screening examination for mental health and behavioral disorders, unspecified', longname = 'Encounter for screening examination for mental health and behavioral disorders, unspecified'
where combotypecode = 'icd10:Z13.30'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Other feeding difficulties', longname = 'Other feeding difficulties'
where combotypecode = 'icd10:R63.39'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Other allergic and dietetic gastroenteritis and colitis', longname = 'Other allergic and dietetic gastroenteritis and colitis'
where combotypecode = 'icd10:K52.29'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Myocardial infarction type 2', longname = 'Myocardial infarction type 2'
where combotypecode = 'icd10:I21.A1'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Pure hypercholesterolemia, unspecified', longname = 'Pure hypercholesterolemia, unspecified'
where combotypecode = 'icd10:E78.00'
and name ilike '%added by load_epic.py%';

update static_dx_code set name = 'Prediabetes', longname = 'Prediabetes'
where combotypecode = 'icd10:R73.03'
and name ilike '%added by load_epic.py%';



