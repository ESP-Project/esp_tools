insert into emr_immunization
   (natural_key,created_timestamp,updated_timestamp,date, 
	mrn,
	imm_type, name, 
	patient_id,provenance_id,provider_id)
select distinct i.natural_key,now(),now(),date::date,p.mrn,
	imm_type,name,
    p.id, 1, 1
from temp_imm i
join emr_patient p on i.patient_natural_key=p.natural_key
where nullif(trim(date),'') is not null;
