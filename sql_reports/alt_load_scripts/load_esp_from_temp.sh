#!/bin/bash
#run this after the quarterly extract is done
DATE=$(date +%Y-%m-%d)
LOGFILE=/srv/esp/logs/load_esp_from_temp.log.$DATE
exec 5>&1 6>&2 >>$LOGFILE.$DATE 2>&1

psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f '/srv/esp/scripts/alt_load_scripts/load_patient.pg.sql'
/bin/bash /srv/esp/scripts/alt_load_scripts/load_encounter.sh  
/bin/bash /srv/esp/scripts/alt_load_scripts/load_enc_dx_codes.sh  
/bin/bash /srv/esp/scripts/alt_load_scripts/load_imm.sh  
/bin/bash /srv/esp/scripts/alt_load_scripts/load_labresult.sh  
/bin/bash /srv/esp/scripts/alt_load_scripts/load_prescription.sh  
/bin/bash /srv/esp/scripts/alt_load_scripts/load_sochist.sh

exec 1>&5 2>&6

