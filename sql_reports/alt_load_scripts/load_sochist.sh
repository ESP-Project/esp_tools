#!/bin/bash
d="2018-04-01"
until [[ $d > 2022-03-01 ]]; do
  echo "$d"
  mstart=$(date -d "$d" +%Y%m%d)
  mend=$(date -d "$d + 1 month - 1 day" +%Y%m%d)
  psql -f '/srv/esp/scripts/load_scripts/load_sochist.pg.sql' -v startdt=$mstart -v enddt=$mend
  d=$(date -I -d "$d + 1 month")
done
