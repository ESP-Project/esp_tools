insert into emr_encounter (natural_key, created_timestamp, updated_timestamp,
  date, mrn, raw_encounter_type, 
  weight, height, bp_systolic, bp_diastolic, bmi,
  raw_weight, raw_height, raw_bp_systolic, raw_bp_diastolic, raw_bmi,
  patient_id, provenance_id, provider_id, primary_payer, priority, pregnant)
select te.natural_key, now(), now(), te.date::date, te.mrn, raw_encounter_type, 
  gen_pop_tools.weightkm_fromlboz(raw_weight) as weight,
  gen_pop_tools.heightcm_fromftin(raw_height) as height,
	nullif(trim(raw_bp_systolic),'')::decimal, 
	nullif(trim(raw_bp_diastolic),'')::decimal,
	Coalesce(nullif(trim(raw_bmi),'')::decimal, 
	gen_pop_tools.bmi_fromhtwt(
	  gen_pop_tools.heightcm_fromftin(raw_height), 
	  gen_pop_tools.weightkm_fromlboz(raw_weight))) as bmi,
	raw_weight, raw_height, raw_bp_systolic, raw_bp_diastolic, raw_bmi, 
	p.id as patient_id, 1, 1, primary_payer, '', False
from temp_enc te join emr_patient p on p.natural_key=te.patient_natural_key
where date between :'startdt' and :'enddt';
