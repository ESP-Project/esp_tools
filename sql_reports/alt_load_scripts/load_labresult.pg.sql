insert into emr_labresult (natural_key,created_timestamp,updated_timestamp,date, 
	mrn,native_code,native_name,result_date,cresult_date,collection_date, 
	ccollection_date,ref_high_string,ref_low_string,ref_high_float, ref_low_float,
    abnormal_flag,result_float,result_string,specimen_source,patient_id,provenance_id,
    provider_id)
select distinct r.natural_key, now(), now(), r.date::date, r.mrn, '--'||native_code,
  native_name, result_date::timestamp, result_date, 
  to_timestamp(collection_date,'yyyymmddhh24miss'), collection_date, 
  ref_high_string, ref_low_string, 
  case when ref_high_string ~ '^[+-]?([0-9]+(\.[0-9]+)?|\.[0-9]+)$' then ref_high_string::decimal
       else null
  end ref_high_float,
  case when ref_low_string ~ '^[+-]?([0-9]+(\.[0-9]+)?|\.[0-9]+)$' then ref_low_string::decimal
       else null
  end ref_low_float,
  abnormal_flag, 
  case when result_string ~ '^[+-]?([0-9]+(\.[0-9]+)?|\.[0-9]+)$' then result_string::decimal
       else null
  end result_float, result_string, specimen_source, p.id, 1, 1
from temp_res r
join emr_patient p on r.patient_natural_key=p.natural_key
where r.date between :'startdt' and :'enddt';
