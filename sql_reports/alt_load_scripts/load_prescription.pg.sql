insert into emr_prescription 
   (natural_key,created_timestamp,updated_timestamp,date, 
	mrn,order_natural_key,name,dose,quantity_float,quantity,refills,
	route,start_date,end_date,patient_id,provenance_id,provider_id)
select distinct order_natural_key||'-'||date, now(),now(),date::date,
    m.mrn,order_natural_key, name, dose,
	case when quantity ~ '^[+-]?([0-9]+(\.[0-9]+)?|\.[0-9]+)$' then quantity::decimal
       else null
    end quantity_float, quantity, refills, route,
	nullif(trim(start_date),'')::date, nullif(trim(end_date),'')::date,
    p.id, 1, 1
from temp_med m
join emr_patient p on m.patient_natural_key=p.natural_key
where m.date between :'startdt' and :'enddt';
