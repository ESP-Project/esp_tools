

WHERE typ.NAME in ('Apppointment','Office Visit','Telemedicine','TeleVideo Encounter','Clinical Support')
	and enc.appt_prc_id in ('114','115','1003','1005','1006','434','4917','315','1036','218','435','12014','436') --NEW PATIENT, PHYSICAL, WELL CHILD, SAME DAY, PRENATAL FOLLOW UP, PRENATAL NEW PATIENT, POSTPARTUM, GROUP, ESTABLISHED HOME VISIT, ESTABLISHED PATIENT, ESTABLISHED PATIENT LONG, ESTABLISHED PATIENT SHORT, EST TELEMEDICINE - DHH
	and appt_stat.NAME in ('Arrived','Completed')
	
	
and enc.DEPARTMENT_ID in (
		SELECT GRP_REC_LIST_VALUE AS DEP_ID
		FROM GROUPER_RECORDS
		WHERE GROUPER_ID = '121887')  

differerent grouper id from DotHouse and Codman but would need to research for BMC
		
	
	
	encounter type
	visit type
	appt status
	depts considered primary care



The short answer is I have not found a consistent way to find all “Codman patients” or “DotHouse Patients”.  There are several ways you could define primary site (PCP, Last Encounter Dept, etc.), and the numbers will change depending on which you use.   I recently heard from one of our analysts that there has been a push to use the “Primary Location” field within Demographics in Epic for this purpose, but I don’t know how widely it is being used yet.  The query below pulls the primary location for patients seen in the past three years. 

 

with LOC_CSN as (

   select enc.PAT_ID, MAX(enc.PAT_ENC_CSN_ID) as CSN

   from PAT_ENC enc

   where enc.PRIMARY_LOC_ID is not null

   and enc.CONTACT_DATE >= GETDATE()-(3*365)

   group by enc.PAT_ID

),

 

PRIM_LOC as (

   select LOC_CSN.PAT_ID

          , convert(date,enc.CONTACT_DATE) as "Prim Loc Date"

         , loc.LOC_NAME as "Primary Location (EPT-5615)"

         , loc.SERV_AREA_ID as "Primary SA ID"

         , sa.SERV_AREA_NAME as "Primary Service Area"

   from LOC_CSN

   inner join PAT_ENC enc on LOC_CSN.CSN = enc.PAT_ENC_CSN_ID

   inner join CLARITY_LOC loc on enc.PRIMARY_LOC_ID = loc.LOC_ID

   inner join CLARITY_SA sa on loc.SERV_AREA_ID = sa.SERV_AREA_ID

)

 

SELECT *

FROM PRIM_LOC


SELECT enc.PAT_ID, enc.PRIMARY_LOC_ID, loc.LOC_NAME, loc.SERV_AREA_ID, sa.SERV_AREA_NAME
FROM PAT_ENC enc
LEFT JOIN CLARITY_LOC loc on enc.PRIMARY_LOC_ID = loc.LOC_ID
LEFT JOIN CLARITY_SA sa on loc.SERV_AREA_ID = sa.SERV_AREA_ID
WHERE enc.PAT_ENC_CSN_ID = '1011322233';

SELECT count(*), enc.PRIMARY_LOC_ID, loc.LOC_NAME, loc.SERV_AREA_ID, sa.SERV_AREA_NAME
FROM PAT_ENC enc
LEFT JOIN CLARITY_LOC loc on enc.PRIMARY_LOC_ID = loc.LOC_ID
LEFT JOIN CLARITY_SA sa on loc.SERV_AREA_ID = sa.SERV_AREA_ID
WHERE contact_date >= '2024-09-01' 
GROUP BY enc.PRIMARY_LOC_ID, loc.LOC_NAME, loc.SERV_AREA_ID, sa.SERV_AREA_NAME;

+--------------------+------------------+------------------------------------+----------------+--------------------+
| (No column name)   | PRIMARY_LOC_ID   | LOC_NAME                           | SERV_AREA_ID   | SERV_AREA_NAME     |
|--------------------+------------------+------------------------------------+----------------+--------------------|
| 12                 | 1002             | SO PEDIATRICS                      | 2              | CHA - SERVICE AREA |
| 29038              | 2022             | CA FAMILY NORTH                    | 2              | CHA - SERVICE AREA |
| 17924              | 2026             | CA ELDER SVC PLAN                  | 2              | CHA - SERVICE AREA |
| 24                 | 1005             | SO HOSPITAL PRIMARY CARE           | 2              | CHA - SERVICE AREA |
| 17162              | 2013             | CA EAST HC                         | 2              | CHA - SERVICE AREA |
| 26169              | 4001             | RE HEALTH CENTER                   | 2              | CHA - SERVICE AREA |
| 872                | 2029             | CA HOME                            | 2              | CHA - SERVICE AREA |
| 1                  | 2507             | CH NURSERY                         | 2              | CHA - SERVICE AREA |
| 30715              | 1016             | SO UNION SQUARE                    | 2              | CHA - SERVICE AREA |
| 264                | 2033             | CA NURSING HOMES                   | 2              | CHA - SERVICE AREA |
| 3939               | 2010             | CA ZINBERG CLINIC                  | 2              | CHA - SERVICE AREA |
| 47255              | 1001             | SO ADULT MEDICINE                  | 2              | CHA - SERVICE AREA |
| 8150               | 1082             | SO HOSPITAL PEDIATRICS             | 2              | CHA - SERVICE AREA |
| 21448              | 3016             | EV HEALTH CENTER                   | 2              | CHA - SERVICE AREA |
| 32                 | 3500             | Everett Hospital                   | 2              | CHA - SERVICE AREA |
| 40321              | 2                | CHA - SERVICE AREA                 | 2              | CHA - SERVICE AREA |
| 173                | 11               | NULL                               | NULL           | NULL               |
| 6                  | 3014             | EV TEEN CENTER                     | 2              | CHA - SERVICE AREA |
| 34704              | 5002             | MALDEN FAMILY MEDICINE CENTER EAST | 2              | CHA - SERVICE AREA |
| 2                  | 2501             | CH EMERGENCY                       | 2              | CHA - SERVICE AREA |
| 5                  | 1007             | SO TEEN CENTER                     | 2              | CHA - SERVICE AREA |
| 16                 | 1502             | SH EMERGENCY                       | 2              | CHA - SERVICE AREA |
| 31367              | 5011             | MALDEN FAMILY MEDICINE CENTER WEST | 2              | CHA - SERVICE AREA |
| 1244               | NULL             | NULL                               | NULL           | NULL               |
| 33745              | 2003             | CA PRIMARY CARE                    | 2              | CHA - SERVICE AREA |
| 438                | 2500             | Cambridge Hospital                 | 2              | CHA - SERVICE AREA |
| 33427              | 2015             | CA WINDSOR HC                      | 2              | CHA - SERVICE AREA |
| 7078               | 2004             | CA PEDIATRICS                      | 2              | CHA - SERVICE AREA |
| 1013               | 2018             | CA FAMILY HEALTH                   | 2              | CHA - SERVICE AREA |
+--------------------+------------------+------------------------------------+----------------+--------------------+

ADD location ids to provider feed


SELECT enc.PAT_ID, enc.PRIMARY_LOC_ID, loc.LOC_NAME, loc.SERV_AREA_ID, sa.SERV_AREA_NAME
FROM PAT_ENC enc
LEFT JOIN CLARITY_LOC loc on enc.PRIMARY_LOC_ID = loc.LOC_ID
LEFT JOIN CLARITY_SA sa on loc.SERV_AREA_ID = sa.SERV_AREA_ID
LEFT JOIN (SELECT PAT_ID, MAX(enc.PAT_ENC_CSN_ID) as CSN
               FROM PAT_ENC enc
			   WHERE enc.PRIMARY_LOC_ID is not null
			   AND enc.CONTACT_DATE >= GETDATE()-(3*365) 
			   GROUP BY PAT_ID) max_enc ON (max_enc.PAT_ID = enc.PAT_ID) 
WHERE enc.PAT_ENC_CSN_ID = max_enc.CSN	
AND enc.PAT_ENC_CSN_ID = '1011322233';
AND enc.PAT_ID = 'Z1680203';	


Add this to patient extract
		LEFT JOIN (SELECT enc.PAT_ID, enc.PRIMARY_LOC_ID, loc.LOC_NAME, loc.SERV_AREA_ID, sa.SERV_AREA_NAME
                     FROM PAT_ENC enc
                     LEFT JOIN CLARITY_LOC loc on enc.PRIMARY_LOC_ID = loc.LOC_ID
                     LEFT JOIN CLARITY_SA sa on loc.SERV_AREA_ID = sa.SERV_AREA_ID
                     LEFT JOIN (SELECT PAT_ID, MAX(enc.PAT_ENC_CSN_ID) as CSN
                                  FROM PAT_ENC enc
			                      WHERE enc.PRIMARY_LOC_ID is not null
			                      AND enc.CONTACT_DATE >= GETDATE()-(3*365) 
			                      GROUP BY PAT_ID) max_enc ON (max_enc.PAT_ID = enc.PAT_ID) 
					WHERE enc.PAT_ENC_CSN_ID = max_enc.CSN ) AS prim_loc 
          ON prim_loc.PAT_ID = patient.PAT_ID 	



--MEDS SIG
SELECT...
sig.SIG_TEXT
...
FROM ORDER_MED med
LEFT JOIN  ORDER_MED_SIG sig on sig.ORDER_ID = med.ORDER_MED_ID




					
