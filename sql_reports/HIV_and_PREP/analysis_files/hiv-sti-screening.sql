-- Here is the background: CDC recommends (https://www.cdc.gov/std/tg2015/screening-recommendations.htm) that people with HIV who are sexually active be screened for GC, chlamydia, and syphilis “at first HIV evaluation” and annually after that. Kathleen Roosevelt explained that in MA there is a high rate of HIV co-infection among individuals diagnosed with infectious syphilis (~40%) – and that while it is not possible to assess sexual activity, she hopes to see annual STD screening for ≥75% HIV patients ages 19-49.
 
-- Here are the details. The only thing I am not 100% sure of is the years – what year is a good starting point for HIV data at Atrius?
 
-- ·         Per year, for 2014 through year-to-date 2017:
-- o   Denominator: HIV positive individuals ages 19-49 with ≥1 encounter in the calendar year
-- o   Numerators (3 separate):
-- §  # that received a chlamydia test
-- §  # that received a gonorrhea test
-- §   # that received a syphilis test
 
-- Actual results for the STIs can be ignored for this report. The test can occur any time in the calendar year (i.e. during or after their first encounter for the year).
 
-- Kathleen asked that individuals be deduplicated but that’s obviously not possible (Libby, let’s make sure we point out this limitation in subsequent discussions/demos of MDPHnet and RiskScape).

-- AGES
-- ENCOUNTER TYPES
-- CENTER FILTERING

DROP TABLE IF EXISTS kre_hiv_sti;
CREATE TABLE kre_hiv_sti AS
 
-- Get list of all HIV patients and case date
WITH hiv_sti_cases AS (
	SELECT patient_id, id as case_id, date as hiv_date
	FROM nodis_case
	WHERE condition = 'hiv'),
	
-- Get the years hiv patients had encounters (equal to or following hiv case date)
    -- hiv_sti_encounters AS (
		-- SELECT c.patient_id, date_trunc('year', date)::date as rpt_year
		-- FROM emr_encounter e, hiv_sti_cases c
		-- WHERE e.patient_id = c.patient_id
		-- AND e.date >= c.hiv_date
		-- GROUP by c.patient_id, rpt_year),
	
-- Get the years hiv patients had encounters (equal to or following hiv case date)
-- Only count the encounter if the patient was of the right age (19-49)
	hiv_sti_encounters AS (
		SELECT c.patient_id, date_trunc('year', date)::date as rpt_year
		FROM emr_encounter e, hiv_sti_cases c, emr_patient p
		WHERE e.patient_id = c.patient_id
		AND e.patient_id = p.id
		AND c.patient_id = p.id
		AND e.date >= c.hiv_date
		AND e.raw_encounter_type NOT IN ('HISTORY')
		AND date_part('year', age(e.date, date_of_birth)) >= 19 
		AND date_part('year', age(e.date, date_of_birth)) <= 49 
		GROUP by c.patient_id, rpt_year),
		
		
-- Tally up distinct patients for the denom
	hiv_sti_denom AS (
		SELECT count(distinct(patient_id)) denom_hiv_pats_with_enc, rpt_year
		FROM hiv_sti_encounters
		GROUP by rpt_year),
	
-- Gather details on patients (with an enc in that year) that had a chlam test
	hiv_sti_all_chlam_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND h.name ilike '%chlamydia%'
		GROUP BY e.patient_id, date_trunc('year', date)::date),
	
-- Tally up chlam tests for num #1
	hiv_sti_chlam_numer AS (
		SELECT count(distinct(patient_id)) chlam_test_pats, rpt_year
		FROM hiv_sti_all_chlam_tests
		GROUP by rpt_year),
	
-- Gather details on patients (with an enc in that year) that had a gonorrhea test
	hiv_sti_all_gon_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND h.name ilike '%gonorrhea%'
		GROUP BY e.patient_id, date_trunc('year', date)::date),
		
-- Tally up gon tests for num #2
	hiv_sti_gon_numer AS (
		SELECT count(distinct(patient_id)) gon_test_pats, rpt_year
		FROM hiv_sti_all_gon_tests
		GROUP by rpt_year),
		
-- Gather details on patients (with an enc in that year) that had a gonorrhea test
	hiv_sti_all_syph_tests AS (
		SELECT e.patient_id, date_trunc('year', date)::date as rpt_year
		FROM hef_event h, hiv_sti_encounters e
		WHERE date_trunc('year', date)::date = e.rpt_year
		AND h.patient_id = e.patient_id
		AND h.name ilike '%syphilis%'
		GROUP BY e.patient_id, date_trunc('year', date)::date),

-- Tally up syphilis tests for num #3
	hiv_sti_syph_numer AS (
		SELECT count(distinct(patient_id)) syph_test_pats, rpt_year
		FROM hiv_sti_all_syph_tests
		GROUP by rpt_year)
		
SELECT 
d.rpt_year,
denom_hiv_pats_with_enc,
chlam_test_pats,
gon_test_pats,
syph_test_pats
FROM hiv_sti_denom d
LEFT JOIN hiv_sti_chlam_numer c ON (d.rpt_year = c.rpt_year)
LEFT JOIN hiv_sti_gon_numer g ON (d.rpt_year = g.rpt_year)
LEFT JOIN hiv_sti_syph_numer s ON (d.rpt_year = s.rpt_year);

		
--SELECT * FROM hiv_sti_syph_numer --LIMIT 100
;

