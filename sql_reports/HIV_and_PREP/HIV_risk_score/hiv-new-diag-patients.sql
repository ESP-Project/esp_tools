CREATE TABLE IF NOT EXISTS advp.donotdrop_hiv_new_diagnosis
(
    patient_id integer NOT NULL,
    year_month character varying(7) NOT NULL,
    hiv_new_diagnosis integer NOT NULL,
    CONSTRAINT donotdrop_hiv_new_diag_pkey PRIMARY KEY (patient_id, year_month)
);

-- BUILD ANCHOR PATIENTS & DATES STEP 1
DROP TABLE IF EXISTS advp.hiv_hrs_hiv_events;
CREATE TABLE advp.hiv_hrs_hiv_events AS
SELECT T1.patient_id,T1.name,T1.date 
FROM hef_event T1
WHERE T1.name in ('lx:hiv_ab_diff:positive', 'lx:hiv_ag_ab:negative', 'lx:hiv_ag_ab:positive', 
'lx:hiv_elisa:negative', 'lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive');

-- BUILD ANCHOR PATIENTS & DATES STEP 2
DROP TABLE IF EXISTS advp.hiv_hrs_hiv_cases;
CREATE TABLE advp.hiv_hrs_hiv_cases AS
SELECT T1.patient_id, T1.id as case_id, T1.date as hiv_per_esp_date
FROM nodis_case T1
WHERE condition = 'hiv';


-- BUILD ANCHOR PATIENTS & DATES STEP 3
DROP TABLE IF EXISTS advp.hiv_hrs_eval_labs;
CREATE TABLE advp.hiv_hrs_eval_labs AS 
SELECT T2.patient_id, T2.case_id,hiv_per_esp_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then 1 else 0 end) pos_lab_met,
min(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) min_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) max_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then 1 else 0 end) neg_lab_met,
min(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then T1.date end) min_neg_lab_date
FROM advp.hiv_hrs_hiv_events T1 
RIGHT OUTER JOIN advp.hiv_hrs_hiv_cases T2 ON ((T1.patient_id = T2.patient_id)) 
GROUP BY T2.patient_id, T2.case_id, hiv_per_esp_date;

-- BUILD ANCHOR PATIENTS & DATES STEP 4
-- to handle events prior to having ab_diff type, need to filter out patients where the pos/neg closest to the case date are on the same date
-- use max_lab_pos_date as index date
-- filter out test patients
DROP TABLE IF EXISTS advp.hiv_hrs_hiv_new_diag_pats;
CREATE TABLE advp.hiv_hrs_hiv_new_diag_pats AS
SELECT SELECT T1.patient_id, T1.case_id, to_char(hiv_per_esp_date, 'YYYY_MM') year_month, 1 as hiv_new_diagnosis
FROM advp.hiv_hrs_eval_labs T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE pos_lab_met = 1
AND neg_lab_met = 1
AND max_lab_pos_date != min_neg_lab_date
AND last_name not ilike 'LABVALIDATION'
AND last_name not ilike 'XXXXX%'
AND last_name not ilike 'Fenway%Test';

