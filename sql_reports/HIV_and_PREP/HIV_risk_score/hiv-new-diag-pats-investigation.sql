
•	HIV New Diagnosis
o	Defined as (positive +Western Blot or +ELISA or +Ab/Ag or HIV-1/HIV-2 differentiation assay) on or before the ESP HIV case date or AND (history of negative HIV ELISA or Ab/Ag within 2 years preceding the NODIS case date)
	Code as 1=yes, 0=no, blank if patient is ESP HIV status negative



DROP TABLE IF EXISTS kre_report.temp_hrs_hiv_events;
CREATE TABLE kre_report.temp_hrs_hiv_events AS
SELECT T1.patient_id,T1.name,T1.date 
FROM hef_event T1
WHERE T1.name in ('lx:hiv_ab_diff:positive', 'lx:hiv_ag_ab:negative', 'lx:hiv_ag_ab:positive', 
'lx:hiv_elisa:negative', 'lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive');

DROP TABLE IF EXISTS kre_report.temp_hrs_hiv_cases;
CREATE TABLE kre_report.temp_hrs_hiv_cases AS
SELECT T1.patient_id, T1.id as case_id, T1.date as hiv_per_esp_date
FROM nodis_case T1
WHERE condition = 'hiv'
AND date >= '2015-01-01';


DROP TABLE IF EXISTS kre_report.temp_hrs_eval_labs;
CREATE TABLE kre_report.temp_hrs_eval_labs AS 
SELECT T2.patient_id, T2.case_id,hiv_per_esp_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then 1 else 0 end) pos_lab_met,
min(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) min_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) max_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then 1 else 0 end) neg_lab_met,
min(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then T1.date end) min_neg_lab_date
FROM kre_report.temp_hrs_hiv_events T1 
RIGHT OUTER JOIN kre_report.temp_hrs_hiv_cases T2 ON ((T1.patient_id = T2.patient_id)) 
GROUP BY T2.patient_id, T2.case_id, hiv_per_esp_date;

-- to handle events prior to having ab_diff type, need to filter out patients where the pos/neg closest to the case date are on the same date
-- use max_lab_pos_date as index date?
-- filter out test patients
DROP TABLE IF EXISTS kre_report.temp_hrs_hiv_new_diag_pats;
CREATE TABLE kre_report.temp_hrs_hiv_new_diag_pats AS
SELECT T1.*
FROM kre_report.temp_hrs_eval_labs T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE pos_lab_met = 1
AND neg_lab_met = 1
AND max_lab_pos_date != min_neg_lab_date
AND last_name not ilike 'LABVALIDATION'
AND last_name not ilike 'XXXXX%';


SELECT (count(distinct(T1.patient_id)) + count(distinct(T2.patient_id)) ) as total_patients,
count(distinct(case when T1.date_diff >= 14 and T1.date_diff <=730 then T1.patient_id end) ) as pats_with_clinvisit_in_range_14_730,
round(avg(case when date_diff >=14 and date_diff <= 730 then date_diff end)) as avg_date_diff_for_clinvisit_in_range,
count(distinct(case when date_diff <14 or date_diff >730 then T1.patient_id end) ) as pats_without_clinvisit_in_range,
count(distinct(case when date_diff <14 then T1.patient_id end) ) as pats_clinvis_lt14,
count(distinct(case when date_diff >730 then T1.patient_id end) ) as pats_clinvis_gt730,
count(distinct(t2.patient_id)) as pats_with_no_prior_clinvisit
FROM (
	SELECT T1.patient_id, max_lab_pos_date, max(date) most_recent_vist, max_lab_pos_date - max(date) as date_diff
	FROM kre_report.temp_hrs_hiv_new_diag_pats T1
	LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
	WHERE date < max_lab_pos_date
	AND source = 'enc'
	GROUP BY T1.patient_id, max_lab_pos_date) T1
LEFT JOIN (SELECT patient_id
FROM kre_report.temp_hrs_hiv_new_diag_pats
WHERE patient_id not in  (
    SELECT T1.patient_id
    FROM kre_report.temp_hrs_hiv_new_diag_pats T1
	INNER JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id) 
	WHERE source = 'enc'
	AND date < max_lab_pos_date) ) T2 ON (1=1);
	
	
select round(avg(date_diff)) as avg_date_diff_for_clinvisit_in_range,
min(date_diff) min_gte14_lte730,
  percentile_cont(0.10) within group (order by date_diff) as pct_10_gte14_lte730,
  percentile_cont(0.25) within group (order by date_diff) as pct_25_gte14_lte730,
  percentile_cont(0.50) within group (order by date_diff) as median,
  percentile_cont(0.75) within group (order by date_diff) as pct_75_gte14_lte730,
  percentile_cont(0.90) within group (order by date_diff) as pct_90_gte14_lte730,
max(date_diff) max_gte14_lte730
FROM (
	SELECT T1.patient_id, max_lab_pos_date, max(date) most_recent_vist, max_lab_pos_date - max(date) as date_diff
	FROM kre_report.temp_hrs_hiv_new_diag_pats T1
	LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
	WHERE date < max_lab_pos_date
	AND source = 'enc'
	GROUP BY T1.patient_id, max_lab_pos_date) T1
where date_diff >=14 and date_diff <= 730;
	
	
---------------------------------------------------------------------------------------





select T5.id as case_id, T5.date as case_date, T1.patient_id, T1.name as neg_hef, T2.name as pos_hef, T1.date as neg_date, T2.date as pos_date, T3.native_code as neg_lab_code, T3.result_string as neg_result,
T3.id as neg_labid, T4.native_code as pos_lab_code, T4.result_string as pos_result, T4.id as pos_labid
FROM hef_event T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
LEFT JOIN emr_labresult T3 ON (T1.patient_id = T3.patient_id and T3.id = T1.object_id)
LEFT JOIN emr_labresult T4 ON (T2.patient_id = T4.patient_id and T4.id = T2.object_id)
LEFT JOIN nodis_case T5 ON (T1.patient_id = T5.patient_id and T5.condition = 'hiv')
WHERE T1.name = 'lx:hiv_elisa:negative'
AND T2.name = 'lx:hiv_elisa:positive'


drop table if exists kre_report.hiv_remap_hef;
create table kre_report.hiv_remap_hef as
select T2.result_string, T1.id as hef_id, T1.name, T1.patient_id, T1.date as hef_date
from hef_event T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id and T1.object_id = T2.id)
where T2.native_code in ('87389--4334')
and T1.name ilike 'lx:hiv_elisa%';

drop table if exists kre_report.hiv_remap_nce;
create table kre_report.hiv_remap_nce as
select * from nodis_case_events
where event_id in (select hef_id from kre_report.hiv_remap_hef);

delete from nodis_case_events where id in (select id from kre_report.hiv_remap_nce )


delete from hef_event
where id in (select hef_id from kre_report.hiv_remap_hef);

drop table if exists kre_report.hiv_remap_hef;



-- FIND DUPE EVENTS FOR CLEANUP
select count(*), object_id, content_type_id
from hef_event
where name ilike '%hiv%'
and name not ilike 'lx:hiv_rna_viral%'
group by object_id, content_type_id
having count(*) > 1;


select count(*), last_name 
from (
select last_name, first_name
from emr_patient T1
INNER JOIN kre_report.temp_hrs_hiv_new_diag_pats T2 ON (T1.id = T2.patient_id)
ORDER BY last_name, first_name) foo
group by last_name
having count(*) > 1

-------------------------------------------------------------------
SELECT count(distinct(patient_id)), dx_code_id, name
--SELECT count(distinct(encounter_id)), raw_encounter_type
FROM 
(
SELECT T2.id as encounter_id, T5.id as case_id, T5.date as case_date, date_diff, T1.patient_id, T1.max_lab_pos_date, T1.most_recent_visit, T2.date, T2.raw_encounter_type, T3.dx_code_id, T4.name
FROM (
SELECT T1.patient_id, max_lab_pos_date, max(date) most_recent_visit, max_lab_pos_date - max(date) as date_diff
	FROM kre_report.temp_hrs_hiv_new_diag_pats T1
	LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
	WHERE date < max_lab_pos_date
	AND source = 'enc'
	GROUP BY T1.patient_id, max_lab_pos_date
	ORDER BY date_diff ) T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id and T1.most_recent_visit = T2.date)
INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
INNER JOIN nodis_case T5 ON (T1.patient_id = T5.patient_id and T5.condition = 'hiv')
WHERE date_diff < 14
and dx_code_id not in ('icd9:799.9')
--AND T1.patient_id = 1226431
order by name) S1
group by dx_code_id, name
--group by raw_encounter_type
order by count(distinct(patient_id)) desc
--order by count(distinct(encounter_id)) desc



BMC
LAB6386--4935 -- should be mapped to hiv_ag_ab
'LAB6387--4938' -- should be mapped to hiv_ag_ab

BMC check case date on re-creation 12029  for multispot test

fenway didn't have collection date back then so order date


