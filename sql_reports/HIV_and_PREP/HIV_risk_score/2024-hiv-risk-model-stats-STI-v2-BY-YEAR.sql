drop table if exists kre_report.hiv_hrs_stats_anchor_years;
create table kre_report.hiv_hrs_stats_anchor_years AS
--select count(*), to_char(anchor_date, 'YYYY-MM') as anchor_month,
select count(*), extract(year from anchor_date) as anchor_year,
gender_mapped as sex
from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE date_part('year', age(anchor_date, date_of_birth)) >= 15
group by anchor_year, sex;

-- total patients (clinical encounter of type enc in the year)
drop table if exists kre_report.hiv_hrs_stats_total_vis_pats_years;
create table kre_report.hiv_hrs_stats_total_vis_pats_years AS
select count(distinct(patient_id)) total_visit_patients, extract(year from T1.date) anchor_year,
CASE WHEN upper(T3.gender) in ('M', 'MALE') then 'M'
WHEN upper(T3.gender) in ('F', 'FEMALE') then 'F' 
WHEN upper(T3.gender) in ('T') then 'T'
WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('FEMALE') then 'F'
WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('MALE') then 'M'
ELSE 'M' END as sex
from gen_pop_tools.clin_enc T1
JOIN emr_patient T3 ON (T1.patient_id = T3.id)
WHERE source = 'enc'
AND extract(year from T1.date) in (SELECT anchor_year from kre_report.hiv_hrs_stats_anchor_years)
GROUP BY anchor_year, sex;


-- eligible patients (patients >= 15 on anchor date and no HIV case past or present)
-- does not include hiv new diag pats or sti patients
-- ONLY INCLUDE PATIENTS WITH MATCHING GENDER
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_eligible_pats_years;
CREATE TABLE kre_report.hiv_hrs_stats_eligible_pats_years AS
select count(distinct(patient_id)) eligible_patients, anchor_year,
gender_mapped as sex
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN kre_report.hiv_hrs_stats_anchor_years T2 ON (extract(year from T1.anchor_date) = T2.anchor_year)
-- don't count new hiv diag patients or sti patients
WHERE T1.patient_id not in (select patient_id from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats)
GROUP BY anchor_year, gender_mapped;


-- control patients (patients with 1 or more non-demographic variable)
-- does not include hiv new diag pats
-- ONLY INCLUDE PATIENTS WITH MATCHING GENDER
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_control_pats_years;
CREATE TABLE kre_report.hiv_hrs_stats_control_pats_years AS
select count(distinct(T1.patient_id)) control_patients, extract(year from T1.anchor_date) as anchor_year,
gender_mapped as sex
FROM kre_report.hiv_hrs_index_patients_w_voi T1
JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.patient_id not in (select patient_id from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats)
GROUP BY anchor_year, sex;


-- control pats sampled per month
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_control_pats_sampled_years;
CREATE TABLE kre_report.hiv_hrs_stats_control_pats_sampled_years AS
SELECT count(distinct(patient_id)) control_pats_sampled, extract(year from anchor_date) anchor_year, gender_mapped as sex
from kre_report.hiv_hrs_control_patients_final
group by anchor_year, gender_mapped;


-- hiv new diag pats and sti pats per month
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_new_diag_pats_years;
CREATE TABLE kre_report.hiv_hrs_stats_new_diag_pats_years AS
SELECT count(distinct(patient_id)) as hiv_new_diag_pat_count, extract(year from anchor_date) anchor_year, gender_mapped as sex
FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats
-- this makes sure patient is >= age15
WHERE patient_id in (select patient_id from kre_report.hiv_hrs_index_patients)
GROUP BY anchor_year, sex;

DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_output_years;
CREATE TABLE kre_report.hiv_hrs_stats_output_years AS
SELECT T1.anchor_year, T1.sex,
total_visit_patients,
coalesce(eligible_patients, 0) eligible_patients,
coalesce(control_patients, 0) control_patients,
control_pats_sampled,
hiv_new_diag_pat_count as case_patient_count
FROM kre_report.hiv_hrs_stats_anchor_years T1
LEFT JOIN kre_report.hiv_hrs_stats_total_vis_pats_years T2 ON (T1.anchor_year = T2.anchor_year and T1.sex = T2.sex)
LEFT JOIN kre_report.hiv_hrs_stats_eligible_pats_years T3 ON (T1.anchor_year = T3.anchor_year and T1.sex = T3.sex)
LEFT JOIN kre_report.hiv_hrs_stats_control_pats_years T4 ON (T1.anchor_year = T4.anchor_year and T1.sex = T4.sex)
LEFT JOIN kre_report.hiv_hrs_stats_control_pats_sampled_years T5 ON (T1.anchor_year = T5.anchor_year and T1.sex = T5.sex)
LEFT JOIN kre_report.hiv_hrs_stats_new_diag_pats_years T6 ON (T1.anchor_year = T6.anchor_year and T1.sex = T6.sex)
ORDER BY T1.anchor_year, T1.sex;


-- nohup psql -d esp30 -f 2024-hiv-risk-model-stats-STI-v2-BY-YEAR.sql &
-- nohup psql -d esp -f 2024-hiv-risk-model-stats-STI-v2-BY-YEAR.sql &

--2024-07-24-FWY-hiv-risk-score-stats-STI-BY-YEAR.csv





