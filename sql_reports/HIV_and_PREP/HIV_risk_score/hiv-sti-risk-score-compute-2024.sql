SET client_min_messages TO WARNING;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_coefficients;
CREATE TABLE hiv_rpt.hivstirs_coefficients AS
SELECT
20.12487128::numeric      intercept,
-0.003298513::numeric     age_on_anchor_date,	
-10.45935669::numeric     gender_mapped,
0.208053047::numeric     gender_male,
9.447397553::numeric     gender_female,
21.56251611::numeric     gender_trans,
0.187831912::numeric     birth_sex_male,
0.310975179::numeric     birth_sex_unk,
-0.539682722::numeric     sexor_straight,
1.297669147::numeric     sexor_gay_lesbian,
0.546782909::numeric     sexor_other,
-0.313044287::numeric     gender_iden_male,
-0.354055057::numeric     gender_iden_trans_male,
0.42506863::numeric     gender_iden_trans_female,
0.018871474::numeric     gender_iden_nonbinary,
-0.02815369::numeric     gender_iden_other,
-0.983904579::numeric     gender_iden_unk,
-0.293034729::numeric     race_white,
0.765057813::numeric     race_black,
0.40514443::numeric     race_other,
0.126625111::numeric     race_unk,
-0.306079828::numeric     ethnicity_hispanic,
-0.570867685::numeric     ethnicity_nonhispanic,
-0.564658513::numeric     ethnicity_unk,
-14.39812609::numeric     sex_partner_male,
-15.37502668::numeric     sex_partner_female,
-13.53762498::numeric     sex_partner_mandf,
-13.05668154::numeric     sex_partner_trans,
-14.26166576::numeric     sex_partner_unk,
-0.518795454::numeric     marital_stat_married,
-0.236282629::numeric     marital_stat_partner,
-0.356846231::numeric     marital_stat_unk,
0.341176974::numeric     marital_stat_widow,
0.004873571::numeric     marital_stat_divorced,
-0.13763815::numeric     marital_stat_other,
-0.042969712::numeric     num_encs_1yr,
-0.017042221::numeric     num_encs_1_to_2yr,
-0.006344024::numeric     num_encs_gt_2yr,
0.033212826::numeric     gon_tests_1yr,
0.089029883::numeric     gon_tests_1_to_2yr,
-0.048485668::numeric     gon_tests_gt_2yr,
0.361286025::numeric     gon_pos_tests_1yr,
0.323993851::numeric     gon_pos_tests_1_to_2yr,
0.188440495::numeric     gon_pos_tests_gt_2yr,
0.29214221::numeric     gon_tests_rectal_1yr,
0.114739332::numeric     gon_tests_rectal_1_to_2yr,
0.143610618::numeric     gon_tests_rectal_gt_2yr,
-0.471028376::numeric     gon_tests_rectal_pos_1yr,
-0.627350313::numeric     gon_tests_rectal_pos_1_to_2yr,
0.088170521::numeric     gon_tests_rectal_pos_gt_2yr,
-0.183625051::numeric     gon_tests_oral_1yr,
-0.13521247::numeric     gon_tests_oral_1_to_2yr,
0.039751743::numeric     gon_tests_oral_gt_2yr,
-1.004758793::numeric     gon_tests_oral_pos_1yr,
-0.326903764::numeric     gon_tests_oral_pos_1_to_2yr,
-0.175184219::numeric     gon_tests_oral_pos_gt_2yr,
-0.114871911::numeric     chlam_tests_1yr,
0.270215681::numeric     chlam_pos_tests_1yr,
-0.15070844::numeric     chlam_pos_tests_1_to_2yr,
0.049970735::numeric     chlam_pos_tests_gt_2yr,
0.110441046::numeric     chlam_tests_rectal_1yr,
-0.161703267::numeric     chlam_tests_rectal_1_to_2yr,
-0.047276053::numeric     chlam_tests_rectal_gt_2yr,
-0.647292735::numeric     chlam_tests_rectal_pos_1yr,
0.001841825::numeric     chlam_tests_rectal_pos_1_to_2yr,
-0.097090414::numeric     chlam_tests_rectal_pos_gt_2yr,
0.422748528::numeric     chlam_tests_oral_1yr,
-0.012349296::numeric     chlam_tests_oral_1_to_2yr,
0.038675987::numeric     chlam_tests_oral_gt_2yr,
0.243140049::numeric     chlam_tests_oral_pos_1yr,
0.315753801::numeric     chlam_tests_oral_pos_1_to_2yr,
0.509576791::numeric     chlam_tests_oral_pos_gt_2yr,
0.412503582::numeric     syph_tests_1yr,
-0.057370573::numeric     syph_tests_1_to_2yr,
0.021630795::numeric     syph_tests_gt_2yr,
-0.737720038::numeric     syph_per_esp_1yr,
-0.161354325::numeric     syph_per_esp_1_to_2yr,
0.201600128::numeric     syph_per_esp_gt_2yr,
-0.086021294::numeric     hcv_ab_1yr,
-0.025149686::numeric     hcv_ab_1_to_2yr,
-0.018805761::numeric     hcv_ab_gt_2yr,
0.130444647::numeric     hcv_rna_1yr,
-0.158000155::numeric     hcv_rna_1_to_2yr,
-0.091151545::numeric     hcv_rna_gt_2yr,
0.011511662::numeric     hcv_ab_or_rna_pos_ev,
0.068759693::numeric     hcv_ab_pos_first_yr,
0.015956188::numeric     hcv_rna_pos_first_yr,
-0.251997011::numeric     hcv_acute_per_esp,
-0.031632097::numeric     hcv_acute_per_esp_case_yrs,
0.092750765::numeric     hbv_sag_1yr,
-0.084822484::numeric     hbv_sag_1_to_2yr,
-0.04718583::numeric     hbv_sag_gt_2yr,
-0.294006261::numeric     hbv_dna_1yr,
-0.178986658::numeric     hbv_dna_1_to_2yr,
0.038324599::numeric     hbv_dna_gt_2yr,
-0.091119619::numeric     hbv_sag_or_dna_pos_first_yr,
-0.271098712::numeric     hbv_acute_per_esp,
0.427185482::numeric     hbv_acute_per_esp_case_yrs,
0.396638711::numeric     hbv_history_ev,
-0.171049665::numeric     hiv_elisa_ag_ab_1yr,
-0.02609741::numeric     hiv_elisa_ag_ab_1_to_2yr,
0.02532687::numeric     hiv_elisa_ag_ab_gt_2yr,
-0.215155976::numeric     hiv_confirm_tests_1yr,
0.322654939::numeric     hiv_confirm_tests_1_to_2yr,
0.564836125::numeric     hiv_confirm_tests_gt_2yr,
-0.12214679::numeric     hiv_rna_viral_1yr,
-0.421296166::numeric     hiv_rna_viral_1_to_2yr,
0.011540624::numeric     hiv_rna_viral_gt_2yr,
0.284899009::numeric     hiv_max_viral_load_yr,
1.478048756::numeric     hiv_neg_w_hiv_med_ev,
-0.04271188::numeric     hiv_dx_1_yr,
0.096800951::numeric     hiv_dx_1_to_2yr,
-0.163963289::numeric     hiv_dx_gt_2yr,
-0.388053622::numeric     hiv_trmt_regimen_1yr,
-0.047718502::numeric     hiv_trmt_regimen_1_to2yr,
0.041073656::numeric     hiv_trmt_regimen_gt_2yr,
0.181430494::numeric     pep_rx_dates_num,
0.027563004::numeric     prep_rx_num_1yr,
0.008992081::numeric     prep_rx_num_1_to_2yr,
-0.0149967::numeric     prep_rx_num_gt_2yr,
-0.066036078::numeric     dx_abn_anal_cyt_yrs,
0.203676342::numeric     dx_syph_yrs,
-1.490246683::numeric     dx_anal_syph_yrs,
0.292128453::numeric     dx_gon_inf_rectum_yrs,
-0.01189707::numeric     dx_gon_pharyn_yrs,
0.060586132::numeric     dx_chlam_inf_rectum_yrs,
0.076641375::numeric     dx_chlam_pharyn_yrs,
0.309777794::numeric     dx_lymph_ven_yrs,
0.072777183::numeric     dx_chancroid_yrs,
-0.024139727::numeric     dx_nongon_ureth_yrs,
-0.010918864::numeric     dx_hsv_w_compl_yrs,
0.001989202::numeric     dx_genital_herpes_yrs,
-0.002182577::numeric     dx_anogenital_warts_yrs,
-0.303446029::numeric     dx_anorectal_ulcer_yrs,
0.184714393::numeric     dx_trich_yrs,
1.048913437::numeric     dx_mpox_yrs,
0.082395503::numeric     dx_unspec_std_yrs,
0.039226661::numeric     dx_pid_yrs,
0.061800845::numeric     dx_cont_w_exp_to_vd_yrs,
0.00071993::numeric     dx_high_risk_sex_behav_yrs,
0.019566109::numeric     dx_hiv_counsel_yrs,
-0.106766185::numeric     dx_anorexia_yrs,
-0.13018384::numeric     dx_bulimia_yrs,
-0.014317902::numeric     dx_eat_disorder_nos_yrs,
0.015623135::numeric     dx_gend_iden_dis_yrs,
-0.038385355::numeric     dx_transsexualism_yrs,
-7.779043711::numeric     dx_couns_or_prb_child_sex_abuse_yrs,
0.038725704::numeric     dx_adult_child_sex_exp_or_abuse_yrs,
0.068294917::numeric     dx_foreign_body_anus_yrs,
-0.023682594::numeric     dx_alcohol_depend_abuse_yrs,
0.019478538::numeric     dx_opioid_depend_abuse_yrs,
-0.023503881::numeric     dx_sed_hypn_anxio_depend_abuse_yrs,
0.052851886::numeric     dx_cocaine_depend_abuse_yrs,
0.069080382::numeric     dx_amphet_stim_depend_abuse_yrs,
0.076207212::numeric     dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
-0.002688975::numeric     dx_sti_screen_yrs,
-0.003801385::numeric     dx_malig_neo_rec_screen_yrs,
-0.462045513::numeric     dx_needle_stick,
0.414872845::numeric     rx_bicillin_ever,
0.332126542::numeric     rx_azith_ever,
0.094659807::numeric     rx_ceft_ever,
-0.205041987::numeric     rx_doxy_ever,
0.364047108::numeric     rx_methadone_ever,
-0.055310816::numeric     rx_buprenorphine_ever,
0.093214396::numeric     rx_naltrexone_ever,
-0.214860382::numeric     rx_viagara_cilais_or_levitra_ever,
-0.27238314::numeric     vax_mpox_ever,
-0.706239797::numeric     vax_hpv_male_ever;

-- DELETE ALL EXISTING ENTRIES FOR YEAR BEING COMPUTED
DELETE FROM gen_pop_tools.hiv_sti_risk_scores where rpt_year =  (EXTRACT(YEAR FROM :end_calc_year::date))::text;

-- ONLY COMPUTING FOR PATIENTS THAT HAD A CLINICAL ENCOUNTER IN THE 2 YEARS PRIOR TO RUN YEAR
-- PATIENTS MUST BE OVER AGE 15
-- EXCLUDE CURRENT HIV PATIENTS
-- EXCLUDE DECEASED PATIENTS
DROP TABLE IF EXISTS hiv_rpt.hivstirs_index_patients;
CREATE TABLE hiv_rpt.hivstirs_index_patients AS
SELECT DISTINCT T1.id as patient_id,
date_part('year', age(:end_calc_year::date, date_of_birth)) age_on_anchor_date,
(EXTRACT(YEAR FROM :end_calc_year::date))::text rpt_year,
-- 1=M, 2=F, 3=T ELSE 0
CASE WHEN upper(T1.gender) in ('M', 'MALE') then 1
     WHEN upper(T1.gender) in ('F', 'FEMALE') then 2 
     WHEN upper(T1.gender) in ('T') then 3
	 WHEN upper(T1.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('FEMALE') then 2
	 WHEN upper(T1.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('MALE') then 1
	 -- TREAT EVERYONE ELSE AS M
     WHEN upper(T1.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') or gender is null then 1
	 ELSE 0 END as gender_mapped,
CASE WHEN upper(gender) in ('M', 'MALE') then 1 ELSE 0 END as gender_male,
CASE WHEN upper(gender) in ('F', 'FEMALE') then 1 ELSE 0 END as gender_female,
CASE WHEN upper(gender) in ('T') then 1 ELSE 0 END as gender_trans,
CASE WHEN upper(birth_sex) in ('M', 'MALE') then 1 ELSE 0 END as birth_sex_male,
CASE WHEN upper(birth_sex) in ('F', 'FEMALE') then 1 ELSE 0 END as birth_sex_female,
CASE WHEN upper(birth_sex) not in ('M', 'MALE', 'F', 'FEMALE') or birth_sex is null then 1 ELSE 0 END as birth_sex_unk,
CASE WHEN upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'STRAIGHT') then 1 ELSE 0 END as sexor_straight,
CASE WHEN upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'GAY/LESBIAN') then 1 ELSE 0 END as sexor_gay_lesbian,
CASE WHEN upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'OTHER') then 1 ELSE 0 END as sexor_other,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'MALE') and (upper(birth_sex) not in ('F', 'FEMALE') or birth_sex is null) then 1 ELSE 0 END as gender_iden_male,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'TRANS_MALE') or (upper(birth_sex) in ('F', 'FEMALE') and upper(gender_identity) in ('MALE')) then 1 ELSE 0 END as gender_iden_trans_male,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'TRANS_FEMALE')  or (upper(birth_sex) in ('M', 'MALE') and upper(gender_identity) in ('FEMALE')) then 1 ELSE 0 END as gender_iden_trans_female,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'NON-BINARY') then 1 ELSE 0 END as gender_iden_nonbinary,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'OTHER') then 1 ELSE 0 END as gender_iden_other,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'UNKNOWN')  or gender_identity is null then 1 ELSE 0 END as gender_iden_unk,
CASE WHEN upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'CAUCASIAN') then 1 ELSE 0 END as race_white,
CASE WHEN upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'BLACK') then 1 ELSE 0 END as race_black,
CASE WHEN upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'OTHER') then 1 ELSE 0 END as race_other,
CASE WHEN upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'Unspecified') 
						  or race is null then 1 ELSE 0 END as race_unk,
CASE WHEN (upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC') 
			OR
			upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'HISPANIC'))
	        then 1 ELSE 0 END as ethnicity_hispanic,
CASE WHEN upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'NOT HISPANIC') 
						  then 1 ELSE 0 end as  ethnicity_nonhispanic,		  
CASE WHEN  (
           (upper(race) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC')
			     OR race is null)
			AND
			(upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'Unspecified') 
						  OR ethnicity is null)
		  )
			then 1 ELSE 0 END as ethnicity_unk,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'MARRIED') then 1 ELSE 0 END as marital_stat_married,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'PARTNER') then 1 ELSE 0 END as marital_stat_partner,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'UNKNOWN') or marital_stat is null then 1 ELSE 0 END as marital_stat_unk,			
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'WIDOWED') then 1 ELSE 0 END as marital_stat_widow,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'DIVORCED') then 1 ELSE 0 END as marital_stat_divorced,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'OTHER') then 1 ELSE 0 END as marital_stat_other
FROM emr_patient T1
JOIN gen_pop_tools.clin_enc T2 on (T1.id = T2.patient_id) 
-- must have an clincial encounter in the past 2 years
AND T2.date >= (:end_calc_year::date - interval '2 years')
-- must be at least age 15
AND date_part('year', age(:end_calc_year::date, date_of_birth)) >= 15
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv' and date <= :end_calc_year)
-- exclude deceased
AND date_of_death is null and (vital_status not ilike 'deceased' or vital_status is null)
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%'
AND T2.date <= :end_calc_year::date;	 

-- SEX PARTNER GENDER
DROP TABLE IF EXISTS hiv_rpt.hivstirs_sex_p_gender;
CREATE TABLE hiv_rpt.hivstirs_sex_p_gender as
SELECT T1.patient_id,
MAX(CASE WHEN sex_partner_gender in ('MALE') THEN 1 ELSE 0 END) as sex_partner_male,
MAX(CASE WHEN sex_partner_gender in ('FEMALE') THEN 1 ELSE 0 END) as sex_partner_female,
MAX(CASE WHEN sex_partner_gender in ('MALE + FEMALE', 'FEMALE + MALE') THEN 1 ELSE 0 END) as sex_partner_mandf,
MAX(CASE WHEN sex_partner_gender in ('TRANS') THEN 1 ELSE 0 END) as sex_partner_trans,
MAX(CASE WHEN sex_partner_gender not in ('MALE','FEMALE', 'MALE + FEMALE', 'FEMALE + MALE', 'TRANS') or sex_partner_gender is null THEN 1 ELSE 0 END) as sex_partner_unk
FROM hiv_rpt.hivstirs_index_patients T1
-- Get the most recent social history date to determine sex_partner_gender
LEFT JOIN ((SELECT max(date) sochist_date, T2.patient_id 
	        FROM emr_socialhistory T1
	        JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
	        WHERE sex_partner_gender is not null		
			--REPLACE WITH END DATE
            AND T1.date <= :end_calc_year::date
        	GROUP BY T2.patient_id) ) T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_socialhistory T3 on (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id and T2.sochist_date = T3.date and sex_partner_gender is not null)
GROUP BY T1.patient_id;

-- ONLY COUNTING ENC ENCOUNTERS HERE!
DROP TABLE IF EXISTS hiv_rpt.hivstirs_clin_enc_visits_num;
CREATE TABLE hiv_rpt.hivstirs_clin_enc_visits_num AS
SELECT T1.patient_id,
count(case when date >= :end_calc_year::date - interval '1 year' and date <= :end_calc_year::date then 1 end) as num_encs_1yr,
count(case when (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as num_encs_1_to_2yr,
count(case when (date < :end_calc_year::date - interval '2 year') then 1 end) as num_encs_gt_2yr
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN 	gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id and source = 'enc')
WHERE date <= :end_calc_year::date
GROUP BY T1.patient_id;

--
-- ALL GONORRHEA/CHLAMYDIA LAB AND HEF EVENTS FOR INDEX PATIENTS
-- NORMALIZE SPECIMEN SOURCES FOR CHLAM/GON TESTS
-- 
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_chlam_gon_labs_events;
CREATE TABLE hiv_rpt.hivstirs_all_chlam_gon_labs_events AS 
SELECT T3.patient_id, T2.date as hef_date, T2.name as hef_name, 
CASE WHEN specimen_source ~* '(RECTUM|RECT|ANAL|ANUS)' then 'RECTAL'
WHEN specimen_source ~* '(PHARYNGEAL|THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA|URETHRA)' then 'UROG'
WHEN (native_name ~* '(PHARYNGEAL|THROAT)' or T1.native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (native_name ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR T1.native_code ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR procedure_name ~* '(RECTAL|ANAL|RECTUM|ANUS)') THEN 'RECTAL'
WHEN (native_name ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
                       OR T1.native_code  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
					   OR procedure_name  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' ) THEN 'UROG'
ELSE 'OTHER_OR_UNKNOWN' END specimen_source,
specimen_source specimen_source_orig,
T1.id as lab_id, T2.object_id, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id)
INNER JOIN hiv_rpt.hivstirs_index_patients T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
WHERE 
(T2.name ilike 'lx:gonorrhea%' or T2.name ilike 'lx:chlamydia%')
AND T2.date <= :end_calc_year::date;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_chlam_gon_labs_counts;
CREATE TABLE hiv_rpt.hivstirs_all_chlam_gon_labs_counts AS 
SELECT patient_id,
count(case when hef_name ilike 'lx:gonorrhea:%' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_tests_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_tests_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_tests_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_pos_tests_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_pos_tests_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_pos_tests_gt_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_tests_rectal_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_tests_rectal_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_tests_rectal_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_tests_rectal_pos_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_tests_rectal_pos_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_tests_rectal_pos_gt_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_tests_oral_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_tests_oral_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_tests_oral_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as gon_tests_oral_pos_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as gon_tests_oral_pos_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as gon_tests_oral_pos_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_tests_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_tests_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_tests_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_pos_tests_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_pos_tests_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_pos_tests_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_tests_rectal_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_tests_rectal_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_tests_rectal_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_tests_rectal_pos_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_tests_rectal_pos_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_tests_rectal_pos_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_tests_oral_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_tests_oral_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_tests_oral_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and hef_date >= :end_calc_year::date - interval '1 year' then 1 end) as chlam_tests_oral_pos_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and (hef_date < :end_calc_year::date - interval '1 year') and (hef_date >= :end_calc_year::date - interval '2 years') then 1 end) as chlam_tests_oral_pos_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and hef_date < :end_calc_year::date - interval '2 year' then 1 end) as chlam_tests_oral_pos_gt_2yr
FROM hiv_rpt.hivstirs_all_chlam_gon_labs_events
GROUP BY patient_id;

-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_syph_events;
CREATE TABLE hiv_rpt.hivstirs_all_syph_events AS
SELECT T1.patient_id, T2.date, T3.test_name--, T4.name as hef_name, result_string
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'tppa-csf', 'fta-abs', 'fta-abs-csf', 'tp-igg', 'tp-igm', 'tp-cia', 'rpr_riskscape')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= :end_calc_year::date
GROUP BY T1.patient_id, T3.test_name, T2.date--, result_string, T4.name
;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_syph_counts;
CREATE TABLE hiv_rpt.hivstirs_all_syph_counts AS
SELECT patient_id,
count(case when date >= :end_calc_year::date - interval '1 year' then 1 end) as syph_tests_1yr,
count(case when (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as syph_tests_1_to_2yr,
count(case when date < :end_calc_year::date - interval '2 year' then 1 end) as syph_tests_gt_2yr
FROM hiv_rpt.hivstirs_all_syph_events
GROUP BY patient_id;	

-- All ESP cases of interest
-- Add in HepC Acute Cases
DROP TABLE IF EXISTS hiv_rpt.hivstirs_cases;
CREATE TABLE hiv_rpt.hivstirs_cases AS 
SELECT T1.patient_id, T1.condition, T1.date as case_date
FROM nodis_case T1,
hiv_rpt.hivstirs_index_patients T2
WHERE T1.patient_id = T2.patient_id
AND condition in ('syphilis', 'hiv') 
AND T1.date <= :end_calc_year::date

UNION

-- CAN ONLY GO FROM Undetermined to Acute (or Chronic) so we just need all Hep C Acute Cases
SELECT T1.patient_id, 'hepatitis_c:acute' as condition, T1.date as case_date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
hiv_rpt.hivstirs_index_patients T3
WHERE T1.condition = 'hepatitis_c'
AND T1.date <= :end_calc_year::date
and T2.status = 'HEP_C-A'
AND T1.id = T2.case_id 
AND T1.patient_id = T3.patient_id

UNION

-- Hep B Acute Cases
SELECT T1.patient_id, 'hepatitis_b:acute' as condition, T1.date as case_date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
hiv_rpt.hivstirs_index_patients T3
WHERE T1.condition = 'hepatitis_b'
AND T1.date <= :end_calc_year::date
and T2.status = 'HEP_B-A'
AND T1.id = T2.case_id 
AND T1.patient_id = T3.patient_id;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_syph_case_counts;
CREATE TABLE hiv_rpt.hivstirs_all_syph_case_counts AS
SELECT patient_id,
count(case when condition = 'syphilis' and case_date >= :end_calc_year::date - interval '1 year' then 1 end) as syph_per_esp_1yr,
count(case when condition = 'syphilis' and  (case_date < :end_calc_year::date - interval '1 year') and (case_date >= :end_calc_year::date - interval '2 years') then 1 end) as syph_per_esp_1_to_2yr,
count(case when condition = 'syphilis' and case_date < :end_calc_year::date - interval '2 year' then 1 end) as syph_per_esp_gt_2yr
FROM hiv_rpt.hivstirs_cases
GROUP BY patient_id;

-- Gather up all hep related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hep_events;
CREATE TABLE hiv_rpt.hivstirs_all_hep_events AS 
SELECT T1.patient_id, T2.date, T3.test_name, T4.name as hef_name--, result_string
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna', 'hepatitis_b_surface_antigen', 'hepatitis_b_viral_dna')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= :end_calc_year::date
GROUP BY T1.patient_id, T3.test_name, T2.date, T4.name --, result_string
;

-- Remove hef name so tests that don't have pos/neg don't get counted twice
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hep_events_nohef;
CREATE TABLE hiv_rpt.hivstirs_all_hep_events_nohef AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.hivstirs_all_hep_events
GROUP BY patient_id, test_name, date;


DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hep_events_nohef_counts;
CREATE TABLE hiv_rpt.hivstirs_all_hep_events_nohef_counts AS 
SELECT patient_id,
count(case when test_name = 'hepatitis_c_elisa' and date >= :end_calc_year::date - interval '1 year' then 1 end) as hcv_ab_1yr,
count(case when test_name = 'hepatitis_c_elisa' and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hcv_ab_1_to_2yr,
count(case when test_name = 'hepatitis_c_elisa' and date < :end_calc_year::date - interval '2 year' then 1 end) as hcv_ab_gt_2yr,
count(case when test_name = 'hepatitis_c_rna' and date >= :end_calc_year::date - interval '1 year' then 1 end) as hcv_rna_1yr,
count(case when test_name = 'hepatitis_c_rna' and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hcv_rna_1_to_2yr,
count(case when test_name = 'hepatitis_c_rna' and date < :end_calc_year::date - interval '2 year' then 1 end) as hcv_rna_gt_2yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and date >= :end_calc_year::date - interval '1 year' then 1 end) as hbv_sag_1yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hbv_sag_1_to_2yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and date < :end_calc_year::date - interval '2 year' then 1 end) as hbv_sag_gt_2yr,
count(case when test_name = 'hepatitis_b_viral_dna' and date >= :end_calc_year::date - interval '1 year' then 1 end) as hbv_dna_1yr,
count(case when test_name = 'hepatitis_b_viral_dna' and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hbv_dna_1_to_2yr,
count(case when test_name = 'hepatitis_b_viral_dna' and date < :end_calc_year::date - interval '2 year' then 1 end) as hbv_dna_gt_2yr
FROM hiv_rpt.hivstirs_all_hep_events_nohef
group by patient_id;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hep_events_hef_counts;
CREATE TABLE hiv_rpt.hivstirs_all_hep_events_hef_counts as
SELECT patient_id,
max(case when (test_name = 'hepatitis_c_elisa' or test_name = 'hepatitis_c_rna') and hef_name ilike '%pos%' then 1 end) as hcv_ab_or_rna_pos_ev,
max(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' then date_part('year', age(:end_calc_year::date, date)) end) as hcv_ab_pos_first_yr,
max(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' then date_part('year', age(:end_calc_year::date, date)) end) as hcv_rna_pos_first_yr,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' then 1 end) as hbv_sag_or_dna_pos_ev,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' then date_part('year', age(:end_calc_year::date, date)) end) as hbv_sag_or_dna_pos_first_yr
FROM hiv_rpt.hivstirs_all_hep_events
GROUP BY patient_id;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hep_case_counts;
CREATE TABLE hiv_rpt.hivstirs_all_hep_case_counts AS
SELECT patient_id,
max(case when condition = 'hepatitis_c:acute' then 1 end) as hcv_acute_per_esp,
max(case when condition = 'hepatitis_c:acute' then date_part('year', age(:end_calc_year::date, case_date)) end) as hcv_acute_per_esp_case_yrs,
max(case when condition = 'hepatitis_b:acute' then 1 end) as hbv_acute_per_esp,
max(case when condition = 'hepatitis_b:acute' then date_part('year', age(:end_calc_year::date, case_date)) end) as hbv_acute_per_esp_case_yrs
FROM hiv_rpt.hivstirs_cases
GROUP BY patient_id;

-- HepB Diagnosis Codes
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hepb_diags;
CREATE TABLE hiv_rpt.hivstirs_all_hepb_diags AS 
SELECT T3.patient_id, 1::int as hep_b_dx
FROM emr_encounter_dx_codes T1
JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
JOIN hiv_rpt.hivstirs_index_patients T3 ON (T2.patient_id = T3.patient_id)
WHERE 
T2.date <= :end_calc_year::date
AND dx_code_id ~'^(icd9:070.2|icd9:070.3|icd9:V02.61|icd10:B16.|icd10:B18.0|icd10:B18.1|icd10:B19.1|icd10:Z22.51)'
GROUP BY T3.patient_id;

-- Gather up all HIV related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hiv_events;
CREATE TABLE hiv_rpt.hivstirs_all_hiv_events AS 
SELECT T1.patient_id, T2.date, T3.test_name, T4.name as hef_name, result_string, result_float
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name ilike '%hiv%'
AND T3.test_name != 'hiv not a test'
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= :end_calc_year::date
GROUP BY T1.patient_id, T3.test_name, T2.date, T4.name, result_string, result_float
;

-- Remove hef name so tests that don't have pos/neg don't get counted twice
-- or not counted at all
DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hiv_events_nohef;
CREATE TABLE hiv_rpt.hivstirs_all_hiv_events_nohef AS 
SELECT patient_id, test_name, date
FROM hiv_rpt.hivstirs_all_hiv_events
GROUP BY patient_id, test_name, date;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_all_hiv_events_nohef_counts;
CREATE TABLE hiv_rpt.hivstirs_all_hiv_events_nohef_counts AS 
SELECT patient_id,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and date >= :end_calc_year::date - interval '1 year' then 1 end) as hiv_elisa_ag_ab_1yr,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hiv_elisa_ag_ab_1_to_2yr,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and date < :end_calc_year::date - interval '2 year' then 1 end) as hiv_elisa_ag_ab_gt_2yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and date >= :end_calc_year::date - interval '1 year' then 1 end) as hiv_confirm_tests_1yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hiv_confirm_tests_1_to_2yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and date < :end_calc_year::date - interval '2 year' then 1 end) as hiv_confirm_tests_gt_2yr,
count(case when test_name = 'hiv_rna_viral' and date >= :end_calc_year::date - interval '1 year' then 1 end) as hiv_rna_viral_1yr,
count(case when test_name = 'hiv_rna_viral' and (date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years') then 1 end) as hiv_rna_viral_1_to_2yr,
count(case when test_name = 'hiv_rna_viral' and date < :end_calc_year::date - interval '2 year' then 1 end) as hiv_rna_viral_gt_2yr,
-- FOR CONT OF CARE REPORTING
max(case when date >= :end_calc_year::date - interval '1 year' then 1 end) as hiv_test_this_yr
FROM hiv_rpt.hivstirs_all_hiv_events_nohef
group by patient_id;


-- HIV VIRAL LOADS
DROP TABLE IF EXISTS hiv_rpt.hivstirs_viral_loads;
CREATE TABLE hiv_rpt.hivstirs_viral_loads AS
SELECT T1.patient_id, min(date) max_viral_load_date, max_viral_load, date_part('year', age(:end_calc_year::date, min(date))) as hiv_max_viral_load_yr
FROM 
(SELECT patient_id, max(result_float) as max_viral_load
        FROM hiv_rpt.hivstirs_all_hiv_events
        WHERE test_name = 'hiv_rna_viral'
        AND result_float > 0
        GROUP BY patient_id) T1
INNER JOIN hiv_rpt.hivstirs_all_hiv_events T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.max_viral_load = T2.result_float
GROUP BY T1.patient_id, max_viral_load;

-- HIV MEDS

--BREAKING INTO 2 QUERIES FOR EFFICIENCY

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_meds_hef;
CREATE TABLE hiv_rpt.hivstirs_hiv_meds_hef as
SELECT T1.patient_id, name as hef_name, object_id
FROM hiv_rpt.hivstirs_index_patients T1
JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.name ilike 'rx:hiv%'
AND T2.date <= :end_calc_year::date;


DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_meds;
CREATE TABLE hiv_rpt.hivstirs_hiv_meds as
SELECT T1.patient_id, T2.name, hef_name, T2.date, T2.quantity, T2.quantity_float, T2.refills, T2.start_date, T2.end_date, 
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1) ELSE 1 END refills_mod,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
-- --Default to 30 for missing quantity values for Truvada/Descovy
-- CASE WHEN refills~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN ((refills::real +1) * 30)
     -- WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
	 -- WHEN refills!~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN 30
	 -- WHEN refills is null AND (quantity_float is null or quantity_float = 0) THEN 30
     -- WHEN refills is null then quantity_float end as total_prep_oral_quantity_per_rx,
--given every 8 weeks so each prescription for cabotegravir should count as 2 prescriptions
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * 2) 
     WHEN refills!~E'^\\d+$' or refills is null THEN 2
	 ELSE 2 end as num_monthly_prep_inj_rx,
CASE WHEN hef_name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') THEN 1 ELSE 0 END as rx_oral_prep,
CASE WHEN hef_name in ('rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END as rx_inj_prep,
CASE WHEN hef_name not in ('rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END other_hiv_rx_non_prep, --really not non-prep anymore due to change request but leaving the variable name
string_to_array(replace(split_part(hef_name, ':', 2), 'hiv_',''), '-') med_array,
CASE WHEN hef_name in ('rx:hiv_dolutegravir-lamivudine', 
                      'rx:hiv_dolutegravir-lamivudine:generic',
					  'rx:hiv_dolutegravir-rilpivirine',
                      'rx:hiv_dolutegravir-rilpivirine:generic',
					  'rx:hiv_cabotegravir-rilpivirine',
                      'rx:hiv_cabotegravir-rilpivirine:generic') THEN 1 END hiv_trmt_reg
FROM hiv_rpt.hivstirs_hiv_meds_hef T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id and T2.id = T1.object_id);


-- DON'T COUNT TDF/FTC and TAF/FTC as non HIV med if patient has a history of HepB
UPDATE hiv_rpt.hivstirs_hiv_meds
SET other_hiv_rx_non_prep = 0
WHERE hef_name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                   'rx:hiv_tenofovir-emtricitabine', 
                   'rx:hiv_tenofovir_alafenamide-emtricitabine', 
				   'rx:hiv_tenofovir_alafenamide-emtricitabine:generic')
AND (  patient_id in (select patient_id from hiv_rpt.hivstirs_all_hepb_diags where hep_b_dx = 1)
       OR patient_id in (select patient_id from hiv_rpt.hivstirs_all_hep_events_hef_counts where hbv_sag_or_dna_pos_ev = 1)
	);
	
-- HIV CASE RELATED VARIABLES
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_case_values;
CREATE TABLE hiv_rpt.hivstirs_hiv_case_values AS
SELECT T1.patient_id, --T2.test_name, T3.case_date
MAX(CASE WHEN T3.case_date is null and other_hiv_rx_non_prep = 1 THEN 1 
         WHEN T3.case_date is null and (other_hiv_rx_non_prep = 0 or other_hiv_rx_non_prep is null) THEN 0
	ELSE 2 END) as hiv_neg_w_hiv_med_ev
FROM hiv_rpt.hivstirs_index_patients T1
LEFT JOIN hiv_rpt.hivstirs_all_hiv_events T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_rpt.hivstirs_cases T3 ON (T1.patient_id = T3.patient_id and T3.condition = 'hiv')
LEFT JOIN hiv_rpt.hivstirs_hiv_meds T4 ON (T1.patient_id = T4.patient_id)
GROUP BY T1.patient_id;

-- HIV MEDS Break down the combo meds in to distinct individual meds.
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_rx_combo_distinct;
CREATE TABLE hiv_rpt.hivstirs_hiv_rx_combo_distinct AS
select distinct unnest(med_array) as distinct_med, patient_id,
date_part('year', age(:end_calc_year::date, date)) med_age_in_yrs, hiv_trmt_reg
from hiv_rpt.hivstirs_hiv_meds
group by patient_id, med_array, med_age_in_yrs, hiv_trmt_reg;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_rx_3_diff;
CREATE TABLE hiv_rpt.hivstirs_hiv_rx_3_diff AS 
SELECT T1.patient_id,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1) and med_age_in_yrs =0 then 1 else 0 end hiv_trmt_regimen_1yr,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1)and med_age_in_yrs in (1,2) then 1 else 0 end hiv_trmt_regimen_1_to2yr,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1) and med_age_in_yrs >2 then 1 else 0 end hiv_trmt_regimen_gt_2yr
FROM hiv_rpt.hivstirs_hiv_rx_combo_distinct T1
-- exclude this prep only combo
WHERE distinct_med != 'cabotegravir_er_600'
GROUP BY T1.patient_id, med_age_in_yrs, hiv_trmt_reg;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_rx_3_diff_count;
CREATE TABLE hiv_rpt.hivstirs_hiv_rx_3_diff_count AS
SELECT T1.patient_id,
MAX(hiv_trmt_regimen_1yr) hiv_trmt_regimen_1yr, 
MAX(hiv_trmt_regimen_1_to2yr) hiv_trmt_regimen_1_to2yr,
MAX(hiv_trmt_regimen_gt_2yr) hiv_trmt_regimen_gt_2yr
FROM hiv_rpt.hivstirs_hiv_rx_3_diff T1
GROUP BY T1.patient_id;

-- ORAL PEP & PREP (Truvada/Descovy)

-- IDENTIFY PEP RX
DROP TABLE IF EXISTS hiv_rpt.hivstirs_pep_meds;
CREATE TABLE hiv_rpt.hivstirs_pep_meds as
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= :end_calc_year::date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= :end_calc_year::date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, name
            FROM hef_event T1
			INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= :end_calc_year::date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name in ('rx:hiv_lopinavir-ritonavir', 'rx:hiv_lopinavir-ritonavir:generic')
UNION
SELECT T1.patient_id, T1.date as pep_date
FROM hef_event T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE name in ('bictegravir-emtricitabine-tenofovir_alafenamide', 'bictegravir-emtricitabine-tenofovir_alafenamide:generic')
AND T1.date <= :end_calc_year::date
GROUP BY T1.patient_id, pep_date;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_pep_counts;
CREATE TABLE hiv_rpt.hivstirs_hiv_pep_counts AS
SELECT count(distinct(pep_date)) as pep_rx_dates_num, patient_id
FROM hiv_rpt.hivstirs_pep_meds
GROUP BY patient_id;

--DELETE PEP MEDS FROM ORAL PREP
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_meds_no_pep;
CREATE TABLE hiv_rpt.hivstirs_hiv_meds_no_pep AS
SELECT * 
FROM hiv_rpt.hivstirs_hiv_meds
WHERE rx_oral_prep = 1
EXCEPT
SELECT T1.*
FROM hiv_rpt.hivstirs_hiv_meds T1
INNER JOIN hiv_rpt.hivstirs_pep_meds T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.date = T2.pep_date
AND rx_oral_prep = 1;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_oral_prep_all;
CREATE TABLE hiv_rpt.hivstirs_hiv_oral_prep_all as 
SELECT DISTINCT T1.patient_id, date, refills_mod, total_quantity_per_rx, 
date + total_quantity_per_rx::int as derived_end_date, 
(total_quantity_per_rx/30) as num_monthly_rx,
date_part('year', age(:end_calc_year::date, date)) rx_age_in_yrs,
(case when date >= :end_calc_year::date - interval '1 year' then '0-1'
           when ((date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years')) then '1-2'
           when date < :end_calc_year::date - interval '2 year' then '2+' end) as med_date_range
FROM hiv_rpt.hivstirs_hiv_meds_no_pep T1
LEFT JOIN hiv_rpt.hivstirs_cases T2 ON (T1.patient_id = T2.patient_id and T2.condition = 'hiv')
WHERE rx_oral_prep = 1
--only inlcude meds before HIV case
AND (T1.date < T2.case_date or T2.case_date is null)
GROUP BY T1.patient_id, date, refills_mod, total_quantity_per_rx;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_oral_prep_compute;
CREATE TABLE hiv_rpt.hivstirs_hiv_oral_prep_compute as 
SELECT patient_id, med_date_range,
max(derived_end_date) - min(date) date_diff_for_rx,
sum(num_monthly_rx) num_monthly_rx
FROM hiv_rpt.hivstirs_hiv_oral_prep_all
GROUP BY patient_id, med_date_range;

-- Must have 2 rx and be greater than 2 months apart
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_oral_prep_eval;
CREATE TABLE hiv_rpt.hivstirs_hiv_oral_prep_eval as 
SELECT * 
FROM hiv_rpt.hivstirs_hiv_oral_prep_compute
WHERE date_diff_for_rx >= 60 and num_monthly_rx >=2;

-- INJ PREP
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_inj_prep_compute;
CREATE TABLE hiv_rpt.hivstirs_hiv_inj_prep_compute as 
SELECT T1.patient_id, date, num_monthly_prep_inj_rx, 
(case when date >= :end_calc_year::date - interval '1 year' then '0-1'
           when ((date < :end_calc_year::date - interval '1 year') and (date >= :end_calc_year::date - interval '2 years')) then '1-2'
           when date < :end_calc_year::date - interval '2 year' then '2+' end) as med_date_range
FROM hiv_rpt.hivstirs_hiv_meds T1
LEFT JOIN hiv_rpt.hivstirs_cases T2 ON (T1.patient_id = T2.patient_id and T2.condition = 'hiv')
WHERE rx_inj_prep = 1
--only inlcude meds before HIV case
AND (T1.date < T2.case_date or T2.case_date is null)
GROUP BY T1.patient_id, date, num_monthly_prep_inj_rx;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_inf_prep_eval;
CREATE TABLE hiv_rpt.hivstirs_hiv_inf_prep_eval as 
SELECT patient_id, sum(num_monthly_prep_inj_rx) as num_monthly_rx, med_date_range
FROM hiv_rpt.hivstirs_hiv_inj_prep_compute
GROUP BY patient_id, med_date_range;

-- ORAL AND INJ PREP TOGETHER
DROP TABLE IF EXISTS hiv_rpt.hivstirs_prep_union;
CREATE TABLE hiv_rpt.hivstirs_prep_union AS
SELECT patient_id, med_date_range, num_monthly_rx FROM hiv_rpt.hivstirs_hiv_oral_prep_eval
UNION
SELECT patient_id, med_date_range, num_monthly_rx FROM hiv_rpt.hivstirs_hiv_inf_prep_eval;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_prep_union_counts;
CREATE TABLE hiv_rpt.hivstirs_prep_union_counts AS
SELECT patient_id, med_date_range, sum(num_monthly_rx) num_monthly_rx
FROM hiv_rpt.hivstirs_prep_union
GROUP BY patient_id, med_date_range;

--FINAL VALUES
DROP TABLE IF EXISTS hiv_rpt.hivstirs_prep_final;
CREATE TABLE hiv_rpt.hivstirs_prep_final AS
SELECT patient_id, 
MAX(CASE WHEN med_date_range = '0-1' THEN round(num_monthly_rx) END) as prep_rx_num_1yr,
MAX(CASE WHEN med_date_range = '1-2' THEN round(num_monthly_rx) END) as prep_rx_num_1_to_2yr,
MAX(CASE WHEN med_date_range = '2+'  THEN round(num_monthly_rx) END) as prep_rx_num_gt_2yr,
-- ADDED FOR MAGIC AND OTHER REPORTING ETC
MAX(CASE WHEN med_date_range = '0-1' THEN 1 END) as prep_rx_this_yr
FROM hiv_rpt.hivstirs_prep_union_counts
GROUP BY patient_id;

-- HIV DX CODES
-- GROUP SO 2 DX ON SAME DATE DON'T COUNT AS 2 
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_dx_codes;
CREATE TABLE hiv_rpt.hivstirs_hiv_dx_codes AS 
SELECT T1.patient_id, T2.date as dx_date, 1::int as hiv_dx 
FROM hiv_rpt.hivstirs_index_patients T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date <= :end_calc_year::date
AND (dx_code_id ~ '^(icd9:042|icd9:V08|icd9:079.53|icd9:795.71|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)')
GROUP BY T1.patient_id, dx_date, hiv_dx;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hiv_dx_code_counts;
CREATE TABLE hiv_rpt.hivstirs_hiv_dx_code_counts AS 
SELECT patient_id,
count(case when dx_date >= :end_calc_year::date - interval '1 year' then 1 end) as hiv_dx_1_yr,
count(case when (dx_date < :end_calc_year::date - interval '1 year') and (dx_date >= :end_calc_year::date - interval '2 years') then 1 end) as hiv_dx_1_to_2yr,
count(case when dx_date < :end_calc_year::date - interval '2 year' then 1 end) as hiv_dx_gt_2yr
FROM hiv_rpt.hivstirs_hiv_dx_codes
GROUP BY patient_id;

-- DX CODES OTHER
DROP TABLE IF EXISTS hiv_rpt.hivstirs_dx_codes;
CREATE TABLE hiv_rpt.hivstirs_dx_codes AS 
SELECT T1.patient_id, T2.date as dx_date, T3.dx_code_id
FROM hiv_rpt.hivstirs_index_patients T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date <= :end_calc_year::date
AND (
 dx_code_id ~ '^(icd9:796.7|icd10:R85.61|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:A59.|icd10:A63.|icd10:Z72.5|icd10:F50.0|icd10:T74.5|icd10:T74.2|icd10:T74.5|icd10:T18.5|icd9:305.0|icd9:303.9|icd10:F10.1|icd10:F10.2|icd9:304.0|icd10:F11.|icd9:305.5|icd9:304.1|icd9:305.4|icd10:F13.|icd9:304.2|icd9:305.6|icd10:F14.|icd9:304.4|icd9:305.7|icd10:F15.|icd9:304.9|icd10:F19)'
OR 
 dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd9:569.44', 'icd10:K62.82', 'icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:091.1', 'icd10:A51.1', 'icd9:098.7','icd10:A54.6',
                'icd9:098.6','icd10:A54.5', 'icd9:099.52','icd10:A56.3', 'icd9:099.51','icd10:A56.4', 'icd9:099.1','icd10:A55', 'icd9:099.0', 'icd10:A57', 'icd9:099.2', 'icd10:A58', 'icd10:N34.1',
				'icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd9:078.11', 'icd10:A63.0', 'icd9:569.41', 'icd10:K62.6', 'icd10:B04', 'icd10:A64', 'icd9:099.9',
				'icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0', 'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9', 'icd9:V01.6', 
				'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V69.2', 'icd9:V65.44','icd10:Z71.7', 'icd9:307.1', 'icd9:307.51','icd10:F50.2', 'icd9:307.50','icd10:F50.9', 'icd9:302.0', 'icd9:302.6',
				'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:F64.1',
				'icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z62.810', 'icd9:937', 'icd9:V74.5', 'icd10:Z11.3', 'icd9:V76.41', 'icd10:Z12.12', 'icd10:W46.0XXA', 'icd10:W46.1XXA', 'icd10:Z29.9', 'icd9:V07.8', 'icd9:V07.9', 'icd9:E920.5')
);

DROP TABLE IF EXISTS hiv_rpt.hivstirs_dx_code_years;
CREATE TABLE hiv_rpt.hivstirs_dx_code_years AS 
SELECT 
	patient_id,
	MAX(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' 
	         OR dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd9:569.44', 'icd10:K62.82') then date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_abn_anal_cyt_yrs,
    MAX(case when dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7)'
	         OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_syph_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:091.1', 'icd10:A51.1') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_anal_syph_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:098.7','icd10:A54.6') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_gon_inf_rectum_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:098.6','icd10:A54.5') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_gon_pharyn_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.52','icd10:A56.3') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_chlam_inf_rectum_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.51','icd10:A56.4') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_chlam_pharyn_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.1','icd10:A55') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_lymph_ven_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.0','icd10:A57') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_chancroid_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.2', 'icd10:A58') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_granuloma_ingu_yrs,
	MAX(CASE WHEN (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_nongon_ureth_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_hsv_w_compl_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_genital_herpes_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:078.11', 'icd10:A63.0') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_anogenital_warts_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:569.41', 'icd10:K62.6') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_anorectal_ulcer_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd10:A59.)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_trich_yrs,
	MAX(CASE WHEN dx_code_id in ('icd10:B04') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_mpox_yrs,
	MAX(CASE WHEN (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.9')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_unspec_std_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0', 'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_pid_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_cont_w_exp_to_vd_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_high_risk_sex_behav_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_hiv_counsel_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_anorexia_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:307.51','icd10:F50.2') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_bulimia_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:307.50','icd10:F50.9') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_eat_disorder_nos_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_gend_iden_dis_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:F64.1') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_transsexualism_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_couns_or_prb_child_sex_abuse_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:T74.5|icd10:T74.2)' or dx_code_id in ('icd10:Z62.810')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_adult_child_sex_exp_or_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd10:T74.5)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_adult_or_child_phys_abuse_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_foreign_body_anus_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:305.0|icd9:303.9|icd10:F10.1|icd10:F10.2)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_alcohol_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_opioid_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_sed_hypn_anxio_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_cocaine_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_amphet_stim_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.9|icd10:F19)' THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V74.5', 'icd10:Z11.3') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_sti_screen_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V76.41', 'icd10:Z12.12') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_malig_neo_rec_screen_yrs,
	MAX(CASE WHEN dx_code_id in ('icd10:W46.0XXA', 'icd10:W46.1XXA', 'icd9:E920.5') THEN date_part('year', age(:end_calc_year::date, dx_date)) END) as dx_needle_stick
FROM hiv_rpt.hivstirs_dx_codes	
GROUP BY patient_id;

--
-- BICILLIN MEDS
--

DROP TABLE IF EXISTS hiv_rpt.hivstirs_bicillin_meds;
CREATE TABLE hiv_rpt.hivstirs_bicillin_meds AS
SELECT count(distinct(T1.id)) as rx_bicillin_num_rx, T2.patient_id, 1 as rx_bicillin_indicator, T1.name, T1.date, T1.dose
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_risk_bicillin_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T1.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T1.directions is null and T1.dose is null)
OR
--quantity_float exact
(T3.match_type = 'q_float_exact' and T1.quantity_float = T3.quantity_float::int)
OR
--quantity_float exact or null quantity_float
(T3.match_type = 'q_float_or_null_q_float' and (T1.quantity_float is null or T1.quantity_float = T3.quantity_float::numeric))
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id, T1.name, T1.date, T1.dose;

-- At CHA need to have 2 rx on the same day (to handle how CHA writes the prescriptions) for 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP'
DROP TABLE IF EXISTS hiv_rpt.hivstirs_bicillin_meds_updated;
CREATE TABLE hiv_rpt.hivstirs_bicillin_meds_updated AS
SELECT patient_id, rx_bicillin_indicator as rx_bicillin_ever FROM hiv_rpt.hivstirs_bicillin_meds
EXCEPT
SELECT patient_id, rx_bicillin_indicator as rx_bicillin_ever FROM hiv_rpt.hivstirs_bicillin_meds
    WHERE name in('PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSY') AND (dose in ('1.2') OR dose is null)
	AND rx_bicillin_num_rx < 2;

--AZITH
-- Azithromycin 1g PO ever

DROP TABLE IF EXISTS hiv_rpt.hivstirs_azith_meds_compare;
CREATE TABLE hiv_rpt.hivstirs_azith_meds_compare AS
SELECT T2.patient_id, 1::int as rx_azith_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_sti_risk_azith_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T1.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T1.directions is null and T1.dose is null)
OR
--quantity_float exact
(T3.match_type = 'q_float_exact' and T1.quantity_float = T3.quantity_float::int)
OR
--quantity_float exact or null quantity_float
(T3.match_type = 'q_float_or_null_q_float' and (T1.quantity_float is null or T1.quantity_float = T3.quantity_float::numeric))
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;



DROP TABLE IF EXISTS hiv_rpt.hivstirs_azith_meds;
CREATE TABLE hiv_rpt.hivstirs_azith_meds AS
SELECT T2.patient_id, 1::int as rx_azith_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--BMC
name = 'AZITHROMYCIN 1 GRAM ORAL PACKET'  --ATRIUS TOO
OR (name = 'AZITHROMYCIN 250 MG TABLET' and dose = '1000 mg')
OR (name = 'AZITHROMYCIN 250 MG TABLET' and dose = '250 mg' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TABLET' and quantity_float = 4) --ATRIUS TOO
OR (name = 'AZITHROMYCIN 500 MG TABLET' and dose = '1000 mg') --ATRIUS TOO
OR (name = 'AZITHROMYCIN 500 MG TABLET' and dose = '500 mg' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG TABLET' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TABLET' and dose = '1000 mg')
OR (name = 'ZITHROMAX Z-PAK 250 MG TABLET' and dose = '1000 mg')
OR (name = 'ZITHROMAX Z-PAK 250 MG TABLET' and dose = '250 mg' and quantity_float = 4)
OR (name = 'ZITHROMAX Z-PAK 250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
--ATRIUS
OR (name = 'AZITHROMYCIN 250 MG CAP' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG CAPSULE' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TAB' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 500 MG TAB' and quantity_float = 2)
OR name = 'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)'
OR (name = 'AZITHROMYCIN CHLAMYDIA RX (250 MG)' and quantity_float = 4)
OR (name = 'AZITHROMYCIN CHLAMYDIA RX (500 MG)' and quantity_float = 2)
OR name = 'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)'
OR (name = 'ZITHROMAX 250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG TAB (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX Z-PAK 250 MG CAP (AZITHROMYCIN)' and quantity_float = 4)
-- CHA
OR name = 'AZITHROMYCIN 1 G PO PACK' --FENWAY TOO
OR (name = 'AZITHROMYCIN 250 MG OR CAPS' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG PO TABS' and (dose in ('1000', '1000 mg') or quantity_float = 4)) --FENWAY TOO
OR (name = 'AZITHROMYCIN 500 MG PO TABS' and (dose in ('1000', '1000 mg') or quantity_float = 2)) --FENWAY TOO
OR name = 'ZITHROMAX 1 G PO PACK'
OR (name = 'ZITHROMAX 250 MG OR CAPS' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG PO TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX Z-PAK 250 MG OR CAPS' and quantity_float = 4)
--FENWAY
OR name = 'AZITHROMYCIN 1G'
OR name = 'AZITHROMYCIN 1 GM ORAL PACK'
OR name = 'AZITHROMYCIN 1 GM ORAL PACKET'
OR name = 'AZITHROMYCIN 1 GM PACK'
OR name = 'AZITHROMYCIN 1 GRAM'
OR name = 'azithromycin 1 gram packet'
OR (name = 'AZITHROMYCIN 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG ORAL TABS' and quantity_float = 4)
OR (name = 'azithromycin 250 mg tablet' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TABS' and quantity_float = 4)
OR (name = '(AZITHROMYCIN) 500MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG ORAL TABLET' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG ORAL TABS' and quantity_float = 2)
OR (name = '(AZITHROMYCIN) 500MG TAB' and quantity_float = 2)
OR (name = 'azithromycin 500 mg tablet' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG TABLETS' and quantity_float = 2) 
OR (name = 'AZITHROMYCIN 500 MG TABS' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG  TABS' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG TABS (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG TABS (AZITHROMYCIN)' and quantity_float = 2)
OR name = 'Med:  Zithromax 1gr slurry PO'
OR name = 'ZITHROMAX 1 GM ORAL PACKET'
OR name = 'ZITHROMAX 1 GM PACK'
OR (name = 'ZITHROMAX 250MG' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG ORAL TABS' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG TAB' and quantity_float = 4)
OR (name = 'ZITHROMAX  250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG  TABS' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG ORAL TABLET' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MGS' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TAB' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG  TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX CAP 250MG' and quantity_float = 2)
OR (name = 'Zithromax Dose' and dose = '1000')
OR name = 'ZITHROMAX POW 1GM PAK'
OR (name = 'ZITHROMAX TAB 250MG' and quantity_float = 4)
OR (name = 'ZITHROMAX TAB 5000MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TAB 500 MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TAB 500MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TABS 250 MG' and quantity_float = 4)
OR (name = 'ZITHROMAX Z-PAK 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG.' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG CAP' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG TABS' and quantity_float = 4)

)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;

--CEFT
-- Ceftriaxone 125mg or 250mg or 500mg IV/IM ever 

DROP TABLE IF EXISTS hiv_rpt.hivstirs_ceft_meds_compare;
CREATE TABLE hiv_rpt.hivstirs_ceft_meds_compare AS
SELECT T2.patient_id, 1::int as rx_ceft_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_sti_risk_ceft_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T1.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T1.directions is null and T1.dose is null)
OR
--quantity_float exact
(T3.match_type = 'q_float_exact' and T1.quantity_float = T3.quantity_float::int)
OR
--quantity_float exact or null quantity_float
(T3.match_type = 'q_float_or_null_q_float' and (T1.quantity_float is null or T1.quantity_float = T3.quantity_float::numeric))
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;



DROP TABLE IF EXISTS hiv_rpt.hivstirs_ceft_meds;
CREATE TABLE hiv_rpt.hivstirs_ceft_meds AS
SELECT T2.patient_id, 1::int as rx_ceft_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--ATRIUS
(name = 'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION' and (dose in ('.125 g', '125 g', '125 mg', '150 mg', '250 mg', '.25 g', '500 mg', '.5 g') or quantity_float in (0.5, 1, 2, 125, 250, 500) or (dose is null and quantity_float is null) ) )-- BMC TOO
OR (name = 'CEFTRIAXONE 500 MG SOLUTION FOR INJECTION' and (dose in ('1 g', '250 mg', '.25 g', '500 mg', '.5 g') or quantity_float in (0, 1, 250, 500) or (dose is null and quantity_float is null) ) ) --BMC TOO
OR name = 'CEFTRIAXONE 500 MG WITH LIDOCAINE 1 % IM KIT'
OR (name = 'CEFTRIAXONE-LIDOCAINE 500 MG IM KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)' and (quantity_float in (1) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)' and (quantity_float in (0, 0.5, 1, 125, 250, 500) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 500 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)' and (quantity_float in (1, 250, 500)  or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN IM CONVENIENCE 500 MG-1 % KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)' and (quantity_float in (1, 250, 500) or (dose is null and quantity_float is null) ) )
--BMC
OR (name = 'CEFTRIAXONE 2 GRAM SOLUTION FOR INJECTION' and dose in ('250 g') )
OR (name = 'CEFTRIAXONE 1 G IN LIDOCAINE IM INJECTION' and dose in ('500 mg') )
OR (name = 'CEFTRIAXONE 1 GRAM SOLUTION FOR INJECTION' and dose in ('125 mg', '250 mg', '500 mg', '.5 g') )
OR (name = 'CEFTRIAXONE 1 GRAM SOLUTION IM INJECTION' and dose in ('125 mg', '250 mg', '500 mg', '.5 g') )
OR (name = 'CEFTRIAXONE 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)' and (dose in ('1 g', '.5 g', '250 mg', '.25 g', '500 mg') or quantity_float in (2)) )
OR (name = 'CEFTRIAXONE 250 MG IN LIDOCAINE IM INJECTION' and dose in ('250 mg') )
OR (name = 'CEFTRIAXONE 500 MG/5 ML IJ (RECONSTITUTED VIAL)' and dose in ('1 g', '500 mg', '.5 g') )
OR name = 'CEFTRIAXONE 500 MG IM INJECTION'
OR (name = 'CEFTRIAXONE 500 MG IN LIDOCAINE IM INJECTION' and dose in ('250 mg', '500 mg'))
OR name = 'CEFTRIAXONE 500 MG IN SWFI IM INJECTION'
OR (name = 'CEFTRIAXONE IM INJECTION <=250 MG' and dose = '250 mg')
OR (name = 'CEFTRIAXONE IM SYR >= 250 MG' and (dose = '250 mg' or quantity_float = '250'))
OR (name = 'CEFTRIAXONE IVPB IN 50 ML' and dose in ('250 mg', '.25 g'))
OR name = 'CEFTRIAXONE SODIUM 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)'
OR name = 'CEFTRIAXONE SODIUM 500 MG/5 ML IJ (RECONSTITUTED VIAL)'
OR name = 'CEFTRIAXONE 500 MG SOLUTION IM INJECTION'
--CHA
OR (name = 'CEFTRIAXONE IVPB' and dose in ('250', '500'))
OR (name = 'CEFTRIAXONE SODIUM 250 MG IJ SOLR' and (dose in ('250', '500', '250 mg', '500 mg', 'None') or quantity_float in (0, 0.5, 1, 2, 10, 125, 250, 500) or (dose is null and quantity_float is null) ) ) --FENWAY TOO
OR (name = 'CEFTRIAXONE SODIUM 500 MG IJ SOLR' and (dose in ('1', '250', '.5', '500', '250 mg', '500 mg') or quantity_float in (0.5, 1, 500) or (dose is null and quantity_float is null) ) )  --FENWAY TOO
OR (name = 'CEFTRIAXONE SODIUM-LIDOCAINE 500 MG IM KIT' and (quantity_float in (1, 500) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 250 MG IJ SOLR' and (quantity_float in (0, 1, 2, 10, 125, 250) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 500 MG IJ SOLR' and (quantity_float in (1, 500) or (dose is null and quantity_float is null) ) )
OR name = 'ROCEPHIN IM CONVENIENCE 500-1 MG-% IM KIT'
OR (name = 'CEFTRIAXONE SODIUM 1 G IJ SOLR' and dose in ('.5', '250', '500') )
--FENWAY
OR name = 'CEFTRIAXONE 250MG'
OR name = 'ceftriaxone 250 mg recon soln'
OR name = 'ceftriaxone 500 mg recon soln'
OR name = 'CEFTRIAXONE EA 250MG'
OR name = 'CEFTRIAXONE EA 500MG'
OR name = 'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)'
OR name = 'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED'
OR name = 'CEFTRIAXONE SODIUM 250 MG INJ SOLR'
OR name = 'CEFTRIAXONE SODIUM 250 MG SOLR'
OR name = 'CEFTRIAXONE SODIUM 500 MG INJECTION SOLUTION RECONSTITUTED'
OR (name = 'CEFTRIAXONE SODIUM POWD' and quantity_float in (125,250,500) )
OR (name = 'CEFTRIAXONE SODIUM POWDER' and quantity_float in (125,250,500) )
OR (name = 'CEFTRIAXONE SODIUM PWDR' and quantity_float in (125,250,500) )
OR name = 'Med:  Rocephin  250mg IM'
OR name = 'Rocephin 250 mg'
OR name = 'ROCEPHIN 250 MG'
OR name = 'ROCEPHIN 250 MG INJ RECON SOL'
OR name = 'ROCEPHIN 250 MG INJ SOLR'
OR name = 'ROCEPHIN 500 MG INJECTION SOLUTION RECONSTITUTED'
OR name = 'ROCEPHIN 500 MG INJ SOLR'
OR (name = 'Rocephin (Ceftriaxone) Injection' and quantity_float in (125,250,500) )
OR name = 'ROCEPHIN INJ 250MG'
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;

--DOXY
-- Doxycycline 100mg PO bid for ≥14 days or Doxycycline 100mg PO x 2 tabs (dispense ≥30 tabs) ever  [Counting matching 100mg rx with quantity >= 28] 

DROP TABLE IF EXISTS hiv_rpt.hivstirs_doxy_meds_compare;
CREATE TABLE hiv_rpt.hivstirs_doxy_meds_compare AS
SELECT T2.patient_id, 1::int as rx_doxy_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_sti_risk_doxy_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T1.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T1.directions is null and T1.dose is null)
OR
--quantity_float exact
(T3.match_type = 'q_float_exact' and T1.quantity_float = T3.quantity_float::int)
OR
--quantity_float exact or null quantity_float
(T3.match_type = 'q_float_or_null_q_float' and (T1.quantity_float is null or T1.quantity_float = T3.quantity_float::numeric))
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;




DROP TABLE IF EXISTS hiv_rpt.hivstirs_doxy_meds;
CREATE TABLE hiv_rpt.hivstirs_doxy_meds AS
SELECT T2.patient_id, 1::int as rx_doxy_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--ATRIUS
(name = 'ADOXA 100 MG TAB (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'ADOXA PAK 100 MG TAB (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'DORYX 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DORYX 100 MG TAB (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXY-CAPS 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE CHLAMYDIA RX (100 MG CAP)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAP' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAP, DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPSULE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPSULE,DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TAB' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TAB, DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABLET' and quantity_float >= 28) --BMC & FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABLET,DELAYED RELEASE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAPSULE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG TABLET' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXY-LEMMON 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'MONODOX 100 MG CAP (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'VIBRAMYCIN 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'VIBRA-TABS 100 MG TAB (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
--BMC
OR (name = 'VIBRAMYCIN 100 MG CAPSULE' and quantity_float >= 28)
--CHA
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO CAPS' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO CPEP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO TABS' and quantity_float >= 28) --FENWAY TOO 
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO TBEC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG PO CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG PO TABS' and quantity_float >= 28) --FENWAY TOO
OR (name = 'MONODOX 100 MG PO CAPS' and quantity_float >= 28)
OR (name = 'VIBRAMYCIN 100 MG PO CAPS' and quantity_float >= 28)
--FENWAY
OR (name = 'ADOXA 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'ADOXA 100 MG  TABS' and quantity_float >= 28)
OR (name = 'ADOXA PAK 1/100 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DORYX 100 MG TBEC' and quantity_float >= 28)
OR (name = 'DORYX 100 MG  TBEC' and quantity_float >= 28)
OR (name = 'DORYX CAP 100MG EC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG CAP 500CT 340B' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE  100 MG CAPS (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYC 100MG CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYC 100MG TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG  CAPS' and quantity_float >= 28)
OR (name = 'doxycycline hyclate 100 mg capsule' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100MG CAPSULES' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CPEP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABLET DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TBEC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG SOLR' and quantity_float >= 28)
OR (name = 'doxycycline hyclate 100 mg tablet' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100MG TABLETS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG  TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE CAPS 100 MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAPS' and quantity_float >= 28)
OR (name = 'doxycycline monohydrate 100 mg capsule' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL CAPSULE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL TABS' and quantity_float >= 28)
OR (name = 'doxycycline monohydrate 100 mg tablet' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG  TABS' and quantity_float >= 28)
)
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_other_meds;
CREATE TABLE hiv_rpt.hivstirs_other_meds AS
SELECT T2.patient_id,
MAX(CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end) rx_methadone_ever,
MAX(CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike '%buprenorphine%' 
	or T1.name ilike '%bunavail%'
	or T1.name ilike '%cassipa%'
	or T1.name ilike '%zubsolv%' 
	or T1.name ilike '%sublocade%' 
	or T1.name ilike '%brixadi%' then 1 end) rx_buprenorphine_ever,
MAX(CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end) rx_viagara_cilais_or_levitra_ever, 
MAX(CASE WHEN
    T1.name ilike '%naltrexone%'
	or T1.name ilike 'vivitrol%'
	or T1.name ilike 'revia%' then 1 end) rx_naltrexone_ever
FROM emr_prescription T1
INNER JOIN hiv_rpt.hivstirs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%' 
	or name ilike '%suboxone%'  
	or name ilike '%buprenorphine%'
	or name ilike '%bunavail%' 
	or name ilike '%cassipa%' 
	or name ilike '%zubsolv%' 
	or name ilike '%sublocade%'
	or name ilike '%brixadi%'
	or name ilike '%tadalafil%'  
	or name ilike '%cilais%'  
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%' 
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%'  
	or name ilike '%viagra%'
	or name ilike '%naltrexone%'
	or name ilike 'vivitrol%'
    or name ilike 'revia%'
	) 
AND name not ilike '%revatio%'
AND name not ilike '%antihypertensive%'
AND name not ilike '%hypertension%'
AND name not ilike '%adcirca%'
AND name not ilike '%alyq%'
AND name not ilike '%methylnaltrexone%'
AND name not ilike '%contrave%'
AND name not ilike '%morphine%'
--FENWAY ISSUES
AND name not ilike '%Methadone screen, urine%'
AND name not ilike '%Age of onset of Methadone use%'
AND name not ilike '%Rationale for suboxone use%'
AND T1.date <= :end_calc_year::date
GROUP BY T2.patient_id;

--MPOX
DROP TABLE IF EXISTS hiv_rpt.hivstirs_mpox_vax;
CREATE TABLE hiv_rpt.hivstirs_mpox_vax AS
SELECT T1.patient_id,
MAX(CASE WHEN upper(T2.name) ~ ('MPOX|MONKEYPOX|JYNNEOS') THEN 1 END) as vax_mpox_ever
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN emr_immunization T2 ON (T1.patient_id = T2.patient_id)
WHERE upper(T2.name) ~ ('MPOX|MONKEYPOX|JYNNEOS')
AND T2.date <= :end_calc_year::date
GROUP BY T1.patient_id;

--HPV VAX FOR MALE >=27
DROP TABLE IF EXISTS hiv_rpt.hivstirs_hpv_vax;
CREATE TABLE hiv_rpt.hivstirs_hpv_vax AS
SELECT T1.patient_id,
MAX(CASE WHEN upper(T2.name) ~ ('HPV') THEN 1 END) as vax_hpv_male_ever
FROM hiv_rpt.hivstirs_index_patients T1
INNER JOIN emr_immunization T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_patient T3 ON (T1.patient_id = T3.id)
WHERE upper(T2.name) ~ ('HPV')
AND upper(T3.gender) in ('M', 'MALE')
AND date_part('year', age(:end_calc_year::date, date_of_birth)) >= 27
AND T2.date <= :end_calc_year::date
GROUP BY T1.patient_id;

-- Bring together all of the fields and values
DROP TABLE IF EXISTS hiv_rpt.hivstirs_index_data;
CREATE TABLE hiv_rpt.hivstirs_index_data AS
SELECT T1.patient_id,
rpt_year,
age_on_anchor_date,
coalesce(gender_mapped, 0) as gender_mapped,
coalesce(gender_male, 0) as gender_male,
coalesce(gender_female, 0) as gender_female,
coalesce(gender_trans, 0) as gender_trans,
coalesce(birth_sex_male, 0) as birth_sex_male,
coalesce(birth_sex_unk, 1) as birth_sex_unk,
coalesce(sexor_straight, 0) as sexor_straight,
coalesce(sexor_gay_lesbian, 0) as sexor_gay_lesbian,
coalesce(sexor_other, 0) as sexor_other,
coalesce(gender_iden_male, 0) as gender_iden_male,
coalesce(gender_iden_trans_male, 0) as gender_iden_trans_male,
coalesce(gender_iden_trans_female, 0) as gender_iden_trans_female,
coalesce(gender_iden_nonbinary, 0) as gender_iden_nonbinary,
coalesce(gender_iden_other, 0) as gender_iden_other,
coalesce(gender_iden_unk, 1) as gender_iden_unk,
coalesce(race_white, 0) as race_white,
coalesce(race_black, 0) as race_black,
coalesce(race_other, 0) as race_other,
coalesce(race_unk, 1) as race_unk,
coalesce(ethnicity_hispanic, 0) as ethnicity_hispanic,
coalesce(ethnicity_nonhispanic, 0) as ethnicity_nonhispanic,
coalesce(ethnicity_unk, 1) as ethnicity_unk,
coalesce(sex_partner_male, 0) as sex_partner_male,
coalesce(sex_partner_female, 0) as sex_partner_female,
coalesce(sex_partner_mandf, 0) as sex_partner_mandf,
coalesce(sex_partner_trans, 0) as sex_partner_trans,
coalesce(sex_partner_unk, 1) as sex_partner_unk,
coalesce(marital_stat_married, 0) as marital_stat_married,
coalesce(marital_stat_partner, 0) as marital_stat_partner,
coalesce(marital_stat_unk, 1) as marital_stat_unk,
coalesce(marital_stat_widow, 0) as marital_stat_widow,
coalesce(marital_stat_divorced, 0) as marital_stat_divorced,
coalesce(marital_stat_other, 0) as marital_stat_other,
coalesce(num_encs_1yr, 0) as num_encs_1yr,
coalesce(num_encs_1_to_2yr, 0) as num_encs_1_to_2yr,
coalesce(num_encs_gt_2yr, 0) as num_encs_gt_2yr,
coalesce(gon_tests_1yr, 0) as gon_tests_1yr,
coalesce(gon_tests_1_to_2yr, 0) as gon_tests_1_to_2yr,
coalesce(gon_tests_gt_2yr, 0) as gon_tests_gt_2yr,
coalesce(gon_pos_tests_1yr, 0) as gon_pos_tests_1yr,
coalesce(gon_pos_tests_1_to_2yr, 0) as gon_pos_tests_1_to_2yr,
coalesce(gon_pos_tests_gt_2yr, 0) as gon_pos_tests_gt_2yr,
coalesce(gon_tests_rectal_1yr, 0) as gon_tests_rectal_1yr,
coalesce(gon_tests_rectal_1_to_2yr, 0) as gon_tests_rectal_1_to_2yr,
coalesce(gon_tests_rectal_gt_2yr, 0) as gon_tests_rectal_gt_2yr,
coalesce(gon_tests_rectal_pos_1yr, 0) as gon_tests_rectal_pos_1yr,
coalesce(gon_tests_rectal_pos_1_to_2yr, 0) as gon_tests_rectal_pos_1_to_2yr,
coalesce(gon_tests_rectal_pos_gt_2yr, 0) as gon_tests_rectal_pos_gt_2yr,
coalesce(gon_tests_oral_1yr, 0) as gon_tests_oral_1yr,
coalesce(gon_tests_oral_1_to_2yr, 0) as gon_tests_oral_1_to_2yr,
coalesce(gon_tests_oral_gt_2yr, 0) as gon_tests_oral_gt_2yr,
coalesce(gon_tests_oral_pos_1yr, 0) as gon_tests_oral_pos_1yr,
coalesce(gon_tests_oral_pos_1_to_2yr, 0) as gon_tests_oral_pos_1_to_2yr,
coalesce(gon_tests_oral_pos_gt_2yr, 0) as gon_tests_oral_pos_gt_2yr,
coalesce(chlam_tests_1yr, 0) as chlam_tests_1yr,
coalesce(chlam_pos_tests_1yr, 0) as chlam_pos_tests_1yr,
coalesce(chlam_pos_tests_1_to_2yr, 0) as chlam_pos_tests_1_to_2yr,
coalesce(chlam_pos_tests_gt_2yr, 0) as chlam_pos_tests_gt_2yr,
coalesce(chlam_tests_rectal_1yr, 0) as chlam_tests_rectal_1yr,
coalesce(chlam_tests_rectal_1_to_2yr, 0) as chlam_tests_rectal_1_to_2yr,
coalesce(chlam_tests_rectal_gt_2yr, 0) as chlam_tests_rectal_gt_2yr,
coalesce(chlam_tests_rectal_pos_1yr, 0) as chlam_tests_rectal_pos_1yr,
coalesce(chlam_tests_rectal_pos_1_to_2yr, 0) as chlam_tests_rectal_pos_1_to_2yr,
coalesce(chlam_tests_rectal_pos_gt_2yr, 0) as chlam_tests_rectal_pos_gt_2yr,
coalesce(chlam_tests_oral_1yr, 0) as chlam_tests_oral_1yr,
coalesce(chlam_tests_oral_1_to_2yr, 0) as chlam_tests_oral_1_to_2yr,
coalesce(chlam_tests_oral_gt_2yr, 0) as chlam_tests_oral_gt_2yr,
coalesce(chlam_tests_oral_pos_1yr, 0) as chlam_tests_oral_pos_1yr,
coalesce(chlam_tests_oral_pos_1_to_2yr, 0) as chlam_tests_oral_pos_1_to_2yr,
coalesce(chlam_tests_oral_pos_gt_2yr, 0) as chlam_tests_oral_pos_gt_2yr,
coalesce(syph_tests_1yr, 0) as syph_tests_1yr,
coalesce(syph_tests_1_to_2yr, 0) as syph_tests_1_to_2yr,
coalesce(syph_tests_gt_2yr, 0) as syph_tests_gt_2yr,
coalesce(syph_per_esp_1yr, 0) as syph_per_esp_1yr,
coalesce(syph_per_esp_1_to_2yr, 0) as syph_per_esp_1_to_2yr,
coalesce(syph_per_esp_gt_2yr, 0) as syph_per_esp_gt_2yr,
coalesce(hcv_ab_1yr, 0) as hcv_ab_1yr,
coalesce(hcv_ab_1_to_2yr, 0) as hcv_ab_1_to_2yr,
coalesce(hcv_ab_gt_2yr, 0) as hcv_ab_gt_2yr,
coalesce(hcv_rna_1yr, 0) as hcv_rna_1yr,
coalesce(hcv_rna_1_to_2yr, 0) as hcv_rna_1_to_2yr,
coalesce(hcv_rna_gt_2yr, 0) as hcv_rna_gt_2yr,
coalesce(hcv_ab_or_rna_pos_ev, 0) as hcv_ab_or_rna_pos_ev,
coalesce(hcv_ab_pos_first_yr, 0) as hcv_ab_pos_first_yr,
coalesce(hcv_rna_pos_first_yr, 0) as hcv_rna_pos_first_yr,
coalesce(hcv_acute_per_esp, 0) as hcv_acute_per_esp,
coalesce(hcv_acute_per_esp_case_yrs, 0) as hcv_acute_per_esp_case_yrs,
coalesce(hbv_sag_1yr, 0) as hbv_sag_1yr,
coalesce(hbv_sag_1_to_2yr, 0) as hbv_sag_1_to_2yr,
coalesce(hbv_sag_gt_2yr, 0) as hbv_sag_gt_2yr,
coalesce(hbv_dna_1yr, 0) as hbv_dna_1yr,
coalesce(hbv_dna_1_to_2yr, 0) as hbv_dna_1_to_2yr,
coalesce(hbv_dna_gt_2yr, 0) as hbv_dna_gt_2yr,
coalesce(hbv_sag_or_dna_pos_first_yr, 0) as hbv_sag_or_dna_pos_first_yr,
coalesce(hbv_acute_per_esp, 0) as hbv_acute_per_esp,
coalesce(hbv_acute_per_esp_case_yrs, 0) as hbv_acute_per_esp_case_yrs,
COALESCE(hbv_sag_or_dna_pos_ev, hep_b_dx, 0) as hbv_history_ev,
coalesce(hiv_elisa_ag_ab_1yr, 0) as hiv_elisa_ag_ab_1yr,
coalesce(hiv_elisa_ag_ab_1_to_2yr, 0) as hiv_elisa_ag_ab_1_to_2yr,
coalesce(hiv_elisa_ag_ab_gt_2yr, 0) as hiv_elisa_ag_ab_gt_2yr,
coalesce(hiv_confirm_tests_1yr, 0) as hiv_confirm_tests_1yr,
coalesce(hiv_confirm_tests_1_to_2yr, 0) as hiv_confirm_tests_1_to_2yr,
coalesce(hiv_confirm_tests_gt_2yr, 0) as hiv_confirm_tests_gt_2yr,
coalesce(hiv_rna_viral_1yr, 0) as hiv_rna_viral_1yr,
coalesce(hiv_rna_viral_1_to_2yr, 0) as hiv_rna_viral_1_to_2yr,
coalesce(hiv_rna_viral_gt_2yr, 0) as hiv_rna_viral_gt_2yr,
coalesce(hiv_max_viral_load_yr, 0) as hiv_max_viral_load_yr,
coalesce(hiv_neg_w_hiv_med_ev, 0) as hiv_neg_w_hiv_med_ev,
coalesce(hiv_dx_1_yr, 0) as hiv_dx_1_yr,
coalesce(hiv_dx_1_to_2yr, 0) as hiv_dx_1_to_2yr,
coalesce(hiv_dx_gt_2yr, 0) as hiv_dx_gt_2yr,
coalesce(hiv_trmt_regimen_1yr, 0) as hiv_trmt_regimen_1yr,
coalesce(hiv_trmt_regimen_1_to2yr, 0) as hiv_trmt_regimen_1_to2yr,
coalesce(hiv_trmt_regimen_gt_2yr, 0) as hiv_trmt_regimen_gt_2yr,
coalesce(pep_rx_dates_num, 0) as pep_rx_dates_num,
coalesce(prep_rx_num_1yr, 0) as prep_rx_num_1yr,
coalesce(prep_rx_num_1_to_2yr, 0) as prep_rx_num_1_to_2yr,
coalesce(prep_rx_num_gt_2yr, 0) as prep_rx_num_gt_2yr,
coalesce(dx_abn_anal_cyt_yrs, 0) as dx_abn_anal_cyt_yrs,
coalesce(dx_syph_yrs, 0) as dx_syph_yrs,
coalesce(dx_anal_syph_yrs, 0) as dx_anal_syph_yrs,
coalesce(dx_gon_inf_rectum_yrs, 0) as dx_gon_inf_rectum_yrs,
coalesce(dx_gon_pharyn_yrs, 0) as dx_gon_pharyn_yrs,
coalesce(dx_chlam_inf_rectum_yrs, 0) as dx_chlam_inf_rectum_yrs,
coalesce(dx_chlam_pharyn_yrs, 0) as dx_chlam_pharyn_yrs,
coalesce(dx_lymph_ven_yrs, 0) as dx_lymph_ven_yrs,
coalesce(dx_chancroid_yrs, 0) as dx_chancroid_yrs,
coalesce(dx_nongon_ureth_yrs, 0) as dx_nongon_ureth_yrs,
coalesce(dx_hsv_w_compl_yrs, 0) as dx_hsv_w_compl_yrs,
coalesce(dx_genital_herpes_yrs, 0) as dx_genital_herpes_yrs,
coalesce(dx_anogenital_warts_yrs, 0) as dx_anogenital_warts_yrs,
coalesce(dx_anorectal_ulcer_yrs, 0) as dx_anorectal_ulcer_yrs,
coalesce(dx_trich_yrs, 0) as dx_trich_yrs,
coalesce(dx_mpox_yrs, 0) as dx_mpox_yrs,
coalesce(dx_unspec_std_yrs, 0) as dx_unspec_std_yrs,
coalesce(dx_pid_yrs, 0) as dx_pid_yrs,
coalesce(dx_cont_w_exp_to_vd_yrs, 0) as dx_cont_w_exp_to_vd_yrs,
coalesce(dx_high_risk_sex_behav_yrs, 0) as dx_high_risk_sex_behav_yrs,
coalesce(dx_hiv_counsel_yrs, 0) as dx_hiv_counsel_yrs,
coalesce(dx_anorexia_yrs, 0) as dx_anorexia_yrs,
coalesce(dx_bulimia_yrs, 0) as dx_bulimia_yrs,
coalesce(dx_eat_disorder_nos_yrs, 0) as dx_eat_disorder_nos_yrs,
coalesce(dx_gend_iden_dis_yrs, 0) as dx_gend_iden_dis_yrs,
coalesce(dx_transsexualism_yrs, 0) as dx_transsexualism_yrs,
coalesce(dx_couns_or_prb_child_sex_abuse_yrs, 0) as dx_couns_or_prb_child_sex_abuse_yrs,
coalesce(dx_adult_child_sex_exp_or_abuse_yrs, 0) as dx_adult_child_sex_exp_or_abuse_yrs,
coalesce(dx_foreign_body_anus_yrs, 0) as dx_foreign_body_anus_yrs,
coalesce(dx_alcohol_depend_abuse_yrs, 0) as dx_alcohol_depend_abuse_yrs,
coalesce(dx_opioid_depend_abuse_yrs, 0) as dx_opioid_depend_abuse_yrs,
coalesce(dx_sed_hypn_anxio_depend_abuse_yrs, 0) as dx_sed_hypn_anxio_depend_abuse_yrs,
coalesce(dx_cocaine_depend_abuse_yrs, 0) as dx_cocaine_depend_abuse_yrs,
coalesce(dx_amphet_stim_depend_abuse_yrs, 0) as dx_amphet_stim_depend_abuse_yrs,
coalesce(dx_oth_psycho_or_nos_subs_depend_abuse_yrs, 0) as dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
coalesce(dx_sti_screen_yrs, 0) as dx_sti_screen_yrs,
coalesce(dx_malig_neo_rec_screen_yrs, 0) as dx_malig_neo_rec_screen_yrs,
coalesce(dx_needle_stick, 0) as dx_needle_stick,
coalesce(rx_bicillin_ever, 0) as rx_bicillin_ever,
coalesce(rx_azith_ever, 0) as rx_azith_ever,
coalesce(rx_ceft_ever, 0) as rx_ceft_ever,
coalesce(rx_doxy_ever, 0) as rx_doxy_ever,
coalesce(rx_methadone_ever, 0) as rx_methadone_ever,
coalesce(rx_buprenorphine_ever, 0) as rx_buprenorphine_ever,
coalesce(rx_naltrexone_ever, 0) as rx_naltrexone_ever,
coalesce(rx_viagara_cilais_or_levitra_ever, 0) as rx_viagara_cilais_or_levitra_ever,
coalesce(vax_mpox_ever, 0) as vax_mpox_ever,
coalesce(vax_hpv_male_ever, 0) as vax_hpv_male_ever,
coalesce(hiv_test_this_yr, 0) as hiv_test_this_yr,
coalesce(prep_rx_this_yr, 0) as prep_rx_this_yr
FROM hiv_rpt.hivstirs_index_patients T1
LEFT JOIN hiv_rpt.hivstirs_sex_p_gender T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_rpt.hivstirs_clin_enc_visits_num T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_chlam_gon_labs_counts T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_syph_counts T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_syph_case_counts T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_hep_events_nohef_counts T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_hep_events_hef_counts T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_hep_case_counts T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_hepb_diags T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN hiv_rpt.hivstirs_all_hiv_events_nohef_counts T11 ON (T1.patient_id = T11.patient_id)
LEFT JOIN hiv_rpt.hivstirs_viral_loads T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN hiv_rpt.hivstirs_hiv_case_values T13 ON (T1.patient_id = T13.patient_id)
LEFT JOIN hiv_rpt.hivstirs_hiv_dx_code_counts T14 ON (T1.patient_id = T14.patient_id)
LEFT JOIN hiv_rpt.hivstirs_hiv_rx_3_diff_count T15 ON (T1.patient_id = T15.patient_id)
LEFT JOIN hiv_rpt.hivstirs_hiv_pep_counts T16 ON (T1.patient_id = T16.patient_id)
LEFT JOIN hiv_rpt.hivstirs_prep_final T17 ON (T1.patient_id = T17.patient_id)
LEFT JOIN hiv_rpt.hivstirs_dx_code_years T18 ON (T1.patient_id = T18.patient_id)
LEFT JOIN hiv_rpt.hivstirs_bicillin_meds_updated T19 ON (T1.patient_id = T19.patient_id)
LEFT JOIN hiv_rpt.hivstirs_azith_meds T20 ON (T1.patient_id = T20.patient_id)
LEFT JOIN hiv_rpt.hivstirs_ceft_meds T21 ON (T1.patient_id = T21.patient_id)
LEFT JOIN hiv_rpt.hivstirs_doxy_meds T22 ON (T1.patient_id = T22.patient_id)
LEFT JOIN hiv_rpt.hivstirs_other_meds T23 ON (T1.patient_id = T23.patient_id)
LEFT JOIN hiv_rpt.hivstirs_mpox_vax T24 ON (T1.patient_id = T24.patient_id)
LEFT JOIN hiv_rpt.hivstirs_hpv_vax T25 ON (T1.patient_id = T25.patient_id);

-- Compute the sum to be used in the risk score calculation
DROP TABLE IF EXISTS hiv_rpt.hivstirs_pat_sum_x_value;
CREATE TABLE hiv_rpt.hivstirs_pat_sum_x_value AS
SELECT patient_id, 
(
( 1 * c.intercept) +
(i.age_on_anchor_date * c.age_on_anchor_date) +
(i.gender_mapped * c.gender_mapped) +
(i.gender_male * c.gender_male) +
(i.gender_female * c.gender_female) +
(i.gender_trans * c.gender_trans) +
(i.birth_sex_male * c.birth_sex_male) +
(i.birth_sex_unk * c.birth_sex_unk) +
(i.sexor_straight * c.sexor_straight) +
(i.sexor_gay_lesbian * c.sexor_gay_lesbian) +
(i.sexor_other * c.sexor_other) +
(i.gender_iden_male * c.gender_iden_male) +
(i.gender_iden_trans_male * c.gender_iden_trans_male) +
(i.gender_iden_trans_female * c.gender_iden_trans_female) +
(i.gender_iden_nonbinary * c.gender_iden_nonbinary) +
(i.gender_iden_other * c.gender_iden_other) +
(i.gender_iden_unk * c.gender_iden_unk) +
(i.race_white * c.race_white) +
(i.race_black * c.race_black) +
(i.race_other * c.race_other) +
(i.race_unk * c.race_unk) +
(i.ethnicity_hispanic * c.ethnicity_hispanic) +
(i.ethnicity_nonhispanic * c.ethnicity_nonhispanic) +
(i.ethnicity_unk * c.ethnicity_unk) +
(i.sex_partner_male * c.sex_partner_male) +
(i.sex_partner_female * c.sex_partner_female) +
(i.sex_partner_mandf * c.sex_partner_mandf) +
(i.sex_partner_trans * c.sex_partner_trans) +
(i.sex_partner_unk * c.sex_partner_unk) +
(i.marital_stat_married * c.marital_stat_married) +
(i.marital_stat_partner * c.marital_stat_partner) +
(i.marital_stat_unk * c.marital_stat_unk) +
(i.marital_stat_widow * c.marital_stat_widow) +
(i.marital_stat_divorced * c.marital_stat_divorced) +
(i.marital_stat_other * c.marital_stat_other) +
(i.num_encs_1yr * c.num_encs_1yr) +
(i.num_encs_1_to_2yr * c.num_encs_1_to_2yr) +
(i.num_encs_gt_2yr * c.num_encs_gt_2yr) +
(i.gon_tests_1yr * c.gon_tests_1yr) +
(i.gon_tests_1_to_2yr * c.gon_tests_1_to_2yr) +
(i.gon_tests_gt_2yr * c.gon_tests_gt_2yr) +
(i.gon_pos_tests_1yr * c.gon_pos_tests_1yr) +
(i.gon_pos_tests_1_to_2yr * c.gon_pos_tests_1_to_2yr) +
(i.gon_pos_tests_gt_2yr * c.gon_pos_tests_gt_2yr) +
(i.gon_tests_rectal_1yr * c.gon_tests_rectal_1yr) +
(i.gon_tests_rectal_1_to_2yr * c.gon_tests_rectal_1_to_2yr) +
(i.gon_tests_rectal_gt_2yr * c.gon_tests_rectal_gt_2yr) +
(i.gon_tests_rectal_pos_1yr * c.gon_tests_rectal_pos_1yr) +
(i.gon_tests_rectal_pos_1_to_2yr * c.gon_tests_rectal_pos_1_to_2yr) +
(i.gon_tests_rectal_pos_gt_2yr * c.gon_tests_rectal_pos_gt_2yr) +
(i.gon_tests_oral_1yr * c.gon_tests_oral_1yr) +
(i.gon_tests_oral_1_to_2yr * c.gon_tests_oral_1_to_2yr) +
(i.gon_tests_oral_gt_2yr * c.gon_tests_oral_gt_2yr) +
(i.gon_tests_oral_pos_1yr * c.gon_tests_oral_pos_1yr) +
(i.gon_tests_oral_pos_1_to_2yr * c.gon_tests_oral_pos_1_to_2yr) +
(i.gon_tests_oral_pos_gt_2yr * c.gon_tests_oral_pos_gt_2yr) +
(i.chlam_tests_1yr * c.chlam_tests_1yr) +
(i.chlam_pos_tests_1yr * c.chlam_pos_tests_1yr) +
(i.chlam_pos_tests_1_to_2yr * c.chlam_pos_tests_1_to_2yr) +
(i.chlam_pos_tests_gt_2yr * c.chlam_pos_tests_gt_2yr) +
(i.chlam_tests_rectal_1yr * c.chlam_tests_rectal_1yr) +
(i.chlam_tests_rectal_1_to_2yr * c.chlam_tests_rectal_1_to_2yr) +
(i.chlam_tests_rectal_gt_2yr * c.chlam_tests_rectal_gt_2yr) +
(i.chlam_tests_rectal_pos_1yr * c.chlam_tests_rectal_pos_1yr) +
(i.chlam_tests_rectal_pos_1_to_2yr * c.chlam_tests_rectal_pos_1_to_2yr) +
(i.chlam_tests_rectal_pos_gt_2yr * c.chlam_tests_rectal_pos_gt_2yr) +
(i.chlam_tests_oral_1yr * c.chlam_tests_oral_1yr) +
(i.chlam_tests_oral_1_to_2yr * c.chlam_tests_oral_1_to_2yr) +
(i.chlam_tests_oral_gt_2yr * c.chlam_tests_oral_gt_2yr) +
(i.chlam_tests_oral_pos_1yr * c.chlam_tests_oral_pos_1yr) +
(i.chlam_tests_oral_pos_1_to_2yr * c.chlam_tests_oral_pos_1_to_2yr) +
(i.chlam_tests_oral_pos_gt_2yr * c.chlam_tests_oral_pos_gt_2yr) +
(i.syph_tests_1yr * c.syph_tests_1yr) +
(i.syph_tests_1_to_2yr * c.syph_tests_1_to_2yr) +
(i.syph_tests_gt_2yr * c.syph_tests_gt_2yr) +
(i.syph_per_esp_1yr * c.syph_per_esp_1yr) +
(i.syph_per_esp_1_to_2yr * c.syph_per_esp_1_to_2yr) +
(i.syph_per_esp_gt_2yr * c.syph_per_esp_gt_2yr) +
(i.hcv_ab_1yr * c.hcv_ab_1yr) +
(i.hcv_ab_1_to_2yr * c.hcv_ab_1_to_2yr) +
(i.hcv_ab_gt_2yr * c.hcv_ab_gt_2yr) +
(i.hcv_rna_1yr * c.hcv_rna_1yr) +
(i.hcv_rna_1_to_2yr * c.hcv_rna_1_to_2yr) +
(i.hcv_rna_gt_2yr * c.hcv_rna_gt_2yr) +
(i.hcv_ab_or_rna_pos_ev * c.hcv_ab_or_rna_pos_ev) +
(i.hcv_ab_pos_first_yr * c.hcv_ab_pos_first_yr) +
(i.hcv_rna_pos_first_yr * c.hcv_rna_pos_first_yr) +
(i.hcv_acute_per_esp * c.hcv_acute_per_esp) +
(i.hcv_acute_per_esp_case_yrs * c.hcv_acute_per_esp_case_yrs) +
(i.hbv_sag_1yr * c.hbv_sag_1yr) +
(i.hbv_sag_1_to_2yr * c.hbv_sag_1_to_2yr) +
(i.hbv_sag_gt_2yr * c.hbv_sag_gt_2yr) +
(i.hbv_dna_1yr * c.hbv_dna_1yr) +
(i.hbv_dna_1_to_2yr * c.hbv_dna_1_to_2yr) +
(i.hbv_dna_gt_2yr * c.hbv_dna_gt_2yr) +
(i.hbv_sag_or_dna_pos_first_yr * c.hbv_sag_or_dna_pos_first_yr) +
(i.hbv_acute_per_esp * c.hbv_acute_per_esp) +
(i.hbv_acute_per_esp_case_yrs * c.hbv_acute_per_esp_case_yrs) +
(i.hbv_history_ev * c.hbv_history_ev) +
(i.hiv_elisa_ag_ab_1yr * c.hiv_elisa_ag_ab_1yr) +
(i.hiv_elisa_ag_ab_1_to_2yr * c.hiv_elisa_ag_ab_1_to_2yr) +
(i.hiv_elisa_ag_ab_gt_2yr * c.hiv_elisa_ag_ab_gt_2yr) +
(i.hiv_confirm_tests_1yr * c.hiv_confirm_tests_1yr) +
(i.hiv_confirm_tests_1_to_2yr * c.hiv_confirm_tests_1_to_2yr) +
(i.hiv_confirm_tests_gt_2yr * c.hiv_confirm_tests_gt_2yr) +
(i.hiv_rna_viral_1yr * c.hiv_rna_viral_1yr) +
(i.hiv_rna_viral_1_to_2yr * c.hiv_rna_viral_1_to_2yr) +
(i.hiv_rna_viral_gt_2yr * c.hiv_rna_viral_gt_2yr) +
(i.hiv_max_viral_load_yr * c.hiv_max_viral_load_yr) +
(i.hiv_neg_w_hiv_med_ev * c.hiv_neg_w_hiv_med_ev) +
(i.hiv_dx_1_yr * c.hiv_dx_1_yr) +
(i.hiv_dx_1_to_2yr * c.hiv_dx_1_to_2yr) +
(i.hiv_dx_gt_2yr * c.hiv_dx_gt_2yr) +
(i.hiv_trmt_regimen_1yr * c.hiv_trmt_regimen_1yr) +
(i.hiv_trmt_regimen_1_to2yr * c.hiv_trmt_regimen_1_to2yr) +
(i.hiv_trmt_regimen_gt_2yr * c.hiv_trmt_regimen_gt_2yr) +
(i.pep_rx_dates_num * c.pep_rx_dates_num) +
(i.prep_rx_num_1yr * c.prep_rx_num_1yr) +
(i.prep_rx_num_1_to_2yr * c.prep_rx_num_1_to_2yr) +
(i.prep_rx_num_gt_2yr * c.prep_rx_num_gt_2yr) +
(i.dx_abn_anal_cyt_yrs * c.dx_abn_anal_cyt_yrs) +
(i.dx_syph_yrs * c.dx_syph_yrs) +
(i.dx_anal_syph_yrs * c.dx_anal_syph_yrs) +
(i.dx_gon_inf_rectum_yrs * c.dx_gon_inf_rectum_yrs) +
(i.dx_gon_pharyn_yrs * c.dx_gon_pharyn_yrs) +
(i.dx_chlam_inf_rectum_yrs * c.dx_chlam_inf_rectum_yrs) +
(i.dx_chlam_pharyn_yrs * c.dx_chlam_pharyn_yrs) +
(i.dx_lymph_ven_yrs * c.dx_lymph_ven_yrs) +
(i.dx_chancroid_yrs * c.dx_chancroid_yrs) +
(i.dx_nongon_ureth_yrs * c.dx_nongon_ureth_yrs) +
(i.dx_hsv_w_compl_yrs * c.dx_hsv_w_compl_yrs) +
(i.dx_genital_herpes_yrs * c.dx_genital_herpes_yrs) +
(i.dx_anogenital_warts_yrs * c.dx_anogenital_warts_yrs) +
(i.dx_anorectal_ulcer_yrs * c.dx_anorectal_ulcer_yrs) +
(i.dx_trich_yrs * c.dx_trich_yrs) +
(i.dx_mpox_yrs * c.dx_mpox_yrs) +
(i.dx_unspec_std_yrs * c.dx_unspec_std_yrs) +
(i.dx_pid_yrs * c.dx_pid_yrs) +
(i.dx_cont_w_exp_to_vd_yrs * c.dx_cont_w_exp_to_vd_yrs) +
(i.dx_high_risk_sex_behav_yrs * c.dx_high_risk_sex_behav_yrs) +
(i.dx_hiv_counsel_yrs * c.dx_hiv_counsel_yrs) +
(i.dx_anorexia_yrs * c.dx_anorexia_yrs) +
(i.dx_bulimia_yrs * c.dx_bulimia_yrs) +
(i.dx_eat_disorder_nos_yrs * c.dx_eat_disorder_nos_yrs) +
(i.dx_gend_iden_dis_yrs * c.dx_gend_iden_dis_yrs) +
(i.dx_transsexualism_yrs * c.dx_transsexualism_yrs) +
(i.dx_couns_or_prb_child_sex_abuse_yrs * c.dx_couns_or_prb_child_sex_abuse_yrs) +
(i.dx_adult_child_sex_exp_or_abuse_yrs * c.dx_adult_child_sex_exp_or_abuse_yrs) +
(i.dx_foreign_body_anus_yrs * c.dx_foreign_body_anus_yrs) +
(i.dx_alcohol_depend_abuse_yrs * c.dx_alcohol_depend_abuse_yrs) +
(i.dx_opioid_depend_abuse_yrs * c.dx_opioid_depend_abuse_yrs) +
(i.dx_sed_hypn_anxio_depend_abuse_yrs * c.dx_sed_hypn_anxio_depend_abuse_yrs) +
(i.dx_cocaine_depend_abuse_yrs * c.dx_cocaine_depend_abuse_yrs) +
(i.dx_amphet_stim_depend_abuse_yrs * c.dx_amphet_stim_depend_abuse_yrs) +
(i.dx_oth_psycho_or_nos_subs_depend_abuse_yrs * c.dx_oth_psycho_or_nos_subs_depend_abuse_yrs) +
(i.dx_sti_screen_yrs * c.dx_sti_screen_yrs) +
(i.dx_malig_neo_rec_screen_yrs * c.dx_malig_neo_rec_screen_yrs) +
(i.dx_needle_stick * c.dx_needle_stick) +
(i.rx_bicillin_ever * c.rx_bicillin_ever) +
(i.rx_azith_ever * c.rx_azith_ever) +
(i.rx_ceft_ever * c.rx_ceft_ever) +
(i.rx_doxy_ever * c.rx_doxy_ever) +
(i.rx_methadone_ever * c.rx_methadone_ever) +
(i.rx_buprenorphine_ever * c.rx_buprenorphine_ever) +
(i.rx_naltrexone_ever * c.rx_naltrexone_ever) +
(i.rx_viagara_cilais_or_levitra_ever * c.rx_viagara_cilais_or_levitra_ever) +
(i.vax_mpox_ever * c.vax_mpox_ever) +
(i.vax_hpv_male_ever * c.vax_hpv_male_ever)
) as pat_sum_x_value
FROM hiv_rpt.hivstirs_index_data i,
hiv_rpt.hivstirs_coefficients c;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_risk_scores;
CREATE TABLE hiv_rpt.hivstirs_risk_scores AS
SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value::numeric)))) hiv_sti_risk_score
FROM hiv_rpt.hivstirs_pat_sum_x_value;


/* CREATE TABLE gen_pop_tools.hiv_sti_risk_scores
( patient_id integer NOT NULL,
rpt_year character varying(4) NOT NULL,
hiv_sti_risk_score numeric,
hiv_test_yn integer,
truvada_rx_yn integer,
CONSTRAINT hiv_sti_risk_score_pkey PRIMARY KEY (patient_id, rpt_year)
); */


-- POPULATE THE TABLE FOR MAGIC/COC REPORTING
INSERT INTO gen_pop_tools.hiv_sti_risk_scores
	(SELECT T1.patient_id, T1.rpt_year, round(hiv_sti_risk_score::numeric, 6) hiv_sti_risk_score, hiv_test_this_yr as hiv_test_yn, prep_rx_this_yr as truvada_rx_yn
	FROM hiv_rpt.hivstirs_index_data T1,
	hiv_rpt.hivstirs_risk_scores T2
	WHERE T1.patient_id = T2.patient_id );
	
	--nohup psql -d esp -f hiv-sti-risk-score-compute-2024.sql -v end_calc_year="'2025-01-06'" &

DROP TABLE IF EXISTS hiv_rpt.hivstirs_risk_factor_totals;	
CREATE TABLE hiv_rpt.hivstirs_risk_factor_totals AS
SELECT i.patient_id, 
(i.age_on_anchor_date * c.age_on_anchor_date) as age_on_anchor_date_sum,
(i.gender_mapped * c.gender_mapped) as gender_mapped_sum,
(i.gender_male * c.gender_male) as gender_male_sum,
(i.gender_female * c.gender_female) as gender_female_sum,
(i.gender_trans * c.gender_trans) as gender_trans_sum,
(i.birth_sex_male * c.birth_sex_male) as birth_sex_male_sum,
(i.birth_sex_unk * c.birth_sex_unk) as birth_sex_unk_sum,
(i.sexor_straight * c.sexor_straight) as sexor_straight_sum,
(i.sexor_gay_lesbian * c.sexor_gay_lesbian) as sexor_gay_lesbian_sum,
(i.sexor_other * c.sexor_other) as sexor_other_sum,
(i.gender_iden_male * c.gender_iden_male) as gender_iden_male_sum,
(i.gender_iden_trans_male * c.gender_iden_trans_male) as gender_iden_trans_male_sum,
(i.gender_iden_trans_female * c.gender_iden_trans_female) as gender_iden_trans_female_sum,
(i.gender_iden_nonbinary * c.gender_iden_nonbinary) as gender_iden_nonbinary_sum,
(i.gender_iden_other * c.gender_iden_other) as gender_iden_other_sum,
(i.gender_iden_unk * c.gender_iden_unk) as gender_iden_unk_sum,
(i.race_white * c.race_white) as race_white_sum,
(i.race_black * c.race_black) as race_black_sum,
(i.race_other * c.race_other) as race_other_sum,
(i.race_unk * c.race_unk) as race_unk_sum,
(i.ethnicity_hispanic * c.ethnicity_hispanic) as ethnicity_hispanic_sum,
(i.ethnicity_nonhispanic * c.ethnicity_nonhispanic) as ethnicity_nonhispanic_sum,
(i.ethnicity_unk * c.ethnicity_unk) as ethnicity_unk_sum,
(i.sex_partner_male * c.sex_partner_male) as sex_partner_male_sum,
(i.sex_partner_female * c.sex_partner_female) as sex_partner_female_sum,
(i.sex_partner_mandf * c.sex_partner_mandf) as sex_partner_mandf_sum,
(i.sex_partner_trans * c.sex_partner_trans) as sex_partner_trans_sum,
(i.sex_partner_unk * c.sex_partner_unk) as sex_partner_unk_sum,
(i.marital_stat_married * c.marital_stat_married) as marital_stat_married_sum,
(i.marital_stat_partner * c.marital_stat_partner) as marital_stat_partner_sum,
(i.marital_stat_unk * c.marital_stat_unk) as marital_stat_unk_sum,
(i.marital_stat_widow * c.marital_stat_widow) as marital_stat_widow_sum,
(i.marital_stat_divorced * c.marital_stat_divorced) as marital_stat_divorced_sum,
(i.marital_stat_other * c.marital_stat_other) as marital_stat_other_sum,
(i.num_encs_1yr * c.num_encs_1yr) as num_encs_1yr_sum,
(i.num_encs_1_to_2yr * c.num_encs_1_to_2yr) as num_encs_1_to_2yr_sum,
(i.num_encs_gt_2yr * c.num_encs_gt_2yr) as num_encs_gt_2yr_sum,
(i.gon_tests_1yr * c.gon_tests_1yr) as gon_tests_1yr_sum,
(i.gon_tests_1_to_2yr * c.gon_tests_1_to_2yr) as gon_tests_1_to_2yr_sum,
(i.gon_tests_gt_2yr * c.gon_tests_gt_2yr) as gon_tests_gt_2yr_sum,
(i.gon_pos_tests_1yr * c.gon_pos_tests_1yr) as gon_pos_tests_1yr_sum,
(i.gon_pos_tests_1_to_2yr * c.gon_pos_tests_1_to_2yr) as gon_pos_tests_1_to_2yr_sum,
(i.gon_pos_tests_gt_2yr * c.gon_pos_tests_gt_2yr) as gon_pos_tests_gt_2yr_sum,
(i.gon_tests_rectal_1yr * c.gon_tests_rectal_1yr) as gon_tests_rectal_1yr_sum,
(i.gon_tests_rectal_1_to_2yr * c.gon_tests_rectal_1_to_2yr) as gon_tests_rectal_1_to_2yr_sum,
(i.gon_tests_rectal_gt_2yr * c.gon_tests_rectal_gt_2yr) as gon_tests_rectal_gt_2yr_sum,
(i.gon_tests_rectal_pos_1yr * c.gon_tests_rectal_pos_1yr) as gon_tests_rectal_pos_1yr_sum,
(i.gon_tests_rectal_pos_1_to_2yr * c.gon_tests_rectal_pos_1_to_2yr) as gon_tests_rectal_pos_1_to_2yr_sum,
(i.gon_tests_rectal_pos_gt_2yr * c.gon_tests_rectal_pos_gt_2yr) as gon_tests_rectal_pos_gt_2yr_sum,
(i.gon_tests_oral_1yr * c.gon_tests_oral_1yr) as gon_tests_oral_1yr_sum,
(i.gon_tests_oral_1_to_2yr * c.gon_tests_oral_1_to_2yr) as gon_tests_oral_1_to_2yr_sum,
(i.gon_tests_oral_gt_2yr * c.gon_tests_oral_gt_2yr) as gon_tests_oral_gt_2yr_sum,
(i.gon_tests_oral_pos_1yr * c.gon_tests_oral_pos_1yr) as gon_tests_oral_pos_1yr_sum,
(i.gon_tests_oral_pos_1_to_2yr * c.gon_tests_oral_pos_1_to_2yr) as gon_tests_oral_pos_1_to_2yr_sum,
(i.gon_tests_oral_pos_gt_2yr * c.gon_tests_oral_pos_gt_2yr) as gon_tests_oral_pos_gt_2yr_sum,
(i.chlam_tests_1yr * c.chlam_tests_1yr) as chlam_tests_1yr_sum,
(i.chlam_pos_tests_1yr * c.chlam_pos_tests_1yr) as chlam_pos_tests_1yr_sum,
(i.chlam_pos_tests_1_to_2yr * c.chlam_pos_tests_1_to_2yr) as chlam_pos_tests_1_to_2yr_sum,
(i.chlam_pos_tests_gt_2yr * c.chlam_pos_tests_gt_2yr) as chlam_pos_tests_gt_2yr_sum,
(i.chlam_tests_rectal_1yr * c.chlam_tests_rectal_1yr) as chlam_tests_rectal_1yr_sum,
(i.chlam_tests_rectal_1_to_2yr * c.chlam_tests_rectal_1_to_2yr) as chlam_tests_rectal_1_to_2yr_sum,
(i.chlam_tests_rectal_gt_2yr * c.chlam_tests_rectal_gt_2yr) as chlam_tests_rectal_gt_2yr_sum,
(i.chlam_tests_rectal_pos_1yr * c.chlam_tests_rectal_pos_1yr) as chlam_tests_rectal_pos_1yr_sum,
(i.chlam_tests_rectal_pos_1_to_2yr * c.chlam_tests_rectal_pos_1_to_2yr) as chlam_tests_rectal_pos_1_to_2yr_sum,
(i.chlam_tests_rectal_pos_gt_2yr * c.chlam_tests_rectal_pos_gt_2yr) as chlam_tests_rectal_pos_gt_2yr_sum,
(i.chlam_tests_oral_1yr * c.chlam_tests_oral_1yr) as chlam_tests_oral_1yr_sum,
(i.chlam_tests_oral_1_to_2yr * c.chlam_tests_oral_1_to_2yr) as chlam_tests_oral_1_to_2yr_sum,
(i.chlam_tests_oral_gt_2yr * c.chlam_tests_oral_gt_2yr) as chlam_tests_oral_gt_2yr_sum,
(i.chlam_tests_oral_pos_1yr * c.chlam_tests_oral_pos_1yr) as chlam_tests_oral_pos_1yr_sum,
(i.chlam_tests_oral_pos_1_to_2yr * c.chlam_tests_oral_pos_1_to_2yr) as chlam_tests_oral_pos_1_to_2yr_sum,
(i.chlam_tests_oral_pos_gt_2yr * c.chlam_tests_oral_pos_gt_2yr) as chlam_tests_oral_pos_gt_2yr_sum,
(i.syph_tests_1yr * c.syph_tests_1yr) as syph_tests_1yr_sum,
(i.syph_tests_1_to_2yr * c.syph_tests_1_to_2yr) as syph_tests_1_to_2yr_sum,
(i.syph_tests_gt_2yr * c.syph_tests_gt_2yr) as syph_tests_gt_2yr_sum,
(i.syph_per_esp_1yr * c.syph_per_esp_1yr) as syph_per_esp_1yr_sum,
(i.syph_per_esp_1_to_2yr * c.syph_per_esp_1_to_2yr) as syph_per_esp_1_to_2yr_sum,
(i.syph_per_esp_gt_2yr * c.syph_per_esp_gt_2yr) as syph_per_esp_gt_2yr_sum,
(i.hcv_ab_1yr * c.hcv_ab_1yr) as hcv_ab_1yr_sum,
(i.hcv_ab_1_to_2yr * c.hcv_ab_1_to_2yr) as hcv_ab_1_to_2yr_sum,
(i.hcv_ab_gt_2yr * c.hcv_ab_gt_2yr) as hcv_ab_gt_2yr_sum,
(i.hcv_rna_1yr * c.hcv_rna_1yr) as hcv_rna_1yr_sum,
(i.hcv_rna_1_to_2yr * c.hcv_rna_1_to_2yr) as hcv_rna_1_to_2yr_sum,
(i.hcv_rna_gt_2yr * c.hcv_rna_gt_2yr) as hcv_rna_gt_2yr_sum,
(i.hcv_ab_or_rna_pos_ev * c.hcv_ab_or_rna_pos_ev) as hcv_ab_or_rna_pos_ev_sum,
(i.hcv_ab_pos_first_yr * c.hcv_ab_pos_first_yr) as hcv_ab_pos_first_yr_sum,
(i.hcv_rna_pos_first_yr * c.hcv_rna_pos_first_yr) as hcv_rna_pos_first_yr_sum,
(i.hcv_acute_per_esp * c.hcv_acute_per_esp) as hcv_acute_per_esp_sum,
(i.hcv_acute_per_esp_case_yrs * c.hcv_acute_per_esp_case_yrs) as hcv_acute_per_esp_case_yrs_sum,
(i.hbv_sag_1yr * c.hbv_sag_1yr) as hbv_sag_1yr_sum,
(i.hbv_sag_1_to_2yr * c.hbv_sag_1_to_2yr) as hbv_sag_1_to_2yr_sum,
(i.hbv_sag_gt_2yr * c.hbv_sag_gt_2yr) as hbv_sag_gt_2yr_sum,
(i.hbv_dna_1yr * c.hbv_dna_1yr) as hbv_dna_1yr_sum,
(i.hbv_dna_1_to_2yr * c.hbv_dna_1_to_2yr) as hbv_dna_1_to_2yr_sum,
(i.hbv_dna_gt_2yr * c.hbv_dna_gt_2yr) as hbv_dna_gt_2yr_sum,
(i.hbv_sag_or_dna_pos_first_yr * c.hbv_sag_or_dna_pos_first_yr) as hbv_sag_or_dna_pos_first_yr_sum,
(i.hbv_acute_per_esp * c.hbv_acute_per_esp) as hbv_acute_per_esp_sum,
(i.hbv_acute_per_esp_case_yrs * c.hbv_acute_per_esp_case_yrs) as hbv_acute_per_esp_case_yrs_sum,
(i.hbv_history_ev * c.hbv_history_ev) as hbv_history_ev_sum,
(i.hiv_elisa_ag_ab_1yr * c.hiv_elisa_ag_ab_1yr) as hiv_elisa_ag_ab_1yr_sum,
(i.hiv_elisa_ag_ab_1_to_2yr * c.hiv_elisa_ag_ab_1_to_2yr) as hiv_elisa_ag_ab_1_to_2yr_sum,
(i.hiv_elisa_ag_ab_gt_2yr * c.hiv_elisa_ag_ab_gt_2yr) as hiv_elisa_ag_ab_gt_2yr_sum,
(i.hiv_confirm_tests_1yr * c.hiv_confirm_tests_1yr) as hiv_confirm_tests_1yr_sum,
(i.hiv_confirm_tests_1_to_2yr * c.hiv_confirm_tests_1_to_2yr) as hiv_confirm_tests_1_to_2yr_sum,
(i.hiv_confirm_tests_gt_2yr * c.hiv_confirm_tests_gt_2yr) as hiv_confirm_tests_gt_2yr_sum,
(i.hiv_rna_viral_1yr * c.hiv_rna_viral_1yr) as hiv_rna_viral_1yr_sum,
(i.hiv_rna_viral_1_to_2yr * c.hiv_rna_viral_1_to_2yr) as hiv_rna_viral_1_to_2yr_sum,
(i.hiv_rna_viral_gt_2yr * c.hiv_rna_viral_gt_2yr) as hiv_rna_viral_gt_2yr_sum,
(i.hiv_max_viral_load_yr * c.hiv_max_viral_load_yr) as hiv_max_viral_load_yr_sum,
(i.hiv_neg_w_hiv_med_ev * c.hiv_neg_w_hiv_med_ev) as hiv_neg_w_hiv_med_ev_sum,
(i.hiv_dx_1_yr * c.hiv_dx_1_yr) as hiv_dx_1_yr_sum,
(i.hiv_dx_1_to_2yr * c.hiv_dx_1_to_2yr) as hiv_dx_1_to_2yr_sum,
(i.hiv_dx_gt_2yr * c.hiv_dx_gt_2yr) as hiv_dx_gt_2yr_sum,
(i.hiv_trmt_regimen_1yr * c.hiv_trmt_regimen_1yr) as hiv_trmt_regimen_1yr_sum,
(i.hiv_trmt_regimen_1_to2yr * c.hiv_trmt_regimen_1_to2yr) as hiv_trmt_regimen_1_to2yr_sum,
(i.hiv_trmt_regimen_gt_2yr * c.hiv_trmt_regimen_gt_2yr) as hiv_trmt_regimen_gt_2yr_sum,
(i.pep_rx_dates_num * c.pep_rx_dates_num) as pep_rx_dates_num_sum,
(i.prep_rx_num_1yr * c.prep_rx_num_1yr) as prep_rx_num_1yr_sum,
(i.prep_rx_num_1_to_2yr * c.prep_rx_num_1_to_2yr) as prep_rx_num_1_to_2yr_sum,
(i.prep_rx_num_gt_2yr * c.prep_rx_num_gt_2yr) as prep_rx_num_gt_2yr_sum,
(i.dx_abn_anal_cyt_yrs * c.dx_abn_anal_cyt_yrs) as dx_abn_anal_cyt_yrs_sum,
(i.dx_syph_yrs * c.dx_syph_yrs) as dx_syph_yrs_sum,
(i.dx_anal_syph_yrs * c.dx_anal_syph_yrs) as dx_anal_syph_yrs_sum,
(i.dx_gon_inf_rectum_yrs * c.dx_gon_inf_rectum_yrs) as dx_gon_inf_rectum_yrs_sum,
(i.dx_gon_pharyn_yrs * c.dx_gon_pharyn_yrs) as dx_gon_pharyn_yrs_sum,
(i.dx_chlam_inf_rectum_yrs * c.dx_chlam_inf_rectum_yrs) as dx_chlam_inf_rectum_yrs_sum,
(i.dx_chlam_pharyn_yrs * c.dx_chlam_pharyn_yrs) as dx_chlam_pharyn_yrs_sum,
(i.dx_lymph_ven_yrs * c.dx_lymph_ven_yrs) as dx_lymph_ven_yrs_sum,
(i.dx_chancroid_yrs * c.dx_chancroid_yrs) as dx_chancroid_yrs_sum,
(i.dx_nongon_ureth_yrs * c.dx_nongon_ureth_yrs) as dx_nongon_ureth_yrs_sum,
(i.dx_hsv_w_compl_yrs * c.dx_hsv_w_compl_yrs) as dx_hsv_w_compl_yrs_sum,
(i.dx_genital_herpes_yrs * c.dx_genital_herpes_yrs) as dx_genital_herpes_yrs_sum,
(i.dx_anogenital_warts_yrs * c.dx_anogenital_warts_yrs) as dx_anogenital_warts_yrs_sum,
(i.dx_anorectal_ulcer_yrs * c.dx_anorectal_ulcer_yrs) as dx_anorectal_ulcer_yrs_sum,
(i.dx_trich_yrs * c.dx_trich_yrs) as dx_trich_yrs_sum,
(i.dx_mpox_yrs * c.dx_mpox_yrs) as dx_mpox_yrs_sum,
(i.dx_unspec_std_yrs * c.dx_unspec_std_yrs) as dx_unspec_std_yrs_sum,
(i.dx_pid_yrs * c.dx_pid_yrs) as dx_pid_yrs_sum,
(i.dx_cont_w_exp_to_vd_yrs * c.dx_cont_w_exp_to_vd_yrs) as dx_cont_w_exp_to_vd_yrs_sum,
(i.dx_high_risk_sex_behav_yrs * c.dx_high_risk_sex_behav_yrs) as dx_high_risk_sex_behav_yrs_sum,
(i.dx_hiv_counsel_yrs * c.dx_hiv_counsel_yrs) as dx_hiv_counsel_yrs_sum,
(i.dx_anorexia_yrs * c.dx_anorexia_yrs) as dx_anorexia_yrs_sum,
(i.dx_bulimia_yrs * c.dx_bulimia_yrs) as dx_bulimia_yrs_sum,
(i.dx_eat_disorder_nos_yrs * c.dx_eat_disorder_nos_yrs) as dx_eat_disorder_nos_yrs_sum,
(i.dx_gend_iden_dis_yrs * c.dx_gend_iden_dis_yrs) as dx_gend_iden_dis_yrs_sum,
(i.dx_transsexualism_yrs * c.dx_transsexualism_yrs) as dx_transsexualism_yrs_sum,
(i.dx_couns_or_prb_child_sex_abuse_yrs * c.dx_couns_or_prb_child_sex_abuse_yrs) as dx_couns_or_prb_child_sex_abuse_yrs_sum,
(i.dx_adult_child_sex_exp_or_abuse_yrs * c.dx_adult_child_sex_exp_or_abuse_yrs) as dx_adult_child_sex_exp_or_abuse_yrs_sum,
(i.dx_foreign_body_anus_yrs * c.dx_foreign_body_anus_yrs) as dx_foreign_body_anus_yrs_sum,
(i.dx_alcohol_depend_abuse_yrs * c.dx_alcohol_depend_abuse_yrs) as dx_alcohol_depend_abuse_yrs_sum,
(i.dx_opioid_depend_abuse_yrs * c.dx_opioid_depend_abuse_yrs) as dx_opioid_depend_abuse_yrs_sum,
(i.dx_sed_hypn_anxio_depend_abuse_yrs * c.dx_sed_hypn_anxio_depend_abuse_yrs) as dx_sed_hypn_anxio_depend_abuse_yrs_sum,
(i.dx_cocaine_depend_abuse_yrs * c.dx_cocaine_depend_abuse_yrs) as dx_cocaine_depend_abuse_yrs_sum,
(i.dx_amphet_stim_depend_abuse_yrs * c.dx_amphet_stim_depend_abuse_yrs) as dx_amphet_stim_depend_abuse_yrs_sum,
(i.dx_oth_psycho_or_nos_subs_depend_abuse_yrs * c.dx_oth_psycho_or_nos_subs_depend_abuse_yrs) as dx_oth_psycho_or_nos_subs_depend_abuse_yrs_sum,
(i.dx_sti_screen_yrs * c.dx_sti_screen_yrs) as dx_sti_screen_yrs_sum,
(i.dx_malig_neo_rec_screen_yrs * c.dx_malig_neo_rec_screen_yrs) as dx_malig_neo_rec_screen_yrs_sum,
(i.dx_needle_stick * c.dx_needle_stick) as dx_needle_stick_sum,
(i.rx_bicillin_ever * c.rx_bicillin_ever) as rx_bicillin_ever_sum,
(i.rx_azith_ever * c.rx_azith_ever) as rx_azith_ever_sum,
(i.rx_ceft_ever * c.rx_ceft_ever) as rx_ceft_ever_sum,
(i.rx_doxy_ever * c.rx_doxy_ever) as rx_doxy_ever_sum,
(i.rx_methadone_ever * c.rx_methadone_ever) as rx_methadone_ever_sum,
(i.rx_buprenorphine_ever * c.rx_buprenorphine_ever) as rx_buprenorphine_ever_sum,
(i.rx_naltrexone_ever * c.rx_naltrexone_ever) as rx_naltrexone_ever_sum,
(i.rx_viagara_cilais_or_levitra_ever * c.rx_viagara_cilais_or_levitra_ever) as rx_viagara_cilais_or_levitra_ever_sum,
(i.vax_mpox_ever * c.vax_mpox_ever) as vax_mpox_ever_sum,
(i.vax_hpv_male_ever * c.vax_hpv_male_ever) as vax_hpv_male_ever_sum
FROM hiv_rpt.hivstirs_index_data i,
hiv_rpt.hivstirs_coefficients c,
hiv_rpt.hivstirs_index_patients p,
hiv_rpt.hivstirs_risk_scores rs
WHERE i.patient_id = p.patient_id
and i.patient_id = rs.patient_id
and rs.hiv_sti_risk_score > .02;

DROP TABLE IF EXISTS hiv_rpt.hivstirs_risk_factor_total_w_descrip;
CREATE TABLE hiv_rpt.hivstirs_risk_factor_total_w_descrip AS
SELECT patient_id, age_on_anchor_date_sum as master_sum, 'Age (in years) on score computation date' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_mapped_sum as master_sum, 'Derived Gender' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_male_sum as master_sum, 'Gender: Male' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_female_sum as master_sum, 'Gender:Female' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_trans_sum as master_sum, 'Gender:Trans' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, birth_sex_male_sum as master_sum, 'Birth Sex:Male' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, birth_sex_unk_sum as master_sum, 'Birth Sex:Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sexor_straight_sum as master_sum, 'Sex Orientation: Straight' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sexor_gay_lesbian_sum as master_sum, 'Sex Orientation: Gay/Lesbian' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sexor_other_sum as master_sum, 'Sex Orientation: Other' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_male_sum as master_sum, 'Gender Identity: Male' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_trans_male_sum as master_sum, 'Gender Identity: Trans Male' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_trans_female_sum as master_sum, 'Gender Identity: Trans Female' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_nonbinary_sum as master_sum, 'Gender Identity: Nonbinary' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_other_sum as master_sum, 'Gender Identity: Other' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gender_iden_unk_sum as master_sum, 'Gender Identity: Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, race_white_sum as master_sum, 'Race: White' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, race_black_sum as master_sum, 'Race: Black' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, race_other_sum as master_sum, 'Race: Other' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, race_unk_sum as master_sum, 'Race: Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, ethnicity_hispanic_sum as master_sum, 'Ethnicity: Hispanic' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, ethnicity_nonhispanic_sum as master_sum, 'Ethnicity: Non-Hispanic' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, ethnicity_unk_sum as master_sum, 'Ethnicity: Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sex_partner_male_sum as master_sum, 'Sex Partner: Male' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sex_partner_female_sum as master_sum, 'Sex Partner: Female' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sex_partner_mandf_sum as master_sum, 'Sex Partner: Male & Female' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sex_partner_trans_sum as master_sum, 'Sex Partner: Trans' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, sex_partner_unk_sum as master_sum, 'Sex Partner: Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_married_sum as master_sum, 'Marital Status: Married' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_partner_sum as master_sum, 'Marital Status: Partner' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_unk_sum as master_sum, 'Marital Status: Unknown' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_widow_sum as master_sum, 'Marital Status: Widow' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_divorced_sum as master_sum, 'Marital Status: Female' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, marital_stat_other_sum as master_sum, 'Marital Status: Other' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, num_encs_1yr_sum as master_sum, 'Clinical Encounter Visits: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, num_encs_1_to_2yr_sum as master_sum, 'Clinical Encounter Visits: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, num_encs_gt_2yr_sum as master_sum, 'Clinical Encounter Visits: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_1yr_sum as master_sum, 'Gonorrhea Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_1_to_2yr_sum as master_sum, 'Gonorrhea Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_gt_2yr_sum as master_sum, 'Gonorrhea Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_pos_tests_1yr_sum as master_sum, 'Gonorrhea Positive Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_pos_tests_1_to_2yr_sum as master_sum, 'Gonorrhea Positive Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_pos_tests_gt_2yr_sum as master_sum, 'Gonorrhea Positive Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_1yr_sum as master_sum, 'Gonorrhea Rectal Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_1_to_2yr_sum as master_sum, 'Gonorrhea Rectal Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_gt_2yr_sum as master_sum, 'Gonorrhea Rectal Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_pos_1yr_sum as master_sum, 'Gonorrhea Positive Rectal Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_pos_1_to_2yr_sum as master_sum, 'Gonorrhea Positive Rectal Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_rectal_pos_gt_2yr_sum as master_sum, 'Gonorrhea Positive Rectal Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_1yr_sum as master_sum, 'Gonorrhea Oral Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_1_to_2yr_sum as master_sum, 'Gonorrhea Oral Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_gt_2yr_sum as master_sum, 'Gonorrhea Oral Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_pos_1yr_sum as master_sum, 'Gonorrhea Positive Oral Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_pos_1_to_2yr_sum as master_sum, 'Gonorrhea Positive Oral Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, gon_tests_oral_pos_gt_2yr_sum as master_sum, 'Gonorrhea Positive Oral Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_1yr_sum as master_sum, 'Chlamydia Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_pos_tests_1yr_sum as master_sum, 'Chlamydia Positive Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_pos_tests_1_to_2yr_sum as master_sum, 'Chlamydia Positive Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_pos_tests_gt_2yr_sum as master_sum, 'Chlamydia Positive Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_1yr_sum as master_sum, 'Chlamydia Rectal Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_1_to_2yr_sum as master_sum, 'Chlamydia Rectal Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_gt_2yr_sum as master_sum, 'Chlamydia Rectal Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_pos_1yr_sum as master_sum, 'Chlamydia Positive Rectal Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_pos_1_to_2yr_sum as master_sum, 'Chlamydia Positive Rectal Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_rectal_pos_gt_2yr_sum as master_sum, 'Chlamydia Positive Rectal Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_1yr_sum as master_sum, 'Chlamydia Oral Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_1_to_2yr_sum as master_sum, 'Chlamydia Oral Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_gt_2yr_sum as master_sum, 'Chlamydia Oral Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_pos_1yr_sum as master_sum, 'Chlamydia Positive Oral Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_pos_1_to_2yr_sum as master_sum, 'Chlamydia Positive Oral Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, chlam_tests_oral_pos_gt_2yr_sum as master_sum, 'Chlamydia Positive Oral Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_tests_1yr_sum as master_sum, 'Syphilis Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_tests_1_to_2yr_sum as master_sum, 'Syphilis Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_tests_gt_2yr_sum as master_sum, 'Syphilis Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_per_esp_1yr_sum as master_sum, 'Syphilis ESP Cases: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_per_esp_1_to_2yr_sum as master_sum, 'Syphilis ESP Cases: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, syph_per_esp_gt_2yr_sum as master_sum, 'Syphilis ESP Cases: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_ab_1yr_sum as master_sum, 'HCV AB Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_ab_1_to_2yr_sum as master_sum, 'HCV AB Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_ab_gt_2yr_sum as master_sum, 'HCV AB Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_rna_1yr_sum as master_sum, 'HCV RNA Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_rna_1_to_2yr_sum as master_sum, 'HCV RNA Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_rna_gt_2yr_sum as master_sum, 'HCV RNA Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_ab_or_rna_pos_ev_sum as master_sum, 'HCV AB or HCV RNA Positive Test: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_ab_pos_first_yr_sum as master_sum, 'Years Since: First Positive HCV AB' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_rna_pos_first_yr_sum as master_sum, 'Years Since: First Positive HCV RNA' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_acute_per_esp_sum as master_sum, 'HCV Acute Per ESP: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hcv_acute_per_esp_case_yrs_sum as master_sum, 'Years Since: Acute HCV ESP Case' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_sag_1yr_sum as master_sum, 'HBV Surface Antigen Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_sag_1_to_2yr_sum as master_sum, 'HBV Surface Antigen Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_sag_gt_2yr_sum as master_sum, 'HBV Surface Antigen Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_dna_1yr_sum as master_sum, 'HBV DNA Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_dna_1_to_2yr_sum as master_sum, 'HBV DNA Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_dna_gt_2yr_sum as master_sum, 'HBV DNA Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_sag_or_dna_pos_first_yr_sum as master_sum, 'Years Since: First Positive HBV Surface Antigen or HBV DNA' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_acute_per_esp_sum as master_sum, 'HBV Acute Per ESP: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_acute_per_esp_case_yrs_sum as master_sum, 'Years Since: Acute HBV ESP Case' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hbv_history_ev_sum as master_sum, 'HBV History: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_elisa_ag_ab_1yr_sum as master_sum, 'HIV Elisa or AG/AB Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_elisa_ag_ab_1_to_2yr_sum as master_sum, 'HIV Elisa or AG/AB Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_elisa_ag_ab_gt_2yr_sum as master_sum, 'HIV Elisa or AG/AB Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_confirm_tests_1yr_sum as master_sum, 'HIV Confirmatory Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_confirm_tests_1_to_2yr_sum as master_sum, 'HIV Confirmatory Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_confirm_tests_gt_2yr_sum as master_sum, 'HIV Confirmatory Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_rna_viral_1yr_sum as master_sum, 'HIV RNA Viral Tests: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_rna_viral_1_to_2yr_sum as master_sum, 'HIV RNA Viral Tests: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_rna_viral_gt_2yr_sum as master_sum, 'HIV RNA Viral Tests: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_max_viral_load_yr_sum as master_sum, 'Years Since: Maximum HIV Viral Load Recorded' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_neg_w_hiv_med_ev_sum as master_sum, 'HIV Negative with HIV Med Ingredient RX (includes Oral PrEP): Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, pep_rx_dates_num_sum as master_sum, 'HIV PEP Medication Order Dates' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_dx_1_yr_sum as master_sum, 'HIV DX Encounter Dates: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_dx_1_to_2yr_sum as master_sum, 'HIV DX Encounter Dates: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_dx_gt_2yr_sum as master_sum, 'HIV DX Encounter Dates: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_trmt_regimen_1yr_sum as master_sum, 'HIV Treatment Regimen: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_trmt_regimen_1_to2yr_sum as master_sum, 'HIV Treatment Regimen: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, hiv_trmt_regimen_gt_2yr_sum as master_sum, 'HIV Treatment Regimen: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, prep_rx_num_1yr_sum as master_sum, 'HIV PrEP 1 month prescriptions: 0-1 yr prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, prep_rx_num_1_to_2yr_sum as master_sum, 'HIV PrEP 1 month prescriptions: 1-2 yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, prep_rx_num_gt_2yr_sum as master_sum, 'HIV PrEP 1 month prescriptions: 2+ yrs prior' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_abn_anal_cyt_yrs_sum as master_sum, 'Years Since: First Abnormal anal cytology, anal dysplasia, or anal carcinoma in situ DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_syph_yrs_sum as master_sum, 'Years Since: First Syphilis of any site or stage except late latent  DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_anal_syph_yrs_sum as master_sum, 'Years Since: First Anal syphilis DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_gon_inf_rectum_yrs_sum as master_sum, 'Years Since: First Gonococcal infection of anus and rectum DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_gon_pharyn_yrs_sum as master_sum, 'Years Since: First Gonococcal pharyngitis DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_chlam_inf_rectum_yrs_sum as master_sum, 'Years Since: First Chlamydial infection of anus and rectum DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_chlam_pharyn_yrs_sum as master_sum, 'Years Since: First Chlamydial infection of pharynx DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_lymph_ven_yrs_sum as master_sum, 'Years Since: First Lymphgranuloma venereum DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_chancroid_yrs_sum as master_sum, 'Years Since: First Chancroid DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_nongon_ureth_yrs_sum as master_sum, 'Years Since: First Nongonococcal urethritis DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_hsv_w_compl_yrs_sum as master_sum, 'Years Since: First Herpes simplex with complications DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_genital_herpes_yrs_sum as master_sum, 'Years Since: First Genital herpes DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_anogenital_warts_yrs_sum as master_sum, 'Years Since: First Anogenital warts DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_anorectal_ulcer_yrs_sum as master_sum, 'Years Since: First Anorectal ulcer DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_trich_yrs_sum as master_sum, 'Years Since: First Trichomonas DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_mpox_yrs_sum as master_sum, 'Years Since: First Mpox DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_unspec_std_yrs_sum as master_sum, 'Years Since: First Unspecified STD DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_pid_yrs_sum as master_sum, 'Years Since: First Pelvic inflammatory disease DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_cont_w_exp_to_vd_yrs_sum as master_sum, 'Years Since: First Contact with or exposure to venereal disease DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_high_risk_sex_behav_yrs_sum as master_sum, 'Years Since: First High risk sexual behavior DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_hiv_counsel_yrs_sum as master_sum, 'Years Since: First HIV counseling DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_anorexia_yrs_sum as master_sum, 'Years Since: First Anorexia nervosa DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_bulimia_yrs_sum as master_sum, 'Years Since: First Bulimia nervosa DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_eat_disorder_nos_yrs_sum as master_sum, 'Years Since: First Eating disorder NOS DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_gend_iden_dis_yrs_sum as master_sum, 'Years Since: First Gender identity disorders DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_transsexualism_yrs_sum as master_sum, 'Years Since: First Trans-sexualism DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_couns_or_prb_child_sex_abuse_yrs_sum as master_sum, 'Years Since: First Counseling or problems relating to child sexual abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_adult_child_sex_exp_or_abuse_yrs_sum as master_sum, 'Years Since: First Adult or child sexual exploitation or sexual abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_foreign_body_anus_yrs_sum as master_sum, 'Years Since: First Foreign body in anus DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_alcohol_depend_abuse_yrs_sum as master_sum, 'Years Since: First Alcohol dependence/abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_opioid_depend_abuse_yrs_sum as master_sum, 'Years Since: First Opioid dependence/abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_sed_hypn_anxio_depend_abuse_yrs_sum as master_sum, 'Years Since: First Sedative, hypnotic, or anxiolytic dependence/abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_cocaine_depend_abuse_yrs_sum as master_sum, 'Years Since: First Cocaine dependence/abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_amphet_stim_depend_abuse_yrs_sum as master_sum, 'Years Since: First Amphetamine dependence DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_oth_psycho_or_nos_subs_depend_abuse_yrs_sum as master_sum, 'Years Since: First Unspecified drug dependence/abuse DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_sti_screen_yrs_sum as master_sum, 'Years Since: First Encntr screen for infections w sexl mode of transmiss DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_malig_neo_rec_screen_yrs_sum as master_sum, 'Years Since: First Encounter for screening for malignant neoplasm of rectum DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, dx_needle_stick_sum as master_sum, 'Years Since: First Hypodermic/Contaminated needle contact/accident DX Code' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_bicillin_ever_sum as master_sum, 'Bicillin 2.4 MU injection RX Record: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_azith_ever_sum as master_sum, 'Azithromycin 1g PO RX Record: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_ceft_ever_sum as master_sum, 'Ceftriaxone 125mg or 250mg or 500mg IV/IM RX Record: Ever ' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_doxy_ever_sum as master_sum, 'Doxycycline 100mg PO with Quantity >= 28 RX Record: Ever ' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_methadone_ever_sum as master_sum, 'Methadone RX Record: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_buprenorphine_ever_sum as master_sum, 'Buprenorphine RX Record: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_naltrexone_ever_sum as master_sum, 'Naltrexone RX Record: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, rx_viagara_cilais_or_levitra_ever_sum as master_sum, 'Sildanefil, Tadalafil, or Vardenafil RX Record: Ever ' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, vax_mpox_ever_sum as master_sum, 'Mpox Vaccine: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals UNION
SELECT patient_id, vax_hpv_male_ever_sum as master_sum, 'HPV Vaccine in Male age >= 27: Ever' as description FROM hiv_rpt.hivstirs_risk_factor_totals
;

-- Rank the risk factors
DROP TABLE IF EXISTS hiv_rpt.hivstirs_risk_factor_rank;
CREATE TABLE hiv_rpt.hivstirs_risk_factor_rank AS
SELECT patient_id,
description, master_sum,
RANK () OVER ( 
		PARTITION BY patient_id
		ORDER BY master_sum DESC
	) risk_factor_rank
FROM 
hiv_rpt.hivstirs_risk_factor_total_w_descrip T1;

-- Identify first and second highest risk factor for each patient
-- If second highest is zero value (or less) customize the output

DROP TABLE IF EXISTS hiv_rpt.hivstirs_hightest_risk_factors;
CREATE TABLE hiv_rpt.hivstirs_hightest_risk_factors AS
SELECT T1.patient_id,
T3.hiv_sti_risk_score,
T1.description as highest_risk_variable,
CASE WHEN T2.description is null then 'NONE' ELSE T2.description END as second_highest_risk_variable,
CASE WHEN T4.description is null then 'NONE' ELSE T4.description END as third_highest_risk_variable
FROM hiv_rpt.hivstirs_risk_factor_rank T1
LEFT JOIN hiv_rpt.hivstirs_risk_factor_rank T2 ON (T1.patient_id = T2.patient_id AND T2.risk_factor_rank = 2 and T2.master_sum > 0)
LEFT JOIN hiv_rpt.hivstirs_risk_factor_rank T4 ON (T1.patient_id = T4.patient_id AND T4.risk_factor_rank = 3 and T4.master_sum > 0)
JOIN hiv_rpt.hivstirs_risk_scores T3 ON (T1.patient_id = T3.patient_id)
WHERE T1.risk_factor_rank = 1;

SELECT 'PROCESSING COMPLETE';

