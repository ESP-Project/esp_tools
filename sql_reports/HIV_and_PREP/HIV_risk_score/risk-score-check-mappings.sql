-- Check for unmapped races
select distinct(upper(race))
from emr_patient
where upper(race) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'race');

--validation example
SELECT count(*), race, race_white, race_black, race_other, race_unk, race_asian
FROM (
SELECT race,
CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'CAUCASIAN') then 1 ELSE 0 END as race_white,
CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'BLACK') then 1 ELSE 0 END as race_black,
CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'OTHER') then 1 ELSE 0 END as race_other,
CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'ASIAN') then 1 ELSE 0 END as race_asian,
CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'Unspecified') 
						  or race is null then 1 ELSE 0 END as race_unk
FROM emr_patient
	) T1
GROUP BY race, race_white, race_black, race_other, race_unk, race_asian;

--Check for unmapped ethnicity
select distinct(upper(ethnicity))
from emr_patient
where upper(ethnicity) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'ethnicity');

-- Validate/Confirm mappings
select count(*), race, ethnicity, ethnicity_hispanic, ethnicity_nonhispanic, ethnicity_unk
FROM (
SELECT race, ethnicity,
CASE when (upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC') 
			OR
			upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'HISPANIC'))
	        then 1 ELSE 0 END as ethnicity_hispanic,
CASE when upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'NOT HISPANIC') 
						  then 1 ELSE 0 end as  ethnicity_nonhispanic,
CASE when  (
           (upper(race) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC')
			     OR race is null)
			AND
			(upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'Unspecified') 
						  OR ethnicity is null)
		  )
			then 1 ELSE 0 END as ethnicity_unk

FROM emr_patient
) T1
GROUP BY race, ethnicity, ethnicity_hispanic, ethnicity_nonhispanic, ethnicity_unk;

--Check for unmapped sex orientation
select distinct(upper(sex_orientation))
from emr_patient
where upper(sex_orientation) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'sex_orientation');

-- Validate/Confirm mappings
select count(*), sex_orientation, sexor_straight, sexor_gay_lesbian, sexor_bisexual, sexor_other, sexor_unk
FROM 
(
SELECT sex_orientation,
CASE when upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'STRAIGHT') then 1 ELSE 0 END as sexor_straight,
CASE when upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'GAY/LESBIAN') then 1 ELSE 0 END as sexor_gay_lesbian,
CASE when upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'BISEXUAL') then 1 ELSE 0 END as sexor_bisexual, 
CASE when upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'OTHER') then 1 ELSE 0 END as sexor_other, 
CASE when upper(sex_orientation) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'sex_orientation' and mapped_value = 'UNKNOWN') or sex_orientation is null then 1 ELSE 0 END as sexor_unk
FROM emr_patient
) T1						  
GROUP BY sex_orientation, sexor_straight, sexor_gay_lesbian, sexor_bisexual, sexor_other, sexor_unk;


--Check for unmapped gender identity
select distinct(upper(gender_identity))
from emr_patient
where upper(gender_identity) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'gender_identity');	

-- Validate/Confirm mappings
SELECT count(*), gender_identity, gender_iden_male, gender_iden_female, gender_iden_trans_male, gender_iden_trans_female, gender_iden_nonbinary, gender_iden_other, gender_iden_unk
FROM (
SELECT gender_identity,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'MALE') then 1 ELSE 0 END as gender_iden_male,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'FEMALE') then 1 ELSE 0 END as gender_iden_female,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'TRANS_MALE') then 1 ELSE 0 END as gender_iden_trans_male,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'TRANS_FEMALE') then 1 ELSE 0 END as gender_iden_trans_female,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'NON-BINARY') then 1 ELSE 0 END as gender_iden_nonbinary,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'OTHER') then 1 ELSE 0 END as gender_iden_other,
CASE when upper(gender_identity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'gender_identity' and mapped_value = 'UNKNOWN')  or gender_identity is null then 1 ELSE 0 END as gender_iden_unk
FROM emr_patient
) T1
GROUP BY gender_identity, gender_iden_male, gender_iden_female, gender_iden_trans_male, gender_iden_trans_female, gender_iden_nonbinary, gender_iden_other, gender_iden_unk;


--Check for unmapped marital status
select distinct(upper(marital_stat))
from emr_patient
where upper(marital_stat) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'marital_stat');

SELECT marital_stat, marital_stat_married, marital_stat_single, marital_stat_partner, marital_stat_unk, marital_stat_widow, marital_stat_divorced,
marital_stat_other
FROM (
SELECT marital_stat,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'MARRIED') then 1 ELSE 0 END as marital_stat_married,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'SINGLE') then 1 ELSE 0 END as marital_stat_single,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'PARTNER') then 1 ELSE 0 END as marital_stat_partner,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'UNKNOWN') or marital_stat is null then 1 ELSE 0 END as marital_stat_unk,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'WIDOWED') then 1 ELSE 0 END as marital_stat_widow,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'DIVORCED') then 1 ELSE 0 END as marital_stat_divorced,
CASE when upper(marital_stat) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'marital_stat' and mapped_value = 'OTHER') then 1 ELSE 0 END as marital_stat_other	
FROM emr_patient
) T1
GROUP BY marital_stat, marital_stat_married, marital_stat_single, marital_stat_partner, marital_stat_unk, marital_stat_widow, marital_stat_divorced, marital_stat_other;








					  






	 