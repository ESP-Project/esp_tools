



select count(*) total_score_pats, 
count(CASE WHEN hiv_sti_risk_score < .01 THEN 1 END) score_lt_01,
count(CASE WHEN hiv_sti_risk_score >=.01 and hiv_sti_risk_score < .02 THEN 1 END) score_p01_to_p02,
count(CASE WHEN hiv_sti_risk_score >=.02 and hiv_sti_risk_score < .03 THEN 1 END) score_p02_to_p03,
count(CASE WHEN hiv_sti_risk_score >=.03 and hiv_sti_risk_score < .04 THEN 1 END) score_p03_to_p04,
count(CASE WHEN hiv_sti_risk_score >=.04 and hiv_sti_risk_score < .05 THEN 1 END) score_p04_to_p05,
count(CASE WHEN hiv_sti_risk_score >=.05 and hiv_sti_risk_score < .06 THEN 1 END) score_p05_to_p06,
count(CASE WHEN hiv_sti_risk_score >=.06 and hiv_sti_risk_score < .07 THEN 1 END) score_p06_to_p07,
count(CASE WHEN hiv_sti_risk_score >=.07 and hiv_sti_risk_score < .08 THEN 1 END) score_p07_to_p08,
count(CASE WHEN hiv_sti_risk_score >=.08 and hiv_sti_risk_score < .09 THEN 1 END) score_p08_to_p09,
count(CASE WHEN hiv_sti_risk_score >=.09 and hiv_sti_risk_score < .1 THEN 1 END) score_p09_to_1,
FROM gen_pop_tools.hiv_sti_risk_scores


select count(*) as total_total_score_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY hiv_sti_risk_score)::numeric hiv_sti_risk_score_99th
FROM gen_pop_tools.hiv_sti_risk_scores


select count(*) total_score_pats, 
count(CASE WHEN hiv_sti_risk_score < .1 THEN 1 END) score_lt_p1,
count(CASE WHEN hiv_sti_risk_score >=.1 and hiv_sti_risk_score < .2 THEN 1 END) score_p1_to_p2,
count(CASE WHEN hiv_sti_risk_score >=.2 and hiv_sti_risk_score < .3 THEN 1 END) score_p2_to_p3,
count(CASE WHEN hiv_sti_risk_score >=.3 and hiv_sti_risk_score < .4 THEN 1 END) score_p3_to_p4,
count(CASE WHEN hiv_sti_risk_score >=.4 and hiv_sti_risk_score < .5 THEN 1 END) score_p4_to_p5,
count(CASE WHEN hiv_sti_risk_score >=.5 and hiv_sti_risk_score < .6 THEN 1 END) score_p5_to_p6,
count(CASE WHEN hiv_sti_risk_score >=.6 and hiv_sti_risk_score < .7 THEN 1 END) score_p6_to_p7,
count(CASE WHEN hiv_sti_risk_score >=.7 and hiv_sti_risk_score < .8 THEN 1 END) score_p7_to_p8,
count(CASE WHEN hiv_sti_risk_score >=.8 and hiv_sti_risk_score < .9 THEN 1 END) score_p8_to_p9,
count(CASE WHEN hiv_sti_risk_score >=.9 THEN 1 END) score_gte_9
FROM gen_pop_tools.hiv_sti_risk_scores

select count(*) total_score_pats, 
count(CASE WHEN hiv_sti_risk_score < .01 THEN 1 END) score_lt_01,
count(CASE WHEN hiv_sti_risk_score >=.01 and hiv_sti_risk_score < .02 THEN 1 END) score_p01_to_p02,
count(CASE WHEN hiv_sti_risk_score >=.02 and hiv_sti_risk_score < .03 THEN 1 END) score_p02_to_p03,
count(CASE WHEN hiv_sti_risk_score >=.03 and hiv_sti_risk_score < .04 THEN 1 END) score_p03_to_p04,
count(CASE WHEN hiv_sti_risk_score >=.04 and hiv_sti_risk_score < .05 THEN 1 END) score_p04_to_p05,
count(CASE WHEN hiv_sti_risk_score >=.05 and hiv_sti_risk_score < .06 THEN 1 END) score_p05_to_p06,
count(CASE WHEN hiv_sti_risk_score >=.06 and hiv_sti_risk_score < .07 THEN 1 END) score_p06_to_p07,
count(CASE WHEN hiv_sti_risk_score >=.07 and hiv_sti_risk_score < .08 THEN 1 END) score_p07_to_p08,
count(CASE WHEN hiv_sti_risk_score >=.08 and hiv_sti_risk_score < .09 THEN 1 END) score_p08_to_p09,
count(CASE WHEN hiv_sti_risk_score >=.09 and hiv_sti_risk_score < .1 THEN 1 END) score_p09_to_1
FROM gen_pop_tools.hiv_sti_risk_scores
WHERE hiv_sti_risk_score < .1;

