
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_events;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_cases;
DROP TABLE IF EXISTS kre_report.hiv_hrs_eval_labs;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_new_diag_pats;
DROP TABLE IF EXISTS kre_report.hiv_hrs_pats_anchor_dates;
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_pats;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_pats_and_anchor_dates;
DROP TABLE IF EXISTS kre_report.hiv_hrs_index_patients;
DROP TABLE IF EXISTS kre_report.hiv_hrs_clin_enc_visits_all;
DROP TABLE IF EXISTS kre_report.hiv_hrs_clin_enc_visits_num;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_chlam_gon_labs_events;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_chlam_gon_labs_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_events;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_cases;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_case_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_nohef;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_nohef_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_hef_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_case_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hepb_diags;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events_nohef;
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events_nohef_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_viral_loads;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_case_values;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_combo_distinct;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_3_diff;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_3_diff_count;
DROP TABLE IF EXISTS kre_report.hiv_hrs_pep_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_pep_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_meds_no_pep;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_all;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_compute;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_eval;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_inj_prep_compute;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_inf_prep_eval;
DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_union;
DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_union_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_final;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_dx_codes;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_dx_code_counts;
DROP TABLE IF EXISTS kre_report.hiv_hrs_dx_codes;
DROP TABLE IF EXISTS kre_report.hiv_hrs_dx_code_years;
DROP TABLE IF EXISTS kre_report.hiv_hrs_bicillin_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_bicillin_meds_updated;
DROP TABLE IF EXISTS kre_report.hiv_hrs_azith_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_ceft_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_doxy_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_other_meds;
DROP TABLE IF EXISTS kre_report.hiv_hrs_mpox_vax;
DROP TABLE IF EXISTS kre_report.hiv_hrs_hpv_vax;
DROP TABLE IF EXISTS kre_report.hiv_hrs_pats_to_drop;
DROP TABLE IF EXISTS kre_report.hiv_hrs_index_patients_w_voi;
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_pats_unique;
DROP TABLE IF EXISTS kre_report.hiv_hrs_num_ctrl_pats;
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_patients_final;
DROP TABLE IF EXISTS kre_report.hiv_hrs_report_pats_dates_final;
DROP TABLE IF EXISTS kre_report.hiv_hrs_demog;
DROP TABLE IF EXISTS kre_report.hiv_hrs_sex_p_gender;
DROP TABLE IF EXISTS kre_report.hiv_hrs_output_prep;
DROP TABLE IF EXISTS kre_report.hiv_hrs_output_final;

-- BUILD ANCHOR PATIENTS & DATES STEP 1
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_events;
CREATE TABLE kre_report.hiv_hrs_hiv_events AS
SELECT T1.patient_id,T1.name,T1.date 
FROM hef_event T1
WHERE T1.name in ('lx:hiv_ab_diff:positive', 'lx:hiv_ag_ab:negative', 'lx:hiv_ag_ab:positive', 
'lx:hiv_elisa:negative', 'lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive');

-- BUILD ANCHOR PATIENTS & DATES STEP 2
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_cases;
CREATE TABLE kre_report.hiv_hrs_hiv_cases AS
SELECT T1.patient_id, T1.id as case_id, T1.date as hiv_per_esp_date
FROM nodis_case T1
WHERE condition = 'hiv'
AND date >= '2015-01-01';

-- BUILD ANCHOR PATIENTS & DATES STEP 3
DROP TABLE IF EXISTS kre_report.hiv_hrs_eval_labs;
CREATE TABLE kre_report.hiv_hrs_eval_labs AS 
SELECT T2.patient_id, T2.case_id,hiv_per_esp_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then 1 else 0 end) pos_lab_met,
min(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) min_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive', 'lx:hiv_ab_diff:positive', 'lx:hiv_geenius:positive', 'lx:hiv_multispot:positive') and T1.date <= T2.hiv_per_esp_date then T1.date end) max_lab_pos_date,
max(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then 1 else 0 end) neg_lab_met,
min(case when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date > (T2.hiv_per_esp_date - INTERVAL '3 years') and T1.date < T2.hiv_per_esp_date then T1.date end) min_neg_lab_date
FROM kre_report.hiv_hrs_hiv_events T1 
RIGHT OUTER JOIN kre_report.hiv_hrs_hiv_cases T2 ON ((T1.patient_id = T2.patient_id)) 
GROUP BY T2.patient_id, T2.case_id, hiv_per_esp_date;

-- BUILD ANCHOR PATIENTS & DATES STEP 4
-- to handle events prior to having ab_diff type, need to filter out patients where the pos/neg closest to the case date are on the same date
-- use max_lab_pos_date as index date
-- filter out test patients
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_new_diag_pats;
CREATE TABLE kre_report.hiv_hrs_hiv_new_diag_pats AS
SELECT T1.*, 
CASE WHEN upper(T2.gender) in ('M', 'MALE') then 'M'
     WHEN upper(T2.gender) in ('F', 'FEMALE') then 'F' 
     WHEN upper(T2.gender) in ('T') then 'T'
     WHEN upper(T2.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') or gender is null then 'U'
	 ELSE 'U' END as gender
--T2.gender
FROM kre_report.hiv_hrs_eval_labs T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE pos_lab_met = 1
AND neg_lab_met = 1
AND max_lab_pos_date != min_neg_lab_date
AND last_name not ilike 'LABVALIDATION'
AND last_name not ilike 'XXXXX%'
AND last_name not ilike 'Fenway%Test';

DROP TABLE IF EXISTS kre_report.hiv_hrs_pats_anchor_dates;
CREATE TABLE kre_report.hiv_hrs_pats_anchor_dates AS
SELECT T1.patient_id, max_lab_pos_date, max(date) anchor_date, max_lab_pos_date - max(date) as date_diff, 1 as hiv_new_diag, T1.gender
	FROM kre_report.hiv_hrs_hiv_new_diag_pats T1
	LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
	--WHERE date < max_lab_pos_date
	-- date range for alternate run
	WHERE date <= max_lab_pos_date - INTERVAL '14 days'
	AND date >= max_lab_pos_date - INTERVAL '730 days'
	AND source = 'enc'
	GROUP BY T1.patient_id, max_lab_pos_date, T1.gender;
	
-- 	IMPORTANT!!!
-- YOU NEED TO HAVE RUN 2024-hiv-risk-model-dev-new-outcome-id.sql BEFORE CONTINUING!!
-- IMPORTANT!!!

-- BRING TOGETHER THE HIV NEW DIAG PATS AND STI RISK PATS
-- USE HIV NEW DIAG DATE FOR PATIENTS THAT ARE BOTH NEW HIV & STI RISK PATIENTS 
DROP TABLE IF EXISTS kre_report.hiv_hrs_pats_anchor_sti_hiv_pats;
CREATE TABLE kre_report.hiv_hrs_pats_anchor_sti_hiv_pats AS
SELECT patient_id, anchor_date, hiv_new_diag, null as sti_risk_pat, gender FROM kre_report.hiv_hrs_pats_anchor_dates
UNION
SELECT patient_id, anchor_date, null as hiv_new_diag, sti_risk_pat, gender FROM kre_report.hiv_hrs_sti_risk_anchor_dates WHERE patient_id not in (SELECT patient_id FROM kre_report.hiv_hrs_pats_anchor_dates);


--GET ALL PATIENTS WHO HAD CLINICAL ENCOUNTER IN THE SAME MONTH AS STI/HIV ANCHOR DATE
--ALSO MATCH ON PATIENT GENDER
--EXCLUDE PATIENTS WITH AN HIV CASE
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_pats;
CREATE TABLE kre_report.hiv_hrs_control_pats AS
SELECT DISTINCT T1.patient_id, T1.date as anchor_date, 
     CASE WHEN upper(T3.gender) in ('M', 'MALE') then 'M'
     WHEN upper(T3.gender) in ('F', 'FEMALE') then 'F' 
     WHEN upper(T3.gender) in ('T') then 'T'
     WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') or T3.gender is null then 'U'
	 ELSE 'U' END as gender 
FROM gen_pop_tools.clin_enc T1
INNER JOIN emr_patient T3 ON (T1.patient_id = T3.id)
--INNER JOIN kre_report.hiv_hrs_pats_anchor_dates T2 ON (to_char(T1.date, 'YYYY-MM') = to_char(T2.anchor_date, 'YYYY-MM') and T2.gender = T3.gender)
INNER JOIN kre_report.hiv_hrs_pats_anchor_sti_hiv_pats T2 ON (to_char(T1.date, 'YYYY-MM') = to_char(T2.anchor_date, 'YYYY-MM') and T2.gender = T3.gender)
-- REMOVE GENDER MATCH AT CHA
--INNER JOIN kre_report.hiv_hrs_pats_anchor_sti_hiv_pats T2 ON (to_char(T1.date, 'YYYY-MM') = to_char(T2.anchor_date, 'YYYY-MM')
WHERE T1.source = 'enc'
-- don't count new hiv diag or sti risk patients  patients
AND T1.patient_id not in (select patient_id from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats)
-- don't include patients with an HIV case
AND T1.patient_id not in (select patient_id from nodis_case where condition = 'hiv');


-- BRING TOGETHER ALL PATIENTS AND ANCHOR DATES
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_pats_and_anchor_dates;
CREATE TABLE kre_report.hiv_hrs_all_pats_and_anchor_dates AS 
SELECT patient_id, anchor_date, hiv_new_diag, gender
--FROM kre_report.hiv_hrs_pats_anchor_dates
FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats
UNION
SELECT patient_id, anchor_date, null as hiv_new_diag, gender
FROM kre_report.hiv_hrs_control_pats
;

-- ONLY INCLUDE PATIENTS WHO ARE >=15 ON ANCHOR DATE
DROP TABLE IF EXISTS kre_report.hiv_hrs_index_patients;
CREATE TABLE kre_report.hiv_hrs_index_patients AS 
SELECT T1.*, date_part('year', age(anchor_date, date_of_birth)) as anchor_age  
FROM kre_report.hiv_hrs_all_pats_and_anchor_dates T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE date_part('year', age(anchor_date, date_of_birth)) >= 15;

-- Clinical encounters of type "enc"
DROP TABLE IF EXISTS kre_report.hiv_hrs_clin_enc_visits_all;
CREATE TABLE kre_report.hiv_hrs_clin_enc_visits_all AS
SELECT DISTINCT T2.patient_id, T2.anchor_date, T1.date as enc_date, T1.source 
FROM gen_pop_tools.clin_enc T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE source = 'enc'
and date <= anchor_date
GROUP BY T2.patient_id, T2.anchor_date, T1.date, T1.source;

-- Number of clinical encounters of type "enc"
DROP TABLE IF EXISTS kre_report.hiv_hrs_clin_enc_visits_num;
CREATE TABLE kre_report.hiv_hrs_clin_enc_visits_num AS
SELECT patient_id, anchor_date,
count(case when enc_date >= anchor_date - interval '1 year' then 1 end) as num_encs_1yr,
count(case when (enc_date < anchor_date - interval '1 year') and (enc_date >= anchor_date - interval '2 years') then 1 end) as num_encs_1_to_2yr,
count(case when (enc_date < anchor_date - interval '2 year') then 1 end) as num_encs_gt_2yr
FROM kre_report.hiv_hrs_clin_enc_visits_all
GROUP BY patient_id, anchor_date;

--
-- ALL GONORRHEA/CHLAMYDIA LAB AND HEF EVENTS FOR INDEX PATIENTS
-- NORMALIZE SPECIMEN SOURCES FOR CHLAM/GON TESTS
-- 
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_chlam_gon_labs_events;
CREATE TABLE kre_report.hiv_hrs_all_chlam_gon_labs_events AS 
SELECT T3.patient_id, T3.anchor_date, T2.date as hef_date, T2.name as hef_name, 
CASE WHEN specimen_source ~* '(RECTUM|RECT|ANAL|ANUS)' then 'RECTAL'
WHEN specimen_source ~* '(PHARYNGEAL|THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA|URETHRA)' then 'UROG'
WHEN (native_name ~* '(PHARYNGEAL|THROAT)' or T1.native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (native_name ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR T1.native_code ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR procedure_name ~* '(RECTAL|ANAL|RECTUM|ANUS)') THEN 'RECTAL'
WHEN (native_name ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
                       OR T1.native_code  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
					   OR procedure_name  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' ) THEN 'UROG'
ELSE 'OTHER_OR_UNKNOWN' END specimen_source,
specimen_source specimen_source_orig,
T1.id as lab_id, T2.object_id, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id)
INNER JOIN kre_report.hiv_hrs_index_patients T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
WHERE 
(T2.name ilike 'lx:gonorrhea%' or T2.name ilike 'lx:chlamydia%')
AND T2.date <= T3.anchor_date;
	
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_chlam_gon_labs_counts;
CREATE TABLE kre_report.hiv_hrs_all_chlam_gon_labs_counts AS 
SELECT patient_id, anchor_date,
count(case when hef_name ilike 'lx:gonorrhea:%' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_tests_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_tests_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_tests_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_pos_tests_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_pos_tests_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_pos_tests_gt_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_tests_rectal_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_tests_rectal_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_tests_rectal_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_tests_rectal_pos_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_tests_rectal_pos_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_tests_rectal_pos_gt_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_tests_oral_1yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_tests_oral_1_to_2yr,
count(case when hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_tests_oral_gt_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and hef_date >= anchor_date - interval '1 year' then 1 end) as gon_tests_oral_pos_1yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as gon_tests_oral_pos_1_to_2yr,
count(case when hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' and hef_date < anchor_date - interval '2 year' then 1 end) as gon_tests_oral_pos_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_tests_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_tests_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_tests_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_pos_tests_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_pos_tests_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_pos_tests_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_tests_rectal_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_tests_rectal_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_tests_rectal_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_tests_rectal_pos_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_tests_rectal_pos_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_tests_rectal_pos_gt_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_tests_oral_1yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_tests_oral_1_to_2yr,
count(case when hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_tests_oral_gt_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and hef_date >= anchor_date - interval '1 year' then 1 end) as chlam_tests_oral_pos_1yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and (hef_date < anchor_date - interval '1 year') and (hef_date >= anchor_date - interval '2 years') then 1 end) as chlam_tests_oral_pos_1_to_2yr,
count(case when hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' and hef_date < anchor_date - interval '2 year' then 1 end) as chlam_tests_oral_pos_gt_2yr
FROM kre_report.hiv_hrs_all_chlam_gon_labs_events
GROUP BY patient_id, anchor_date;


-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_events;
CREATE TABLE kre_report.hiv_hrs_all_syph_events AS
SELECT T1.patient_id, T1.anchor_date, T2.date, T3.test_name--, T4.name as hef_name, result_string
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'tppa-csf', 'fta-abs', 'fta-abs-csf', 'tp-igg', 'tp-igm', 'tp-cia', 'rpr-riskscape')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= T1.anchor_date
GROUP BY T1.patient_id, T1.anchor_date, T3.test_name, T2.date--, result_string, T4.name
;

DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_counts;
CREATE TABLE kre_report.hiv_hrs_all_syph_counts AS
SELECT patient_id, anchor_date,
count(case when date >= anchor_date - interval '1 year' then 1 end) as syph_tests_1yr,
count(case when (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as syph_tests_1_to_2yr,
count(case when date < anchor_date - interval '2 year' then 1 end) as syph_tests_gt_2yr
FROM kre_report.hiv_hrs_all_syph_events
GROUP BY patient_id, anchor_date;	


-- All ESP cases of interest
-- Add in HepC Acute Cases
DROP TABLE IF EXISTS kre_report.hiv_hrs_cases;
CREATE TABLE kre_report.hiv_hrs_cases AS 
SELECT T1.patient_id, T2.anchor_date, T1.condition, T1.date as case_date
FROM nodis_case T1,
kre_report.hiv_hrs_index_patients T2
WHERE T1.patient_id = T2.patient_id
AND condition in ('syphilis', 'hiv') 
AND T1.date <= T2.anchor_date

UNION

-- CAN ONLY GO FROM Undetermined to Acute (or Chronic) so we just need all Hep C Acute Cases
SELECT T1.patient_id, T3.anchor_date, 'hepatitis_c:acute' as condition, T1.date as case_date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
kre_report.hiv_hrs_index_patients T3
WHERE T1.condition = 'hepatitis_c'
AND T1.date <= T3.anchor_date
and T2.status = 'HEP_C-A'
AND T1.id = T2.case_id 
AND T1.patient_id = T3.patient_id

UNION

-- Hep B Acute Cases
SELECT T1.patient_id, T3.anchor_date, 'hepatitis_b:acute' as condition, T1.date as case_date
FROM nodis_case T1, 
nodis_caseactivehistory T2,
kre_report.hiv_hrs_index_patients T3
WHERE T1.condition = 'hepatitis_b'
AND T1.date <= T3.anchor_date
and T2.status = 'HEP_B-A'
AND T1.id = T2.case_id 
AND T1.patient_id = T3.patient_id;

DROP TABLE IF EXISTS kre_report.hiv_hrs_all_syph_case_counts;
CREATE TABLE kre_report.hiv_hrs_all_syph_case_counts AS
SELECT patient_id, anchor_date,
count(case when condition = 'syphilis' and case_date >= anchor_date - interval '1 year' then 1 end) as syph_per_esp_1yr,
count(case when condition = 'syphilis' and  (case_date < anchor_date - interval '1 year') and (case_date >= anchor_date - interval '2 years') then 1 end) as syph_per_esp_1_to_2yr,
count(case when condition = 'syphilis' and case_date < anchor_date - interval '2 year' then 1 end) as syph_per_esp_gt_2yr
FROM kre_report.hiv_hrs_cases
GROUP BY patient_id, anchor_date;


-- Gather up all hep related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
-- ONLY PRINT RESULT STRING FOR TESTING. GROUP SO LOG RESULTS DON'T GET COUNTED TWICE
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events;
CREATE TABLE kre_report.hiv_hrs_all_hep_events AS 
SELECT T1.patient_id, T1.anchor_date, T2.date, T3.test_name, T4.name as hef_name--, result_string
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna', 'hepatitis_b_surface_antigen', 'hepatitis_b_viral_dna')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= T1.anchor_date
GROUP BY T1.patient_id, T1.anchor_date, T3.test_name, T2.date, T4.name --, result_string
;

-- Remove hef name so tests that don't have pos/neg don't get counted twice
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_nohef;
CREATE TABLE kre_report.hiv_hrs_all_hep_events_nohef AS 
SELECT patient_id, anchor_date, test_name, date
FROM kre_report.hiv_hrs_all_hep_events
GROUP BY patient_id, anchor_date, test_name, date;

DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_nohef_counts;
CREATE TABLE kre_report.hiv_hrs_all_hep_events_nohef_counts AS 
SELECT patient_id, anchor_date,
count(case when test_name = 'hepatitis_c_elisa' and date >= anchor_date - interval '1 year' then 1 end) as hcv_ab_1yr,
count(case when test_name = 'hepatitis_c_elisa' and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hcv_ab_1_to_2yr,
count(case when test_name = 'hepatitis_c_elisa' and date < anchor_date - interval '2 year' then 1 end) as hcv_ab_gt_2yr,
count(case when test_name = 'hepatitis_c_rna' and date >= anchor_date - interval '1 year' then 1 end) as hcv_rna_1yr,
count(case when test_name = 'hepatitis_c_rna' and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hcv_rna_1_to_2yr,
count(case when test_name = 'hepatitis_c_rna' and date < anchor_date - interval '2 year' then 1 end) as hcv_rna_gt_2yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and date >= anchor_date - interval '1 year' then 1 end) as hbv_sag_1yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hbv_sag_1_to_2yr,
count(case when test_name = 'hepatitis_b_surface_antigen' and date < anchor_date - interval '2 year' then 1 end) as hbv_sag_gt_2yr,
count(case when test_name = 'hepatitis_b_viral_dna' and date >= anchor_date - interval '1 year' then 1 end) as hbv_dna_1yr,
count(case when test_name = 'hepatitis_b_viral_dna' and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hbv_dna_1_to_2yr,
count(case when test_name = 'hepatitis_b_viral_dna' and date < anchor_date - interval '2 year' then 1 end) as hbv_dna_gt_2yr
FROM kre_report.hiv_hrs_all_hep_events_nohef
group by patient_id,anchor_date;


DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_events_hef_counts;
CREATE TABLE kre_report.hiv_hrs_all_hep_events_hef_counts as
SELECT patient_id, anchor_date,
max(case when (test_name = 'hepatitis_c_elisa' or test_name = 'hepatitis_c_rna') and hef_name ilike '%pos%' then 1 end) as hcv_ab_or_rna_pos_ev,
max(case when test_name = 'hepatitis_c_elisa' and hef_name ilike '%pos%' then date_part('year', age(anchor_date, date)) end) as hcv_ab_pos_first_yr,
max(case when test_name = 'hepatitis_c_rna' and hef_name ilike '%pos%' then date_part('year', age(anchor_date, date)) end) as hcv_rna_pos_first_yr,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' then 1 end) as hbv_sag_or_dna_pos_ev,
max(case when (test_name = 'hepatitis_b_surface_antigen' or test_name = 'hepatitis_b_viral_dna') and hef_name ilike '%pos%' then date_part('year', age(anchor_date, date)) end) as hbv_sag_or_dna_pos_first_yr
FROM kre_report.hiv_hrs_all_hep_events
GROUP BY patient_id, anchor_date;


DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hep_case_counts;
CREATE TABLE kre_report.hiv_hrs_all_hep_case_counts AS
SELECT patient_id, anchor_date,
max(case when condition = 'hepatitis_c:acute' then 1 end) as hcv_acute_per_esp,
max(case when condition = 'hepatitis_c:acute' then date_part('year', age(anchor_date, case_date)) end) as hcv_acute_per_esp_case_yrs,
max(case when condition = 'hepatitis_b:acute' then 1 end) as hbv_acute_per_esp,
max(case when condition = 'hepatitis_b:acute' then date_part('year', age(anchor_date, case_date)) end) as hbv_acute_per_esp_case_yrs
FROM kre_report.hiv_hrs_cases
GROUP BY patient_id, anchor_date;

-- HepB Diagnosis Codes
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hepb_diags;
CREATE TABLE kre_report.hiv_hrs_all_hepb_diags AS 
SELECT T3.patient_id, anchor_date, 1::int as hep_b_dx
FROM emr_encounter_dx_codes T1
JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
JOIN kre_report.hiv_hrs_index_patients T3 ON (T2.patient_id = T3.patient_id)
WHERE 
T2.date <= T3.anchor_date
AND dx_code_id ~'^(icd9:070.2|icd9:070.3|icd9:V02.61|icd10:B16.|icd10:B18.0|icd10:B18.1|icd10:B19.1|icd10:Z22.51)'
GROUP BY T3.patient_id, T3.anchor_date;



-- Gather up all HIV related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events;
CREATE TABLE kre_report.hiv_hrs_all_hiv_events AS 
SELECT T1.patient_id, T1.anchor_date, T2.date, T3.test_name, T4.name as hef_name, result_string, result_float
--, T2.id, T3.test_name, T4.name as hef_name, T2.date, result_string 
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
LEFT JOIN hef_event T4 ON ((T2.id = T4.object_id) AND (T1.patient_id = T4.patient_id))
WHERE T3.test_name ilike '%hiv%'
AND T3.test_name != 'hiv not a test'
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND T2.date <= T1.anchor_date
GROUP BY T1.patient_id, T1.anchor_date, T3.test_name, T2.date, T4.name, result_string, result_float
;


-- Remove hef name so tests that don't have pos/neg don't get counted twice
-- or not counted at all
DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events_nohef;
CREATE TABLE kre_report.hiv_hrs_all_hiv_events_nohef AS 
SELECT patient_id, anchor_date, test_name, date
FROM kre_report.hiv_hrs_all_hiv_events
GROUP BY patient_id, anchor_date, test_name, date;


DROP TABLE IF EXISTS kre_report.hiv_hrs_all_hiv_events_nohef_counts;
CREATE TABLE kre_report.hiv_hrs_all_hiv_events_nohef_counts AS 
SELECT patient_id, anchor_date,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and date >= anchor_date - interval '1 year' then 1 end) as hiv_elisa_ag_ab_1yr,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hiv_elisa_ag_ab_1_to_2yr,
count(case when test_name in ('hiv_elisa', 'hiv_ag_ab') and date < anchor_date - interval '2 year' then 1 end) as hiv_elisa_ag_ab_gt_2yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and date >= anchor_date - interval '1 year' then 1 end) as hiv_confirm_tests_1yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hiv_confirm_tests_1_to_2yr,
count(case when test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot', 'hiv_pcr', 'hiv_geenius') and date < anchor_date - interval '2 year' then 1 end) as hiv_confirm_tests_gt_2yr,
count(case when test_name = 'hiv_rna_viral' and date >= anchor_date - interval '1 year' then 1 end) as hiv_rna_viral_1yr,
count(case when test_name = 'hiv_rna_viral' and (date < anchor_date - interval '1 year') and (date >= anchor_date - interval '2 years') then 1 end) as hiv_rna_viral_1_to_2yr,
count(case when test_name = 'hiv_rna_viral' and date < anchor_date - interval '2 year' then 1 end) as hiv_rna_viral_gt_2yr
FROM kre_report.hiv_hrs_all_hiv_events_nohef
group by patient_id,anchor_date;

-- HIV VIRAL LOADS
DROP TABLE IF EXISTS kre_report.hiv_hrs_viral_loads;
CREATE TABLE kre_report.hiv_hrs_viral_loads AS
SELECT T1.patient_id, T1.anchor_date, min(date) max_viral_load_date, max_viral_load, date_part('year', age(T1.anchor_date, min(date))) as hiv_max_viral_load_yr
FROM 
(SELECT patient_id, anchor_date, max(result_float) as max_viral_load
        FROM kre_report.hiv_hrs_all_hiv_events
        WHERE test_name = 'hiv_rna_viral'
        AND result_float > 0
        GROUP BY patient_id, anchor_date) T1
INNER JOIN kre_report.hiv_hrs_all_hiv_events T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
WHERE T1.max_viral_load = T2.result_float
GROUP BY T1.patient_id, T1.anchor_date, max_viral_load;


-- HIV MEDS
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_meds;
CREATE TABLE kre_report.hiv_hrs_hiv_meds as
SELECT T1.patient_id, T1.anchor_date, T2.name, T3.name as hef_name, T2.date, T2.quantity, T2.quantity_float, T2.refills, T2.start_date, T2.end_date, 
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1) ELSE 1 END refills_mod,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
-- --Default to 30 for missing quantity values for Truvada/Descovy
-- CASE WHEN refills~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN ((refills::real +1) * 30)
     -- WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
	 -- WHEN refills!~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN 30
	 -- WHEN refills is null AND (quantity_float is null or quantity_float = 0) THEN 30
     -- WHEN refills is null then quantity_float end as total_prep_oral_quantity_per_rx,
--given every 8 weeks so each prescription for cabotegravir should count as 2 prescriptions
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * 2) 
     WHEN refills!~E'^\\d+$' or refills is null THEN 2
	 ELSE 2 end as num_monthly_prep_inj_rx,
CASE WHEN T3.name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') THEN 1 ELSE 0 END as rx_oral_prep,
CASE WHEN T3.name in ('rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END as rx_inj_prep,
CASE WHEN T3.name not in ('rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END other_hiv_rx_non_prep, --really not non-prep anymore due to change request but leaving the variable name
string_to_array(replace(split_part(T3.name, ':', 2), 'hiv_',''), '-') med_array,
CASE WHEN T3.name in ('rx:hiv_dolutegravir-lamivudine', 
                      'rx:hiv_dolutegravir-lamivudine:generic',
					  'rx:hiv_dolutegravir-rilpivirine',
                      'rx:hiv_dolutegravir-rilpivirine:generic',
					  'rx:hiv_cabotegravir-rilpivirine',
                      'rx:hiv_cabotegravir-rilpivirine:generic') THEN 1 END hiv_trmt_reg
FROM kre_report.hiv_hrs_index_patients T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
JOIN hef_event T3 on (T2.patient_id = T3.patient_id and T2.id = T3.object_id)
WHERE T3.name ilike 'rx:hiv%'
AND T2.date <= T1.anchor_date;

-- DON'T COUNT TDF/FTC and TAF/FTC as non HIV med if patient has a history of HepB
UPDATE kre_report.hiv_hrs_hiv_meds
SET other_hiv_rx_non_prep = 0
WHERE hef_name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                   'rx:hiv_tenofovir-emtricitabine', 
                   'rx:hiv_tenofovir_alafenamide-emtricitabine', 
				   'rx:hiv_tenofovir_alafenamide-emtricitabine:generic')
AND (  patient_id in (select patient_id from kre_report.hiv_hrs_all_hepb_diags where hep_b_dx = 1)
       OR patient_id in (select patient_id from kre_report.hiv_hrs_all_hep_events_hef_counts where hbv_sag_or_dna_pos_ev = 1)
	);


-- HIV CASE RELATED VARIABLES
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_case_values;
CREATE TABLE kre_report.hiv_hrs_hiv_case_values AS
SELECT T1.patient_id, T1.anchor_date, --T2.test_name, T3.case_date
MAX(CASE WHEN T3.case_date is not null THEN 1 --positive
     WHEN T2.test_name is not null and T3.case_date is null THEN 0 --negative
	 WHEN T2.test_name is null and T3.case_date is null THEN 2 --untested
	 END) as hiv_status_per_esp,
date_part('year', age(T1.anchor_date, min(case_date))) hiv_per_esp_case_yrs,
MAX(CASE WHEN T3.case_date is not null and hiv_new_diag is null THEN 0
         WHEN T3.case_date is null and hiv_new_diag is null THEN null
		 ELSE hiv_new_diag END) as hiv_new_diag_final,
MAX(CASE WHEN T3.case_date is null and T2.test_name = 'hiv_rna_viral' THEN 1 
         WHEN T3.case_date is null and (T2.test_name != 'hiv_rna_viral' or T2.test_name is null) THEN 0
	ELSE 2 END) as hiv_neg_w_rna_test_ev,
MAX(CASE WHEN T3.case_date is null and other_hiv_rx_non_prep = 1 THEN 1 
         WHEN T3.case_date is null and (other_hiv_rx_non_prep = 0 or other_hiv_rx_non_prep is null) THEN 0
	ELSE 2 END) as hiv_neg_w_hiv_med_ev
FROM kre_report.hiv_hrs_index_patients T1
LEFT JOIN kre_report.hiv_hrs_all_hiv_events T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
LEFT JOIN kre_report.hiv_hrs_cases T3 ON (T1.patient_id = T3.patient_id and T1.anchor_date = T3.anchor_date and T3.condition = 'hiv')
LEFT JOIN kre_report.hiv_hrs_hiv_meds T4 ON (T1.patient_id = T4.patient_id and T1.anchor_date = T4.anchor_date)
GROUP BY T1.patient_id, T1.anchor_date;

-- HIV MEDS Break down the combo meds in to distinct individual meds.
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_combo_distinct;
CREATE TABLE kre_report.hiv_hrs_hiv_rx_combo_distinct AS
select distinct unnest(med_array) as distinct_med, patient_id, anchor_date,
date_part('year', age(anchor_date, date)) med_age_in_yrs, hiv_trmt_reg
from kre_report.hiv_hrs_hiv_meds
group by patient_id, anchor_date, med_array, med_age_in_yrs, hiv_trmt_reg;

DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_3_diff;
CREATE TABLE kre_report.hiv_hrs_hiv_rx_3_diff AS 
SELECT T1.patient_id, T1.anchor_date,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1) and med_age_in_yrs =0 then 1 else 0 end hiv_trmt_regimen_1yr,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1)and med_age_in_yrs in (1,2) then 1 else 0 end hiv_trmt_regimen_1_to2yr,
CASE WHEN (count(distinct(distinct_med)) >= 3 or hiv_trmt_reg = 1) and med_age_in_yrs >2 then 1 else 0 end hiv_trmt_regimen_gt_2yr
FROM kre_report.hiv_hrs_hiv_rx_combo_distinct T1
-- exclude this prep only combo
WHERE distinct_med != 'cabotegravir_er_600'
GROUP BY T1.patient_id, T1.anchor_date, med_age_in_yrs, hiv_trmt_reg;

DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_rx_3_diff_count;
CREATE TABLE kre_report.hiv_hrs_hiv_rx_3_diff_count AS
SELECT T1.patient_id, T1.anchor_date,
MAX(hiv_trmt_regimen_1yr) hiv_trmt_regimen_1yr, 
MAX(hiv_trmt_regimen_1_to2yr) hiv_trmt_regimen_1_to2yr,
MAX(hiv_trmt_regimen_gt_2yr) hiv_trmt_regimen_gt_2yr
FROM kre_report.hiv_hrs_hiv_rx_3_diff T1
GROUP BY T1.patient_id, T1.anchor_date;


-- ORAL PEP & PREP (Truvada/Descovy)

-- IDENTIFY PEP RX
DROP TABLE IF EXISTS kre_report.hiv_hrs_pep_meds;
CREATE TABLE kre_report.hiv_hrs_pep_meds as
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= T2.anchor_date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T2.anchor_date = T3.anchor_date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= T2.anchor_date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T2.anchor_date = T3.anchor_date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name in ('rx:hiv_lopinavir-ritonavir', 'rx:hiv_lopinavir-ritonavir:generic')
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE name in ('bictegravir-emtricitabine-tenofovir_alafenamide', 'bictegravir-emtricitabine-tenofovir_alafenamide:generic')
AND T1.date <= T2.anchor_date
GROUP BY T1.patient_id, pep_date, anchor_date;


DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_pep_counts;
CREATE TABLE kre_report.hiv_hrs_hiv_pep_counts AS
SELECT count(distinct(pep_date)) as pep_rx_dates_num, patient_id, anchor_date
FROM kre_report.hiv_hrs_pep_meds
GROUP BY patient_id, anchor_date;


--DELETE PEP MEDS FROM ORAL PREP
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_meds_no_pep;
CREATE TABLE kre_report.hiv_hrs_hiv_meds_no_pep AS
SELECT * 
FROM kre_report.hiv_hrs_hiv_meds
WHERE rx_oral_prep = 1
EXCEPT
SELECT T1.*
FROM kre_report.hiv_hrs_hiv_meds T1
INNER JOIN kre_report.hiv_hrs_pep_meds T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
WHERE T1.date = T2.pep_date
AND rx_oral_prep = 1;


DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_all;
CREATE TABLE kre_report.hiv_hrs_hiv_oral_prep_all as 
SELECT DISTINCT T1.patient_id, T1.anchor_date, date, refills_mod, total_quantity_per_rx, 
date + total_quantity_per_rx::int as derived_end_date, 
(total_quantity_per_rx/30) as num_monthly_rx,
date_part('year', age(T1.anchor_date, date)) rx_age_in_yrs,
(case when date >= T1.anchor_date - interval '1 year' then '0-1'
           when ((date < T1.anchor_date - interval '1 year') and (date >= T1.anchor_date - interval '2 years')) then '1-2'
           when date < T1.anchor_date - interval '2 year' then '2+' end) as med_date_range
FROM kre_report.hiv_hrs_hiv_meds_no_pep T1
LEFT JOIN kre_report.hiv_hrs_cases T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date and T2.condition = 'hiv')
WHERE rx_oral_prep = 1
--only inlcude meds before HIV case
AND (T1.date < T2.case_date or T2.case_date is null)
GROUP BY T1.patient_id, T1.anchor_date, date, refills_mod, total_quantity_per_rx;

DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_compute;
CREATE TABLE kre_report.hiv_hrs_hiv_oral_prep_compute as 
SELECT patient_id, anchor_date, med_date_range,
max(derived_end_date) - min(date) date_diff_for_rx,
sum(num_monthly_rx) num_monthly_rx
FROM kre_report.hiv_hrs_hiv_oral_prep_all
GROUP BY patient_id, anchor_date, med_date_range;

-- Must have 2 rx and be greater than 2 months apart
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_oral_prep_eval;
CREATE TABLE kre_report.hiv_hrs_hiv_oral_prep_eval as 
SELECT * 
FROM kre_report.hiv_hrs_hiv_oral_prep_compute
WHERE date_diff_for_rx >= 60 and num_monthly_rx >=2;


-- INJ PREP
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_inj_prep_compute;
CREATE TABLE kre_report.hiv_hrs_hiv_inj_prep_compute as 
SELECT T1.patient_id, T1.anchor_date, date, num_monthly_prep_inj_rx, 
(case when date >= T1.anchor_date - interval '1 year' then '0-1'
           when ((date < T1.anchor_date - interval '1 year') and (date >= T1.anchor_date - interval '2 years')) then '1-2'
           when date < T1.anchor_date - interval '2 year' then '2+' end) as med_date_range
FROM kre_report.hiv_hrs_hiv_meds T1
LEFT JOIN kre_report.hiv_hrs_cases T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date and T2.condition = 'hiv')
WHERE rx_inj_prep = 1
--only inlcude meds before HIV case
AND (T1.date < T2.case_date or T2.case_date is null)
GROUP BY T1.patient_id, T1.anchor_date, date, num_monthly_prep_inj_rx;

DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_inf_prep_eval;
CREATE TABLE kre_report.hiv_hrs_hiv_inf_prep_eval as 
SELECT patient_id, anchor_date, sum(num_monthly_prep_inj_rx) as num_monthly_rx, med_date_range
FROM kre_report.hiv_hrs_hiv_inj_prep_compute
GROUP BY patient_id, anchor_date, med_date_range;

-- ORAL AND INJ PREP TOGETHER
DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_union;
CREATE TABLE kre_report.hiv_hrs_prep_union AS
SELECT patient_id, anchor_date, med_date_range, num_monthly_rx FROM kre_report.hiv_hrs_hiv_oral_prep_eval
UNION
SELECT patient_id, anchor_date, med_date_range, num_monthly_rx FROM kre_report.hiv_hrs_hiv_inf_prep_eval;

DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_union_counts;
CREATE TABLE kre_report.hiv_hrs_prep_union_counts AS
SELECT patient_id, anchor_date, med_date_range, sum(num_monthly_rx) num_monthly_rx
FROM kre_report.hiv_hrs_prep_union
GROUP BY patient_id, anchor_date, med_date_range;

--FINAL VALUES
DROP TABLE IF EXISTS kre_report.hiv_hrs_prep_final;
CREATE TABLE kre_report.hiv_hrs_prep_final AS
SELECT patient_id, anchor_date,
MAX(CASE WHEN med_date_range = '0-1' THEN round(num_monthly_rx) END) as prep_rx_num_1yr,
MAX(CASE WHEN med_date_range = '1-2' THEN round(num_monthly_rx) END) as prep_rx_num_1_to_2yr,
MAX(CASE WHEN med_date_range = '2+'  THEN round(num_monthly_rx) END) as prep_rx_num_gt_2yr
FROM kre_report.hiv_hrs_prep_union_counts
GROUP BY patient_id, anchor_date;

-- HIV DX CODES
-- GROUP SO 2 DX ON SAME DATE DON'T COUNT AS 2 
DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_dx_codes;
CREATE TABLE kre_report.hiv_hrs_hiv_dx_codes AS 
SELECT T1.patient_id, T1.anchor_date, T2.date as dx_date, 1::int as hiv_dx 
FROM kre_report.hiv_hrs_index_patients T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date <= T1.anchor_date
AND (dx_code_id ~ '^(icd9:042|icd9:V08|icd9:079.53|icd9:795.71|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)')
GROUP BY T1.patient_id, T1.anchor_date, dx_date, hiv_dx;

DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_dx_code_counts;
CREATE TABLE kre_report.hiv_hrs_hiv_dx_code_counts AS 
SELECT patient_id, anchor_date,
count(case when dx_date >= anchor_date - interval '1 year' then 1 end) as hiv_dx_1_yr,
count(case when (dx_date < anchor_date - interval '1 year') and (dx_date >= anchor_date - interval '2 years') then 1 end) as hiv_dx_1_to_2yr,
count(case when dx_date < anchor_date - interval '2 year' then 1 end) as hiv_dx_gt_2yr
FROM kre_report.hiv_hrs_hiv_dx_codes
GROUP BY patient_id, anchor_date;

-- DX CODES OTHER
DROP TABLE IF EXISTS kre_report.hiv_hrs_dx_codes;
CREATE TABLE kre_report.hiv_hrs_dx_codes AS 
SELECT T1.patient_id, T1.anchor_date, T2.date as dx_date, T3.dx_code_id
FROM kre_report.hiv_hrs_index_patients T1
JOIN emr_encounter T2 on (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 on (T3.encounter_id = T2.id)
WHERE T2.date <= T1.anchor_date
AND (
 dx_code_id ~ '^(icd9:796.7|icd10:R85.61|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:A59.|icd10:A63.|icd10:Z72.5|icd10:F50.0|icd10:T74.5|icd10:T74.2|icd10:T74.5|icd10:T18.5|icd9:305.0|icd9:303.9|icd10:F10.1|icd10:F10.2|icd9:304.0|icd10:F11.|icd9:305.5|icd9:304.1|icd9:305.4|icd10:F13.|icd9:304.2|icd9:305.6|icd10:F14.|icd9:304.4|icd9:305.7|icd10:F15.|icd9:304.9|icd10:F19)'
OR 
 dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd9:569.44', 'icd10:K62.82', 'icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:091.1', 'icd10:A51.1', 'icd9:098.7','icd10:A54.6',
                'icd9:098.6','icd10:A54.5', 'icd9:099.52','icd10:A56.3', 'icd9:099.51','icd10:A56.4', 'icd9:099.1','icd10:A55', 'icd9:099.0', 'icd10:A57', 'icd9:099.2', 'icd10:A58', 'icd10:N34.1',
				'icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd9:078.11', 'icd10:A63.0', 'icd9:569.41', 'icd10:K62.6', 'icd10:B04', 'icd10:A64', 'icd9:099.9',
				'icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0', 'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9', 'icd9:V01.6', 
				'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V69.2', 'icd9:V65.44','icd10:Z71.7', 'icd9:307.1', 'icd9:307.51','icd10:F50.2', 'icd9:307.50','icd10:F50.9', 'icd9:302.0', 'icd9:302.6',
				'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:F64.1',
				'icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5', 'icd10:Z62.810', 'icd9:937', 'icd9:V74.5', 'icd10:Z11.3', 'icd9:V76.41', 'icd10:Z12.12', 'icd10:W46.0XXA', 'icd10:W46.1XXA', 'icd10:Z29.9', 'icd9:V07.8', 'icd9:V07.9', 'icd9:E920.5')
);

DROP TABLE IF EXISTS kre_report.hiv_hrs_dx_code_years;
CREATE TABLE kre_report.hiv_hrs_dx_code_years AS 
SELECT 
	patient_id,
	anchor_date,
	MAX(case when dx_code_id ~ '^(icd9:796.7|icd10:R85.61)' 
	         OR dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd9:569.44', 'icd10:K62.82') then date_part('year', age(anchor_date, dx_date)) END) as dx_abn_anal_cyt_yrs,
    MAX(case when dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7)'
	         OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then date_part('year', age(anchor_date, dx_date)) END) as dx_syph_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:091.1', 'icd10:A51.1') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_anal_syph_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:098.7','icd10:A54.6') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_gon_inf_rectum_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:098.6','icd10:A54.5') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_gon_pharyn_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.52','icd10:A56.3') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_chlam_inf_rectum_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.51','icd10:A56.4') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_chlam_pharyn_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.1','icd10:A55') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_lymph_ven_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.0','icd10:A57') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_chancroid_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:099.2', 'icd10:A58') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_granuloma_ingu_yrs,
	MAX(CASE WHEN (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_nongon_ureth_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_hsv_w_compl_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_genital_herpes_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:078.11', 'icd10:A63.0') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_anogenital_warts_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:569.41', 'icd10:K62.6') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_anorectal_ulcer_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd10:A59.)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_trich_yrs,
	MAX(CASE WHEN dx_code_id in ('icd10:B04') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_mpox_yrs,
	MAX(CASE WHEN (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.9')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_unspec_std_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0', 'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_pid_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_cont_w_exp_to_vd_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_high_risk_sex_behav_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_hiv_counsel_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_anorexia_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:307.51','icd10:F50.2') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_bulimia_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:307.50','icd10:F50.9') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_eat_disorder_nos_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_gend_iden_dis_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:F64.1') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_transsexualism_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_couns_or_prb_child_sex_abuse_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:T74.5|icd10:T74.2)' or dx_code_id in ('icd10:Z62.810')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_adult_child_sex_exp_or_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd10:T74.5)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_adult_or_child_phys_abuse_yrs,
	MAX(CASE WHEN (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) THEN date_part('year', age(anchor_date, dx_date)) END) as dx_foreign_body_anus_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:305.0|icd9:303.9|icd10:F10.1|icd10:F10.2)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_alcohol_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_opioid_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_sed_hypn_anxio_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_cocaine_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_amphet_stim_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id ~ '^(icd9:304.9|icd10:F19)' THEN date_part('year', age(anchor_date, dx_date)) END) as dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V74.5', 'icd10:Z11.3') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_sti_screen_yrs,
	MAX(CASE WHEN dx_code_id in ('icd9:V76.41', 'icd10:Z12.12') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_malig_neo_rec_screen_yrs,
	MAX(CASE WHEN dx_code_id in ('icd10:W46.0XXA', 'icd10:W46.1XXA', 'icd9:E920.5') THEN date_part('year', age(anchor_date, dx_date)) END) as dx_needle_stick
FROM kre_report.hiv_hrs_dx_codes	
GROUP BY patient_id, anchor_date;


--
-- BICILLIN MEDS
--

DROP TABLE IF EXISTS kre_report.hiv_hrs_bicillin_meds;
CREATE TABLE kre_report.hiv_hrs_bicillin_meds AS
SELECT count(distinct(T1.id)) as rx_bicillin_num_rx, T2.patient_id, T2.anchor_date, 1 as rx_bicillin_indicator, T1.name, T1.date, T1.dose
FROM emr_prescription T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_risk_bicillin_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T1.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T1.directions is null and T1.dose is null)
)
AND T1.date <= T2.anchor_date
GROUP BY T2.patient_id, T2.anchor_date, T1.name, T1.date, T1.dose;


-- At CHA need to have 2 rx on the same day (to handle how CHA writes the prescriptions) for 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP'
DROP TABLE IF EXISTS kre_report.hiv_hrs_bicillin_meds_updated;
CREATE TABLE kre_report.hiv_hrs_bicillin_meds_updated AS
SELECT patient_id, anchor_date, rx_bicillin_indicator as rx_bicillin_ever FROM kre_report.hiv_hrs_bicillin_meds
EXCEPT
SELECT patient_id, anchor_date, rx_bicillin_indicator as rx_bicillin_ever FROM kre_report.hiv_hrs_bicillin_meds
    WHERE name in('PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSY') AND (dose in ('1.2') OR dose is null)
	AND rx_bicillin_num_rx < 2;


--AZITH

DROP TABLE IF EXISTS kre_report.hiv_hrs_azith_meds;
CREATE TABLE kre_report.hiv_hrs_azith_meds AS
SELECT T2.patient_id, T2.anchor_date, 1::int as rx_azith_ever
FROM emr_prescription T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--BMC
name = 'AZITHROMYCIN 1 GRAM ORAL PACKET'  --ATRIUS TOO
OR (name = 'AZITHROMYCIN 250 MG TABLET' and dose = '1000 mg')
OR (name = 'AZITHROMYCIN 250 MG TABLET' and dose = '250 mg' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TABLET' and quantity_float = 4) --ATRIUS TOO
OR (name = 'AZITHROMYCIN 500 MG TABLET' and dose = '1000 mg') --ATRIUS TOO
OR (name = 'AZITHROMYCIN 500 MG TABLET' and dose = '500 mg' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG TABLET' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TABLET' and dose = '1000 mg')
OR (name = 'ZITHROMAX Z-PAK 250 MG TABLET' and dose = '1000 mg')
OR (name = 'ZITHROMAX Z-PAK 250 MG TABLET' and dose = '250 mg' and quantity_float = 4)
OR (name = 'ZITHROMAX Z-PAK 250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
--ATRIUS
OR (name = 'AZITHROMYCIN 250 MG CAP' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG CAPSULE' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TAB' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 500 MG TAB' and quantity_float = 2)
OR name = 'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)'
OR (name = 'AZITHROMYCIN CHLAMYDIA RX (250 MG)' and quantity_float = 4)
OR (name = 'AZITHROMYCIN CHLAMYDIA RX (500 MG)' and quantity_float = 2)
OR name = 'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)'
OR (name = 'ZITHROMAX 250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG TAB (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX Z-PAK 250 MG CAP (AZITHROMYCIN)' and quantity_float = 4)
-- CHA
OR name = 'AZITHROMYCIN 1 G PO PACK' --FENWAY TOO
OR (name = 'AZITHROMYCIN 250 MG OR CAPS' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG PO TABS' and (dose in ('1000', '1000 mg') or quantity_float = 4)) --FENWAY TOO
OR (name = 'AZITHROMYCIN 500 MG PO TABS' and (dose in ('1000', '1000 mg') or quantity_float = 2)) --FENWAY TOO
OR name = 'ZITHROMAX 1 G PO PACK'
OR (name = 'ZITHROMAX 250 MG OR CAPS' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG PO TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX Z-PAK 250 MG OR CAPS' and quantity_float = 4)
--FENWAY
OR name = 'AZITHROMYCIN 1G'
OR name = 'AZITHROMYCIN 1 GM ORAL PACK'
OR name = 'AZITHROMYCIN 1 GM ORAL PACKET'
OR name = 'AZITHROMYCIN 1 GM PACK'
OR name = 'AZITHROMYCIN 1 GRAM'
OR name = 'azithromycin 1 gram packet'
OR (name = 'AZITHROMYCIN 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG ORAL TABS' and quantity_float = 4)
OR (name = 'azithromycin 250 mg tablet' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG TABS' and quantity_float = 4)
OR (name = '(AZITHROMYCIN) 500MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG ORAL TABLET' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG ORAL TABS' and quantity_float = 2)
OR (name = '(AZITHROMYCIN) 500MG TAB' and quantity_float = 2)
OR (name = 'azithromycin 500 mg tablet' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG TABLETS' and quantity_float = 2) 
OR (name = 'AZITHROMYCIN 500 MG TABS' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG  TABS' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500 MG TABS (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'AZITHROMYCIN 500MG TABS (AZITHROMYCIN)' and quantity_float = 2)
OR name = 'Med:  Zithromax 1gr slurry PO'
OR name = 'ZITHROMAX 1 GM ORAL PACKET'
OR name = 'ZITHROMAX 1 GM PACK'
OR (name = 'ZITHROMAX 250MG' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG ORAL TABS' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG TAB' and quantity_float = 4)
OR (name = 'ZITHROMAX  250 MG TAB (AZITHROMYCIN)' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG  TABS' and quantity_float = 4)
OR (name = 'ZITHROMAX 500 MG' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG ORAL TABLET' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MGS' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TAB' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX 500 MG  TABS' and quantity_float = 2)
OR (name = 'ZITHROMAX CAP 250MG' and quantity_float = 2)
OR (name = 'Zithromax Dose' and dose = '1000')
OR name = 'ZITHROMAX POW 1GM PAK'
OR (name = 'ZITHROMAX TAB 250MG' and quantity_float = 4)
OR (name = 'ZITHROMAX TAB 5000MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TAB 500 MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TAB 500MG (AZITHROMYCIN)' and quantity_float = 2)
OR (name = 'ZITHROMAX TABS 250 MG' and quantity_float = 4)
OR (name = 'ZITHROMAX Z-PAK 250 MG ORAL TABLET' and quantity_float = 4)
OR (name = 'AZITHROMYCIN 250 MG.' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG CAP' and quantity_float = 4)
OR (name = 'ZITHROMAX 250 MG TABS' and quantity_float = 4)

)
AND T1.date <= T2.anchor_date
GROUP BY T2.patient_id, T2.anchor_date;


--CEFT
DROP TABLE IF EXISTS kre_report.hiv_hrs_ceft_meds;
CREATE TABLE kre_report.hiv_hrs_ceft_meds AS
SELECT T2.patient_id, T2.anchor_date, 1::int as rx_ceft_ever
FROM emr_prescription T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--ATRIUS
(name = 'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION' and (dose in ('.125 g', '125 g', '125 mg', '150 mg', '250 mg', '.25 g', '500 mg', '.5 g') or quantity_float in (0.5, 1, 2, 125, 250, 500) or (dose is null and quantity_float is null) ) )-- BMC TOO
OR (name = 'CEFTRIAXONE 500 MG SOLUTION FOR INJECTION' and (dose in ('1 g', '250 mg', '.25 g', '500 mg', '.5 g') or quantity_float in (0, 1, 250, 500) or (dose is null and quantity_float is null) ) ) --BMC TOO
OR name = 'CEFTRIAXONE 500 MG WITH LIDOCAINE 1 % IM KIT'
OR (name = 'CEFTRIAXONE-LIDOCAINE 500 MG IM KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)' and (quantity_float in (1) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)' and (quantity_float in (0, 0.5, 1, 125, 250, 500) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 500 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)' and (quantity_float in (1, 250, 500)  or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN IM CONVENIENCE 500 MG-1 % KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)' and (quantity_float in (1, 250, 500) or (dose is null and quantity_float is null) ) )
--BMC
OR (name = 'CEFTRIAXONE 2 GRAM SOLUTION FOR INJECTION' and dose in ('250 g') )
OR (name = 'CEFTRIAXONE 1 G IN LIDOCAINE IM INJECTION' and dose in ('500 mg') )
OR (name = 'CEFTRIAXONE 1 GRAM SOLUTION FOR INJECTION' and dose in ('125 mg', '250 mg', '500 mg', '.5 g') )
OR (name = 'CEFTRIAXONE 1 GRAM SOLUTION IM INJECTION' and dose in ('125 mg', '250 mg', '500 mg', '.5 g') )
OR (name = 'CEFTRIAXONE 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)' and (dose in ('1 g', '.5 g', '250 mg', '.25 g', '500 mg') or quantity_float in (2)) )
OR (name = 'CEFTRIAXONE 250 MG IN LIDOCAINE IM INJECTION' and dose in ('250 mg') )
OR (name = 'CEFTRIAXONE 500 MG/5 ML IJ (RECONSTITUTED VIAL)' and dose in ('1 g', '500 mg', '.5 g') )
OR name = 'CEFTRIAXONE 500 MG IM INJECTION'
OR (name = 'CEFTRIAXONE 500 MG IN LIDOCAINE IM INJECTION' and dose in ('250 mg', '500 mg'))
OR name = 'CEFTRIAXONE 500 MG IN SWFI IM INJECTION'
OR (name = 'CEFTRIAXONE IM INJECTION <=250 MG' and dose = '250 mg')
OR (name = 'CEFTRIAXONE IM SYR >= 250 MG' and (dose = '250 mg' or quantity_float = '250'))
OR (name = 'CEFTRIAXONE IVPB IN 50 ML' and dose in ('250 mg', '.25 g'))
OR name = 'CEFTRIAXONE SODIUM 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)'
OR name = 'CEFTRIAXONE SODIUM 500 MG/5 ML IJ (RECONSTITUTED VIAL)'
OR name = 'CEFTRIAXONE 500 MG SOLUTION IM INJECTION'
--CHA
OR (name = 'CEFTRIAXONE IVPB' and dose in ('250', '500'))
OR (name = 'CEFTRIAXONE SODIUM 250 MG IJ SOLR' and (dose in ('250', '500', '250 mg', '500 mg', 'None') or quantity_float in (0, 0.5, 1, 2, 10, 125, 250, 500) or (dose is null and quantity_float is null) ) ) --FENWAY TOO
OR (name = 'CEFTRIAXONE SODIUM 500 MG IJ SOLR' and (dose in ('1', '250', '.5', '500', '250 mg', '500 mg') or quantity_float in (0.5, 1, 500) or (dose is null and quantity_float is null) ) )  --FENWAY TOO
OR (name = 'CEFTRIAXONE SODIUM-LIDOCAINE 500 MG IM KIT' and (quantity_float in (1, 500) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 250 MG IJ SOLR' and (quantity_float in (0, 1, 2, 10, 125, 250) or (dose is null and quantity_float is null) ) )
OR (name = 'ROCEPHIN 500 MG IJ SOLR' and (quantity_float in (1, 500) or (dose is null and quantity_float is null) ) )
OR name = 'ROCEPHIN IM CONVENIENCE 500-1 MG-% IM KIT'
OR (name = 'CEFTRIAXONE SODIUM 1 G IJ SOLR' and dose in ('.5', '250', '500') )
--FENWAY
OR name = 'CEFTRIAXONE 250MG'
OR name = 'ceftriaxone 250 mg recon soln'
OR name = 'ceftriaxone 500 mg recon soln'
OR name = 'CEFTRIAXONE EA 250MG'
OR name = 'CEFTRIAXONE EA 500MG'
OR name = 'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)'
OR name = 'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED'
OR name = 'CEFTRIAXONE SODIUM 250 MG INJ SOLR'
OR name = 'CEFTRIAXONE SODIUM 250 MG SOLR'
OR name = 'CEFTRIAXONE SODIUM 500 MG INJECTION SOLUTION RECONSTITUTED'
OR (name = 'CEFTRIAXONE SODIUM POWD' and quantity_float in (125,250,500) )
OR (name = 'CEFTRIAXONE SODIUM POWDER' and quantity_float in (125,250,500) )
OR (name = 'CEFTRIAXONE SODIUM PWDR' and quantity_float in (125,250,500) )
OR name = 'Med:  Rocephin  250mg IM'
OR name = 'Rocephin 250 mg'
OR name = 'ROCEPHIN 250 MG'
OR name = 'ROCEPHIN 250 MG INJ RECON SOL'
OR name = 'ROCEPHIN 250 MG INJ SOLR'
OR name = 'ROCEPHIN 500 MG INJECTION SOLUTION RECONSTITUTED'
OR name = 'ROCEPHIN 500 MG INJ SOLR'
OR (name = 'Rocephin (Ceftriaxone) Injection' and quantity_float in (125,250,500) )
OR name = 'ROCEPHIN INJ 250MG'
)
AND T1.date <= T2.anchor_date
GROUP BY T2.patient_id, T2.anchor_date;

--DOXY
DROP TABLE IF EXISTS kre_report.hiv_hrs_doxy_meds;
CREATE TABLE kre_report.hiv_hrs_doxy_meds AS
SELECT T2.patient_id, T2.anchor_date, 1::int as rx_doxy_ever
FROM emr_prescription T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE 
(
--ATRIUS
(name = 'ADOXA 100 MG TAB (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'ADOXA PAK 100 MG TAB (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'DORYX 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DORYX 100 MG TAB (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXY-CAPS 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE CHLAMYDIA RX (100 MG CAP)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAP' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAP, DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPSULE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPSULE,DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TAB' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TAB, DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABLET' and quantity_float >= 28) --BMC & FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABLET,DELAYED RELEASE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAPSULE' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG TABLET' and quantity_float >= 28) --BMC TOO
OR (name = 'DOXY-LEMMON 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'MONODOX 100 MG CAP (DOXYCYCLINE MONOHYDRATE)' and quantity_float >= 28)
OR (name = 'VIBRAMYCIN 100 MG CAP (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'VIBRA-TABS 100 MG TAB (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
--BMC
OR (name = 'VIBRAMYCIN 100 MG CAPSULE' and quantity_float >= 28)
--CHA
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO CAPS' and quantity_float >= 28) --FENWAY TOO
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO CPEP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO TABS' and quantity_float >= 28) --FENWAY TOO 
OR (name = 'DOXYCYCLINE HYCLATE 100 MG PO TBEC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG PO CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG PO TABS' and quantity_float >= 28) --FENWAY TOO
OR (name = 'MONODOX 100 MG PO CAPS' and quantity_float >= 28)
OR (name = 'VIBRAMYCIN 100 MG PO CAPS' and quantity_float >= 28)
--FENWAY
OR (name = 'ADOXA 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'ADOXA 100 MG  TABS' and quantity_float >= 28)
OR (name = 'ADOXA PAK 1/100 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DORYX 100 MG TBEC' and quantity_float >= 28)
OR (name = 'DORYX 100 MG  TBEC' and quantity_float >= 28)
OR (name = 'DORYX CAP 100MG EC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100 MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE 100MG CAP 500CT 340B' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE  100 MG CAPS (DOXYCYCLINE HYCLATE)' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYC 100MG CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYC 100MG TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG  CAPS' and quantity_float >= 28)
OR (name = 'doxycycline hyclate 100 mg capsule' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100MG CAPSULES' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG CPEP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABLET DELAYED RELEASE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TBEC' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG SOLR' and quantity_float >= 28)
OR (name = 'doxycycline hyclate 100 mg tablet' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100MG TABLETS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE 100 MG  TABS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE HYCLATE CAPS 100 MG' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG CAP' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG TAB' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONO 100 MG TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG CAPS' and quantity_float >= 28)
OR (name = 'doxycycline monohydrate 100 mg capsule' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL CAPS' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL CAPSULE' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL TABLET' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG ORAL TABS' and quantity_float >= 28)
OR (name = 'doxycycline monohydrate 100 mg tablet' and quantity_float >= 28)
OR (name = 'DOXYCYCLINE MONOHYDRATE 100 MG  TABS' and quantity_float >= 28)
)
AND T1.date <= T2.anchor_date
GROUP BY T2.patient_id, T2.anchor_date;

DROP TABLE IF EXISTS kre_report.hiv_hrs_other_meds;
CREATE TABLE kre_report.hiv_hrs_other_meds AS
SELECT T2.patient_id, anchor_date, 
MAX(CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end) rx_methadone_ever,
MAX(CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike '%buprenorphine%' 
	or T1.name ilike '%bunavail%'
	or T1.name ilike '%cassipa%'
	or T1.name ilike '%zubsolv%' 
	or T1.name ilike '%sublocade%' 
	or T1.name ilike '%brixadi%' then 1 end) rx_buprenorphine_ever,
MAX(CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end) rx_viagara_cilais_or_levitra_ever, 
MAX(CASE WHEN
    T1.name ilike '%naltrexone%'
	or T1.name ilike 'vivitrol%'
	or T1.name ilike 'revia%' then 1 end) rx_naltrexone_ever
FROM emr_prescription T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%' 
	or name ilike '%suboxone%'  
	or name ilike '%buprenorphine%'
	or name ilike '%bunavail%' 
	or name ilike '%cassipa%' 
	or name ilike '%zubsolv%' 
	or name ilike '%sublocade%'
	or name ilike '%brixadi%'
	or name ilike '%tadalafil%'  
	or name ilike '%cilais%'  
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%' 
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%'  
	or name ilike '%viagra%'
	or name ilike '%naltrexone%'
	or name ilike 'vivitrol%'
    or name ilike 'revia%'
	) 
AND name not ilike '%revatio%'
AND name not ilike '%antihypertensive%'
AND name not ilike '%hypertension%'
AND name not ilike '%adcirca%'
AND name not ilike '%alyq%'
AND name not ilike '%methylnaltrexone%'
AND name not ilike '%contrave%'
AND name not ilike '%morphine%'
--FENWAY ISSUES
AND name not ilike '%Methadone screen, urine%'
AND name not ilike '%Age of onset of Methadone use%'
AND name not ilike '%Rationale for suboxone use%'
AND T1.date <= T2.anchor_date
GROUP BY T2.patient_id, T2.anchor_date;

DROP TABLE IF EXISTS kre_report.hiv_hrs_mpox_vax;
CREATE TABLE kre_report.hiv_hrs_mpox_vax AS
SELECT T1.patient_id, T1.anchor_date,
MAX(CASE WHEN upper(T2.name) ~ ('MPOX|MONKEYPOX|JYNNEOS') THEN 1 END) as vax_mpox_ever
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN emr_immunization T2 ON (T1.patient_id = T2.patient_id)
WHERE upper(T2.name) ~ ('MPOX|MONKEYPOX|JYNNEOS')
AND T2.date <= T1.anchor_date
GROUP BY T1.patient_id, T1.anchor_date;

--HPV VAX FOR MALE >=27
DROP TABLE IF EXISTS kre_report.hiv_hrs_hpv_vax;
CREATE TABLE kre_report.hiv_hrs_hpv_vax AS
SELECT T1.patient_id, T1.anchor_date,
MAX(CASE WHEN upper(T2.name) ~ ('HPV') THEN 1 END) as vax_hpv_male_ever
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN emr_immunization T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_patient T3 ON (T1.patient_id = T3.id)
WHERE upper(T2.name) ~ ('HPV')
AND upper(T3.gender) in ('M', 'MALE')
AND date_part('year', age(T2.date, date_of_birth)) >= 27
AND T2.date <= T1.anchor_date
GROUP BY T1.patient_id, T1.anchor_date;

  
--IDENTIFY PATS WITHOUT ANY NON-DEMOG VARIABLES
DROP TABLE IF EXISTS kre_report.hiv_hrs_pats_to_drop;
CREATE TABLE kre_report.hiv_hrs_pats_to_drop AS
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
LEFT JOIN kre_report.hiv_hrs_all_chlam_gon_labs_counts T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_syph_counts T3 ON (T1.patient_id = T3.patient_id and T1.anchor_date = T3.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_syph_case_counts T4 ON (T1.patient_id = T4.patient_id and T1.anchor_date = T4.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_events_nohef_counts T5 ON (T1.patient_id = T5.patient_id and T1.anchor_date = T5.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_events_hef_counts T6 ON (T1.patient_id = T6.patient_id and T1.anchor_date = T6.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_case_counts T7 ON (T1.patient_id = T7.patient_id and T1.anchor_date = T7.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hepb_diags T8 ON (T1.patient_id = T8.patient_id and T1.anchor_date = T8.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hiv_events_nohef_counts T9 ON (T1.patient_id = T9.patient_id and T1.anchor_date = T9.anchor_date)
LEFT JOIN kre_report.hiv_hrs_viral_loads T10 ON (T1.patient_id = T10.patient_id and T1.anchor_date = T10.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_case_values T11 ON (T1.patient_id = T11.patient_id and T1.anchor_date = T11.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_rx_3_diff_count T12 ON (T1.patient_id = T12.patient_id and T1.anchor_date = T12.anchor_date)
LEFT JOIN kre_report.hiv_hrs_prep_final T13 ON (T1.patient_id = T13.patient_id and T1.anchor_date = T13.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_dx_code_counts T14 ON (T1.patient_id = T14.patient_id and T1.anchor_date = T14.anchor_date)
LEFT JOIN kre_report.hiv_hrs_dx_code_years T15 ON (T1.patient_id = T15.patient_id and T1.anchor_date = T15.anchor_date)
LEFT JOIN kre_report.hiv_hrs_bicillin_meds_updated T16 ON (T1.patient_id = T16.patient_id and T1.anchor_date = T16.anchor_date)
LEFT JOIN kre_report.hiv_hrs_azith_meds T17 ON (T1.patient_id = T17.patient_id and T1.anchor_date = T17.anchor_date)
LEFT JOIN kre_report.hiv_hrs_ceft_meds T18 ON (T1.patient_id = T18.patient_id and T1.anchor_date = T18.anchor_date)
LEFT JOIN kre_report.hiv_hrs_doxy_meds T19 ON (T1.patient_id = T19.patient_id and T1.anchor_date = T19.anchor_date)
LEFT JOIN kre_report.hiv_hrs_other_meds T20 ON (T1.patient_id = T20.patient_id and T1.anchor_date = T20.anchor_date)
LEFT JOIN kre_report.hiv_hrs_mpox_vax T21 ON (T1.patient_id = T21.patient_id and T1.anchor_date = T21.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hpv_vax T22 ON (T1.patient_id = T22.patient_id and T1.anchor_date = T22.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_pep_counts T23 ON (T1.patient_id = T23.patient_id and T1.anchor_date = T23.anchor_date)
WHERE 
(gon_tests_1yr = 0 or gon_tests_1yr is null)
AND (gon_tests_1_to_2yr = 0 or gon_tests_1_to_2yr is null)
AND (gon_tests_gt_2yr = 0 or gon_tests_gt_2yr is null)
AND (gon_pos_tests_1yr = 0 or gon_pos_tests_1yr is null)
AND (gon_pos_tests_1_to_2yr = 0 or gon_pos_tests_1_to_2yr is null)
AND (gon_pos_tests_gt_2yr = 0 or gon_pos_tests_gt_2yr is null)
AND (gon_tests_rectal_1yr = 0 or gon_tests_rectal_1yr is null)
AND (gon_tests_rectal_1_to_2yr = 0 or gon_tests_rectal_1_to_2yr is null)
AND (gon_tests_rectal_gt_2yr = 0 or gon_tests_rectal_gt_2yr is null)
AND (gon_tests_rectal_pos_1yr = 0 or gon_tests_rectal_pos_1yr is null)
AND (gon_tests_rectal_pos_1_to_2yr = 0 or gon_tests_rectal_pos_1_to_2yr is null)
AND (gon_tests_rectal_pos_gt_2yr = 0 or gon_tests_rectal_pos_gt_2yr is null)
AND (gon_tests_oral_1yr = 0 or gon_tests_oral_1yr is null)
AND (gon_tests_oral_1_to_2yr = 0 or gon_tests_oral_1_to_2yr is null)
AND (gon_tests_oral_gt_2yr = 0 or gon_tests_oral_gt_2yr is null)
AND (gon_tests_oral_pos_1yr = 0 or gon_tests_oral_pos_1yr is null)
AND (gon_tests_oral_pos_1_to_2yr = 0 or gon_tests_oral_pos_1_to_2yr is null)
AND (gon_tests_oral_pos_gt_2yr = 0 or gon_tests_oral_pos_gt_2yr is null)
AND (chlam_tests_1yr = 0 or chlam_tests_1yr is null)
AND (chlam_tests_1_to_2yr = 0 or chlam_tests_1_to_2yr is null)
AND (chlam_tests_gt_2yr = 0 or chlam_tests_gt_2yr is null)
AND (chlam_pos_tests_1yr = 0 or chlam_pos_tests_1yr is null)
AND (chlam_pos_tests_1_to_2yr = 0 or chlam_pos_tests_1_to_2yr is null)
AND (chlam_pos_tests_gt_2yr = 0 or chlam_pos_tests_gt_2yr is null)
AND (chlam_tests_rectal_1yr = 0 or chlam_tests_rectal_1yr is null)
AND (chlam_tests_rectal_1_to_2yr = 0 or chlam_tests_rectal_1_to_2yr is null)
AND (chlam_tests_rectal_gt_2yr = 0 or chlam_tests_rectal_gt_2yr is null)
AND (chlam_tests_rectal_pos_1yr = 0 or chlam_tests_rectal_pos_1yr is null)
AND (chlam_tests_rectal_pos_1_to_2yr = 0 or chlam_tests_rectal_pos_1_to_2yr is null)
AND (chlam_tests_rectal_pos_gt_2yr = 0 or chlam_tests_rectal_pos_gt_2yr is null)
AND (chlam_tests_oral_1yr = 0 or chlam_tests_oral_1yr is null)
AND (chlam_tests_oral_1_to_2yr = 0 or chlam_tests_oral_1_to_2yr is null)
AND (chlam_tests_oral_gt_2yr = 0 or chlam_tests_oral_gt_2yr is null)
AND (chlam_tests_oral_pos_1yr = 0 or chlam_tests_oral_pos_1yr is null)
AND (chlam_tests_oral_pos_1_to_2yr = 0 or chlam_tests_oral_pos_1_to_2yr is null)
AND (chlam_tests_oral_pos_gt_2yr = 0 or chlam_tests_oral_pos_gt_2yr is null)
AND (syph_tests_1yr = 0 or syph_tests_1yr is null)
AND (syph_tests_1_to_2yr = 0 or syph_tests_1_to_2yr is null)
AND (syph_tests_gt_2yr = 0 or syph_tests_gt_2yr is null)
AND (syph_per_esp_1yr = 0 or syph_per_esp_1yr is null)
AND (syph_per_esp_1_to_2yr = 0 or syph_per_esp_1_to_2yr is null)
AND (syph_per_esp_gt_2yr = 0 or syph_per_esp_gt_2yr is null)
AND (hcv_ab_1yr = 0 or hcv_ab_1yr is null)
AND (hcv_ab_1_to_2yr = 0 or hcv_ab_1_to_2yr is null)
AND (hcv_ab_gt_2yr = 0 or hcv_ab_gt_2yr is null)
AND (hcv_rna_1yr = 0 or hcv_rna_1yr is null)
AND (hcv_rna_1_to_2yr = 0 or hcv_rna_1_to_2yr is null)
AND (hcv_rna_gt_2yr = 0 or hcv_rna_gt_2yr is null)
AND (hbv_sag_1yr = 0 or hbv_sag_1yr is null)
AND (hbv_sag_1_to_2yr = 0 or hbv_sag_1_to_2yr is null)
AND (hbv_sag_gt_2yr = 0 or hbv_sag_gt_2yr is null)
AND (hbv_dna_1yr = 0 or hbv_dna_1yr is null)
AND (hbv_dna_1_to_2yr = 0 or hbv_dna_1_to_2yr is null)
AND (hbv_dna_gt_2yr = 0 or hbv_dna_gt_2yr is null)
AND (hcv_ab_or_rna_pos_ev = 0 or hcv_ab_or_rna_pos_ev is null)
AND (hcv_ab_pos_first_yr is null)
AND (hcv_rna_pos_first_yr is null)
AND (hbv_sag_or_dna_pos_ev = 0 or hbv_sag_or_dna_pos_ev is null)
AND (hbv_sag_or_dna_pos_first_yr is null)
AND (hcv_acute_per_esp = 0 or hcv_acute_per_esp is null)
AND (hcv_acute_per_esp_case_yrs is null)
AND (hbv_acute_per_esp = 0 or hbv_acute_per_esp is null)
AND (hbv_acute_per_esp_case_yrs is null)
AND (hep_b_dx = 0 or hep_b_dx is null)
AND (hiv_elisa_ag_ab_1yr = 0 or hiv_elisa_ag_ab_1yr is null)
AND (hiv_elisa_ag_ab_1_to_2yr = 0 or hiv_elisa_ag_ab_1_to_2yr is null)
AND (hiv_elisa_ag_ab_gt_2yr = 0 or hiv_elisa_ag_ab_gt_2yr is null)
AND (hiv_confirm_tests_1yr = 0 or hiv_confirm_tests_1yr is null)
AND (hiv_confirm_tests_1_to_2yr = 0 or hiv_confirm_tests_1_to_2yr is null)
AND (hiv_confirm_tests_gt_2yr = 0 or hiv_confirm_tests_gt_2yr is null)
AND (hiv_rna_viral_1yr = 0 or hiv_rna_viral_1yr is null)
AND (hiv_rna_viral_1_to_2yr = 0 or hiv_rna_viral_1_to_2yr is null)
AND (hiv_rna_viral_gt_2yr = 0 or hiv_rna_viral_gt_2yr is null)
AND (hiv_max_viral_load_yr is null)
AND (hiv_status_per_esp = 2 or hiv_status_per_esp is null)
AND (hiv_new_diag_final is null)
AND (hiv_neg_w_rna_test_ev = 0 or hiv_neg_w_rna_test_ev is null)
AND (hiv_neg_w_hiv_med_ev = 0 or hiv_neg_w_hiv_med_ev is null)
AND (hiv_trmt_regimen_1yr = 0 or hiv_trmt_regimen_1yr is null)
AND (hiv_trmt_regimen_1_to2yr = 0 or hiv_trmt_regimen_1_to2yr is null)
AND (hiv_trmt_regimen_gt_2yr = 0 or hiv_trmt_regimen_gt_2yr is null)
AND (pep_rx_dates_num = 0 or pep_rx_dates_num is null)
AND (prep_rx_num_1yr = 0 or prep_rx_num_1yr is null)
AND (prep_rx_num_1_to_2yr = 0 or prep_rx_num_1_to_2yr is null)
AND (prep_rx_num_gt_2yr = 0 or prep_rx_num_gt_2yr is null)
AND (hiv_dx_1_yr = 0 or hiv_dx_1_yr is null)
AND (hiv_dx_1_to_2yr = 0 or hiv_dx_1_to_2yr is null)
AND (hiv_dx_gt_2yr = 0 or hiv_dx_gt_2yr is null)
AND (dx_abn_anal_cyt_yrs is null )
AND (dx_syph_yrs is null )
AND (dx_anal_syph_yrs is null )
AND (dx_gon_inf_rectum_yrs is null )
AND (dx_gon_pharyn_yrs is null )
AND (dx_chlam_inf_rectum_yrs is null )
AND (dx_chlam_pharyn_yrs is null )
AND (dx_lymph_ven_yrs is null )
AND (dx_chancroid_yrs is null )
AND (dx_granuloma_ingu_yrs is null )
AND (dx_nongon_ureth_yrs is null )
AND (dx_hsv_w_compl_yrs is null )
AND (dx_genital_herpes_yrs is null )
AND (dx_anogenital_warts_yrs is null )
AND (dx_anorectal_ulcer_yrs is null )
AND (dx_trich_yrs is null )
AND (dx_mpox_yrs is null )
AND (dx_unspec_std_yrs is null )
AND (dx_pid_yrs is null )
AND (dx_cont_w_exp_to_vd_yrs is null )
AND (dx_high_risk_sex_behav_yrs is null )
AND (dx_hiv_counsel_yrs is null )
AND (dx_anorexia_yrs is null )
AND (dx_bulimia_yrs is null )
AND (dx_eat_disorder_nos_yrs is null )
AND (dx_gend_iden_dis_yrs is null )
AND (dx_transsexualism_yrs is null )
AND (dx_couns_or_prb_child_sex_abuse_yrs is null )
AND (dx_adult_child_sex_exp_or_abuse_yrs is null )
AND (dx_adult_or_child_phys_abuse_yrs is null )
AND (dx_foreign_body_anus_yrs is null )
AND (dx_alcohol_depend_abuse_yrs is null )
AND (dx_opioid_depend_abuse_yrs is null )
AND (dx_sed_hypn_anxio_depend_abuse_yrs is null )
AND (dx_cocaine_depend_abuse_yrs is null )
AND (dx_amphet_stim_depend_abuse_yrs is null )
AND (dx_oth_psycho_or_nos_subs_depend_abuse_yrs is null )
AND (dx_sti_screen_yrs is null )
AND (dx_malig_neo_rec_screen_yrs is null )
AND (dx_needle_stick is null )
AND (rx_bicillin_ever = 0 or rx_bicillin_ever is null)
AND (rx_azith_ever = 0 or rx_azith_ever is null)
AND (rx_ceft_ever = 0 or rx_ceft_ever is null)
AND (rx_doxy_ever = 0 or rx_doxy_ever is null)
AND (rx_methadone_ever = 0 or rx_methadone_ever is null)
AND (rx_buprenorphine_ever = 0 or rx_buprenorphine_ever is null)
AND (rx_naltrexone_ever = 0 or rx_naltrexone_ever is null)
AND (rx_viagara_cilais_or_levitra_ever = 0 or rx_viagara_cilais_or_levitra_ever is null)
AND (vax_mpox_ever = 0 or vax_mpox_ever is null)
AND (vax_hpv_male_ever = 0 or vax_hpv_male_ever is null);

-- DROP PATIENTS WITH NO VARIABLES OF INTEREST & NOT A NEW_HIV_DIAG_PATIENT & NOT A HIV STI PATIENT
DROP TABLE IF EXISTS kre_report.hiv_hrs_index_patients_w_voi;
CREATE TABLE kre_report.hiv_hrs_index_patients_w_voi AS 
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_index_patients
EXCEPT
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_pats_to_drop
EXCEPT
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_hiv_case_values WHERE hiv_new_diag_final = 1
EXCEPT
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats;

--
-- CONTROL PATIENT LIMITING
--

--LIMIT TO ONE PATIENT SINCE THE SAME PATIENT MAY MATCH MULTIPLE ANCHOR DATES
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_pats_unique;
CREATE TABLE kre_report.hiv_hrs_control_pats_unique AS 
SELECT T1.patient_id, max(T1.anchor_date) anchor_date
from kre_report.hiv_hrs_index_patients_w_voi T1
group by T1.patient_id;



/* -- GET # OF CONTROL PATIENTS NEEDED PER ANCHOR DATE
DROP TABLE IF EXISTS kre_report.hiv_hrs_num_ctrl_pats;
CREATE TABLE kre_report.hiv_hrs_num_ctrl_pats as
select count(*) as hiv_new_diag_pat_count, (to_char(anchor_date, 'YYYY-MM')) anchor_month, gender,
case when count(*) = 1 then 10 
     when count(*) = 2 then 20 
	 when count(*) = 3 then 30 
	 when count(*) = 4 then 40 else null end as num_control_pats
--from kre_report.hiv_hrs_pats_anchor_dates
FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats
group by anchor_month, gender; */


-- GET # OF CONTROL PATIENTS NEEDED PER ANCHOR DATE
DROP TABLE IF EXISTS kre_report.hiv_hrs_num_ctrl_pats;
CREATE TABLE kre_report.hiv_hrs_num_ctrl_pats as
select count(*) as hiv_new_diag_pat_count, (to_char(anchor_date, 'YYYY-MM')) anchor_month, gender,
count(*) * 10 as num_control_pats
--from kre_report.hiv_hrs_pats_anchor_dates
FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats
group by anchor_month, gender;

/* -- IDENTIFY RANDOM PATIENTS PER ANCHOR DATE
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_patients_final;
CREATE TABLE kre_report.hiv_hrs_control_patients_final as
SELECT 
    patient_id, T1.anchor_date
FROM
  ( SELECT patient_id, anchor_date, 
           ROW_NUMBER() OVER (PARTITION BY anchor_date
                              ORDER BY random() 
                             ) AS rn
    FROM kre_report.hiv_hrs_control_pats_unique
	--FROM kre_report.hiv_hrs_index_patients_w_voi
    GROUP BY patient_id, anchor_date
  ) AS T1
  JOIN  kre_report.hiv_hrs_num_ctrl_pats T2 ON (T1.anchor_date = T2.anchor_date)
WHERE
	rn <= num_control_pats
ORDER BY 
    patient_id, rn ; */
	


-- IDENTIFY RANDOM PATIENTS PER ANCHOR MONTH & GENDER
DROP TABLE IF EXISTS kre_report.hiv_hrs_control_patients_final;
CREATE TABLE kre_report.hiv_hrs_control_patients_final as
SELECT 
    patient_id, T1.anchor_date, T1.gender
FROM
  ( SELECT T1.patient_id, gender, anchor_date, 
           ROW_NUMBER() OVER (PARTITION BY to_char(anchor_date, 'YYYY-MM'), gender
                              ORDER BY random() 
                             ) AS rn
    FROM kre_report.hiv_hrs_control_pats_unique T1
	INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
    GROUP BY patient_id, anchor_date, gender
  ) AS T1
  JOIN  kre_report.hiv_hrs_num_ctrl_pats T2 ON (to_char(T1.anchor_date, 'YYYY-MM') = T2.anchor_month and T1.gender = T2.gender)
WHERE
	rn <= num_control_pats
ORDER BY 
    patient_id, rn ;
	

	
/* -- CHECK FOR ANY DATES THAT DON'T HAVE ENOUGH CONTROL PATIENTS	
SELECT anchor_date, record_count, num_control_pats
FROM (
select count(T1.anchor_date) record_count, T1.anchor_date, num_control_pats
from kre_report.hiv_hrs_control_patients_final T1
INNER JOIN kre_report.hiv_hrs_num_ctrl_pats T2 ON (T1.anchor_date = T2.anchor_date)
GROUP BY T1.anchor_date, num_control_pats) T1
WHERE record_count < num_control_pats;	 */

-- CHECK FOR ANY MONTHS THAT DON'T HAVE ENOUGH CONTROL PATIENTS	
SELECT anchor_month, record_count, num_control_pats, gender
FROM (
select count(to_char(T2.anchor_date, 'YYYY-MM')) as record_count, T1.anchor_month, num_control_pats, T1.gender
FROM kre_report.hiv_hrs_num_ctrl_pats T1
LEFT JOIN kre_report.hiv_hrs_control_patients_final T2 ON (to_char(T2.anchor_date, 'YYYY-MM') = T1.anchor_month and T1.gender = T2.gender)
GROUP BY T1.anchor_month, num_control_pats, T1.gender) T1
WHERE record_count < num_control_pats;	
	

--ADD BACK IN NEW HIV DIAG PATIENTS
--THIS IS OUR LIST OF PATIENTS TO USE FOR REPORTING
DROP TABLE IF EXISTS kre_report.hiv_hrs_report_pats_dates_final;
CREATE TABLE kre_report.hiv_hrs_report_pats_dates_final as
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_control_patients_final
UNION
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_hiv_case_values WHERE hiv_new_diag_final = 1
UNION
-- THIS ADDS BACK IN STI RISK PATS THAT ARE >= AGE 15 ON ANCHOR DATE
SELECT patient_id, anchor_date FROM kre_report.hiv_hrs_pats_anchor_sti_hiv_pats WHERE sti_risk_pat = 1 and patient_id in (select patient_id from kre_report.hiv_hrs_index_patients);

--DEMOGRAPHICS FROM PATIENT
DROP TABLE IF EXISTS kre_report.hiv_hrs_demog;
CREATE TABLE kre_report.hiv_hrs_demog as
SELECT T1.patient_id, T1.anchor_date,
date_part('year', age(anchor_date, date_of_birth)) age_on_anchor_date,
CASE WHEN upper(gender) in ('M', 'MALE') then 1 ELSE 0 END as gender_male,
CASE WHEN upper(gender) in ('F', 'FEMALE') then 1 ELSE 0 END as gender_female,
CASE WHEN upper(gender) in ('T') then 1 ELSE 0 END as gender_trans,
CASE WHEN upper(gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') or gender is null then 1 ELSE 0 END as gender_unk,
CASE WHEN upper(birth_sex) in ('M', 'MALE') then 1 ELSE 0 END as birth_sex_male,
CASE WHEN upper(birth_sex) in ('F', 'FEMALE') then 1 ELSE 0 END as birth_sex_female,
CASE WHEN upper(birth_sex) not in ('M', 'MALE', 'F', 'FEMALE') or birth_sex is null then 1 ELSE 0 END as birth_sex_unk,
CASE WHEN upper(sex_orientation) in ('STRAIGHT', 'STRAIGHT OR HETEROSEXUAL', 'STRAIGHT (NOT LESBIAN OR GAY)') then 1 ELSE 0 END as sexor_straight,
CASE WHEN upper(sex_orientation) in ('GAY', 'LESBIAN', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL', 'LESBIAN OR GAY') then 1 ELSE 0 END as sexor_gay_lesbian,
CASE WHEN upper(sex_orientation) in ('BISEXUAL', 'BI')  then 1 ELSE 0 END as sexor_bisexual,
CASE WHEN upper(sex_orientation) not in ('--', 'STRAIGHT', 'STRAIGHT OR HETEROSEXUAL', 'STRAIGHT (NOT LESBIAN OR GAY)','GAY', 'LESBIAN', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL', 'LESBIAN OR GAY','BISEXUAL', 'BI', 'CHOOSE NOT TO DISCLOSE', '''DON''''T KNOW''', 'DON''T KNOW', 'NOT REPORTED', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'NOTDISCLOSED', '') and sex_orientation is not null THEN 1 ELSE 0  END as sexor_other,
CASE WHEN upper(sex_orientation) in ('--', 'CHOOSE NOT TO DISCLOSE', '''DON''''T KNOW''', 'DON''T KNOW', 'NOT REPORTED', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'NOTDISCLOSED', '') or sex_orientation is null THEN 1 ELSE 0 END as sexor_unk,
CASE WHEN upper(gender_identity) in ('MALE') and (upper(birth_sex) not in ('F', 'FEMALE') or birth_sex is null) then 1 ELSE 0 END as gender_iden_male,
CASE WHEN upper(gender_identity) in ('FEMALE') and (upper(birth_sex) not in ('M', 'MALE') or birth_sex is null) then 1 ELSE 0 END as gender_iden_female,
CASE WHEN ((upper(gender_identity) in ('TRANSGENDER MALE', 'TRANSGENDER MALE / FEMALE-TO-MALE')) or (upper(birth_sex) in ('F', 'FEMALE') and upper(gender_identity) in ('MALE'))) then 1 ELSE 0 END as gender_iden_trans_male,
CASE WHEN ((upper(gender_identity) in ('TRANSGENDER FEMALE')) or (upper(birth_sex) in ('M', 'MALE') and upper(gender_identity) in ('FEMALE'))) then 1 ELSE 0 END as gender_iden_trans_female,
CASE WHEN upper(gender_identity) in ('NON-BINARY', 'NONBINARY') then 1 ELSE 0 END as gender_iden_nonbinary,
CASE WHEN upper(gender_identity) not in ('MALE', 'FEMALE', 'TRANSGENDER MALE', 'TRANSGENDER MALE / FEMALE-TO-MALE', 'TRANSGENDER FEMALE', 'NON-BINARY', 'NONBINARY', 'CHOOSE NOT TO DISCLOSE', 'I DON''T KNOW MY GENDER IDENTITY', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOES NOT KNOW', 'PATIENT NOT SURE/DOES NOT KNOW', 'CHOOSE NOT TO DISCLO', 'NOT SURE', '') and gender_identity is not null then 1 ELSE 0 END as gender_iden_other,
CASE WHEN upper(gender_identity) in ('CHOOSE NOT TO DISCLOSE', 'I DON''T KNOW MY GENDER IDENTITY', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOES NOT KNOW', 'PATIENT NOT SURE/DOES NOT KNOW', 'CHOOSE NOT TO DISCLO', 'NOT SURE', '') or gender_identity is null then 1 ELSE 0 END as gender_iden_unk,
CASE when upper(race) in ('WHITE', 'MIDDLE EAST', 'CAUCASIAN', 'W') then 1 ELSE 0 END as race_white,
CASE when upper(race) in ('BLACK', 'BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B') then 1 ELSE 0 END as race_black,
CASE when upper(race) in ('ASIAN', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 1 ELSE 0 END as race_asian,

CASE when upper(race) not in('WHITE', 'MIDDLE EAST', 'CAUCASIAN', 'W', 'BLACK', 'BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B','ASIAN', 'ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'F', 'M', 'PATIENT DECLINED', 'RACE', 'UNKNOWN', 'CHOOSE NOT TO ANSWER', 'DECLINED TO ANSWER', 'I DON''T KNOW MY RACE', 'NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PT IS UNSURE', 'PT REFUSED', 'DECLINED', '') and race is not null then 1 else 0 end as race_other,
CASE when upper(race) in('DECLINED/REFUSED', 'DECLINE TO ANSWER', 'F', 'M', 'PATIENT DECLINED', 'RACE', 'UNKNOWN', 'CHOOSE NOT TO ANSWER', 'DECLINED TO ANSWER', 'I DON''T KNOW MY RACE', 'NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PT IS UNSURE', 'PT REFUSED', 'DECLINED', '') or race is null then 1 else 0 end as race_unk,
CASE when upper(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') or upper(ethnicity) in ('HISPANIC OR LATINO', 'HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN') then 1 ELSE 0 END as ethnicity_hispanic,
CASE when upper(ethnicity) in ('NOT HISPANIC OR LATINO', 'NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN') then 1 ELSE 0 end as  ethnicity_nonhispanic,
CASE when upper(race) not in ('HISPANIC','HISP', 'HISPANIC/LAT') and (upper(ethnicity) not in ('HISPANIC OR LATINO', 'HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'NOT HISPANIC OR LATINO', 'NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN') or ethnicity is null) then 1 ELSE 0 end as ethnicity_unk,
CASE when upper(marital_stat) in ('MARRIED') then 1 ELSE 0 END as marital_stat_married,
CASE WHEN upper(marital_stat) in ('DOMESTIC PARTNER', 'LIFE PARTNER', 'PARTNERED', 'SIGNIFICANT OTHER', 'SIGNFICANT OTHER', 'PARTNER', 'COHABITATING', 'ENGAGED') THEN 1 ELSE 0 END as marital_stat_partner,
CASE WHEN upper(marital_stat) in ('SINGLE') THEN 1 ELSE 0 END as marital_stat_single,
CASE WHEN upper(marital_stat) in ('DIVORCED') THEN 1 ELSE 0 END as marital_stat_divorced,
CASE WHEN upper(marital_stat) in ('WIDOWED') THEN 1 ELSE 0 END as marital_stat_widow,
CASE WHEN upper(marital_stat) in ('CHILD', 'MARITAL_STAT', 'UNKNOWN', 'UNKNOWN OR OTHER', 'NONE', 'PREFER NOT TO ANSWER', '') OR marital_stat is null THEN 1 ELSE 0 END as marital_stat_unk,
CASE WHEN upper(marital_stat) not in ('MARRIED', 'DOMESTIC PARTNER', 'LIFE PARTNER', 'PARTNERED', 'SIGNIFICANT OTHER', 'SIGNFICANT OTHER', 'PARTNER', 'COHABITATING', 'ENGAGED', 'SINGLE', 'DIVORCED', 'WIDOWED', 'CHILD', 'MARITAL_STAT', 'UNKNOWN', 'UNKNOWN OR OTHER', 'NONE', 'PREFER NOT TO ANSWER', '') and marital_stat is not null THEN 1 ELSE 0 END as marital_stat_other,
CASE WHEN upper(insurance_status)in ('BLUE CROSS', 'COMMERCIAL') THEN 1
     WHEN upper(insurance_status)in ('HEALTH SAFETY NET', 'MEDICAID', 'MEDICAID REPLACEMENT', 'MEDICARE', 'MEDICARE REPLACEMENT', 'TRICARE' ) THEN 2
	 WHEN upper(insurance_status)in ('SELF-PAY') THEN 3
	 ELSE 0 END as insurance_status
FROM kre_report.hiv_hrs_report_pats_dates_final T1
JOIN emr_patient T2 ON (T1.patient_id = T2.id);


DROP TABLE IF EXISTS kre_report.hiv_hrs_sex_p_gender;
CREATE TABLE kre_report.hiv_hrs_sex_p_gender as
SELECT T1.patient_id, T1.anchor_date, 
MAX(CASE WHEN sex_partner_gender in ('MALE') THEN 1 ELSE 0 END) as sex_partner_male,
MAX(CASE WHEN sex_partner_gender in ('FEMALE') THEN 1 ELSE 0 END) as sex_partner_female,
MAX(CASE WHEN sex_partner_gender in ('MALE + FEMALE', 'FEMALE + MALE') THEN 1 ELSE 0 END) as sex_partner_mandf,
MAX(CASE WHEN sex_partner_gender in ('TRANS') THEN 1 ELSE 0 END) as sex_partner_trans,
MAX(CASE WHEN sex_partner_gender not in ('MALE','FEMALE', 'MALE + FEMALE', 'FEMALE + MALE', 'TRANS') or sex_partner_gender is null THEN 1 ELSE 0 END) as sex_partner_unk
FROM kre_report.hiv_hrs_report_pats_dates_final T1
-- Get the most recent social history date to determine sex_partner_gender
LEFT JOIN ((SELECT max(date) sochist_date, T2.patient_id, T2.anchor_date
	        FROM emr_socialhistory T1,
	        kre_report.hiv_hrs_report_pats_dates_final T2
	        WHERE T1.patient_id = T2.patient_id
	        AND sex_partner_gender is not null		
			--REPLACE WITH END DATE
            AND T1.date <= T2.anchor_date
        	GROUP BY T2.patient_id, T2.anchor_date) ) T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_socialhistory T3 on (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id and T2.sochist_date = T3.date and sex_partner_gender is not null)
GROUP BY T1.patient_id, T1.anchor_date;

DROP TABLE IF EXISTS kre_report.hiv_hrs_output_prep;
CREATE TABLE kre_report.hiv_hrs_output_prep AS
SELECT T1.patient_id, 
T1.anchor_date,
coalesce(T27.sti_risk_pat, 0) sti_risk_pat,
coalesce(sti_risk_gon_pos, 0) sti_risk_gon_pos,
coalesce(sti_risk_gon_rectal, 0) sti_risk_gon_rectal,
coalesce(sti_risk_gon_oral, 0) sti_risk_gon_oral,
coalesce(sti_risk_chlam_rectal, 0) sti_risk_chlam_rectal,
coalesce(sti_risk_chlam_oral, 0) sti_risk_chlam_oral,
coalesce(sti_risk_syphilis, 0) sti_risk_syphilis,
coalesce(sti_risk_2hiv_labs, 0) sti_risk_2hiv_labs,
coalesce(sti_risk_mpox_pos, 0) sti_risk_mpox_pos,
coalesce(sti_risk_high_risk_sex_icd, 0) sti_risk_high_risk_sex_icd,
extract(year from T1.anchor_date) as anchor_date_yr,
T2.age_on_anchor_date,
gender_male,
gender_female,
gender_trans,
gender_unk,
birth_sex_male,
birth_sex_female,
birth_sex_unk,
sexor_straight,
sexor_gay_lesbian,
sexor_bisexual,
sexor_other,
sexor_unk,
gender_iden_male,
gender_iden_female,
gender_iden_trans_male,
gender_iden_trans_female,
gender_iden_nonbinary,
gender_iden_other,
gender_iden_unk,
race_white,
race_black,
race_asian,
race_other,
race_unk,
ethnicity_hispanic,
ethnicity_nonhispanic,
ethnicity_unk,
sex_partner_male,
sex_partner_female,
sex_partner_mandf,
sex_partner_trans,
sex_partner_unk,
marital_stat_married,
marital_stat_partner,
marital_stat_single,
marital_stat_unk,
marital_stat_widow,
marital_stat_divorced,
marital_stat_other,
--DO NOT INCLUDE IN REPORT
--insurance_status,
COALESCE(num_encs_1yr, 0) as num_encs_1yr,
COALESCE(num_encs_1_to_2yr, 0) as num_encs_1_to_2yr,
COALESCE(num_encs_gt_2yr, 0) as num_encs_gt_2yr,
COALESCE(gon_tests_1yr, 0) as gon_tests_1yr,
COALESCE(gon_tests_1_to_2yr, 0) as gon_tests_1_to_2yr,
COALESCE(gon_tests_gt_2yr, 0) as gon_tests_gt_2yr,
COALESCE(gon_pos_tests_1yr, 0) as gon_pos_tests_1yr,
COALESCE(gon_pos_tests_1_to_2yr, 0) as gon_pos_tests_1_to_2yr,
COALESCE(gon_pos_tests_gt_2yr, 0) as gon_pos_tests_gt_2yr,
COALESCE(gon_tests_rectal_1yr, 0) as gon_tests_rectal_1yr,
COALESCE(gon_tests_rectal_1_to_2yr, 0) as gon_tests_rectal_1_to_2yr,
COALESCE(gon_tests_rectal_gt_2yr, 0) as gon_tests_rectal_gt_2yr,
COALESCE(gon_tests_rectal_pos_1yr, 0) as gon_tests_rectal_pos_1yr,
COALESCE(gon_tests_rectal_pos_1_to_2yr, 0) as gon_tests_rectal_pos_1_to_2yr,
COALESCE(gon_tests_rectal_pos_gt_2yr, 0) as gon_tests_rectal_pos_gt_2yr,
COALESCE(gon_tests_oral_1yr, 0) as gon_tests_oral_1yr,
COALESCE(gon_tests_oral_1_to_2yr, 0) as gon_tests_oral_1_to_2yr,
COALESCE(gon_tests_oral_gt_2yr, 0) as gon_tests_oral_gt_2yr,
COALESCE(gon_tests_oral_pos_1yr, 0) as gon_tests_oral_pos_1yr,
COALESCE(gon_tests_oral_pos_1_to_2yr, 0) as gon_tests_oral_pos_1_to_2yr,
COALESCE(gon_tests_oral_pos_gt_2yr, 0) as gon_tests_oral_pos_gt_2yr,
COALESCE(chlam_tests_1yr, 0) as chlam_tests_1yr,
COALESCE(chlam_tests_1_to_2yr, 0) as chlam_tests_1_to_2yr,
COALESCE(chlam_tests_gt_2yr, 0) as chlam_tests_gt_2yr,
COALESCE(chlam_pos_tests_1yr, 0) as chlam_pos_tests_1yr,
COALESCE(chlam_pos_tests_1_to_2yr, 0) as chlam_pos_tests_1_to_2yr,
COALESCE(chlam_pos_tests_gt_2yr, 0) as chlam_pos_tests_gt_2yr,
COALESCE(chlam_tests_rectal_1yr, 0) as chlam_tests_rectal_1yr,
COALESCE(chlam_tests_rectal_1_to_2yr, 0) as chlam_tests_rectal_1_to_2yr,
COALESCE(chlam_tests_rectal_gt_2yr, 0) as chlam_tests_rectal_gt_2yr,
COALESCE(chlam_tests_rectal_pos_1yr, 0) as chlam_tests_rectal_pos_1yr,
COALESCE(chlam_tests_rectal_pos_1_to_2yr, 0) as chlam_tests_rectal_pos_1_to_2yr,
COALESCE(chlam_tests_rectal_pos_gt_2yr, 0) as chlam_tests_rectal_pos_gt_2yr,
COALESCE(chlam_tests_oral_1yr, 0) as chlam_tests_oral_1yr,
COALESCE(chlam_tests_oral_1_to_2yr, 0) as chlam_tests_oral_1_to_2yr,
COALESCE(chlam_tests_oral_gt_2yr, 0) as chlam_tests_oral_gt_2yr,
COALESCE(chlam_tests_oral_pos_1yr, 0) as chlam_tests_oral_pos_1yr,
COALESCE(chlam_tests_oral_pos_1_to_2yr, 0) as chlam_tests_oral_pos_1_to_2yr,
COALESCE(chlam_tests_oral_pos_gt_2yr, 0) as chlam_tests_oral_pos_gt_2yr,
COALESCE(syph_tests_1yr, 0) as syph_tests_1yr,
COALESCE(syph_tests_1_to_2yr, 0) as syph_tests_1_to_2yr,
COALESCE(syph_tests_gt_2yr, 0) as syph_tests_gt_2yr,
COALESCE(syph_per_esp_1yr, 0) as syph_per_esp_1yr,
COALESCE(syph_per_esp_1_to_2yr, 0) as syph_per_esp_1_to_2yr,
COALESCE(syph_per_esp_gt_2yr, 0) as syph_per_esp_gt_2yr,
COALESCE(hcv_ab_1yr, 0) as hcv_ab_1yr,
COALESCE(hcv_ab_1_to_2yr, 0) as hcv_ab_1_to_2yr,
COALESCE(hcv_ab_gt_2yr, 0) as hcv_ab_gt_2yr,
COALESCE(hcv_rna_1yr, 0) as hcv_rna_1yr,
COALESCE(hcv_rna_1_to_2yr, 0) as hcv_rna_1_to_2yr,
COALESCE(hcv_rna_gt_2yr, 0) as hcv_rna_gt_2yr,
COALESCE(hcv_ab_or_rna_pos_ev, 0) as hcv_ab_or_rna_pos_ev,
COALESCE(hcv_ab_pos_first_yr + 1, 0) as hcv_ab_pos_first_yr,
COALESCE(hcv_rna_pos_first_yr + 1, 0) as hcv_rna_pos_first_yr,
COALESCE(hcv_acute_per_esp, 0) as hcv_acute_per_esp,
COALESCE(hcv_acute_per_esp_case_yrs + 1, 0) as hcv_acute_per_esp_case_yrs,
COALESCE(hbv_sag_1yr, 0) as hbv_sag_1yr,
COALESCE(hbv_sag_1_to_2yr, 0) as hbv_sag_1_to_2yr,
COALESCE(hbv_sag_gt_2yr, 0) as hbv_sag_gt_2yr,
COALESCE(hbv_dna_1yr, 0) as hbv_dna_1yr,
COALESCE(hbv_dna_1_to_2yr, 0) as hbv_dna_1_to_2yr,
COALESCE(hbv_dna_gt_2yr, 0) as hbv_dna_gt_2yr,
COALESCE(hbv_sag_or_dna_pos_ev, 0) as hbv_sag_or_dna_pos_ev,
COALESCE(hbv_sag_or_dna_pos_first_yr + 1, 0) as hbv_sag_or_dna_pos_first_yr,
COALESCE(hbv_acute_per_esp, 0) as hbv_acute_per_esp,
COALESCE(hbv_acute_per_esp_case_yrs + 1, 0) as hbv_acute_per_esp_case_yrs,
COALESCE(hbv_sag_or_dna_pos_ev, hep_b_dx, 0) as hbv_history_ev,
COALESCE(hiv_elisa_ag_ab_1yr, 0) as hiv_elisa_ag_ab_1yr,
COALESCE(hiv_elisa_ag_ab_1_to_2yr, 0) as hiv_elisa_ag_ab_1_to_2yr,
COALESCE(hiv_elisa_ag_ab_gt_2yr, 0) as hiv_elisa_ag_ab_gt_2yr,
COALESCE(hiv_confirm_tests_1yr, 0) as hiv_confirm_tests_1yr,
COALESCE(hiv_confirm_tests_1_to_2yr, 0) as hiv_confirm_tests_1_to_2yr,
COALESCE(hiv_confirm_tests_gt_2yr, 0) as hiv_confirm_tests_gt_2yr,
COALESCE(hiv_rna_viral_1yr, 0) as hiv_rna_viral_1yr,
COALESCE(hiv_rna_viral_1_to_2yr, 0) as hiv_rna_viral_1_to_2yr,
COALESCE(hiv_rna_viral_gt_2yr, 0) as hiv_rna_viral_gt_2yr,
COALESCE(hiv_max_viral_load_yr + 1, 0) as hiv_max_viral_load_yr,
-- DO NOT INCLUDE IN THE OUTPUT
--hiv_status_per_esp,
--COALESCE(hiv_per_esp_case_yrs + 1, 0) as hiv_per_esp_case_yrs,
COALESCE(hiv_new_diag_final, 0) as hiv_new_diag_final,
-- DO NOT INCLUDE IN THE OUTPUT
--hiv_neg_w_rna_test_ev,
hiv_neg_w_hiv_med_ev,
COALESCE(hiv_dx_1_yr, 0) as hiv_dx_1_yr,
COALESCE(hiv_dx_1_to_2yr, 0) as hiv_dx_1_to_2yr,
COALESCE(hiv_dx_gt_2yr, 0) as hiv_dx_gt_2yr,
COALESCE(hiv_trmt_regimen_1yr, 0) as hiv_trmt_regimen_1yr,
COALESCE(hiv_trmt_regimen_1_to2yr, 0) as hiv_trmt_regimen_1_to2yr,
COALESCE(hiv_trmt_regimen_gt_2yr, 0) as hiv_trmt_regimen_gt_2yr,
COALESCE(pep_rx_dates_num, 0) as pep_rx_dates_num,
COALESCE(prep_rx_num_1yr, 0) as prep_rx_num_1yr,
COALESCE(prep_rx_num_1_to_2yr, 0) as prep_rx_num_1_to_2yr,
COALESCE(prep_rx_num_gt_2yr, 0) as prep_rx_num_gt_2yr,
COALESCE(dx_abn_anal_cyt_yrs + 1, 0) as dx_abn_anal_cyt_yrs,
COALESCE(dx_syph_yrs + 1, 0) as dx_syph_yrs,
COALESCE(dx_anal_syph_yrs + 1, 0) as dx_anal_syph_yrs,
COALESCE(dx_gon_inf_rectum_yrs + 1, 0) as dx_gon_inf_rectum_yrs,
COALESCE(dx_gon_pharyn_yrs + 1, 0) as dx_gon_pharyn_yrs,
COALESCE(dx_chlam_inf_rectum_yrs +1, 0) as dx_chlam_inf_rectum_yrs,
COALESCE(dx_chlam_pharyn_yrs + 1, 0) as dx_chlam_pharyn_yrs,
COALESCE(dx_lymph_ven_yrs + 1, 0) as dx_lymph_ven_yrs,
COALESCE(dx_chancroid_yrs + 1, 0) as dx_chancroid_yrs,
COALESCE(dx_granuloma_ingu_yrs + 1, 0) as dx_granuloma_ingu_yrs,
COALESCE(dx_nongon_ureth_yrs + 1, 0) as dx_nongon_ureth_yrs,
COALESCE(dx_hsv_w_compl_yrs + 1, 0) as dx_hsv_w_compl_yrs,
COALESCE(dx_genital_herpes_yrs + 1, 0) as dx_genital_herpes_yrs,
COALESCE(dx_anogenital_warts_yrs + 1, 0) as dx_anogenital_warts_yrs,
COALESCE(dx_anorectal_ulcer_yrs + 1, 0) as dx_anorectal_ulcer_yrs,
COALESCE(dx_trich_yrs + 1, 0) as dx_trich_yrs,
COALESCE(dx_mpox_yrs + 1, 0) as dx_mpox_yrs,
COALESCE(dx_unspec_std_yrs + 1, 0) as dx_unspec_std_yrs,
COALESCE(dx_pid_yrs + 1, 0) as dx_pid_yrs,
COALESCE(dx_cont_w_exp_to_vd_yrs + 1, 0) as dx_cont_w_exp_to_vd_yrs,
COALESCE(dx_high_risk_sex_behav_yrs + 1, 0) as dx_high_risk_sex_behav_yrs,
COALESCE(dx_hiv_counsel_yrs + 1, 0) as dx_hiv_counsel_yrs,
COALESCE(dx_anorexia_yrs + 1, 0) as dx_anorexia_yrs,
COALESCE(dx_bulimia_yrs + 1, 0) as dx_bulimia_yrs,
COALESCE(dx_eat_disorder_nos_yrs + 1, 0) as dx_eat_disorder_nos_yrs,
COALESCE(dx_gend_iden_dis_yrs + 1, 0) as dx_gend_iden_dis_yrs,
COALESCE(dx_transsexualism_yrs + 1, 0) as dx_transsexualism_yrs,
COALESCE(dx_couns_or_prb_child_sex_abuse_yrs + 1, 0) as dx_couns_or_prb_child_sex_abuse_yrs,
COALESCE(dx_adult_child_sex_exp_or_abuse_yrs + 1, 0) as dx_adult_child_sex_exp_or_abuse_yrs,
COALESCE(dx_adult_or_child_phys_abuse_yrs + 1, 0) as dx_adult_or_child_phys_abuse_yrs,
COALESCE(dx_foreign_body_anus_yrs + 1, 0) as dx_foreign_body_anus_yrs,
COALESCE(dx_alcohol_depend_abuse_yrs + 1, 0) as dx_alcohol_depend_abuse_yrs,
COALESCE(dx_opioid_depend_abuse_yrs + 1, 0) as dx_opioid_depend_abuse_yrs,
COALESCE(dx_sed_hypn_anxio_depend_abuse_yrs + 1, 0) as dx_sed_hypn_anxio_depend_abuse_yrs,
COALESCE(dx_cocaine_depend_abuse_yrs + 1, 0) as dx_cocaine_depend_abuse_yrs,
COALESCE(dx_amphet_stim_depend_abuse_yrs + 1, 0) as dx_amphet_stim_depend_abuse_yrs,
COALESCE(dx_oth_psycho_or_nos_subs_depend_abuse_yrs + 1, 0) as dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
COALESCE(dx_sti_screen_yrs + 1, 0) as dx_sti_screen_yrs,
COALESCE(dx_malig_neo_rec_screen_yrs + 1, 0) as dx_malig_neo_rec_screen_yrs,
COALESCE(dx_needle_stick + 1, 0) as dx_needle_stick,
COALESCE(rx_bicillin_ever, 0) as rx_bicillin_ever,
COALESCE(rx_azith_ever, 0) as rx_azith_ever,
COALESCE(rx_ceft_ever, 0) as rx_ceft_ever,
COALESCE(rx_doxy_ever, 0) as rx_doxy_ever,
COALESCE(rx_methadone_ever, 0) as rx_methadone_ever,
COALESCE(rx_buprenorphine_ever, 0) as rx_buprenorphine_ever,
COALESCE(rx_naltrexone_ever, 0) as rx_naltrexone_ever,
COALESCE(rx_viagara_cilais_or_levitra_ever, 0) as rx_viagara_cilais_or_levitra_ever,
COALESCE(vax_mpox_ever, 0) as vax_mpox_ever,
COALESCE(vax_hpv_male_ever, 0) as vax_hpv_male_ever
FROM kre_report.hiv_hrs_report_pats_dates_final T1
LEFT JOIN kre_report.hiv_hrs_demog T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
LEFT JOIN kre_report.hiv_hrs_sex_p_gender T3 ON (T1.patient_id = T3.patient_id and T1.anchor_date = T3.anchor_date)
LEFT JOIN kre_report.hiv_hrs_clin_enc_visits_num T4 ON (T1.patient_id = T4.patient_id and T1.anchor_date = T4.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_chlam_gon_labs_counts T5 ON (T1.patient_id = T5.patient_id and T1.anchor_date = T5.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_syph_counts T6 ON (T1.patient_id = T6.patient_id and T1.anchor_date = T6.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_syph_case_counts T7 ON (T1.patient_id = T7.patient_id and T1.anchor_date = T7.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_events_nohef_counts T8 ON (T1.patient_id = T8.patient_id and T1.anchor_date = T8.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_events_hef_counts T9 ON (T1.patient_id = T9.patient_id and T1.anchor_date = T9.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hep_case_counts T10 ON (T1.patient_id = T10.patient_id and T1.anchor_date = T10.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hepb_diags T11 ON (T1.patient_id = T11.patient_id and T1.anchor_date = T11.anchor_date)
LEFT JOIN kre_report.hiv_hrs_all_hiv_events_nohef_counts T12 ON (T1.patient_id = T12.patient_id and T1.anchor_date = T12.anchor_date)
LEFT JOIN kre_report.hiv_hrs_viral_loads T13 ON (T1.patient_id = T13.patient_id and T1.anchor_date = T13.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_case_values T14 ON (T1.patient_id = T14.patient_id and T1.anchor_date = T14.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_dx_code_counts T15 ON (T1.patient_id = T15.patient_id and T1.anchor_date = T15.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_rx_3_diff_count T16 ON (T1.patient_id = T16.patient_id and T1.anchor_date = T16.anchor_date)
LEFT JOIN kre_report.hiv_hrs_prep_final T17 ON (T1.patient_id = T17.patient_id and T1.anchor_date = T17.anchor_date)
LEFT JOIN kre_report.hiv_hrs_dx_code_years T18 ON (T1.patient_id = T18.patient_id and T1.anchor_date = T18.anchor_date)
LEFT JOIN kre_report.hiv_hrs_bicillin_meds_updated T19 ON (T1.patient_id = T19.patient_id and T1.anchor_date = T19.anchor_date)
LEFT JOIN kre_report.hiv_hrs_azith_meds T20 ON (T1.patient_id = T20.patient_id and T1.anchor_date = T20.anchor_date)
LEFT JOIN kre_report.hiv_hrs_ceft_meds T21 ON (T1.patient_id = T21.patient_id and T1.anchor_date = T21.anchor_date)
LEFT JOIN kre_report.hiv_hrs_doxy_meds T22 ON (T1.patient_id = T22.patient_id and T1.anchor_date = T22.anchor_date)
LEFT JOIN kre_report.hiv_hrs_other_meds T23 ON (T1.patient_id = T23.patient_id and T1.anchor_date = T23.anchor_date)
LEFT JOIN kre_report.hiv_hrs_mpox_vax T24 ON (T1.patient_id = T24.patient_id and T1.anchor_date = T24.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hpv_vax T25 ON (T1.patient_id = T25.patient_id and T1.anchor_date = T25.anchor_date)
LEFT JOIN kre_report.hiv_hrs_hiv_pep_counts T26 ON (T1.patient_id = T26.patient_id and T1.anchor_date = T26.anchor_date)
LEFT JOIN kre_report.hiv_hrs_pats_anchor_sti_hiv_pats T27 ON (T1.patient_id = T27.patient_id and T1.anchor_date = T27.anchor_date)
LEFT JOIN kre_report.hiv_hrs_sti_risk_variable_matches T28 ON (T1.patient_id = T28.patient_id)



-- -- NEEDED FOR REPORTING
-- -- ONE TIME RUN ONLY
-- CREATE SEQUENCE IF NOT EXISTS  kre_report.hiv_hrs_masked_id_seq;


-- CREATE TABLE kre_report.hiv_hrs_masked_patients
-- (
    -- patient_id integer,
    -- masked_patient_id character varying(128),
	-- anchor_date date
-- );

-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST
-- CHA-, BMC-, FWY-, ATR-

INSERT INTO kre_report.hiv_hrs_masked_patients
SELECT 
DISTINCT T1.patient_id,
CONCAT(:site_abbr, '-', NEXTVAL('kre_report.hiv_hrs_masked_id_seq')) masked_patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_output_prep T1
LEFT JOIN (SELECT patient_id, anchor_date from kre_report.hiv_hrs_masked_patients) T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date)
WHERE T2.patient_id is null 
AND T2.anchor_date is null
GROUP by T1.patient_id, T1.anchor_date;


--FINAL OUTPUT
DROP TABLE IF EXISTS kre_report.hiv_hrs_output_final;
CREATE TABLE kre_report.hiv_hrs_output_final AS
SELECT 
:site_abbr as site,
anchor_date_yr,
masked_patient_id,
age_on_anchor_date,
gender_male,
gender_female,
gender_trans,
gender_unk,
birth_sex_male,
birth_sex_female,
birth_sex_unk,
sexor_straight,
sexor_gay_lesbian,
sexor_bisexual,
sexor_other,
sexor_unk,
gender_iden_male,
gender_iden_female,
gender_iden_trans_male,
gender_iden_trans_female,
gender_iden_nonbinary,
gender_iden_other,
gender_iden_unk,
race_white,
race_black,
race_asian,
race_other,
race_unk,
ethnicity_hispanic,
ethnicity_nonhispanic,
ethnicity_unk,
sex_partner_male,
sex_partner_female,
sex_partner_mandf,
sex_partner_trans,
sex_partner_unk,
marital_stat_married,
marital_stat_partner,
marital_stat_single,
marital_stat_unk,
marital_stat_widow,
marital_stat_divorced,
marital_stat_other,
-- DO NOT INCLUDE IN OUTPUT
--insurance_status,
num_encs_1yr,
num_encs_1_to_2yr,
num_encs_gt_2yr,
gon_tests_1yr,
gon_tests_1_to_2yr,
gon_tests_gt_2yr,
gon_pos_tests_1yr,
gon_pos_tests_1_to_2yr,
gon_pos_tests_gt_2yr,
gon_tests_rectal_1yr,
gon_tests_rectal_1_to_2yr,
gon_tests_rectal_gt_2yr,
gon_tests_rectal_pos_1yr,
gon_tests_rectal_pos_1_to_2yr,
gon_tests_rectal_pos_gt_2yr,
gon_tests_oral_1yr,
gon_tests_oral_1_to_2yr,
gon_tests_oral_gt_2yr,
gon_tests_oral_pos_1yr,
gon_tests_oral_pos_1_to_2yr,
gon_tests_oral_pos_gt_2yr,
chlam_tests_1yr,
chlam_tests_1_to_2yr,
chlam_tests_gt_2yr,
chlam_pos_tests_1yr,
chlam_pos_tests_1_to_2yr,
chlam_pos_tests_gt_2yr,
chlam_tests_rectal_1yr,
chlam_tests_rectal_1_to_2yr,
chlam_tests_rectal_gt_2yr,
chlam_tests_rectal_pos_1yr,
chlam_tests_rectal_pos_1_to_2yr,
chlam_tests_rectal_pos_gt_2yr,
chlam_tests_oral_1yr,
chlam_tests_oral_1_to_2yr,
chlam_tests_oral_gt_2yr,
chlam_tests_oral_pos_1yr,
chlam_tests_oral_pos_1_to_2yr,
chlam_tests_oral_pos_gt_2yr,
syph_tests_1yr,
syph_tests_1_to_2yr,
syph_tests_gt_2yr,
syph_per_esp_1yr,
syph_per_esp_1_to_2yr,
syph_per_esp_gt_2yr,
hcv_ab_1yr,
hcv_ab_1_to_2yr,
hcv_ab_gt_2yr,
hcv_rna_1yr,
hcv_rna_1_to_2yr,
hcv_rna_gt_2yr,
hcv_ab_or_rna_pos_ev,
hcv_ab_pos_first_yr,
hcv_rna_pos_first_yr,
hcv_acute_per_esp,
hcv_acute_per_esp_case_yrs,
hbv_sag_1yr,
hbv_sag_1_to_2yr,
hbv_sag_gt_2yr,
hbv_dna_1yr,
hbv_dna_1_to_2yr,
hbv_dna_gt_2yr,
hbv_sag_or_dna_pos_ev,
hbv_sag_or_dna_pos_first_yr,
hbv_acute_per_esp,
hbv_acute_per_esp_case_yrs,
hbv_history_ev,
hiv_elisa_ag_ab_1yr,
hiv_elisa_ag_ab_1_to_2yr,
hiv_elisa_ag_ab_gt_2yr,
hiv_confirm_tests_1yr,
hiv_confirm_tests_1_to_2yr,
hiv_confirm_tests_gt_2yr,
hiv_rna_viral_1yr,
hiv_rna_viral_1_to_2yr,
hiv_rna_viral_gt_2yr,
hiv_max_viral_load_yr,
-- DO NOT INCLUDE IN THE OUTPUT
--hiv_status_per_esp,
--hiv_per_esp_case_yrs,
hiv_new_diag_final,
-- DO NOT INCLUDE IN THE OUTPUT
--hiv_neg_w_rna_test_ev,
hiv_neg_w_hiv_med_ev,
hiv_dx_1_yr,
hiv_dx_1_to_2yr,
hiv_dx_gt_2yr,
hiv_trmt_regimen_1yr,
hiv_trmt_regimen_1_to2yr,
hiv_trmt_regimen_gt_2yr,
pep_rx_dates_num,
prep_rx_num_1yr,
prep_rx_num_1_to_2yr,
prep_rx_num_gt_2yr,
dx_abn_anal_cyt_yrs,
dx_syph_yrs,
dx_anal_syph_yrs,
dx_gon_inf_rectum_yrs,
dx_gon_pharyn_yrs,
dx_chlam_inf_rectum_yrs,
dx_chlam_pharyn_yrs,
dx_lymph_ven_yrs,
dx_chancroid_yrs,
dx_granuloma_ingu_yrs,
dx_nongon_ureth_yrs,
dx_hsv_w_compl_yrs,
dx_genital_herpes_yrs,
dx_anogenital_warts_yrs,
dx_anorectal_ulcer_yrs,
dx_trich_yrs,
dx_mpox_yrs,
dx_unspec_std_yrs,
dx_pid_yrs,
dx_cont_w_exp_to_vd_yrs,
dx_high_risk_sex_behav_yrs,
dx_hiv_counsel_yrs,
dx_anorexia_yrs,
dx_bulimia_yrs,
dx_eat_disorder_nos_yrs,
dx_gend_iden_dis_yrs,
dx_transsexualism_yrs,
dx_couns_or_prb_child_sex_abuse_yrs,
dx_adult_child_sex_exp_or_abuse_yrs,
dx_adult_or_child_phys_abuse_yrs,
dx_foreign_body_anus_yrs,
dx_alcohol_depend_abuse_yrs,
dx_opioid_depend_abuse_yrs,
dx_sed_hypn_anxio_depend_abuse_yrs,
dx_cocaine_depend_abuse_yrs,
dx_amphet_stim_depend_abuse_yrs,
dx_oth_psycho_or_nos_subs_depend_abuse_yrs,
dx_sti_screen_yrs,
dx_malig_neo_rec_screen_yrs,
dx_needle_stick,
rx_bicillin_ever,
rx_azith_ever,
rx_ceft_ever,
rx_doxy_ever,
rx_methadone_ever,
rx_buprenorphine_ever,
rx_naltrexone_ever,
rx_viagara_cilais_or_levitra_ever,
vax_mpox_ever,
vax_hpv_male_ever
FROM kre_report.hiv_hrs_output_prep T1
INNER JOIN kre_report.hiv_hrs_masked_patients T2 ON (T1.patient_id = T2.patient_id and T1.anchor_date = T2.anchor_date);






-- delete from kre_report.hiv_hrs_masked_patients;

--nohup psql -d esp30 -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'ATR_14_730'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'BMC_14_730'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'CHA_14_730'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'FWY_14_730'" &

-- do this so you have a copy of the output
--create table kre_report.hiv_hrs_output_final_14_730 as SELECT * FROM kre_report.hiv_hrs_output_final;

-- select * from kre_report.hiv_hrs_output_final_14_730;
-- 2024-05-21-FWY-hiv-risk-score-data-14-730

-- get the stats and save output
-- select * from kre_report.hiv_hrs_stats_output;
--2024-05-21-FWY-hiv-risk-score-stats-14-730

-- MODIFY THE DATE RANGE IN THE SCRIPT


--nohup psql -d esp30 -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'ATR'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'BMC'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'CHA'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-V2.sql -v site_abbr="'FWY'" &


--create table kre_report.hiv_hrs_output_final_1_plus as SELECT * FROM kre_report.hiv_hrs_output_final;
--2024-05-21-FWY-hiv-risk-score-data-1-plus

-- get the stats and save output
-- select * from kre_report.hiv_hrs_stats_output;

--2024-05-21-FWY-hiv-risk-score-stats-1-plus

--STI-RISK
--nohup psql -d esp30 -f 2024-hiv-risk-model-dev-data-STI-RISK-version.sql -v site_abbr="'ATR_STI'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-STI-RISK-version.sql -v site_abbr="'BMC_STI'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-STI-RISK-version.sql -v site_abbr="'CHA_STI'" &
--nohup psql -d esp -f 2024-hiv-risk-model-dev-data-STI-RISK-version.sql -v site_abbr="'FWY_STI'" &



