--
-- ALL GONORRHEA/CHLAMYDIA LAB AND HEF EVENTS FOR POTENTIAL ANCHOR PATIENTS
-- NORMALIZE SPECIMEN SOURCES FOR CHLAM/GON TESTS
-- 
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_all_chlam_gon_labs_events;
CREATE TABLE kre_report.hiv_hrs_sti_risk_all_chlam_gon_labs_events AS 
SELECT T1.patient_id, T2.date as hef_date, T2.name as hef_name, 
CASE WHEN specimen_source ~* '(RECTUM|RECT|ANAL|ANUS)' then 'RECTAL'
WHEN specimen_source ~* '(PHARYNGEAL|THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA|URETHRA)' then 'UROG'
WHEN (native_name ~* '(PHARYNGEAL|THROAT)' or T1.native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (native_name ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR T1.native_code ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR procedure_name ~* '(RECTAL|ANAL|RECTUM|ANUS)') THEN 'RECTAL'
WHEN (native_name ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
                       OR T1.native_code  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
					   OR procedure_name  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' ) THEN 'UROG'
ELSE 'OTHER_OR_UNKNOWN' END specimen_source,
specimen_source specimen_source_orig,
T1.id as lab_id, T2.object_id, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id)
WHERE 
(T2.name ilike 'lx:gonorrhea%' or T2.name ilike 'lx:chlamydia%')
AND T2.date >= '2018-01-01'
-- EXCLUDE HIV PATIENTS
AND T1.patient_id not in (select patient_id from nodis_case where condition = 'hiv')
;


--VARIABLES IDENTIFIED AS HIGH RISK
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_all_chlam_gon_inclusion;
CREATE TABLE kre_report.hiv_hrs_sti_risk_all_chlam_gon_inclusion AS
SELECT patient_id, hef_date, 
MAX(CASE WHEN hef_name = 'lx:gonorrhea:positive' and specimen_source in ('RECTAL', 'THROAT') then 1 END) sti_risk_gon_pos_oral_rectal,
MAX(CASE WHEN hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' then 1 END)  sti_risk_chlam_pos_rectal
FROM kre_report.hiv_hrs_sti_risk_all_chlam_gon_labs_events
WHERE 
(hef_name = 'lx:gonorrhea:positive' and specimen_source in ('RECTAL', 'THROAT') )
OR
(hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL')
GROUP BY patient_id, hef_date;

--FOR EACH PATIENT IDENTIFY EARLIEST DATE OF HIGH RISK VARIABLE
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_all_chlam_gon_pats;
CREATE TABLE  kre_report.hiv_hrs_sti_risk_all_chlam_gon_pats AS
SELECT patient_id, min(hef_date) as min_qual_date
FROM kre_report.hiv_hrs_sti_risk_all_chlam_gon_inclusion
GROUP BY patient_id;

-- GET DATE OF FIRST SYPHILIS CASE
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_syph_pats;
CREATE TABLE kre_report.hiv_hrs_sti_risk_syph_pats AS 
SELECT patient_id, min(date) as min_qual_date, 1 sti_risk_syphilis
FROM nodis_case
WHERE condition = ('syphilis')
AND date >= '2018-01-01'
-- EXCLUDE HIV PATIENTS
AND patient_id not in (select patient_id from nodis_case where condition = 'hiv')
GROUP BY patient_id;


/* DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs;
CREATE TABLE kre_report.hiv_hrs_sti_risk_hiv_labs AS 
SELECT T1.patient_id, T1.date, T2.test_name
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE test_name in ('hiv_elisa', 'hiv_ag_ab')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
--AND date >= '2015-01-01'
-- EXCLUDE HIV PATS
AND T1.patient_id not in (select patient_id from nodis_case where condition = 'hiv')
GROUP BY T1.patient_id, T1.date, T2.test_name
ORDER BY patient_id, date;

-- GET THE DATE OF THE HIV LAB BEFORE THE ONE BEING EVALUATED
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs_date_diffs;
CREATE TABLE kre_report.hiv_hrs_sti_risk_hiv_labs_date_diffs AS
SELECT patient_id, date, lead(date) OVER (PARTITION BY patient_id) as lab_after, lag(date) OVER (PARTITION BY patient_id) as lab_before
FROM kre_report.hiv_hrs_sti_risk_hiv_labs
--where patient_id in (44268600,146688, 7989553)
ORDER BY patient_id, date;

-- FIND PATS WITH 2 LABS IN 2 YEARS
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs_2_in_last_2yrs;
CREATE TABLE kre_report.hiv_hrs_sti_risk_hiv_labs_2_in_last_2yrs AS
select patient_id, date,  lab_before, date - lab_before days_between_labs
from kre_report.hiv_hrs_sti_risk_hiv_labs_date_diffs
WHERE date - lab_before <= 730;

-- GET DATE OF SECOND HIV LAB THAT MEETS THE CRITERIA
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_lab_pats;
CREATE TABLE kre_report.hiv_hrs_sti_risk_hiv_lab_pats AS 
SELECT patient_id, min(date) as min_qual_date, 1 sti_risk_2hiv_labs
FROM kre_report.hiv_hrs_sti_risk_hiv_labs_2_in_last_2yrs
--where patient_id in (44268600,146688, 7989553)
GROUP BY patient_id; */

-- MPOX POS PATIENTS
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_mpox_pats;
CREATE TABLE kre_report.hiv_hrs_sti_risk_mpox_pats AS 
SELECT patient_id, min(date) as min_qual_date, 1 sti_risk_mpox_pos
FROM emr_labresult
WHERE (native_name ilike '%mpox%' OR native_name ilike '%monkeypox%')
AND (result_string ilike 'DETECTED%' OR result_string ilike 'POSITIVE&')
AND date >= '2018-01-01'
-- EXCLUDE HIV PATS
AND patient_id not in (select patient_id from nodis_case where condition = 'hiv')
GROUP BY patient_id;


/* --HIGH RISK SEX ICD PATS
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_icd_pats;
CREATE TABLE kre_report.hiv_hrs_sti_risk_icd_pats AS
SELECT patient_id, min(date) as min_qual_date, 1 sti_risk_high_risk_sex_icd 
FROM emr_encounter T1
INNER JOIN emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
WHERE (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) 
--AND date >= '2015-01-01'
-- EXCLUDE HIV PATS
AND patient_id not in (select patient_id from nodis_case where condition = 'hiv')
GROUP BY patient_id; */

/* --PrEP Rx
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_prep_pats;
CREATE TABLE kre_report.hiv_hrs_sti_risk_prep_pats AS 
SELECT patient_id, min(date) as min_qual_date, 1 sti_risk_prep
FROM hef_event
WHERE 
name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
					  'rx:hiv_cabotegravir_er_600')
AND patient_id not in (select patient_id from nodis_case where condition = 'hiv')			
GROUP BY patient_id; */


--BRING ALL PATIENTS AND DATES TOGETHER
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_all_pats_combo;
CREATE TABLE kre_report.hiv_hrs_sti_risk_all_pats_combo AS
SELECT patient_id, min_qual_date FROM kre_report.hiv_hrs_sti_risk_all_chlam_gon_pats
UNION
SELECT patient_id, min_qual_date FROM kre_report.hiv_hrs_sti_risk_syph_pats
UNION
SELECT patient_id, min_qual_date FROM kre_report.hiv_hrs_sti_risk_mpox_pats;
-- UNION
-- SELECT patient_id, min_qual_date FROM kre_report.hiv_hrs_sti_risk_prep_pats
-- GROUP BY patient_id, min_qual_date;

--DETERMINE EARLIEST QUAL DATE FOR EACH PATIENT
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_qual_dates;
CREATE TABLE kre_report.hiv_hrs_sti_risk_qual_dates AS
SELECT patient_id, min(min_qual_date) min_qual_date
FROM kre_report.hiv_hrs_sti_risk_all_pats_combo
GROUP BY patient_id;


-- IDENTIFY WHICH VARIABLE(S) IS PRIMARY ON MIN QUAL DATE (1)
-- WHICH ARE THERE IN THE FUTURE (2)
-- WHICH PATIENT DOES NOT HAVE (3)
-- 0 WILL BE FOR NON-STI RISK PATIENTS?
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_variable_matches;
CREATE TABLE kre_report.hiv_hrs_sti_risk_variable_matches AS 
SELECT T1.patient_id, T1.min_qual_date,
MIN(CASE WHEN hef_date = T1.min_qual_date and sti_risk_gon_pos_oral_rectal =1 then 1
   WHEN hef_date != T1.min_qual_date and sti_risk_gon_pos_oral_rectal = 1 then 2 
   ELSE 3	
   END) as sti_risk_gon_pos_oral_rectal,
MIN(CASE WHEN hef_date = T1.min_qual_date and sti_risk_chlam_pos_rectal = 1 then 1
     WHEN hef_date != T1.min_qual_date and sti_risk_chlam_pos_rectal = 1 then 2
	 ELSE 3
	 END) as sti_risk_chlam_pos_rectal,
MIN(CASE WHEN T3.min_qual_date = T1.min_qual_date and sti_risk_syphilis = 1 then 1
     WHEN T3.min_qual_date != T1.min_qual_date and sti_risk_syphilis = 1 then 2
	 ELSE 3
	 END) as sti_risk_syphilis,
MIN(CASE WHEN T5.min_qual_date = T1.min_qual_date and sti_risk_mpox_pos = 1 then 1
     WHEN T5.min_qual_date != T1.min_qual_date and sti_risk_mpox_pos = 1 then 2
	 ELSE 3
	 END) as sti_risk_mpox_pos
/* 	 ,
MIN(CASE WHEN T6.min_qual_date = T1.min_qual_date and sti_risk_prep  = 1 then 1
     WHEN T6.min_qual_date != T1.min_qual_date and sti_risk_prep  = 1 then 2
	 ELSE 3
	 END) as sti_risk_prep  */
FROM kre_report.hiv_hrs_sti_risk_qual_dates T1
LEFT JOIN kre_report.hiv_hrs_sti_risk_all_chlam_gon_inclusion T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.hiv_hrs_sti_risk_syph_pats T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN kre_report.hiv_hrs_sti_risk_mpox_pats T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN kre_report.hiv_hrs_sti_risk_prep_pats T6 ON (T1.patient_id = T6.patient_id)
GROUP BY T1.patient_id, T1.min_qual_date;


-- FIND CLIN ENC PRIOR TO QUAL DATE
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_anchor_dates;
CREATE TABLE kre_report.hiv_hrs_sti_risk_anchor_dates AS 
SELECT T1.patient_id, max(date) anchor_date, min_qual_date - max(date) as date_diff, 1 as sti_risk_pat,
     CASE WHEN upper(T3.gender) in ('M', 'MALE') then 'M'
     WHEN upper(T3.gender) in ('F', 'FEMALE') then 'F' 
     WHEN upper(T3.gender) in ('T') then 'T'
	 WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('FEMALE') then 'F'
	 WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('MALE') then 'M'
	 --TREAT EVERYONE ELSE AS MALE
     WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') or T3.gender is null then 'M'
	 ELSE 'M' END as gender_mapped 
	FROM kre_report.hiv_hrs_sti_risk_qual_dates T1
	LEFT JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
	JOIN emr_patient T3 ON (T1.patient_id = T3.id)
	--WHERE date < max_lab_pos_date
	-- date range for alternate run
	WHERE date <= min_qual_date - INTERVAL '14 days'
	AND date >= min_qual_date - INTERVAL '730 days'
	AND source = 'enc'
	GROUP BY T1.patient_id, min_qual_date, gender_mapped;
	

-- OLD TABLES TO DROP
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs;
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs_date_diffs;
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_labs_2_in_last_2yrs;
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_hiv_lab_pats;
DROP TABLE IF EXISTS kre_report.hiv_hrs_sti_risk_icd_pats;





