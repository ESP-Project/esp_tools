drop table if exists kre_report.hiv_hrs_stats_anchor_months;
create table kre_report.hiv_hrs_stats_anchor_months AS
select count(*), to_char(anchor_date, 'YYYY-MM') as anchor_month,
gender_mapped as sex
from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE date_part('year', age(anchor_date, date_of_birth)) >= 15
group by anchor_month, sex;

-- total patients (clinical encounter of type enc in the month)
drop table if exists kre_report.hiv_hrs_stats_total_vis_pats;
create table kre_report.hiv_hrs_stats_total_vis_pats AS
select count(distinct(patient_id)) total_visit_patients, to_char(T1.date, 'YYYY-MM') anchor_month,
CASE WHEN upper(T3.gender) in ('M', 'MALE') then 'M'
WHEN upper(T3.gender) in ('F', 'FEMALE') then 'F' 
WHEN upper(T3.gender) in ('T') then 'T'
WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('FEMALE') then 'F'
WHEN upper(T3.gender) not in ('M', 'MALE', 'F', 'FEMALE', 'T') and upper(birth_sex) in ('MALE') then 'M'
ELSE 'M' END as sex
from gen_pop_tools.clin_enc T1
JOIN emr_patient T3 ON (T1.patient_id = T3.id)
WHERE source = 'enc'
AND to_char(T1.date, 'YYYY-MM') in (SELECT anchor_month from kre_report.hiv_hrs_stats_anchor_months)
GROUP BY anchor_month, sex;


-- eligible patients (patients >= 15 on anchor date and no HIV case past or present)
-- does not include hiv new diag pats or sti patients
-- ONLY INCLUDE PATIENTS WITH MATCHING GENDER
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_eligible_pats;
CREATE TABLE kre_report.hiv_hrs_stats_eligible_pats AS
select count(distinct(patient_id)) eligible_patients, anchor_month,
gender_mapped as sex
FROM kre_report.hiv_hrs_index_patients T1
INNER JOIN kre_report.hiv_hrs_stats_anchor_months T2 ON (to_char(T1.anchor_date, 'YYYY-MM') = T2.anchor_month)
-- don't count new hiv diag patients or sti patients
WHERE T1.patient_id not in (select patient_id from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats)
GROUP BY anchor_month, gender_mapped;


-- control patients (patients with 1 or more non-demographic variable)
-- does not include hiv new diag pats
-- ONLY INCLUDE PATIENTS WITH MATCHING GENDER
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_control_pats;
CREATE TABLE kre_report.hiv_hrs_stats_control_pats AS
select count(distinct(T1.patient_id)) control_patients, (to_char(T1.anchor_date, 'YYYY-MM')) as anchor_month,
gender_mapped as sex
FROM kre_report.hiv_hrs_index_patients_w_voi T1
JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.patient_id not in (select patient_id from kre_report.hiv_hrs_pats_anchor_sti_hiv_pats)
GROUP BY anchor_month, sex;


-- control pats sampled per month
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_control_pats_sampled;
CREATE TABLE kre_report.hiv_hrs_stats_control_pats_sampled AS
SELECT record_count as control_pats_sampled, anchor_month, gender_mapped as sex
FROM (
select count(to_char(T2.anchor_date, 'YYYY-MM')) as record_count, T1.anchor_month, num_control_pats, T1.gender_mapped 
FROM kre_report.hiv_hrs_num_ctrl_pats T1
LEFT JOIN kre_report.hiv_hrs_control_patients_final T2 ON (to_char(T2.anchor_date, 'YYYY-MM') = T1.anchor_month and T1.gender_mapped = T2.gender_mapped)
GROUP BY T1.anchor_month, num_control_pats, T1.gender_mapped) T1;


-- hiv new diag pats and sti pats per month
DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_new_diag_pats;
CREATE TABLE kre_report.hiv_hrs_stats_new_diag_pats AS
select hiv_new_diag_pat_count, anchor_month,
gender_mapped as sex
FROM kre_report.hiv_hrs_num_ctrl_pats;

DROP TABLE IF EXISTS kre_report.hiv_hrs_stats_output;
CREATE TABLE kre_report.hiv_hrs_stats_output AS
SELECT T1.anchor_month, T1.sex,
total_visit_patients,
coalesce(eligible_patients, 0) eligible_patients,
coalesce(control_patients, 0) control_patients,
control_pats_sampled,
hiv_new_diag_pat_count as case_patient_count
FROM kre_report.hiv_hrs_stats_anchor_months T1
LEFT JOIN kre_report.hiv_hrs_stats_total_vis_pats T2 ON (T1.anchor_month = T2.anchor_month and T1.sex = T2.sex)
LEFT JOIN kre_report.hiv_hrs_stats_eligible_pats T3 ON (T1.anchor_month = T3.anchor_month and T1.sex = T3.sex)
LEFT JOIN kre_report.hiv_hrs_stats_control_pats T4 ON (T1.anchor_month = T4.anchor_month and T1.sex = T4.sex)
LEFT JOIN kre_report.hiv_hrs_stats_control_pats_sampled T5 ON (T1.anchor_month = T5.anchor_month and T1.sex = T5.sex)
LEFT JOIN kre_report.hiv_hrs_stats_new_diag_pats T6 ON (T1.anchor_month = T6.anchor_month and T1.sex = T6.sex)
ORDER BY T1.anchor_month, T1.sex;







