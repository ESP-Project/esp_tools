drop table if exists hiv_rpt.unmapped_hiv_sti_risk_meds;
create table hiv_rpt.unmapped_hiv_sti_risk_meds as 
select DISTINCT count(*), name, dose, quantity, quantity_float, directions, max(date) most_recent_rx_date
from emr_prescription
where (name ilike '%BICILLIN%'
or name ilike '%PEN G%'
or name ilike '%PENICILLIN G%'
or name ilike '%AZITHROMYCIN%'
or name ilike '%ZITHROMAX%'
or name ilike '%CEFTRIAXONE%'
or name ilike '%ROCEPHIN%'
or name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%'
)
AND name not in (select name from hiv_risk_bicillin_meds)
AND name not in (select name from hiv_sti_risk_azith_meds)
AND name not in (select name from hiv_sti_risk_ceft_meds)
AND name not in (select name from hiv_sti_risk_doxy_meds)
group by name, dose, quantity, quantity_float, directions
order by name, dose, quantity, quantity_float, directions;




/* Bicillin 2.4 MU injection ever  
Azithromycin 1g PO ever  
Ceftriaxone 125mg or 250mg or 500mg IV/IM ever 
Doxycycline 100mg PO bid for ≥14 days or Doxycycline 100mg PO x 2 tabs (dispense ≥30 tabs) ever  [Counting matching 100mg rx with quantity >= 28] */


