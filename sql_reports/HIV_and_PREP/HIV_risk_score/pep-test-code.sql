

'COMBIVIR PEP KIT'
'ISENTRESS PEP KIT'
'KALETRA PEP KIT'
'TIVICAY PEP KIT'
'TRUVADA PEP KIT'

https://stacks.cdc.gov/view/cdc/38856


tenofovir DF 300 mg and fixed dose combination
emtricitabine 200 mg (Truvadac) once daily
with
raltegravir 400 mg twice daily
or
dolutegravir 50 mg once daily


'rx:hiv_tenofovir-emtricitabine:generic', 
'rx:hiv_tenofovir-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine:generic'


'rx:hiv_raltegravir'
OR
'rx:hiv_dolutegravir'



DROP TABLE IF EXISTS kre_report.hiv_hrs_pep_meds;
CREATE TABLE kre_report.hiv_hrs_pep_meds as
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_tenofovir-emtricitabine:generic', 
                            'rx:hiv_tenofovir-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine', 
                            'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') 
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= T2.anchor_date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T2.anchor_date = T3.anchor_date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_raltegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name = 'rx:hiv_dolutegravir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in ('rx:hiv_ritonavir') 
			AND T1.date <= T2.anchor_date) T3 ON (T1.patient_id = T3.patient_id and T1.date = T3.date and T2.anchor_date = T3.anchor_date)
WHERE T1.name = 'rx:hiv_darunavir'
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN (SELECT T1.patient_id, date, anchor_date, name
            FROM hef_event T1
			INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
            WHERE name in (
                           'rx:hiv_zidovudine-lamivudine', 
                           'rx:hiv_zidovudine-lamivudine:generic')
			AND T1.date <= T2.anchor_date) T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
WHERE T1.name in ('rx:hiv_lopinavir-ritonavir', 'rx:hiv_lopinavir-ritonavir:generic')
UNION
SELECT T1.patient_id, T1.date as pep_date, T2.anchor_date
FROM hef_event T1
INNER JOIN kre_report.hiv_hrs_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE name in ('bictegravir-emtricitabine-tenofovir_alafenamide', 'bictegravir-emtricitabine-tenofovir_alafenamide:generic')
AND T1.date <= T2.anchor_date
GROUP BY T1.patient_id, pep_date, anchor_date;

-- EXCLUDE PATIENTS THAT HAVE HAD AN HIV DX PRIOR TO 

select T1.*, T2.date, T2.name, T2.patient_id
FROM kre_report.hiv_hrs_pep_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE name = 'dx:hiv'
AND T2.date <= T1.pep_date
AND T2.date >= '2015-01-01'




DROP TABLE IF EXISTS kre_report.hiv_hrs_hiv_meds;
CREATE TABLE kre_report.hiv_hrs_hiv_meds as
SELECT T1.patient_id, T1.anchor_date, T2.name, T3.name as hef_name, T2.date, T2.quantity, T2.quantity_float, T2.refills, T2.start_date, T2.end_date, 
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1) ELSE 1 END refills_mod,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null then quantity_float end as total_quantity_per_rx,
-- --Default to 30 for missing quantity values for Truvada/Descovy
-- CASE WHEN refills~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN ((refills::real +1) * 30)
     -- WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
	 -- WHEN refills!~E'^\\d+$' AND (quantity_float is null or quantity_float = 0) THEN 30
	 -- WHEN refills is null AND (quantity_float is null or quantity_float = 0) THEN 30
     -- WHEN refills is null then quantity_float end as total_prep_oral_quantity_per_rx,
--given every 8 weeks so each prescription for cabotegravir should count as 2 prescriptions
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * 2) 
     WHEN refills!~E'^\\d+$' or refills is null THEN 2
	 ELSE 2 end as num_monthly_prep_inj_rx,
CASE WHEN T3.name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic') THEN 1 ELSE 0 END as rx_oral_prep,
CASE WHEN T3.name in ('rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END as rx_inj_prep,
CASE WHEN T3.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
                      'rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END other_hiv_rx_non_prep,
string_to_array(replace(split_part(T3.name, ':', 2), 'hiv_',''), '-') med_array,
CASE WHEN T3.name in ('rx:hiv_dolutegravir-lamivudine', 
                      'rx:hiv_dolutegravir-lamivudine:generic',
					  'rx:hiv_dolutegravir-rilpivirine',
                      'rx:hiv_dolutegravir-rilpivirine:generic',
					  'rx:hiv_cabotegravir-rilpivirine',
                      'rx:hiv_cabotegravir-rilpivirine:generic') THEN 1 END hiv_trmt_reg
FROM kre_report.hiv_hrs_index_patients T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
JOIN hef_event T3 on (T2.patient_id = T3.patient_id and T2.id = T3.object_id)
WHERE T3.name ilike 'rx:hiv%'
AND T2.date <= T1.anchor_date;

