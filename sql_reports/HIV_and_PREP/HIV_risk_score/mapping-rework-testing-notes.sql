-- check for unmapped race values
select distinct(upper(race))
from emr_patient
where upper(race) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'race')

-- check for unmapped ethnicity
select distinct(upper(ethnicity))
from emr_patient
where upper(ethnicity) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field = 'ethnicity')



CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'Unspecified') 
						  or race is null then 1 ELSE 0 END as race_other



CASE when upper(race) in ('HISPANIC','HISP', 'HISPANIC/LAT') or upper(ethnicity) in ('HISPANIC OR LATINO', 'HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN') then 1 ELSE 0 END as ethnicity_hispanic,


CASE when upper(race) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC') 
						  then 1 ELSE 0 END as ethnicity_hispanic


CASE when (upper(race) not in ('HISPANIC','HISP', 'HISPANIC/LAT') and (upper(ethnicity) not in ('HISPANIC OR LATINO', 'HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'NOT HISPANIC OR LATINO', 'NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN') ) or ethnicity is null) then 1 ELSE 0 end as ethnicity_unk,

CASE when (upper(race) not in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'race' and mapped_value = 'HISPANIC') 
			AND
			(upper(ethnicity) in (select distinct(src_value) from gen_pop_tools.rs_conf_mapping 
                          where src_table = 'emr_patient' and src_field = 'ethnicity' and mapped_value = 'Unspecified') 
						  OR ethnicity is null)
		  )
						  
	        then 1 ELSE 0 END as ethnicity_unk,


No Selection,ethnicity
Non-Hispanic,ethnicity
Not Hispanic,ethnicity
Not Hispanic or Latino,ethnicity
Not Reported,ethnicity
OTHER OR UNDETERMINED,ethnicity
Patient Declined,ethnicity
U,ethnicity
,ethnicity
,primary_payer
American Indian or Alaska Native,race
Black,race
Black or African American,race
Hispanic,race
Multiracial,race
Native Hawaiian or Other Pacific Islander,race
Other,race
Patient Declined,race
Unknown,race
,race
NOT ASKED,tobacco_use
,tobacco_use











