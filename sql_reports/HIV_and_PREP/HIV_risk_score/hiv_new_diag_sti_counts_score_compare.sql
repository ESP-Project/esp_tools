-- new hiv_diag_pat cases
DROP TABLE IF EXISTS kre_report.temp_hiv_new_diag;
CREATE TABLE kre_report.temp_hiv_new_diag as 
SELECT patient_id, id as case_id, date as case_date
FROM nodis_case
WHERE patient_id in (SELECT patient_id FROM advp.hiv_hiv_new_diag_pats)
AND condition = 'hiv';



--19
DROP TABLE IF EXISTS kre_report.temp_sti_before_hiv_new_diag;
CREATE TABLE kre_report.temp_sti_before_hiv_new_diag as 
SELECT DISTINCT T1.patient_id,
case_date as hiv_case_date--, T2.date as sti_date, T2.condition
FROM kre_report.temp_hiv_new_diag T1
INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.condition in ('chlamydia', 'gonorrhea', 'syphilis')
AND T2.date < T1.case_date
ORDER BY patient_id;



-- years for patients with no sti
DROP TABLE IF EXISTS kre_report.temp_no_sti_before_hiv_counts;
CREATE TABLE kre_report.temp_no_sti_before_hiv_counts AS
SELECT count(*) hiv_no_sti_pats, extract(year FROM case_date) hiv_case_year
FROM kre_report.temp_hiv_new_diag
WHERE patient_id not in (SELECT patient_id FROM  kre_report.temp_sti_before_hiv_new_diag)
GROUP BY hiv_case_year
ORDER BY hiv_case_year;


-- years for patients with sti
DROP TABLE IF EXISTS kre_report.temp_hiv_with_prior_sti_counts;
CREATE TABLE kre_report.temp_hiv_with_prior_sti_counts AS
SELECT count(*) hiv_prior_sti_pats, extract(year FROM hiv_case_date) hiv_case_year
FROM kre_report.temp_sti_before_hiv_new_diag
GROUP BY hiv_case_year
ORDER BY hiv_case_year;



/* SELECT T1.hiv_case_year, 
hiv_no_sti_pats,
coalesce(hiv_prior_sti_pats, 0) as hiv_prior_sti_pats,
coalesce(round(((hiv_prior_sti_pats::numeric / (hiv_no_sti_pats + hiv_prior_sti_pats)::numeric)), 2), 0) as pct_sti
FROM kre_report.temp_no_sti_before_hiv_counts T1
LEFT JOIN kre_report.temp_hiv_with_prior_sti_counts T2 ON (T1.hiv_case_year = T2.hiv_case_year); */


DROP TABLE IF EXISTS kre_report.temp_hiv_no_sti_case_dates;
CREATE TABLE kre_report.temp_hiv_no_sti_case_dates AS
SELECT patient_id, case_date, extract(year from case_date)::int as case_year
FROM kre_report.temp_hiv_new_diag T1
WHERE patient_id not in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag);

DROP TABLE IF EXISTS kre_report.temp_hiv_score_year_before_hiv;
CREATE TABLE kre_report.temp_hiv_score_year_before_hiv AS
SELECT T1.patient_id, case_date, hiv_score_yr_before_hiv, T1.hiv_risk_score
FROM gen_pop_tools.cc_hiv_risk_score T1
JOIN (	
	SELECT T1.patient_id, max(rpt_year) hiv_score_yr_before_hiv, case_date
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id not in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	GROUP BY T1.patient_id, case_date) T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.rpt_year = hiv_score_yr_before_hiv
ORDER BY hiv_score_yr_before_hiv;

DROP TABLE IF EXISTS kre_report.temp_hiv_score_year_before_hiv_with_sti;
CREATE TABLE kre_report.temp_hiv_score_year_before_hiv_with_sti AS
SELECT T1.patient_id, case_date, hiv_score_yr_before_hiv_sti_pats, T1.hiv_risk_score
FROM gen_pop_tools.cc_hiv_risk_score T1
JOIN (	
	SELECT T1.patient_id, max(rpt_year) hiv_score_yr_before_hiv_sti_pats, case_date
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	GROUP BY T1.patient_id, case_date) T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.rpt_year = hiv_score_yr_before_hiv_sti_pats
ORDER BY hiv_score_yr_before_hiv_sti_pats;

DROP TABLE IF EXISTS kre_report.temp_no_sti_score_counts;
CREATE TABLE kre_report.temp_no_sti_score_counts AS
select extract(year from case_date) hiv_case_year, 
count(*) no_sti_pats_w_score_py,
count(CASE WHEN hiv_risk_score >= .02 THEN 1 END) no_sti_pats_w_score_gte_02_py,
count(CASE WHEN hiv_risk_score >= .01 THEN 1 END ) no_sti_pats_w_score_gte_01_py
FROM kre_report.temp_hiv_score_year_before_hiv
GROUP BY hiv_case_year
ORDER BY hiv_case_year;

DROP TABLE IF EXISTS kre_report.temp_with_sti_score_counts;
CREATE TABLE kre_report.temp_with_sti_score_counts AS
select extract(year from case_date) hiv_case_year, 
count(*) with_sti_pats_w_score_py,
count(CASE WHEN hiv_risk_score >= .02 THEN 1 END) sti_pats_w_score_gte_02_py,
count(CASE WHEN hiv_risk_score >= .01 THEN 1 END ) sti_pats_w_score_gte_01_py
FROM kre_report.temp_hiv_score_year_before_hiv_with_sti
GROUP BY hiv_case_year
ORDER BY hiv_case_year;

-- PATS WITH RISK SCORE >=.02 EVER PRIOR TO HIV

DROP TABLE IF EXISTS kre_report.temp_with_sti_score_ever_02_counts;
CREATE TABLE kre_report.temp_with_sti_score_ever_02_counts AS
SELECT count(distinct(patient_id)) sti_pats_w_score_gte_02_ever_prior, extract(year from case_date)hiv_case_year
FROM (SELECT T1.patient_id, rpt_year, case_date, hiv_risk_score
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	AND hiv_risk_score >= .02) T1
GROUP BY hiv_case_year;

DROP TABLE IF EXISTS kre_report.temp_no_sti_score_ever_02_counts;
CREATE TABLE kre_report.temp_no_sti_score_ever_02_counts AS
SELECT count(distinct(patient_id)) no_sti_pats_w_score_gte_02_ever_prior, extract(year from case_date)hiv_case_year
FROM (SELECT T1.patient_id, rpt_year, case_date, hiv_risk_score
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id not in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	AND hiv_risk_score >= .02) T1
GROUP BY hiv_case_year;

DROP TABLE IF EXISTS kre_report.temp_with_sti_score_ever_01_counts;
CREATE TABLE kre_report.temp_with_sti_score_ever_01_counts AS
SELECT count(distinct(patient_id)) sti_pats_w_score_gte_01_ever_prior, extract(year from case_date)hiv_case_year
FROM (SELECT T1.patient_id, rpt_year, case_date, hiv_risk_score
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	AND hiv_risk_score >= .01) T1
GROUP BY hiv_case_year;

DROP TABLE IF EXISTS kre_report.temp_no_sti_score_ever_01_counts;
CREATE TABLE kre_report.temp_no_sti_score_ever_01_counts AS
SELECT count(distinct(patient_id)) no_sti_pats_w_score_gte_01_ever_prior, extract(year from case_date)hiv_case_year
FROM (SELECT T1.patient_id, rpt_year, case_date, hiv_risk_score
	FROM kre_report.temp_hiv_new_diag T1
	JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.patient_id not in (SELECT patient_id FROM kre_report.temp_sti_before_hiv_new_diag)
	AND hiv_risk_score >= .01) T1
GROUP BY hiv_case_year;






--OUTPUT
SELECT T1.hiv_case_year, 
hiv_no_sti_pats,
coalesce(hiv_prior_sti_pats, 0) as hiv_prior_sti_pats,
coalesce(round(((hiv_prior_sti_pats::numeric / (hiv_no_sti_pats + hiv_prior_sti_pats)::numeric)), 2), 0) as pct_sti,
coalesce(no_sti_pats_w_score_py, 0) no_sti_pats_w_score_py,
coalesce(no_sti_pats_w_score_gte_02_py, 0) no_sti_pats_w_score_gte_02_py,
coalesce(no_sti_pats_w_score_gte_01_py, 0) no_sti_pats_w_score_gte_01_py,
coalesce(no_sti_pats_w_score_gte_02_ever_prior, 0) no_sti_pats_w_score_gte_02_ever_prior,
coalesce(no_sti_pats_w_score_gte_01_ever_prior, 0) no_sti_pats_w_score_gte_01_ever_prior,
coalesce(with_sti_pats_w_score_py, 0) with_sti_pats_w_score_py,
coalesce(sti_pats_w_score_gte_02_py, 0) sti_pats_w_score_gte_02_py,
coalesce(sti_pats_w_score_gte_01_py, 0) sti_pats_w_score_gte_01_py,
coalesce(sti_pats_w_score_gte_02_ever_prior, 0) sti_pats_w_score_gte_02_ever_prior,
coalesce(sti_pats_w_score_gte_01_ever_prior, 0) sti_pats_w_score_gte_01_ever_prior
FROM kre_report.temp_no_sti_before_hiv_counts T1
LEFT JOIN kre_report.temp_hiv_with_prior_sti_counts T2 ON (T1.hiv_case_year = T2.hiv_case_year)
LEFT JOIN kre_report.temp_no_sti_score_counts T3 ON (T1.hiv_case_year = T3.hiv_case_year)
LEFT JOIN kre_report.temp_with_sti_score_counts T4 ON (T1.hiv_case_year = T4.hiv_case_year)
LEFT JOIN kre_report.temp_with_sti_score_ever_02_counts T5 ON (T1.hiv_case_year = T5.hiv_case_year)
LEFT JOIN kre_report.temp_no_sti_score_ever_02_counts T6 ON (T1.hiv_case_year = T6.hiv_case_year)
LEFT JOIN kre_report.temp_with_sti_score_ever_01_counts T7 ON (T1.hiv_case_year = T7.hiv_case_year)
LEFT JOIN kre_report.temp_no_sti_score_ever_01_counts T8 ON (T1.hiv_case_year = T8.hiv_case_year)
ORDER BY T1.hiv_case_year;


--DROP TABLE IF EXISTS kre_report.temp_hiv_new_diag;
--DROP TABLE IF EXISTS kre_report.temp_sti_before_hiv_new_diag;
--DROP TABLE IF EXISTS kre_report.temp_no_sti_before_hiv_counts;
--DROP TABLE IF EXISTS kre_report.temp_hiv_with_prior_sti_counts;



--nohup psql -d esp -f hiv-risk-prediction-historical.sql -v end_calc_year="'2012-12-31'" &








