1. Create the function from the hiv_risk_score_calc_for_exact_date-FUNCTION.sql file


2. Create a table based on values supplied by Susan


-- Table: kre_report.hiv_hrs_raw_data_to_score

-- DROP TABLE IF EXISTS kre_report.hiv_hrs_raw_data_to_score;

CREATE TABLE IF NOT EXISTS kre_report.hiv_hrs_raw_data_to_score
(
    row_num character varying COLLATE pg_catalog."default",
	site character varying COLLATE pg_catalog."default",
    anchor_date_yr character varying COLLATE pg_catalog."default",
    masked_patient_id character varying COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT hiv_hrs_raw_data_to_score_pkey PRIMARY KEY (masked_patient_id)
)


3. Create the table to save the risk scores

CREATE TABLE IF NOT EXISTS kre_report.hiv_hrs_manual_risk_score
(
    patient_id integer NOT NULL,
    masked_patient_id character varying COLLATE pg_catalog."default" NOT NULL,
    score_date date NOT NULL,
    hiv_risk_score numeric,
    CONSTRAINT hiv_hrs_manual_risk_score_pkey PRIMARY KEY (patient_id)
)


4. Load the raw data from the file

psql
\COPY kre_report.hiv_hrs_raw_data_to_score("row_num", "site", "anchor_date_yr", "masked_patient_id") FROM '/tmp/raw_data_to_score.csv' WITH DELIMITER ',' CSV HEADER;


5. Identify specific site patients and get actual patient_id and anchor_date from existing tables

DROP TABLE IF EXISTS kre_report.hiv_hrs_scores_to_compute;
CREATE TABLE kre_report.hiv_hrs_scores_to_compute AS 
SELECT patient_id, T1.masked_patient_id, anchor_date, (anchor_date - INTERVAL '1 year')::date as score_date
FROM kre_report.hiv_hrs_raw_data_to_score T1
JOIN kre_report.hiv_hrs_masked_patients T2 ON (T1.masked_patient_id = T2.masked_patient_id)
WHERE site ilike '%CHA%';

6. Create the SQL statements to run

DROP TABLE IF EXISTS kre_report.hiv_hrs_score_to_compute_sql;
CREATE TABLE kre_report.hiv_hrs_score_to_compute_sql AS
SELECT CONCAT('INSERT INTO kre_report.hiv_hrs_manual_risk_score SELECT ', patient_id, ', ''', masked_patient_id, ''', ''', score_date, 
			  ''', hiv_rpt.hiv_risk_score_calc_for_exact_date(', patient_id, ', ''', score_date, ''');') 
FROM kre_report.hiv_hrs_scores_to_compute;

7. Create the file
psql
\o /tmp/kre_test.txt
select * from kre_report.hiv_hrs_score_to_compute_sql;

8. Fix up the file to make it pure SQL
vi /tmp/kre_test.txt
remove header and final lines of file
check file count to make sure it lines up

9. Drop previously computed/test scores

delete from kre_report.hiv_hrs_manual_risk_score;

10. Run the SQL to create the scores
cd /tmp
nohup psql -d esp -f kre_test.txt &


11. Review the output
select * from kre_report.hiv_hrs_manual_risk_score;


12. Format the output and email results file

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/CHA-risk_scores-for-Susan.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA-risk_scores-for-Susan.csv -s "CHA Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/FWY-risk_scores-for-Susan.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n FWY-risk_scores-for-Susan.csv -s "FWY Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/BMC-risk_scores-for-Susan.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r keberhardt@commoninf.com -p /tmp -n BMC-risk_scores-for-Susan.csv -s "BMC Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/ATR-risk_scores-for-Susan.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR-risk_scores-for-Susan.csv -s "ATR Risk Scores"


---------
---------
REPEAT STEPS
---------
---------
1. Remove data from existing tables & create new output table with different constraints
delete from kre_report.hiv_hrs_raw_data_to_score;

DROP TABLE kre_report.hiv_hrs_manual_risk_score;
CREATE TABLE IF NOT EXISTS kre_report.hiv_hrs_manual_risk_score
(
    patient_id integer NOT NULL,
    masked_patient_id character varying COLLATE pg_catalog."default" NOT NULL,
    score_date date NOT NULL,
    hiv_risk_score numeric,
	PRIMARY KEY (patient_id, score_date)
);


2. Load all patients to be scored (use prep tables as with STI the same patient can have multiple dates)
   CHANGE THE MASKED PAT ID STRING BEFORE RUNNING

DROP TABLE IF EXISTS kre_report.hiv_hrs_scores_to_compute;
CREATE TABLE kre_report.hiv_hrs_scores_to_compute AS 
SELECT T1.patient_id, T2.masked_patient_id, T1.anchor_date, (T1.anchor_date - INTERVAL '1 year')::date as score_date
FROM kre_report.hiv_hrs_output_prep T1
JOIN kre_report.hiv_hrs_masked_patients T2 ON (T1.patient_id = T2.patient_id)
AND T2.masked_patient_id ilike 'ATR_NHIVO_STI%';

3. Create the SQL statements to run

DROP TABLE IF EXISTS kre_report.hiv_hrs_score_to_compute_sql;
CREATE TABLE kre_report.hiv_hrs_score_to_compute_sql AS
SELECT CONCAT('INSERT INTO kre_report.hiv_hrs_manual_risk_score SELECT ', patient_id, ', ''', masked_patient_id, ''', ''', score_date, 
			  ''', hiv_rpt.hiv_risk_score_calc_for_exact_date(', patient_id, ', ''', score_date, ''');') 
FROM kre_report.hiv_hrs_scores_to_compute;

4. Create the file
psql
\o /tmp/kre_test.txt
select * from kre_report.hiv_hrs_score_to_compute_sql;

5. Fix up the file to make it pure SQL
vi /tmp/kre_test.txt
remove header and final lines of file
check file count to make sure it lines up

6. Run the SQL to create the scores
cd /tmp
nohup psql -d esp -f kre_test.txt &

7. Review the output
select * from kre_report.hiv_hrs_manual_risk_score;

8. Format the output and email results file

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/CHA_NHIVO_STI-risk_scores.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA_NHIVO_STI-risk_scores.csv -s "CHA Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/FWY_NHIVO_STI-risk_scores.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n FWY_NHIVO_STI-risk_scores.csv -s "FWY Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/BMC_NHIVO_STI-risk_scores.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r keberhardt@commoninf.com -p /tmp -n BMC_NHIVO_STI-risk_scores.csv -s "BMC Risk Scores"

\COPY (select masked_patient_id, extract(year from score_date), hiv_risk_score from kre_report.hiv_hrs_manual_risk_score) TO '/tmp/ATR_NHIVO_STI-risk_scores.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR_NHIVO_STI-risk_scores.csv -s "ATR Risk Scores"
