drop table if exists hiv_rpt.compare_all_2023_scores;
create table hiv_rpt.compare_all_2023_scores as
select patient_id, hiv_risk_score
from gen_pop_tools.cc_hiv_risk_score
where rpt_year = '2023';

drop table if exists hiv_rpt.compare_scores_hivsti_ind;
create table hiv_rpt.compare_scores_hivsti_ind as 
select T1.patient_id, hiv_risk_score, 
MAX(case when condition = 'hiv' then 1 else 0 end) as hiv_yn, 
MAX(case when condition in ('syphilis', 'chlamydia', 'gonorrhea') then 1 else 0 end) sti_yn,
MAX(case when condition in ('hiv', 'syphilis', 'chlamydia', 'gonorrhea') then 1 else 0 end) hiv_or_sti_yn
from hiv_rpt.compare_all_2023_scores T1
left join nodis_case T2 on (T1.patient_id = T2.patient_id and date >= '2024-01-01'and date <= '2024-12-31' and condition in ('hiv', 'syphilis', 'gonorrhea', 'chlamydia'))
group by T1.patient_id, hiv_risk_score;


-- drop table if exists hiv_rpt.compare_scores_hivsti_ind_running_total;
-- create table hiv_rpt.compare_scores_hivsti_ind_running_total as
-- select *,
-- CASE WHEN hiv_or_sti_yn = 1 then (SUM(hiv_or_sti_yn) OVER (ORDER BY hiv_risk_score desc)) end AS hiv_or_sti_running_total
-- from hiv_rpt.compare_scores_hivsti_ind
-- order by hiv_risk_score desc;

-- drop table if exists hiv_rpt.compare_scores_hivsti_sens;
-- create table hiv_rpt.compare_scores_hivsti_sens as 
-- select 
-- hiv_yn_total,
-- sti_yn_total,
-- hiv_or_sti_yn_total,
-- round(hiv_or_sti_yn_total * .05, 0) as sens_05,
-- round(hiv_or_sti_yn_total * .10, 0) as sens_10,
-- round(hiv_or_sti_yn_total * .25, 0) as sens_25,
-- round(hiv_or_sti_yn_total * .50, 0) as sens_50
-- FROM (
-- select sum(case when hiv_yn = 1 then 1 else 0 end ) as hiv_yn_total,
-- sum(case when sti_yn = 1 then 1 else 0 end) as sti_yn_total,
-- sum(case when sti_yn = 1 or hiv_yn = 1 then 1 else 0 end ) as hiv_or_sti_yn_total
-- from hiv_rpt.compare_scores_hivsti_ind) T1;

-- drop table if exists hiv_rpt.compare_scores_hivsti_cor_risk_thresholds;
-- create table hiv_rpt.compare_scores_hivsti_cor_risk_thresholds as 
-- select max(corresponding_risk_threshold_05) corresponding_risk_threshold_05,
-- max(corresponding_risk_threshold_10) corresponding_risk_threshold_10,
-- max(corresponding_risk_threshold_25) corresponding_risk_threshold_25,
-- max(corresponding_risk_threshold_50) corresponding_risk_threshold_50
-- FROM (

-- (select hiv_risk_score as corresponding_risk_threshold_05, 0 as corresponding_risk_threshold_10, 0 as corresponding_risk_threshold_25, 0 as corresponding_risk_threshold_50 
-- FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
-- JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_or_sti_running_total >= sens_05 limit 1)
-- UNION
-- (select null, hiv_risk_score, null, null FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
-- JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_or_sti_running_total >= sens_10 limit 1)
-- UNION
-- (select null, null, hiv_risk_score, null FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
-- JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_or_sti_running_total >= sens_25 limit 1)
-- UNION
-- (select null, null, null, hiv_risk_score FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
-- JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_or_sti_running_total >= sens_50 limit 1)

-- ) T1;


-- drop table if exists hiv_rpt.compare_scores_hivsti_pats_gte_thresholds;
-- create table hiv_rpt.compare_scores_hivsti_pats_gte_thresholds as 
-- select max(pats_gte_threshold_05) pats_gte_threshold_05,
-- max(pats_gte_threshold_10) pats_gte_threshold_10,
-- max(pats_gte_threshold_25) pats_gte_threshold_25,
-- max(pats_gte_threshold_50) pats_gte_threshold_50
-- FROM ( 
-- select count(*) pats_gte_threshold_05 , 0 as pats_gte_threshold_10, 0 as pats_gte_threshold_25, 0 as pats_gte_threshold_50 
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_risk_score >= corresponding_risk_threshold_05
-- UNION
-- select 0, count(*), 0,0 
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_risk_score >= corresponding_risk_threshold_10
-- UNION
-- select 0, 0, count(*), 0 as pats_gte_threshold_25
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_risk_score >= corresponding_risk_threshold_25
-- UNION
-- select 0, 0, 0, count(*) as pats_gte_threshold_50
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_risk_score >= corresponding_risk_threshold_50
-- ) T1;


-- drop table if exists hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds;
-- create table hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds as 
-- select max(hivsti_gte_threshold_05) hivsti_gte_threshold_05,
-- max(hivsti_gte_threshold_10) hivsti_gte_threshold_10,
-- max(hivsti_gte_threshold_25) hivsti_gte_threshold_25,
-- max(hivsti_gte_threshold_50) hivsti_gte_threshold_50
-- FROM 
-- (
-- select count(*) hivsti_gte_threshold_05, 0 hivsti_gte_threshold_10, 0 hivsti_gte_threshold_25, 0 hivsti_gte_threshold_50
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_or_sti_yn = 1
-- and hiv_risk_score >= corresponding_risk_threshold_05
-- UNION
-- select 0, count(*), 0, 0
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_or_sti_yn = 1
-- and hiv_risk_score >= corresponding_risk_threshold_10
-- UNION
-- select 0, 0, count(*), 0
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_or_sti_yn = 1
-- and hiv_risk_score >= corresponding_risk_threshold_25
-- UNION
-- select 0, 0, 0, count(*)
-- from hiv_rpt.compare_scores_hivsti_ind T1
-- JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
-- where hiv_or_sti_yn = 1
-- and hiv_risk_score >= corresponding_risk_threshold_50
-- ) T1;


-- drop table if exists hiv_rpt.compare_scores_hivsti_ppv;
-- create table hiv_rpt.compare_scores_hivsti_ppv as
-- select 
-- round((hivsti_gte_threshold_05/pats_gte_threshold_05::numeric) * 100, 3) as ppv_05,
-- round((hivsti_gte_threshold_10/pats_gte_threshold_10::numeric) * 100, 3) as ppv_10,
-- round((hivsti_gte_threshold_25/pats_gte_threshold_25::numeric) * 100, 3) as ppv_25,
-- round((hivsti_gte_threshold_50/pats_gte_threshold_50::numeric) * 100, 3) as ppv_50
-- from hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds T1
-- JOIN hiv_rpt.compare_scores_hivsti_pats_gte_thresholds ON (1=1);


drop table if exists hiv_rpt.compare_scores_hivsti_ind_running_total;
create table hiv_rpt.compare_scores_hivsti_ind_running_total as
select *,
CASE WHEN hiv_yn = 1 then (SUM(hiv_yn) OVER (ORDER BY hiv_risk_score desc)) end AS hiv_running_total
from hiv_rpt.compare_scores_hivsti_ind
order by hiv_risk_score desc;

drop table if exists hiv_rpt.compare_scores_hivsti_sens;
create table hiv_rpt.compare_scores_hivsti_sens as 
select 
hiv_yn_total,
sti_yn_total,
hiv_or_sti_yn_total,
round(hiv_yn_total * .05, 0) as sens_05,
round(hiv_yn_total * .10, 0) as sens_10,
round(hiv_yn_total * .25, 0) as sens_25,
round(hiv_yn_total * .50, 0) as sens_50
FROM (
select sum(case when hiv_yn = 1 then 1 else 0 end ) as hiv_yn_total,
sum(case when sti_yn = 1 then 1 else 0 end) as sti_yn_total,
sum(case when sti_yn = 1 or hiv_yn = 1 then 1 else 0 end ) as hiv_or_sti_yn_total
from hiv_rpt.compare_scores_hivsti_ind) T1;

drop table if exists hiv_rpt.compare_scores_hivsti_cor_risk_thresholds;
create table hiv_rpt.compare_scores_hivsti_cor_risk_thresholds as 
select max(corresponding_risk_threshold_05) corresponding_risk_threshold_05,
max(corresponding_risk_threshold_10) corresponding_risk_threshold_10,
max(corresponding_risk_threshold_25) corresponding_risk_threshold_25,
max(corresponding_risk_threshold_50) corresponding_risk_threshold_50
FROM (

(select hiv_risk_score as corresponding_risk_threshold_05, 0 as corresponding_risk_threshold_10, 0 as corresponding_risk_threshold_25, 0 as corresponding_risk_threshold_50 
FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_running_total >= sens_05 limit 1)
UNION
(select null, hiv_risk_score, null, null FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_running_total >= sens_10 limit 1)
UNION
(select null, null, hiv_risk_score, null FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_running_total >= sens_25 limit 1)
UNION
(select null, null, null, hiv_risk_score FROM hiv_rpt.compare_scores_hivsti_ind_running_total T1
JOIN hiv_rpt.compare_scores_hivsti_sens T2 ON (1=1) where hiv_running_total >= sens_50 limit 1)

) T1;


drop table if exists hiv_rpt.compare_scores_hivsti_pats_gte_thresholds;
create table hiv_rpt.compare_scores_hivsti_pats_gte_thresholds as 
select max(pats_gte_threshold_05) pats_gte_threshold_05,
max(pats_gte_threshold_10) pats_gte_threshold_10,
max(pats_gte_threshold_25) pats_gte_threshold_25,
max(pats_gte_threshold_50) pats_gte_threshold_50
FROM ( 
select count(*) pats_gte_threshold_05 , 0 as pats_gte_threshold_10, 0 as pats_gte_threshold_25, 0 as pats_gte_threshold_50 
from hiv_rpt.compare_scores_hivsti_ind T1
join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_risk_score >= corresponding_risk_threshold_05
UNION
select 0, count(*), 0,0 
from hiv_rpt.compare_scores_hivsti_ind T1
join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_risk_score >= corresponding_risk_threshold_10
UNION
select 0, 0, count(*), 0 as pats_gte_threshold_25
from hiv_rpt.compare_scores_hivsti_ind T1
join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_risk_score >= corresponding_risk_threshold_25
UNION
select 0, 0, 0, count(*) as pats_gte_threshold_50
from hiv_rpt.compare_scores_hivsti_ind T1
join hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_risk_score >= corresponding_risk_threshold_50
) T1;


drop table if exists hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds;
create table hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds as 
select max(hivsti_gte_threshold_05) hivsti_gte_threshold_05,
max(hivsti_gte_threshold_10) hivsti_gte_threshold_10,
max(hivsti_gte_threshold_25) hivsti_gte_threshold_25,
max(hivsti_gte_threshold_50) hivsti_gte_threshold_50
FROM 
(
select count(*) hivsti_gte_threshold_05, 0 hivsti_gte_threshold_10, 0 hivsti_gte_threshold_25, 0 hivsti_gte_threshold_50
from hiv_rpt.compare_scores_hivsti_ind T1
JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_yn = 1
and hiv_risk_score >= corresponding_risk_threshold_05
UNION
select 0, count(*), 0, 0
from hiv_rpt.compare_scores_hivsti_ind T1
JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_yn = 1
and hiv_risk_score >= corresponding_risk_threshold_10
UNION
select 0, 0, count(*), 0
from hiv_rpt.compare_scores_hivsti_ind T1
JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_yn = 1
and hiv_risk_score >= corresponding_risk_threshold_25
UNION
select 0, 0, 0, count(*)
from hiv_rpt.compare_scores_hivsti_ind T1
JOIN hiv_rpt.compare_scores_hivsti_cor_risk_thresholds T2 ON (1=1)
where hiv_yn = 1
and hiv_risk_score >= corresponding_risk_threshold_50
) T1;


drop table if exists hiv_rpt.compare_scores_hivsti_ppv;
create table hiv_rpt.compare_scores_hivsti_ppv as
select 
round((hivsti_gte_threshold_05/pats_gte_threshold_05::numeric) * 100, 3) as ppv_05,
round((hivsti_gte_threshold_10/pats_gte_threshold_10::numeric) * 100, 3) as ppv_10,
round((hivsti_gte_threshold_25/pats_gte_threshold_25::numeric) * 100, 3) as ppv_25,
round((hivsti_gte_threshold_50/pats_gte_threshold_50::numeric) * 100, 3) as ppv_50
from hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds T1
JOIN hiv_rpt.compare_scores_hivsti_pats_gte_thresholds ON (1=1);


select hiv_yn_total,
sti_yn_total,
hiv_or_sti_yn_total
from hiv_rpt.compare_scores_hivsti_sens;

select 
sens_05,
sens_10,
sens_25,
sens_50
from hiv_rpt.compare_scores_hivsti_sens;

select * from hiv_rpt.compare_scores_hivsti_cor_risk_thresholds;

select * from hiv_rpt.compare_scores_hivsti_pats_gte_thresholds;

select * from hiv_rpt.compare_scores_hivsti_pos_pats_gte_thresholds;

select * from  hiv_rpt.compare_scores_hivsti_ppv;


select concat('BMC-', patient_id) as patient_id, hiv_risk_score, hiv_yn, sti_yn, hiv_or_sti_yn
from hiv_rpt.compare_scores_hivsti_ind;


\COPY (select concat('BMC-', patient_id) as patient_id, hiv_risk_score, hiv_yn, sti_yn, hiv_or_sti_yn from hiv_rpt.compare_scores_hivsti_ind) TO '/tmp/BMC-2025-02-14-hivsti-sens-analysis.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r keberhardt@commoninf.com -p /tmp -n BMC-2025-02-14-hivsti-sens-analysis.csv -s "BMC HIVSTI"

\COPY (select concat('FWY-', patient_id) as patient_id, hiv_risk_score, hiv_yn, sti_yn, hiv_or_sti_yn from hiv_rpt.compare_scores_hivsti_ind) TO '/tmp/FWY-2025-02-14-hivsti-sens-analysis.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n FWY-2025-02-14-hivsti-sens-analysis.csv -s "FWY HIVSTI"

\COPY (select concat('ATR-', patient_id) as patient_id, hiv_risk_score, hiv_yn, sti_yn, hiv_or_sti_yn from hiv_rpt.compare_scores_hivsti_ind) TO '/tmp/ATR-2025-02-14-hivsti-sens-analysis.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR-2025-02-14-hivsti-sens-analysis.csv -s "ATR HIVSTI"

\COPY (select concat('CHA-', patient_id) as patient_id, hiv_risk_score, hiv_yn, sti_yn, hiv_or_sti_yn from hiv_rpt.compare_scores_hivsti_ind) TO '/tmp/CHA-2025-02-14-hivsti-sens-analysis.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA-2025-02-14-hivsti-sens-analysis.csv -s "CHA HIVSTI"






1.000000	0.999994	0.683226	0.000479

