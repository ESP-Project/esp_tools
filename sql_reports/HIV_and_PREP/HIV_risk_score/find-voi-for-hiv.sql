DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_1;
CREATE TABLE kre_report.hiv_hrs_voi_1 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_chlam_gon_labs_counts T2 ON (T1.patient_id = T2.patient_id AND T1.anchor_date = T2.anchor_date)
WHERE 
gon_tests_1yr > 0
OR gon_tests_1_to_2yr > 0 
OR gon_tests_gt_2yr > 0 
OR gon_pos_tests_1yr > 0 
OR gon_pos_tests_1_to_2yr > 0
OR gon_pos_tests_gt_2yr > 0
OR gon_tests_rectal_1yr > 0
OR gon_tests_rectal_1_to_2yr > 0
OR gon_tests_rectal_gt_2yr > 0
OR gon_tests_rectal_pos_1yr > 0
OR gon_tests_rectal_pos_1_to_2yr > 0
OR gon_tests_rectal_pos_gt_2yr > 0
OR gon_tests_oral_1yr > 0
OR gon_tests_oral_1_to_2yr > 0
OR gon_tests_oral_gt_2yr > 0
OR gon_tests_oral_pos_1yr > 0
OR gon_tests_oral_pos_1_to_2yr > 0
OR gon_tests_oral_pos_gt_2yr > 0
OR chlam_tests_1yr > 0
OR chlam_tests_1_to_2yr > 0
OR chlam_tests_gt_2yr > 0
OR chlam_pos_tests_1yr > 0
OR chlam_pos_tests_1_to_2yr > 0
OR chlam_pos_tests_gt_2yr > 0
OR chlam_tests_rectal_1yr > 0
OR chlam_tests_rectal_1_to_2yr > 0
OR chlam_tests_rectal_gt_2yr > 0
OR chlam_tests_rectal_pos_1yr > 0
OR chlam_tests_rectal_pos_1_to_2yr > 0
OR chlam_tests_rectal_pos_gt_2yr > 0
OR chlam_tests_oral_1yr > 0
OR chlam_tests_oral_1_to_2yr > 0
OR chlam_tests_oral_gt_2yr > 0
OR chlam_tests_oral_pos_1yr > 0
OR chlam_tests_oral_pos_1_to_2yr > 0
OR chlam_tests_oral_pos_gt_2yr > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_2;
CREATE TABLE kre_report.hiv_hrs_voi_2 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_syph_counts T2 ON (T1.patient_id = T2.patient_id AND T1.anchor_date = T2.anchor_date)
WHERE 
syph_tests_1yr > 0 
OR syph_tests_1_to_2yr > 0 
OR syph_tests_gt_2yr > 0; 


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_3;
CREATE TABLE kre_report.hiv_hrs_voi_3 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_syph_case_counts T2 ON (T1.patient_id = T2.patient_id AND T1.anchor_date = T2.anchor_date)
WHERE 
syph_per_esp_1yr > 0 
OR syph_per_esp_1_to_2yr > 0 
OR syph_per_esp_gt_2yr > 0;

DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_4;
CREATE TABLE kre_report.hiv_hrs_voi_4 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_hep_events_nohef_counts T5 ON (T1.patient_id = T5.patient_id AND T1.anchor_date = T5.anchor_date)
WHERE 
hcv_ab_1yr > 0 
OR hcv_ab_1_to_2yr > 0 
OR hcv_ab_gt_2yr > 0 
OR hcv_rna_1yr > 0 
OR hcv_rna_1_to_2yr > 0 
OR hcv_rna_gt_2yr > 0 
OR hbv_sag_1yr > 0 
OR hbv_sag_1_to_2yr > 0 
OR hbv_sag_gt_2yr > 0 
OR hbv_dna_1yr > 0 
OR hbv_dna_1_to_2yr > 0 
OR hbv_dna_gt_2yr > 0;

 
DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_5;
CREATE TABLE kre_report.hiv_hrs_voi_5 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_hep_events_hef_counts T6 ON (T1.patient_id = T6.patient_id AND T1.anchor_date = T6.anchor_date)
WHERE 
hcv_ab_or_rna_pos_ev > 0 
OR hcv_ab_pos_first_yr is not null
OR hcv_rna_pos_first_yr is not  null
OR hbv_sag_or_dna_pos_ev > 0 
OR hbv_sag_or_dna_pos_first_yr is not null;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_6;
CREATE TABLE kre_report.hiv_hrs_voi_6 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_hep_case_counts T7 ON (T1.patient_id = T7.patient_id AND T1.anchor_date = T7.anchor_date)
WHERE 
hcv_acute_per_esp > 0 
OR hcv_acute_per_esp_case_yrs is not null
OR hbv_acute_per_esp > 0 
OR hbv_acute_per_esp_case_yrs is not null;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_7;
CREATE TABLE kre_report.hiv_hrs_voi_7 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_hepb_diags T8 ON (T1.patient_id = T8.patient_id AND T1.anchor_date = T8.anchor_date)
WHERE hep_b_dx > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_8;
CREATE TABLE kre_report.hiv_hrs_voi_8 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_all_hiv_events_nohef_counts T9 ON (T1.patient_id = T9.patient_id AND T1.anchor_date = T9.anchor_date)
WHERE
hiv_elisa_ag_ab_1yr > 0
OR hiv_elisa_ag_ab_1_to_2yr > 0
OR hiv_elisa_ag_ab_gt_2yr > 0
OR hiv_confirm_tests_1yr > 0
OR hiv_confirm_tests_1_to_2yr > 0
OR hiv_confirm_tests_gt_2yr > 0
OR hiv_rna_viral_1yr > 0
OR hiv_rna_viral_1_to_2yr > 0
OR hiv_rna_viral_gt_2yr > 0;

DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_9;
CREATE TABLE kre_report.hiv_hrs_voi_9 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_viral_loads T10 ON (T1.patient_id = T10.patient_id AND T1.anchor_date = T10.anchor_date)
WHERE
hiv_max_viral_load_yr is not null;

DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_10;
CREATE TABLE kre_report.hiv_hrs_voi_10 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_hiv_case_values T11 ON (T1.patient_id = T11.patient_id AND T1.anchor_date = T11.anchor_date)
WHERE
hiv_status_per_esp != 2
OR hiv_new_diag_final is not null
OR hiv_neg_w_rna_test_ev > 0
OR hiv_neg_w_hiv_med_ev > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_11;
CREATE TABLE kre_report.hiv_hrs_voi_11 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_hiv_rx_3_diff_count T12 ON (T1.patient_id = T12.patient_id AND T1.anchor_date = T12.anchor_date)
WHERE
hiv_trmt_regimen_1yr > 0
OR hiv_trmt_regimen_1_to2yr > 0
OR hiv_trmt_regimen_gt_2yr > 0;

DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_12;
CREATE TABLE kre_report.hiv_hrs_voi_12 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_hiv_pep_counts T23 ON (T1.patient_id = T23.patient_id AND T1.anchor_date = T23.anchor_date)
WHERE
pep_rx_dates_num > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_13;
CREATE TABLE kre_report.hiv_hrs_voi_13 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_prep_final T13 ON (T1.patient_id = T13.patient_id AND T1.anchor_date = T13.anchor_date)
WHERE
prep_rx_num_1yr > 0
OR prep_rx_num_1_to_2yr > 0
OR prep_rx_num_gt_2yr > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_14;
CREATE TABLE kre_report.hiv_hrs_voi_14 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_hiv_dx_code_counts T14 ON (T1.patient_id = T14.patient_id AND T1.anchor_date = T14.anchor_date)
WHERE
hiv_dx_1_yr > 0
OR hiv_dx_1_to_2yr > 0
OR hiv_dx_gt_2yr > 0;

DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_15;
CREATE TABLE kre_report.hiv_hrs_voi_15 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_dx_code_years T15 ON (T1.patient_id = T15.patient_id AND T1.anchor_date = T15.anchor_date)
WHERE
dx_abn_anal_cyt_yrs is not null
OR dx_syph_yrs is not null
OR dx_anal_syph_yrs is not null
OR dx_gon_inf_rectum_yrs is not null
OR dx_gon_pharyn_yrs is not null
OR dx_chlam_inf_rectum_yrs is not null
OR dx_chlam_pharyn_yrs is not null
OR dx_lymph_ven_yrs is not null
OR dx_chancroid_yrs is not null
OR dx_granuloma_ingu_yrs is not null
OR dx_nongon_ureth_yrs is not null
OR dx_hsv_w_compl_yrs is not null
OR dx_genital_herpes_yrs is not null
OR dx_anogenital_warts_yrs is not null
OR dx_anorectal_ulcer_yrs is not null
OR dx_trich_yrs is not null
OR dx_mpox_yrs is not null
OR dx_unspec_std_yrs is not null
OR dx_pid_yrs is not null
OR dx_cont_w_exp_to_vd_yrs is not null
OR dx_high_risk_sex_behav_yrs is not null
OR dx_hiv_counsel_yrs is not null
OR dx_anorexia_yrs is not null
OR dx_bulimia_yrs is not null
OR dx_eat_disorder_nos_yrs is not null
OR dx_gend_iden_dis_yrs is not null
OR dx_transsexualism_yrs is not null
OR dx_couns_or_prb_child_sex_abuse_yrs is not null
OR dx_adult_child_sex_exp_or_abuse_yrs is not null
OR dx_adult_or_child_phys_abuse_yrs is not null
OR dx_foreign_body_anus_yrs is not null
OR dx_alcohol_depend_abuse_yrs is not null
OR dx_opioid_depend_abuse_yrs is not null
OR dx_sed_hypn_anxio_depend_abuse_yrs is not null
OR dx_cocaine_depend_abuse_yrs is not null
OR dx_amphet_stim_depend_abuse_yrs is not null
OR dx_oth_psycho_or_nos_subs_depend_abuse_yrs is not null
OR dx_sti_screen_yrs is not null
OR dx_malig_neo_rec_screen_yrs is not null
OR dx_needle_stick is not null;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_16;
CREATE TABLE kre_report.hiv_hrs_voi_16 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_bicillin_meds_updated T16 ON (T1.patient_id = T16.patient_id AND T1.anchor_date = T16.anchor_date)
WHERE
rx_bicillin_ever > 0
UNION
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_azith_meds T17 ON (T1.patient_id = T17.patient_id and T1.anchor_date = T17.anchor_date)
WHERE
rx_azith_ever > 0
UNION
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_ceft_meds T18 ON (T1.patient_id = T18.patient_id and T1.anchor_date = T18.anchor_date)
WHERE
rx_ceft_ever > 0
UNION
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_doxy_meds T19 ON (T1.patient_id = T19.patient_id and T1.anchor_date = T19.anchor_date)
WHERE
rx_doxy_ever > 0
UNION
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_other_meds T20 ON (T1.patient_id = T20.patient_id and T1.anchor_date = T20.anchor_date)
WHERE
rx_methadone_ever > 0
OR rx_buprenorphine_ever > 0
OR rx_naltrexone_ever > 0
OR rx_viagara_cilais_or_levitra_ever > 0;


DROP TABLE IF EXISTS kre_report.hiv_hrs_voi_17;
CREATE TABLE kre_report.hiv_hrs_voi_17 AS 
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_mpox_vax T21 ON (T1.patient_id = T21.patient_id and T1.anchor_date = T21.anchor_date)
WHERE
vax_mpox_ever > 0
UNION
SELECT T1.patient_id, T1.anchor_date
FROM kre_report.hiv_hrs_index_patients T1
JOIN kre_report.hiv_hrs_hpv_vax T22 ON (T1.patient_id = T22.patient_id and T1.anchor_date = T22.anchor_date)
WHERE
vax_hpv_male_ever > 0;

DROP TABLE IF EXISTS kre_report.hiv_hrs_pat_voi_compare;
CREATE TABLE kre_report.hiv_hrs_pat_voi_compare AS 
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_1
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_2
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_3
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_4
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_5
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_6
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_7
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_8
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_9
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_10
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_11
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_12
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_13
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_14
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_15
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_16
UNION
SELECT patient_id, anchor_date
FROM kre_report.hiv_hrs_voi_17
GROUP BY patient_id, anchor_date;