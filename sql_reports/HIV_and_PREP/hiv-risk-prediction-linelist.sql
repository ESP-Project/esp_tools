-- generate an Excel-based linelist on demand with one row per patient that includes 
-- name, 
-- MRN, 
-- age, 
-- risk score, 
-- PCP name, 
-- PCP’s site of practice, 
-- the value for each variable in the dataset, 
-- last creatinine value and date, 
-- last Chlamydia test date + result, 
-- last Gonorrhea test date + result, 
-- last HIV test date + result, 
-- and last syphilis test date + result, 


-- done done # sex	-7.508866 if male, -9.374485 if female	1.00000000
-- done done # race_black	1 if race is recorded as black, 0 otherwise	1.06186066
-- done done # race_caucasian	1 if race is recorded as caucasian, 0 otherwise	-0.6609999
-- done done # english_lang	1 if English is primary language, 0 otherwise	-0.4213635
-- done done # has_language_data	1 if language is recorded, 0 if missing	-0.0784053
-- done done # years_of_data	length of available EHR history in years	-0.0660395
-- done done # t_hiv_rna_1_year	number of HIV RNA tests in the previous year	0.14676104
-- done done # rx_bicillin_1_year	number of prescriptions for bicillin in previous year	1.35516827
-- done done # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- done done # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
-- done done # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years	3.07004984
-- done done # t_hiv_test_2yr	total number of HIV tests in past 2 years (regardless of test result)	0.23349732
-- done done # t_hiv_test_2yr	total number of HIV tests in past 2 years (regardless of test result)	0.23349732
-- done done # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years	0.20795003
-- done done # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years	0.19721102
-- done done # t_chla_test_t_e	total number of chlamydia tests ever	-0.154315
-- done done # t_hiv_test_e		total number of HIV tests ever (ELISA or Ab/Ag or RNA)	0.12024552
-- done done # t_hiv_elisa_e	total number of HIV ELISA or Ab/Ag tests ever	0.16006917
-- done done # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- done # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise	0.9997348
-- done # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	0.28886449
-- done done # acute_hiv_test_2yr	1 if had an HIV RNA test in past 2 years, 0 otherwise	0.15925514
-- done done # acute_hiv_test_e	1 if had an HIV RNA test ever, 0 otherwise	1.82082955
-- done # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)	1.09544347

DROP TABLE IF EXISTS kre_report.hrp_coefficients;
DROP TABLE IF EXISTS kre_report.hrp_index_patients;
DROP TABLE IF EXISTS kre_report.hrp_pat_encs;
DROP TABLE IF EXISTS kre_report.hrp_hiv_labs;
DROP TABLE IF EXISTS kre_report.hrp_hiv_lab_values;
DROP TABLE IF EXISTS kre_report.hrp_hiv_lab_result_dates;
DROP TABLE IF EXISTS kre_report.hrp_gon_chlam_events;
DROP TABLE IF EXISTS kre_report.hrp_gon_chlam_values;
DROP TABLE IF EXISTS kre_report.hrp_syph_events;
DROP TABLE IF EXISTS kre_report.hrp_diags;
DROP TABLE IF EXISTS kre_report.hrp_diags_values;
DROP TABLE IF EXISTS kre_report.hrp_bicillin;
DROP TABLE IF EXISTS kre_report.hrp_bicillin_exclusion;
DROP TABLE IF EXISTS kre_report.hrp_bicillin_exclusion_2;
DROP TABLE IF EXISTS kre_report.hrp_bicillin_rx_exclusion_final;
DROP TABLE IF EXISTS kre_report.hrp_bicillin_subset;
DROP TABLE IF EXISTS kre_report.hrp_bicillin_values;
DROP TABLE IF EXISTS kre_report.hrp_suboxone;
DROP TABLE IF EXISTS kre_report.hrp_suboxone_values;
DROP TABLE IF EXISTS kre_report.hrp_creatinine_most_recent;
DROP TABLE IF EXISTS kre_report.hrp_chlam_gon_syph_max_dates;
DROP TABLE IF EXISTS kre_report.hrp_chlam_most_recent;
DROP TABLE IF EXISTS kre_report.hrp_gon_most_recent;
DROP TABLE IF EXISTS kre_report.hrp_syph_max_dates;
DROP TABLE IF EXISTS kre_report.hrp_syph_most_recent;
DROP TABLE IF EXISTS kre_report.hrp_hiv_max_dates;
DROP TABLE IF EXISTS kre_report.hrp_hiv_most_recent;
DROP TABLE IF EXISTS kre_report.hrp_most_recent_weight;
DROP TABLE IF EXISTS kre_report.hrp_truvada;
DROP TABLE IF EXISTS kre_report.hrp_pat_sum_x_value;
DROP TABLE IF EXISTS kre_report.hrp_index_data;
DROP TABLE IF EXISTS kre_report.hrp_risk_scores;

DROP TABLE IF EXISTS kre_report.hrp_data_output;
DROP TABLE IF EXISTS kre_report.hrp_high_risk_percentile_with_provider;


CREATE TABLE IF NOT EXISTS kre_report.hrp_high_risk_pats_for_tracking
(
hiv_risk_score numeric,
percentile integer,
pat_sum_x_value double precision,
patient_id integer,
mrn character varying(25),
score_date date,
provider_contacted character varying(25),
contact_date date,
CONSTRAINT hrp_high_risk_pats_for_tracking_pkey PRIMARY KEY (patient_id, score_date)
);


CREATE TABLE kre_report.hrp_coefficients AS
SELECT 1.00000000::real sex,
1.06186066::real race_black,
-0.6609999::real race_caucasian,
-0.4213635::real english_lang,
-0.0784053::real has_language_data,
-0.0660395::real years_of_data,
-0.6311702::real has_1yr_data,
-0.4035768::real has_2yr_data,
0.14676104::real t_hiv_rna_1_yr,
0.23349732::real t_hiv_test_2yr,
0.12024552::real t_hiv_test_e,
0.16006917::real t_hiv_elisa_e,
1.82082955::real acute_hiv_test_e,
0.15925514::real acute_hiv_test_2yr,
3.07004984::real t_pos_gon_test_2yr,
-0.154315::real t_chla_test_t_e,
1.35516827::real rx_bicillin_1_yr,
0.20795003::real rx_bicillin_2yr,
1.79771045::real rx_bicillin_e,
0.19721102::real rx_suboxone_2yr,
0.9997348::real syphilis_any_site_state_x_late_e,
0.28886449::real contact_w_or_expo_venereal_dis_e,
1.09544347::real hiv_counseling_2yr;


CREATE TABLE kre_report.hrp_index_patients AS
SELECT T1.id as patient_id,
concat(T1.last_name, ', ', T1.first_name) as name,
T1.mrn,
date_part('year',age(date_of_birth)) current_age,
concat(T2.last_name, ', ', T2.first_name, ' ', T2.title) as pcp_name,
T2.dept as pcp_site,
gender as sex_raw,
CASE WHEN gender in ('F', 'FEMALE') then -9.374485
     WHEN gender in ('M', 'MALE') then -7.508866
     END sex,
race as race_raw,
CASE WHEN race = 'BLACK' then 1 ELSE 0 END race_black,
CASE WHEN race in ('WHITE', 'CAUCASIAN') then 1 ELSE 0 END race_caucasian,
home_language as home_language_raw,
CASE WHEN home_language = 'ENGLISH' then 1 ELSE 0 END english_lang,
CASE WHEN home_language not in ('DECLINED', 'UNKNOWN', 'UNABLE TO BE DETERMINED', 'None', '') and home_language is not null then 1 else 0 END has_language_data
FROM emr_patient T1
LEFT JOIN emr_provider T2 on (T1.pcp_id = T2.id)
JOIN emr_encounter T3 on (T1.id = T3.patient_id)
LEFT JOIN static_enc_type_lookup T4 on (T3.raw_encounter_type = T4.raw_encounter_type)
WHERE gender in ('M', 'MALE', 'FEMALE', 'F')
-- must have an ambulatory encounter in the past 2 years
AND T3.date >= (now()::date - interval '2 years')
AND T4.ambulatory = 1
-- must be at least age 15
AND date_part('year',age(date_of_birth)) >= 15
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv')
-- filter out test patients
AND T1.last_name not in ('TEST', 'TEST**')
AND T1.last_name not ilike '%ssmctest%' 
AND T1.last_name not ilike '% test%' 
AND T1.last_name not ilike 'XB%' 
AND T1.last_name not ilike 'XX%'
AND T1.last_name not ilike 'XYZ%'
AND T1.last_name not ilike 'ZZ%'
AND T1.last_name not ilike 'YY%'
AND T1.last_name not ilike 'test'
AND T1.last_name not ilike 'esp'
AND T1.first_name NOT ILIKE 'test'
AND T1.first_name not ilike 'yytest%'
AND T1.first_name not ILIKE 'zztest%'
GROUP BY T1.id, name, T1.mrn, current_age, pcp_name, pcp_site, sex_raw, sex, race_raw, race_black, race_caucasian, home_language_raw, english_lang, has_language_data;


-- years_of_data 
-- length of available EHR history in years (based on encounter of specific type)
-- # has_1yr_data	1 if EHR history exists for previous year, 0 otherwise	-0.6311702
-- # has_2yr_data	1 if EHR history exists for previous 2 years, 0 otherwise	-0.4035768
-- CREATE TABLE kre_report.hrp_pat_encs AS 
-- SELECT patient_id, (max(abs(date_part('year', age(date, now()::date))) ) + 1) years_of_data,
-- max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 0 then 1 END) has_1yr_data,
-- max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 1 then 1 END) has_2yr_data
-- FROM emr_encounter 
-- WHERE date >= '01-01-2006' 
-- AND raw_encounter_type in ('CONSULTATION', 'DIALYSIS', 'ED ADMISSION', 'EMERGENCY RO', 'ENDO', 'EXTENDED CAR', 'HOME VISIT', 'HOSPITAL', 'HOSPITAL ADM', 'HOSPITAL OB', 'INPATIENT -', 'INPT ROUND', 'MTM OFFICE', 'OB VISIT', 'OUTPT DEPT', 'POST DISCHAR', 'PRE-OP', 'PROCEDURE', 'REHAB', 'REG', 'SHARED PP', 'SHARED RETUR', 'SMAG OFFICE', 'SMA OFFICE', 'URGENT CARE', 'VISIT')
-- GROUP BY patient_id;

CREATE TABLE kre_report.hrp_pat_encs AS 
SELECT patient_id, (max(abs(date_part('year', age(date, now()::date))) ) + 1) years_of_data,
max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 0 then 1 END) has_1yr_data,
max(CASE WHEN abs(date_part('year', age(date, now()::date))) = 1 then 1 END) has_2yr_data
FROM emr_encounter T1,
static_enc_type_lookup T2
WHERE T1.raw_encounter_type = T2.raw_encounter_type
AND T2.ambulatory = 1
AND date >= '01-01-2006' 
GROUP BY patient_id;

-- Gather up all of the HIV lab tests
-- (ELISA or Ab/Ag or RNA) 
-- NEED TO LIMIT TO THESE TEST TYPES TO ELISA, HIV_RNA_VIRAL, HIV_AG_AB
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH A HEF EVENT OR ELSE YOU WILL MISS VIRAL LOAD TESTS
CREATE TABLE kre_report.hrp_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM T1.date) rpt_year, abs(date_part('year', age(T1.date, now()::date))) yrs_since_test, T1.date, result_string, name as hef_name, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
LEFT JOIN hef_event T3 ON (T1.id = T3.object_id and T1.patient_id = T3.patient_id)
WHERE test_name in ('hiv_elisa', 'hiv_rna_viral', 'hiv_ag_ab')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined')
AND T1.date >= '01-01-2006' 
AND result_string is not null;

-- Compute HIV test values
-- number of HIV RNA tests in the previous year
-- total number of HIV tests in past 2 years (regardless of test result)
-- total number of HIV tests ever (ELISA or Ab/Ag or RNA)
-- total number of HIV ELISA or Ab/Ag tests ever
-- 1 if had an HIV RNA test in past 2 years, 0 otherwise
-- 1 if had an HIV RNA test ever, 0 otherwise

CREATE TABLE kre_report.hrp_hiv_lab_values AS
SELECT
patient_id,
count(CASE WHEN yrs_since_test = 0 AND test_name = 'hiv_rna_viral' THEN 1 END) t_hiv_rna_1_yr,
count(CASE WHEN yrs_since_test <= 1 THEN 1 END) t_hiv_test_2yr,
count(*) t_hiv_test_e,
count(CASE WHEN test_name in ('hiv_elisa', 'hiv_ag_ab') THEN 1 END) t_hiv_elisa_e,
max(CASE WHEN yrs_since_test <= 1 AND test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_2yr,
max(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 END) acute_hiv_test_e
FROM kre_report.hrp_hiv_labs
GROUP BY patient_id;

-- Count number of unique test result dates ever
CREATE TABLE kre_report.hrp_hiv_lab_result_dates AS
SELECT patient_id, count(tests_per_date) t_hiv_test_dates_e
FROM 
	(SELECT patient_id, date, count(*) tests_per_date
	 FROM kre_report.hrp_hiv_labs
	--where patient_id = 15549693
	 GROUP BY patient_id, date) T1
GROUP BY patient_id;


-- Gather up labs hef events for chlamydia and gonorrhea 
CREATE TABLE kre_report.hrp_gon_chlam_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, now()::date))) yrs_since_test, result_string
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T2.test_name in ('chlamydia', 'gonorrhea');


-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
CREATE TABLE kre_report.hrp_syph_events AS 
SELECT T1.id,T2.test_name as condition, T3.name as hef_name,T1.patient_id,T1.date, abs(date_part('year', age(T1.date, now()::date))) yrs_since_test, result_string, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Test not done', 'TNP', 'Test not performed', 'TNP', 'declined', 'Declined', 'not done', 'pt declined', 'Not Done', 'Not performed',  'Not tested', 'Not Tested', 'test')
and result_string is not null;


-- Compute Chlamydia and Gonorrhea values
-- # t_pos_gon_test_2yr	total number of positive gonorrhea tests in past 2 years
-- # t_chla_test_t_e	total number of chlamydia tests ever

CREATE TABLE kre_report.hrp_gon_chlam_values AS
SELECT 
patient_id,
count(CASE WHEN hef_name = 'lx:gonorrhea:positive' AND yrs_since_test <= 1 THEN 1 END) t_pos_gon_test_2yr,
count(CASE WHEN condition = 'chlamydia' THEN 1 END) t_chla_test_t_e
FROM kre_report.hrp_gon_chlam_events
GROUP BY patient_id;

-- Gather up diagnosis codes of interest
-- # syphilis_any_site_state_x_late_e	1 if syphilis ever diagnosed, 0 otherwise (icd9:091.*, icd9:092.*, icd9:093.*, icd9:094.*, icd9:095.*, icd10:A51.*, icd9:097.9, icd10:A52.0, icd10:A52.7) 
-- # contact_w_or_expo_venereal_dis_e	1 if ever had contact or exposure to venereal disease, 0 otherwise (ICD9=V01.6 or ICD10=Z20.2 or Z20.6 ever)	
-- # hiv_counseling_2yr	1 if counseled for HIV in past 2 years, 0 otherwise (ICD9=V65.44 or ICD10=Z71.7)
CREATE TABLE kre_report.hrp_diags AS 
SELECT T2.patient_id, T1.id, T1.encounter_id, T1.dx_code_id, abs(date_part('year', age(T2.date, now()::date))) yrs_since_diag
FROM emr_encounter_dx_codes T1, 
emr_encounter T2
WHERE 
T1.encounter_id = T2.id
AND (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V65.44','icd10:Z71.7'));


CREATE TABLE kre_report.hrp_diags_values AS
SELECT T1.patient_id,
MAX(CASE WHEN dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') then 1 end) syphilis_any_site_state_x_late_e,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then  1 end) contact_w_or_expo_venereal_dis_e,
MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') and yrs_since_diag <=1  then 1 end) hiv_counseling_2yr
FROM kre_report.hrp_diags T1
GROUP BY patient_id;


-- # rx_bicillin_1_year	number of prescriptions for bicillin in previous year
-- # rx_bicillin_2yr	total number of prescriptions for bicillin in past 2 years
-- # rx_bicillin_e	total number of prescriptions for bicillin ever	1.79771045
-- Only count 1 rx per date/ per name

CREATE TABLE kre_report.hrp_bicillin AS
SELECT patient_id, name, date, abs(date_part('year', age(date, now()::date))) yrs_since_rx
FROM emr_prescription
WHERE (name in (
--ATRIUS STARTS HERE
'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)',
'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)',
'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE',
'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)',
--CHA STARTS HERE
'BICILLIN C-R 1200000 UNIT/2ML IM SUSP',
'BICILLIN C-R (2400000) 300000-300000 UNIT/ML IM SUSP',
'BICILLIN L-A 1200000 UNIT/2ML IM SUSP',
'BICILLIN L-A 2400000 UNIT/4ML IM SUSP',
'BICILLIN L-A 600000 UNIT/ML IM SUSP',
'PENICILLIN G 5 MILLION UNITS/100 ML NS (VIAL MATE)',
'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G BENZATHINE 2400000 UNIT/4ML IM SUSP',
'PENICILLIN G BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G BENZATHINE & PROC 1200000 UNIT/2ML IM SUSP',
'PENICILLIN G IVPB IN 100 ML',
'PENICILLIN G IVPB IN 50 ML',
'PENICILLIN G IVPB (MINIBAG-PLUS) 5 MILLION UNITS',
'PENICILLIN G IVPB MINIBAG PLUS 5 MILLION UNITS',
'PENICILLIN G POTASSIUM 20000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM 5000000 UNITS IJ SOLR',
'PENICILLIN G POTASSIUM IN D5W 40000 UNIT/ML IV SOLN',
'PENICILLIN G POTASSIUM IN D5W 60000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 20000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 40000 UNIT/ML IV SOLN',
'PENICILLIN G POT IN DEXTROSE 60000 UNIT/ML IV SOLN',
'PENICILLIN G PROCAINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 300000-900000 UNIT/ML IM SUSP',
'PENICILLIN G PROC & BENZATHINE 600000 UNIT/ML IM SUSP',
'PENICILLIN G SODIUM 5000000 IU IJ SOLR'
)
-- ATRIUS 
OR (name in ('BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE') and quantity_float = 2)
OR (name in ('BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE') and quantity_float = 4))
GROUP BY patient_id, name, date;



-- Identify patients where bicillin is excluded due to ICD code
CREATE TABLE kre_report.hrp_bicillin_exclusion AS
SELECT T1.patient_id, 'Bicillin Exclusion: Non-Syph ICD Codes'::text bicillin_rx_exclusion
FROM kre_report.hrp_bicillin T1, 
emr_encounter T2,
emr_encounter_dx_codes T3
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
and T1.date = T2.date
GROUP BY T1.patient_id
order by patient_id;


-- Exclude bicillin prescriptions that match specific dx codes 
-- occurring on the same date
CREATE TABLE kre_report.hrp_bicillin_subset AS
SELECT * FROM  kre_report.hrp_bicillin
EXCEPT
SELECT T1.*
FROM kre_report.hrp_bicillin T1, 
emr_encounter T2,
emr_encounter_dx_codes T3
where T1.patient_id = T2.patient_id
AND T2.id = T3.encounter_id
AND T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
and T1.date = T2.date
order by patient_id;


CREATE TABLE kre_report.hrp_bicillin_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx = 0 THEN 1 END) rx_bicillin_1_yr,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_bicillin_2yr,
count(*) rx_bicillin_e
FROM kre_report.hrp_bicillin_subset T1
GROUP BY patient_id;

-- # rx_suboxone_2yr	total number of prescriptions for suboxone in past 2 years

CREATE TABLE kre_report.hrp_suboxone AS
SELECT patient_id, name, date, abs(date_part('year', age(date, now()::date))) yrs_since_rx
FROM emr_prescription T1
WHERE (T1.name ilike '%suboxone%' 
or T1.name ilike 'buprenorphine%naloxone%' 
or T1.name ilike '%zubsolv%'
or T1.name ilike '%bunavail%'
or T1.name ilike '%cassipa%')
GROUP BY patient_id, name, date;

CREATE TABLE kre_report.hrp_suboxone_values AS
SELECT T1.patient_id,
COUNT(CASE WHEN yrs_since_rx <= 1 THEN 1 END )rx_suboxone_2yr
FROM kre_report.hrp_suboxone T1
GROUP BY patient_id;


-- Gather up all of creatinine tests and last value
CREATE TABLE kre_report.hrp_creatinine_most_recent AS 
SELECT T1.patient_id, T2.test_name, max_creat_date as creat_recent_date, array_agg(distinct(result_string)) creat_recent_result
FROM emr_labresult T1, 
conf_labtestmap T2,
(select patient_id, max(date) max_creat_date from emr_labresult l, conf_labtestmap c where l.native_code = c.native_code and test_name = 'creatinine' group by patient_id) T3
WHERE T1.native_code = T2.native_code
AND T1.patient_id = T3.patient_id
AND test_name = 'creatinine'
AND T1.date = T3.max_creat_date
AND date >= '01-01-2006'
GROUP BY T1.patient_id, T2.test_name, max_creat_date;


-- Get date of most recent chlam and/or gon event for each patient
CREATE TABLE kre_report.hrp_chlam_gon_syph_max_dates AS 
SELECT T1.patient_id, T2.max_chlam_date, T3.max_gon_date
FROM kre_report.hrp_gon_chlam_events T1
LEFT JOIN (select patient_id, max(date) max_chlam_date from kre_report.hrp_gon_chlam_events where condition = 'chlamydia' group by patient_id) T2 on (T1.patient_id = T2.patient_id)
LEFT JOIN (select patient_id, max(date) max_gon_date from kre_report.hrp_gon_chlam_events where condition = 'gonorrhea' group by patient_id) T3 on (T1.patient_id = T3.patient_id)
GROUP BY T1.patient_id, T2.max_chlam_date, T3.max_gon_date;

-- Get most recent chlamydia result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE kre_report.hrp_chlam_most_recent AS 
SELECT T1.patient_id, T1.max_chlam_date chlam_recent_date, array_agg(distinct(T2.hef_name)) chlam_recent_result
FROM kre_report.hrp_chlam_gon_syph_max_dates T1,
kre_report.hrp_gon_chlam_events T2
WHERE T1.patient_id = T2.patient_id
AND condition = 'chlamydia'
AND hef_name ilike '%chlam%'
AND T1.max_chlam_date = T2.date
GROUP BY T1.patient_id, T1.max_chlam_date;

-- Get most recent gonorrhea result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE kre_report.hrp_gon_most_recent AS 
SELECT T1.patient_id, T1.max_gon_date gon_recent_date, array_agg(distinct(T2.hef_name)) gon_recent_result
FROM kre_report.hrp_chlam_gon_syph_max_dates T1,
kre_report.hrp_gon_chlam_events T2
WHERE T1.patient_id = T2.patient_id
AND condition = 'gonorrhea'
AND hef_name ilike '%gon%'
AND T1.max_gon_date = T2.date
GROUP BY T1.patient_id, T1.max_gon_date;

-- Get date of most recent syph event for each patient
CREATE TABLE kre_report.hrp_syph_max_dates AS 
SELECT T1.patient_id, max(date) as max_syph_date
FROM kre_report.hrp_syph_events T1
GROUP BY T1.patient_id;

-- Get most recent syphilis result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE kre_report.hrp_syph_most_recent AS 
SELECT T1.patient_id, T1.max_syph_date syph_recent_date, array_agg(distinct(T2.result_data)) syph_recent_result
FROM kre_report.hrp_syph_max_dates T1,
kre_report.hrp_syph_events T2
WHERE T1.patient_id = T2.patient_id
AND T1.max_syph_date = T2.date
GROUP BY T1.patient_id, T1.max_syph_date;

-- Get date of most recent hiv lab date for each patient
CREATE TABLE kre_report.hrp_hiv_max_dates AS 
SELECT T1.patient_id, max(date) as max_hiv_date
FROM kre_report.hrp_hiv_labs T1
GROUP BY patient_id;

-- Get most recent hiv result. Multiple results may appear on the same date so put these in an array.
CREATE TABLE kre_report.hrp_hiv_most_recent AS 
SELECT T1.patient_id, T1.max_hiv_date hiv_recent_date, array_agg(distinct(T2.result_data)) hiv_recent_result
FROM kre_report.hrp_hiv_max_dates T1,
kre_report.hrp_hiv_labs T2
WHERE T1.patient_id = T2.patient_id
AND T1.max_hiv_date = T2.date
GROUP BY T1.patient_id, T1.max_hiv_date;

-- Get the most recent weight for the patient
-- Get the most recent weight for the patient
CREATE TABLE kre_report.hrp_most_recent_weight AS 
SELECT DISTINCT ON (T1.patient_id) T1.patient_id, round(weight::numeric, 2) most_recent_weight, raw_weight, max_weight_date from
emr_encounter T1,
(select patient_id, max(date) max_weight_date from emr_encounter where weight is not null group by patient_id) T2,
kre_report.hrp_index_patients T3
WHERE T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.patient_id = T3.patient_id
AND T1.date = max_weight_date
AND T1.weight is not null
ORDER BY T1.patient_id;

-- Get most recent Truvada rx date
CREATE TABLE kre_report.hrp_truvada AS
SELECT patient_id, max(date) most_recent_truvada_rx
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
GROUP BY patient_id;


-- Bring together all of the fields and values
CREATE TABLE kre_report.hrp_index_data AS
SELECT T1.patient_id,
name,
mrn,
current_age,
pcp_name,
pcp_site,
sex_raw,
sex,
race_raw,
race_black,
race_caucasian,
home_language_raw,
english_lang,
has_language_data,
coalesce(years_of_data, 0) years_of_data,
coalesce(has_1yr_data, 0) has_1yr_data,
coalesce(has_2yr_data, 0) has_2yr_data,
coalesce(t_hiv_rna_1_yr, 0) t_hiv_rna_1_yr,
coalesce(t_hiv_test_2yr, 0) t_hiv_test_2yr,
coalesce(t_hiv_test_e, 0) t_hiv_test_e,
coalesce(t_hiv_test_dates_e, 0) t_hiv_test_dates_e,
coalesce(t_hiv_elisa_e, 0) t_hiv_elisa_e,
coalesce(acute_hiv_test_2yr, 0) acute_hiv_test_2yr,
coalesce(acute_hiv_test_e, 0) acute_hiv_test_e,
coalesce(rx_bicillin_1_yr, 0) rx_bicillin_1_yr,
coalesce(rx_bicillin_2yr, 0) rx_bicillin_2yr,
coalesce(rx_bicillin_e, 0) rx_bicillin_e,
coalesce(rx_suboxone_2yr, 0) rx_suboxone_2yr,
coalesce(t_pos_gon_test_2yr, 0) t_pos_gon_test_2yr,
coalesce(t_chla_test_t_e, 0) t_chla_test_t_e,
coalesce(syphilis_any_site_state_x_late_e, 0) syphilis_any_site_state_x_late_e,
coalesce(contact_w_or_expo_venereal_dis_e, 0) contact_w_or_expo_venereal_dis_e,
coalesce(hiv_counseling_2yr, 0) hiv_counseling_2yr,
coalesce(creat_recent_date, null) creat_last_date,
coalesce(creat_recent_result, null) creat_last_value,
coalesce(chlam_recent_date, null) chlam_last_date,
coalesce(chlam_recent_result, null) chlam_last_result,
coalesce(gon_recent_date, null) gon_last_date,
coalesce(gon_recent_result, null) gon_last_result,
coalesce(syph_recent_date, null) syph_last_date,
coalesce(syph_recent_result, null) syph_last_result,
coalesce(hiv_recent_date, null) hiv_last_date,
coalesce(hiv_recent_result, null) hiv_last_result,
coalesce(most_recent_weight, null) most_recent_weight,
coalesce(most_recent_truvada_rx, null) most_recent_truvada_rx
FROM kre_report.hrp_index_patients T1
LEFT JOIN kre_report.hrp_pat_encs T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.hrp_hiv_lab_values T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN kre_report.hrp_bicillin_values T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN kre_report.hrp_suboxone_values T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN kre_report.hrp_gon_chlam_values T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN kre_report.hrp_diags_values T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN kre_report.hrp_creatinine_most_recent T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN kre_report.hrp_chlam_most_recent T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN kre_report.hrp_gon_most_recent T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN kre_report.hrp_syph_most_recent T11 ON (T1.patient_id = T11.patient_id)
LEFT JOIN kre_report.hrp_hiv_most_recent T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN kre_report.hrp_most_recent_weight T13 ON (T1.patient_id = T13.patient_id)
LEFT JOIN kre_report.hrp_hiv_lab_result_dates T14 ON (T1.patient_id = T14.patient_id)
LEFT JOIN kre_report.hrp_truvada T15 ON (T1.patient_id = T15.patient_id)
;

-- IDENTIFY BICILLIN PATIENTS THAT 
-- DO NOT HAVE OTHER INDICATORS
CREATE TABLE kre_report.hrp_bicillin_exclusion_2 AS
SELECT patient_id, 'Bicillin Exclusion - No Other Indicators'::text bicillin_rx_exclusion
FROM kre_report.hrp_index_data
WHERE
t_hiv_rna_1_yr = 0 and
t_hiv_test_2yr = 0 and
t_hiv_test_e = 0 and
t_hiv_elisa_e = 0 and
acute_hiv_test_2yr = 0 and
acute_hiv_test_e = 0 and
rx_suboxone_2yr = 0 and
t_pos_gon_test_2yr = 0 and
t_chla_test_t_e = 0 and
syphilis_any_site_state_x_late_e = 0 and
contact_w_or_expo_venereal_dis_e = 0 and
hiv_counseling_2yr = 0 and
syph_last_date is null and
most_recent_truvada_rx is null
and rx_bicillin_e > 0;

-- JOIN TOGETHER DETAILS ON BICILLIN EXCLUSION
CREATE TABLE kre_report.hrp_bicillin_rx_exclusion_final AS
SELECT patient_id, array_agg(distinct(T1.bicillin_rx_exclusion)) bicillin_rx_exclusion
FROM (
	SELECT * FROM kre_report.hrp_bicillin_exclusion
	UNION 
	SELECT * FROM kre_report.hrp_bicillin_exclusion_2) T1
GROUP BY patient_id;

-- OVERWRITE BICILLIN VALUES FOR PATIENTS WITHOUT OTHER INDICATORS
UPDATE kre_report.hrp_index_data
SET rx_bicillin_1_yr = 0,
rx_bicillin_2yr = 0,
rx_bicillin_e = 0
WHERE patient_id IN (
	SELECT patient_id
	FROM kre_report.hrp_index_data
	WHERE
	t_hiv_rna_1_yr = 0 and
	t_hiv_test_2yr = 0 and
	t_hiv_test_e = 0 and
	t_hiv_elisa_e = 0 and
	acute_hiv_test_2yr = 0 and
	acute_hiv_test_e = 0 and
	rx_suboxone_2yr = 0 and
	t_pos_gon_test_2yr = 0 and
	t_chla_test_t_e = 0 and
	syphilis_any_site_state_x_late_e = 0 and
	contact_w_or_expo_venereal_dis_e = 0 and
	hiv_counseling_2yr = 0 and
	syph_last_date is null and
	most_recent_truvada_rx is null
	and rx_bicillin_e > 0)
;

-- Compute the sum to be used in the risk score calculation
CREATE TABLE kre_report.hrp_pat_sum_x_value AS
SELECT patient_id, 
(
(i.sex * c.sex) +
(i.race_black * c.race_black) +
(i.race_caucasian * c.race_caucasian) +
(i.english_lang * c.english_lang) +
(i.has_language_data * c.has_language_data) +
(i.years_of_data * c.years_of_data) +
(i.has_1yr_data * c.has_1yr_data) +
(i.has_2yr_data * c.has_2yr_data) +
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) + 
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) +
(i.t_hiv_test_e * c.t_hiv_test_e) +
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) +
(i.acute_hiv_test_e * c.acute_hiv_test_e) +
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) +
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) +
(i.t_chla_test_t_e * c.t_chla_test_t_e) +
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) +
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) +
(i.rx_bicillin_e * c.rx_bicillin_e) +
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) +
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) +
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) +
(i.hiv_counseling_2yr * c.hiv_counseling_2yr)
) as pat_sum_x_value
FROM kre_report.hrp_index_data i,
kre_report.hrp_coefficients c;

CREATE TABLE kre_report.hrp_risk_scores AS
SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value)))) hiv_risk_score
FROM kre_report.hrp_pat_sum_x_value;

CREATE TABLE kre_report.hrp_data_output AS
SELECT round(hiv_risk_score::numeric, 6) hiv_risk_score, pat_sum_x_value, T3.*, T4.bicillin_rx_exclusion
FROM kre_report.hrp_pat_sum_x_value T1
INNER JOIN kre_report.hrp_risk_scores T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN kre_report.hrp_index_data T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN kre_report.hrp_bicillin_rx_exclusion_final T4 ON (T1.patient_id = T4.patient_id);

----------------------------------------------------------------------------------------------------
-- CUSTOMIZATIONS START HERE
----------------------------------------------------------------------------------------------------


-- ATRIUS
-- ASSIGN A PERCENTILE TO HIGH RISK PATIENTS (BUT ONLY IF THEY ARE ASSOCIATED WITH AN ATRIUS SITE/PROVIDER )
CREATE TABLE kre_report.hrp_high_risk_percentile_with_provider as 
select ntile(100) over (order by hiv_risk_score desc) as percentile, * 
from kre_report.hrp_data_output
where pcp_site is not null 
and pcp_site not ilike 'EXTERNAL%'
and pcp_site not ilike 'UNKNOWN';

-- CHA
-- ASSIGN A PERCENTILE TO HIGH RISK PATIENTS (BUT ONLY IF THEY ARE ASSOCIATED WITH A CHA SITE/PROVIDER )
-- CREATE TABLE kre_report.hrp_high_risk_percentile_with_provider as 
-- select ntile(100) over (order by hiv_risk_score desc) as percentile, * 
-- from kre_report.hrp_data_output
-- where pcp_site is not null 
-- and pcp_site ilike 'CHA%';


-- ADD TRACKING FOR HIGH RISK PATIENTS TO BE CONTACTED
-- HIGH RISK CURRENTLY DEFINED AS TOP 1%
-- EXCLUDE PATIENTS THAT HAVE BEEN IDENTIFIED AS HAVING A TRUVADA RX
INSERT INTO kre_report.hrp_high_risk_pats_for_tracking (
SELECT hiv_risk_score, percentile, pat_sum_x_value, patient_id,mrn, now()::date as score_date, null::text as provider_contacted, null::date as contact_date
FROM kre_report.hrp_high_risk_percentile_with_provider
WHERE most_recent_truvada_rx is null
AND percentile <= 1);


-- QUERY TO RUN FOR DOUG TO GET ALL DETAILS FOR HIGH RISK PATIENTS
-- NEED TO CHANGE THE DATE TO REFLECT MOST RECENT RUN
--SELECT o.* 
--FROM kre_report.hrp_high_risk_pats_for_tracking t,
--kre_report.hrp_data_output o
--WHERE o.patient_id = t.patient_id
--AND score_date = '2019-09-17';

-- QUERY TO RUN FOR DOUG TO GET LIST OF HIGH RISK PATIENTS
-- THAT ARE !!NEW!!! TO THE HIGH RISK LIST
-- NEED TO CHANGE THE DATES TO REFLECT MOST RECENT RUN
-- SELECT o.* 
-- FROM kre_report.hrp_high_risk_pats_for_tracking t,
-- kre_report.hrp_data_output o
-- WHERE o.patient_id = t.patient_id
-- AND o.patient_id not in (select patient_id from kre_report.hrp_high_risk_pats_for_tracking where score_date < '2019-09-17')
-- AND score_date = '2019-09-17';

-- QUERY FOR ATRIUS FOR SPECIFIC ATRIUS SITES
-- SELECT o.* 
-- FROM kre_report.hrp_high_risk_pats_for_tracking t,
-- kre_report.hrp_data_output o
-- WHERE o.patient_id = t.patient_id
-- and (pcp_site in ('Cambridge Internal Medicine')
-- or pcp_site in ('Kenmore Internal Medicine')
-- or pcp_site in ('Copley Internal Medicine'))
-- AND score_date = '2019-10-22'
-- order by pcp_site, hiv_risk_score desc, name;

-- QUERY TO PROVIDE ATRIUS WITH INFO FOR PROVIDING ONE TIME NEXT APPT DATE REPORT
-- SELECT p.natural_key as atrius_patient_id, o.patient_id as esp_patient_id
-- FROM  kre_report.hrp_high_risk_pats_for_tracking t,
-- kre_report.hrp_data_output o,
-- emr_patient p
-- WHERE o.patient_id = t.patient_id
-- AND o.patient_id = p.id
-- AND p.id = t.patient_id
-- and (pcp_site in ('Cambridge Internal Medicine')
-- or pcp_site in ('Kenmore Internal Medicine')
-- or pcp_site in ('Copley Internal Medicine'))
-- AND score_date = '2019-10-22'
-- order by pcp_site, o.hiv_risk_score desc, name;

-- QUERY TO PROVIDE CONTACT LIST TO DOUG WITH NEXT APPT INFO PROVIDED BY ATRIUS
CREATE TABLE kre_report.hrp_high_risk_with_appt_for_dk AS 
SELECT 
split_part(split_part(nextimvis, ':', 2), ' at', 1)::date next_date,
ltrim(rtrim(split_part(nextimvis, 'at ', 2))) next_time,
split_part(split_part(nextimvis, 'with ', 2), ':', 1) next_provider,
o.* 
FROM kre_report.hrp_high_risk_pats_for_tracking t,
kre_report.hrp_data_output o,
kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius a
WHERE o.patient_id = t.patient_id
AND a.esp_patient_id = t.patient_id
and (pcp_site in ('Cambridge Internal Medicine')
or pcp_site in ('Kenmore Internal Medicine')
or pcp_site in ('Copley Internal Medicine'))
AND score_date = '2019-10-22'
AND nextimvis != 'No future Internal Medicine visits on file.'
order by next_date, next_time, pcp_site, hiv_risk_score desc, name;




-- CHA QUERY UNDER REVIEW
-- CREATE TABLE kre_report.cha_hiv_high_risk AS
-- SELECT t.percentile, o.hiv_risk_score, o.patient_id as esp_patient_id, 
-- name,
-- o.mrn,
-- current_age,
-- pcp_name,
-- pcp_site,
-- sex_raw,
-- race_raw,
-- home_language_raw,
-- has_language_data,
-- years_of_data,
-- has_1yr_data,
-- has_2yr_data,
-- t_hiv_rna_1_yr,
-- t_hiv_test_2yr,
-- t_hiv_test_e as t_hiv_test_ever,
-- t_hiv_test_dates_e t_hiv_test_dates_ever,
-- t_hiv_elisa_e,
-- acute_hiv_test_2yr,
-- acute_hiv_test_e acute_hiv_test_ever,
-- rx_bicillin_1_yr,
-- rx_bicillin_2yr,
-- rx_bicillin_e rx_bicilin_ever,
-- rx_suboxone_2yr,
-- t_pos_gon_test_2yr,
-- t_chla_test_t_e total_chlam_tests_ever,
-- syphilis_any_site_state_x_late_e,
-- contact_w_or_expo_venereal_dis_e,
-- hiv_counseling_2yr,
-- creat_last_date,
-- creat_last_value,
-- chlam_last_date,
-- chlam_last_result,
-- gon_last_date,
-- gon_last_result,
-- syph_last_date,
-- syph_last_result,
-- hiv_last_date,
-- hiv_last_result,
-- most_recent_weight,
-- most_recent_truvada_rx,
-- bicillin_rx_exclusion
-- FROM kre_report.hrp_high_risk_pats_for_tracking t,
-- kre_report.hrp_data_output o
-- WHERE o.patient_id = t.patient_id
-- AND score_date = '2019-10-01'
-- order by o.hiv_risk_score desc;

-- COPY kre_report.cha_hiv_high_risk  TO '/tmp/2019-10-01-cha_hiv_high_risk.csv' DELIMITER ',' CSV HEADER;

-- TABLE FOR ONE TIME LOAD FROM ATRIUS
-- Table: kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius

-- DROP TABLE kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius;

-- CREATE TABLE kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius
-- (
  -- atrius_patient_id character varying,
  -- esp_patient_id integer NOT NULL,
  -- atrius_mrn character varying,
  -- ept_2 character varying,
  -- nextimvis character varying,
  -- CONSTRAINT hrp_high_risk_pats_for_tracking_appts_from_atrius_pkey PRIMARY KEY (esp_patient_id)
-- )
-- WITH (
  -- OIDS=FALSE
-- );
-- ALTER TABLE kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius
  -- OWNER TO esp30;

COPY kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius(atrius_patient_id, esp_patient_id, atrius_mrn, ept_2, nextimvis) 
FROM '/tmp/2019-10-28-atrius-ids-for-next-appt-with-NEXTIMVIS.csv' DELIMITER ',' CSV HEADER;


select nextimvis, split_part(split_part(nextimvis, 'with ', 2), ':', 1) next_provider, split_part(split_part(nextimvis, ':', 2), ' at', 1)::date next_date, ltrim(rtrim(split_part(nextimvis, 'at ', 2))) next_time
from kre_report.hrp_high_risk_pats_for_tracking_appts_from_atrius
where nextimvis != 'No future Internal Medicine visits on file.'
order by next_date, next_time, next_provider












