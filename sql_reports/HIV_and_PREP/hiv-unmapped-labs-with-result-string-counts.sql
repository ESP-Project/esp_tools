drop table if exists temp_hiv_unmapped;

-- Define search strings here
create table temp_hiv_unmapped as
select e.count, e.native_name, e.native_code, procedure_name, max(l.date) as max_lab_date
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
and (e.native_name ~* '(hiv|immuno|cd4)' or l.procedure_name ~* '(hiv|immuno|cd4)' )
group by e.count, e.native_name, e.native_code, procedure_name;


DROP TABLE IF EXISTS temp_hiv_unmapped_with_result_strings;
CREATE TABLE temp_hiv_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, T2.max_lab_date, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_hiv_unmapped S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name, T2.max_lab_date 
  from emr_labresult T1,
  temp_hiv_unmapped T2
  where T1.native_code = T2.native_code 
  group by T1.native_code, T1.native_name, T1.procedure_name, T2.max_lab_date) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, T2.max_lab_date
ORDER BY T2.native_code, T2.native_name;


\COPY temp_hiv_unmapped_with_result_strings  TO '/tmp/hiv_unmapped_labs_with_result_strings.csv' DELIMITER ',' CSV HEADER;


drop table temp_hiv_unmapped;
drop table temp_hiv_unmapped_with_result_strings;