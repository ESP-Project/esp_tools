-- Table: public.hiv_risk_bicillin_meds

CREATE SCHEMA IF NOT EXISTS hiv_rpt;

DROP TABLE IF EXISTS public.hiv_risk_bicillin_meds;

CREATE TABLE IF NOT EXISTS public.hiv_risk_bicillin_meds
(
    id SERIAL PRIMARY KEY,
    name character varying(500) NOT NULL,
    qualifying_med integer NOT NULL,
    dose character varying(100) NOT NULL,
    quantity character varying(100) NOT NULL,
    quantity_float character varying(25) NOT NULL,
    directions character varying(500) NOT NULL,
    match_type character varying(100) NOT NULL,
	CONSTRAINT bicillin_med_uniq UNIQUE (name, dose, quantity, quantity_float, directions, match_type)
);

