-- Get all the male patients that have **ever** been defined as MSM

-- Men who have sex with men in the clinical systems associated with the EHR. 
-- Demographics for sex and gender will be used to define men. 
-- Men who have sex with men (MSM) will be identified by some combo of sex, gender identity, sexual orientation, gender of sex partners, and pharyngeal or rectal testing for STIs. 
-- The age range of interest for all EHR will be adult MSM, i.e., MSM aged 15 or older. We will not have an upper age restriction for these data.

-- Must be 15 on date of interest

-- BMC 01/01/2014
-- CHA 01/1/2012
-- FENWAY 01/01/2013
-- END DATE 09/01/2023


-- REPLACEMENTS
-- "REPLACE WITH START DATE & END DATE"


--
-- MALE IDENTIFICATION
-- 
DROP TABLE IF EXISTS kre_report.stirrup_all_males;
CREATE TABLE kre_report.stirrup_all_males as
SELECT id as patient_id, 'MALE' as sex, gender, gender_identity, birth_sex, sex_orientation
FROM emr_patient
WHERE (gender in ('M', 'MALE')) 
UNION
SELECT id as patient_id, 'MALE' as sex, gender, gender_identity, birth_sex, sex_orientation
FROM emr_patient
WHERE birth_sex in ('MALE')
-- Remove patients that list anything other than MALE or NULL for birth_sex
EXCEPT
SELECT id as patient_id, 'MALE' as sex, gender, gender_identity, birth_sex, sex_orientation
FROM emr_patient
WHERE (birth_sex not in ('MALE', '') and birth_sex is not null)
-- Remove any patients where gender_identity is NOT MALE or NULL
EXCEPT
SELECT id as patient_id, 'MALE' as sex, gender, gender_identity, birth_sex, sex_orientation
FROM emr_patient
WHERE (gender_identity not in ('MALE', '') and gender_identity is not null)
-- Exclude patients where gender is reported as 'FEMALE'
EXCEPT
SELECT id as patient_id, 'MALE' as sex, gender, gender_identity, birth_sex, sex_orientation
FROM emr_patient
WHERE (gender in ('F', 'FEMALE'));


--
-- MSM IDENTIFICATION
-- 
DROP TABLE IF EXISTS kre_report.stirrup_msm;
CREATE TABLE kre_report.stirrup_msm as
SELECT DISTINCT patient_id, msm_yes as msm_indicator FROM (
    -- Identify MSM using sexual orientation
	SELECT patient_id, 
	CASE WHEN upper(sex_orientation) in ('BISEXUAL', 'LESBIAN OR GAY', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL', 'GAY', 'PANSEXUAL') then 1
		 END as msm_yes
	FROM kre_report.stirrup_all_males
	UNION
	-- Identify MSM using gender of sex partners
	-- If patient has **ANY** history of MSM consider them MSM
	SELECT DISTINCT T1.patient_id, 1 as msm_yes
	FROM emr_socialhistory T1
	INNER JOIN kre_report.stirrup_all_males T2 ON (T1.patient_id = T2.patient_id)
	WHERE upper(T1.sex_partner_gender) in ('MALE', 'MALE + FEMALE', 'FEMALE + MALE')  ) S1
WHERE msm_yes = 1;

DROP TABLE IF EXISTS kre_report.stirrup_sti_test_inclusion_pats;
CREATE TABLE kre_report.stirrup_sti_test_inclusion_pats AS
SELECT DISTINCT T1.patient_id, 1 as rectal_or_pharyn_clam_gon_test_ever 
--select T1.patient_id, specimen_source, T2.native_code, native_name, procedure_name
FROM kre_report.stirrup_all_males T1
INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code)
INNER JOIN hef_event T4 ON (T1.patient_id = T4.patient_id and T2.patient_id = T4.patient_id and T2.id = T4.object_id) --hef event must exist 
WHERE test_name in ('gonorrhea', 'chlamydia')
--REPLACE WITH START DATE & END DATE
AND T4.date >= '2012-01-01'
AND T4.date < '2023-09-01'
AND (native_name ~* '(PHARYNGEAL|THROAT|RECTAL|ANAL|RECTUM|ANUS)'
OR T2.native_code ~* '(PHARYNGEAL|THROAT|RECTAL|ANAL|RECTUM|ANUS)'
OR procedure_name ~* '(PHARYNGEAL|THROAT|RECTAL|ANAL|RECTUM|ANUS)'
OR specimen_source ~* '(PHARYNGEAL|THROAT|RECTUM|RECT|ANAL|ANUS)'
);


--
-- INDEX PATIENTS
-- 
DROP TABLE IF EXISTS kre_report.stirrup_index_pats;
CREATE TABLE kre_report.stirrup_index_pats AS
SELECT patient_id 
FROM kre_report.stirrup_msm
UNION 
SELECT patient_id
FROM kre_report.stirrup_sti_test_inclusion_pats;

--
-- ALL GONORRHEA/CHLAMYDIA LAB AND HEF EVENTS FOR INDEX PATIENTS
-- NORMALIZE SPECIMEN SOURCES FOR CHLAM/GON TESTS
-- 
DROP TABLE IF EXISTS kre_report.stirrup_all_chlam_gon_labs_events;
CREATE TABLE kre_report.stirrup_all_chlam_gon_labs_events AS 
SELECT T3.patient_id, T2.date as hef_date, T2.name as hef_name, 
CASE WHEN specimen_source ~* '(RECTUM|RECT|ANAL|ANUS)' then 'RECTAL'
WHEN specimen_source ~* '(PHARYNGEAL|THROAT)' then 'THROAT'
WHEN specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG|UREATHRA|URETHRA)' then 'UROG'
WHEN (native_name ~* '(PHARYNGEAL|THROAT)' or T1.native_code ~* '(PHARYNGEAL|THROAT)' or procedure_name ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (native_name ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR T1.native_code ~* '(RECTAL|ANAL|RECTUM|ANUS)' OR procedure_name ~* '(RECTAL|ANAL|RECTUM|ANUS)') THEN 'RECTAL'
WHEN (native_name ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
                       OR T1.native_code  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)'
					   OR procedure_name  ~* '(GENITAL|URINE|URN|URI|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP|ENDOCERVICAL)' ) THEN 'UROG'
ELSE 'OTHER_OR_UNKNOWN' END specimen_source,
specimen_source specimen_source_orig,
T1.id as lab_id, T2.object_id, T1.native_code, T1.native_name, T1.procedure_name
FROM emr_labresult T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id)
INNER JOIN kre_report.stirrup_index_pats T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
WHERE 
(T2.name ilike 'lx:gonorrhea%' or T2.name ilike 'lx:chlamydia%')
--REPLACE WITH START DATE & END DATE
AND T2.date >= '2012-01-01'
AND T2.date < '2023-09-01';


-- VALIDATION
--select * from kre_report.stirrup_all_chlam_gon_labs_events
--where specimen_source_orig is null and specimen_source is null
--limit 1000;

--select patient_id from kre_report.stirrup_sti_test_inclusion_pats
--EXCEPT 
--select patient_id from kre_report.stirrup_all_chlam_gon_labs_events where specimen_source in ('RECTAL', 'THROAT')

--select patient_id from kre_report.stirrup_all_chlam_gon_labs_events where specimen_source in ('RECTAL', 'THROAT')
--EXCEPT
--select patient_id from kre_report.stirrup_sti_test_inclusion_pats

-- select count(*), specimen_source_orig
-- from kre_report.stirrup_all_chlam_gon_labs_events
-- where specimen_source = 'OTHER_OR_UNKNOWN'
-- group by specimen_source_orig

DROP TABLE IF EXISTS kre_report.stirrup_gon_chlam_testing;
CREATE TABLE kre_report.stirrup_gon_chlam_testing AS
SELECT T1.patient_id,
T1.hef_date as date_of_interest,
MAX(CASE WHEN hef_name ilike 'lx:gonorrhea:%' THEN 1 ELSE 0 END) any_gon_any_site,
MAX(CASE WHEN hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'RECTAL' THEN 1 ELSE 0 END) any_gon_rectal,
MAX(CASE WHEN hef_name ilike 'lx:gonorrhea:%' and specimen_source = 'THROAT' THEN 1 ELSE 0 END) any_gon_throat,
MAX(CASE WHEN hef_name = 'lx:gonorrhea:positive' and specimen_source = 'RECTAL' THEN 1 ELSE 0 END) pos_gon_rectal,
MAX(CASE WHEN hef_name = 'lx:gonorrhea:positive' and specimen_source = 'THROAT' THEN 1 ELSE 0 END) pos_gon_throat,
MAX(CASE WHEN hef_name = 'lx:gonorrhea:positive' and specimen_source = 'UROG' THEN 1 ELSE 0 END) pos_gon_urog,
MAX(CASE WHEN hef_name = 'lx:gonorrhea:positive' and specimen_source = 'OTHER_OR_UNKNOWN' THEN 1 ELSE 0 END) pos_gon_oth_or_unk,
MAX(CASE WHEN hef_name ilike 'lx:chlamydia:%' THEN 1 ELSE 0 END) any_chlam_any_site,
MAX(CASE WHEN hef_name ilike 'lx:chlamydia:%' and specimen_source = 'RECTAL' THEN 1 ELSE 0 END) any_chlam_rectal,
MAX(CASE WHEN hef_name ilike 'lx:chlamydia:%' and specimen_source = 'THROAT' THEN 1 ELSE 0 END) any_chlam_throat,
MAX(CASE WHEN hef_name = 'lx:chlamydia:positive' and specimen_source = 'RECTAL' THEN 1 ELSE 0 END) pos_chlam_rectal,
MAX(CASE WHEN hef_name = 'lx:chlamydia:positive' and specimen_source = 'THROAT' THEN 1 ELSE 0 END) pos_chlam_throat,
MAX(CASE WHEN hef_name = 'lx:chlamydia:positive' and specimen_source = 'UROG' THEN 1 ELSE 0 END) pos_chlam_urog,
MAX(CASE WHEN hef_name = 'lx:chlamydia:positive' and specimen_source = 'OTHER_OR_UNKNOWN' THEN 1 ELSE 0 END) pos_chlam_oth_or_unk
FROM kre_report.stirrup_all_chlam_gon_labs_events T1
GROUP BY T1.patient_id, date_of_interest;


--
-- GATHER UP CASES OF INTEREST
--

DROP TABLE IF EXISTS kre_report.stirrup_all_cases_of_interest;
CREATE TABLE kre_report.stirrup_all_cases_of_interest AS
SELECT T2.patient_id, date as case_date, condition 
FROM nodis_case T1 
INNER JOIN kre_report.stirrup_index_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE condition in ('gonorrhea', 'chlamydia', 'syphilis')
--REPLACE WITH START DATE & END DATE
AND date >= '2012-01-01'
AND date < '2023-09-01';

DROP TABLE IF EXISTS kre_report.stirrup_esp_cases;
CREATE TABLE kre_report.stirrup_esp_cases AS
SELECT patient_id, case_date as date_of_interest,
MAX(CASE WHEN condition = 'gonorrhea' THEN 1 ELSE 0 END) gon_esp_case,
MAX(CASE WHEN condition = 'chlamydia' THEN 1 ELSE 0 END) chlam_esp_case,
MAX(CASE WHEN condition = 'syphilis' THEN 1 ELSE 0 END) syph_esp_case
FROM kre_report.stirrup_all_cases_of_interest
GROUP BY patient_id, date_of_interest;

-- VALIDATION TO CHECK IF ALL CLIN ENC YEARS EXIST
-- select count(*), date_trunc('month', date)::date as datemonth
-- from gen_pop_tools.clin_enc
-- group by datemonth
-- order by datemonth;



--
-- CLINICAL ENCOUNTERS
--
DROP TABLE IF EXISTS kre_report.stirrup_clinical_encs;
CREATE TABLE kre_report.stirrup_clinical_encs AS
SELECT DISTINCT T2.patient_id, date as date_of_interest, 1 as clin_enc
FROM gen_pop_tools.clin_enc T1
INNER JOIN kre_report.stirrup_index_pats T2 ON (T1.patient_id = T2.patient_id)
WHERE source = 'enc'
--REPLACE WITH START DATE & END DATE
AND date >= '2012-01-01'
AND date < '2023-09-01';


--
-- HIV LAB TESTS
--
DROP TABLE IF EXISTS kre_report.stirrup_hiv_labs;
CREATE TABLE kre_report.stirrup_hiv_labs AS
SELECT T2.patient_id, T1.date as hiv_lab_date, T3.test_name--, T1.result_string
FROM emr_labresult T1
INNER JOIN kre_report.stirrup_index_pats T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T1.native_code = T3.native_code)
WHERE T3.test_name ilike '%hiv%'
AND T3.test_name not in ('hiv not a test')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
--REPLACE WITH START DATE & END DATE
AND date >= '2012-01-01'
AND date < '2023-09-01'
GROUP BY T2.patient_id, T1.date, T3.test_name--, T1.result_string
;

DROP TABLE IF EXISTS kre_report.stirrup_hiv_labs_categorized;
CREATE TABLE kre_report.stirrup_hiv_labs_categorized AS
SELECT patient_id,
hiv_lab_date as date_of_interest,
MAX(CASE WHEN test_name = 'hiv_elisa' THEN 1 ELSE 0 END) hiv_elisa_test,
MAX(CASE WHEN test_name in ('hiv_ab_diff', 'hiv_wb', 'hiv_multispot') THEN 1 ELSE 0 END) hiv_confirm_test,
MAX(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 ELSE 0 END) hiv_rna_viral_test,
MAX(CASE WHEN test_name = 'hiv_pcr' THEN 1 ELSE 0 END) hiv_pcr_test,
MAX(CASE WHEN test_name = 'hiv_ag_ab' THEN 1 ELSE 0 END) hiv_ag_ab_test
FROM kre_report.stirrup_hiv_labs
GROUP BY patient_id, date_of_interest;


--VALIDATION
-- select count(*), result_string
-- from kre_report.stirrup_hiv_labs
-- group by result_string
-- order by result_string desc;


--
-- SYPHILIS LAB TESTS
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
--
DROP TABLE IF EXISTS kre_report.stirrup_syph_labs;
CREATE TABLE kre_report.stirrup_syph_labs AS
SELECT T2.patient_id, T1.date as date_of_interest, 1 as syph_lab_any --, T1.result_string
FROM emr_labresult T1
INNER JOIN kre_report.stirrup_index_pats T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN conf_labtestmap T3 ON (T1.native_code = T3.native_code)
WHERE T3.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia', 'tppa-csf', 'fta-abs-csf')
AND upper(result_string) not in ('', 'TEST NOT DONE', 'TNP', 'TEST NOT PERFORMED', 'TNP', 'DECLINED', 'NOT DONE', 'PT DECLINED', 'NOT PERFORMED',  
    					  'NOT TESTED', 'TEST', 'NOT INDICATED')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
--REPLACE WITH START DATE & END DATE
AND date >= '2012-01-01'
AND date < '2023-09-01'
GROUP BY T2.patient_id, T1.date --, T1.result_string
;

--VALIDATION
-- select count(*), result_string
-- from kre_report.stirrup_syph_labs
-- group by result_string
-- order by result_string desc;

--
-- HIV MEDS
--
DROP TABLE IF EXISTS kre_report.stirrup_hiv_meds;
CREATE TABLE kre_report.stirrup_hiv_meds AS
SELECT DISTINCT T2.patient_id, T1.name, T3.name as hef_name, T1.date, T1.quantity, T1.quantity_float, T1.refills, status,T1.start_date, T1.end_date, 
CASE WHEN refills~E'^\\d+$' THEN refills::real END as refills_numeric,
CASE WHEN refills~E'^\\d+$' THEN ((refills::real +1) * quantity_float) 
     WHEN refills is null THEN quantity_float END as total_quantity_per_rx,
CASE WHEN T3.name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
                      'rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END rx_prep,
CASE WHEN T3.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
                      'rx:hiv_cabotegravir_er_600') THEN 1 ELSE 0 END other_hiv_rx_non_truvada
FROM emr_prescription T1
JOIN kre_report.stirrup_index_pats T2 on (T1.patient_id = T2.patient_id)
JOIN hef_event T3 on (T1.patient_id = T3.patient_id and T1.id = T3.object_id)
WHERE T3.name ilike 'rx:hiv%'
--REPLACE WITH START DATE & END DATE
AND T1.date >= '2012-01-01'
AND T1.date < '2023-09-01';

DROP TABLE IF EXISTS kre_report.stirrup_hiv_meds_output_prep;
CREATE TABLE kre_report.stirrup_hiv_meds_output_prep AS
SELECT patient_id, date as date_of_interest,
MAX(rx_prep) as rx_prep_indicator,
SUM(CASE WHEN rx_prep = 1 THEN 1 ELSE 0 END) as rx_prep_num_rx,
SUM(CASE WHEN rx_prep = 1 THEN refills_numeric ELSE 0 END) as rx_prep_refills,
SUM(CASE WHEN rx_prep = 1 THEN total_quantity_per_rx ELSE 0 END) as rx_prep_quantity,
MAX(other_hiv_rx_non_truvada) as rx_hiv_other_indicator,
SUM(CASE WHEN other_hiv_rx_non_truvada = 1 THEN 1 ELSE 0 END) as rx_hiv_other_num_rx,
SUM(CASE WHEN other_hiv_rx_non_truvada = 1 THEN refills_numeric ELSE 0 END) as rx_hiv_other_refills,
SUM(CASE WHEN other_hiv_rx_non_truvada = 1 THEN total_quantity_per_rx ELSE 0 END) as rx_hiv_other_quantity
FROM kre_report.stirrup_hiv_meds
GROUP BY patient_id, date;


--
-- BICILLIN MEDS
--

DROP TABLE IF EXISTS kre_report.stirrup_bicillin_meds;
CREATE TABLE kre_report.stirrup_bicillin_meds AS
SELECT count(distinct(T1.id)) as rx_bicillin_num_rx, T2.patient_id, 1 as rx_bicillin_indicator, T1.name, T1.date, T1.dose
FROM emr_prescription T1
INNER JOIN kre_report.stirrup_index_pats T2 ON (T1.patient_id = T2.patient_id)
--only include qualified meds
INNER JOIN hiv_risk_bicillin_meds T3 ON (T1.name = T3.name and T3.qualifying_med = 1)
WHERE 
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T1.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T1.dose is null or T1.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T1.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T1.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T1.quantity ilike (concat('%', T3.quantity, '%')))

)
--REPLACE WITH START DATE & END DATE
AND T1.date >= '2012-01-01'
AND T1.date < '2023-09-01'
GROUP BY T2.patient_id, T1.name, T1.date, T1.dose;


-- Get only values of interest for all sites
-- At CHA need to have 2 rx on the same day (to handle how CHA writes the prescriptions) for 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP'

DROP TABLE IF EXISTS kre_report.stirrup_bicillin_meds_updated;
CREATE TABLE kre_report.stirrup_bicillin_meds_updated AS
SELECT patient_id, rx_bicillin_indicator, date as date_of_interest FROM kre_report.stirrup_bicillin_meds
EXCEPT
SELECT patient_id, rx_bicillin_indicator, date as date_of_interest FROM kre_report.stirrup_bicillin_meds
    WHERE name in('PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP', 'PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSY') AND (dose in ('1.2') OR dose is null)
	AND rx_bicillin_num_rx < 2;



--
-- AZITHROMYCIN &  CEFTRIAXONE MEDS
--
DROP TABLE IF EXISTS kre_report.stirrup_az_cft_meds;
CREATE TABLE kre_report.stirrup_az_cft_meds AS
SELECT DISTINCT T2.patient_id, T1.date date_of_interest, 	 
MAX(CASE WHEN T1.name in (
    -- ATRIUS (not validated)
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	-- CHA (validated 02/08/2023)
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR', --old med forumulation that was deemed it should be included previously
	-- BMC  (validated 02/08/2023)
	'AZITHROMYCIN 1 GRAM ORAL PACKET',
	-- FENWAY (validated 02/09/2023)
	'AZITHROMYCIN 1G',
	'AZITHROMYCIN 1 GM ORAL PACK',
	'AZITHROMYCIN 1 GM ORAL PACKET',
	'AZITHROMYCIN 1 GM PACK',
	'AZITHROMYCIN 1 GRAM',
	'azithromycin 1 gram packet',
	'Med:  Zithromax 1gr slurry PO',
	'ZITHROMAX 1 GM ORAL PACKET',
	'ZITHROMAX 1 GM PACK',
	'ZITHROMAX POW 1GM PAK'	
	) 
	--CHA (validated 02/08/2023)
	OR (name in ('AZITHROMYCIN 250 MG PO TABS', 'AZITHROMYCIN 500 MG PO TABS') and dose = '1000')
	OR (name in ('AZITHROMYCIN 250 MG PO TABS') and quantity_float in (4, 8) and dose is null and (directions ilike '%4 tab%' 
	              or directions ilike '%four tab%'
	              or directions ilike '%1000%mg%' or directions ilike '%1%gm%'
                  or directions ilike '%gram%' or directions ilike '4%' or directions ilike '%all%at once%'
				  or directions ilike '%4 pill%' or directions ilike '%four pill%' or directions ilike '%partner%') )
	OR (name in ('ZITHROMAX 500 MG PO TABS') and quantity_float = 2 )
	OR (name in ('AZITHROMYCIN 500 MG PO TABS') and quantity_float in (2, 4) and dose is null and (directions ilike '%2 tab%' 
	              or directions ilike '%two tab%'
	              or directions ilike '%1000%mg%' or directions ilike '%1%gm%'
                  or directions ilike '%gram%' or directions ilike '2%' or directions ilike '%all%at once%'
				  or directions ilike '%2 pill%' or directions ilike '%two pill%' or directions ilike '%both%' or directions ilike '%partner%') )
	--BMC (validated 02/010/2023)
	OR (name in ('AZITHROMYCIN 250 MG TABLET', 'AZITHROMYCIN 500 MG TABLET', 'ZITHROMAX 500 MG TABLET', 'ZITHROMAX Z-PAK 250 MG TABLET') and dose = '1000 mg')
	OR (name in ('AZITHROMYCIN 250 MG TABLET') and quantity_float = 4 and dose is null and (directions ilike '%4 tab%' 
	              or directions ilike '%four tab%'
	              or directions ilike '%1000%mg%' or directions ilike '%1%gm%'
                  or directions ilike '%gram%' or directions ilike '4%' or directions ilike '%all%at once%'
				  or directions ilike '%4 pill%' or directions ilike '%four pill%' or directions ilike '%partner%') )
	OR (name in ('AZITHROMYCIN 500 MG TABLET') and quantity_float = 2 and dose is null and (directions ilike '%2 tab%' 
	              or directions ilike '%two tab%'
	              or directions ilike '%1000%mg%' or directions ilike '%1%gm%'
                  or directions ilike '%gram%' or directions ilike '2%' or directions ilike '%all%at once%'
				  or directions ilike '%2 pill%' or directions ilike '%two pill%' or directions ilike '%both%' or directions ilike '%partner%') ) 
		
	--FENWAY (validated 02/09/2023)
	OR (name in ('Zithromax Dose') and 	( (quantity ilike '%1 g%' or quantity ilike '%1g%' or quantity ilike '%1000 mg%' or quantity ilike '%2%500mg%') or quantity in ('2 tabs 500 mg each', '500 mg tablet x2' ) ) )
	OR (name in ('AZITHROMYCIN 250 MG PO TABS') and dose in ('1000 mg') ) --added 09/15/2023
	OR (name in ('AZITHROMYCIN 500 MG PO TABS') and dose in ('1000 mg') ) --added 09/15/2023
	OR (name in ('AZITHROMYCIN 250 MG ORAL TABLET', 'AZITHROMYCIN 250 MG ORAL TABS', 'azithromycin 250 mg tablet', 'AZITHROMYCIN 250 MG TABS', 'ZITHROMAX 250 MG ORAL TABLET',
	             'ZITHROMAX 250 MG ORAL TABS', 'ZITHROMAX TAB 250MG', 'ZITHROMAX Z-PAK 250 MG ORAL TABLET') and quantity_float in (4,8) )
	OR (name in ('AZITHROMYCIN 500MG', 'AZITHROMYCIN 500 MG ORAL TABLET', 'AZITHROMYCIN 500 MG ORAL TABS', 'azithromycin 500 mg tablet', 'AZITHROMYCIN 500MG TABLETS', 
	             'AZITHROMYCIN 500 MG TABS', 'ZITHROMAX 500 MG ORAL TABLET', 'ZITHROMAX 500 MG TABS', 'ZITHROMAX TAB 500 MG (AZITHROMYCIN)', 'ZITHROMAX TAB 500MG (AZITHROMYCIN)') and quantity_float in (2, 4) )		
	THEN 1 end ) rx_azith_indicator,
MAX(CASE WHEN T1.name in ( 
	-- CHA (validated 02/09/2023)
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR',
	'ROCEPHIN 500 MG IJ SOLR', --added 09/15/2023
	-- BMC (validated 02/09/2023)
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)', --added 09/15/2023
	'CEFTRIAXONE 500 MG/5 ML IJ (RECONSTITUTED VIAL)', --added 09/15/2023
	'CEFTRIAXONE 500 MG IM INJECTION', --added 09/15/2023
	'CEFTRIAXONE 500 MG SOLUTION FOR INJECTION', --added 09/15/2023
	'CEFTRIAXONE SODIUM 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)', --added 09/15/2023
	'CEFTRIAXONE SODIUM 500 MG/5 ML IJ (RECONSTITUTED VIAL)', --added 09/15/2023
	-- FENWAY (validated 02/09/2023)
	'CEFTRIAXONE 250MG',
	'ceftriaxone 250 mg recon soln',
	'CEFTRIAXONE EA 250MG',
	'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)',
	'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED',
	'CEFTRIAXONE SODIUM 250 MG INJ SOLR',
	'CEFTRIAXONE SODIUM 250 MG SOLR',
	'Med:  Rocephin  250mg IM',
	'Rocephin 250 mg',
	'ROCEPHIN 250 MG',
	'ROCEPHIN 250 MG INJ RECON SOL',
	'ROCEPHIN 250 MG INJ SOLR',
	'ROCEPHIN INJ 250MG',
	'ceftriaxone 500 mg recon soln', -- added 09/15/2023
	'CEFTRIAXONE EA 500MG', -- added 09/15/2023
	'CEFTRIAXONE SODIUM 500 MG IJ SOLR', -- added 09/15/2023
	'CEFTRIAXONE SODIUM 500 MG INJECTION SOLUTION RECONSTITUTED', -- added 09/15/2023
	'CEFTRIAXONE SODIUM POWD (CEFTRIAXONE SODIUM) 500MG', -- added 09/15/2023
	'ROCEPHIN 500 MG INJECTION SOLUTION RECONSTITUTED', -- added 09/15/2023
	'ROCEPHIN 500 MG INJ SOLR' -- added 09/15/2023
	
	) 
	-- CHA
	OR (name in ('CEFTRIAXONE IVPB') and dose in ('250', '500') )
	-- BMC (validated 02/09/2023)
	OR (name in ('CEFTRIAXONE IM INJECTION <=250 MG', 'CEFTRIAXONE IM SYR >= 250 MG') and dose = '250 mg' )
	OR (name in ('CEFTRIAXONE IVPB IN 50 ML') and dose in ('.25 g', '250 mg' ) )
	OR (name in ('CEFTRIAXONE 1 GRAM SOLUTION FOR INJECTION') and dose in ('125 mg', '250 mg', '500 mg') ) --added 09/15/2023
	-- FENWAY (validated 02/09/2023)
	OR (name in ('CEFTRIAXONE SODIUM POWD', 'CEFTRIAXONE SODIUM POWDER', 'CEFTRIAXONE SODIUM PWDR') and directions ilike '%250mg IM %' )
	OR (name in ('CEFTRIAXONE SODIUM POWD', 'CEFTRIAXONE SODIUM POWDER', 'CEFTRIAXONE SODIUM PWDR', 'Rocephin (Ceftriaxone) Injection') 
	     and (quantity ilike '%250%' or quantity ilike '%125%' or quantity_float = 500) )
	then 1 end) rx_ceft_indicator
FROM emr_prescription T1
JOIN kre_report.stirrup_index_pats T2 on (T1.patient_id = T2.patient_id)
WHERE 
    ( name in (
	--CHA 
	'AZITHROMYCIN 250 MG PO TABS',
	'AZITHROMYCIN 500 MG PO TABS',
	'AZITHROMYCIN 1 G PO PACK',
	'ZITHROMAX 500 MG PO TABS',
	'ZITHROMAX 1 G PO PACK',
	'AZITHROMYCIN 2 G PO SUSR', --old med forumulation that was deemed it should be included previously
	'CEFTRIAXONE SODIUM 250 MG IJ SOLR',
	'ROCEPHIN 250 MG IJ SOLR', 	
    'ROCEPHIN 500 MG IJ SOLR',	
	'CEFTRIAXONE IVPB'
	--BMC 
	'AZITHROMYCIN 250 MG TABLET',
	'AZITHROMYCIN 500 MG TABLET',
	'AZITHROMYCIN 1 GRAM ORAL PACKET',
	'ZITHROMAX 500 MG TABLET',
	'ZITHROMAX Z-PAK 250 MG TABLET',
	'CEFTRIAXONE 1 GRAM SOLUTION FOR INJECTION',
	'CEFTRIAXONE 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)'
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE 500 MG/5 ML IJ (RECONSTITUTED VIAL)',
	'CEFTRIAXONE 500 MG IM INJECTION',
	'CEFTRIAXONE 500 MG SOLUTION FOR INJECTION',
	'CEFTRIAXONE IM INJECTION <=250 MG',
	'CEFTRIAXONE SODIUM 250 MG/2.5 ML IJ (RECONSTITUTED VIAL)',
	'CEFTRIAXONE SODIUM 500 MG/5 ML IJ (RECONSTITUTED VIAL)',
	'CEFTRIAXONE IM SYR >= 250 MG',
	'CEFTRIAXONE IVPB IN 50 ML',
	--FENWAY
	'AZITHROMYCIN 1 GM ORAL PACK',
	'AZITHROMYCIN 1 GM ORAL PACKET',
	'AZITHROMYCIN 1 GM PACK',
	'AZITHROMYCIN 1 GRAM',
	'azithromycin 1 gram packet',
	'AZITHROMYCIN 1G',
	'AZITHROMYCIN 250 MG ORAL TABLET',
	'AZITHROMYCIN 250 MG ORAL TABS',
	'azithromycin 250 mg tablet',
	'AZITHROMYCIN 250 MG TABS',
	'AZITHROMYCIN 500 MG ORAL TABLET',
	'AZITHROMYCIN 500 MG ORAL TABS',
	'azithromycin 500 mg tablet',
	'AZITHROMYCIN 500 MG TABS',
	'AZITHROMYCIN 500MG',
	'AZITHROMYCIN 500MG TABLETS',
	'Med:  Zithromax 1gr slurry PO',
	'ZITHROMAX 1 GM ORAL PACKET',
	'ZITHROMAX 1 GM PACK',
	'ZITHROMAX 250 MG ORAL TABLET',
	'ZITHROMAX 250 MG ORAL TABS',
	'ZITHROMAX 500 MG ORAL TABLET',
	'ZITHROMAX 500 MG TABS',
	'Zithromax Dose',
	'ZITHROMAX POW 1GM PAK',
	'ZITHROMAX TAB 250MG',
	'ZITHROMAX TAB 500 MG (AZITHROMYCIN)',
	'ZITHROMAX TAB 500MG (AZITHROMYCIN)',
	'ZITHROMAX Z-PAK 250 MG ORAL TABLET',	
	'CEFTRIAXONE 250MG',
	'ceftriaxone 250 mg recon soln'
	'CEFTRIAXONE EA 250MG',
	'CEFTRIAXONE SODIUM 250 MG/1ML INJ SOLR (CEFTRIAXONE SODIUM)',
	'CEFTRIAXONE SODIUM 250 MG INJECTION SOLUTION RECONSTITUTED',
	'CEFTRIAXONE SODIUM 250 MG INJ SOLR',
	'CEFTRIAXONE SODIUM 250 MG SOLR',
	'Med:  Rocephin  250mg IM',
	'Rocephin 250 mg',
	'ROCEPHIN 250 MG',
	'ROCEPHIN 250 MG INJ RECON SOL',
	'ROCEPHIN 250 MG INJ SOLR',
	'ROCEPHIN INJ 250MG',
	'CEFTRIAXONE SODIUM POWD', 
	'CEFTRIAXONE SODIUM POWDER', 
	'CEFTRIAXONE SODIUM PWDR',
	'Rocephin (Ceftriaxone) Injection',
	'ceftriaxone 500 mg recon soln',
	'CEFTRIAXONE EA 500MG',
	'CEFTRIAXONE SODIUM 500 MG IJ SOLR',
	'CEFTRIAXONE SODIUM 500 MG INJECTION SOLUTION RECONSTITUTED',
	'CEFTRIAXONE SODIUM POWD (CEFTRIAXONE SODIUM) 500MG',
	'ROCEPHIN 500 MG INJECTION SOLUTION RECONSTITUTED',
	'ROCEPHIN 500 MG INJ SOLR'
	) )
--REPLACE WITH START DATE & END DATE
AND T1.date >= '2012-01-01'
AND T1.date < '2023-09-01'
GROUP BY T2.patient_id, date;

DROP TABLE IF EXISTS kre_report.stirrup_az_cft_meds_final;
CREATE TABLE kre_report.stirrup_az_cft_meds_final AS
SELECT * FROM kre_report.stirrup_az_cft_meds WHERE rx_ceft_indicator = 1 or rx_azith_indicator = 1;



--
-- DIAGNOSIS CODES
--
DROP TABLE IF EXISTS kre_report.stirrup_dx_codes;
CREATE TABLE kre_report.stirrup_dx_codes AS
SELECT T3.patient_id, date date_of_interest, 
MAX(CASE WHEN dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7)' 
          OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1') THEN 1 ELSE 0 END) dx_syph_any,
MAX(CASE WHEN dx_code_id in ('icd9:091.1', 'icd10:A51.1') THEN 1 ELSE 0 END) dx_syph_anal,
MAX(CASE WHEN dx_code_id in ('icd9:098.7','icd10:A54.6') THEN 1 ELSE 0 END) dx_gonoc_inf_rectum,
MAX(CASE WHEN dx_code_id in ('icd9:098.6','icd10:A54.5') THEN 1 ELSE 0 END) dx_gonoc_pharyn,
MAX(CASE WHEN dx_code_id in ('icd9:099.52','icd10:A56.3') THEN 1 ELSE 0 END) dx_chlam_inf_rectum,
MAX(CASE WHEN dx_code_id in ('icd9:099.51','icd10:A56.4') THEN 1 ELSE 0 END) dx_chlam_pharyn,
MAX(CASE WHEN dx_code_id in ('icd9:099.1','icd10:A55') THEN 1 ELSE 0 END) dx_lymph_venereum,
MAX(CASE WHEN dx_code_id in ('icd9:099.0','icd10:A57') THEN 1 ELSE 0 END) dx_chancroid,
MAX(CASE WHEN dx_code_id in ('icd9:099.2', 'icd10:A58') THEN 1 ELSE 0 END) dx_granuloma_ingu,
MAX(CASE WHEN (dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1')) THEN 1 ELSE 0 END) dx_nongon_ureth,
MAX(CASE WHEN dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9') THEN 1 ELSE 0 END) dx_herpes_simp_w_compl,
MAX(CASE WHEN dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' THEN 1 ELSE 0 END) dx_genital_herpes,
MAX(CASE WHEN dx_code_id in ('icd9:078.11', 'icd10:A63.0') THEN 1 ELSE 0 END) dx_anogenital_warts,
MAX(CASE WHEN dx_code_id in ('icd9:569.41', 'icd10:K62.6') THEN 1 ELSE 0 END) dx_anorectal_ulcer,
MAX(CASE WHEN (dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.9')) THEN 1 ELSE 0 END) dx_unspec_std,
MAX(CASE WHEN dx_code_id in ('icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0', 'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9') THEN 1 ELSE 0 END) dx_pid,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') THEN 1 ELSE 0 END) dx_cont_w_exp_to_vd,
MAX(CASE WHEN (dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2')) THEN 1 ELSE 0 END) dx_high_risk_sex_behav,
MAX(CASE WHEN dx_code_id in ('icd9:V65.44','icd10:Z71.7') THEN 1 ELSE 0 END) dx_hiv_counsel,
MAX(CASE WHEN (dx_code_id ~ '^(icd10:F50.0)' or dx_code_id in ('icd9:307.1')) THEN 1 ELSE 0 END) dx_anorexia,
MAX(CASE WHEN dx_code_id in ('icd9:307.51','icd10:F50.2') THEN 1 ELSE 0 END) dx_bulimia,
MAX(CASE WHEN dx_code_id in ('icd9:307.50','icd10:F50.9') THEN 1 ELSE 0 END) dx_eat_disorder_nos,
MAX(CASE WHEN (dx_code_id ~ '^(icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.)' 
          OR dx_code_id in ('icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309.0', 'icd9:309.1', 'icd9:309.28', 'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32')) 
		  THEN 1 ELSE 0 END) dx_depression,
MAX(CASE WHEN dx_code_id ~ '^(icd9:314.|icd10:F90.)' THEN 1 ELSE 0 END) dx_att_def_disorder,
MAX(CASE WHEN dx_code_id in ('icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0') THEN 1 ELSE 0 END) dx_gend_iden_dis,
MAX(CASE WHEN dx_code_id in ('icd10:Z87.890') THEN 1 ELSE 0 END) dx_sex_reassignment,
MAX(CASE WHEN dx_code_id in ('icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:F64.1') THEN 1 ELSE 0 END) dx_transsexualism,
MAX(CASE WHEN dx_code_id in ('icd9:V61.21', 'icd10:Z62.810') THEN 1 ELSE 0 END) dx_couns_for_child_sex_abuse,
MAX(CASE WHEN (dx_code_id ~ '^(icd10:T18.5)' or dx_code_id in ('icd9:937')) THEN 1 ELSE 0 END) dx_foreign_body_anus,
MAX(CASE WHEN (dx_code_id ~ '^(icd9:649.0|icd10:F17.)' or dx_code_id in ('icd9:305.1', 'icd9:V15.82')) THEN 1 ELSE 0 END) dx_smoking_tob_use,
MAX(CASE WHEN (dx_code_id ~ '^(icd9:305.0|icd10:F10.1|icd10:F10.2)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92')) THEN 1 ELSE 0 END) dx_alcohol_depend_abuse,
MAX(CASE WHEN dx_code_id ~ '^(icd9:304.0|icd10:F11.|icd9:305.5)' THEN 1 ELSE 0 END) dx_opioid_depend_abuse,
MAX(CASE WHEN dx_code_id ~ '^(icd9:304.1|icd9:305.4|icd10:F13.)' THEN 1 ELSE 0 END) dx_sed_hypn_anxio_depend_abuse,
MAX(CASE WHEN dx_code_id ~ '^(icd9:304.2|icd9:305.6|icd10:F14.)' THEN 1 ELSE 0 END) dx_cocaine_depend_abuse,
MAX(CASE WHEN dx_code_id ~ '^(icd9:304.4|icd9:305.7|icd10:F15.)' THEN 1 ELSE 0 END) dx_amphet_stim_depend_abuse,
MAX(CASE WHEN dx_code_id ~ '^(icd9:304.9|icd9:292.8|icd10:F19)' THEN 1 ELSE 0 END) dx_oth_psycho_or_unspec_subs_depend_abuse
FROM emr_encounter T1
INNER JOIN emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
INNER JOIN kre_report.stirrup_index_pats T3 on (T1.patient_id = T3.patient_id) 
WHERE 
(

dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.0|icd10.A52.1|icd10:A52.2|icd10:A52.3|icd10:A52.4|icd10:A52.5|icd10:A52.6|icd10:A52.7|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:A63.|icd10:Z72.5|icd10:F50.0|icd9:296.2|icd9:296.3|icd10:F32.|icd10:F33.|icd9:314.|icd10:F90.|icd10:T18.5|icd9:649.0|icd10:F17.|icd9:305.0|icd10:F10.1|icd10:F10.2|icd9:304.0|icd10:F11.|icd9:305.5|icd9:304.1|icd9:305.4|icd10:F13.|icd9:304.2|icd9:305.6|icd10:F14.|icd9:304.4|icd9:305.7|icd10:F15.|icd9:304.9|icd9:292.8|icd10:F19)'
or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:091.1', 'icd10:A51.1', 'icd9:098.7','icd10:A54.6', 'icd9:098.6','icd10:A54.5',
                  'icd9:099.52','icd10:A56.3', 'icd9:099.51','icd10:A56.4', 'icd9:099.1','icd10:A55', 'icd9:099.0','icd10:A57', 'icd9:099.2', 'icd10:A58', 'icd10:N34.1',
				  'icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd9:078.11', 'icd10:A63.0', 'icd9:569.41', 'icd10:K62.6', 
				  'icd10:A64', 'icd9:099.9', 'icd9:614.0', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9', 'icd10:N72', 'icd10:N73.0',
				  'icd10:N73.2', 'icd10:N73.5', 'icd10:N73.9', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:V69.2', 'icd9:V65.44','icd10:Z71.7', 'icd9:307.1',
				  'icd9:307.51','icd10:F50.2', 'icd9:307.50','icd10:F50.9', 'icd9:296.82', 'icd9:300.4', 'icd9:301.12', 'icd9:308', 'icd9:309.0', 'icd9:309.1', 'icd9:309.28', 
				  'icd9:311', 'icd10:F34.1', 'icd10:F43.21', 'icd10:F43.23', 'icd10:F06.31', 'icd10:F06.32', 'icd9:302.0', 'icd9:302.6', 'icd9:302.85', 'icd10:F64.2',
				  'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0', 'icd10:Z87.890', 'icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 
				  'icd10:F64.1', 'icd9:V61.21', 'icd9:937', 'icd9:305.1', 'icd9:V15.82', 'icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:Z62.810')
)
--REPLACE WITH START DATE & END DATE
AND T1.date >= '2012-01-01'
AND T1.date < '2023-09-01'
GROUP BY T3.patient_id, date_of_interest;

-- Validation

/* select sum(dx_syph_any) dx_syph_any, sum(dx_syph_anal) dx_syph_anal, sum(dx_gonoc_inf_rectum) dx_gonoc_inf_rectum, sum(dx_gonoc_pharyn) dx_gonoc_pharyn,
sum(dx_chlam_inf_rectum) dx_chlam_inf_rectum, sum(dx_chlam_pharyn) dx_chlam_pharyn, sum(dx_lymph_venereum) dx_lymph_venereum, sum(dx_chancroid) dx_chancroid, sum(dx_granuloma_ingu) dx_granuloma_ingu,
sum(dx_nongon_ureth) dx_nongon_ureth, sum(dx_herpes_simp_w_compl) dx_herpes_simp_w_compl, sum(dx_genital_herpes) dx_genital_herpes, sum(dx_anogenital_warts) dx_anogenital_warts, sum(dx_anorectal_ulcer) dx_anorectal_ulcer,
sum(dx_unspec_std) dx_unspec_std, sum(dx_pid) dx_pid, sum(dx_cont_w_exp_to_vd) dx_cont_w_exp_to_vd, sum(dx_high_risk_sex_behav) dx_high_risk_sex_behav, sum(dx_hiv_counsel) dx_hiv_counsel, sum(dx_anorexia) dx_anorexia,
sum(dx_bulimia) dx_bulimia, sum(dx_eat_disorder_nos) dx_eat_disorder_nos, sum(dx_depression) dx_depression, sum(dx_att_def_disorder) dx_att_def_disorder, sum(dx_gend_iden_dis) dx_gend_iden_dis, sum(dx_transsexualism) dx_transsexualism,
sum(dx_couns_for_child_sex_abuse) dx_couns_for_child_sex_abuse, sum(dx_foreign_body_anus) dx_foreign_body_anus, sum(dx_smoking_tob_use) dx_smoking_tob_use, sum(dx_alcohol_depend_abuse) dx_alcohol_depend_abuse,
sum(dx_opioid_depend_abuse) dx_opioid_depend_abuse, sum(dx_sed_hypn_anxio_depend_abuse) dx_sed_hypn_anxio_depend_abuse, sum(dx_cocaine_depend_abuse) dx_cocaine_depend_abuse, sum(dx_amphet_stim_depend_abuse) dx_amphet_stim_depend_abuse,
sum(dx_oth_psycho_or_unspec_subs_depend_abuse) dx_oth_psycho_or_unspec_subs_depend_abuse
FROM kre_report.stirrup_dx_codes; */

--
-- GET ALL DATES OF INTEREST
--
DROP TABLE IF EXISTS kre_report.stirrup_index_pats_and_dates;
CREATE TABLE kre_report.stirrup_index_pats_and_dates AS 
SELECT patient_id, date_of_interest FROM kre_report.stirrup_gon_chlam_testing
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_esp_cases
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_clinical_encs
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_hiv_labs_categorized
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_syph_labs
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_hiv_meds_output_prep
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_bicillin_meds_updated
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_az_cft_meds_final
UNION
SELECT patient_id, date_of_interest FROM kre_report.stirrup_dx_codes;


-- 
-- PATIENT DEMOGRAPHICS
--

DROP TABLE IF EXISTS kre_report.stirrup_demog;
CREATE TABLE kre_report.stirrup_demog AS
SELECT T1.patient_id,
date_part('year', age(now(), date_of_birth)) current_age,
gender as sex, 
CASE WHEN birth_sex IS null then 'UNKNOWN' ELSE birth_sex END birth_sex,
CASE WHEN gender_identity IS null then 'UNKNOWN' ELSE gender_identity END gender_identity,
race,
ethnicity,
upper(city) as current_city,
zip5 as current_zip,
MAX(CASE WHEN sex_partner_gender in ('MALE + FEMALE', 'FEMALE + MALE') THEN 'MALE + FEMALE'
     WHEN sex_partner_gender in ('MALE') THEN 'MALE'
	 WHEN sex_partner_gender in ('FEMALE') THEN 'FEMALE'
	 ELSE 'UNKNOWN' END ) sex_partner_gender,
CASE WHEN sex_orientation IS null then 'UNKNOWN' ELSE sex_orientation END sex_orientation,
CASE WHEN T6.msm_indicator = 1 then 1 else 0 END as msm_indicator,
CASE WHEN rectal_or_pharyn_clam_gon_test_ever = 1 then 1 else 0 END rectal_or_pharyn_clam_gon_test_ever
FROM kre_report.stirrup_index_pats T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
-- Get the most recent social history date to determine sex_partner_gender
LEFT JOIN ((SELECT max(date) sochist_date, T2.patient_id
	        FROM emr_socialhistory T1,
	        kre_report.stirrup_index_pats T2
	        WHERE T1.patient_id = T2.patient_id
	        AND sex_partner_gender is not null		
			--REPLACE WITH END DATE
            AND T1.date < '2023-09-01'
        	GROUP BY T2.patient_id) ) T3 ON (T3.patient_id = T1.patient_id)
LEFT JOIN emr_socialhistory T4 on (T3.patient_id = T4.patient_id and T1.patient_id = T4.patient_id and T3.sochist_date = T4.date and sex_partner_gender is not null)
LEFT JOIN kre_report.stirrup_sti_test_inclusion_pats T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN kre_report.stirrup_msm T6 ON (T1.patient_id = T6.patient_id)
GROUP BY T1.patient_id, current_age, gender, birth_sex, gender_identity, race, ethnicity, city, zip5, sex_orientation, msm_indicator, rectal_or_pharyn_clam_gon_test_ever;


--
-- PULLING TOGETHER DETAILED ROWS
--

DROP TABLE IF EXISTS kre_report.stirrup_variables_of_interest;
CREATE TABLE kre_report.stirrup_variables_of_interest AS
SELECT T1.patient_id,
T1.date_of_interest,
coalesce(T2.gon_esp_case, 0) as gon_esp_case,
coalesce(T3.pos_gon_rectal, 0) as pos_gon_rectal,
coalesce(T3.pos_gon_throat, 0) as pos_gon_throat,
coalesce(T3.pos_gon_urog, 0) as pos_gon_urog,
coalesce(T3.pos_gon_oth_or_unk, 0) as pos_gon_oth_or_unk,
coalesce(T2.chlam_esp_case, 0) as chlam_esp_case,
coalesce(T3.pos_chlam_rectal, 0) as pos_chlam_rectal,
coalesce(T3.pos_chlam_throat, 0) as pos_chlam_throat,
coalesce(T3.pos_chlam_urog, 0) as pos_chlam_urog,
coalesce(T3.pos_chlam_oth_or_unk, 0) as pos_chlam_oth_or_unk,
coalesce(T2.syph_esp_case, 0) as syph_esp_case,
coalesce(T4.clin_enc, 0) as clinical_encounter,
coalesce(T5.hiv_elisa_test, 0) as hiv_elisa_test,
COALESCE(T5.hiv_confirm_test, 0) as hiv_confirm_test,
COALESCE(T5.hiv_rna_viral_test, 0) as hiv_rna_viral_test,
COALESCE(T5.hiv_pcr_test, 0) as hiv_pcr_test,
COALESCE(T5.hiv_ag_ab_test, 0) as hiv_ag_ab_test,
COALESCE(T3.any_gon_any_site, 0) as any_gon_any_site,
COALESCE(T3.any_chlam_any_site, 0) as any_chlam_any_site,
COALESCE(T6.syph_lab_any, 0) as syph_lab_any,
COALESCE(T3.any_gon_rectal, 0) as any_gon_rectal,
COALESCE(T3.any_chlam_rectal, 0) as any_chlam_rectal,
COALESCE(T3.any_gon_throat, 0) as any_gon_throat,
COALESCE(T3.any_chlam_throat, 0) as any_chlam_throat,
COALESCE(T7.rx_prep_indicator, 0) as rx_prep_indicator,
CASE WHEN rx_prep_indicator = 1 THEN T7.rx_prep_num_rx ELSE null END rx_prep_num_rx,
CASE WHEN rx_prep_indicator = 1 THEN T7.rx_prep_refills ELSE null END rx_prep_refills,
CASE WHEN rx_prep_indicator = 1 THEN T7.rx_prep_quantity ELSE null END rx_prep_quantity,
COALESCE(T7.rx_hiv_other_indicator, 0) rx_hiv_other_indicator,
CASE WHEN rx_hiv_other_indicator = 1 THEN T7.rx_hiv_other_num_rx ELSE null END rx_hiv_other_num_rx,
CASE WHEN rx_hiv_other_indicator = 1 THEN T7.rx_hiv_other_refills ELSE null END rx_hiv_other_refills,
CASE WHEN rx_hiv_other_indicator =1 THEN T7.rx_hiv_other_quantity ELSE null END rx_hiv_other_quantity,
COALESCE(T8.rx_bicillin_indicator, 0) as rx_bicillin_indicator,
COALESCE (T9.rx_azith_indicator, 0) as rx_azith_indicator,
COALESCE(T9.rx_ceft_indicator, 0) as rx_ceft_indicator,
COALESCE(T10.dx_syph_any, 0) as dx_syph_any,
COALESCE(T10.dx_syph_anal, 0) as dx_syph_anal,
COALESCE(T10.dx_gonoc_inf_rectum, 0) as dx_gonoc_inf_rectum,
COALESCE(T10.dx_gonoc_pharyn, 0) as dx_gonoc_pharyn,
COALESCE(T10.dx_chlam_inf_rectum, 0) as dx_chlam_inf_rectum,
COALESCE(T10.dx_chlam_pharyn, 0) as dx_chlam_pharyn,
COALESCE(T10.dx_lymph_venereum, 0) as dx_lymph_venereum,
COALESCE(T10.dx_chancroid, 0) as dx_chancroid,
COALESCE(T10.dx_granuloma_ingu, 0) as dx_granuloma_ingu,
COALESCE(T10.dx_nongon_ureth, 0) as dx_nongon_ureth,
COALESCE(T10.dx_herpes_simp_w_compl, 0) as dx_herpes_simp_w_compl,
COALESCE(T10.dx_genital_herpes, 0) as dx_genital_herpes,
COALESCE(T10.dx_anogenital_warts, 0) as dx_anogenital_warts,
COALESCE(T10.dx_anorectal_ulcer, 0) as dx_anorectal_ulcer,
COALESCE(T10.dx_unspec_std, 0) as dx_unspec_std,
COALESCE(T10.dx_pid, 0) as dx_pid,
COALESCE(T10.dx_cont_w_exp_to_vd, 0) as dx_cont_w_exp_to_vd,
COALESCE(T10.dx_high_risk_sex_behav, 0) as dx_high_risk_sex_behav,
COALESCE(T10.dx_hiv_counsel, 0) as dx_hiv_counsel,
COALESCE(T10.dx_anorexia, 0) as dx_anorexia,
COALESCE(T10.dx_bulimia, 0) as dx_bulimia,
COALESCE(T10.dx_eat_disorder_nos, 0) as dx_eat_disorder_nos,
COALESCE(T10.dx_depression, 0) as dx_depression,
COALESCE(T10.dx_att_def_disorder, 0) as dx_att_def_disorder,
COALESCE(T10.dx_gend_iden_dis, 0) as dx_gend_iden_dis,
COALESCE(T10.dx_sex_reassignment, 0) as dx_sex_reassignment,
COALESCE(T10.dx_transsexualism, 0) as dx_transsexualism,
COALESCE(T10.dx_couns_for_child_sex_abuse, 0) as dx_couns_for_child_sex_abuse,
COALESCE(T10.dx_foreign_body_anus, 0) as dx_foreign_body_anus,
COALESCE(T10.dx_smoking_tob_use, 0) as dx_smoking_tob_use,
COALESCE(T10.dx_alcohol_depend_abuse, 0) as dx_alcohol_depend_abuse,
COALESCE(T10.dx_opioid_depend_abuse, 0) as dx_opioid_depend_abuse,
COALESCE(T10.dx_sed_hypn_anxio_depend_abuse, 0) as dx_sed_hypn_anxio_depend_abuse,
COALESCE(T10.dx_cocaine_depend_abuse, 0) as dx_cocaine_depend_abuse,
COALESCE(T10.dx_amphet_stim_depend_abuse, 0) as dx_amphet_stim_depend_abuse,
COALESCE(T10.dx_oth_psycho_or_unspec_subs_depend_abuse, 0) as dx_oth_psycho_or_unspec_subs_depend_abuse
FROM kre_report.stirrup_index_pats_and_dates T1
LEFT JOIN kre_report.stirrup_esp_cases T2 ON (T1.patient_id = T2.patient_id and T1.date_of_interest = T2.date_of_interest )
LEFT JOIN kre_report.stirrup_gon_chlam_testing T3 ON (T1.patient_id = T3.patient_id and T1.date_of_interest = T3.date_of_interest)
LEFT JOIN kre_report.stirrup_clinical_encs T4 ON (T1.patient_id = T4.patient_id and T1.date_of_interest = T4.date_of_interest)
LEFT JOIN kre_report.stirrup_hiv_labs_categorized T5 ON (T1.patient_id = T5.patient_id and T1.date_of_interest = T5.date_of_interest)
LEFT JOIN kre_report.stirrup_syph_labs T6 ON (T1.patient_id = T6.patient_id and T1.date_of_interest = T6.date_of_interest)
LEFT JOIN kre_report.stirrup_hiv_meds_output_prep T7 ON (T1.patient_id = T7.patient_id and T1.date_of_interest = T7.date_of_interest)
LEFT JOIN kre_report.stirrup_bicillin_meds_updated T8 ON (T1.patient_id = T8.patient_id and T1.date_of_interest = T8.date_of_interest)
LEFT JOIN kre_report.stirrup_az_cft_meds_final T9 ON (T1.patient_id = T9.patient_id and T1.date_of_interest = T9.date_of_interest)
LEFT JOIN kre_report.stirrup_dx_codes T10 ON (T1.patient_id = T10.patient_id and T1.date_of_interest = T10.date_of_interest);

-- DROP PATIENTS THAT HAVE NO VARIABLES OF INTEREST OR ENCOUNTERS
DELETE FROM kre_report.stirrup_demog
    WHERE patient_id NOT IN (SELECT patient_id FROM kre_report.stirrup_variables_of_interest);
	
-- BRING TOGETEHER DEMOG & VARIABLES OF INTEREST
DROP TABLE IF EXISTS kre_report.stirrup_demog_and_var_of_int;
CREATE TABLE kre_report.stirrup_demog_and_var_of_int AS
SELECT 	
T1.patient_id,
current_age,
date_part('year', age(date_of_interest, date_of_birth)) age_on_doi,
sex,
T1.birth_sex,
T1.gender_identity,
T1.race,
T1.ethnicity,
current_city,
current_zip,
sex_partner_gender,
T1.sex_orientation,
msm_indicator,
rectal_or_pharyn_clam_gon_test_ever,
date_of_interest,
gon_esp_case,
pos_gon_rectal,
pos_gon_throat,
pos_gon_urog,
pos_gon_oth_or_unk,
chlam_esp_case,
pos_chlam_rectal,
pos_chlam_throat,
pos_chlam_urog,
pos_chlam_oth_or_unk,
syph_esp_case,
clinical_encounter,
hiv_elisa_test,
hiv_confirm_test,
hiv_rna_viral_test,
hiv_pcr_test,
hiv_ag_ab_test,
any_gon_any_site,
any_chlam_any_site,
syph_lab_any,
any_gon_rectal,
any_chlam_rectal,
any_gon_throat,
any_chlam_throat,
rx_prep_indicator,
rx_prep_num_rx,
rx_prep_refills,
rx_prep_quantity,
rx_hiv_other_indicator,
rx_hiv_other_num_rx,
rx_hiv_other_refills,
rx_hiv_other_quantity,
rx_bicillin_indicator,
rx_azith_indicator,
rx_ceft_indicator,
dx_syph_any,
dx_syph_anal,
dx_gonoc_inf_rectum,
dx_gonoc_pharyn,
dx_chlam_inf_rectum,
dx_chlam_pharyn,
dx_lymph_venereum,
dx_chancroid,
dx_granuloma_ingu,
dx_nongon_ureth,
dx_herpes_simp_w_compl,
dx_genital_herpes,
dx_anogenital_warts,
dx_anorectal_ulcer,
dx_unspec_std,
dx_pid,
dx_cont_w_exp_to_vd,
dx_high_risk_sex_behav,
dx_hiv_counsel,
dx_anorexia,
dx_bulimia,
dx_eat_disorder_nos,
dx_depression,
dx_att_def_disorder,
dx_gend_iden_dis,
dx_sex_reassignment,
dx_transsexualism,
dx_couns_for_child_sex_abuse,
dx_foreign_body_anus,
dx_smoking_tob_use,
dx_alcohol_depend_abuse,
dx_opioid_depend_abuse,
dx_sed_hypn_anxio_depend_abuse,
dx_cocaine_depend_abuse,
dx_amphet_stim_depend_abuse,
dx_oth_psycho_or_unspec_subs_depend_abuse
FROM kre_report.stirrup_demog T1
INNER JOIN kre_report.stirrup_variables_of_interest T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN public.emr_patient T3 ON (T1.patient_id = T3.id)
ORDER BY T1.patient_id, date_of_interest;

-- 
-- DELETE RECORDS FOR ANY DATES WHERE PATIENT IS < 15
--
DELETE FROM kre_report.stirrup_demog_and_var_of_int
WHERE age_on_doi < 15;

-- -- NEEDED FOR REPORTING
-- -- ONE TIME RUN ONLY
-- CREATE SEQUENCE IF NOT EXISTS  kre_report.stirrup_masked_id_seq;


-- CREATE TABLE kre_report.stirrup_masked_patients
-- (
    -- patient_id integer,
    -- masked_patient_id character varying(128)
-- );

-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST
-- CHA-, BMC-, FWY-

INSERT INTO kre_report.stirrup_masked_patients
SELECT 
DISTINCT patient_id,
CONCAT(:site_abbr, NEXTVAL('kre_report.stirrup_masked_id_seq')) masked_patient_id
FROM kre_report.stirrup_demog_and_var_of_int
WHERE patient_id not in (select patient_id from kre_report.stirrup_masked_patients)
GROUP by patient_id;


-- CREATE THE OUTPUT TABLE WITH MASKED ID VALUES
DROP TABLE IF EXISTS kre_report.stirrup_output_table_masked;
CREATE TABLE kre_report.stirrup_output_table_masked as
SELECT
masked_patient_id,
current_age,
age_on_doi,
sex,
birth_sex,
gender_identity,
race,
ethnicity,
current_city,
current_zip,
sex_partner_gender,
sex_orientation,
rectal_or_pharyn_clam_gon_test_ever,
date_of_interest,
gon_esp_case,
pos_gon_rectal,
pos_gon_throat,
pos_gon_urog,
pos_gon_oth_or_unk,
chlam_esp_case,
pos_chlam_rectal,
pos_chlam_throat,
pos_chlam_urog,
pos_chlam_oth_or_unk,
syph_esp_case,
clinical_encounter,
hiv_elisa_test,
hiv_confirm_test,
hiv_rna_viral_test,
hiv_pcr_test,
hiv_ag_ab_test,
any_gon_any_site,
any_chlam_any_site,
syph_lab_any,
any_gon_rectal,
any_chlam_rectal,
any_gon_throat,
any_chlam_throat,
rx_prep_indicator,
rx_prep_num_rx,
rx_prep_refills,
rx_prep_quantity,
rx_hiv_other_indicator,
rx_hiv_other_num_rx,
rx_hiv_other_refills,
rx_hiv_other_quantity,
rx_bicillin_indicator,
rx_azith_indicator,
rx_ceft_indicator,
dx_syph_any,
dx_syph_anal,
dx_gonoc_inf_rectum,
dx_gonoc_pharyn,
dx_chlam_inf_rectum,
dx_chlam_pharyn,
dx_lymph_venereum,
dx_chancroid,
dx_granuloma_ingu,
dx_nongon_ureth,
dx_herpes_simp_w_compl,
dx_genital_herpes,
dx_anogenital_warts,
dx_anorectal_ulcer,
dx_unspec_std,
dx_pid,
dx_cont_w_exp_to_vd,
dx_high_risk_sex_behav,
dx_hiv_counsel,
dx_anorexia,
dx_bulimia,
dx_eat_disorder_nos,
dx_depression,
dx_att_def_disorder,
dx_gend_iden_dis,
dx_sex_reassignment,
dx_transsexualism,
dx_couns_for_child_sex_abuse,
dx_foreign_body_anus,
dx_smoking_tob_use,
dx_alcohol_depend_abuse,
dx_opioid_depend_abuse,
dx_sed_hypn_anxio_depend_abuse,
dx_cocaine_depend_abuse,
dx_amphet_stim_depend_abuse,
dx_oth_psycho_or_unspec_subs_depend_abuse
FROM kre_report.stirrup_demog_and_var_of_int T1
INNER JOIN kre_report.stirrup_masked_patients T2 ON (T1.patient_id = T2.patient_id);

-- \COPY kre_report.stirrup_output_table_masked TO '/tmp/2023-09-22-bmc-stirrup.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_output_table_masked TO '/tmp/2023-09-22-cha-stirrup.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_output_table_masked TO '/tmp/2023-09-22-fwy-stirrup.csv' DELIMITER ',' CSV HEADER;

-- BMC FIND PATIENTS CHANGED BY PATIENT MERGE
-- SELECT T1.patient_id, T1.masked_patient_id, T2.id
-- FROM kre_report.stirrup_masked_patients T1
-- LEFT JOIN emr_patient T2 ON (T1.patient_id = T2.id)
-- WHERE T2.id is null
-- ORDER BY patient_id;

--FIGURE OUT NEW PATIENT ID AND UPDATE


--
-- DATA REQUESTS -- NOT PART OF STANDARD REPORTING
-- PATIENTS WITH HIV


CREATE TABLE kre_report.stirrup_adhoc_pats_with_hiv AS
select T1.masked_patient_id, T3.date as esp_hiv_case_date
FROM kre_report.stirrup_output_table_masked T1
INNER JOIN kre_report.stirrup_masked_patients T2 ON (T1.masked_patient_id = T2.masked_patient_id)
INNER JOIN nodis_case T3 ON (T2.patient_id = T3.patient_id)
WHERE T3.date < '2023-09-01'
AND condition = 'hiv'
GROUP BY T1.masked_patient_id, T3.date
ORDER BY T1.masked_patient_id;

-- \COPY kre_report.stirrup_adhoc_pats_with_hiv TO '/tmp/2024-04-18-bmc-hiv-cases.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_pats_with_hiv TO '/tmp/2024-04-18-cha-hiv-cases.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_pats_with_hiv TO '/tmp/2024-04-18-fwy-hiv-cases.csv' DELIMITER ',' CSV HEADER;

-- Inj PrEP 
DROP TABLE IF EXISTS kre_report.stirrup_adhoc_inj_prep;
CREATE TABLE kre_report.stirrup_adhoc_inj_prep AS
--select DISTINCT T3.masked_patient_id, T2.date, T1.name as event_name, T2.name, T2.refills, T2.quantity_float
SELECT DISTINCT T3.masked_patient_id, T2.name, T1.name as hef_name, T2.date, T2.quantity, T2.quantity_float, T2.refills, status,T2.start_date, T2.end_date
FROM hef_event T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id and T1.object_id = T2.id)
JOIN kre_report.stirrup_masked_patients T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
WHERE T1.name = 'rx:hiv_cabotegravir_er_600'
AND T2.date >= '2013-01-01'
AND T2.date < '2023-09-01';

-- \COPY kre_report.stirrup_adhoc_inj_prep TO '/tmp/2024-04-18-bmc-inj-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_inj_prep TO '/tmp/2024-04-18-cha-inj-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_inj_prep TO '/tmp/2024-04-18-fwy-inj-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;

--ORAL PrEP
DROP TABLE IF EXISTS kre_report.stirrup_adhoc_oral_prep;
CREATE TABLE kre_report.stirrup_adhoc_oral_prep AS
SELECT DISTINCT T3.masked_patient_id, T2.name, T1.name as hef_name, T2.date, T2.quantity, T2.quantity_float, T2.refills, status,T2.start_date, T2.end_date
FROM hef_event T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id and T1.object_id = T2.id)
JOIN kre_report.stirrup_masked_patients T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 
                      'rx:hiv_tenofovir-emtricitabine', 
                      'rx:hiv_tenofovir_alafenamide-emtricitabine', 
					  'rx:hiv_tenofovir_alafenamide-emtricitabine:generic')
AND T2.date >= '2013-01-01'
AND T2.date < '2023-09-01';

-- \COPY kre_report.stirrup_adhoc_oral_prep TO '/tmp/2024-04-18-bmc-oral-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_oral_prep TO '/tmp/2024-04-18-cha-oral-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;
-- \COPY kre_report.stirrup_adhoc_oral_prep TO '/tmp/2024-04-18-fwy-oral-prep-rx-detailed.csv' DELIMITER ',' CSV HEADER;

