--
-- Script setup section 
--
DROP TABLE IF EXISTS stiprep_index_pats CASCADE;
DROP TABLE IF EXISTS stiprep_hef_w_lab_details CASCADE;
DROP TABLE IF EXISTS stiprep_cases_of_interest CASCADE;
DROP TABLE IF EXISTS stiprep_gon_events CASCADE;
DROP TABLE IF EXISTS stiprep_chlam_events CASCADE;
DROP TABLE IF EXISTS stiprep_syph_labs CASCADE;
DROP TABLE IF EXISTS stiprep_hepc_labs CASCADE;
DROP TABLE IF EXISTS stiprep_hepc_events CASCADE;
DROP TABLE IF EXISTS stiprep_hebp_labs CASCADE;
DROP TABLE IF EXISTS stiprep_hepb_events CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_labs CASCADE;
DROP TABLE IF EXISTS stiprep_wbpos_elisaposneg CASCADE;
DROP TABLE IF EXISTS stiprep_gon_counts CASCADE;
DROP TABLE IF EXISTS stiprep_gon_cases CASCADE;
DROP TABLE IF EXISTS stiprep_chlam_counts CASCADE;
DROP TABLE IF EXISTS stiprep_chlam_cases CASCADE;
DROP TABLE IF EXISTS stiprep_syph_counts CASCADE;
DROP TABLE IF EXISTS stiprep_syph_cases CASCADE;
DROP TABLE IF EXISTS stiprep_sti_case_dates CASCADE;
DROP TABLE IF EXISTS stiprep_sti_enc_py CASCADE;

DROP TABLE IF EXISTS stiprep_first_sti CASCADE;
DROP TABLE IF EXISTS stiprep_second_sti CASCADE;
DROP TABLE IF EXISTS stiprep_third_sti CASCADE;
DROP TABLE IF EXISTS stiprep_fourth_sti CASCADE;
DROP TABLE IF EXISTS stiprep_first_sti_agg CASCADE;
DROP TABLE IF EXISTS stiprep_second_sti_agg CASCADE;
DROP TABLE IF EXISTS stiprep_third_sti_agg CASCADE;
DROP TABLE IF EXISTS stiprep_fourth_sti_agg CASCADE;
DROP TABLE IF EXISTS stiprep_first_sti_index CASCADE;
DROP TABLE IF EXISTS stiprep_second_sti_index CASCADE;
DROP TABLE IF EXISTS stiprep_third_sti_index CASCADE;
DROP TABLE IF EXISTS stiprep_fourth_sti_index CASCADE;
DROP TABLE IF EXISTS stiprep_second_days_compute CASCADE;
DROP TABLE IF EXISTS stiprep_third_days_compute CASCADE;
DROP TABLE IF EXISTS stiprep_fourth_days_compute CASCADE;
DROP TABLE IF EXISTS stiprep_second_days CASCADE;
DROP TABLE IF EXISTS stiprep_third_days CASCADE;
DROP TABLE IF EXISTS stiprep_fourth_days CASCADE;



DROP TABLE IF EXISTS stiprep_cpts_of_interest CASCADE;
DROP TABLE IF EXISTS stiprep_hepc_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hepc_cases CASCADE;
DROP TABLE IF EXISTS stiprep_hepb_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hebp_cases CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_cases CASCADE;
DROP TABLE IF EXISTS stiprep_max_viral_load CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_new_diag CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_meds CASCADE;
DROP TABLE IF EXISTS stiprep_truvada_array CASCADE;
DROP TABLE IF EXISTS stiprep_truvada_2mogap CASCADE;
DROP TABLE IF EXISTS stiprep_truvada_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_all_details CASCADE;
DROP TABLE IF EXISTS stiprep_dx_codes CASCADE;
DROP TABLE IF EXISTS stiprep_diag_fields CASCADE;
DROP TABLE IF EXISTS stiprep_rx_of_interest CASCADE;
DROP TABLE IF EXISTS stiprep_rx_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hepb_diags CASCADE;
DROP TABLE IF EXISTS stiprep_hepb_diags_2 CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_problem_list CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_rx_combo_distinct CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_rx_3_diff CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_rx_3_diff_count CASCADE;
DROP TABLE IF EXISTS stiprep_truvada_rx_and_pills CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_meds_distinct CASCADE;
DROP TABLE IF EXISTS stiprep_truvada_rx_and_pills_peryear CASCADE;
DROP TABLE IF EXISTS stiprep_all_encs CASCADE;
DROP TABLE IF EXISTS stiprep_enc_counts CASCADE;
DROP TABLE IF EXISTS stiprep_hiv_enc_counts CASCADE;

DROP TABLE IF EXISTS stiprep_output_part_1;
DROP TABLE IF EXISTS stiprep_output_with_patient;
DROP TABLE IF EXISTS stiprep_report_final_output CASCADE;

--
-- Script body 
--

-- INDEX PATIENTS 
-- All individuals with at least one case of Chlamydia, Gonorrhea, Syphillis, or HIV at any time. (2006-2017)
CREATE TABLE stiprep_index_pats AS
SELECT T1.patient_id,
date_part('year', age('2006-12-31', date_of_birth)) age_2006,
date_part('year', age('2007-12-31', date_of_birth)) age_2007,
date_part('year', age('2008-12-31', date_of_birth)) age_2008,
date_part('year', age('2009-12-31', date_of_birth)) age_2009,
date_part('year', age('2010-12-31', date_of_birth)) age_2010,
date_part('year', age('2011-12-31', date_of_birth)) age_2011,
date_part('year', age('2012-12-31', date_of_birth)) age_2012,
date_part('year', age('2013-12-31', date_of_birth)) age_2013,
date_part('year', age('2014-12-31', date_of_birth)) age_2014,
date_part('year', age('2015-12-31', date_of_birth)) age_2015,
date_part('year', age('2016-12-31', date_of_birth)) age_2016,
date_part('year', age('2017-12-31', date_of_birth)) age_2017
FROM nodis_case T1,
emr_patient T2
WHERE T1.patient_id = T2.id
-- exclude patients that are not 15 on last day of reporting year
AND date_part('year', age('2017-12-31', date_of_birth)) >= 15 
AND T1.condition in ('chlamydia', 'gonorrhea', 'syphilis', 'hiv')
-- exclude patients that were not 15 on case date
AND date_part('year', age(T1.date, date_of_birth)) >= 15 
AND T1.date >= '01-01-2006'
AND T1.date < '01-01-2018'
GROUP BY T1.patient_id, T2.date_of_birth;

-- BASE Join the hef events with the lab results
CREATE TABLE stiprep_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name
FROM emr_labresult T1,
hef_event T2, 
stiprep_index_pats T3
WHERE T1.id = T2.object_id
AND T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T3.patient_id = T2.patient_id
AND T2.date >= '01-01-2006'
AND T2.date < '01-01-2018';

-- BASE - Get the cases we are interested in 
CREATE TABLE stiprep_cases_of_interest AS 
SELECT T1.condition, T1.date, T1.patient_id, date case_date, EXTRACT(YEAR FROM date) rpt_year 
FROM  nodis_case T1,
stiprep_index_pats T2   
WHERE  date >= '01-01-2006' 
AND date < '01-01-2018'
AND condition in ('hepatitis_c:acute', 'hepatitis_b:acute', 'syphilis', 'gonorrhea', 'chlamydia', 'hiv')
AND T1.patient_id = T2.patient_id;

-- GONORRHEA - Gather up gonorrhea events
CREATE TABLE stiprep_gon_events AS
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP)') THEN 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM stiprep_hef_w_lab_details T1
WHERE  name like 'lx:gonorrhea%';

-- CHLAMYDIA - Gather up chlamydia events
CREATE TABLE stiprep_chlam_events AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP)') THEN 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM stiprep_hef_w_lab_details T1
WHERE  name like 'lx:chlamydia%';

-- 02/15/2018: Don't think this table is needed any more
-- -- SYPHILIS - Gather up syphillis events
-- CREATE TABLE stiprep_syph_events AS 
-- SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year 
-- FROM stiprep_hef_w_lab_details 
-- WHERE  name ~ '^(lx:rpr|lx:vdrl|lx:tppa|lx:fta-abs|lx:tp-igg|lx:tp-igm)';

-- SYPHILIS - Gather up Lab Tests
CREATE TABLE stiprep_syph_labs AS 
SELECT T2.test_name,T1.patient_id, EXTRACT(YEAR FROM date)  rpt_year,T1.id 
FROM emr_labresult T1, 
conf_labtestmap T2,
stiprep_index_pats T3 
WHERE T1.native_code = T2.native_code  
AND T1.patient_id = T3.patient_id
AND test_name in ('rpr', 'vdrl-csf', 'vdrl', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm') 
AND date >= '01-01-2006'
AND date < '01-01-2018';

-- HEP C - Gather up Lab Tests
CREATE TABLE stiprep_hepc_labs AS 
SELECT T2.test_name,T1.patient_id, EXTRACT(YEAR FROM date)  rpt_year,T1.id 
FROM emr_labresult T1, 
conf_labtestmap T2,
stiprep_index_pats T3 
WHERE T1.native_code = T2.native_code  
AND T1.patient_id = T3.patient_id
AND test_name in ('hepatitis_c_elisa', 'hepatitis_c_rna') 
AND date >= '01-01-2006'
AND date < '01-01-2018';

-- HEP C - Gather up Hep C Hef Events
CREATE TABLE stiprep_hepc_events AS 
SELECT T1.patient_id,EXTRACT(YEAR FROM date)  rpt_year,T1.name,T1.object_id 
FROM  stiprep_hef_w_lab_details T1   
WHERE name LIKE 'lx:hepatitis_c_elisa%' OR name LIKE 'lx:hepatitis_c_rna%';

-- HEP B Gather up lab tests
CREATE TABLE stiprep_hebp_labs AS 
SELECT T2.test_name,T1.patient_id, EXTRACT(YEAR FROM date)  rpt_year,T1.id 
FROM emr_labresult T1,
conf_labtestmap T2,
stiprep_index_pats T3
WHERE T1.native_code = T2.native_code
AND T1.patient_id = T3.patient_id
AND test_name in ('hepatitis_b_viral_dna', 'hepatitis_b_surface_antigen') 
AND date >= '01-01-2006'
AND date < '01-01-2018';

-- HEP B - Gather up heb b events
CREATE TABLE stiprep_hepb_events AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year 
FROM stiprep_hef_w_lab_details 
WHERE name like 'lx:hepatitis_b_surface_antigen%' OR name like 'lx:hepatitis_b_viral_dna%';

-- HIV - Gather Up Lab Tests 
CREATE TABLE stiprep_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM date) rpt_year, result_float
FROM emr_labresult T1, 
conf_labtestmap T2,
stiprep_index_pats T3
WHERE T1.native_code = T2.native_code
AND T1.patient_id = T3.patient_id
AND test_name ilike 'hiv_%' 
AND date >= '01-01-2006'
AND date < '01-01-2018';

-- HIV - Gather up lab specific hiv tests
CREATE TABLE stiprep_wbpos_elisaposneg AS 
SELECT T1.patient_id,T1.name,T1.date 
FROM  stiprep_hef_w_lab_details T1   
WHERE name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative', 'lx:hiv_ag_ab:positive');

-- GONORRHEA - Counts & Column Creation
CREATE TABLE stiprep_gon_counts AS 
SELECT T1.patient_id,count(*) t_gon_tests, count(CASE WHEN rpt_year = '2006' THEN 1 END)  t_gon_tests_06,
count(CASE WHEN rpt_year = '2007' THEN 1 END)  t_gon_tests_07, count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_gon_tests_08,
count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_gon_tests_09, count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_gon_tests_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_gon_tests_11, count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_gon_tests_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_gon_tests_13, count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_gon_tests_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_gon_tests_15, count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_gon_tests_16, 
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_gon_tests_17, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2006' THEN 1 END)  p_gon_tests_06,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2007' THEN 1 END)  p_gon_tests_07, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_gon_tests_08,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_gon_tests_09, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_10,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_11, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_12,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_13, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_14, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_15, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_16, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_17, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2006' THEN 1 END) t_gon_tests_throat_06, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2007' THEN 1 END)  t_gon_tests_throat_07, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and  rpt_year = '2008' THEN 1 END)  t_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2009' THEN 1 END)  t_gon_tests_throat_09, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2010' THEN 1 END)  t_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and  rpt_year = '2011' THEN 1 END)  t_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and  rpt_year = '2012' THEN 1 END)  t_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2013' THEN 1 END)  t_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2014' THEN 1 END)  t_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2015' THEN 1 END)  t_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2016' THEN 1 END)  t_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2017' THEN 1 END)  t_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_throat_06, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END)  p_gon_tests_throat_07, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_gon_tests_throat_09, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2006' THEN 1 END) t_gon_tests_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2007' THEN 1 END) t_gon_tests_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2008' THEN 1 END) t_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2009' THEN 1 END) t_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2010' THEN 1 END) t_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2011' THEN 1 END) t_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2012' THEN 1 END) t_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2013' THEN 1 END) t_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2014' THEN 1 END) t_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2015' THEN 1 END) t_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2016' THEN 1 END) t_gon_tests_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2017' THEN 1 END) t_gon_tests_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_gon_tests_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_rectal_16,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_rectal_17,
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2006' THEN 1 END) t_gon_tests_urog_06, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2007' THEN 1 END) t_gon_tests_urog_07, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2008' THEN 1 END) t_gon_tests_urog_08, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2009' THEN 1 END) t_gon_tests_urog_09, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2010' THEN 1 END) t_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2011' THEN 1 END) t_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2012' THEN 1 END) t_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2013' THEN 1 END) t_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2014' THEN 1 END) t_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2015' THEN 1 END) t_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2016' THEN 1 END) t_gon_tests_urog_16, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2017' THEN 1 END) t_gon_tests_urog_17, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_gon_tests_urog_06, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_gon_tests_urog_07, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_urog_08, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_urog_09,
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_urog_16,  
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_urog_17
FROM  stiprep_gon_events T1   
GROUP BY T1.patient_id;

-- GONORRHEA - Cases
CREATE TABLE stiprep_gon_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2006' then 1 end) gon_06,
count(CASE WHEN T1.rpt_year = '2007' then 1 end) gon_07,
count(CASE WHEN T1.rpt_year = '2008' then 1 end) gon_08, 
count(CASE WHEN T1.rpt_year = '2009' then 1 end) gon_09,
count(CASE WHEN T1.rpt_year = '2010' then 1 end) gon_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) gon_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) gon_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) gon_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) gon_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) gon_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) gon_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) gon_17
FROM  stiprep_cases_of_interest T1   
WHERE condition = 'gonorrhea' GROUP BY T1.patient_id;

-- CHLAMYDIA - Counts & Column Creation
CREATE TABLE stiprep_chlam_counts AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END)  t_chlam_06, count(CASE WHEN rpt_year = '2007' THEN 1 END)  t_chlam_07, 
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_chlam_08, count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_chlam_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_chlam_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_chlam_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_chlam_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_chlam_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_chlam_14,count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_chlam_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_chlam_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_chlam_17, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2006' THEN 1 END)  p_chlam_06, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2007' THEN 1 END)  p_chlam_07,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_chlam_08, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_chlam_09,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_chlam_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_chlam_11,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_chlam_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_chlam_13,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_chlam_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_chlam_15,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_chlam_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_chlam_17,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2006' THEN 1 END) t_chlam_throat_06,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2007' THEN 1 END) t_chlam_throat_07,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2008' THEN 1 END) t_chlam_throat_08,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2009' THEN 1 END) t_chlam_throat_09,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2010' THEN 1 END) t_chlam_throat_10,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2011' THEN 1 END) t_chlam_throat_11,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2012' THEN 1 END) t_chlam_throat_12,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2013' THEN 1 END) t_chlam_throat_13,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2014' THEN 1 END) t_chlam_throat_14,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2015' THEN 1 END) t_chlam_throat_15, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2016' THEN 1 END) t_chlam_throat_16, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and rpt_year = '2017' THEN 1 END) t_chlam_throat_17,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_throat_06, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_throat_07, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_throat_08, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_throat_09, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_throat_10, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_throat_11, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_throat_12, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_throat_13, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_throat_14, 
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_throat_15,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_throat_16,
count(CASE WHEN T1.specimen_source ~* '(THROAT)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_throat_17,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2006' THEN 1 END) t_chlam_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2007' THEN 1 END) t_chlam_rectal_07,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2008' THEN 1 END) t_chlam_rectal_08,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2009' THEN 1 END) t_chlam_rectal_09,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2010' THEN 1 END) t_chlam_rectal_10,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2011' THEN 1 END) t_chlam_rectal_11,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2012' THEN 1 END) t_chlam_rectal_12,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2013' THEN 1 END) t_chlam_rectal_13,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2014' THEN 1 END) t_chlam_rectal_14,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2015' THEN 1 END) t_chlam_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2016' THEN 1 END) t_chlam_rectal_16, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and rpt_year = '2017' THEN 1 END) t_chlam_rectal_17, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_rectal_06, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_rectal_07, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_rectal_08, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_rectal_09, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_rectal_10, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_rectal_11, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_rectal_12, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)'and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_rectal_13, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)'and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_rectal_14, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_rectal_15, 
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_rectal_16,
count(CASE WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_rectal_17,
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2006' THEN 1 END) t_chlam_urog_06, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2007' THEN 1 END) t_chlam_urog_07, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2008' THEN 1 END) t_chlam_urog_08, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2009' THEN 1 END) t_chlam_urog_09, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2010' THEN 1 END) t_chlam_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2011' THEN 1 END) t_chlam_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2012' THEN 1 END) t_chlam_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2013' THEN 1 END) t_chlam_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2014' THEN 1 END) t_chlam_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2015' THEN 1 END) t_chlam_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2016' THEN 1 END) t_chlam_urog_16, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and rpt_year = '2017' THEN 1 END) t_chlam_urog_17, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2006' THEN 1 END) p_chlam_urog_06, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2007' THEN 1 END) p_chlam_urog_07, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_chlam_urog_08, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_chlam_urog_09,
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_chlam_urog_10, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_chlam_urog_11, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_chlam_urog_12, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_chlam_urog_13, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_chlam_urog_14, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_chlam_urog_15, 
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_chlam_urog_16,  
count(CASE WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_chlam_urog_17  
FROM  stiprep_chlam_events T1   
GROUP BY T1.patient_id;

-- CHLAMYIDA - Cases
CREATE TABLE stiprep_chlam_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2006' then 1 end) chlam_06,
count(CASE WHEN T1.rpt_year = '2007' then 1 end) chlam_07,
count(CASE WHEN T1.rpt_year = '2008' then 1 end) chlam_08, 
count(CASE WHEN T1.rpt_year = '2009' then 1 end) chlam_09,
count(CASE WHEN T1.rpt_year = '2010' then 1 end) chlam_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) chlam_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) chlam_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) chlam_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) chlam_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) chlam_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) chlam_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) chlam_17
FROM  stiprep_cases_of_interest T1   
WHERE condition = 'chlamydia' GROUP BY T1.patient_id;


-- SYPHILLIS - Counts & Column Creation
CREATE TABLE stiprep_syph_counts AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END)  t_syph_06, 
count(CASE WHEN rpt_year = '2007' THEN 1 END)  t_syph_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_syph_08, 
count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_syph_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_syph_10, 
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_syph_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_syph_12, 
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_syph_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_syph_14, 
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_syph_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_syph_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_syph_17
FROM  stiprep_syph_labs T1   
GROUP BY T1.patient_id;

-- SYPHILIS - Cases
CREATE TABLE stiprep_syph_cases AS 
SELECT T1.patient_id, 
max(CASE WHEN T1.rpt_year = '2006' then 1 end) syphilis_06, 
max(CASE WHEN T1.rpt_year = '2007' then 1 end) syphilis_07,
max(CASE WHEN T1.rpt_year = '2008' then 1 end) syphilis_08, 
max(CASE WHEN T1.rpt_year = '2009' then 1 end) syphilis_09,
max(CASE WHEN T1.rpt_year = '2010' then 1 end) syphilis_10,
max(CASE WHEN T1.rpt_year = '2011' then 1 end) syphilis_11,
max(CASE WHEN T1.rpt_year = '2012' then 1 end) syphilis_12, 
max(CASE WHEN T1.rpt_year = '2013' then 1 end) syphilis_13,
max(CASE WHEN T1.rpt_year = '2014' then 1 end) syphilis_14, 
max(CASE WHEN T1.rpt_year = '2015' then 1 end) syphilis_15, 
max(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_16,
max(CASE WHEN T1.rpt_year = '2017' then 1 end) syphilis_17
FROM  stiprep_cases_of_interest T1   
WHERE condition = 'syphilis' GROUP BY T1.patient_id;

-- STI Number of STI case dates (unique per case date) per year (2006-2017)
CREATE TABLE stiprep_sti_case_dates AS
SELECT patient_id, case_date, rpt_year
FROM stiprep_cases_of_interest
WHERE condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv')
GROUP BY patient_id, case_date, rpt_year;

CREATE TABLE stiprep_sti_enc_py AS
SELECT patient_id,
COUNT(CASE WHEN rpt_year = '2006' THEN 1 END)  t_sti_enc_py_06,
COUNT(CASE WHEN rpt_year = '2007' THEN 1 END)  t_sti_enc_py_07,
COUNT(CASE WHEN rpt_year = '2008' THEN 1 END)  t_sti_enc_py_08,
COUNT(CASE WHEN rpt_year = '2009' THEN 1 END)  t_sti_enc_py_09,
COUNT(CASE WHEN rpt_year = '2010' THEN 1 END)  t_sti_enc_py_10,
COUNT(CASE WHEN rpt_year = '2011' THEN 1 END)  t_sti_enc_py_11,
COUNT(CASE WHEN rpt_year = '2012' THEN 1 END)  t_sti_enc_py_12,
COUNT(CASE WHEN rpt_year = '2013' THEN 1 END)  t_sti_enc_py_13,
COUNT(CASE WHEN rpt_year = '2014' THEN 1 END)  t_sti_enc_py_14,
COUNT(CASE WHEN rpt_year = '2015' THEN 1 END)  t_sti_enc_py_15,
COUNT(CASE WHEN rpt_year = '2016' THEN 1 END)  t_sti_enc_py_16,
COUNT(CASE WHEN rpt_year = '2017' THEN 1 END)  t_sti_enc_py_17
FROM stiprep_sti_case_dates
GROUP BY patient_id;

-- First STI diagnosed per year (2006-2017)
-- Variable names:  IndexSTI_2006, IndexSTI_2007…IndexSTI_2017
-- Options include HIV, Chlamydia, Gonorrhea – rectal, Gonorrhea – oropharynx, Gonorrhea – genital, or Syphilis.  If more than one with the same case date then concatenate and list all.

-- 05/30/2018 - Per Cara do not include HIV in the first sti index year.

CREATE TABLE stiprep_first_sti AS
SELECT T1.patient_id, case_date first_sti_date, T1.rpt_year, T1.rpt_year indexsti_year, specimen_source, 
CASE WHEN condition = 'gonorrhea' and specimen_source ~* '(THROAT)' then 'gonorrhea-oropharynx'
WHEN condition = 'gonorrhea' and specimen_source ~* '(RECTUM|RECT|ANAL)' then 'gonorrhea-rectal'
WHEN condition = 'gonorrhea' and specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' then 'gonorrhea-gential'
else condition end condition
FROM stiprep_cases_of_interest T1
INNER JOIN (select patient_id, min(date)first_date from stiprep_cases_of_interest where condition in ('syphilis', 'gonorrhea', 'chlamydia') group by patient_id, rpt_year) T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN stiprep_gon_events T3 ON (T1.patient_id = T3.patient_id and T1.case_date = T3.date and name like '%positive%')
WHERE T1.case_date = T2.first_date
AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia');

CREATE TABLE stiprep_first_sti_agg AS
SELECT T1.patient_id, extract(month from first_sti_date) first_sti_month, first_sti_date,
array_agg(distinct(T1.condition)) condition,
rpt_year
FROM stiprep_first_sti T1
GROUP BY T1.patient_id, T1.rpt_year, first_sti_date; 

CREATE TABLE stiprep_first_sti_index AS
SELECT T1.patient_id,
MAX(CASE WHEN rpt_year = '2006' THEN condition END) indexsti_2006,
MAX(CASE WHEN rpt_year = '2006' THEN first_sti_month END) indexsti_month_06,
MAX(CASE WHEN rpt_year = '2007' THEN condition END) indexsti_2007,
MAX(CASE WHEN rpt_year = '2007' THEN first_sti_month END) indexsti_month_07,
MAX(CASE WHEN rpt_year = '2008' THEN condition END) indexsti_2008,
MAX(CASE WHEN rpt_year = '2008' THEN first_sti_month END) indexsti_month_08,
MAX(CASE WHEN rpt_year = '2009' THEN condition END) indexsti_2009,
MAX(CASE WHEN rpt_year = '2009' THEN first_sti_month END) indexsti_month_09,
MAX(CASE WHEN rpt_year = '2010' THEN condition END) indexsti_2010,
MAX(CASE WHEN rpt_year = '2010' THEN first_sti_month END) indexsti_month_10,
MAX(CASE WHEN rpt_year = '2011' THEN condition END) indexsti_2011,
MAX(CASE WHEN rpt_year = '2011' THEN first_sti_month END) indexsti_month_11,
MAX(CASE WHEN rpt_year = '2012' THEN condition END) indexsti_2012,
MAX(CASE WHEN rpt_year = '2012' THEN first_sti_month END) indexsti_month_12,
MAX(CASE WHEN rpt_year = '2013' THEN condition END) indexsti_2013,
MAX(CASE WHEN rpt_year = '2013' THEN first_sti_month END) indexsti_month_13,
MAX(CASE WHEN rpt_year = '2014' THEN condition END) indexsti_2014,
MAX(CASE WHEN rpt_year = '2014' THEN first_sti_month END) indexsti_month_14,
MAX(CASE WHEN rpt_year = '2015' THEN condition END) indexsti_2015,
MAX(CASE WHEN rpt_year = '2015' THEN first_sti_month END) indexsti_month_15,
MAX(CASE WHEN rpt_year = '2016' THEN condition END) indexsti_2016,
MAX(CASE WHEN rpt_year = '2016' THEN first_sti_month END) indexsti_month_16,
MAX(CASE WHEN rpt_year = '2017' THEN condition END) indexsti_2017,
MAX(CASE WHEN rpt_year = '2017' THEN first_sti_month END) indexsti_month_17
FROM stiprep_first_sti_agg T1
GROUP BY T1.patient_id;


-- SECOND STI 
CREATE TABLE stiprep_second_sti AS
SELECT T1.patient_id, case_date second_sti_date, specimen_source, T2.indexsti_year,
CASE WHEN condition = 'gonorrhea' and specimen_source ~* '(THROAT)' then 'gonorrhea-oropharynx'
WHEN condition = 'gonorrhea' and specimen_source ~* '(RECTUM|RECT|ANAL)' then 'gonorrhea-rectal'
WHEN condition = 'gonorrhea' and specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' then 'gonorrhea-gential'
else condition end condition
FROM stiprep_cases_of_interest T1
INNER JOIN (SELECT T1.patient_id, min(date)first_date, T2.indexsti_year 
		FROM stiprep_cases_of_interest T1, stiprep_first_sti T2 
		WHERE T1.date > T2.first_sti_date 
		AND T1.patient_id = T2.patient_id 
		AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv')
		GROUP BY T1.patient_id, T2.indexsti_year) T2  
		ON (T1.patient_id = T2.patient_id)
LEFT JOIN stiprep_gon_events T3 ON (T1.patient_id = T3.patient_id and T1.case_date = T3.date and name like '%positive%')
WHERE T1.case_date = T2.first_date
AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv');

CREATE TABLE stiprep_second_sti_agg AS
SELECT T1.patient_id, second_sti_date,
array_agg(distinct(T1.condition)) condition,
indexsti_year
FROM stiprep_second_sti T1
GROUP BY T1.patient_id, T1.indexsti_year, second_sti_date;

CREATE TABLE stiprep_second_sti_index AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN condition END) secsti_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN condition END) secsti_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN condition END) secsti_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN condition END) secsti_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN condition END) secsti_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN condition END) secsti_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN condition END) secsti_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN condition END) secsti_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN condition END) secsti_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN condition END) secsti_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN condition END) secsti_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN condition END) secsti_index2017
FROM stiprep_second_sti_agg T1
GROUP BY T1.patient_id;

-- Count of days between first STI case date and second STI case date
CREATE TABLE stiprep_second_days_compute AS 
SELECT T2.patient_id,
(second_sti_date - min(first_sti_date)) days_btwn_st1_sti2, indexsti_year
FROM stiprep_first_sti_agg T1,
stiprep_second_sti_agg T2
WHERE T1.patient_id = T2.patient_id
AND T1.rpt_year = T2.indexsti_year
GROUP BY T2.patient_id, second_sti_date, indexsti_year, rpt_year;

CREATE TABLE stiprep_second_days AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN days_btwn_st1_sti2 END) secsti_1to2_days_index2017
FROM stiprep_second_days_compute T1
GROUP BY T1.patient_id;

-- THIRD STI 
CREATE TABLE stiprep_third_sti AS
SELECT T1.patient_id, case_date third_sti_date, specimen_source, T2.indexsti_year,
CASE WHEN condition = 'gonorrhea' and specimen_source ~* '(THROAT)' then 'gonorrhea-oropharynx'
WHEN condition = 'gonorrhea' and specimen_source ~* '(RECTUM|RECT|ANAL)' then 'gonorrhea-rectal'
WHEN condition = 'gonorrhea' and specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' then 'gonorrhea-gential'
else condition end condition
FROM stiprep_cases_of_interest T1
INNER JOIN (SELECT T1.patient_id, min(date)first_date, T2.indexsti_year 
		FROM stiprep_cases_of_interest T1, stiprep_second_sti T2 
		WHERE T1.date > T2.second_sti_date 
		AND T1.patient_id = T2.patient_id 
		AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv')
		GROUP BY T1.patient_id, T2.indexsti_year) T2  
		ON (T1.patient_id = T2.patient_id)
LEFT JOIN stiprep_gon_events T3 ON (T1.patient_id = T3.patient_id and T1.case_date = T3.date and name like '%positive%')
WHERE T1.case_date = T2.first_date
AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv');

CREATE TABLE stiprep_third_sti_agg AS
SELECT T1.patient_id, third_sti_date,
array_agg(distinct(T1.condition)) condition,
indexsti_year
FROM stiprep_third_sti T1
GROUP BY T1.patient_id, T1.indexsti_year, third_sti_date;

CREATE TABLE stiprep_third_sti_index AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN condition END) thirdsti_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN condition END) thirdsti_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN condition END) thirdsti_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN condition END) thirdsti_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN condition END) thirdsti_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN condition END) thirdsti_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN condition END) thirdsti_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN condition END) thirdsti_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN condition END) thirdsti_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN condition END) thirdsti_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN condition END) thirdsti_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN condition END) thirdsti_index2017
FROM stiprep_third_sti_agg T1
GROUP BY T1.patient_id;

-- Count of days between second STI case date and third STI case date
CREATE TABLE stiprep_third_days_compute AS 
SELECT T2.patient_id,
(third_sti_date - min(second_sti_date)) days_btwn_st2_sti3, T2.indexsti_year
FROM stiprep_second_sti_agg T1,
stiprep_third_sti_agg T2
WHERE T1.patient_id = T2.patient_id
AND T1.indexsti_year = T2.indexsti_year
GROUP BY T2.patient_id, third_sti_date, T2.indexsti_year;

CREATE TABLE stiprep_third_days AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN days_btwn_st2_sti3 END) thirdsti_2to3_days_index2017
FROM stiprep_third_days_compute T1
GROUP BY T1.patient_id;

-- FOURTH STI 
CREATE TABLE stiprep_fourth_sti AS
SELECT T1.patient_id, case_date fourth_sti_date, specimen_source, T2.indexsti_year,
CASE WHEN condition = 'gonorrhea' and specimen_source ~* '(THROAT)' then 'gonorrhea-oropharynx'
WHEN condition = 'gonorrhea' and specimen_source ~* '(RECTUM|RECT|ANAL)' then 'gonorrhea-rectal'
WHEN condition = 'gonorrhea' and specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' then 'gonorrhea-gential'
else condition end condition
FROM stiprep_cases_of_interest T1
INNER JOIN (SELECT T1.patient_id, min(date)first_date, T2.indexsti_year 
		FROM stiprep_cases_of_interest T1, stiprep_third_sti T2 
		WHERE T1.date > T2.third_sti_date 
		AND T1.patient_id = T2.patient_id 
		AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv')
		GROUP BY T1.patient_id, T2.indexsti_year) T2  
		ON (T1.patient_id = T2.patient_id)
LEFT JOIN stiprep_gon_events T3 ON (T1.patient_id = T3.patient_id and T1.case_date = T3.date and name like '%positive%')
WHERE T1.case_date = T2.first_date
AND T1.condition in ('syphilis', 'gonorrhea', 'chlamydia', 'hiv');

CREATE TABLE stiprep_fourth_sti_agg AS
SELECT T1.patient_id, fourth_sti_date,
array_agg(distinct(T1.condition)) condition,
indexsti_year
FROM stiprep_fourth_sti T1
GROUP BY T1.patient_id, T1.indexsti_year, fourth_sti_date;


CREATE TABLE stiprep_fourth_sti_index AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN condition END) fourthsti_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN condition END) fourthsti_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN condition END) fourthsti_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN condition END) fourthsti_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN condition END) fourthsti_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN condition END) fourthsti_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN condition END) fourthsti_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN condition END) fourthsti_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN condition END) fourthsti_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN condition END) fourthsti_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN condition END) fourthsti_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN condition END) fourthsti_index2017
FROM stiprep_fourth_sti_agg T1
GROUP BY T1.patient_id;

-- Count of days between third STI case date and fourth STI case date
CREATE TABLE stiprep_fourth_days_compute AS 
SELECT T2.patient_id,
(fourth_sti_date - min(third_sti_date)) days_btwn_st3_sti4, T2.indexsti_year
FROM stiprep_third_sti_agg T1,
stiprep_fourth_sti_agg T2
WHERE T1.patient_id = T2.patient_id
AND T1.indexsti_year = T2.indexsti_year
GROUP BY T2.patient_id, fourth_sti_date, T2.indexsti_year;

CREATE TABLE stiprep_fourth_days AS
SELECT T1.patient_id,
MAX(CASE WHEN indexsti_year = '2006' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2006,
MAX(CASE WHEN indexsti_year = '2007' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2007,
MAX(CASE WHEN indexsti_year = '2008' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2008,
MAX(CASE WHEN indexsti_year = '2009' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2009,
MAX(CASE WHEN indexsti_year = '2010' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2010,
MAX(CASE WHEN indexsti_year = '2011' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2011,
MAX(CASE WHEN indexsti_year = '2012' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2012,
MAX(CASE WHEN indexsti_year = '2013' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2013,
MAX(CASE WHEN indexsti_year = '2014' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2014,
MAX(CASE WHEN indexsti_year = '2015' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2015,
MAX(CASE WHEN indexsti_year = '2016' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2016,
MAX(CASE WHEN indexsti_year = '2017' THEN days_btwn_st3_sti4 END) fourthsti_3to4_days_index2017
FROM stiprep_fourth_days_compute T1
GROUP BY T1.patient_id;

-- test pat with multiple sti's 685685

-- CPT CODES - Counts & Column Creation
CREATE TABLE stiprep_cpts_of_interest AS 
SELECT patient_id,
min(anal_cytology_test_ever) anal_cytology_test_ever, 
min(ser_test_for_lgv_ever) ser_test_for_lgv_ever
FROM (
	SELECT T1.patient_id, 
		extract(year from date) as anal_cytology_test_ever,
	        null::real ser_test_for_lgv_ever	
		FROM  emr_labresult T1,
		stiprep_index_pats T2  
		WHERE native_code like '88160%' 
		AND T1.patient_id = T2.patient_id
		AND date < '01-01-2018'
		GROUP BY T1.patient_id, date
	UNION
	SELECT T1.patient_id, 
		extract(year from date) AS anal_cytology_test_ever,
		null::real ser_test_for_lgv_ever
		FROM emr_laborder T1,
		stiprep_index_pats T2  
		WHERE procedure_name in ( 'CYTOLOGY ANAL', 'CYTOLOGY THINPREP VIAL (ANAL)')
		AND T1.patient_id = T2.patient_id
		AND date < '01-01-2018'
		GROUP BY T1.patient_id, date
	UNION
	SELECT T1.patient_id, 
	        null::real anal_cytology_test_ever,
		extract(year from date) as ser_test_for_lgv_ever
		FROM  emr_labresult T1,
		stiprep_index_pats T2     
		WHERE native_code like '86631%' 
		AND T1.patient_id = T2.patient_id
		AND date < '01-01-2018'
		GROUP BY T1.patient_id, date)T1
GROUP BY patient_id;


-- HEP C - Counts & Column Creation
CREATE TABLE stiprep_hepc_counts AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2006' THEN 1 END)  t_hcv_anti_06,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2007' THEN 1 END)  t_hcv_anti_07,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2008' THEN 1 END)  t_hcv_anti_08,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2009' THEN 1 END)  t_hcv_anti_09,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2010' THEN 1 END)  t_hcv_anti_10,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2011' THEN 1 END)  t_hcv_anti_11,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2012' THEN 1 END)  t_hcv_anti_12,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2013' THEN 1 END)  t_hcv_anti_13,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2014' THEN 1 END)  t_hcv_anti_14,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2015' THEN 1 END)  t_hcv_anti_15,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2016' THEN 1 END)  t_hcv_anti_16,
count(CASE WHEN T1.test_name like '%elisa%' and T1.rpt_year = '2017' THEN 1 END)  t_hcv_anti_17,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2006' THEN 1 END)  t_hcv_rna_06,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2007' THEN 1 END)  t_hcv_rna_07,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2008' THEN 1 END)  t_hcv_rna_08,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2009' THEN 1 END)  t_hcv_rna_09,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2010' THEN 1 END)  t_hcv_rna_10,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2011' THEN 1 END)  t_hcv_rna_11,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2012' THEN 1 END)  t_hcv_rna_12,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2013' THEN 1 END)  t_hcv_rna_13,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2014' THEN 1 END)  t_hcv_rna_14,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2015' THEN 1 END)  t_hcv_rna_15,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2016' THEN 1 END)  t_hcv_rna_16,
count(CASE WHEN T1.test_name like '%rna%' and T1.rpt_year = '2017' THEN 1 END)  t_hcv_rna_17,
max (CASE WHEN T2.name like '%elisa:positive%' or name like '%rna:positive%' THEN 1 END) hcv_anti_or_rna_pos,
min(CASE WHEN T2.name like '%elisa:positive%' THEN T2.rpt_year  END) hcv_anti_pos_date,
min(CASE WHEN T2.name like '%rna:positive%' THEN T2.rpt_year  END) hcv_rna_pos_date 
FROM stiprep_hepc_labs T1 
LEFT OUTER JOIN stiprep_hepc_events T2 ON ((T1.patient_id = T2.patient_id) AND (T1.id = T2.object_id))  
GROUP BY T1.patient_id;


-- HEP C - Cases
CREATE TABLE stiprep_hepc_cases AS 
SELECT T1.patient_id,1 acute_hepc_per_esp,min(rpt_year) acute_hepc_per_esp_date 
FROM  stiprep_cases_of_interest T1   
WHERE condition = 'hepatitis_c:acute' 
GROUP BY T1.patient_id;

-- HEP B - Counts & Column Creation
CREATE TABLE stiprep_hepb_counts AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.test_name like '%surface_antigen%' and T1.rpt_year = '2006' THEN 1 END)  t_hepb_antigen_06,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2007' THEN 1 END)  t_hepb_antigen_07,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2008' THEN 1 END)  t_hepb_antigen_08,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2009' THEN 1 END)  t_hepb_antigen_09,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2010' THEN 1 END)  t_hepb_antigen_10,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2011' THEN 1 END)  t_hepb_antigen_11,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2012' THEN 1 END)  t_hepb_antigen_12,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2013' THEN 1 END)  t_hepb_antigen_13,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2014' THEN 1 END)  t_hepb_antigen_14,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2015' THEN 1 END)  t_hepb_antigen_15,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2016' THEN 1 END)  t_hepb_antigen_16,
count(CASE WHEN T1.test_name  like '%surface_antigen%' and T1.rpt_year = '2017' THEN 1 END)  t_hepb_antigen_17,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2006' THEN 1 END)  t_hepb_dna_06,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2007' THEN 1 END)  t_hepb_dna_07,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2008' THEN 1 END)  t_hepb_dna_08,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2009' THEN 1 END)  t_hepb_dna_09,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2010' THEN 1 END)  t_hepb_dna_10,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2011' THEN 1 END)  t_hepb_dna_11,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2012' THEN 1 END)  t_hepb_dna_12,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2013' THEN 1 END)  t_hepb_dna_13,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2014' THEN 1 END)  t_hepb_dna_14,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2015' THEN 1 END)  t_hepb_dna_15,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2016' THEN 1 END)  t_hepb_dna_16,
count(CASE WHEN T1.test_name  like '%dna%' and T1.rpt_year = '2017' THEN 1 END)  t_hepb_dna_17,
max(CASE WHEN T2.name like '%surface_antigen:positive%' or name like '%dna:positive%' THEN 1 END) hepb_pos_antigen_or_dna,
min(CASE WHEN T2.name like '%surface_antigen:positive%' or name like '%dna:positive%' THEN T2.rpt_year  END) hepb_pos_antigen_or_dna_date 
FROM stiprep_hebp_labs T1 
LEFT OUTER JOIN stiprep_hepb_events T2 ON ((T1.patient_id = T2.patient_id) AND (T1.id = T2.object_id))  
GROUP BY T1.patient_id;

-- HEP B - Cases
CREATE TABLE stiprep_hebp_cases AS 
SELECT T1.patient_id, 1 acute_hepb_per_esp, min(rpt_year) acute_hepb_per_esp_date 
FROM  stiprep_cases_of_interest T1   
WHERE condition = 'hepatitis_b:acute'  
GROUP BY T1.patient_id;

-- HepB Diagnosis Codes
CREATE TABLE stiprep_hepb_diags AS 
SELECT T1.id, T1.encounter_id, T1.dx_code_id 
FROM  emr_encounter_dx_codes T1
WHERE  
(dx_code_id ~ '^(icd9:070.2)' or dx_code_id ~ '^(icd9:070.3)' or dx_code_id ~ '^(icd10:B18.0)'
or  dx_code_id ~ '^(icd10:B18.1)'  or  dx_code_id ~ '^(icd10:B19.1)');

-- HepB Diagnosis Codes - Faster as 2 queries
CREATE TABLE stiprep_hepb_diags_2 AS
SELECT T2.patient_id, 1::int hist_of_hep_b
FROM stiprep_hepb_diags T1,
emr_encounter T2,
stiprep_index_pats T3
WHERE T1.encounter_id = T2.id
AND T2.patient_id = T3.patient_id
AND T2.date < '01-01-2018'
GROUP BY T2.patient_id;


-- HIV - Lab Counts & Column Creation
CREATE TABLE stiprep_hiv_counts AS 
SELECT T1.patient_id,
count(*) total_hiv_tests, 
MIN(CASE WHEN test_name = 'hiv_rna_viral' then rpt_year END) first_rna_viral_date,
count(CASE WHEN test_name = 'hiv_rna_viral' then 1 END) total_hiv_rna_tests, 
count(CASE WHEN rpt_year = '2006' THEN 1 END)  t_hiv_06, count(CASE WHEN rpt_year = '2007' THEN 1 END)  t_hiv_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_hiv_08, count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_hiv_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_hiv_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_hiv_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_hiv_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_hiv_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_hiv_14, count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_hiv_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_hiv_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_hiv_17,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2006' THEN 1 END)  t_hiv_elisa_06,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2007' THEN 1 END)  t_hiv_elisa_07,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2008' THEN 1 END)  t_hiv_elisa_08,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2009' THEN 1 END)  t_hiv_elisa_09,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2010' THEN 1 END)  t_hiv_elisa_10,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2011' THEN 1 END)  t_hiv_elisa_11,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2012' THEN 1 END)  t_hiv_elisa_12,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2013' THEN 1 END)  t_hiv_elisa_13,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2014' THEN 1 END)  t_hiv_elisa_14,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2015' THEN 1 END)  t_hiv_elisa_15,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2016' THEN 1 END)  t_hiv_elisa_16,
count(CASE WHEN test_name = 'hiv_elisa' and rpt_year = '2017' THEN 1 END)  t_hiv_elisa_17,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2006' THEN 1 END) t_hiv_wb_06,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2007' THEN 1 END) t_hiv_wb_07,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2008' THEN 1 END) t_hiv_wb_08,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2009' THEN 1 END) t_hiv_wb_09,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2010' THEN 1 END) t_hiv_wb_10,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2011' THEN 1 END) t_hiv_wb_11,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2012' THEN 1 END) t_hiv_wb_12,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2013' THEN 1 END) t_hiv_wb_13,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2014' THEN 1 END) t_hiv_wb_14,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2015' THEN 1 END) t_hiv_wb_15,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2016' THEN 1 END) t_hiv_wb_16,
count(CASE WHEN test_name = 'hiv_wb' and rpt_year = '2017' THEN 1 END) t_hiv_wb_17,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2006' THEN 1 END) t_hiv_rna_06,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2007' THEN 1 END) t_hiv_rna_07,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2008' THEN 1 END) t_hiv_rna_08,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2009' THEN 1 END) t_hiv_rna_09,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2010' THEN 1 END) t_hiv_rna_10,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2011' THEN 1 END) t_hiv_rna_11,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2012' THEN 1 END) t_hiv_rna_12,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2013' THEN 1 END) t_hiv_rna_13,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2014' THEN 1 END) t_hiv_rna_14,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2015' THEN 1 END) t_hiv_rna_15,
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2016' THEN 1 END) t_hiv_rna_16,  
count(CASE WHEN test_name = 'hiv_rna_viral' and rpt_year = '2017' THEN 1 END) t_hiv_rna_17,  
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2006' THEN 1 END) t_hiv_agab_06,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2007' THEN 1 END) t_hiv_agab_07,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2008' THEN 1 END) t_hiv_agab_08,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2009' THEN 1 END) t_hiv_agab_09,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2010' THEN 1 END) t_hiv_agab_10,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2011' THEN 1 END) t_hiv_agab_11,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2012' THEN 1 END) t_hiv_agab_12,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2013' THEN 1 END) t_hiv_agab_13,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2014' THEN 1 END) t_hiv_agab_14,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2015' THEN 1 END) t_hiv_agab_15,
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2016' THEN 1 END) t_hiv_agab_16, 
count(CASE WHEN test_name = 'hiv_ag_ab' and rpt_year = '2017' THEN 1 END) t_hiv_agab_17   
FROM  stiprep_hiv_labs T1   
GROUP BY T1.patient_id;

-- HIV - Cases
CREATE TABLE stiprep_hiv_cases AS 
SELECT T1.patient_id, 1::int hiv_per_esp, min(EXTRACT(YEAR FROM date)) hiv_per_esp_date, T1.date
FROM  stiprep_cases_of_interest T1
WHERE condition = 'hiv' 
GROUP BY T1.patient_id,T1.date;

-- HIV MAX VIRAL LOAD YEAR
-- Use most recent year when same max viral load is encountered on diff years
CREATE TABLE stiprep_max_viral_load AS
SELECT T1.patient_id, max(rpt_year) as max_viral_load_year, max_viral_load
FROM stiprep_hiv_labs T1,
(SELECT patient_id, max(result_float) max_viral_load FROM stiprep_hiv_labs WHERE test_name like '%rna%' GROUP BY patient_id) T2
WHERE T1.patient_id = T2.patient_id
AND T1.result_float = T2.max_viral_load
AND T1.test_name like '%rna%'
GROUP BY T1.patient_id, max_viral_load;


-- HIV - New Diagnosis Computation
CREATE TABLE stiprep_hiv_new_diag AS 
SELECT T2.patient_id, 
max(case  when T1.name in ('lx:hiv_elisa:positive', 'lx:hiv_wb:positive', 'lx:hiv_ag_ab:positive') and T1.date <=  T2.date then 1  
		  when T1.name in ('lx:hiv_elisa:negative', 'lx:hiv_ag_ab:negative') and T1.date >= (T2.date - INTERVAL '2 years') then 1 else 0 end) new_hiv_diagnosis 
FROM stiprep_wbpos_elisaposneg T1 
RIGHT OUTER JOIN stiprep_hiv_cases T2 ON ((T1.patient_id = T2.patient_id))  
GROUP BY T2.patient_id;

-- HIV - Medications
CREATE TABLE stiprep_hiv_meds AS 
SELECT T1.patient_id,
T1.name,
split_part(T1.name, ':', 2) stripped_name,
T1.date,
case when T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end truvada_rx,
case when T1.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1)  ELSE 1 END  refills_mod,
1::int hiv_meds,
string_to_array(replace(T1.name, 'rx:hiv_',''), '-') test4,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-')::text test5,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-') med_array,
quantity,
quantity_float,
quantity_type,
EXTRACT(YEAR FROM T1.date)  rpt_year
FROM hef_event T1,
emr_prescription T2,
stiprep_index_pats T3 
WHERE T1.object_id = T2.id
AND T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.patient_id = T2.patient_id 
AND T1.name ilike 'rx:hiv_%'
AND T2.date <= '01-01-2018'; 

-- -- HIV - Meds - Patient Id's Only
-- CREATE TABLE stiprep_hiv_meds_distinct AS
-- SELECT T1.patient_id
-- FROM stiprep_hiv_meds T1
-- GROUP BY T1.patient_id;

-- HIV - Meds - Patient Id's Only & First HIV Med Date
CREATE TABLE stiprep_hiv_meds_distinct AS
SELECT T1.patient_id, min(extract(year from date)) first_hiv_med_date
FROM stiprep_hiv_meds T1
GROUP BY T1.patient_id;

-- HIV on the Problem List
CREATE TABLE stiprep_hiv_problem_list AS 
SELECT DISTINCT ON(patient_id) T1.id,T1.patient_id, extract(year from T2.first_date) hiv_first_prob_date, T1.dx_code_id, 1::int hiv_prob 
FROM  emr_problem T1,
	(SELECT patient_id, min(date) as first_date from emr_problem WHERE  dx_code_id ~ '^(icd9:042)' or dx_code_id ~ '^(icd9:V08)' or dx_code_id ~ '^(icd10:B20)' or  dx_code_id ~ '^(icd10:B21)'
	or  dx_code_id ~ '^(icd10:B22)'  or  dx_code_id ~ '^(icd10:B23)' or dx_code_id ~ '^(icd10:B24)' 
	or dx_code_id ~ '^(icd10:B97.35)' or dx_code_id ~ '^(icd10:Z21)' group by patient_id) T2,
	stiprep_index_pats T3 
WHERE  (dx_code_id ~ '^(icd9:042)' or dx_code_id ~ '^(icd9:V08)' or dx_code_id ~ '^(icd10:B20)' or  dx_code_id ~ '^(icd10:B21)'
or  dx_code_id ~ '^(icd10:B22)'  or  dx_code_id ~ '^(icd10:B23)' or dx_code_id ~ '^(icd10:B24)' 
or dx_code_id ~ '^(icd10:B97.35)' or dx_code_id ~ '^(icd10:Z21)')
AND T1.date = T2.first_date
AND T1.patient_id = T2.patient_id
AND T1.patient_id = T3.patient_id
AND T2.patient_id = T3.patient_id ;

-- HIV - Truvada Array - For those with more than 2 rx (includes refills)
CREATE TABLE stiprep_truvada_array AS 
SELECT T1.patient_id, T1.rpt_year, sum(T1.refills_mod)  total_truvada_rx,
array_agg(T1.date ORDER BY T1.date)  truvada_array 
FROM  stiprep_hiv_meds T1 
WHERE truvada_rx = 1  
GROUP BY T1.patient_id,T1.rpt_year 
HAVING sum(T1.refills_mod) >=2;


-- Truvada - Find those that have prescriptions 2 or more months apart in the same year
CREATE TABLE stiprep_truvada_2mogap AS 
SELECT T1.patient_id, 
T1.truvada_array[1] first_rx,
truvada_array[array_length(truvada_array, 1)]   final_rx,
T1.rpt_year,
truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] two_month_check,
1 truvada_criteria_met 
FROM  stiprep_truvada_array T1   
WHERE truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] >= 60;

-- HIV - Break down the combo meds in to distinct individual meds.
CREATE TABLE stiprep_hiv_rx_combo_distinct AS
SELECT distinct unnest(med_array) as distinct_med, patient_id, rpt_year 
FROM stiprep_hiv_meds 
GROUP BY patient_id, med_array, rpt_year
ORDER BY patient_id;

-- HIV - Rx for ≥3 different HIV meds - Part 1
CREATE TABLE stiprep_hiv_rx_3_diff AS 
SELECT T1.patient_id,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2006 then 1 else 0 end three_diff_hiv_med_06, 
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2007 then 1 else 0 end three_diff_hiv_med_07,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2008 then 1 else 0 end three_diff_hiv_med_08,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2009 then 1 else 0 end three_diff_hiv_med_09,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2010 then 1 else 0 end three_diff_hiv_med_10,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2011 then 1 else 0 end three_diff_hiv_med_11,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2012 then 1 else 0 end three_diff_hiv_med_12,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2013 then 1 else 0 end three_diff_hiv_med_13,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2014 then 1 else 0 end three_diff_hiv_med_14,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2015 then 1 else 0 end three_diff_hiv_med_15,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2016 then 1 else 0 end three_diff_hiv_med_16,
CASE WHEN count(distinct(distinct_med)) >= 3 and rpt_year = 2017 then 1 else 0 end three_diff_hiv_med_17
FROM  stiprep_hiv_rx_combo_distinct T1   
GROUP BY T1.patient_id, T1.rpt_year;

-- HIV - Rx for ≥3 different HIV meds - Part 2
CREATE TABLE stiprep_hiv_rx_3_diff_count AS
SELECT T1.patient_id,
MAX(three_diff_hiv_med_06) three_diff_hiv_med_06, 
MAX(three_diff_hiv_med_07) three_diff_hiv_med_07,
MAX(three_diff_hiv_med_08) three_diff_hiv_med_08,
MAX(three_diff_hiv_med_09) three_diff_hiv_med_09,
MAX(three_diff_hiv_med_10) three_diff_hiv_med_10,
MAX(three_diff_hiv_med_11) three_diff_hiv_med_11,
MAX(three_diff_hiv_med_12) three_diff_hiv_med_12,
MAX(three_diff_hiv_med_13) three_diff_hiv_med_13,
MAX(three_diff_hiv_med_14) three_diff_hiv_med_14,
MAX(three_diff_hiv_med_15) three_diff_hiv_med_15,
MAX(three_diff_hiv_med_16) three_diff_hiv_med_16,
MAX(three_diff_hiv_med_17) three_diff_hiv_med_17
FROM  stiprep_hiv_rx_3_diff T1
GROUP BY T1.patient_id;

-- HIV Number of Truvada Prescriptions & Total Number of Pills
CREATE TABLE stiprep_truvada_rx_and_pills AS 
SELECT T1.patient_id,rpt_year,
sum( refills_mod ) truvada_num_rx,
sum(case when quantity_type in ('tab', 'tabs', '', 'tablet') or quantity_type is null then (quantity_float * refills_mod) else 0 end) truvada_num_pills
FROM  stiprep_hiv_meds T1   
WHERE  truvada_rx = 1 
GROUP BY T1.patient_id, rpt_year;

CREATE TABLE stiprep_truvada_rx_and_pills_peryear AS
SELECT T1.patient_id,
max(coalesce(CASE WHEN rpt_year = 2006 then truvada_num_rx end, 0)) truvada_num_rx_06,
max(coalesce(CASE WHEN rpt_year = 2007 then truvada_num_rx end, 0)) truvada_num_rx_07,
max(coalesce(CASE WHEN rpt_year = 2008 then truvada_num_rx end, 0)) truvada_num_rx_08,
max(coalesce(CASE WHEN rpt_year = 2009 then truvada_num_rx end, 0)) truvada_num_rx_09,
max(coalesce(CASE WHEN rpt_year = 2010 then truvada_num_rx end, 0)) truvada_num_rx_10,
max(coalesce(CASE WHEN rpt_year = 2011 then truvada_num_rx end, 0)) truvada_num_rx_11,
max(coalesce(CASE WHEN rpt_year = 2012 then truvada_num_rx end, 0)) truvada_num_rx_12,
max(coalesce(CASE WHEN rpt_year = 2013 then truvada_num_rx end, 0)) truvada_num_rx_13,
max(coalesce(CASE WHEN rpt_year = 2014 then truvada_num_rx end, 0)) truvada_num_rx_14,
max(coalesce(CASE WHEN rpt_year = 2015 then truvada_num_rx end, 0)) truvada_num_rx_15,
max(coalesce(CASE WHEN rpt_year = 2016 then truvada_num_rx end, 0)) truvada_num_rx_16,
max(coalesce(CASE WHEN rpt_year = 2017 then truvada_num_rx end, 0)) truvada_num_rx_17,
max(coalesce(CASE WHEN rpt_year = 2006 then truvada_num_pills end, 0)) truvada_num_pills_06,
max(coalesce(CASE WHEN rpt_year = 2007 then truvada_num_pills end, 0)) truvada_num_pills_07,
max(coalesce(CASE WHEN rpt_year = 2008 then truvada_num_pills end, 0)) truvada_num_pills_08,
max(coalesce(CASE WHEN rpt_year = 2009 then truvada_num_pills end, 0)) truvada_num_pills_09,
max(coalesce(CASE WHEN rpt_year = 2010 then truvada_num_pills end, 0)) truvada_num_pills_10,
max(coalesce(CASE WHEN rpt_year = 2011 then truvada_num_pills end, 0)) truvada_num_pills_11,
max(coalesce(CASE WHEN rpt_year = 2012 then truvada_num_pills end, 0)) truvada_num_pills_12,
max(coalesce(CASE WHEN rpt_year = 2013 then truvada_num_pills end, 0)) truvada_num_pills_13,
max(coalesce(CASE WHEN rpt_year = 2014 then truvada_num_pills end, 0)) truvada_num_pills_14,
max(coalesce(CASE WHEN rpt_year = 2015 then truvada_num_pills end, 0)) truvada_num_pills_15,
max(coalesce(CASE WHEN rpt_year = 2016 then truvada_num_pills end, 0)) truvada_num_pills_16,
max(coalesce(CASE WHEN rpt_year = 2017 then truvada_num_pills end, 0)) truvada_num_pills_17
FROM stiprep_truvada_rx_and_pills T1
GROUP BY T1.patient_id;


-- HIV - Truvada Counts & Column Creation 
CREATE TABLE stiprep_truvada_counts AS 
SELECT T1.patient_id,
max(case when truvada_criteria_met = 1 and rpt_year = 2006  and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date)  then 1 else 0 end)  hiv_neg_truvada_06,
max(case when truvada_criteria_met = 1 and rpt_year = 2007 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_07,
max(case when truvada_criteria_met = 1  and rpt_year = 2008 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_08,
max(case when truvada_criteria_met = 1  and rpt_year = 2009 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_09,
max(case when truvada_criteria_met = 1 and rpt_year = 2010 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_10,
max(case when truvada_criteria_met = 1 and rpt_year = 2011 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_11,
max(case when truvada_criteria_met = 1  and rpt_year = 2012 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_12,
max(case when truvada_criteria_met = 1  and rpt_year = 2013 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_13,
max(case when truvada_criteria_met = 1 and rpt_year = 2014 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_14,
max(case when truvada_criteria_met = 1  and rpt_year = 2015 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_15,
max(case when truvada_criteria_met = 1  and rpt_year = 2016 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_16,
max(case when truvada_criteria_met = 1  and rpt_year = 2017 and (hiv_per_esp is null or hiv_per_esp =1 and rpt_year < hiv_per_esp_date) then 1 else 0 end) hiv_neg_truvada_17   
FROM stiprep_truvada_2mogap T1 
LEFT OUTER JOIN stiprep_hiv_cases T2 ON ((T1.patient_id = T2.patient_id))  
GROUP BY T1.patient_id;



-- HIV - Full Outer Join HIV Lab Events, Meds, and Cases - Derive Compound Values
CREATE TABLE stiprep_hiv_all_details AS SELECT 
coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id, hiv_3_diff.patient_id, hiv_truv_rx.patient_id) patient_id,
(case when total_hiv_tests is null and hiv_per_esp is null then 2
        when total_hiv_tests > 0 and hiv_per_esp is null then 0
        else hiv_per_esp end) as hiv_per_esp_final, 
coalesce(hiv_per_esp_date, null) as hiv_per_esp_date,
(case when hiv_per_esp is null and total_hiv_rna_tests > 0 then 1
        when hiv_per_esp is null and (total_hiv_rna_tests = 0 or total_hiv_rna_tests is null) then 0
        else null end) as hiv_neg_with_rna_test,
-- If HIV neg but has had a rna test then print the first year of the rna viral test
(case when hiv_per_esp is null and total_hiv_rna_tests > 0 then first_rna_viral_date else null end) hiv_neg_first_rna_test_yr,
(case when meds.patient_id is null and hiv_per_esp is null then 0
         when meds.patient_id is not null and hiv_per_esp is null then 1
         else null end) as hiv_neg_with_meds,
(case when meds.patient_id is not null and hiv_per_esp is null then first_hiv_med_date
         else null end) as hiv_neg_first_hiv_med_yr,
coalesce(hiv_neg_truvada_06,0) as hiv_neg_truvada_06,
coalesce(hiv_neg_truvada_07,0) as hiv_neg_truvada_07,
coalesce(hiv_neg_truvada_08,0) as hiv_neg_truvada_08,
coalesce(hiv_neg_truvada_09,0) as hiv_neg_truvada_09,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_prob, 0) as hiv_on_problem_list, 
coalesce(hiv_first_prob_date, null) as hiv_first_prob_date,
coalesce(three_diff_hiv_med_06, 0) three_diff_hiv_med_06, 
coalesce(three_diff_hiv_med_07, 0) three_diff_hiv_med_07,
coalesce(three_diff_hiv_med_08, 0) three_diff_hiv_med_08,
coalesce(three_diff_hiv_med_09, 0) three_diff_hiv_med_09,
coalesce(three_diff_hiv_med_10, 0) three_diff_hiv_med_10,
coalesce(three_diff_hiv_med_11, 0) three_diff_hiv_med_11,
coalesce(three_diff_hiv_med_12, 0) three_diff_hiv_med_12,
coalesce(three_diff_hiv_med_13, 0) three_diff_hiv_med_13,
coalesce(three_diff_hiv_med_14, 0) three_diff_hiv_med_14,
coalesce(three_diff_hiv_med_15, 0) three_diff_hiv_med_15,
coalesce(three_diff_hiv_med_16, 0) three_diff_hiv_med_16,
coalesce(three_diff_hiv_med_17, 0) three_diff_hiv_med_17,
coalesce(truvada_num_rx_06, 0) truvada_num_rx_06,
coalesce(truvada_num_rx_07, 0) truvada_num_rx_07,
coalesce(truvada_num_rx_08, 0) truvada_num_rx_08,
coalesce(truvada_num_rx_09, 0) truvada_num_rx_09,
coalesce(truvada_num_rx_10, 0) truvada_num_rx_10,
coalesce(truvada_num_rx_11, 0) truvada_num_rx_11,
coalesce(truvada_num_rx_12, 0) truvada_num_rx_12,
coalesce(truvada_num_rx_13, 0) truvada_num_rx_13,
coalesce(truvada_num_rx_14, 0) truvada_num_rx_14,
coalesce(truvada_num_rx_15, 0) truvada_num_rx_15,
coalesce(truvada_num_rx_16, 0) truvada_num_rx_16,
coalesce(truvada_num_rx_17, 0) truvada_num_rx_17,
coalesce(truvada_num_pills_06, 0) truvada_num_pills_06,
coalesce(truvada_num_pills_07, 0) truvada_num_pills_07,
coalesce(truvada_num_pills_08, 0) truvada_num_pills_08,
coalesce(truvada_num_pills_09, 0) truvada_num_pills_09,
coalesce(truvada_num_pills_10, 0) truvada_num_pills_10,
coalesce(truvada_num_pills_11, 0) truvada_num_pills_11,
coalesce(truvada_num_pills_12, 0) truvada_num_pills_12,
coalesce(truvada_num_pills_13, 0) truvada_num_pills_13,
coalesce(truvada_num_pills_14, 0) truvada_num_pills_14,
coalesce(truvada_num_pills_15, 0) truvada_num_pills_15,
coalesce(truvada_num_pills_16, 0) truvada_num_pills_16,
coalesce(truvada_num_pills_17, 0) truvada_num_pills_17
FROM stiprep_hiv_counts lab
FULL OUTER JOIN stiprep_hiv_cases cases
    ON lab.patient_id=cases.patient_id
FULL OUTER JOIN stiprep_hiv_meds_distinct meds
    ON meds.patient_id = coalesce(lab.patient_id, cases.patient_id)
FULL OUTER JOIN stiprep_truvada_counts truvada
    ON truvada.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id)
FULL OUTER JOIN stiprep_hiv_problem_list hiv_prob
	ON hiv_prob.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id)
FULL OUTER JOIN stiprep_hiv_rx_3_diff_count hiv_3_diff
	ON hiv_3_diff.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id)
FULL OUTER JOIN stiprep_truvada_rx_and_pills_peryear hiv_truv_rx
	ON hiv_truv_rx.patient_id = coalesce(lab.patient_id, cases.patient_id, meds.patient_id, truvada.patient_id, hiv_prob.patient_id, hiv_3_diff.patient_id);
	
	
-- -- ENCOUNTERS - Gather up encounters
-- CREATE TABLE stiprep_all_encs  AS 
-- SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.id as enc_id  
-- FROM emr_encounter T1, 
-- stiprep_index_pats T2,
-- static_enc_type_lookup T3
-- WHERE T1.patient_id = T2.patient_id  
-- AND T1.raw_encounter_type = T3.raw_encounter_type 
-- AND (T1.raw_encounter_type is NULL or T3.ambulatory = 1)
-- AND  T1.date >= '01-01-2006' 
-- AND T1.date < '01-01-2018';

-- TEST TEST TEST
CREATE TABLE stiprep_all_encs  AS 
SELECT patient_id, rpt_year, date, array_agg(enc_id) enc_id
FROM (
	SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.date, T1.id as enc_id  
	FROM emr_encounter T1, 
	stiprep_index_pats T2,
	emr_encounter_dx_codes T3
	WHERE T1.patient_id = T2.patient_id 
	AND T1.id = T3.encounter_id
	AND (
		 (T1.weight > 0 OR T1.height > 0
		 OR T1.bp_systolic > 0 OR T1.bp_diastolic > 0 
		 OR T1.temperature > 0 
		 OR T1.pregnant = TRUE OR T1.edd is not null
		 )
		 OR dx_code_id <> 'icd9:799.9'
	)
	AND  T1.date >= '01-01-2006' 
	AND T1.date < '01-01-2018'
		UNION
	SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.date, null as enc_id  
	FROM emr_labresult T1, 
	stiprep_index_pats T2
	WHERE T1.patient_id = T2.patient_id  
	AND  T1.date >= '01-01-2006' 
	AND T1.date < '01-01-2018'
		UNION
	SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.date, null as enc_id  
	FROM emr_prescription T1, 
	stiprep_index_pats T2
	WHERE T1.patient_id = T2.patient_id  
	AND  T1.date >= '01-01-2006' 
	AND T1.date < '01-01-2018') T1
group by patient_id, rpt_year, date;


-- ENCOUNTERS - Counts & Column Creation
CREATE TABLE stiprep_enc_counts AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2006' THEN 1 END) t_encounters_06,
count(CASE WHEN rpt_year = '2007' THEN 1 END) t_encounters_07,
count(CASE WHEN rpt_year = '2008' THEN 1 END) t_encounters_08,
count(CASE WHEN rpt_year = '2009' THEN 1 END) t_encounters_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END) t_encounters_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END) t_encounters_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END) t_encounters_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END) t_encounters_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END) t_encounters_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END) t_encounters_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END) t_encounters_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END) t_encounters_17
FROM stiprep_all_encs T1 
GROUP BY T1.patient_id;



-- BASE Gather up dx_codes 
CREATE TABLE stiprep_dx_codes AS 
SELECT * FROM emr_encounter_dx_codes 
WHERE dx_code_id ~'^(icd9:796.7|icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd9:099.4|icd9:054.1|icd10:A63.|icd9:304.0|icd10:F11.|icd9:304.1|icd10:F13.|icd9:304.2|icd10:F14.|icd9:304.4|icd10:F15.|icd10:F19.|icd9:304.9|icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21|icd10:F32.|icd10:F33.)' 
OR dx_code_id in (
	'icd10:D01.3',
	'icd9:569.44',
	'icd9:230.5',
	'icd9:230.6',
	'icd9:097.9',
	'icd10:A52.0',
	'icd10:A52,7',
	'icd9:091.1',
	'icd10:A51.1',
	'icd9:098.7',
	'icd10:A54.6',
	'icd9:098.6',
	'icd10:A54.5', 
	'icd9:099.52', 
	'icd10:A56.3', 
	'icd9:099.51', 
	'icd10:A56.4', 
	'icd9:099.1', 
	'icd10:A55', 
	'icd9:099.0', 
	'icd10:A57', 
	'icd9:099.2', 
	'icd10:A58', 
	'icd10:N34.1', 
	'icd9:054.8', 
	'icd9:054.9', 
	'icd9:054.79', 
	'icd10:A60.1', 
	'icd10:A60.9', 
	'icd10:A60.0', 
	'icd9:078.11', 
	'icd10:A63.0', 
	'icd9:569.41', 
	'icd10:A64', 
	'icd10:K62.6', 
	'icd9:614', 
	'icd9:614.1', 
	'icd9:614.2', 
	'icd9:614.3', 
	'icd9:614.5', 
	'icd9:614.9', 
	'icd10:N72', 
	'icd10:N73.0', 
	'icd10:N73.5', 
	'icd10:N73.9', 
	'icd9:V01.6', 
	'icd10:Z20.2', 
	'icd10:Z20.6', 
	'icd9:V69.2', 
	'icd10:Z72.5', 
	'icd9:V65.44', 
	'icd10:Z71.7', 
	'icd9:307.1', 
	'icd10:F50.0', 
	'icd9:307.51', 
	'icd10:F50.2', 
	'icd9:307.50', 
	'icd10:F50.9', 
	'icd9:302.0', 
	'icd10:F64.8', 
	'icd10:F64.9', 
	'icd10:F66.0', 
	'icd9:302.5', 
	'icd9:302.50',  
	'icd9:302.51', 
	'icd9:302.52', 
	'icd9:302.53', 
	'icd10:F64.0', 
	'icd10:64.1', 
	'icd9:V61.21', 
	'icd9:937', 
	'icd10:T18.5', 
	'icd9:305.00', 
	'icd9:305.01', 
	'icd9:305.02', 
	'icd9:303.90', 
	'icd9:303.91', 
	'icd9:303.92', 
	'icd10:F10.2', 
	'icd10:Z61.4', 
	'icd10:Z61.5',
	'icd10:R85.619',
	'icd10:K62.82',
	'icd9:296.2',
	'icd9:296.3',
	'icd9:296.82',
	'icd9:300.4',
	'icd9:301.12',
	'icd9:308.0',
	'icd9:309.1',
	'icd9:309.28',
	'icd9:311',
	'icd10:F34.1',
	'icd10:F43.21',
	'icd10:F43.23',
	'icd10:F06.31',
	'icd10:F06.32'
);

set enable_nestloop=off;


-- DIAGNOSES - Create fields
CREATE TABLE stiprep_diag_fields AS 
SELECT T1.patient_id, 
min(case when T2.dx_code_id like 'icd9:796.7%' or T2.dx_code_id in ('icd10:R85.619')  then to_char(T1.date, 'yyyy') end) abnormal_anal_cytology,
min(case when T2.dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.)' 
	or T2.dx_code_id in ('icd9:097.9', 'icd10:A52.0','icd10:A51.1', 'icd10:A52.7') then to_char(T1.date, 'yyyy')  end) syphillis_of_any_site_or_stage_except_late,
min(case when T2.dx_code_id in ('icd10:A51.1','icd9:091.1') then to_char(T1.date, 'yyyy')  end) anal_syphilis,
min(case when T2.dx_code_id in ('icd9:098.7','icd10:A54.6') then to_char(T1.date, 'yyyy') end) gonococcal_infection_of_anus_and_rectum,
min(case when T2.dx_code_id in ('icd9:098.6','icd10:A54.5') then to_char(T1.date, 'yyyy') end) gonococcal_pharyngitis,
min(case when T2.dx_code_id in ('icd9:099.52','icd10:A56.3') then to_char(T1.date, 'yyyy')  end) chlamydial_infection_of_anus_and_recturm,
min(case when T2.dx_code_id in ('icd9:099.51','icd10:A56.4') then to_char(T1.date, 'yyyy')  end) chlamydial_infection_of_pharynx,
min(case when T2.dx_code_id in ('icd9:099.1','icd10:A55') then to_char(T1.date, 'yyyy') end) lymphgranuloma_venereum,
min(case when T2.dx_code_id in ('icd9:099.0','icd10:A57') then to_char(T1.date, 'yyyy') end) chancroid,
min(case when T2.dx_code_id in ('icd9:099.2', 'icd10:A58') then to_char(T1.date, 'yyyy')  end) granuloma_inguinale,
min(case when T2.dx_code_id like 'icd9:099.4%' or dx_code_id in ('icd10:N34.1') then to_char(T1.date, 'yyyy')  end) nongonococcal_urethritis,
min(case when T2.dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9') then to_char(T1.date, 'yyyy') end) herpes_simplex_w_complications,
min(case when T2.dx_code_id like 'icd9:054.1%' or dx_code_id in ('icd10:A60.0') then to_char(T1.date, 'yyyy')  end) genital_herpes,
min(case when T2.dx_code_id in ('icd9:078.11', 'icd10:A63.0') then to_char(T1.date, 'yyyy') end) anogenital_warts,
min(case when T2.dx_code_id in ('icd9:569.41', 'icd10:62.6') then to_char(T1.date, 'yyyy') end) anorectal_ulcer,
min(case when T2.dx_code_id like 'icd10:A63.%' or dx_code_id in ('icd10:A64') then to_char(T1.date, 'yyyy') end) unspecified_std,
min(case when T2.dx_code_id in ('icd9:614', 'icd9:614.1', 'icd9:614.2', 'icd9:614.3', 'icd9:614.5', 'icd9:614.9','icd10:N72', 'icd10:N73.0', 'icd10:N73.5', 'icd10:N73.9') 
	then to_char(T1.date, 'yyyy') end) pelvic_inflammatory_disease,
min(case when T2.dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') then to_char(T1.date, 'yyyy')  end) contact_with_or_exposure_to_venereal_disease,
min(case when T2.dx_code_id in ('icd9:V69.2','icd10:Z72.5') then to_char(T1.date, 'yyyy')  end) high_risk_sexual_behavior,
min(case when T2.dx_code_id in ('icd9:V65.44','icd10:Z71.7') then to_char(T1.date, 'yyyy') end) hiv_counseling,
min(case when T2.dx_code_id in ('icd9:307.1','icd10:F50.0') then to_char(T1.date, 'yyyy')  end) anorexia_nervosa,
min(case when T2.dx_code_id in ('icd9:307.51','icd10:F50.2') then to_char(T1.date, 'yyyy') end) bulimia_nervosa,
min(case when T2.dx_code_id in ('icd9:307.50','icd10:F50.9') then to_char(T1.date, 'yyyy') end) eating_disorder_nos,
min(case when T2.dx_code_id in ('icd9:302.0', 'icd10:F64.8', 'icd10:F64.9', 'icd10:F66.0') then to_char(T1.date, 'yyyy')  end) gender_identity_disorders,
min(case when T2.dx_code_id in ('icd9:302.5', 'icd9:302.50', 'icd9:302.51', 'icd9:302.52', 'icd9:302.53', 'icd10:F64.0', 'icd10:64.1') then to_char(T1.date, 'yyyy') end) trans_sexualism,
min(case when T2.dx_code_id in ('icd9:V61.21', 'icd10:Z61.4', 'icd10:Z61.5') then to_char(T1.date, 'yyyy') end) counseling_for_child_sexual_abuse,
min(case when T2.dx_code_id in ('icd9:937', 'icd10:T18.5') then to_char(T1.date, 'yyyy') end) foreign_body_in_anus,
min(case when T2.dx_code_id in ('icd9:305.00', 'icd9:305.01', 'icd9:305.02', 'icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2') then to_char(T1.date, 'yyyy') end) alcohol_dependence,
min(case when T2.dx_code_id ~ '^(icd9:304.0|icd10:F11.)' then to_char(T1.date, 'yyyy')  end) opioid_dependence,
min(case when T2.dx_code_id ~ '^(icd9:304.1|icd10:F13.)' then to_char(T1.date, 'yyyy') end) sedative_hypnotic_or_anxiolytic_dependence,
min(case when T2.dx_code_id ~ '^(icd9:304.2|icd10:F14.)' then to_char(T1.date, 'yyyy') end) cocaine_dependence,
min(case when T2.dx_code_id ~ '^(icd9:304.4|icd10:F15.)' then to_char(T1.date, 'yyyy') end) amphetamine_dependence,
min(case when T2.dx_code_id~ '^(icd9:304.9|icd10:F19.)' then to_char(T1.date, 'yyyy') end) unspecified_drug_dependence,
min(case when T2.dx_code_id in ('icd9:569.44', 'icd10:K62.82') then to_char(T1.date, 'yyyy') end)  anal_dysplasia,
min(case when T2.dx_code_id in ('icd9:230.5', 'icd9:230.6', 'icd10:D01.3') then to_char(T1.date, 'yyyy') end)  anal_carcinoma_in_situ,
min(case when T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' then to_char(T1.date, 'yyyy') end) hiv_first_icd_year,
min(case when T2.dx_code_id~ '^(icd10:F32.|icd10:F33.)' or T2.dx_code_id in ('icd9:296.2','icd9:296.3','icd9:296.82','icd9:300.4','icd9:301.12','icd9:308.0','icd9:309.1','icd9:309.28','icd9:311','icd10:F34.1','icd10:F43.21','icd10:F43.23','icd10:F06.31','icd10:F06.32') then to_char(T1.date, 'yyyy') end) depression
FROM emr_encounter T1,
stiprep_dx_codes T2,
stiprep_index_pats T3 
WHERE T1.id = T2.encounter_id
AND T1.patient_id = T3.patient_id
AND date < '01-01-2018'
GROUP BY T1.patient_id;

set enable_nestloop=on;

-- -- HIV Encounters - # of HIV encounters per year
-- CREATE TABLE stiprep_hiv_enc_counts AS 
-- SELECT T1.patient_id,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2006' THEN 1 END) t_hiv_encounters_06,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2007' THEN 1 END) t_hiv_encounters_07,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2008' THEN 1 END) t_hiv_encounters_08,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2009' THEN 1 END) t_hiv_encounters_09,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2010' THEN 1 END) t_hiv_encounters_10,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2011' THEN 1 END) t_hiv_encounters_11,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2012' THEN 1 END) t_hiv_encounters_12,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2013' THEN 1 END) t_hiv_encounters_13,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2014' THEN 1 END) t_hiv_encounters_14,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2015' THEN 1 END) t_hiv_encounters_15,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2016' THEN 1 END) t_hiv_encounters_16,
-- count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND rpt_year = '2017' THEN 1 END) t_hiv_encounters_17  
-- FROM stiprep_all_encs T1,
-- stiprep_dx_codes T2
-- WHERE T1.enc_id = T2.encounter_id
-- GROUP BY T1.patient_id;

-- HIV Encounters - # of HIV encounters per year
CREATE TABLE stiprep_hiv_enc_counts AS 
SELECT T1.patient_id,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2006' THEN 1 END) t_hiv_encounters_06,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2007' THEN 1 END) t_hiv_encounters_07,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2008' THEN 1 END) t_hiv_encounters_08,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2009' THEN 1 END) t_hiv_encounters_09,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2010' THEN 1 END) t_hiv_encounters_10,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2011' THEN 1 END) t_hiv_encounters_11,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2012' THEN 1 END) t_hiv_encounters_12,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2013' THEN 1 END) t_hiv_encounters_13,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2014' THEN 1 END) t_hiv_encounters_14,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2015' THEN 1 END) t_hiv_encounters_15,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2016' THEN 1 END) t_hiv_encounters_16,
count(CASE WHEN T2.dx_code_id~ '^(icd9:042|icd9:V08|icd10:B20|icd10:B21|icd10:B21|icd10:B22|icd10:B23|icd10:B24|icd10:B97.35|icd10:Z21)' AND extract(year from date) = '2017' THEN 1 END) t_hiv_encounters_17  
FROM emr_encounter T1,
stiprep_dx_codes T2,
stiprep_index_pats T3 
WHERE T1.id = T2.encounter_id
AND T1.patient_id = T3.patient_id
AND date < '01-01-2018'
GROUP BY T1.patient_id;


-- GATHER UP PRESCRIPTIONS AND DATES
CREATE TABLE stiprep_rx_of_interest AS 
SELECT T1.patient_id,T1.name, EXTRACT(YEAR FROM date)  rpt_year,
max(CASE WHEN T1.name in (
	'BICILLIN C-R 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)' , 
	'BICILLIN C-R 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R DISP SYRIN 1.2MMU/2ML IM (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R IM'
	'BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)' , 
	'BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A IM', 
	'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE&PROCAIN 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)') then 1 end)   rx_bicillin,
max(CASE WHEN T1.name in (
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'AZITHROMYCIN CHLAMYDIA RX (250 MG)', 
	'AZITHROMYCIN CHLAMYDIA RX (500 MG)', 
	'AZITHROMYCIN SR 2 GRAM/60 ML ORAL SUSP', 
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	'ZMAX 2 GRAM/60 ML ORAL SUSP (AZITHROMYCIN)' ) then 1 end)   rx_azithromycin,
max(CASE WHEN T1.name in ( 
	'CEFTRIAXONE IM INJECTION <=250 MG',  
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	'CEFTRIAXONE-LIDOCAINE 500 MG IM KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)', 
	'ROCEPHIN IM CONVENIENCE 500 MG-1 % KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)' ) then 1 end)   rx_ceftriaxone,
max(CASE WHEN 
	T1.name ilike '%methadone%' 
	or T1.name ilike '%dolophine%' 
	or T1.name ilike '%methadose%' then 1 end) rx_methadone,
max(CASE WHEN 
	T1.name ilike '%suboxone%' 
	or T1.name ilike 'buprenorphine%naloxone%' 
	or T1.name ilike '%zubsolv%' then 1 end) rx_suboxone,
max(CASE WHEN 
	T1.name ilike '%tadalafil%' 
	or T1.name ilike '%cilais%' 
	or T1.name ilike '%vardenafil%' 
	or T1.name ilike '%staxyn%'
	or T1.name ilike '%levitra%' 
	or T1.name ilike '%sildenafil%' 
	or T1.name ilike '%viagra%' then 1 end) rx_viagara_cilais_or_levitra 
FROM  emr_prescription T1,
stiprep_index_pats T2  
WHERE (name in (
	'BICILLIN C-R 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)' , 
	'BICILLIN C-R 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R DISP SYRIN 1.2MMU/2ML IM (PEN G BENZ/PEN G PROCAINE)', 
	'BICILLIN C-R IM'
	'BICILLIN L-A 1,200,000 UNIT/2 ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A 2,400,000 UNIT/4 ML IM SYRINGE (PENICILLIN G BENZATHINE)' , 
	'BICILLIN L-A 600,000 UNIT/ML IM SYRINGE (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 2.4MMU/4ML IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 600000 U IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A DISP SYRIN 600MU/1ML IM (PENICILLIN G BENZATHINE)', 
	'BICILLIN L-A IM', 'PENICILLIN G BENZATHINE 1,200,000 UNIT/2 ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE 2,400,000 UNIT/4 ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE 600,000 UNIT/ML IM SYRINGE', 
	'PENICILLIN G BENZATHINE&PROCAIN 1,200,000 UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 2,400,000 UNIT/4 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 600,000 UNIT/ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'PENICILLIN G BENZATHINE&PROCAIN 900,000 UNIT-300K UNIT/2 ML IM SYRINGE (PEN G BENZ/PEN G PROCAINE)', 
	'AZITHROMYCIN 1 GRAM ORAL PACKET', 
	'AZITHROMYCIN CHLAMYDIA RX (1 G POWDER PACKET)', 
	'AZITHROMYCIN CHLAMYDIA RX (250 MG)', 
	'AZITHROMYCIN CHLAMYDIA RX (500 MG)', 
	'AZITHROMYCIN SR 2 GRAM/60 ML ORAL SUSP',
	'ZITHROMAX 1 GRAM ORAL PACKET (AZITHROMYCIN)', 
	'ZMAX 2 GRAM/60 ML ORAL SUSP (AZITHROMYCIN)', 
	'CEFTRIAXONE IM INJECTION <=250 MG',  
	'ROCEPHIN 250 MG SOLUTION FOR INJECTION (CEFTRIAXONE SODIUM)', 
	'CEFTRIAXONE 250 MG SOLUTION FOR INJECTION', 
	'CEFTRIAXONE-LIDOCAINE 500 MG IM KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)', 
	'ROCEPHIN IM CONVENIENCE 500 MG-1 % KIT (CEFTRIAXONE SOD/LIDOCAINE HCL)'
	) 
	or name ilike '%methadone%' 
	or name ilike '%dolophine%' 
	or name ilike '%methadose%'
	or name ilike '%suboxone%' 
	or name ilike 'buprenorphine%naloxone%' 
	or name ilike '%zubsolv%'
	or name ilike '%tadalafil%' 
	or name ilike '%cilais%' 
	or name ilike '%vardenafil%' 
	or name ilike '%staxyn%'
	or name ilike '%levitra%' 
	or name ilike '%sildenafil%' 
	or name ilike '%viagra%' )
AND date >= '01-01-2006' 
AND date < '01-01-2018'
AND T1.patient_id = T2.patient_id
GROUP BY T1.patient_id, T1.name, EXTRACT(YEAR FROM date);

-- PRESCRIPTIONS - CREATE FIELDS
CREATE TABLE stiprep_rx_counts AS 
SELECT T1.patient_id,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2006 then 1 else 0 end) rx_bicillin_06,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2007 then 1 else 0 end) rx_bicillin_07,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2008 then 1 else 0 end) rx_bicillin_08,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2009 then 1 else 0 end) rx_bicillin_09,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2010 then 1 else 0 end) rx_bicillin_10,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2011 then 1 else 0 end) rx_bicillin_11,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2012 then 1 else 0 end) rx_bicillin_12,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2013 then 1 else 0 end) rx_bicillin_13,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2014 then 1 else 0 end) rx_bicillin_14,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2015 then 1 else 0 end) rx_bicillin_15,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2016 then 1 else 0 end) rx_bicillin_16,
max(CASE WHEN  rx_bicillin = 1 and rpt_year = 2017 then 1 else 0 end) rx_bicillin_17,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2006 then 1 else 0 end)  rx_azithromycin_06,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2007 then 1 else 0 end)  rx_azithromycin_07,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2008 then 1 else 0 end)  rx_azithromycin_08,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2009 then 1 else 0 end)  rx_azithromycin_09,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2010 then 1 else 0 end)  rx_azithromycin_10,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2011 then 1 else 0 end)  rx_azithromycin_11,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2012 then 1 else 0 end)  rx_azithromycin_12,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2013 then 1 else 0 end)  rx_azithromycin_13,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2014 then 1 else 0 end)  rx_azithromycin_14,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2015 then 1 else 0 end)  rx_azithromycin_15,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2016 then 1 else 0 end)  rx_azithromycin_16,
max(CASE WHEN  rx_azithromycin  = 1 and rpt_year = 2017 then 1 else 0 end)  rx_azithromycin_17,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2006 then 1 else 0 end)   rx_ceftriaxone_06,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2007 then 1 else 0 end)   rx_ceftriaxone_07,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2008 then 1 else 0 end)   rx_ceftriaxone_08,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2009 then 1 else 0 end)   rx_ceftriaxone_09,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2010 then 1 else 0 end)   rx_ceftriaxone_10,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2011 then 1 else 0 end)   rx_ceftriaxone_11,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2012 then 1 else 0 end)   rx_ceftriaxone_12,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2013 then 1 else 0 end)   rx_ceftriaxone_13,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2014 then 1 else 0 end)   rx_ceftriaxone_14,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2015 then 1 else 0 end)   rx_ceftriaxone_15,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2016 then 1 else 0 end)   rx_ceftriaxone_16,
max(CASE WHEN  rx_ceftriaxone  = 1 and rpt_year = 2017 then 1 else 0 end)   rx_ceftriaxone_17,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2006 then 1 else 0 end)   rx_methadone_06,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2007 then 1 else 0 end)   rx_methadone_07,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2008 then 1 else 0 end)   rx_methadone_08,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2009 then 1 else 0 end)   rx_methadone_09,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2010 then 1 else 0 end)   rx_methadone_10,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2011 then 1 else 0 end)   rx_methadone_11,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2012 then 1 else 0 end)   rx_methadone_12,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2013 then 1 else 0 end)   rx_methadone_13,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2014 then 1 else 0 end)   rx_methadone_14,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2015 then 1 else 0 end)   rx_methadone_15,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2016 then 1 else 0 end)   rx_methadone_16,
max(CASE WHEN  rx_methadone  = 1 and rpt_year = 2017 then 1 else 0 end)   rx_methadone_17,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2006 then 1 else 0 end)   rx_suboxone_06,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2007 then 1 else 0 end)   rx_suboxone_07,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2008 then 1 else 0 end)   rx_suboxone_08,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2009 then 1 else 0 end)   rx_suboxone_09,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2010 then 1 else 0 end)   rx_suboxone_10,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2011 then 1 else 0 end)   rx_suboxone_11,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2012 then 1 else 0 end)   rx_suboxone_12,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2013 then 1 else 0 end)   rx_suboxone_13,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2014 then 1 else 0 end)   rx_suboxone_14,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2015 then 1 else 0 end)   rx_suboxone_15,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2016 then 1 else 0 end)   rx_suboxone_16,
max(CASE WHEN  rx_suboxone   = 1 and rpt_year = 2017 then 1 else 0 end)   rx_suboxone_17,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2006 then 1 else 0 end)    rx_viagara_cilais_or_levitra_06,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2007 then 1 else 0 end)   rx_viagara_cilais_or_levitra_07,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2008 then 1 else 0 end)   rx_viagara_cilais_or_levitra_08,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2009 then 1 else 0 end)   rx_viagara_cilais_or_levitra_09,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2010 then 1 else 0 end)   rx_viagara_cilais_or_levitra_10,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2011 then 1 else 0 end)   rx_viagara_cilais_or_levitra_11,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2012 then 1 else 0 end)   rx_viagara_cilais_or_levitra_12,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2013 then 1 else 0 end)   rx_viagara_cilais_or_levitra_13,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2014 then 1 else 0 end)   rx_viagara_cilais_or_levitra_14,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2015 then 1 else 0 end)   rx_viagara_cilais_or_levitra_15,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2016 then 1 else 0 end)   rx_viagara_cilais_or_levitra_16,
max(CASE WHEN  rx_viagara_cilais_or_levitra   = 1 and rpt_year = 2017 then 1 else 0 end)   rx_viagara_cilais_or_levitra_17    
FROM  stiprep_rx_of_interest T1   
GROUP BY T1.patient_id;

-- FULL OUTER JOIN ALL OF THE DATA TOGETHER
CREATE TABLE stiprep_output_part_1 AS SELECT 
idx.patient_id as master_patient_id,
age_2006,
age_2007,
age_2008,
age_2009,
age_2010,
age_2011,
age_2012,
age_2013,
age_2014,
age_2015,
age_2016,
age_2017,
coalesce(t_encounters_06, 0) t_amb_encounters_06,
coalesce(t_encounters_07, 0) t_amb_encounters_07,
coalesce(t_encounters_08, 0) t_amb_encounters_08,
coalesce(t_encounters_09, 0) t_amb_encounters_09,
coalesce(t_encounters_10, 0) t_amb_encounters_10,
coalesce(t_encounters_11, 0) t_amb_encounters_11,
coalesce(t_encounters_12, 0) t_amb_encounters_12,
coalesce(t_encounters_13, 0) t_amb_encounters_13,
coalesce(t_encounters_14, 0) t_amb_encounters_14,
coalesce(t_encounters_15, 0) t_amb_encounters_15,
coalesce(t_encounters_16, 0) t_amb_encounters_16,
coalesce(t_encounters_17, 0) t_amb_encounters_17,
coalesce(t_gon_tests_06, 0) as total_gonorrhea_tests_06,
coalesce(t_gon_tests_07, 0) as total_gonorrhea_tests_07,
coalesce(t_gon_tests_08, 0) as total_gonorrhea_tests_08,
coalesce(t_gon_tests_09, 0) as total_gonorrhea_tests_09,
coalesce(t_gon_tests_10, 0) as total_gonorrhea_tests_10,
coalesce(t_gon_tests_11, 0) as total_gonorrhea_tests_11,
coalesce(t_gon_tests_12, 0) as total_gonorrhea_tests_12,
coalesce(t_gon_tests_13, 0) as total_gonorrhea_tests_13,
coalesce(t_gon_tests_14, 0) as total_gonorrhea_tests_14,
coalesce(t_gon_tests_15, 0) as total_gonorrhea_tests_15,
coalesce(t_gon_tests_16, 0) as total_gonorrhea_tests_16,
coalesce(t_gon_tests_17, 0) as total_gonorrhea_tests_17,
coalesce(p_gon_tests_06, 0) as positive_gonorrhea_tests_06,
coalesce(p_gon_tests_07, 0) as positive_gonorrhea_tests_07,
coalesce(p_gon_tests_08, 0) as positive_gonorrhea_tests_08,
coalesce(p_gon_tests_09, 0) as positive_gonorrhea_tests_09,
coalesce(p_gon_tests_10, 0) as positive_gonorrhea_tests_10,
coalesce(p_gon_tests_11, 0) as positive_gonorrhea_tests_11,
coalesce(p_gon_tests_12, 0) as positive_gonorrhea_tests_12,
coalesce(p_gon_tests_13, 0) as positive_gonorrhea_tests_13,
coalesce(p_gon_tests_14, 0) as positive_gonorrhea_tests_14,
coalesce(p_gon_tests_15, 0) as positive_gonorrhea_tests_15,
coalesce(p_gon_tests_16, 0) as positive_gonorrhea_tests_16,
coalesce(p_gon_tests_17, 0) as positive_gonorrhea_tests_17,
coalesce(t_gon_tests_throat_06, 0) as total_gonorrhea_tests_throat_06,
coalesce(t_gon_tests_throat_07, 0) as total_gonorrhea_tests_throat_07,
coalesce(t_gon_tests_throat_08, 0) as total_gonorrhea_tests_throat_08,
coalesce(t_gon_tests_throat_09, 0) as total_gonorrhea_tests_throat_09,
coalesce(t_gon_tests_throat_10, 0) as total_gonorrhea_tests_throat_10,
coalesce(t_gon_tests_throat_11, 0) as total_gonorrhea_tests_throat_11,
coalesce(t_gon_tests_throat_12, 0) as total_gonorrhea_tests_throat_12,
coalesce(t_gon_tests_throat_13, 0) as total_gonorrhea_tests_throat_13,
coalesce(t_gon_tests_throat_14, 0) as total_gonorrhea_tests_throat_14,
coalesce(t_gon_tests_throat_15, 0) as total_gonorrhea_tests_throat_15,
coalesce(t_gon_tests_throat_16, 0) as total_gonorrhea_tests_throat_16,
coalesce(t_gon_tests_throat_17, 0) as total_gonorrhea_tests_throat_17,
coalesce(p_gon_tests_throat_06, 0) as positive_gonorrhea_tests_throat_06,
coalesce(p_gon_tests_throat_07, 0) as positive_gonorrhea_tests_throat_07,
coalesce(p_gon_tests_throat_08, 0) as positive_gonorrhea_tests_throat_08,
coalesce(p_gon_tests_throat_09, 0) as positive_gonorrhea_tests_throat_09,
coalesce(p_gon_tests_throat_10, 0) as positive_gonorrhea_tests_throat_10,
coalesce(p_gon_tests_throat_11, 0) as positive_gonorrhea_tests_throat_11,
coalesce(p_gon_tests_throat_12, 0) as positive_gonorrhea_tests_throat_12,
coalesce(p_gon_tests_throat_13, 0) as positive_gonorrhea_tests_throat_13,
coalesce(p_gon_tests_throat_14, 0) as positive_gonorrhea_tests_throat_14,
coalesce(p_gon_tests_throat_15, 0) as positive_gonorrhea_tests_throat_15,
coalesce(p_gon_tests_throat_16, 0) as positive_gonorrhea_tests_throat_16,
coalesce(p_gon_tests_throat_17, 0) as positive_gonorrhea_tests_throat_17,
coalesce(t_gon_tests_rectal_06, 0) as total_gonorrhea_tests_rectal_06,
coalesce(t_gon_tests_rectal_07, 0) as total_gonorrhea_tests_rectal_07,
coalesce(t_gon_tests_rectal_08, 0) as total_gonorrhea_tests_rectal_08,
coalesce(t_gon_tests_rectal_09, 0) as total_gonorrhea_tests_rectal_09,
coalesce(t_gon_tests_rectal_10, 0) as total_gonorrhea_tests_rectal_10,
coalesce(t_gon_tests_rectal_11, 0) as total_gonorrhea_tests_rectal_11,
coalesce(t_gon_tests_rectal_12, 0) as total_gonorrhea_tests_rectal_12,
coalesce(t_gon_tests_rectal_13, 0) as total_gonorrhea_tests_rectal_13,
coalesce(t_gon_tests_rectal_14, 0) as total_gonorrhea_tests_rectal_14,
coalesce(t_gon_tests_rectal_15, 0) as total_gonorrhea_tests_rectal_15,
coalesce(t_gon_tests_rectal_16, 0) as total_gonorrhea_tests_rectal_16,
coalesce(t_gon_tests_rectal_17, 0) as total_gonorrhea_tests_rectal_17,
coalesce(p_gon_tests_rectal_06, 0) as positive_gonorrhea_tests_rectal_06,
coalesce(p_gon_tests_rectal_07, 0) as positive_gonorrhea_tests_rectal_07,
coalesce(p_gon_tests_rectal_08, 0) as positive_gonorrhea_tests_rectal_08,
coalesce(p_gon_tests_rectal_09, 0) as positive_gonorrhea_tests_rectal_09,
coalesce(p_gon_tests_rectal_10, 0) as positive_gonorrhea_tests_rectal_10,
coalesce(p_gon_tests_rectal_11, 0) as positive_gonorrhea_tests_rectal_11,
coalesce(p_gon_tests_rectal_12, 0) as positive_gonorrhea_tests_rectal_12,
coalesce(p_gon_tests_rectal_13, 0) as positive_gonorrhea_tests_rectal_13,
coalesce(p_gon_tests_rectal_14, 0) as positive_gonorrhea_tests_rectal_14,
coalesce(p_gon_tests_rectal_15, 0) as positive_gonorrhea_tests_rectal_15,
coalesce(p_gon_tests_rectal_16, 0) as positive_gonorrhea_tests_rectal_16,
coalesce(p_gon_tests_rectal_17, 0) as positive_gonorrhea_tests_rectal_17,
coalesce(t_gon_tests_urog_06, 0) as total_gonorrhea_tests_urog_06,
coalesce(t_gon_tests_urog_07, 0) as total_gonorrhea_tests_urog_07,
coalesce(t_gon_tests_urog_08, 0) as total_gonorrhea_tests_urog_08,
coalesce(t_gon_tests_urog_09, 0) as total_gonorrhea_tests_urog_09,
coalesce(t_gon_tests_urog_10, 0) as total_gonorrhea_tests_urog_10,
coalesce(t_gon_tests_urog_11, 0) as total_gonorrhea_tests_urog_11,
coalesce(t_gon_tests_urog_12, 0) as total_gonorrhea_tests_urog_12,
coalesce(t_gon_tests_urog_13, 0) as total_gonorrhea_tests_urog_13,
coalesce(t_gon_tests_urog_14, 0) as total_gonorrhea_tests_urog_14,
coalesce(t_gon_tests_urog_15, 0) as total_gonorrhea_tests_urog_15,
coalesce(t_gon_tests_urog_16, 0) as total_gonorrhea_tests_urog_16,
coalesce(t_gon_tests_urog_17, 0) as total_gonorrhea_tests_urog_17,
coalesce(p_gon_tests_urog_06, 0) as positive_gonorrhea_tests_urog_06,
coalesce(p_gon_tests_urog_07, 0) as positive_gonorrhea_tests_urog_07,
coalesce(p_gon_tests_urog_08, 0) as positive_gonorrhea_tests_urog_08,
coalesce(p_gon_tests_urog_09, 0) as positive_gonorrhea_tests_urog_09,
coalesce(p_gon_tests_urog_10, 0) as positive_gonorrhea_tests_urog_10,
coalesce(p_gon_tests_urog_11, 0) as positive_gonorrhea_tests_urog_11,
coalesce(p_gon_tests_urog_12, 0) as positive_gonorrhea_tests_urog_12,
coalesce(p_gon_tests_urog_13, 0) as positive_gonorrhea_tests_urog_13,
coalesce(p_gon_tests_urog_14, 0) as positive_gonorrhea_tests_urog_14,
coalesce(p_gon_tests_urog_15, 0) as positive_gonorrhea_tests_urog_15,
coalesce(p_gon_tests_urog_16, 0) as positive_gonorrhea_tests_urog_16,
coalesce(p_gon_tests_urog_17, 0) as positive_gonorrhea_tests_urog_17,
coalesce(gon_06, 0) as gonorrhea_cases_06,
coalesce(gon_07, 0) as gonorrhea_cases_07,
coalesce(gon_08, 0) as gonorrhea_cases_08,
coalesce(gon_09, 0) as gonorrhea_cases_09,
coalesce(gon_10, 0) as gonorrhea_cases_10,
coalesce(gon_11, 0) as gonorrhea_cases_11,
coalesce(gon_12, 0) as gonorrhea_cases_12,
coalesce(gon_13, 0) as gonorrhea_cases_13,
coalesce(gon_14, 0) as gonorrhea_cases_14,
coalesce(gon_15, 0) as gonorrhea_cases_15,
coalesce(gon_16, 0) as gonorrhea_cases_16,
coalesce(gon_17, 0) as gonorrhea_cases_17,
coalesce(t_chlam_06, 0) as total_chlamydia_tests_06,
coalesce(t_chlam_07, 0) as total_chlamydia_tests_07,
coalesce(t_chlam_08, 0) as total_chlamydia_tests_08,
coalesce(t_chlam_09, 0) as total_chlamydia_tests_09,
coalesce(t_chlam_10, 0) as total_chlamydia_tests_10,
coalesce(t_chlam_11, 0) as total_chlamydia_tests_11,
coalesce(t_chlam_12, 0) as total_chlamydia_tests_12,
coalesce(t_chlam_13, 0) as total_chlamydia_tests_13,
coalesce(t_chlam_14, 0) as total_chlamydia_tests_14,
coalesce(t_chlam_15, 0) as total_chlamydia_tests_15,
coalesce(t_chlam_16, 0) as total_chlamydia_tests_16,
coalesce(t_chlam_17, 0) as total_chlamydia_tests_17,
coalesce(p_chlam_06, 0) as positive_chlamydia_tests_06,
coalesce(p_chlam_07, 0) as positive_chlamydia_tests_07,
coalesce(p_chlam_08, 0) as positive_chlamydia_tests_08,
coalesce(p_chlam_09, 0) as positive_chlamydia_tests_09,
coalesce(p_chlam_10, 0) as positive_chlamydia_tests_10,
coalesce(p_chlam_11, 0) as positive_chlamydia_tests_11,
coalesce(p_chlam_12, 0) as positive_chlamydia_tests_12,
coalesce(p_chlam_13, 0) as positive_chlamydia_tests_13,
coalesce(p_chlam_14, 0) as positive_chlamydia_tests_14,
coalesce(p_chlam_15, 0) as positive_chlamydia_tests_15,
coalesce(p_chlam_16, 0) as positive_chlamydia_tests_16,
coalesce(p_chlam_17, 0) as positive_chlamydia_tests_17,
coalesce(t_chlam_throat_06, 0) as total_chlamydia_tests_throat_06,
coalesce(t_chlam_throat_07, 0) as total_chlamydia_tests_throat_07,
coalesce(t_chlam_throat_08, 0) as total_chlamydia_tests_throat_08,
coalesce(t_chlam_throat_09, 0) as total_chlamydia_tests_throat_09,
coalesce(t_chlam_throat_10, 0) as total_chlamydia_tests_throat_10,
coalesce(t_chlam_throat_11, 0) as total_chlamydia_tests_throat_11,
coalesce(t_chlam_throat_12, 0) as total_chlamydia_tests_throat_12,
coalesce(t_chlam_throat_13, 0) as total_chlamydia_tests_throat_13,
coalesce(t_chlam_throat_14, 0) as total_chlamydia_tests_throat_14,
coalesce(t_chlam_throat_15, 0) as total_chlamydia_tests_throat_15,
coalesce(t_chlam_throat_16, 0) as total_chlamydia_tests_throat_16,
coalesce(t_chlam_throat_17, 0) as total_chlamydia_tests_throat_17,
coalesce(p_chlam_throat_06, 0) as positive_chlamydia_tests_throat_06,
coalesce(p_chlam_throat_07, 0) as positive_chlamydia_tests_throat_07,
coalesce(p_chlam_throat_08, 0) as positive_chlamydia_tests_throat_08,
coalesce(p_chlam_throat_09, 0) as positive_chlamydia_tests_throat_09,
coalesce(p_chlam_throat_10, 0) as positive_chlamydia_tests_throat_10,
coalesce(p_chlam_throat_11, 0) as positive_chlamydia_tests_throat_11,
coalesce(p_chlam_throat_12, 0) as positive_chlamydia_tests_throat_12,
coalesce(p_chlam_throat_13, 0) as positive_chlamydia_tests_throat_13,
coalesce(p_chlam_throat_14, 0) as positive_chlamydia_tests_throat_14,
coalesce(p_chlam_throat_15, 0) as positive_chlamydia_tests_throat_15,
coalesce(p_chlam_throat_16, 0) as positive_chlamydia_tests_throat_16,
coalesce(p_chlam_throat_17, 0) as positive_chlamydia_tests_throat_17,
coalesce(t_chlam_rectal_06, 0) as total_chlamydia_tests_rectal_06,
coalesce(t_chlam_rectal_07, 0) as total_chlamydia_tests_rectal_07,
coalesce(t_chlam_rectal_08, 0) as total_chlamydia_tests_rectal_08,
coalesce(t_chlam_rectal_09, 0) as total_chlamydia_tests_rectal_09,
coalesce(t_chlam_rectal_10, 0) as total_chlamydia_tests_rectal_10,
coalesce(t_chlam_rectal_11, 0) as total_chlamydia_tests_rectal_11,
coalesce(t_chlam_rectal_12, 0) as total_chlamydia_tests_rectal_12,
coalesce(t_chlam_rectal_13, 0) as total_chlamydia_tests_rectal_13,
coalesce(t_chlam_rectal_14, 0) as total_chlamydia_tests_rectal_14,
coalesce(t_chlam_rectal_15, 0) as total_chlamydia_tests_rectal_15,
coalesce(t_chlam_rectal_16, 0) as total_chlamydia_tests_rectal_16,
coalesce(t_chlam_rectal_17, 0) as total_chlamydia_tests_rectal_17,
coalesce(p_chlam_rectal_06, 0) as positive_chlamydia_tests_rectal_06,
coalesce(p_chlam_rectal_07, 0) as positive_chlamydia_tests_rectal_07,
coalesce(p_chlam_rectal_08, 0) as positive_chlamydia_tests_rectal_08,
coalesce(p_chlam_rectal_09, 0) as positive_chlamydia_tests_rectal_09,
coalesce(p_chlam_rectal_10, 0) as positive_chlamydia_tests_rectal_10,
coalesce(p_chlam_rectal_11, 0) as positive_chlamydia_tests_rectal_11,
coalesce(p_chlam_rectal_12, 0) as positive_chlamydia_tests_rectal_12,
coalesce(p_chlam_rectal_13, 0) as positive_chlamydia_tests_rectal_13,
coalesce(p_chlam_rectal_14, 0) as positive_chlamydia_tests_rectal_14,
coalesce(p_chlam_rectal_15, 0) as positive_chlamydia_tests_rectal_15,
coalesce(p_chlam_rectal_16, 0) as positive_chlamydia_tests_rectal_16,
coalesce(p_chlam_rectal_17, 0) as positive_chlamydia_tests_rectal_17,
coalesce(t_chlam_urog_06, 0) as total_chlamydia_tests_urog_06,
coalesce(t_chlam_urog_07, 0) as total_chlamydia_tests_urog_07,
coalesce(t_chlam_urog_08, 0) as total_chlamydia_tests_urog_08,
coalesce(t_chlam_urog_09, 0) as total_chlamydia_tests_urog_09,
coalesce(t_chlam_urog_10, 0) as total_chlamydia_tests_urog_10,
coalesce(t_chlam_urog_11, 0) as total_chlamydia_tests_urog_11,
coalesce(t_chlam_urog_12, 0) as total_chlamydia_tests_urog_12,
coalesce(t_chlam_urog_13, 0) as total_chlamydia_tests_urog_13,
coalesce(t_chlam_urog_14, 0) as total_chlamydia_tests_urog_14,
coalesce(t_chlam_urog_15, 0) as total_chlamydia_tests_urog_15,
coalesce(t_chlam_urog_16, 0) as total_chlamydia_tests_urog_16,
coalesce(t_chlam_urog_17, 0) as total_chlamydia_tests_urog_17,
coalesce(p_chlam_urog_06, 0) as positive_chlamydia_tests_urog_06,
coalesce(p_chlam_urog_07, 0) as positive_chlamydia_tests_urog_07,
coalesce(p_chlam_urog_08, 0) as positive_chlamydia_tests_urog_08,
coalesce(p_chlam_urog_09, 0) as positive_chlamydia_tests_urog_09,
coalesce(p_chlam_urog_10, 0) as positive_chlamydia_tests_urog_10,
coalesce(p_chlam_urog_11, 0) as positive_chlamydia_tests_urog_11,
coalesce(p_chlam_urog_12, 0) as positive_chlamydia_tests_urog_12,
coalesce(p_chlam_urog_13, 0) as positive_chlamydia_tests_urog_13,
coalesce(p_chlam_urog_14, 0) as positive_chlamydia_tests_urog_14,
coalesce(p_chlam_urog_15, 0) as positive_chlamydia_tests_urog_15,
coalesce(p_chlam_urog_16, 0) as positive_chlamydia_tests_urog_16,
coalesce(p_chlam_urog_17, 0) as positive_chlamydia_tests_urog_17,
coalesce(chlam_06, 0) as chlamydia_cases_06,
coalesce(chlam_07, 0) as chlamydia_cases_07,
coalesce(chlam_08, 0) as chlamydia_cases_08,
coalesce(chlam_09, 0) as chlamydia_cases_09,
coalesce(chlam_10, 0) as chlamydia_cases_10,
coalesce(chlam_11, 0) as chlamydia_cases_11,
coalesce(chlam_12, 0) as chlamydia_cases_12,
coalesce(chlam_13, 0) as chlamydia_cases_13,
coalesce(chlam_14, 0) as chlamydia_cases_14,
coalesce(chlam_15, 0) as chlamydia_cases_15,
coalesce(chlam_16, 0) as chlamydia_cases_16,
coalesce(chlam_17, 0) as chlamydia_cases_17,
coalesce(t_syph_06, 0) as total_syphilis_tests_06,
coalesce(t_syph_07, 0) as total_syphilis_tests_07,
coalesce(t_syph_08, 0) as total_syphilis_tests_08,
coalesce(t_syph_09, 0) as total_syphilis_tests_09,
coalesce(t_syph_10, 0) as total_syphilis_tests_10,
coalesce(t_syph_11, 0) as total_syphilis_tests_11,
coalesce(t_syph_12, 0) as total_syphilis_tests_12,
coalesce(t_syph_13, 0) as total_syphilis_tests_13,
coalesce(t_syph_14, 0) as total_syphilis_tests_14,
coalesce(t_syph_15, 0) as total_syphilis_tests_15,
coalesce(t_syph_16, 0) as total_syphilis_tests_16,
coalesce(t_syph_17, 0) as total_syphilis_tests_17,
coalesce(syphilis_06, 0) as syphilis_cases_06,
coalesce(syphilis_07, 0) as syphilis_cases_07,
coalesce(syphilis_08, 0) as syphilis_cases_08,
coalesce(syphilis_09, 0) as syphilis_cases_09,
coalesce(syphilis_10, 0) as syphilis_cases_10,
coalesce(syphilis_11, 0) as syphilis_cases_11,
coalesce(syphilis_12, 0) as syphilis_cases_12,
coalesce(syphilis_13, 0) as syphilis_cases_13,
coalesce(syphilis_14, 0) as syphilis_cases_14,
coalesce(syphilis_15, 0) as syphilis_cases_15,
coalesce(syphilis_16, 0) as syphilis_cases_16,
coalesce(syphilis_17, 0) as syphilis_cases_17,
coalesce(t_sti_enc_py_06, 0) as t_sti_enc_py_cases_06,
coalesce(t_sti_enc_py_07, 0) as t_sti_enc_py_cases_07,
coalesce(t_sti_enc_py_08, 0) as t_sti_enc_py_cases_08,
coalesce(t_sti_enc_py_09, 0) as t_sti_enc_py_cases_09,
coalesce(t_sti_enc_py_10, 0) as t_sti_enc_py_cases_10,
coalesce(t_sti_enc_py_11, 0) as t_sti_enc_py_cases_11,
coalesce(t_sti_enc_py_12, 0) as t_sti_enc_py_cases_12,
coalesce(t_sti_enc_py_13, 0) as t_sti_enc_py_cases_13,
coalesce(t_sti_enc_py_14, 0) as t_sti_enc_py_cases_14,
coalesce(t_sti_enc_py_15, 0) as t_sti_enc_py_cases_15,
coalesce(t_sti_enc_py_16, 0) as t_sti_enc_py_cases_16,
coalesce(t_sti_enc_py_17, 0) as t_sti_enc_py_cases_17,
coalesce(indexsti_2006, null) as indexsti_2006,
coalesce(indexsti_month_06, null) as indexsti_month_06,
coalesce(indexsti_2007, null) as indexsti_2007,
coalesce(indexsti_month_07, null) as indexsti_month_07,
coalesce(indexsti_2008, null) as indexsti_2008,
coalesce(indexsti_month_08, null) as indexsti_month_08,
coalesce(indexsti_2009, null) as indexsti_2009,
coalesce(indexsti_month_09, null) as indexsti_month_09,
coalesce(indexsti_2010, null) as indexsti_2010,
coalesce(indexsti_month_10, null) as indexsti_month_10,
coalesce(indexsti_2011, null) as indexsti_2011,
coalesce(indexsti_month_11, null) as indexsti_month_11,
coalesce(indexsti_2012, null) as indexsti_2012,
coalesce(indexsti_month_12, null) as indexsti_month_12,
coalesce(indexsti_2013, null) as indexsti_2013,
coalesce(indexsti_month_13, null) as indexsti_month_13,
coalesce(indexsti_2014, null) as indexsti_2014,
coalesce(indexsti_month_14, null) as indexsti_month_14,
coalesce(indexsti_2015, null) as indexsti_2015,
coalesce(indexsti_month_15, null) as indexsti_month_15,
coalesce(indexsti_2016, null) as indexsti_2016,
coalesce(indexsti_month_16, null) as indexsti_month_16,
coalesce(indexsti_2017, null) as indexsti_2017,
coalesce(indexsti_month_17, null) as indexsti_month_17,
coalesce(secsti_index2006, null) as secsti_index2006,
coalesce(secsti_1to2_days_index2006, null) as secsti_1to2_days_index2006,
coalesce(secsti_index2007, null) as secsti_index2007,
coalesce(secsti_1to2_days_index2007, null) as secsti_1to2_days_index2007,
coalesce(secsti_index2008, null) as secsti_index2008,
coalesce(secsti_1to2_days_index2008, null) as secsti_1to2_days_index2008,
coalesce(secsti_index2009, null) as secsti_index2009,
coalesce(secsti_1to2_days_index2009, null) as secsti_1to2_days_index2009,
coalesce(secsti_index2010, null) as secsti_index2010,
coalesce(secsti_1to2_days_index2010, null) as secsti_1to2_days_index2010,
coalesce(secsti_index2011, null) as secsti_index2011,
coalesce(secsti_1to2_days_index2011, null) as secsti_1to2_days_index2011,
coalesce(secsti_index2012, null) as secsti_index2012,
coalesce(secsti_1to2_days_index2012, null) as secsti_1to2_days_index2012,
coalesce(secsti_index2013, null) as secsti_index2013,
coalesce(secsti_1to2_days_index2013, null) as secsti_1to2_days_index2013,
coalesce(secsti_index2014, null) as secsti_index2014,
coalesce(secsti_1to2_days_index2014, null) as secsti_1to2_days_index2014,
coalesce(secsti_index2015, null) as secsti_index2015,
coalesce(secsti_1to2_days_index2015, null) as secsti_1to2_days_index2015,
coalesce(secsti_index2016, null) as secsti_index2016,
coalesce(secsti_1to2_days_index2016, null) as secsti_1to2_days_index2016,
coalesce(secsti_index2017, null) as secsti_index2017,
coalesce(secsti_1to2_days_index2017, null) as secsti_1to2_days_index2017,
coalesce(thirdsti_index2006, null) as thirdsti_index2006,
coalesce(thirdsti_2to3_days_index2006, null) as thirdsti_2to3_days_index2006,
coalesce(thirdsti_index2007, null) as thirdsti_index2007,
coalesce(thirdsti_2to3_days_index2007, null) as thirdsti_2to3_days_index2007,
coalesce(thirdsti_index2008, null) as thirdsti_index2008,
coalesce(thirdsti_2to3_days_index2008, null) as thirdsti_2to3_days_index2008,
coalesce(thirdsti_index2009, null) as thirdsti_index2009,
coalesce(thirdsti_2to3_days_index2009, null) as thirdsti_2to3_days_index2009,
coalesce(thirdsti_index2010, null) as thirdsti_index2010,
coalesce(thirdsti_2to3_days_index2010, null) as thirdsti_2to3_days_index2010,
coalesce(thirdsti_index2011, null) as thirdsti_index2011,
coalesce(thirdsti_2to3_days_index2011, null) as thirdsti_2to3_days_index2011,
coalesce(thirdsti_index2012, null) as thirdsti_index2012,
coalesce(thirdsti_2to3_days_index2012, null) as thirdsti_2to3_days_index2012,
coalesce(thirdsti_index2013, null) as thirdsti_index2013,
coalesce(thirdsti_2to3_days_index2013, null) as thirdsti_2to3_days_index2013,
coalesce(thirdsti_index2014, null) as thirdsti_index2014,
coalesce(thirdsti_2to3_days_index2014, null) as thirdsti_2to3_days_index2014,
coalesce(thirdsti_index2015, null) as thirdsti_index2015,
coalesce(thirdsti_2to3_days_index2015, null) as thirdsti_2to3_days_index2015,
coalesce(thirdsti_index2016, null) as thirdsti_index2016,
coalesce(thirdsti_2to3_days_index2016, null) as thirdsti_2to3_days_index2016,
coalesce(thirdsti_index2017, null) as thirdsti_index2017,
coalesce(thirdsti_2to3_days_index2017, null) as thirdsti_2to3_days_index2017,
coalesce(fourthsti_index2006, null) as fourthsti_index2006,
coalesce(fourthsti_3to4_days_index2006, null) as fourthsti_3to4_days_index2006,
coalesce(fourthsti_index2007, null) as fourthsti_index2007,
coalesce(fourthsti_3to4_days_index2007, null) as fourthsti_3to4_days_index2007,
coalesce(fourthsti_index2008, null) as fourthsti_index2008,
coalesce(fourthsti_3to4_days_index2008, null) as fourthsti_3to4_days_index2008,
coalesce(fourthsti_index2009, null) as fourthsti_index2009,
coalesce(fourthsti_3to4_days_index2009, null) as fourthsti_3to4_days_index2009,
coalesce(fourthsti_index2010, null) as fourthsti_index2010,
coalesce(fourthsti_3to4_days_index2010, null) as fourthsti_3to4_days_index2010,
coalesce(fourthsti_index2011, null) as fourthsti_index2011,
coalesce(fourthsti_3to4_days_index2011, null) as fourthsti_3to4_days_index2011,
coalesce(fourthsti_index2012, null) as fourthsti_index2012,
coalesce(fourthsti_3to4_days_index2012, null) as fourthsti_3to4_days_index2012,
coalesce(fourthsti_index2013, null) as fourthsti_index2013,
coalesce(fourthsti_3to4_days_index2013, null) as fourthsti_3to4_days_index2013,
coalesce(fourthsti_index2014, null) as fourthsti_index2014,
coalesce(fourthsti_3to4_days_index2014, null) as fourthsti_3to4_days_index2014,
coalesce(fourthsti_index2015, null) as fourthsti_index2015,
coalesce(fourthsti_3to4_days_index2015, null) as fourthsti_3to4_days_index2015,
coalesce(fourthsti_index2016, null) as fourthsti_index2016,
coalesce(fourthsti_3to4_days_index2016, null) as fourthsti_3to4_days_index2016,
coalesce(fourthsti_index2017, null) as fourthsti_index2017,
coalesce(fourthsti_3to4_days_index2017, null) as fourthsti_3to4_days_index2017,
coalesce(ser_test_for_lgv_ever, 0) as ser_testing_for_lgv_ever,
coalesce(anal_cytology_test_ever, 0) as anal_cytology_test_ever,
coalesce(t_hcv_anti_06, 0) as total_hcv_antibody_tests_06,
coalesce(t_hcv_anti_07, 0) as total_hcv_antibody_tests_07,
coalesce(t_hcv_anti_08, 0) as total_hcv_antibody_tests_08,
coalesce(t_hcv_anti_09, 0) as total_hcv_antibody_tests_09,
coalesce(t_hcv_anti_10, 0) as total_hcv_antibody_tests_10,
coalesce(t_hcv_anti_11, 0) as total_hcv_antibody_tests_11,
coalesce(t_hcv_anti_12, 0) as total_hcv_antibody_tests_12,
coalesce(t_hcv_anti_13, 0) as total_hcv_antibody_tests_13,
coalesce(t_hcv_anti_14, 0) as total_hcv_antibody_tests_14,
coalesce(t_hcv_anti_15, 0) as total_hcv_antibody_tests_15,
coalesce(t_hcv_anti_16, 0) as total_hcv_antibody_tests_16,
coalesce(t_hcv_anti_17, 0) as total_hcv_antibody_tests_17,
coalesce(t_hcv_rna_06, 0) as total_hcv_rna_tests_06,
coalesce(t_hcv_rna_07, 0) as total_hcv_rna_tests_07,
coalesce(t_hcv_rna_08, 0) as total_hcv_rna_tests_08,
coalesce(t_hcv_rna_09, 0) as total_hcv_rna_tests_09,
coalesce(t_hcv_rna_10, 0) as total_hcv_rna_tests_10,
coalesce(t_hcv_rna_11, 0) as total_hcv_rna_tests_11,
coalesce(t_hcv_rna_12, 0) as total_hcv_rna_tests_12,
coalesce(t_hcv_rna_13, 0) as total_hcv_rna_tests_13,
coalesce(t_hcv_rna_14, 0) as total_hcv_rna_tests_14,
coalesce(t_hcv_rna_15, 0) as total_hcv_rna_tests_15,
coalesce(t_hcv_rna_16, 0) as total_hcv_rna_tests_16,
coalesce(t_hcv_rna_17, 0) as total_hcv_rna_tests_17,
coalesce(hcv_anti_or_rna_pos, 0) as hcv_antibody_or_rna_positive,
coalesce(hcv_anti_pos_date, null) as hcv_antibody_first_pos_year,
coalesce(hcv_rna_pos_date, null) as hcv_rna_first_pos_year,
coalesce(acute_hepc_per_esp, 0) as acute_hepc_per_esp,
coalesce(acute_hepc_per_esp_date, null) as acute_hepc_per_esp_diagnosis_year,
coalesce(t_hepb_antigen_06, 0) as total_hepb_surface_antigen_tests_06,
coalesce(t_hepb_antigen_07, 0) as total_hepb_surface_antigen_tests_07,
coalesce(t_hepb_antigen_08, 0) as total_hepb_surface_antigen_tests_08,
coalesce(t_hepb_antigen_09, 0) as total_hepb_surface_antigen_tests_09,
coalesce(t_hepb_antigen_10, 0) as total_hepb_surface_antigen_tests_10,
coalesce(t_hepb_antigen_11, 0) as total_hepb_surface_antigen_tests_11,
coalesce(t_hepb_antigen_12, 0) as total_hepb_surface_antigen_tests_12,
coalesce(t_hepb_antigen_13, 0) as total_hepb_surface_antigen_tests_13,
coalesce(t_hepb_antigen_14, 0) as total_hepb_surface_antigen_tests_14,
coalesce(t_hepb_antigen_15, 0) as total_hepb_surface_antigen_tests_15,
coalesce(t_hepb_antigen_16, 0) as total_hepb_surface_antigen_tests_16,
coalesce(t_hepb_antigen_17, 0) as total_hepb_surface_antigen_tests_17,
coalesce(t_hepb_dna_06, 0) as total_hepb_dna_tests_06,
coalesce(t_hepb_dna_07, 0) as total_hepb_dna_tests_07,
coalesce(t_hepb_dna_08, 0) as total_hepb_dna_tests_08,
coalesce(t_hepb_dna_09, 0) as total_hepb_dna_tests_09,
coalesce(t_hepb_dna_10, 0) as total_hepb_dna_tests_10,
coalesce(t_hepb_dna_11, 0) as total_hepb_dna_tests_11,
coalesce(t_hepb_dna_12, 0) as total_hepb_dna_tests_12,
coalesce(t_hepb_dna_13, 0) as total_hepb_dna_tests_13,
coalesce(t_hepb_dna_14, 0) as total_hepb_dna_tests_14,
coalesce(t_hepb_dna_15, 0) as total_hepb_dna_tests_15,
coalesce(t_hepb_dna_16, 0) as total_hepb_dna_tests_16,
coalesce(t_hepb_dna_17, 0) as total_hepb_dna_tests_17,
coalesce(hepb_pos_antigen_or_dna, 0) as hepb_antigen_or_dna_positive,
coalesce(hepb_pos_antigen_or_dna_date, null) as hepb_antigen_or_dna_first_positive_year,
coalesce(acute_hepb_per_esp, 0) as acute_hepb_per_esp,
coalesce(acute_hepb_per_esp_date, null) as acute_hepb_per_esp_year,
coalesce(hepb_pos_antigen_or_dna, hist_of_hep_b, 0) as hist_of_hep_b_ever,
coalesce(t_hiv_elisa_06, 0) as t_hiv_elisa_06,
coalesce(t_hiv_elisa_07, 0) as t_hiv_elisa_07,
coalesce(t_hiv_elisa_08, 0) as t_hiv_elisa_08,
coalesce(t_hiv_elisa_09, 0) as t_hiv_elisa_09,
coalesce(t_hiv_elisa_10, 0) as t_hiv_elisa_10,
coalesce(t_hiv_elisa_11, 0) as t_hiv_elisa_11,
coalesce(t_hiv_elisa_12, 0) as t_hiv_elisa_12,
coalesce(t_hiv_elisa_13, 0) as t_hiv_elisa_13,
coalesce(t_hiv_elisa_14, 0) as t_hiv_elisa_14,
coalesce(t_hiv_elisa_15, 0) as t_hiv_elisa_15,
coalesce(t_hiv_elisa_16, 0) as t_hiv_elisa_16,
coalesce(t_hiv_elisa_17, 0) as t_hiv_elisa_17,
coalesce(t_hiv_wb_06, 0) as t_hiv_wb_06,
coalesce(t_hiv_wb_07, 0) as t_hiv_wb_07,
coalesce(t_hiv_wb_08, 0) as t_hiv_wb_08,
coalesce(t_hiv_wb_09, 0) as t_hiv_wb_09,
coalesce(t_hiv_wb_10, 0) as t_hiv_wb_10,
coalesce(t_hiv_wb_11, 0) as t_hiv_wb_11,
coalesce(t_hiv_wb_12, 0) as t_hiv_wb_12,
coalesce(t_hiv_wb_13, 0) as t_hiv_wb_13,
coalesce(t_hiv_wb_14, 0) as t_hiv_wb_14,
coalesce(t_hiv_wb_15, 0) as t_hiv_wb_15,
coalesce(t_hiv_wb_16, 0) as t_hiv_wb_16,
coalesce(t_hiv_wb_17, 0) as t_hiv_wb_17,
coalesce(t_hiv_rna_06, 0) as t_hiv_rna_06,
coalesce(t_hiv_rna_07, 0) as t_hiv_rna_07,
coalesce(t_hiv_rna_08, 0) as t_hiv_rna_08,
coalesce(t_hiv_rna_09, 0) as t_hiv_rna_09,
coalesce(t_hiv_rna_10, 0) as t_hiv_rna_10,
coalesce(t_hiv_rna_11, 0) as t_hiv_rna_11,
coalesce(t_hiv_rna_12, 0) as t_hiv_rna_12,
coalesce(t_hiv_rna_13, 0) as t_hiv_rna_13,
coalesce(t_hiv_rna_14, 0) as t_hiv_rna_14,
coalesce(t_hiv_rna_15, 0) as t_hiv_rna_15,
coalesce(t_hiv_rna_16, 0) as t_hiv_rna_16,
coalesce(t_hiv_rna_17, 0) as t_hiv_rna_17,
coalesce(max_viral_load_year, null) as hiv_max_viral_load_year,
coalesce(t_hiv_rna_06, 0) as t_hiv_agab_06,
coalesce(t_hiv_rna_07, 0) as t_hiv_agab_07,
coalesce(t_hiv_rna_08, 0) as t_hiv_agab_08,
coalesce(t_hiv_rna_09, 0) as t_hiv_agab_09,
coalesce(t_hiv_rna_10, 0) as t_hiv_agab_10,
coalesce(t_hiv_rna_11, 0) as t_hiv_agab_11,
coalesce(t_hiv_rna_12, 0) as t_hiv_agab_12,
coalesce(t_hiv_rna_13, 0) as t_hiv_agab_13,
coalesce(t_hiv_rna_14, 0) as t_hiv_agab_14,
coalesce(t_hiv_rna_15, 0) as t_hiv_agab_15,
coalesce(t_hiv_rna_16, 0) as t_hiv_agab_16,
coalesce(t_hiv_rna_17, 0) as t_hiv_agab_17,
coalesce(hiv_per_esp_final, 2) as hiv_per_esp_spec,
coalesce(hiv_per_esp_date, null) as hiv_per_esp_first_year,
coalesce(new_hiv_diagnosis, null) as new_hiv_diagnosis,
coalesce(hiv_neg_with_rna_test, 0) as hiv_neg_with_rna_test,
coalesce(hiv_neg_first_rna_test_yr, null) as hiv_neg_first_rna_test_yr,
coalesce(hiv_neg_with_meds, 0) as hiv_neg_with_meds,
coalesce(hiv_neg_first_hiv_med_yr, null) as hiv_neg_first_hiv_med_yr,
coalesce(hiv_on_problem_list, 0) as hiv_on_problem_list,
coalesce(hiv_first_prob_date, null) as hiv_first_prob_date,
coalesce(t_hiv_encounters_06, 0) t_hiv_encounters_06,
coalesce(t_hiv_encounters_07, 0) t_hiv_encounters_07,
coalesce(t_hiv_encounters_08, 0) t_hiv_encounters_08,
coalesce(t_hiv_encounters_09, 0) t_hiv_encounters_09,
coalesce(t_hiv_encounters_10, 0) t_hiv_encounters_10,
coalesce(t_hiv_encounters_11, 0) t_hiv_encounters_11,
coalesce(t_hiv_encounters_12, 0) t_hiv_encounters_12,
coalesce(t_hiv_encounters_13, 0) t_hiv_encounters_13,
coalesce(t_hiv_encounters_14, 0) t_hiv_encounters_14,
coalesce(t_hiv_encounters_15, 0) t_hiv_encounters_15,
coalesce(t_hiv_encounters_16, 0) t_hiv_encounters_16,
coalesce(t_hiv_encounters_17, 0) t_hiv_encounters_17,
coalesce(three_diff_hiv_med_06, 0) three_diff_hiv_med_06, 
coalesce(three_diff_hiv_med_07, 0) three_diff_hiv_med_07,
coalesce(three_diff_hiv_med_08, 0) three_diff_hiv_med_08,
coalesce(three_diff_hiv_med_09, 0) three_diff_hiv_med_09,
coalesce(three_diff_hiv_med_10, 0) three_diff_hiv_med_10,
coalesce(three_diff_hiv_med_11, 0) three_diff_hiv_med_11,
coalesce(three_diff_hiv_med_12, 0) three_diff_hiv_med_12,
coalesce(three_diff_hiv_med_13, 0) three_diff_hiv_med_13,
coalesce(three_diff_hiv_med_14, 0) three_diff_hiv_med_14,
coalesce(three_diff_hiv_med_15, 0) three_diff_hiv_med_15,
coalesce(three_diff_hiv_med_16, 0) three_diff_hiv_med_16,
coalesce(three_diff_hiv_med_17, 0) three_diff_hiv_med_17,
coalesce(truvada_num_rx_06, 0) truvada_num_rx_06,
coalesce(truvada_num_rx_07, 0) truvada_num_rx_07,
coalesce(truvada_num_rx_08, 0) truvada_num_rx_08,
coalesce(truvada_num_rx_09, 0) truvada_num_rx_09,
coalesce(truvada_num_rx_10, 0) truvada_num_rx_10,
coalesce(truvada_num_rx_11, 0) truvada_num_rx_11,
coalesce(truvada_num_rx_12, 0) truvada_num_rx_12,
coalesce(truvada_num_rx_13, 0) truvada_num_rx_13,
coalesce(truvada_num_rx_14, 0) truvada_num_rx_14,
coalesce(truvada_num_rx_15, 0) truvada_num_rx_15,
coalesce(truvada_num_rx_16, 0) truvada_num_rx_16,
coalesce(truvada_num_rx_17, 0) truvada_num_rx_17,
coalesce(truvada_num_pills_06, 0) truvada_num_pills_06,
coalesce(truvada_num_pills_07, 0) truvada_num_pills_07,
coalesce(truvada_num_pills_08, 0) truvada_num_pills_08,
coalesce(truvada_num_pills_09, 0) truvada_num_pills_09,
coalesce(truvada_num_pills_10, 0) truvada_num_pills_10,
coalesce(truvada_num_pills_11, 0) truvada_num_pills_11,
coalesce(truvada_num_pills_12, 0) truvada_num_pills_12,
coalesce(truvada_num_pills_13, 0) truvada_num_pills_13,
coalesce(truvada_num_pills_14, 0) truvada_num_pills_14,
coalesce(truvada_num_pills_15, 0) truvada_num_pills_15,
coalesce(truvada_num_pills_16, 0) truvada_num_pills_16,
coalesce(truvada_num_pills_17, 0) truvada_num_pills_17,
coalesce(hiv_neg_truvada_06,0) as hiv_neg_truvada_06,
coalesce(hiv_neg_truvada_07,0) as hiv_neg_truvada_07,
coalesce(hiv_neg_truvada_08,0) as hiv_neg_truvada_08,
coalesce(hiv_neg_truvada_09,0) as hiv_neg_truvada_09,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_first_icd_year, null) as hiv_first_icd_year, 
coalesce(abnormal_anal_cytology, null) as abnormal_anal_cytology,
coalesce(anal_dysplasia, null) as anal_dysplasia,
coalesce(anal_carcinoma_in_situ, null) as anal_carcinoma_in_situ,
coalesce(syphillis_of_any_site_or_stage_except_late, null) as syphillis_of_any_site_or_stage_except_late,
coalesce(anal_syphilis, null) as anal_syphilis,
coalesce(gonococcal_infection_of_anus_and_rectum, null) as gonococcal_infection_of_anus_and_rectum, 
coalesce(gonococcal_pharyngitis, null) as gonococcal_pharyngitis,
coalesce(chlamydial_infection_of_anus_and_recturm, null) as chlamydial_infection_of_anus_and_recturm,
coalesce(chlamydial_infection_of_pharynx, null) as chlamydial_infection_of_pharynx,
coalesce(lymphgranuloma_venereum, null) as lymphgranuloma_venereum,
coalesce(chancroid, null) as chancroid,
coalesce(granuloma_inguinale, null) as granuloma_inguinale,
coalesce(nongonococcal_urethritis, null) as nongonococcal_urethritis,
coalesce(herpes_simplex_w_complications, null) as herpes_simplex_w_complications,
coalesce(genital_herpes, null) as genital_herpes,
coalesce(anogenital_warts, null) as anogenital_warts,
coalesce(anorectal_ulcer, null) as anorectal_ulcer,
coalesce(unspecified_std, null) as unspecified_std,
coalesce(pelvic_inflammatory_disease, null) as pelvic_inflammatory_disease,
coalesce(contact_with_or_exposure_to_venereal_disease, null) as contact_with_or_exposure_to_venereal_disease,
coalesce(high_risk_sexual_behavior, null) as high_risk_sexual_behavior,
coalesce(hiv_counseling, null) as hiv_counseling,
coalesce(anorexia_nervosa, null) as anorexia_nervosa,
coalesce(bulimia_nervosa, null) as bulimia_nervosa,
coalesce(eating_disorder_nos, null) as eating_disorder_nos,
coalesce(gender_identity_disorders, null) as gender_identity_disorders,
coalesce(trans_sexualism, null) as trans_sexualism,
coalesce(counseling_for_child_sexual_abuse, null) as counseling_for_child_sexual_abuse,
coalesce(foreign_body_in_anus, null) as foreign_body_in_anus,
coalesce(alcohol_dependence, null) as alcohol_dependence,
coalesce(opioid_dependence, null) as opioid_dependence,
coalesce(sedative_hypnotic_or_anxiolytic_dependence, null) as sedative_hypnotic_or_anxiolytic_dependence,
coalesce(cocaine_dependence, null) as cocaine_dependence,
coalesce(amphetamine_dependence, null) as amphetamine_dependence,
coalesce(unspecified_drug_dependence, null) as unspecified_drug_dependence,
coalesce(depression, null) as depression,
coalesce(rx_bicillin_06, 0) as rx_bicillin_06,
coalesce(rx_bicillin_07, 0) as rx_bicillin_07,
coalesce(rx_bicillin_08, 0) as rx_bicillin_08,
coalesce(rx_bicillin_09, 0) as rx_bicillin_09,
coalesce(rx_bicillin_10, 0) as rx_bicillin_10,
coalesce(rx_bicillin_11, 0) as rx_bicillin_11,
coalesce(rx_bicillin_12, 0) as rx_bicillin_12,
coalesce(rx_bicillin_13, 0) as rx_bicillin_13,
coalesce(rx_bicillin_14, 0) as rx_bicillin_14,
coalesce(rx_bicillin_15, 0) as rx_bicillin_15,
coalesce(rx_bicillin_16, 0) as rx_bicillin_16,
coalesce(rx_bicillin_17, 0) as rx_bicillin_17,
coalesce(rx_azithromycin_06,0) as rx_azithromycin_06,
coalesce(rx_azithromycin_07,0) as rx_azithromycin_07,
coalesce(rx_azithromycin_08,0) as rx_azithromycin_08,
coalesce(rx_azithromycin_09,0) as rx_azithromycin_09,
coalesce(rx_azithromycin_10,0) as rx_azithromycin_10,
coalesce(rx_azithromycin_11,0) as rx_azithromycin_11,
coalesce(rx_azithromycin_12,0) as rx_azithromycin_12,
coalesce(rx_azithromycin_13,0) as rx_azithromycin_13,
coalesce(rx_azithromycin_14,0) as rx_azithromycin_14,
coalesce(rx_azithromycin_15,0) as rx_azithromycin_15,
coalesce(rx_azithromycin_16,0) as rx_azithromycin_16,
coalesce(rx_azithromycin_17,0) as rx_azithromycin_17,
coalesce(rx_ceftriaxone_06, 0) as rx_ceftriaxone_06,
coalesce(rx_ceftriaxone_07, 0) as rx_ceftriaxone_07,
coalesce(rx_ceftriaxone_08, 0) as rx_ceftriaxone_08,
coalesce(rx_ceftriaxone_09, 0) as rx_ceftriaxone_09,
coalesce(rx_ceftriaxone_10, 0) as rx_ceftriaxone_10,
coalesce(rx_ceftriaxone_11, 0) as rx_ceftriaxone_11,
coalesce(rx_ceftriaxone_12, 0) as rx_ceftriaxone_12,
coalesce(rx_ceftriaxone_13, 0) as rx_ceftriaxone_13,
coalesce(rx_ceftriaxone_14, 0) as rx_ceftriaxone_14,
coalesce(rx_ceftriaxone_15, 0) as rx_ceftriaxone_15,
coalesce(rx_ceftriaxone_16, 0) as rx_ceftriaxone_16,
coalesce(rx_ceftriaxone_17, 0) as rx_ceftriaxone_17,
coalesce(rx_methadone_06, 0) as rx_methadone_06,
coalesce(rx_methadone_07, 0) as rx_methadone_07,
coalesce(rx_methadone_08, 0) as rx_methadone_08,
coalesce(rx_methadone_09, 0) as rx_methadone_09,
coalesce(rx_methadone_10, 0) as rx_methadone_10,
coalesce(rx_methadone_11, 0) as rx_methadone_11,
coalesce(rx_methadone_12, 0) as rx_methadone_12,
coalesce(rx_methadone_13, 0) as rx_methadone_13,
coalesce(rx_methadone_14, 0) as rx_methadone_14,
coalesce(rx_methadone_15, 0) as rx_methadone_15,
coalesce(rx_methadone_16, 0) as rx_methadone_16,
coalesce(rx_methadone_17, 0) as rx_methadone_17,
coalesce(rx_suboxone_06, 0) as rx_suboxone_06,
coalesce(rx_suboxone_07, 0) as rx_suboxone_07,
coalesce(rx_suboxone_08, 0) as rx_suboxone_08,
coalesce(rx_suboxone_09, 0) as rx_suboxone_09,
coalesce(rx_suboxone_10, 0) as rx_suboxone_10,
coalesce(rx_suboxone_11, 0) as rx_suboxone_11,
coalesce(rx_suboxone_12, 0) as rx_suboxone_12,
coalesce(rx_suboxone_13, 0) as rx_suboxone_13,
coalesce(rx_suboxone_14, 0) as rx_suboxone_14,
coalesce(rx_suboxone_15, 0) as rx_suboxone_15,
coalesce(rx_suboxone_16, 0) as rx_suboxone_16,
coalesce(rx_suboxone_17, 0) as rx_suboxone_17,
coalesce(rx_viagara_cilais_or_levitra_06, 0) as rx_viagara_cilais_or_levitra_06,
coalesce(rx_viagara_cilais_or_levitra_07, 0) as rx_viagara_cilais_or_levitra_07,
coalesce(rx_viagara_cilais_or_levitra_08, 0) as rx_viagara_cilais_or_levitra_08,
coalesce(rx_viagara_cilais_or_levitra_09, 0) as rx_viagara_cilais_or_levitra_09,
coalesce(rx_viagara_cilais_or_levitra_10, 0) as rx_viagara_cilais_or_levitra_10,
coalesce(rx_viagara_cilais_or_levitra_11, 0) as rx_viagara_cilais_or_levitra_11,
coalesce(rx_viagara_cilais_or_levitra_12, 0) as rx_viagara_cilais_or_levitra_12,
coalesce(rx_viagara_cilais_or_levitra_13, 0) as rx_viagara_cilais_or_levitra_13,
coalesce(rx_viagara_cilais_or_levitra_14, 0) as rx_viagara_cilais_or_levitra_14,
coalesce(rx_viagara_cilais_or_levitra_15, 0) as rx_viagara_cilais_or_levitra_15,
coalesce(rx_viagara_cilais_or_levitra_16, 0) as rx_viagara_cilais_or_levitra_16,
coalesce(rx_viagara_cilais_or_levitra_17, 0) as rx_viagara_cilais_or_levitra_17
FROM stiprep_index_pats idx
FULL OUTER JOIN stiprep_diag_fields d
	ON idx.patient_id = d.patient_id
FULL OUTER JOIN stiprep_gon_counts g
	ON idx.patient_id = g.patient_id
FULL OUTER JOIN stiprep_chlam_counts c
    ON idx.patient_id = c.patient_id 
FULL OUTER JOIN stiprep_syph_counts s
    ON idx.patient_id = s.patient_id 
FULL OUTER JOIN stiprep_hepc_counts hc
    ON idx.patient_id = hc.patient_id 
FULL OUTER JOIN stiprep_hepc_cases hcc
    ON idx.patient_id = hcc.patient_id 
FULL OUTER JOIN stiprep_hepb_counts hb
    ON idx.patient_id = hb.patient_id
FULL OUTER JOIN stiprep_hebp_cases hbc
    ON idx.patient_id = hbc.patient_id
FULL OUTER JOIN stiprep_rx_counts rx
    ON idx.patient_id = rx.patient_id
FULL OUTER JOIN stiprep_cpts_of_interest cpt
    ON idx.patient_id = cpt.patient_id
FULL OUTER JOIN stiprep_gon_cases gonc
	ON idx.patient_id = gonc.patient_id
FULL OUTER JOIN stiprep_chlam_cases chlamc
	ON idx.patient_id = chlamc.patient_id
FULL OUTER JOIN stiprep_syph_cases syphc
	ON idx.patient_id = syphc.patient_id
FULL OUTER JOIN stiprep_sti_enc_py sti_enc
	ON idx.patient_id = sti_enc.patient_id
FULL OUTER JOIN stiprep_first_sti_index sti_first
	ON idx.patient_id = sti_first.patient_id
FULL OUTER JOIN stiprep_second_sti_index sti_second
	ON idx.patient_id = sti_second.patient_id
FULL OUTER JOIN stiprep_second_days sti_sec_days
	ON idx.patient_id = sti_sec_days.patient_id
FULL OUTER JOIN stiprep_third_sti_index sti_third
	ON idx.patient_id = sti_third.patient_id
FULL OUTER JOIN stiprep_third_days third_days
	ON idx.patient_id = third_days.patient_id
FULL OUTER JOIN stiprep_fourth_sti_index sti_fourth
	ON idx.patient_id = sti_fourth.patient_id
FULL OUTER JOIN stiprep_fourth_days fourth_days
	ON idx.patient_id = fourth_days.patient_id
FULL OUTER JOIN stiprep_hepb_diags_2 hepb_hist
	ON idx.patient_id = hepb_hist.patient_id
FULL OUTER JOIN stiprep_hiv_counts hiv_counts
	ON idx.patient_id = hiv_counts.patient_id
FULL OUTER JOIN stiprep_max_viral_load viral_load
	ON idx.patient_id = viral_load.patient_id
FULL OUTER JOIN stiprep_hiv_all_details hiv_details
	ON idx.patient_id = hiv_details.patient_id
FULL OUTER JOIN stiprep_enc_counts enc_counts
	ON idx.patient_id = enc_counts.patient_id
FULL OUTER JOIN stiprep_hiv_enc_counts hiv_enc_counts
	ON idx.patient_id = hiv_enc_counts.patient_id
FULL OUTER JOIN stiprep_hiv_new_diag hiv_new_diag
	ON idx.patient_id = hiv_new_diag.patient_id;
	

-- JOIN TO PATIENT TABLE
CREATE TABLE stiprep_output_with_patient AS SELECT 
T1.id as patient_id, 
date_part('year',age(date_of_birth)) current_age,
T1.gender,
T1.race,
T1.home_language,
'unknown'::text gender_of_sex_partners,
concat(T1.last_name, ', ',T1.first_name) as name,
T1.date_of_birth::date,
T2.*
FROM emr_patient T1 
INNER JOIN stiprep_output_part_1 T2 ON ((T1.id = T2.master_patient_id)) 
-- filter out test patients
WHERE (last_name not ilike 'XB%' and last_name not ilike 'XX%');

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_06 = null,
fourthsti_3to4_days_index2006 = null,
fourthsti_index2006 = null,
gonorrhea_cases_06 = null,
hiv_neg_truvada_06 = null,
indexsti_2006 = null,
indexsti_month_06 = null,
positive_chlamydia_tests_06 = null,
positive_chlamydia_tests_rectal_06 = null,
positive_chlamydia_tests_throat_06 = null,
positive_chlamydia_tests_urog_06 = null,
positive_gonorrhea_tests_06 = null,
positive_gonorrhea_tests_rectal_06 = null,
positive_gonorrhea_tests_throat_06 = null,
positive_gonorrhea_tests_urog_06 = null,
rx_azithromycin_06 = null,
rx_bicillin_06 = null,
rx_ceftriaxone_06 = null,
rx_methadone_06 = null,
rx_suboxone_06 = null,
rx_viagara_cilais_or_levitra_06 = null,
secsti_1to2_days_index2006 = null,
secsti_index2006 = null,
syphilis_cases_06 = null,
t_amb_encounters_06 = null,
t_hiv_agab_06 = null,
t_hiv_elisa_06 = null,
t_hiv_encounters_06 = null,
t_hiv_rna_06 = null,
t_hiv_wb_06 = null,
t_sti_enc_py_cases_06 = null,
thirdsti_2to3_days_index2006 = null,
thirdsti_index2006 = null,
three_diff_hiv_med_06 = null,
total_chlamydia_tests_06 = null,
total_chlamydia_tests_rectal_06 = null,
total_chlamydia_tests_throat_06 = null,
total_chlamydia_tests_urog_06 = null,
total_gonorrhea_tests_06 = null,
total_gonorrhea_tests_rectal_06 = null,
total_gonorrhea_tests_throat_06 = null,
total_gonorrhea_tests_urog_06 = null,
total_hcv_antibody_tests_06 = null,
total_hcv_rna_tests_06 = null,
total_hepb_dna_tests_06 = null,
total_hepb_surface_antigen_tests_06 = null,
total_syphilis_tests_06 = null,
truvada_num_pills_06 = null,
truvada_num_rx_06 = null
WHERE age_2006 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_07 = null,
fourthsti_3to4_days_index2007 = null,
fourthsti_index2007 = null,
gonorrhea_cases_07 = null,
hiv_neg_truvada_07 = null,
indexsti_2007 = null,
indexsti_month_07 = null,
positive_chlamydia_tests_07 = null,
positive_chlamydia_tests_rectal_07 = null,
positive_chlamydia_tests_throat_07 = null,
positive_chlamydia_tests_urog_07 = null,
positive_gonorrhea_tests_07 = null,
positive_gonorrhea_tests_rectal_07 = null,
positive_gonorrhea_tests_throat_07 = null,
positive_gonorrhea_tests_urog_07 = null,
rx_azithromycin_07 = null,
rx_bicillin_07 = null,
rx_ceftriaxone_07 = null,
rx_methadone_07 = null,
rx_suboxone_07 = null,
rx_viagara_cilais_or_levitra_07 = null,
secsti_1to2_days_index2007 = null,
secsti_index2007 = null,
syphilis_cases_07 = null,
t_amb_encounters_07 = null,
t_hiv_agab_07 = null,
t_hiv_elisa_07 = null,
t_hiv_encounters_07 = null,
t_hiv_rna_07 = null,
t_hiv_wb_07 = null,
t_sti_enc_py_cases_07 = null,
thirdsti_2to3_days_index2007 = null,
thirdsti_index2007 = null,
three_diff_hiv_med_07 = null,
total_chlamydia_tests_07 = null,
total_chlamydia_tests_rectal_07 = null,
total_chlamydia_tests_throat_07 = null,
total_chlamydia_tests_urog_07 = null,
total_gonorrhea_tests_07 = null,
total_gonorrhea_tests_rectal_07 = null,
total_gonorrhea_tests_throat_07 = null,
total_gonorrhea_tests_urog_07 = null,
total_hcv_antibody_tests_07 = null,
total_hcv_rna_tests_07 = null,
total_hepb_dna_tests_07 = null,
total_hepb_surface_antigen_tests_07 = null,
total_syphilis_tests_07 = null,
truvada_num_pills_07 = null,
truvada_num_rx_07 = null
WHERE age_2007 <15;


UPDATE stiprep_output_with_patient
SET
chlamydia_cases_08 = null,
fourthsti_3to4_days_index2008 = null,
fourthsti_index2008 = null,
gonorrhea_cases_08 = null,
hiv_neg_truvada_08 = null,
indexsti_2008 = null,
indexsti_month_08 = null,
positive_chlamydia_tests_08 = null,
positive_chlamydia_tests_rectal_08 = null,
positive_chlamydia_tests_throat_08 = null,
positive_chlamydia_tests_urog_08 = null,
positive_gonorrhea_tests_08 = null,
positive_gonorrhea_tests_rectal_08 = null,
positive_gonorrhea_tests_throat_08 = null,
positive_gonorrhea_tests_urog_08 = null,
rx_azithromycin_08 = null,
rx_bicillin_08 = null,
rx_ceftriaxone_08 = null,
rx_methadone_08 = null,
rx_suboxone_08 = null,
rx_viagara_cilais_or_levitra_08 = null,
secsti_1to2_days_index2008 = null,
secsti_index2008 = null,
syphilis_cases_08 = null,
t_amb_encounters_08 = null,
t_hiv_agab_08 = null,
t_hiv_elisa_08 = null,
t_hiv_encounters_08 = null,
t_hiv_rna_08 = null,
t_hiv_wb_08 = null,
t_sti_enc_py_cases_08 = null,
thirdsti_2to3_days_index2008 = null,
thirdsti_index2008 = null,
three_diff_hiv_med_08 = null,
total_chlamydia_tests_08 = null,
total_chlamydia_tests_rectal_08 = null,
total_chlamydia_tests_throat_08 = null,
total_chlamydia_tests_urog_08 = null,
total_gonorrhea_tests_08 = null,
total_gonorrhea_tests_rectal_08 = null,
total_gonorrhea_tests_throat_08 = null,
total_gonorrhea_tests_urog_08 = null,
total_hcv_antibody_tests_08 = null,
total_hcv_rna_tests_08 = null,
total_hepb_dna_tests_08 = null,
total_hepb_surface_antigen_tests_08 = null,
total_syphilis_tests_08 = null,
truvada_num_pills_08 = null,
truvada_num_rx_08 = null
WHERE age_2008 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_09 = null,
fourthsti_3to4_days_index2009 = null,
fourthsti_index2009 = null,
gonorrhea_cases_09 = null,
hiv_neg_truvada_09 = null,
indexsti_2009 = null,
indexsti_month_09 = null,
positive_chlamydia_tests_09 = null,
positive_chlamydia_tests_rectal_09 = null,
positive_chlamydia_tests_throat_09 = null,
positive_chlamydia_tests_urog_09 = null,
positive_gonorrhea_tests_09 = null,
positive_gonorrhea_tests_rectal_09 = null,
positive_gonorrhea_tests_throat_09 = null,
positive_gonorrhea_tests_urog_09 = null,
rx_azithromycin_09 = null,
rx_bicillin_09 = null,
rx_ceftriaxone_09 = null,
rx_methadone_09 = null,
rx_suboxone_09 = null,
rx_viagara_cilais_or_levitra_09 = null,
secsti_1to2_days_index2009 = null,
secsti_index2009 = null,
syphilis_cases_09 = null,
t_amb_encounters_09 = null,
t_hiv_agab_09 = null,
t_hiv_elisa_09 = null,
t_hiv_encounters_09 = null,
t_hiv_rna_09 = null,
t_hiv_wb_09 = null,
t_sti_enc_py_cases_09 = null,
thirdsti_2to3_days_index2009 = null,
thirdsti_index2009 = null,
three_diff_hiv_med_09 = null,
total_chlamydia_tests_09 = null,
total_chlamydia_tests_rectal_09 = null,
total_chlamydia_tests_throat_09 = null,
total_chlamydia_tests_urog_09 = null,
total_gonorrhea_tests_09 = null,
total_gonorrhea_tests_rectal_09 = null,
total_gonorrhea_tests_throat_09 = null,
total_gonorrhea_tests_urog_09 = null,
total_hcv_antibody_tests_09 = null,
total_hcv_rna_tests_09 = null,
total_hepb_dna_tests_09 = null,
total_hepb_surface_antigen_tests_09 = null,
total_syphilis_tests_09 = null,
truvada_num_pills_09 = null,
truvada_num_rx_09 = null
WHERE age_2009 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_10 = null,
fourthsti_3to4_days_index2010 = null,
fourthsti_index2010 = null,
gonorrhea_cases_10 = null,
hiv_neg_truvada_10 = null,
indexsti_2010 = null,
indexsti_month_10 = null,
positive_chlamydia_tests_10 = null,
positive_chlamydia_tests_rectal_10 = null,
positive_chlamydia_tests_throat_10 = null,
positive_chlamydia_tests_urog_10 = null,
positive_gonorrhea_tests_10 = null,
positive_gonorrhea_tests_rectal_10 = null,
positive_gonorrhea_tests_throat_10 = null,
positive_gonorrhea_tests_urog_10 = null,
rx_azithromycin_10 = null,
rx_bicillin_10 = null,
rx_ceftriaxone_10 = null,
rx_methadone_10 = null,
rx_suboxone_10 = null,
rx_viagara_cilais_or_levitra_10 = null,
secsti_1to2_days_index2010 = null,
secsti_index2010 = null,
syphilis_cases_10 = null,
t_amb_encounters_10 = null,
t_hiv_agab_10 = null,
t_hiv_elisa_10 = null,
t_hiv_encounters_10 = null,
t_hiv_rna_10 = null,
t_hiv_wb_10 = null,
t_sti_enc_py_cases_10 = null,
thirdsti_2to3_days_index2010 = null,
thirdsti_index2010 = null,
three_diff_hiv_med_10 = null,
total_chlamydia_tests_10 = null,
total_chlamydia_tests_rectal_10 = null,
total_chlamydia_tests_throat_10 = null,
total_chlamydia_tests_urog_10 = null,
total_gonorrhea_tests_10 = null,
total_gonorrhea_tests_rectal_10 = null,
total_gonorrhea_tests_throat_10 = null,
total_gonorrhea_tests_urog_10 = null,
total_hcv_antibody_tests_10 = null,
total_hcv_rna_tests_10 = null,
total_hepb_dna_tests_10 = null,
total_hepb_surface_antigen_tests_10 = null,
total_syphilis_tests_10 = null,
truvada_num_pills_10 = null,
truvada_num_rx_10 = null
WHERE age_2010 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_11 = null,
fourthsti_3to4_days_index2011 = null,
fourthsti_index2011 = null,
gonorrhea_cases_11 = null,
hiv_neg_truvada_11 = null,
indexsti_2011 = null,
indexsti_month_11 = null,
positive_chlamydia_tests_11 = null,
positive_chlamydia_tests_rectal_11 = null,
positive_chlamydia_tests_throat_11 = null,
positive_chlamydia_tests_urog_11 = null,
positive_gonorrhea_tests_11 = null,
positive_gonorrhea_tests_rectal_11 = null,
positive_gonorrhea_tests_throat_11 = null,
positive_gonorrhea_tests_urog_11 = null,
rx_azithromycin_11 = null,
rx_bicillin_11 = null,
rx_ceftriaxone_11 = null,
rx_methadone_11 = null,
rx_suboxone_11 = null,
rx_viagara_cilais_or_levitra_11 = null,
secsti_1to2_days_index2011 = null,
secsti_index2011 = null,
syphilis_cases_11 = null,
t_amb_encounters_11 = null,
t_hiv_agab_11 = null,
t_hiv_elisa_11 = null,
t_hiv_encounters_11 = null,
t_hiv_rna_11 = null,
t_hiv_wb_11 = null,
t_sti_enc_py_cases_11 = null,
thirdsti_2to3_days_index2011 = null,
thirdsti_index2011 = null,
three_diff_hiv_med_11 = null,
total_chlamydia_tests_11 = null,
total_chlamydia_tests_rectal_11 = null,
total_chlamydia_tests_throat_11 = null,
total_chlamydia_tests_urog_11 = null,
total_gonorrhea_tests_11 = null,
total_gonorrhea_tests_rectal_11 = null,
total_gonorrhea_tests_throat_11 = null,
total_gonorrhea_tests_urog_11 = null,
total_hcv_antibody_tests_11 = null,
total_hcv_rna_tests_11 = null,
total_hepb_dna_tests_11 = null,
total_hepb_surface_antigen_tests_11 = null,
total_syphilis_tests_11 = null,
truvada_num_pills_11 = null,
truvada_num_rx_11 = null
WHERE age_2011 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_12 = null,
fourthsti_3to4_days_index2012 = null,
fourthsti_index2012 = null,
gonorrhea_cases_12 = null,
hiv_neg_truvada_12 = null,
indexsti_2012 = null,
indexsti_month_12 = null,
positive_chlamydia_tests_12 = null,
positive_chlamydia_tests_rectal_12 = null,
positive_chlamydia_tests_throat_12 = null,
positive_chlamydia_tests_urog_12 = null,
positive_gonorrhea_tests_12 = null,
positive_gonorrhea_tests_rectal_12 = null,
positive_gonorrhea_tests_throat_12 = null,
positive_gonorrhea_tests_urog_12 = null,
rx_azithromycin_12 = null,
rx_bicillin_12 = null,
rx_ceftriaxone_12 = null,
rx_methadone_12 = null,
rx_suboxone_12 = null,
rx_viagara_cilais_or_levitra_12 = null,
secsti_1to2_days_index2012 = null,
secsti_index2012 = null,
syphilis_cases_12 = null,
t_amb_encounters_12 = null,
t_hiv_agab_12 = null,
t_hiv_elisa_12 = null,
t_hiv_encounters_12 = null,
t_hiv_rna_12 = null,
t_hiv_wb_12 = null,
t_sti_enc_py_cases_12 = null,
thirdsti_2to3_days_index2012 = null,
thirdsti_index2012 = null,
three_diff_hiv_med_12 = null,
total_chlamydia_tests_12 = null,
total_chlamydia_tests_rectal_12 = null,
total_chlamydia_tests_throat_12 = null,
total_chlamydia_tests_urog_12 = null,
total_gonorrhea_tests_12 = null,
total_gonorrhea_tests_rectal_12 = null,
total_gonorrhea_tests_throat_12 = null,
total_gonorrhea_tests_urog_12 = null,
total_hcv_antibody_tests_12 = null,
total_hcv_rna_tests_12 = null,
total_hepb_dna_tests_12 = null,
total_hepb_surface_antigen_tests_12 = null,
total_syphilis_tests_12 = null,
truvada_num_pills_12 = null,
truvada_num_rx_12 = null
WHERE age_2012 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_13 = null,
fourthsti_3to4_days_index2013 = null,
fourthsti_index2013 = null,
gonorrhea_cases_13 = null,
hiv_neg_truvada_13 = null,
indexsti_2013 = null,
indexsti_month_13 = null,
positive_chlamydia_tests_13 = null,
positive_chlamydia_tests_rectal_13 = null,
positive_chlamydia_tests_throat_13 = null,
positive_chlamydia_tests_urog_13 = null,
positive_gonorrhea_tests_13 = null,
positive_gonorrhea_tests_rectal_13 = null,
positive_gonorrhea_tests_throat_13 = null,
positive_gonorrhea_tests_urog_13 = null,
rx_azithromycin_13 = null,
rx_bicillin_13 = null,
rx_ceftriaxone_13 = null,
rx_methadone_13 = null,
rx_suboxone_13 = null,
rx_viagara_cilais_or_levitra_13 = null,
secsti_1to2_days_index2013 = null,
secsti_index2013 = null,
syphilis_cases_13 = null,
t_amb_encounters_13 = null,
t_hiv_agab_13 = null,
t_hiv_elisa_13 = null,
t_hiv_encounters_13 = null,
t_hiv_rna_13 = null,
t_hiv_wb_13 = null,
t_sti_enc_py_cases_13 = null,
thirdsti_2to3_days_index2013 = null,
thirdsti_index2013 = null,
three_diff_hiv_med_13 = null,
total_chlamydia_tests_13 = null,
total_chlamydia_tests_rectal_13 = null,
total_chlamydia_tests_throat_13 = null,
total_chlamydia_tests_urog_13 = null,
total_gonorrhea_tests_13 = null,
total_gonorrhea_tests_rectal_13 = null,
total_gonorrhea_tests_throat_13 = null,
total_gonorrhea_tests_urog_13 = null,
total_hcv_antibody_tests_13 = null,
total_hcv_rna_tests_13 = null,
total_hepb_dna_tests_13 = null,
total_hepb_surface_antigen_tests_13 = null,
total_syphilis_tests_13 = null,
truvada_num_pills_13 = null,
truvada_num_rx_13 = null
WHERE age_2013 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_14 = null,
fourthsti_3to4_days_index2014 = null,
fourthsti_index2014 = null,
gonorrhea_cases_14 = null,
hiv_neg_truvada_14 = null,
indexsti_2014 = null,
indexsti_month_14 = null,
positive_chlamydia_tests_14 = null,
positive_chlamydia_tests_rectal_14 = null,
positive_chlamydia_tests_throat_14 = null,
positive_chlamydia_tests_urog_14 = null,
positive_gonorrhea_tests_14 = null,
positive_gonorrhea_tests_rectal_14 = null,
positive_gonorrhea_tests_throat_14 = null,
positive_gonorrhea_tests_urog_14 = null,
rx_azithromycin_14 = null,
rx_bicillin_14 = null,
rx_ceftriaxone_14 = null,
rx_methadone_14 = null,
rx_suboxone_14 = null,
rx_viagara_cilais_or_levitra_14 = null,
secsti_1to2_days_index2014 = null,
secsti_index2014 = null,
syphilis_cases_14 = null,
t_amb_encounters_14 = null,
t_hiv_agab_14 = null,
t_hiv_elisa_14 = null,
t_hiv_encounters_14 = null,
t_hiv_rna_14 = null,
t_hiv_wb_14 = null,
t_sti_enc_py_cases_14 = null,
thirdsti_2to3_days_index2014 = null,
thirdsti_index2014 = null,
three_diff_hiv_med_14 = null,
total_chlamydia_tests_14 = null,
total_chlamydia_tests_rectal_14 = null,
total_chlamydia_tests_throat_14 = null,
total_chlamydia_tests_urog_14 = null,
total_gonorrhea_tests_14 = null,
total_gonorrhea_tests_rectal_14 = null,
total_gonorrhea_tests_throat_14 = null,
total_gonorrhea_tests_urog_14 = null,
total_hcv_antibody_tests_14 = null,
total_hcv_rna_tests_14 = null,
total_hepb_dna_tests_14 = null,
total_hepb_surface_antigen_tests_14 = null,
total_syphilis_tests_14 = null,
truvada_num_pills_14 = null,
truvada_num_rx_14 = null
WHERE age_2014 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_15 = null,
fourthsti_3to4_days_index2015 = null,
fourthsti_index2015 = null,
gonorrhea_cases_15 = null,
hiv_neg_truvada_15 = null,
indexsti_2015 = null,
indexsti_month_15 = null,
positive_chlamydia_tests_15 = null,
positive_chlamydia_tests_rectal_15 = null,
positive_chlamydia_tests_throat_15 = null,
positive_chlamydia_tests_urog_15 = null,
positive_gonorrhea_tests_15 = null,
positive_gonorrhea_tests_rectal_15 = null,
positive_gonorrhea_tests_throat_15 = null,
positive_gonorrhea_tests_urog_15 = null,
rx_azithromycin_15 = null,
rx_bicillin_15 = null,
rx_ceftriaxone_15 = null,
rx_methadone_15 = null,
rx_suboxone_15 = null,
rx_viagara_cilais_or_levitra_15 = null,
secsti_1to2_days_index2015 = null,
secsti_index2015 = null,
syphilis_cases_15 = null,
t_amb_encounters_15 = null,
t_hiv_agab_15 = null,
t_hiv_elisa_15 = null,
t_hiv_encounters_15 = null,
t_hiv_rna_15 = null,
t_hiv_wb_15 = null,
t_sti_enc_py_cases_15 = null,
thirdsti_2to3_days_index2015 = null,
thirdsti_index2015 = null,
three_diff_hiv_med_15 = null,
total_chlamydia_tests_15 = null,
total_chlamydia_tests_rectal_15 = null,
total_chlamydia_tests_throat_15 = null,
total_chlamydia_tests_urog_15 = null,
total_gonorrhea_tests_15 = null,
total_gonorrhea_tests_rectal_15 = null,
total_gonorrhea_tests_throat_15 = null,
total_gonorrhea_tests_urog_15 = null,
total_hcv_antibody_tests_15 = null,
total_hcv_rna_tests_15 = null,
total_hepb_dna_tests_15 = null,
total_hepb_surface_antigen_tests_15 = null,
total_syphilis_tests_15 = null,
truvada_num_pills_15 = null,
truvada_num_rx_15 = null
WHERE age_2015 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_16 = null,
fourthsti_3to4_days_index2016 = null,
fourthsti_index2016 = null,
gonorrhea_cases_16 = null,
hiv_neg_truvada_16 = null,
indexsti_2016 = null,
indexsti_month_16 = null,
positive_chlamydia_tests_16 = null,
positive_chlamydia_tests_rectal_16 = null,
positive_chlamydia_tests_throat_16 = null,
positive_chlamydia_tests_urog_16 = null,
positive_gonorrhea_tests_16 = null,
positive_gonorrhea_tests_rectal_16 = null,
positive_gonorrhea_tests_throat_16 = null,
positive_gonorrhea_tests_urog_16 = null,
rx_azithromycin_16 = null,
rx_bicillin_16 = null,
rx_ceftriaxone_16 = null,
rx_methadone_16 = null,
rx_suboxone_16 = null,
rx_viagara_cilais_or_levitra_16 = null,
secsti_1to2_days_index2016 = null,
secsti_index2016 = null,
syphilis_cases_16 = null,
t_amb_encounters_16 = null,
t_hiv_agab_16 = null,
t_hiv_elisa_16 = null,
t_hiv_encounters_16 = null,
t_hiv_rna_16 = null,
t_hiv_wb_16 = null,
t_sti_enc_py_cases_16 = null,
thirdsti_2to3_days_index2016 = null,
thirdsti_index2016 = null,
three_diff_hiv_med_16 = null,
total_chlamydia_tests_16 = null,
total_chlamydia_tests_rectal_16 = null,
total_chlamydia_tests_throat_16 = null,
total_chlamydia_tests_urog_16 = null,
total_gonorrhea_tests_16 = null,
total_gonorrhea_tests_rectal_16 = null,
total_gonorrhea_tests_throat_16 = null,
total_gonorrhea_tests_urog_16 = null,
total_hcv_antibody_tests_16 = null,
total_hcv_rna_tests_16 = null,
total_hepb_dna_tests_16 = null,
total_hepb_surface_antigen_tests_16 = null,
total_syphilis_tests_16 = null,
truvada_num_pills_16 = null,
truvada_num_rx_16 = null
WHERE age_2016 <15;

UPDATE stiprep_output_with_patient
SET
chlamydia_cases_17 = null,
fourthsti_3to4_days_index2017 = null,
fourthsti_index2017 = null,
gonorrhea_cases_17 = null,
hiv_neg_truvada_17 = null,
indexsti_2017 = null,
indexsti_month_17 = null,
positive_chlamydia_tests_17 = null,
positive_chlamydia_tests_rectal_17 = null,
positive_chlamydia_tests_throat_17 = null,
positive_chlamydia_tests_urog_17 = null,
positive_gonorrhea_tests_17 = null,
positive_gonorrhea_tests_rectal_17 = null,
positive_gonorrhea_tests_throat_17 = null,
positive_gonorrhea_tests_urog_17 = null,
rx_azithromycin_17 = null,
rx_bicillin_17 = null,
rx_ceftriaxone_17 = null,
rx_methadone_17 = null,
rx_suboxone_17 = null,
rx_viagara_cilais_or_levitra_17 = null,
secsti_1to2_days_index2017 = null,
secsti_index2017 = null,
syphilis_cases_17 = null,
t_amb_encounters_17 = null,
t_hiv_agab_17 = null,
t_hiv_elisa_17 = null,
t_hiv_encounters_17 = null,
t_hiv_rna_17 = null,
t_hiv_wb_17 = null,
t_sti_enc_py_cases_17 = null,
thirdsti_2to3_days_index2017 = null,
thirdsti_index2017 = null,
three_diff_hiv_med_17 = null,
total_chlamydia_tests_17 = null,
total_chlamydia_tests_rectal_17 = null,
total_chlamydia_tests_throat_17 = null,
total_chlamydia_tests_urog_17 = null,
total_gonorrhea_tests_17 = null,
total_gonorrhea_tests_rectal_17 = null,
total_gonorrhea_tests_throat_17 = null,
total_gonorrhea_tests_urog_17 = null,
total_hcv_antibody_tests_17 = null,
total_hcv_rna_tests_17 = null,
total_hepb_dna_tests_17 = null,
total_hepb_surface_antigen_tests_17 = null,
total_syphilis_tests_17 = null,
truvada_num_pills_17 = null,
truvada_num_rx_17 = null
WHERE age_2017 <15;

CREATE TABLE stiprep_report_final_output AS
SELECT 
patient_id,
name,
date_of_birth,
current_age,
gender,
race,
home_language,
gender_of_sex_partners,
t_amb_encounters_06,
t_amb_encounters_07,
t_amb_encounters_08,
t_amb_encounters_09,
t_amb_encounters_10,
t_amb_encounters_11,
t_amb_encounters_12,
t_amb_encounters_13,
t_amb_encounters_14,
t_amb_encounters_15,
t_amb_encounters_16,
t_amb_encounters_17,
total_gonorrhea_tests_06,
total_gonorrhea_tests_07,
total_gonorrhea_tests_08,
total_gonorrhea_tests_09,
total_gonorrhea_tests_10,
total_gonorrhea_tests_11,
total_gonorrhea_tests_12,
total_gonorrhea_tests_13,
total_gonorrhea_tests_14,
total_gonorrhea_tests_15,
total_gonorrhea_tests_16,
total_gonorrhea_tests_17,
positive_gonorrhea_tests_06,
positive_gonorrhea_tests_07,
positive_gonorrhea_tests_08,
positive_gonorrhea_tests_09,
positive_gonorrhea_tests_10,
positive_gonorrhea_tests_11,
positive_gonorrhea_tests_12,
positive_gonorrhea_tests_13,
positive_gonorrhea_tests_14,
positive_gonorrhea_tests_15,
positive_gonorrhea_tests_16,
positive_gonorrhea_tests_17,
total_gonorrhea_tests_throat_06,
total_gonorrhea_tests_throat_07,
total_gonorrhea_tests_throat_08,
total_gonorrhea_tests_throat_09,
total_gonorrhea_tests_throat_10,
total_gonorrhea_tests_throat_11,
total_gonorrhea_tests_throat_12,
total_gonorrhea_tests_throat_13,
total_gonorrhea_tests_throat_14,
total_gonorrhea_tests_throat_15,
total_gonorrhea_tests_throat_16,
total_gonorrhea_tests_throat_17,
positive_gonorrhea_tests_throat_06,
positive_gonorrhea_tests_throat_07,
positive_gonorrhea_tests_throat_08,
positive_gonorrhea_tests_throat_09,
positive_gonorrhea_tests_throat_10,
positive_gonorrhea_tests_throat_11,
positive_gonorrhea_tests_throat_12,
positive_gonorrhea_tests_throat_13,
positive_gonorrhea_tests_throat_14,
positive_gonorrhea_tests_throat_15,
positive_gonorrhea_tests_throat_16,
positive_gonorrhea_tests_throat_17,
total_gonorrhea_tests_rectal_06,
total_gonorrhea_tests_rectal_07,
total_gonorrhea_tests_rectal_08,
total_gonorrhea_tests_rectal_09,
total_gonorrhea_tests_rectal_10,
total_gonorrhea_tests_rectal_11,
total_gonorrhea_tests_rectal_12,
total_gonorrhea_tests_rectal_13,
total_gonorrhea_tests_rectal_14,
total_gonorrhea_tests_rectal_15,
total_gonorrhea_tests_rectal_16,
total_gonorrhea_tests_rectal_17,
positive_gonorrhea_tests_rectal_06,
positive_gonorrhea_tests_rectal_07,
positive_gonorrhea_tests_rectal_08,
positive_gonorrhea_tests_rectal_09,
positive_gonorrhea_tests_rectal_10,
positive_gonorrhea_tests_rectal_11,
positive_gonorrhea_tests_rectal_12,
positive_gonorrhea_tests_rectal_13,
positive_gonorrhea_tests_rectal_14,
positive_gonorrhea_tests_rectal_15,
positive_gonorrhea_tests_rectal_16,
positive_gonorrhea_tests_rectal_17,
total_gonorrhea_tests_urog_06,
total_gonorrhea_tests_urog_07,
total_gonorrhea_tests_urog_08,
total_gonorrhea_tests_urog_09,
total_gonorrhea_tests_urog_10,
total_gonorrhea_tests_urog_11,
total_gonorrhea_tests_urog_12,
total_gonorrhea_tests_urog_13,
total_gonorrhea_tests_urog_14,
total_gonorrhea_tests_urog_15,
total_gonorrhea_tests_urog_16,
total_gonorrhea_tests_urog_17,
positive_gonorrhea_tests_urog_06,
positive_gonorrhea_tests_urog_07,
positive_gonorrhea_tests_urog_08,
positive_gonorrhea_tests_urog_09,
positive_gonorrhea_tests_urog_10,
positive_gonorrhea_tests_urog_11,
positive_gonorrhea_tests_urog_12,
positive_gonorrhea_tests_urog_13,
positive_gonorrhea_tests_urog_14,
positive_gonorrhea_tests_urog_15,
positive_gonorrhea_tests_urog_16,
positive_gonorrhea_tests_urog_17,
gonorrhea_cases_06,
gonorrhea_cases_07,
gonorrhea_cases_08,
gonorrhea_cases_09,
gonorrhea_cases_10,
gonorrhea_cases_11,
gonorrhea_cases_12,
gonorrhea_cases_13,
gonorrhea_cases_14,
gonorrhea_cases_15,
gonorrhea_cases_16,
gonorrhea_cases_17,
total_chlamydia_tests_06,
total_chlamydia_tests_07,
total_chlamydia_tests_08,
total_chlamydia_tests_09,
total_chlamydia_tests_10,
total_chlamydia_tests_11,
total_chlamydia_tests_12,
total_chlamydia_tests_13,
total_chlamydia_tests_14,
total_chlamydia_tests_15,
total_chlamydia_tests_16,
total_chlamydia_tests_17,
positive_chlamydia_tests_06,
positive_chlamydia_tests_07,
positive_chlamydia_tests_08,
positive_chlamydia_tests_09,
positive_chlamydia_tests_10,
positive_chlamydia_tests_11,
positive_chlamydia_tests_12,
positive_chlamydia_tests_13,
positive_chlamydia_tests_14,
positive_chlamydia_tests_15,
positive_chlamydia_tests_16,
positive_chlamydia_tests_17,
total_chlamydia_tests_throat_06,
total_chlamydia_tests_throat_07,
total_chlamydia_tests_throat_08,
total_chlamydia_tests_throat_09,
total_chlamydia_tests_throat_10,
total_chlamydia_tests_throat_11,
total_chlamydia_tests_throat_12,
total_chlamydia_tests_throat_13,
total_chlamydia_tests_throat_14,
total_chlamydia_tests_throat_15,
total_chlamydia_tests_throat_16,
total_chlamydia_tests_throat_17,
positive_chlamydia_tests_throat_06,
positive_chlamydia_tests_throat_07,
positive_chlamydia_tests_throat_08,
positive_chlamydia_tests_throat_09,
positive_chlamydia_tests_throat_10,
positive_chlamydia_tests_throat_11,
positive_chlamydia_tests_throat_12,
positive_chlamydia_tests_throat_13,
positive_chlamydia_tests_throat_14,
positive_chlamydia_tests_throat_15,
positive_chlamydia_tests_throat_16,
positive_chlamydia_tests_throat_17,
total_chlamydia_tests_rectal_06,
total_chlamydia_tests_rectal_07,
total_chlamydia_tests_rectal_08,
total_chlamydia_tests_rectal_09,
total_chlamydia_tests_rectal_10,
total_chlamydia_tests_rectal_11,
total_chlamydia_tests_rectal_12,
total_chlamydia_tests_rectal_13,
total_chlamydia_tests_rectal_14,
total_chlamydia_tests_rectal_15,
total_chlamydia_tests_rectal_16,
total_chlamydia_tests_rectal_17,
positive_chlamydia_tests_rectal_06,
positive_chlamydia_tests_rectal_07,
positive_chlamydia_tests_rectal_08,
positive_chlamydia_tests_rectal_09,
positive_chlamydia_tests_rectal_10,
positive_chlamydia_tests_rectal_11,
positive_chlamydia_tests_rectal_12,
positive_chlamydia_tests_rectal_13,
positive_chlamydia_tests_rectal_14,
positive_chlamydia_tests_rectal_15,
positive_chlamydia_tests_rectal_16,
positive_chlamydia_tests_rectal_17,
total_chlamydia_tests_urog_06,
total_chlamydia_tests_urog_07,
total_chlamydia_tests_urog_08,
total_chlamydia_tests_urog_09,
total_chlamydia_tests_urog_10,
total_chlamydia_tests_urog_11,
total_chlamydia_tests_urog_12,
total_chlamydia_tests_urog_13,
total_chlamydia_tests_urog_14,
total_chlamydia_tests_urog_15,
total_chlamydia_tests_urog_16,
total_chlamydia_tests_urog_17,
positive_chlamydia_tests_urog_06,
positive_chlamydia_tests_urog_07,
positive_chlamydia_tests_urog_08,
positive_chlamydia_tests_urog_09,
positive_chlamydia_tests_urog_10,
positive_chlamydia_tests_urog_11,
positive_chlamydia_tests_urog_12,
positive_chlamydia_tests_urog_13,
positive_chlamydia_tests_urog_14,
positive_chlamydia_tests_urog_15,
positive_chlamydia_tests_urog_16,
positive_chlamydia_tests_urog_17,
chlamydia_cases_06,
chlamydia_cases_07,
chlamydia_cases_08,
chlamydia_cases_09,
chlamydia_cases_10,
chlamydia_cases_11,
chlamydia_cases_12,
chlamydia_cases_13,
chlamydia_cases_14,
chlamydia_cases_15,
chlamydia_cases_16,
chlamydia_cases_17,
total_syphilis_tests_06,
total_syphilis_tests_07,
total_syphilis_tests_08,
total_syphilis_tests_09,
total_syphilis_tests_10,
total_syphilis_tests_11,
total_syphilis_tests_12,
total_syphilis_tests_13,
total_syphilis_tests_14,
total_syphilis_tests_15,
total_syphilis_tests_16,
total_syphilis_tests_17,
syphilis_cases_06,
syphilis_cases_07,
syphilis_cases_08,
syphilis_cases_09,
syphilis_cases_10,
syphilis_cases_11,
syphilis_cases_12,
syphilis_cases_13,
syphilis_cases_14,
syphilis_cases_15,
syphilis_cases_16,
syphilis_cases_17,
t_sti_enc_py_cases_06,
t_sti_enc_py_cases_07,
t_sti_enc_py_cases_08,
t_sti_enc_py_cases_09,
t_sti_enc_py_cases_10,
t_sti_enc_py_cases_11,
t_sti_enc_py_cases_12,
t_sti_enc_py_cases_13,
t_sti_enc_py_cases_14,
t_sti_enc_py_cases_15,
t_sti_enc_py_cases_16,
t_sti_enc_py_cases_17,
indexsti_2006,
indexsti_month_06,
indexsti_2007,
indexsti_month_07,
indexsti_2008,
indexsti_month_08,
indexsti_2009,
indexsti_month_09,
indexsti_2010,
indexsti_month_10,
indexsti_2011,
indexsti_month_11,
indexsti_2012,
indexsti_month_12,
indexsti_2013,
indexsti_month_13,
indexsti_2014,
indexsti_month_14,
indexsti_2015,
indexsti_month_15,
indexsti_2016,
indexsti_month_16,
indexsti_2017,
indexsti_month_17,
secsti_index2006,
secsti_1to2_days_index2006,
secsti_index2007,
secsti_1to2_days_index2007,
secsti_index2008,
secsti_1to2_days_index2008,
secsti_index2009,
secsti_1to2_days_index2009,
secsti_index2010,
secsti_1to2_days_index2010,
secsti_index2011,
secsti_1to2_days_index2011,
secsti_index2012,
secsti_1to2_days_index2012,
secsti_index2013,
secsti_1to2_days_index2013,
secsti_index2014,
secsti_1to2_days_index2014,
secsti_index2015,
secsti_1to2_days_index2015,
secsti_index2016,
secsti_1to2_days_index2016,
secsti_index2017,
secsti_1to2_days_index2017,
thirdsti_index2006,
thirdsti_2to3_days_index2006,
thirdsti_index2007,
thirdsti_2to3_days_index2007,
thirdsti_index2008,
thirdsti_2to3_days_index2008,
thirdsti_index2009,
thirdsti_2to3_days_index2009,
thirdsti_index2010,
thirdsti_2to3_days_index2010,
thirdsti_index2011,
thirdsti_2to3_days_index2011,
thirdsti_index2012,
thirdsti_2to3_days_index2012,
thirdsti_index2013,
thirdsti_2to3_days_index2013,
thirdsti_index2014,
thirdsti_2to3_days_index2014,
thirdsti_index2015,
thirdsti_2to3_days_index2015,
thirdsti_index2016,
thirdsti_2to3_days_index2016,
thirdsti_index2017,
thirdsti_2to3_days_index2017,
fourthsti_index2006,
fourthsti_3to4_days_index2006,
fourthsti_index2007,
fourthsti_3to4_days_index2007,
fourthsti_index2008,
fourthsti_3to4_days_index2008,
fourthsti_index2009,
fourthsti_3to4_days_index2009,
fourthsti_index2010,
fourthsti_3to4_days_index2010,
fourthsti_index2011,
fourthsti_3to4_days_index2011,
fourthsti_index2012,
fourthsti_3to4_days_index2012,
fourthsti_index2013,
fourthsti_3to4_days_index2013,
fourthsti_index2014,
fourthsti_3to4_days_index2014,
fourthsti_index2015,
fourthsti_3to4_days_index2015,
fourthsti_index2016,
fourthsti_3to4_days_index2016,
fourthsti_index2017,
fourthsti_3to4_days_index2017,
ser_testing_for_lgv_ever,
anal_cytology_test_ever,
total_hcv_antibody_tests_06,
total_hcv_antibody_tests_07,
total_hcv_antibody_tests_08,
total_hcv_antibody_tests_09,
total_hcv_antibody_tests_10,
total_hcv_antibody_tests_11,
total_hcv_antibody_tests_12,
total_hcv_antibody_tests_13,
total_hcv_antibody_tests_14,
total_hcv_antibody_tests_15,
total_hcv_antibody_tests_16,
total_hcv_antibody_tests_17,
total_hcv_rna_tests_06,
total_hcv_rna_tests_07,
total_hcv_rna_tests_08,
total_hcv_rna_tests_09,
total_hcv_rna_tests_10,
total_hcv_rna_tests_11,
total_hcv_rna_tests_12,
total_hcv_rna_tests_13,
total_hcv_rna_tests_14,
total_hcv_rna_tests_15,
total_hcv_rna_tests_16,
total_hcv_rna_tests_17,
hcv_antibody_or_rna_positive,
hcv_antibody_first_pos_year,
hcv_rna_first_pos_year,
acute_hepc_per_esp,
acute_hepc_per_esp_diagnosis_year,
total_hepb_surface_antigen_tests_06,
total_hepb_surface_antigen_tests_07,
total_hepb_surface_antigen_tests_08,
total_hepb_surface_antigen_tests_09,
total_hepb_surface_antigen_tests_10,
total_hepb_surface_antigen_tests_11,
total_hepb_surface_antigen_tests_12,
total_hepb_surface_antigen_tests_13,
total_hepb_surface_antigen_tests_14,
total_hepb_surface_antigen_tests_15,
total_hepb_surface_antigen_tests_16,
total_hepb_surface_antigen_tests_17,
total_hepb_dna_tests_06,
total_hepb_dna_tests_07,
total_hepb_dna_tests_08,
total_hepb_dna_tests_09,
total_hepb_dna_tests_10,
total_hepb_dna_tests_11,
total_hepb_dna_tests_12,
total_hepb_dna_tests_13,
total_hepb_dna_tests_14,
total_hepb_dna_tests_15,
total_hepb_dna_tests_16,
total_hepb_dna_tests_17,
hepb_antigen_or_dna_positive,
hepb_antigen_or_dna_first_positive_year,
acute_hepb_per_esp,
acute_hepb_per_esp_year,
hist_of_hep_b_ever,
t_hiv_elisa_06,
t_hiv_elisa_07,
t_hiv_elisa_08,
t_hiv_elisa_09,
t_hiv_elisa_10,
t_hiv_elisa_11,
t_hiv_elisa_12,
t_hiv_elisa_13,
t_hiv_elisa_14,
t_hiv_elisa_15,
t_hiv_elisa_16,
t_hiv_elisa_17,
t_hiv_wb_06,
t_hiv_wb_07,
t_hiv_wb_08,
t_hiv_wb_09,
t_hiv_wb_10,
t_hiv_wb_11,
t_hiv_wb_12,
t_hiv_wb_13,
t_hiv_wb_14,
t_hiv_wb_15,
t_hiv_wb_16,
t_hiv_wb_17,
t_hiv_rna_06,
t_hiv_rna_07,
t_hiv_rna_08,
t_hiv_rna_09,
t_hiv_rna_10,
t_hiv_rna_11,
t_hiv_rna_12,
t_hiv_rna_13,
t_hiv_rna_14,
t_hiv_rna_15,
t_hiv_rna_16,
t_hiv_rna_17,
hiv_max_viral_load_year,
t_hiv_agab_06,
t_hiv_agab_07,
t_hiv_agab_08,
t_hiv_agab_09,
t_hiv_agab_10,
t_hiv_agab_11,
t_hiv_agab_12,
t_hiv_agab_13,
t_hiv_agab_14,
t_hiv_agab_15,
t_hiv_agab_16,
t_hiv_agab_17,
hiv_per_esp_spec,
hiv_per_esp_first_year,
new_hiv_diagnosis,
hiv_neg_with_rna_test,
hiv_neg_first_rna_test_yr,
hiv_neg_with_meds,
hiv_neg_first_hiv_med_yr,
hiv_on_problem_list,
hiv_first_prob_date,
t_hiv_encounters_06,
t_hiv_encounters_07,
t_hiv_encounters_08,
t_hiv_encounters_09,
t_hiv_encounters_10,
t_hiv_encounters_11,
t_hiv_encounters_12,
t_hiv_encounters_13,
t_hiv_encounters_14,
t_hiv_encounters_15,
t_hiv_encounters_16,
t_hiv_encounters_17,
three_diff_hiv_med_06,
three_diff_hiv_med_07,
three_diff_hiv_med_08,
three_diff_hiv_med_09,
three_diff_hiv_med_10,
three_diff_hiv_med_11,
three_diff_hiv_med_12,
three_diff_hiv_med_13,
three_diff_hiv_med_14,
three_diff_hiv_med_15,
three_diff_hiv_med_16,
three_diff_hiv_med_17,
truvada_num_rx_06,
truvada_num_rx_07,
truvada_num_rx_08,
truvada_num_rx_09,
truvada_num_rx_10,
truvada_num_rx_11,
truvada_num_rx_12,
truvada_num_rx_13,
truvada_num_rx_14,
truvada_num_rx_15,
truvada_num_rx_16,
truvada_num_rx_17,
truvada_num_pills_06,
truvada_num_pills_07,
truvada_num_pills_08,
truvada_num_pills_09,
truvada_num_pills_10,
truvada_num_pills_11,
truvada_num_pills_12,
truvada_num_pills_13,
truvada_num_pills_14,
truvada_num_pills_15,
truvada_num_pills_16,
truvada_num_pills_17,
hiv_neg_truvada_06,
hiv_neg_truvada_07,
hiv_neg_truvada_08,
hiv_neg_truvada_09,
hiv_neg_truvada_10,
hiv_neg_truvada_11,
hiv_neg_truvada_12,
hiv_neg_truvada_13,
hiv_neg_truvada_14,
hiv_neg_truvada_15,
hiv_neg_truvada_16,
hiv_neg_truvada_17,
hiv_first_icd_year,
abnormal_anal_cytology,
anal_dysplasia,
anal_carcinoma_in_situ,
syphillis_of_any_site_or_stage_except_late,
anal_syphilis,
gonococcal_infection_of_anus_and_rectum,
gonococcal_pharyngitis,
chlamydial_infection_of_anus_and_recturm,
chlamydial_infection_of_pharynx,
lymphgranuloma_venereum,
chancroid,
granuloma_inguinale,
nongonococcal_urethritis,
herpes_simplex_w_complications,
genital_herpes,
anogenital_warts,
anorectal_ulcer,
unspecified_std,
pelvic_inflammatory_disease,
contact_with_or_exposure_to_venereal_disease,
high_risk_sexual_behavior,
hiv_counseling,
anorexia_nervosa,
bulimia_nervosa,
eating_disorder_nos,
gender_identity_disorders,
trans_sexualism,
counseling_for_child_sexual_abuse,
foreign_body_in_anus,
alcohol_dependence,
opioid_dependence,
sedative_hypnotic_or_anxiolytic_dependence,
cocaine_dependence,
amphetamine_dependence,
unspecified_drug_dependence,
depression,
rx_bicillin_06,
rx_bicillin_07,
rx_bicillin_08,
rx_bicillin_09,
rx_bicillin_10,
rx_bicillin_11,
rx_bicillin_12,
rx_bicillin_13,
rx_bicillin_14,
rx_bicillin_15,
rx_bicillin_16,
rx_bicillin_17,
rx_azithromycin_06,
rx_azithromycin_07,
rx_azithromycin_08,
rx_azithromycin_09,
rx_azithromycin_10,
rx_azithromycin_11,
rx_azithromycin_12,
rx_azithromycin_13,
rx_azithromycin_14,
rx_azithromycin_15,
rx_azithromycin_16,
rx_azithromycin_17,
rx_ceftriaxone_06,
rx_ceftriaxone_07,
rx_ceftriaxone_08,
rx_ceftriaxone_09,
rx_ceftriaxone_10,
rx_ceftriaxone_11,
rx_ceftriaxone_12,
rx_ceftriaxone_13,
rx_ceftriaxone_14,
rx_ceftriaxone_15,
rx_ceftriaxone_16,
rx_ceftriaxone_17,
rx_methadone_06,
rx_methadone_07,
rx_methadone_08,
rx_methadone_09,
rx_methadone_10,
rx_methadone_11,
rx_methadone_12,
rx_methadone_13,
rx_methadone_14,
rx_methadone_15,
rx_methadone_16,
rx_methadone_17,
rx_suboxone_06,
rx_suboxone_07,
rx_suboxone_08,
rx_suboxone_09,
rx_suboxone_10,
rx_suboxone_11,
rx_suboxone_12,
rx_suboxone_13,
rx_suboxone_14,
rx_suboxone_15,
rx_suboxone_16,
rx_suboxone_17,
rx_viagara_cilais_or_levitra_06,
rx_viagara_cilais_or_levitra_07,
rx_viagara_cilais_or_levitra_08,
rx_viagara_cilais_or_levitra_09,
rx_viagara_cilais_or_levitra_10,
rx_viagara_cilais_or_levitra_11,
rx_viagara_cilais_or_levitra_12,
rx_viagara_cilais_or_levitra_13,
rx_viagara_cilais_or_levitra_14,
rx_viagara_cilais_or_levitra_15,
rx_viagara_cilais_or_levitra_16,
rx_viagara_cilais_or_levitra_17
FROM stiprep_output_with_patient;




-- CREATE TABLE stiprep_report_final_output_agetestpat_filter AS
-- SELECT * FROM stiprep_report_final_output
-- WHERE age >= 15 
-- AND master_patient_id not in 
-- (4856539,
-- 2388755,
-- 20021289,
-- 2622639,
-- 7393799,
-- 7405902,
-- 19086,
-- 2245972,
-- 9345414,
-- 1121717,
-- 1120095,
-- 5092053,
-- 1119941,
-- 15810936,
-- 2801547,
-- 7395105,
-- 6808599,
-- 467766,
-- 465618,
-- 9345404,
-- 24120559,
-- 20257460,
-- 6840308,
-- 467872,
-- 4512833,
-- 454136,
-- 8429720,
-- 8000774,
-- 377556,
-- 456070,
-- 8012787,
-- 7441382,
-- 7997968,
-- 3268327,
-- 3268319,
-- 3268323,
-- 8726972,
-- 9645683,
-- 456988,
-- 7395153,
-- 4476409,
-- 1400172,
-- 269278,
-- 5044824,
-- 5092208,
-- 269284,
-- 269622,
-- 269290,
-- 5689686,
-- 5651871,
-- 269282,
-- 269276,
-- 5042048,
-- 6272750,
-- 299112,
-- 269286,
-- 269288,
-- 5044808,
-- 291608,
-- 2733840,
-- 269280,
-- 4476411,
-- 8425284,
-- 7394534,
-- 2733842,
-- 5092285,
-- 7394532,
-- 3906359,
-- 1629211,
-- 7394546,
-- 7394548,
-- 5638755,
-- 3069021,
-- 7394544,
-- 16697245,
-- 16697270,
-- 16700804,
-- 16803981);

--
-- Script shutdown section 
--

