--set client_min_messages to error;

DROP TABLE IF EXISTS hiv_rpt.hrp_hist_coefficients;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_index_patients;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_labs;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_lab_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_labs_elisa_or_ag_ab;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_elisa_or_agab_counts;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_gon_chlam_events;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_gon_chlam_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_diags;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_diags_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_count_exlusions;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_subset;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_subset_2;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_syph_events;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_syph_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_events;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_b_value;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_meds;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_soc_hist_sa;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_soc_hist_bcm;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hsv_labs;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hsv_labs_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_meds_of_int;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_meds_of_int_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_opiate_labs;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_opiate_lab_values;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_prep;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_pat_sum_x_value;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_index_data;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_risk_scores;
DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_risk_score_linelist;


CREATE TABLE hiv_rpt.hrp_hist_coefficients AS
SELECT 
-5.79843163536551::double precision intercept,
-0.01640540141538::double precision current_age,
2.01326024472746::double precision gender_male,
1.90646315593039::double precision gender_trans,
1.2164060627115::double precision gender_unk,
-0.0612596796521859::double precision marital_stat_married,
0.919868723112714::double precision marital_stat_partner,
0.176048157550863::double precision marital_stat_single,
-0.235027851011016::double precision marital_stat_unk,
0.342170147818618::double precision marital_stat_widow,
2.74070869965095::double precision hbsag_or_dna_pos_ever,
-9.76423180807119::double precision hiv_neg_with_rna_test_ever,
-2.66403249612724::double precision hiv_neg_with_hiv_meds_ever,
-0.0682183367160148::double precision sexually_active,
-0.889466730165689::double precision bcm_pill,
0.292032663787863::double precision t_gon_pos_1yr,
0.105220513695572::double precision t_gon_throat_1yr,
0.055820926388355::double precision t_chlam_1yr,
-0.0218627473532249::double precision t_chlam_pos_1yr,
0.552974126200101::double precision t_syph_1yr,
-0.0546933382631432::double precision t_syph_pos_1yr,
-0.797470849500461::double precision herpes_direct_pos_ever,
-0.602882645677963::double precision herpes_serology_1yr,
-0.549083907525474::double precision herpes_serology_pos_ever,
3.8230086149483::double precision t_hiv_1yr,
-2.39224247785902::double precision t_hiv_rna_viral_1yr,
1.03098326429326::double precision bicillin_rx_1yr,
-0.0883744353769024::double precision azith_rx_1yr,
0.212194134695708::double precision ceft_rx_1yr,
-4.60084019353106::double precision t_hiv_agab_or_elisa_1yr,
0.736719437071412::double precision syph_dx_2yr,
-0.774885562065857::double precision hepc_ab_pos_ever,
-1.71527474409782::double precision nongon_ureth_dx_ever,
-0.194661144327166::double precision herpes_simplex_dx_ever,
-0.200308575527786::double precision genital_herpes_dx_ever,
-0.719170311768286::double precision anogenital_warts_dx_ever,
-1.66623011109125::double precision high_risk_sex_dx_ever,
-2.08929938947073::double precision hepb_dx_or_pos_lab_ever,
0.508652924977738::double precision gonoc_inf_anus_rect_dx_1yr,
-0.261295948242094::double precision gonoc_pharyn_dx_ever,
-5.97949779287643::double precision gonoc_pharyn_dx_2yr,
-0.665361118329087::double precision chlam_infec_of_pharynx_dx_ever,
-1.11288873642458::double precision unspecified_std_dx_ever,
-0.915181995956318::double precision contact_exposure_vd_dx_ever,
-0.667874389466176::double precision alcohol_depen_abuse_dx_ever,
1.05708943050798::double precision child_sex_abuse_dx_ever,
-0.113720670657452::double precision cannabis_abuse_dx_ever,
-0.183771540775511::double precision emergency_contracept_dx_ever,
-1.38941012158306::double precision enc_hiv_screen_dx_ever,
-2.39845497377153::double precision opiate_pos_ever;


-- DELETE ALL EXISTING ENTRIES FOR YEAR BEING COMPUTED
DELETE FROM gen_pop_tools.cc_hiv_risk_score where rpt_year =  (EXTRACT(YEAR FROM :end_calc_year::date))::text;


-- ONLY COMPUTING FOR PATIENTS THAT HAD AN ENCOUNTER IN THE 2 YEARS PRIOR TO RUN YEAR
CREATE TABLE hiv_rpt.hrp_hist_index_patients AS
SELECT T1.id as patient_id,
(EXTRACT(YEAR FROM :end_calc_year::date))::text rpt_year,
date_part('year',age(:end_calc_year::date, date_of_birth)) current_age,
CASE WHEN upper(gender) in ('M', 'MALE') THEN 1 ELSE 0 END gender_male,
CASE WHEN upper(gender) in ('T') THEN 1 ELSE 0 END gender_trans,
CASE WHEN upper(gender) in ('U', 'UNKNOWN', '') OR gender is null THEN 1 ELSE 0 END gender_unk,
CASE WHEN upper(marital_stat) in ('MARRIED') THEN 1 ELSE 0 END marital_stat_married,
CASE WHEN upper(marital_stat) in ('PARTNER') THEN 1 ELSE 0 END marital_stat_partner,
CASE WHEN upper(marital_stat) in ('SINGLE') THEN 1 ELSE 0 END marital_stat_single,
CASE WHEN upper(marital_stat) in ('U', 'UNKNOWN', '') OR marital_stat is null THEN 1 ELSE 0 END marital_stat_unk,
CASE WHEN upper(marital_stat) in ('WIDOWED') THEN 1 ELSE 0 END marital_stat_widow
FROM emr_patient T1
JOIN emr_encounter T2 on (T1.id = T2.patient_id) 
-- must have an encounter in the past 2 years
AND T2.date >= (:end_calc_year::date - interval '2 years')
AND T2.date <= :end_calc_year::date
-- must be at least age 15
AND date_part('year', age(:end_calc_year::date, date_of_birth)) >= 18
-- don't compute for known hiv patients
AND T1.id NOT IN (select patient_id from nodis_case where condition = 'hiv' and date <= :end_calc_year)
-- filter out test patients
GROUP BY T1.id, rpt_year, gender, marital_stat;



-- Gather up all of the HIV lab tests
-- NEED TO LIMIT TO THESE TEST TYPES TO ELISA, HIV_RNA_VIRAL, HIV AG/AB
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH A HEF EVENT OR ELSE YOU WILL MISS VIRAL LOAD TESTS
CREATE TABLE hiv_rpt.hrp_hist_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, EXTRACT(YEAR FROM date) rpt_year, abs(date_part('year', age(T1.date, :end_calc_year))) yrs_since_test, T1.date, collection_date::date,
CASE WHEN test_name = 'hiv_elisa' or test_name = 'hiv_ag_ab' THEN 1 END hiv_elisa_or_agab
FROM emr_labresult T1 
INNER JOIN conf_labtestmap T2 ON ((T1.native_code = T2.native_code)) 
INNER JOIN hiv_rpt.hrp_hist_index_patients T3 ON (T1.patient_id = T3.patient_id)
WHERE test_name ilike 'hiv_%' 
AND test_name != 'hiv not a test'
AND test_name != 'cii_hiv_donotmap'
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.', 'NO FROZEN SPECIMEN RECEIVED', 'NO FROZEN SPECIMEN RECIVED', 'NO SPECIMEN FOUND', 'no specimen received', 'NO SPECIMEN RECEIVED', 'NO SPECIMEN RECEIVED PER LABCORP', 'NOSR', 'NSR', 'Test Not Performed', 'wrong lab ordered', 'No Specimen Request Received', 'NO SPECIMEN REQUEST RECEIVED', 'Test not Performed')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|DISREGARD|INCORRECTLY|TNP|NOT PROCESSED')
AND result_string is not null
AND date <= :end_calc_year
GROUP BY test_name, T1.patient_id, rpt_year, yrs_since_test, date, collection_date;

-- Compute HIV test values
CREATE TABLE hiv_rpt.hrp_hist_hiv_lab_values AS
SELECT
patient_id,
max(CASE WHEN test_name = 'hiv_rna_viral' THEN 1 ELSE 0 END) hiv_neg_with_rna_test_ever,
COUNT(CASE WHEN yrs_since_test = 0 THEN 1 END) t_hiv_1yr,
COUNT(CASE WHEN yrs_since_test = 0 AND test_name = 'hiv_rna_viral' THEN 1 END) t_hiv_rna_viral_1yr
FROM hiv_rpt.hrp_hist_hiv_labs
GROUP BY patient_id;

-- HIV LABS IDENTIFY ELISA OR AG/AB ONLY ONE PER DATE
CREATE TABLE hiv_rpt.hrp_hist_hiv_labs_elisa_or_ag_ab AS 
SELECT patient_id, yrs_since_test, date, max(hiv_elisa_or_agab) hiv_elisa_agab_max
FROM hiv_rpt.hrp_hist_hiv_labs
GROUP BY patient_id, rpt_year, date, yrs_since_test;

-- COMPUTE COUNT BY TEST DATE IN THE LAST YEAR
CREATE TABLE hiv_rpt.hrp_hist_hiv_elisa_or_agab_counts AS
SELECT patient_id,
COUNT(CASE WHEN yrs_since_test = 0 THEN 1 END) t_hiv_agab_or_elisa_1yr
FROM hiv_rpt.hrp_hist_hiv_labs_elisa_or_ag_ab 
GROUP BY patient_id;


-- Gather up labs hef events for chlamydia and gonorrhea for the last year
-- Having multiple results on the same date is acceptable here as tests are done for multiple specimen sources and we want to capture both
CREATE TABLE hiv_rpt.hrp_hist_gon_chlam_events AS 
SELECT name, T1.date, test_name, T2.patient_id, object_id, EXTRACT(YEAR FROM T1.date) rpt_year, abs(date_part('year', age(T1.date, :end_calc_year))) yrs_since_test, T3.native_code,
CASE WHEN T3.native_code ~* '(PHARYN|THROAT)' then 'THROAT' 
     WHEN T3.native_name ~* '(PHARYN|THROAT)' then 'THROAT'
	 ELSE specimen_source END specimen_source 
FROM hef_event T1
INNER JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_labresult T3 ON (T1.object_id = T3.id and T1.patient_id = T3.patient_id)
INNER JOIN conf_labtestmap T4 ON (T3.native_code = T4.native_code)
WHERE test_name in ('gonorrhea', 'chlamydia')
AND T1.date >= (:end_calc_year::date - interval '1 year')
AND T1.date <= :end_calc_year;


-- Compute Chlamydia and Gonorrhea values
CREATE TABLE hiv_rpt.hrp_hist_gon_chlam_values AS
SELECT 
patient_id,
COUNT(CASE WHEN name = 'lx:gonorrhea:positive' AND yrs_since_test = 0 THEN 1 END) t_gon_pos_1yr,
COUNT(CASE WHEN test_name = 'gonorrhea' AND upper(specimen_source) ~* '^(THROAT|ORAL|OROPHARYNGEAL|PHARYNGEAL|PN |TH |NASOPHARYNGEAL|OTHER ORAL SWAB|PHARYNX)' AND yrs_since_test = 0 THEN 1 END) t_gon_throat_1yr,
COUNT(CASE WHEN test_name = 'chlamydia' AND yrs_since_test = 0 THEN 1 END) t_chlam_1yr,
COUNT(CASE WHEN name = 'lx:chlamydia:positive' AND yrs_since_test = 0 THEN 1 END) t_chlam_pos_1yr
FROM hiv_rpt.hrp_hist_gon_chlam_events
GROUP BY patient_id;

-- Gather up diagnosis codes of interest
CREATE TABLE hiv_rpt.hrp_hist_diags AS 
SELECT T2.patient_id, T1.id, T1.encounter_id, T1.dx_code_id, abs(date_part('year', age(T2.date, :end_calc_year))) yrs_since_diag
FROM emr_encounter_dx_codes T1 
INNER JOIN emr_encounter T2 on (T1.encounter_id = T2.id)
INNER JOIN hiv_rpt.hrp_hist_index_patients T3 ON (T2.patient_id = T3.patient_id)
WHERE 
dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.|icd9:099.4|icd9:054.1|icd10:A60.0|icd10:Z72.5|icd9:070.2|icd9:070.3|icd10:B18.0|icd10:B18.1|icd10:B19.1|icd10:A63.|icd9:305.0|icd10:F10.1|icd10:T74.22|icd9:305.2|icd10:F12.1|icd10:T76.22)' 
OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd10:N34.1', 'icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9',
'icd9:078.11', 'icd10:A63.0', 'icd9:V69.2', 'icd9:098.7','icd10:A54.6', 'icd9:098.6','icd10:A54.5', 'icd10:A64', 'icd9:099.8', 'icd9:099.9', 'icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6', 'icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2', 'icd9:995.53', 'icd10:T76.22', 'icd10:Z11.4', 'icd9:099.51','icd10:A56.4', 'icd9:V25.03', 'icd10:Z30.012')
AND T2.date <= :end_calc_year;


CREATE TABLE hiv_rpt.hrp_hist_diags_values AS
SELECT T1.patient_id,
MAX(CASE WHEN (dx_code_id ~  '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' or dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1')) AND yrs_since_diag <= 1 then 1 end) syph_dx_2yr,
MAX(CASE WHEN dx_code_id ilike 'icd9:099.4%' or dx_code_id in ('icd10:N34.1') then 1 end) nongon_ureth_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:054.8', 'icd9:054.9', 'icd9:054.79', 'icd10:A60.1','icd10:A60.9', 'icd10:B00.9') THEN 1 END) herpes_simplex_dx_ever,
MAX(CASE WHEN dx_code_id ~ '^(icd9:054.1|icd10:A60.0)' THEN 1 END) genital_herpes_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:078.11', 'icd10:A63.0') THEN 1 END) anogenital_warts_dx_ever,
MAX(CASE WHEN dx_code_id ~ '^(icd10:Z72.5)' or dx_code_id in ('icd9:V69.2') THEN 1 END) high_risk_sex_dx_ever,
MAX(CASE WHEN dx_code_id ~ '^(icd9:070.2|icd9:070.3|icd10:B18.0|icd10:B18.1|icd10:B19.1)' THEN 1 END) hist_of_hepb_dx,
MAX(CASE WHEN dx_code_id in ('icd9:098.7','icd10:A54.6') AND yrs_since_diag = 0 THEN 1 END) gonoc_inf_anus_rect_dx_1yr,
MAX(CASE WHEN dx_code_id in ('icd9:098.6','icd10:A54.5') THEN 1 END) gonoc_pharyn_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:098.6','icd10:A54.5') AND yrs_since_diag <= 1 THEN 1 END) gonoc_pharyn_dx_2yr,
MAX(CASE WHEN dx_code_id in ('icd9:099.51','icd10:A56.4') THEN 1 END) chlam_infec_of_pharynx_dx_ever,
MAX(CASE WHEN dx_code_id ilike 'icd10:A63.%' or dx_code_id in ('icd10:A64', 'icd9:099.8', 'icd9:099.9') THEN 1 END) unspecified_std_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:V01.6', 'icd10:Z20.2', 'icd10:Z20.6') THEN 1 END) contact_exposure_vd_dx_ever,
MAX(CASE WHEN dx_code_id ~ '^(icd9:305.0|icd10:F10.1)' or dx_code_id in ('icd9:303.90', 'icd9:303.91', 'icd9:303.92', 'icd10:F10.2') THEN 1 END) alcohol_depen_abuse_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:995.53') or dx_code_id ~ '^(icd10:T74.22|icd10:T76.22)' THEN 1 END) child_sex_abuse_dx_ever,
MAX(CASE WHEN dx_code_id ~ '^(icd9:305.2|icd10:F12.1)' THEN 1 END) cannabis_abuse_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd9:V25.03', 'icd10:Z30.012') THEN 1 END) emergency_contracept_dx_ever,
MAX(CASE WHEN dx_code_id in ('icd10:Z11.4') THEN 1 END) enc_hiv_screen_dx_ever
FROM hiv_rpt.hrp_hist_diags T1
GROUP BY patient_id;

--
-- Bicillin RX
-- Only need RX in the last year
--
CREATE TABLE hiv_rpt.hrp_hist_bicillin AS
SELECT count(*) as rx_count, T1.patient_id, name, date, dose, abs(date_part('year', age(date, :end_calc_year))) yrs_since_rx
FROM emr_prescription T1
INNER JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE ( 
( name in ('Bicillin L-A 1,200,000 unit/2 mL intramuscular syringe') and dose in ('2.000000 mL', '2.000000 unit(s)', '240000.000000 unit(s)', '2.400000 mg', '2.400000 million units', '2.400000 unit(s)', '4.000000 mL') )
OR 
name in ('Bicillin L-A 2,400,000 unit/4 mL intramuscular syringe', 'penicillin G benzathine 2,400,000 unit/4 mL intramuscular syringe')
)
AND upper(status) not in ('CANCELLED')
AND date >= (:end_calc_year::date - interval '1 year')
AND date <= :end_calc_year
GROUP BY T1.patient_id, name, date, dose;


-- Exclude bicillin prescriptions that match specific dx codes 
-- occurring on the same date
-- Only include 1 rx per date  04/2022
-- Added icd10:O99.82|icd10:Z22.330 as exclusions 04/2022
-- Don't exclude if syph diag on the same day PLUS 098.12 and Z20.2 04/2022 04/2022
CREATE TABLE hiv_rpt.hrp_hist_bicillin_subset AS
SELECT patient_id, date, yrs_since_rx 
FROM hiv_rpt.hrp_hist_bicillin
EXCEPT
SELECT T1.patient_id, T1.date, T1.yrs_since_rx
FROM hiv_rpt.hrp_hist_bicillin T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
WHERE T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5|icd10:O99.82|icd10:Z22.330'
and T1.date = T2.date
-- if patient has syph dx on same date, don't exclude
AND T1.patient_id not in (SELECT T1.patient_id
                          FROM hiv_rpt.hrp_hist_bicillin T1
                          INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
                          INNER JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
                          WHERE (T3.dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
		                         OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1', 'icd9:098.12', 'icd10:Z20.2'))
                          AND T1.date = T2.date);
	

-- COMMENTING OUT BECAUSE I DON'T KNOW IF THEY WANT THIS OR NOT AND ISSUES WITH ICD CODES
/* -- Exclude bicillin prescriptions for patients that have never had
-- a POSITIVE syphilis test AND have never had a "standard" syphilis dx code 04/2022
CREATE TABLE hiv_rpt.hrp_hist_bicillin_subset_2 AS
SELECT patient_id, date, yrs_since_rx 
FROM  hiv_rpt.hrp_hist_bicillin_subset
EXCEPT
SELECT T1.patient_id, T1.date, T1.yrs_since_rx
FROM hiv_rpt.hrp_hist_bicillin_subset T1
WHERE patient_id NOT IN (
		SELECT patient_id from hef_event 
		WHERE name in ('lx:fta-abs-csf:positive', 'lx:fta-abs:positive', 'lx:rpr:positive', 'lx:tp-cia:positive', 'lx:tp-igg:positive', 'lx:tppa-csf:positive', 'lx:tppa:positive', 'lx:vdrl-csf:positive') )
AND patient_id NOT in (
		SELECT T1.patient_id
		FROM hiv_rpt.hrp_hist_bicillin_subset T1
	    INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
		INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
		INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
		WHERE dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
		OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1'))
order by patient_id; */


CREATE TABLE hiv_rpt.hrp_hist_bicillin_values AS
SELECT T1.patient_id,
MAX(CASE WHEN yrs_since_rx = 0 THEN 1 END) bicillin_rx_1yr
--FROM hiv_rpt.hrp_hist_bicillin_subset_2 T1
FROM hiv_rpt.hrp_hist_bicillin_subset T1
GROUP BY patient_id;


-- Gather up all syphilis related tests
-- NEED TO GRAB ALL TEST NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
-- JUST NEED THE LAST 1 YEAR FOR VARIABLES
CREATE TABLE hiv_rpt.hrp_hist_syph_events AS 
SELECT T1.patient_id, test_name, T3.name as hef_name, T1.date, abs(date_part('year', age(T1.date, :end_calc_year))) yrs_since_test
--, result_string, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hiv_rpt.hrp_hist_index_patients T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date <= :end_calc_year
AND T1.date >= (:end_calc_year::date - interval '1 year')
AND T2.test_name in ('syphilis_rpr_8', 'rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm', 'tp-cia')
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.', 'NO FROZEN SPECIMEN RECEIVED', 'NO FROZEN SPECIMEN RECIVED', 'NO SPECIMEN FOUND', 'no specimen received', 'NO SPECIMEN RECEIVED', 'NO SPECIMEN RECEIVED PER LABCORP', 'NOSR', 'NSR', 'Test Not Performed', 'wrong lab ordered', 'No Specimen Request Received', 'NO SPECIMEN REQUEST RECEIVED', 'Test not Performed')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|QUANTITY NOT SUFFICIENT|NOT PROCESSED')
and result_string is not null
GROUP BY T1.patient_id, test_name, hef_name, T1.date, yrs_since_test;

--
--NEW TABLES START HERE
--

CREATE TABLE hiv_rpt.hrp_hist_syph_values AS
SELECT patient_id,
COUNT(CASE WHEN yrs_since_test = 0 THEN 1 END) t_syph_1yr,
COUNT(CASE WHEN yrs_since_test = 0 AND hef_name ilike '%positive%' THEN 1 END) t_syph_pos_1yr
FROM hiv_rpt.hrp_hist_syph_events
GROUP BY patient_id;

--
-- HEP Labs Of Interest
--
CREATE TABLE hiv_rpt.hrp_hist_hep_events AS 
SELECT T1.patient_id, test_name, T3.name as hef_name, T1.date, abs(date_part('year', age(T1.date, :end_calc_year))) yrs_since_test
--, result_string, concat(test_name, ':' , result_string) as result_data
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hiv_rpt.hrp_hist_index_patients T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date <= :end_calc_year
AND T2.test_name in ('hepatitis_b_viral_dna', 'hepatitis_b_surface_antigen', 'hepatitis_c_elisa') 
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.', 'NO FROZEN SPECIMEN RECEIVED', 'NO FROZEN SPECIMEN RECIVED', 'NO SPECIMEN FOUND', 'no specimen received', 'NO SPECIMEN RECEIVED', 'NO SPECIMEN RECEIVED PER LABCORP', 'NOSR', 'NSR', 'Test Not Performed', 'wrong lab ordered', 'No Specimen Request Received', 'NO SPECIMEN REQUEST RECEIVED', 'Test not Performed')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|QUANTITY NOT SUFFICIENT|NOT PROCESSED')
and result_string is not null
GROUP BY T1.patient_id, test_name, hef_name, T1.date, yrs_since_test;

CREATE TABLE hiv_rpt.hrp_hist_hep_values AS 
SELECT patient_id,
MAX(CASE WHEN hef_name in ('lx:hepatitis_b_surface_antigen:positive', 'lx:hepatitis_b_viral_dna:positive') THEN 1 END) hbsag_or_dna_pos_ever,
MAX(CASE WHEN hef_name in ('lx:hepatitis_c_elisa:positive') THEN 1 END) hepc_ab_pos_ever
FROM hiv_rpt.hrp_hist_hep_events
GROUP BY patient_id;

--
-- HEP B If either lab pos or dx
--
CREATE TABLE hiv_rpt.hrp_hist_hep_b_value AS
SELECT DISTINCT patient_id, 1::int as hepb_dx_or_pos_lab_ever
FROM hiv_rpt.hrp_hist_hep_values
WHERE hbsag_or_dna_pos_ever = 1
UNION
SELECT patient_id, 1::int as hepb_dx_or_pos_lab_ever
FROM hiv_rpt.hrp_hist_diags_values 
WHERE hist_of_hepb_dx = 1;


-- 
-- HIV Meds
--

CREATE TABLE hiv_rpt.hrp_hist_hiv_meds AS
SELECT T1.patient_id,
1 as hiv_neg_with_hiv_meds_ever
FROM hef_event T1 
INNER JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.name ilike 'rx:hiv_%'
AND T1.date <= :end_calc_year
AND T1.patient_id not in (select patient_id from nodis_case where condition = 'hiv' and date <= :end_calc_year)
GROUP BY T1.patient_id;



--
-- SOCIAL HISTORY
--

CREATE TABLE hiv_rpt.hrp_hist_soc_hist_sa AS
SELECT T1.patient_id, 1::int sexually_active
FROM 
    (SELECT T1.patient_id, max(date) sexually_active_last_date 
		 FROM emr_socialhistory T1
		 JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
		 WHERE upper(sexually_active) in ('Y', 'YES')
		 AND T1.date >= (:end_calc_year::date - interval '1 year')
		 AND T1.date <= :end_calc_year::date
		 GROUP BY T1.patient_id) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T2.date = T1.sexually_active_last_date)
	GROUP BY T1.patient_id;
	
CREATE TABLE hiv_rpt.hrp_hist_soc_hist_bcm AS
SELECT T1.patient_id, 1::int bcm_pill
FROM 
    (SELECT T1.patient_id, max(date) bcm_pill_last_date 
		 FROM emr_socialhistory T1
		 JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
		 WHERE 
		 (birth_control_method ilike '%PILL%' 
          or birth_control_method ilike '%oral con%' 
		  or birth_control_method ilike '%patch%'
		  or birth_control_method ilike '%OCP%'
		  or birth_control_method in ('BCPs', 'Patch')
		  )
 		 AND T1.date >= (:end_calc_year::date - interval '1 year')
		 AND T1.date <= :end_calc_year::date
		 GROUP BY T1.patient_id) T1
	JOIN emr_socialhistory T2 on (T1.patient_id = T2.patient_id and T2.date = T1.bcm_pill_last_date)
	GROUP BY T1.patient_id;	
	

--
-- HSV Testing
--

CREATE TABLE hiv_rpt.hrp_hist_hsv_labs AS 
SELECT T1.patient_id, test_name, 
T1.date, abs(date_part('year', age(T1.date, :end_calc_year::date))) yrs_since_test
--, result_string--, concat(test_name, ':' , result_string) as result_data,
,CASE WHEN (result_string ILIKE ANY(ARRAY['pos%', 'det%', 'Antibody to%det%', 'Antibodes to both % detec', '%antibody%detected%', '%antibody%positive%', '>%', 'ISOLATED']) and result_string not ilike '%not det%') 
               OR (result_float >= 0.9 and result_string not ilike '%:%') then 'positive' 
     WHEN (result_string is null or result_string = '' ) and abnormal_flag is not null and upper(abnormal_flag) not in('', 'NORMAL')  then 'positive'
     ELSE NULL end as positive_test
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
INNER JOIN hiv_rpt.hrp_hist_index_patients T4 ON (T1.patient_id = T4.patient_id)
WHERE T1.date <= :end_calc_year::date
AND T2.test_name in ('hsv_serology', 'hsv_direct') 
AND result_string not in ('Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.', 'NO FROZEN SPECIMEN RECEIVED', 'NO FROZEN SPECIMEN RECIVED', 'NO SPECIMEN FOUND', 'no specimen received', 'NO SPECIMEN RECEIVED', 'NO SPECIMEN RECEIVED PER LABCORP', 'NOSR', 'NSR', 'Test Not Performed', 'wrong lab ordered', 'No Specimen Request Received', 'NO SPECIMEN REQUEST RECEIVED', 'Test not Performed')
AND upper(result_string) !~ ('CANCELLED|NOT PERFORMED|CREDITED|QUANTITY NOT SUFFICIENT|NOT PROCESSED')
--and result_string is not null
GROUP BY T1.patient_id, test_name, T1.date, yrs_since_test, positive_test;

CREATE TABLE hiv_rpt.hrp_hist_hsv_labs_values AS
SELECT patient_id,
MAX(CASE WHEN test_name = 'hsv_direct' AND positive_test = 'positive' then 1 END) herpes_direct_pos_ever,
MAX(CASE WHEN test_name = 'hsv_serology' AND yrs_since_test = 0 then 1 END) herpes_serology_1yr,
MAX(CASE WHEN test_name = 'hsv_serology' AND positive_test = 'positive' then 1 END) herpes_serology_pos_ever
FROM hiv_rpt.hrp_hist_hsv_labs
GROUP BY patient_id;


-- 
-- OTHER MEDS
--

CREATE TABLE hiv_rpt.hrp_hist_meds_of_int AS 
SELECT T1.patient_id,T1.name, abs(date_part('year', age(T1.date, :end_calc_year::date))) yrs_since_med,
max(CASE WHEN T1.name ilike '%AZITHROMYCIN%' or T1.name ilike '%ZITHROMAX%' then 1 end) rx_azithromycin,
max(CASE WHEN T1.name ilike '%ROCEPHIN%' or T1.name ilike '%CEFTRIAXONE%' then 1 end) rx_ceftriaxone
FROM emr_prescription T1 
INNER JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE
(
  name in ('azithromycin 1 gram oral packet', 'Zithromax 1 gram oral packet') 
OR 
 (name in ('azithromycin 200 mg/5 mL oral suspension', 'azithromycin 500 mg tablet', 'Zithromax 500 mg tablet') and dose in ('1000.000000 mg'))
OR 
 (name in ('azithromycin 500 mg tablet', 'Zithromax 500 mg tablet') and dose in ('2.000000 tablet(s)'))
OR
 (name in ('ceftriaxone 1 gram solution for injection') and dose in ('250.000000 mg', '500.000000 g', '500.000000 mg'))
OR
 name in ('ceftriaxone 250 mg solution for injection', 'ceftriaxone 500 mg solution for injection')
)
AND name not ilike '%ANTIHYPERTENSIVE%'
AND name not ilike '%HYPERTENSION%'
AND name not ilike '%adcirca%'
AND name not ilike '%alyq%'	
AND upper(status) not in ('CANCELLED')
AND T1.date >= (:end_calc_year::date - interval '1 year')
AND T1.date <= :end_calc_year::date
GROUP BY T1.patient_id, T1.name, T1.date;

CREATE TABLE hiv_rpt.hrp_hist_meds_of_int_values AS
SELECT patient_id,
MAX(CASE WHEN rx_azithromycin = 1 THEN 1 END) azith_rx_1yr,
MAX(CASE WHEN rx_ceftriaxone = 1 THEN 1 END) ceft_rx_1yr
FROM hiv_rpt.hrp_hist_meds_of_int
GROUP BY patient_id;

-- OPIATE TESTING
CREATE TABLE hiv_rpt.hrp_hist_opiate_labs AS
SELECT T1.patient_id, date, abs(date_part('year', age(T1.date, :end_calc_year::date))) yrs_since_test, result_float, result_string, native_name, native_code, procedure_name,
CASE WHEN result_string ilike '%pos%' or result_string ilike 'detec%' or result_string ilike '>%' or result_string ilike 'present' 
or (native_code = '--DRUG SCREEN, URINE' and result_string = 'positive' and comment ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%']) )
then 'positive' ELSE NULL end as positive_test,
comment
FROM emr_labresult T1
INNER JOIN hiv_rpt.hrp_hist_index_patients T2 ON (T1.patient_id = T2.patient_id)
WHERE (
 result_string ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
OR
 native_name ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
OR procedure_name ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%'])
OR (native_code = '--DRUG SCREEN, URINE' and comment ILIKE ANY(ARRAY['%MORPHINE%', '%CODEINE%', '%OXYCODONE%', '%HYDROCODONE%', '%HYDROMORPHONE%', '%PROPOXYPHENE%', '%OPIATE%', '%MEPERIDINE%', '%TRAMADOL%', '%TAPENTADOL%']) and comment not ilike '%no opiates%' )
)	   
AND result_string not in ('', 'Cancelled', 'declined', 'Declined','Not Done', 'Not performed', 'Not tested', 'NP', 'ordered in error', 'pt declined', 'QNS', 'Replaced',
'Test not performed', 'Test replaced', 'tnp', 'tnp', 'TNP %', 'TNP mg/dl', 'TNP U/L', 'TNP', 'TNP-REFLEX TESTING NOT REQUIRED.', 
'TNP-SUPPLEMENTAL TESTING NOT PERFORMED.', 'NO FROZEN SPECIMEN RECEIVED', 'NO FROZEN SPECIMEN RECIVED', 'NO SPECIMEN FOUND', 'no specimen received', 'NO SPECIMEN RECEIVED', 'NO SPECIMEN RECEIVED PER LABCORP', 'NOSR', 'NSR', 'Test Not Performed', 'wrong lab ordered', 'No Specimen Request Received', 'NO SPECIMEN REQUEST RECEIVED', 'Test not Performed', 'No Specimen Received', 'NO SPECIMAN RECIVED', '00000')
and result_string not ilike '%NO SPECIMEN RECEIVED%'
and result_string not ilike '%NOT PERFORMED%'
and result_string not ilike '%NOT DONE%'
and result_string not ilike '%No Specimen Found%'
and result_string not ilike '%SPECIMEN NOT FOUND%'
and result_string not ilike '%NO SERUM GEL RECEIVED%'
AND date <= :end_calc_year::date;


CREATE TABLE hiv_rpt.hrp_hist_opiate_lab_values AS
SELECT patient_id,
MAX(CASE WHEN positive_test = 'positive' then 1 END) opiate_pos_ever
FROM hiv_rpt.hrp_hist_opiate_labs
GROUP BY patient_id;


-- Bring together all of the fields and values
CREATE TABLE hiv_rpt.hrp_hist_index_data AS
SELECT T1.patient_id,
T1.rpt_year,
current_age,
coalesce(gender_male, 0) gender_male,
coalesce(gender_trans, 0) gender_trans,
coalesce(gender_unk, 0) gender_unk,
coalesce(marital_stat_married, 0) marital_stat_married,
coalesce(marital_stat_partner, 0) marital_stat_partner,
coalesce(marital_stat_single, 0) marital_stat_single,
coalesce(marital_stat_unk, 0) marital_stat_unk,
coalesce(marital_stat_widow, 0) marital_stat_widow,
coalesce(T2.t_hiv_agab_or_elisa_1yr, 0) t_hiv_agab_or_elisa_1yr,
coalesce(T3.hiv_neg_with_rna_test_ever, 0) hiv_neg_with_rna_test_ever,
coalesce(T3.t_hiv_1yr, 0) t_hiv_1yr,
coalesce(T3.t_hiv_rna_viral_1yr, 0) t_hiv_rna_viral_1yr,
coalesce(T4.bicillin_rx_1yr, 0) bicillin_rx_1yr,
coalesce(T5.hiv_neg_with_hiv_meds_ever, 0) hiv_neg_with_hiv_meds_ever,
coalesce(T6.t_gon_pos_1yr, 0) t_gon_pos_1yr,
coalesce(T6.t_gon_throat_1yr, 0) t_gon_throat_1yr,
coalesce(T6.t_chlam_1yr, 0) t_chlam_1yr,
coalesce(T6.t_chlam_pos_1yr, 0) t_chlam_pos_1yr,
coalesce(T7.syph_dx_2yr, 0) syph_dx_2yr,
coalesce(T7.nongon_ureth_dx_ever,0) nongon_ureth_dx_ever,
coalesce(T7.herpes_simplex_dx_ever, 0) herpes_simplex_dx_ever,
coalesce(T7.genital_herpes_dx_ever, 0) genital_herpes_dx_ever,
coalesce(T7.anogenital_warts_dx_ever, 0) anogenital_warts_dx_ever,
coalesce(T7.high_risk_sex_dx_ever, 0) high_risk_sex_dx_ever,
coalesce(T7.gonoc_inf_anus_rect_dx_1yr, 0) gonoc_inf_anus_rect_dx_1yr,
coalesce(T7.gonoc_pharyn_dx_ever, 0) gonoc_pharyn_dx_ever,
coalesce(T7.gonoc_pharyn_dx_2yr, 0) gonoc_pharyn_dx_2yr,
coalesce(T7.chlam_infec_of_pharynx_dx_ever, 0) chlam_infec_of_pharynx_dx_ever,
coalesce(T7.unspecified_std_dx_ever, 0) unspecified_std_dx_ever,
coalesce(T7.contact_exposure_vd_dx_ever, 0) contact_exposure_vd_dx_ever,
coalesce(T7.alcohol_depen_abuse_dx_ever, 0) alcohol_depen_abuse_dx_ever,
coalesce(T7.child_sex_abuse_dx_ever, 0) child_sex_abuse_dx_ever,
coalesce(T7.cannabis_abuse_dx_ever, 0) cannabis_abuse_dx_ever,
coalesce(T7.emergency_contracept_dx_ever, 0) emergency_contracept_dx_ever,
coalesce(T7.enc_hiv_screen_dx_ever, 0) enc_hiv_screen_dx_ever,
coalesce(T8.hbsag_or_dna_pos_ever, 0) hbsag_or_dna_pos_ever,
coalesce(T8.hepc_ab_pos_ever, 0) hepc_ab_pos_ever,
coalesce(T9.sexually_active, 0) sexually_active,
coalesce(T10.bcm_pill, 0) bcm_pill,
coalesce(T11.t_syph_1yr, 0) t_syph_1yr,
coalesce(T11.t_syph_pos_1yr, 0) t_syph_pos_1yr,
coalesce(T12.herpes_direct_pos_ever, 0) herpes_direct_pos_ever,
coalesce(T12.herpes_serology_1yr, 0) herpes_serology_1yr,
coalesce(T12.herpes_serology_pos_ever, 0) herpes_serology_pos_ever,
coalesce(T13.azith_rx_1yr, 0) azith_rx_1yr,
coalesce(T13.ceft_rx_1yr, 0) ceft_rx_1yr,
coalesce(T14.hepb_dx_or_pos_lab_ever, 0) hepb_dx_or_pos_lab_ever,
coalesce(T15.opiate_pos_ever, 0) opiate_pos_ever
FROM hiv_rpt.hrp_hist_index_patients T1
LEFT JOIN hiv_rpt.hrp_hist_hiv_elisa_or_agab_counts T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_hiv_lab_values T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_bicillin_values T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_hiv_meds T5 ON (T1.patient_id = T5.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_gon_chlam_values T6 ON (T1.patient_id = T6.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_diags_values T7 ON (T1.patient_id = T7.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_hep_values T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_soc_hist_sa T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_soc_hist_bcm T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_syph_values T11 ON (T1.patient_id = T11.patient_id) 
LEFT JOIN hiv_rpt.hrp_hist_hsv_labs_values T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_meds_of_int_values T13 ON (T1.patient_id = T13.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_hep_b_value T14 ON (T1.patient_id = T14.patient_id)
LEFT JOIN hiv_rpt.hrp_hist_opiate_lab_values T15 ON (T1.patient_id = T15.patient_id)
;

/*  -- COMMENTING OUT FOR JCDH
    -- OVERWRITE BICILLIN VALUES FOR PATIENTS WITHOUT OTHER INDICATORS
UPDATE hiv_rpt.hrp_hist_index_data
SET rx_bicillin_1_yr = 0,
rx_bicillin_2yr = 0,
rx_bicillin_e = 0
WHERE patient_id IN (
	SELECT patient_id
	FROM hiv_rpt.hrp_hist_index_data
	WHERE
	t_hiv_rna_1_yr = 0 and
	t_hiv_test_2yr = 0 and
	t_hiv_test_e = 0 and
	t_hiv_elisa_e = 0 and
	acute_hiv_test_2yr = 0 and
	acute_hiv_test_e = 0 and
	t_pos_gon_test_2yr = 0 and
	t_chla_test_t_e = 0 and
	syphilis_any_site_state_x_late_e = 0 and
	contact_w_or_expo_venereal_dis_e = 0 and
	hiv_counseling_2yr = 0 and
	syph_last_date is null and
	rx_bicillin_e > 0)
; */

-- Compute the sum to be used in the risk score calculation
CREATE TABLE hiv_rpt.hrp_hist_pat_sum_x_value AS
SELECT patient_id, 
(
(c.intercept) +
(i.current_age * c.current_age) +
(i.gender_male * c.gender_male) +
(i.gender_trans * c.gender_trans) +
(i.gender_unk * c.gender_unk) +
(i.marital_stat_married * c.marital_stat_married) +
(i.marital_stat_partner * c.marital_stat_partner) +
(i.marital_stat_single * c.marital_stat_single) +
(i.marital_stat_unk * c.marital_stat_unk) +
(i.marital_stat_widow * c.marital_stat_widow) +
(i.hbsag_or_dna_pos_ever * c.hbsag_or_dna_pos_ever) +
(i.hiv_neg_with_rna_test_ever * c.hiv_neg_with_rna_test_ever) +
(i.hiv_neg_with_hiv_meds_ever * c.hiv_neg_with_hiv_meds_ever) +
(i.sexually_active * c.sexually_active) +
(i.bcm_pill * c.bcm_pill) +
(i.t_gon_pos_1yr * c.t_gon_pos_1yr) +
(i.t_gon_throat_1yr * c.t_gon_throat_1yr) +
(i.t_chlam_1yr * c.t_chlam_1yr) +
(i.t_chlam_pos_1yr * c.t_chlam_pos_1yr) +
(i.t_syph_1yr * c.t_syph_1yr) +
(i.t_syph_pos_1yr * c.t_syph_pos_1yr) +
(i.herpes_direct_pos_ever * c.herpes_direct_pos_ever) +
(i.herpes_serology_1yr * c.herpes_serology_1yr) +
(i.herpes_serology_pos_ever * c.herpes_serology_pos_ever) +
(i.t_hiv_1yr * c.t_hiv_1yr) +
(i.t_hiv_rna_viral_1yr * c.t_hiv_rna_viral_1yr) +
(i.t_hiv_agab_or_elisa_1yr * c.t_hiv_agab_or_elisa_1yr) +
(i.bicillin_rx_1yr * c.bicillin_rx_1yr) +
(i.azith_rx_1yr * c.azith_rx_1yr) +
(i.ceft_rx_1yr * c.ceft_rx_1yr) +
(i.syph_dx_2yr * c.syph_dx_2yr) +
(i.hepc_ab_pos_ever * c.hepc_ab_pos_ever) +
(i.nongon_ureth_dx_ever * c.nongon_ureth_dx_ever) +
(i.herpes_simplex_dx_ever * c.herpes_simplex_dx_ever) +
(i.genital_herpes_dx_ever * c.genital_herpes_dx_ever) +
(i.anogenital_warts_dx_ever * c.anogenital_warts_dx_ever) +
(i.high_risk_sex_dx_ever * c.high_risk_sex_dx_ever) +
(i.hepb_dx_or_pos_lab_ever * c.hepb_dx_or_pos_lab_ever) +
(i.gonoc_inf_anus_rect_dx_1yr * c.gonoc_inf_anus_rect_dx_1yr) +
(i.gonoc_pharyn_dx_ever * c.gonoc_pharyn_dx_ever) +
(i.gonoc_pharyn_dx_2yr * c.gonoc_pharyn_dx_2yr) +
(i.chlam_infec_of_pharynx_dx_ever * c.chlam_infec_of_pharynx_dx_ever) +
(i.unspecified_std_dx_ever * c.unspecified_std_dx_ever) +
(i.contact_exposure_vd_dx_ever * c.contact_exposure_vd_dx_ever) +
(i.alcohol_depen_abuse_dx_ever * c.alcohol_depen_abuse_dx_ever) +
(i.child_sex_abuse_dx_ever * c.child_sex_abuse_dx_ever) +
(i.cannabis_abuse_dx_ever * c.cannabis_abuse_dx_ever) +
(i.emergency_contracept_dx_ever * c.emergency_contracept_dx_ever) +
(i.enc_hiv_screen_dx_ever * c.enc_hiv_screen_dx_ever) +
(i.opiate_pos_ever * c.opiate_pos_ever)
) as pat_sum_x_value
FROM hiv_rpt.hrp_hist_index_data i,
hiv_rpt.hrp_hist_coefficients c;


CREATE TABLE hiv_rpt.hrp_hist_risk_scores AS
SELECT patient_id, (1 / (1 + exp(-(pat_sum_x_value)))) hiv_risk_score
FROM hiv_rpt.hrp_hist_pat_sum_x_value;

/* CREATE TABLE hiv_rpt.hrp_hist_hiv_risk_score_linelist AS 
SELECT round(hiv_risk_score::numeric, 6) hiv_risk_score, :end_calc_year::date as score_date, T1.patient_id as esp_patient_id, T2.mrn, T2.last_name, T2.first_name, T2.date_of_birth::date,
concat(T3.last_name, ', ', T3.first_name) as pcp_name,
T3.dept as pcp_site
FROM hiv_rpt.hrp_hist_risk_scores T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN emr_provider T3 on (T2.pcp_id = T3.id)
ORDER BY hiv_risk_score DESC; */


-- Get most recent PrEP rx date
-- 07/2023 - Broke out Descovy from Truvada and added APRETUDE
CREATE TABLE hiv_rpt.hrp_hist_prep AS
SELECT patient_id, max(date) most_recent_prep_rx
FROM hef_event T1
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
'rx:hiv_cabotegravir_er_600')
AND date <= :end_calc_year
GROUP BY patient_id;

-- PATIENT DETAILS WITH PCP PROVIDER AND PROVIDER FROM LAST ENCOUNTER 
-- WHERE PROVIDER LAST NAME IS NOT NULL...JUST USING MAX ID WHICH ISN'T ALWAYS MOST RECENT BUT GENERALLY THE BEST METHOD)
CREATE TABLE hiv_rpt.hrp_hist_hiv_risk_score_linelist AS
SELECT round(hiv_risk_score::numeric, 6) hiv_risk_score, :end_calc_year::date as score_date, T1.patient_id as esp_patient_id, T2.mrn, T2.last_name, T2.first_name, T2.date_of_birth::date,
most_recent_prep_rx,
concat(T3.last_name, ', ', T3.first_name) as pcp_name,
T3.dept as pcp_site,
concat(T6.last_name, ', ', T6.first_name) as last_provider_name, T6.dept as last_provider_dept
FROM hiv_rpt.hrp_hist_risk_scores T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN emr_provider T3 on (T2.pcp_id = T3.id)
LEFT JOIN (SELECT T1.patient_id, max(T1.id) most_recent_enc_id
           FROM emr_encounter T1
		   INNER JOIN emr_provider T2 ON (T1.provider_id = T2.id)
	       INNER JOIN hiv_rpt.hrp_hist_risk_scores T3 ON (T1.patient_id = T3.patient_id)
		   WHERE T2.last_name is not null and last_name != '' and last_name not ilike 'UNKNOWN'
		   --AND (provider_type = 1 or provider_type = 0 or provider_type is null)
		   GROUP BY T1.patient_id) T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN emr_encounter T5 ON (T1.patient_id = T5.patient_id and T4.most_recent_enc_id = T5.id)
LEFT JOIN emr_provider T6 ON (T5.provider_id = T6.id)
LEFT JOIN hiv_rpt.hrp_hist_prep T7 ON (T7.patient_id = T1.patient_id)
ORDER BY hiv_risk_score DESC;




--COPY hiv_rpt.hrp_hist_hiv_risk_score_linelist TO '/tmp/2023-07-06-SAMPLE-mchd-hiv-risk-score-linelist.csv' DELIMITER ',' CSV HEADER;


/* -- POPULATE THE TABLE FOR COC REPORTING
INSERT INTO gen_pop_tools.cc_hiv_risk_score
	(SELECT T1.patient_id, T1.rpt_year, round(hiv_risk_score::numeric, 6) hiv_risk_score, hiv_test_this_yr as hiv_test_yn, truvada_rx_this_yr truvada_rx_yn
	FROM hiv_rpt.hrp_hist_index_data T1,
	hiv_rpt.hrp_hist_risk_scores T2
	WHERE T1.patient_id = T2.patient_id ); */


-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_coefficients;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_index_patients;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_labs;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_lab_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_labs_elisa_or_ag_ab;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_elisa_or_agab_counts;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_gon_chlam_events;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_gon_chlam_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_diags;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_diags_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_count_exlusions;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_subset;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_subset_2;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_bicillin_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_syph_events;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_syph_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_events;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hep_b_value;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_meds;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_soc_hist_sa;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_soc_hist_bcm;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hsv_labs;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hsv_labs_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_meds_of_int;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_meds_of_int_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_opiate_labs;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_opiate_lab_values;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_prep;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_pat_sum_x_value;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_index_data;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_risk_scores;
-- DROP TABLE IF EXISTS hiv_rpt.hrp_hist_hiv_risk_score_linelist;


-- TO RUN
-- nohup psql -d esp -f mchd-hiv-risk-prediction-historical.sql  -v end_calc_year="'2023-04-29'" &










