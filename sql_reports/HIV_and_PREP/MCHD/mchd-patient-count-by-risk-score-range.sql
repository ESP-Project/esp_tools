


CREATE TABLE hiv_rpt.score_counts_01 AS

WITH series AS (
                SELECT generate_series(0, 1, .01) AS r_from 
), range AS (
                SELECT r_from, (r_from + .01) AS r_to FROM series 
), score_counts_by_01 AS (
SELECT r_from, r_to,
       (SELECT count(*) FROM hiv_rpt.hrp_hist_risk_scores WHERE hiv_risk_score BETWEEN r_from AND r_to) as patient_count
FROM range
	)
SELECT * from score_counts_by_01;


CREATE TABLE hiv_rpt.score_counts_001_to_01 AS

WITH series AS (
                SELECT generate_series(0, .01, .001) AS r_from 
), range AS (
                SELECT r_from, (r_from + .001) AS r_to FROM series 
), score_counts_001_to_01 AS (
SELECT r_from, r_to,
       (SELECT count(*) FROM hiv_rpt.hrp_hist_risk_scores WHERE hiv_risk_score BETWEEN r_from AND r_to) as patient_count
FROM range
	)
SELECT * from score_counts_001_to_01;



--COPY hiv_rpt.score_counts_01 TO '/tmp/2023-07-06-MCHD-hiv_rpt.score_counts_01.csv' DELIMITER ',' CSV HEADER;
--COPY hiv_rpt.score_counts_001_to_01 TO '/tmp/2023-07-06-MCHD-hiv_rpt.score_counts_001_to_01.csv' DELIMITER ',' CSV HEADER;