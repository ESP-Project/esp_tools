-- SPECIMEN SOURCES
select count(*), specimen_source 
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and T2.test_name in ('gonorrhea', 'chlamydia')
group by specimen_source
order by specimen_source;


-- SPECIMEN SOURCE: HOW WELL POPULATED?
select count(T1.id) total_count,
count (case when specimen_source is not null then 1 end) as specimen_source_populated
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and T2.test_name in ('gonorrhea', 'chlamydia');

-- SPECIMEN SOURCE: NATIVE NAMES FOR CHLAM/GON
select count(*), native_name
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and T2.test_name in ('gonorrhea', 'chlamydia')
group by native_name
order by native_name;


-- SPECIMEN SOURCE: PROCEDURE NAMES FOR CHLAM/GON
select count(*), procedure_name
from emr_labresult T1,
conf_labtestmap T2
where T1.native_code = T2.native_code
and T2.test_name in ('gonorrhea', 'chlamydia')
group by procedure_name
order by procedure_name;