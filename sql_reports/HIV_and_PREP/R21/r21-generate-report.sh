#!/bin/bash

## Run the full report and then compute the risk score by passing in patient_id and month_0 date using
## the hiv_risk_score_calc_for_month function that must already be installed

R21_REPORT=/srv/esp/esp_tools/sql_reports/HIV_and_PREP/prep-R21.sql
ESP_DB_NAME=esp
PSQL=/usr/bin/psql

#RUN THE R21 PREP REPORT
R21_REPORT_RUN=$($PSQL -d $ESP_DB_NAME -f $R21_REPORT)
wait


#CREATE THE FILES FOR USE BY SQL
echo 'SELECT hiv_rpt.hiv_risk_score_calc_for_month(:patient_id, :month_0);' > call-risk-score-function.sql
echo 'UPDATE hiv_rpt.r21_output_table set risk_score_month_0 = :risk_score WHERE master_patient_id = :patient_id;' > update-output-table.sql
echo 'UPDATE hiv_rpt.r21_output_table_masked T1 SET risk_score_month_0 = :risk_score FROM hiv_rpt.r21_masked_patients T2 WHERE T1.masked_patient_id = T2.masked_patient_id AND T2.master_patient_id = :patient_id;' > update-masked-output-table.sql

#CALL THE RISK SCORE FUNCTION AND UPDATE THE OUTPUT TABLE WITH THE VALUE
$PSQL -c "select master_patient_id, month_0 from hiv_rpt.r21_index_pats" --single-transaction --no-align  -t --field-separator ' ' -d $ESP_DB_NAME \
| while read patient_id month_0 ; do
    echo "USER: $patient_id $month_0"
    risk_score=$($PSQL -v patient_id="$patient_id" -v month_0="'$month_0'" -f "./call-risk-score-function.sql"  --single-transaction --no-align  -t --field-separator ' ' -d $ESP_DB_NAME)
    echo $risk_score
    update_report=$($PSQL -v patient_id="$patient_id" -v risk_score="$risk_score" -f "./update-output-table.sql" --single-transaction --no-align  -t --field-separator ' ' -d $ESP_DB_NAME)
    update_masked_report=$($PSQL -v patient_id="$patient_id" -v risk_score="$risk_score" -f "./update-masked-output-table.sql" --single-transaction --no-align  -t --field-separator ' ' -d $ESP_DB_NAME)

done



