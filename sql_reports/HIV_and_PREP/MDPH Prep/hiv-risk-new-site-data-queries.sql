-- gender
-- mapped values: ('F', 'FEMALE', 'M', 'MALE', 'U', 'UNKNOWN', 'T')
select count(*), gender
from emr_patient
group by gender;

-- race
-- mapped values ('BLACK', 'BLACK OR AFRICAN AMERICAN') ('WHITE', 'CAUCASIAN')
select count(*), race
from emr_patient
group by race;

-- home_language
-- mapped values: 'ENGLISH'
-- ignored values: ('DECLINED', 'UNKNOWN', 'UNABLE TO BE DETERMINED', 'NONE', '', 'PATIENT DECLINED', 'DECLINE TO ANSWER', 'CHOSE NOT TO')
select count(*), home_language
from emr_patient
group by home_language;



