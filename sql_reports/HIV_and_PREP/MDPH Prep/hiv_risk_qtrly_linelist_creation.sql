DROP TABLE IF EXISTS hiv_rpt.dphll_index_pats;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_totals;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_total_w_descrip;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_rank;
DROP TABLE IF EXISTS hiv_rpt.dphll_hightest_risk_factors;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_syph_diags;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bicillin;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bupren_naloxone_rx;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_gonorrhea;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_hiv_tests;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_std_case_2yr;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ill_drug_use_2yr;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_sexually_active;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_monkeypox;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_doxy_rx;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output_excl_prep;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output_excl_disposition;


-- Threshold set to .02
-- Using patients with a risk_score this year
CREATE TABLE hiv_rpt.dphll_index_pats AS
SELECT patient_id, hiv_risk_score
FROM gen_pop_tools.cc_hiv_risk_score
WHERE hiv_risk_score >= .02
AND rpt_year = EXTRACT(YEAR FROM now())::text;


-- Previous PrEP order (Y/N)
-- Date of last PrEP order
-- 07/2023 Broke out Descovy from Truvada and added APRETUDE
CREATE TABLE hiv_rpt.dphll_prep AS
SELECT T1.patient_id, 
       1 as previous_prep_order, 
	   max(date) last_prep_order_date
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
'rx:hiv_cabotegravir_er_600')
AND T2.date <= now()
GROUP BY T1.patient_id;



-- Use the HIV Risk Score table from normal monthly run
-- For each variable, compute a total for each risk factor
CREATE TABLE hiv_rpt.dphll_risk_factor_totals AS
SELECT i.patient_id, 
(i.sex * c.sex) as sex_sum,
(i.race_black * c.race_black) as black_sum,
(i.race_caucasian * c.race_caucasian) as caucasian_sum,
(i.english_lang * c.english_lang) as english_sum,
(i.has_language_data * c.has_language_data) as lang_sum,
(i.years_of_data * c.years_of_data) as yrs_data_sum,
(i.has_1yr_data * c.has_1yr_data) as yr1_data_sum,
(i.has_2yr_data * c.has_2yr_data) as yr2_data_sum,
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) as hiv_rna_1yr_sum,
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) as hiv_test_2yr_sum,
(i.t_hiv_test_e * c.t_hiv_test_e) as hiv_test_e_sum,
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) as hiv_elisa_e_sum,
(i.acute_hiv_test_e * c.acute_hiv_test_e) as acute_hiv_test_e_sum,
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) as acute_hiv_test_2yr_sum,
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) as pos_gon_2yr_sum,
(i.t_chla_test_t_e * c.t_chla_test_t_e) as t_chla_test_e_sum,
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) as rx_bicill_1yr_sum,
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) as rx_bicill_2_yr_sum,
(i.rx_bicillin_e * c.rx_bicillin_e) as rx_bicillin_e_sum,
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) as suboxone_sum,
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) as syph_dx_sum,
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) as contact_sum,
(i.hiv_counseling_2yr * c.hiv_counseling_2yr) as hiv_counsel_sum
FROM hiv_rpt.hrp_hist_index_data i,
hiv_rpt.hrp_hist_coefficients c,
hiv_rpt.dphll_index_pats p
WHERE i.patient_id = p.patient_id;

-- Create a table with a single row per patient and risk factor 
-- with a description

CREATE TABLE hiv_rpt.dphll_risk_factor_total_w_descrip AS
select patient_id, sex_sum as master_sum, 'Patient Sex' as description FROM 
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, black_sum as master_sum, 'Race: Black' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, caucasian_sum as master_sum, 'Race: Caucasian' as description  FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, english_sum as master_sum, 'Home Language: English' as description  FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, lang_sum as master_sum, 'Patient has Home Language' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, yrs_data_sum as master_sum, 'Length of available EHR history' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, yr1_data_sum as master_sum, 'EHR history exists for previous year' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, yr2_data_sum as master_sum, 'EHR history exists for previous 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, hiv_rna_1yr_sum as master_sum, 'Total # of HIV RNA tests in the previous year' as description  FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, hiv_test_2yr_sum as master_sum, 'Total # of HIV tests in past 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, hiv_test_e_sum as master_sum, 'Total # of HIV tests ever (ELISA, RNA Viral, Ag/Ab)' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, hiv_elisa_e_sum as master_sum, 'Total # of HIV ELISA or Ab/Ag tests ever' as description  FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, acute_hiv_test_e_sum as master_sum, 'Patient has had an HIV RNA Viral test ever' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, acute_hiv_test_2yr_sum as master_sum, 'Patient has had an HIV RNA Viral test in the last 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, pos_gon_2yr_sum as master_sum, 'Total # of positive gonorrhea tests in past 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, t_chla_test_e_sum as master_sum, 'Total # of chlamydia tests ever' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, rx_bicill_1yr_sum as master_sum, 'Total # of rx for bicillin in previous year' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, rx_bicill_2_yr_sum as master_sum, 'Total # of rx for bicillin in past 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, rx_bicillin_e_sum as master_sum, 'Total # of rx for bicillin ever' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, suboxone_sum as master_sum, 'Total # of rx for suboxone in past 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, syph_dx_sum as master_sum, 'Dx code for syphilis ever' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, contact_sum as master_sum, 'Dx for contact or exposure to venereal disease ever' as description FROM
hiv_rpt.dphll_risk_factor_totals
UNION
select patient_id, hiv_counsel_sum as master_sum, 'Dx code for HIV counseling in past 2 years' as description FROM
hiv_rpt.dphll_risk_factor_totals;

-- Rank the risk factors
CREATE TABLE hiv_rpt.dphll_risk_factor_rank AS
SELECT patient_id,
description, master_sum,
RANK () OVER ( 
		PARTITION BY patient_id
		ORDER BY master_sum DESC
	) risk_factor_rank
FROM 
hiv_rpt.dphll_risk_factor_total_w_descrip T1;

-- Identify first and second highest risk factor for each patient
-- If second highest is zero value (or less) customize the output

CREATE TABLE hiv_rpt.dphll_hightest_risk_factors AS
SELECT T1.patient_id,
T1.description as highest_risk_variable,
CASE WHEN T2.description is null then 'NONE' ELSE T2.description END as second_highest_risk_variable
FROM hiv_rpt.dphll_risk_factor_rank T1
LEFT JOIN hiv_rpt.dphll_risk_factor_rank T2 ON (T1.patient_id = T2.patient_id AND T2.risk_factor_rank = 2 and T2.master_sum > 0)
WHERE T1.risk_factor_rank = 1;



CREATE TABLE hiv_rpt.dphll_prep_syph_diags AS
SELECT DISTINCT T1.patient_id, most_recent_date, syph_dx_name as syph_dx_name_last_2yr,
1 AS syph_dx_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN (select S1.patient_id, max(date) most_recent_date 
			FROM hiv_rpt.dphll_index_pats S1
			INNER JOIN emr_encounter S2 ON (S1.patient_id = S2.patient_id)
			INNER JOIN emr_encounter_dx_codes S3 ON (S2.id = S3.encounter_id)
			WHERE (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
                  OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1'))
                  AND date >= (now()::date - interval '2 years')
                  AND date <= now() 
		          GROUP BY S1.patient_id) T5 ON (T1.patient_id = T5.patient_id and T5.most_recent_date = T2.date)
INNER JOIN (select S1.patient_id, max(name) syph_dx_name, date
            FROM hiv_rpt.dphll_index_pats S1
			INNER JOIN emr_encounter S2 ON (S1.patient_id = S2.patient_id)
			INNER JOIN emr_encounter_dx_codes S3 ON (S2.id = S3.encounter_id)
			LEFT JOIN static_dx_code S4 ON (S4.combotypecode = S3.dx_code_id) 
			WHERE (dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' 
                  OR dx_code_id in ('icd9:097.9', 'icd10:A53.0', 'icd10:A53.9', 'icd9:097.1'))
                  AND date >= (now()::date - interval '2 years')
                  AND date <= now() 
				  GROUP BY S1.patient_id, date) T6 ON (T1.patient_id = T6.patient_id and T5.most_recent_date = T6.date)
ORDER BY T1.patient_id, most_recent_date DESC;


-- Bicillin Rx Last 2 Years
CREATE TABLE hiv_rpt.dphll_prep_bicillin AS
SELECT T1.patient_id, 1 AS bicillin_24_inj_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN hiv_risk_bicillin_meds T3 ON (T2.name = T3.name and T3.qualifying_med = 1)
WHERE
(
--name only matches
T3.match_type = 'name'
OR
--quantity_float_gte matches
(T3.match_type = 'q_float_gte' and T2.quantity_float >= T3.quantity_float::int)
OR
--dose_or_null_dose matches
(T3.match_type = 'dose_or_null_dose' and (T2.dose is null or T2.dose = T3.dose))
OR
--dose_exact matches
(T3.match_type = 'dose_exact' and T2.dose = T3.dose)
OR
--quantity matches
(T3.match_type = 'quantity' and T2.quantity ilike T3.quantity)
OR
--quantity_like matches
(T3.match_type = 'quantity_like' and T2.quantity ilike (concat('%', T3.quantity, '%')))
OR
--directions_like matches
(T3.match_type = 'directions_like' and T2.directions ilike (concat('%', T3.directions, '%')))
OR
--dose and directions null matches
(T3.match_type = 'dose_and_directions_null' and T2.directions is null and T2.dose is null)
)
AND T2.date >= (now()::date - interval '2 years')
AND T2.date <= now()
GROUP BY T1.patient_id;

-- Prescription for buprenorphine/naloxone (suboxone) in prior 2 years (Y/N)
CREATE TABLE hiv_rpt.dphll_prep_bupren_naloxone_rx AS
SELECT T1.patient_id, 1 AS suboxone_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE (T2.name ilike '%suboxone%' 
or T2.name ilike 'buprenorphine%naloxone%' 
or T2.name ilike '%BUPREN/NALOX%' --added 09/16/2022
or T2.name ilike '%BUPRNPH/NALOX%' --added 09/16/2022
or T2.name ilike '%BUPRENO-NALOX%' --added 06/16/2022
or T2.name ilike '%BUPRENORP-NALOX%' --added 09/16/2022
or T2.name ilike '%zubsolv%'
or T2.name ilike '%bunavail%'
or T2.name ilike '%cassipa%')
-- must be within last 2 years
AND T2.date >= (now()::date - interval '2 years')
AND T2.date <= now()
GROUP BY T1.patient_id;


-- Number of positive Gonorrhea tests in prior 2 years
-- Only count 1 pos test per date
CREATE TABLE hiv_rpt.dphll_prep_gonorrhea AS
SELECT patient_id, count(*) AS pos_gon_tests_last_2yr
FROM 
	(SELECT T1.patient_id, date
	FROM hiv_rpt.dphll_index_pats T1
	INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name = 'lx:gonorrhea:positive'
	AND T2.date >= (now()::date - interval '2 years')
	AND T2.date <= now()
	GROUP BY T1.patient_id, T2.date) S1
GROUP BY patient_id;


-- Number of HIV tests in prior 2 years
-- Only count 1 test per date
-- Add last_hiv_test_date to query - need to go against all years not just last 2
-- Going against HIV Risk Score table so HIV RNA is included
CREATE TABLE hiv_rpt.dphll_prep_hiv_tests AS
SELECT S1.patient_id, count(S2.*) as hiv_tst_dates_last_2yr, max(last_hiv_test_date) as last_hiv_test_date
FROM
	(SELECT T1.patient_id, max(date) as last_hiv_test_date
	           FROM hiv_rpt.dphll_index_pats T1
	           INNER JOIN hiv_rpt.hrp_hist_hiv_labs T2 ON (T1.patient_id = T2.patient_id)
	           GROUP BY T1.patient_id) S1
    LEFT JOIN (SELECT T1.patient_id, date
	        FROM hiv_rpt.dphll_index_pats T1
	        INNER JOIN hiv_rpt.hrp_hist_hiv_labs T2 ON (T1.patient_id = T2.patient_id)
	        WHERE T2.date >= (now()::date - interval '2 years')
	        AND T2.date <= now()
	        GROUP BY T1.patient_id, T2.date) S2	ON (S1.patient_id = S2.patient_id)
GROUP BY S1.patient_id;


-- Patients that have had an ESP case of syph, gon, or clam
-- in last 2 years
CREATE TABLE hiv_rpt.dphll_prep_std_case_2yr AS
select T1.patient_id,
1 AS std_case_last_2yr
from hiv_rpt.dphll_index_pats T1
INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
WHERE condition in ('gonorrhea', 'syphilis', 'chlamydia')
AND T2.date >= (now()::date - interval '2 years')
AND T2.date <= now()
GROUP BY T1.patient_id;

-- Patients that have reported ill_drug_use in last 2 years
-- If they ever said YES (most recent answer could be no)
CREATE TABLE hiv_rpt.dphll_prep_ill_drug_use_2yr AS
SELECT distinct(T1.patient_id), 1 as ill_drug_use_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_socialhistory T2 ON (T1.patient_id = T2.patient_id)
where upper(ill_drug_use) = 'YES'
AND date >= (now()::date - interval '2 years')
AND date <= now();


-- Get most recent not null response for sexually_active 
-- in the LAST 2 YEARS
CREATE TABLE hiv_rpt.dphll_prep_sexually_active AS
SELECT T1.patient_id, sex_active_date, 
case when upper(sexually_active) = 'YES' then 1
     when upper(sexually_active) = 'NEVER' then 0
	 when upper(sexually_active) = 'NOT CURRENTLY' then 0
	 else null
	 END sexually_active_status,
sexually_active
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN (SELECT S1.patient_id, max(date) sex_active_date
	FROM hiv_rpt.dphll_index_pats S1
	INNER JOIN emr_socialhistory S2 ON (S1.patient_id = S2.patient_id)
	WHERE S2.sexually_active is not null
	AND S2.sexually_active != ''
	AND S2.date >= (now()::date - interval '2 years')
	AND S2.date <= now()
	GROUP BY S1.patient_id) T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN (SELECT DISTINCT S1.patient_id, date, max(sexually_active) sexually_active
	FROM hiv_rpt.dphll_index_pats S1
	INNER JOIN emr_socialhistory S2 ON (S1.patient_id = S2.patient_id)
	WHERE S2.sexually_active is not null
	AND S2.sexually_active != ''
	AND S2.date >= (now()::date - interval '2 years')
	AND S2.date <= now()
	GROUP BY S1.patient_id, date) T3 ON (T1.patient_id = T3.patient_id and T2.sex_active_date = T3.date);
	

-- Get details on Monkeypox vaccine
CREATE TABLE hiv_rpt.dphll_prep_monkeypox AS
SELECT patient_id, max(date) as last_mpox_vax_date, count(distinct(date)) as num_mpox_vax
FROM (
     SELECT T1.patient_id, date, name 
     FROM emr_immunization T1
     INNER JOIN hiv_rpt.dphll_index_pats T2 ON (T1.patient_id = T2.patient_id)
     WHERE upper(T1.name) ~ ('MPOX|MONKEYPOX|JYNNEOS')
) T1
GROUP BY patient_id;


-- Get details on last Doxy RX
CREATE TABLE hiv_rpt.dphll_prep_doxy_rx AS
SELECT DISTINCT T1.patient_id, max(date) as last_doxy_rx_date
FROM emr_prescription T1
INNER JOIN hiv_rpt.dphll_index_pats T2 ON (T1.patient_id = T2.patient_id)
JOIN static_drugsynonym T3 on T1.name ilike '%' || T3.other_name || '%'  
WHERE T3.generic_name ilike '%Doxycycline%'
GROUP BY T1.patient_id;


-- Join with patient demographics
CREATE TABLE hiv_rpt.dphll_prep_ll_output AS
SELECT 
T1.patient_id,
T1.hiv_risk_score,
T14.highest_risk_variable,
T14.second_highest_risk_variable,
coalesce(T8.previous_prep_order, 0) AS previous_prep_order,
T8.last_prep_order_date,
coalesce(T11.suboxone_last_2yr, 0) AS suboxone_last_2yr,
coalesce(T16.ill_drug_use_last_2yr, 0) as ill_drug_use_last_2yr,
CASE WHEN T11.suboxone_last_2yr = 1 and T15.std_case_last_2yr = 1 THEN 1 ELSE 0 END AS suboxone_and_std_last_2yr,
coalesce(T15.std_case_last_2yr, 0) AS std_case_last_2yr,
coalesce(T17.sexually_active_status, null) as sexually_active_status,
coalesce(T12.pos_gon_tests_last_2yr, 0) AS num_pos_gon_tests_last_2yr,
coalesce(T13.hiv_tst_dates_last_2yr, 0) AS num_hiv_tst_dates_last_2yr,
coalesce(T10.bicillin_24_inj_last_2yr, 0) AS bicillin_24_inj_last_2yr,
coalesce(T9.syph_dx_last_2yr, 0) as syph_dx_last_2yr,
T9.syph_dx_name_last_2yr,
upper(T2.first_name) AS first_name,
upper(T2.last_name) AS last_name,
T2.mrn,
T2.gender,
T4.sex_partner_gender,
T18.last_mpox_vax_date,
coalesce(T18.num_mpox_vax, 0) num_mpox_vax,
T19.last_doxy_rx_date,
T13.last_hiv_test_date,
upper(T2.race) AS race,
upper(T2.ethnicity) AS ethnicity,
date_part('year',age(T2.date_of_birth)) current_age,
upper(T2.home_language) AS home_language,
UPPER(concat(T7.last_name, ', ', T7.first_name)) AS  pcp_name
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN (
    (select max(date) sochist_date, T2.patient_id
	from emr_socialhistory T1,
	hiv_rpt.dphll_index_pats T2
	where T1.patient_id = T2.patient_id
	and sex_partner_gender is not null
	and date <= now()
	GROUP BY T2.patient_id) 
	) T3 on (T1.patient_id = T3.patient_id)
-- If multiple sex_partner_gender values on the same date, just take one of them
LEFT JOIN (select patient_id, max(sex_partner_gender) as sex_partner_gender, date from emr_socialhistory where sex_partner_gender is not null GROUP BY patient_id, date) T4 
               on (T3.patient_id = T4.patient_id and T1.patient_id = T4.patient_id and T3.sochist_date = T4.date)
LEFT JOIN emr_provider T7 ON (T7.id = T2.pcp_id)
LEFT JOIN hiv_rpt.dphll_prep T8 ON (T1.patient_id = T8.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_syph_diags T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_bicillin T10 ON (T1.patient_id = T10.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_bupren_naloxone_rx T11 ON (T1.patient_id = T11.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_gonorrhea T12 ON (T1.patient_id = T12.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_hiv_tests T13 ON (T1.patient_id = T13.patient_id)
LEFT JOIN hiv_rpt.dphll_hightest_risk_factors T14 ON (T1.patient_id = T14.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_std_case_2yr T15 ON (T1.patient_id = T15.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_ill_drug_use_2yr T16 ON (T1.patient_id = T16.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_sexually_active T17 ON (T1.patient_id = T17.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_monkeypox T18 ON (T1.patient_id = T18.patient_id)
LEFT JOIN hiv_rpt.dphll_prep_doxy_rx T19 ON (T1.patient_id = T19.patient_id)
-- exclude deceased patients
WHERE T2.date_of_death is null and (T2.vital_status not ilike 'deceased' or T2.vital_status is null)
ORDER BY T1.hiv_risk_score desc;


-- Filter out patients that have had a PrEP rx within the last year
CREATE TABLE hiv_rpt.dphll_prep_ll_output_excl_prep AS
SELECT *
FROM hiv_rpt.dphll_prep_ll_output 
WHERE last_prep_order_date is null 
OR last_prep_order_date <= now() - INTERVAL '1 year';

-- Filter out patients that have an exclusion based on a recorded disposition that supresses them from the list for 365 days
-- There are the disposition_id's that prompt supression (1, 2, 3, 7, 9, 10, 11, 12, 13)
-- 1 = 'No PrEP Rx - Provider determined patient is LOW RISK for HIV'
-- 2 = 'No PrEP Rx - Provider determined CLINICAL CONTRAINDICATIONS '
-- 3 = 'No PrEP Rx - Provider determined MEDICATION ADHERENCE or medical monitoring challenges'
-- 7 = 'Patient declined offer to schedule an appointment with provider to discuss PrEP'
-- 9 = 'No PrEP Rx After Patient/Provider discussion - Provider determined patient is LOW RISK for HIV'
-- 10 = 'No PrEP Rx After Patient/Provider discussion - Patient perceives self to be LOW RISK for HIV'
-- 11 = 'No PrEP Rx After Patient/Provider discussion - Due to Cost'
-- 12 = 'No PrEP Rx After Patient/Provider discussion - Due to Frequency of Medical Monitoring'
-- 13 = 'No PrEP Rx After Patient/Provider discussion - Due to Possible Side Effects'
CREATE TABLE hiv_rpt.dphll_prep_ll_output_excl_disposition AS
SELECT *
FROM hiv_rpt.dphll_prep_ll_output_excl_prep 
WHERE patient_id not in (select patient_id from hiv_risk_patient_dispositions where disposition_id in (1, 2, 3, 7, 9, 10, 11, 12, 13) and disposition_date >= now() - INTERVAL '365 days' );


-- Filter out patients where the most recent disposition is 'Patient no longer in care at the site'
-- and the patient does not have any clinical encounters since the disposition date

DELETE FROM hiv_rpt.dphll_prep_ll_output_excl_disposition
WHERE patient_id in (
     SELECT patient_id FROM (
          SELECT T2.patient_id, max(T1.date) most_recent_clin_enc, T3.disposition_date
          FROM gen_pop_tools.clin_enc T1
          JOIN (SELECT patient_id, max(id) most_recent_dispo_id FROM hiv_risk_patient_dispositions WHERE disposition_id = 4 GROUP BY patient_id) T2 ON (T1.patient_id = T2.patient_id)
          JOIN hiv_risk_patient_dispositions T3 ON (T2.most_recent_dispo_id = T3.id)
          GROUP BY T2.patient_id, T3.disposition_date ) INNER1
     WHERE most_recent_clin_enc < disposition_date );
	 

-- Replace current line list with new data
DELETE FROM public.hiv_risk_patient_linelist;
INSERT INTO public.hiv_risk_patient_linelist
SELECT patient_id, hiv_risk_score, highest_risk_variable, second_highest_risk_variable,
previous_prep_order, last_prep_order_date, suboxone_last_2yr, ill_drug_use_last_2yr,
suboxone_and_std_last_2yr, std_case_last_2yr, sexually_active_status, num_pos_gon_tests_last_2yr,
num_hiv_tst_dates_last_2yr, bicillin_24_inj_last_2yr, syph_dx_last_2yr,
syph_dx_name_last_2yr, sex_partner_gender, last_doxy_rx_date, last_mpox_vax_date, num_mpox_vax, last_hiv_test_date
from hiv_rpt.dphll_prep_ll_output_excl_disposition;

-- Delete any historical existing entries for the quarter being run.
-- This is to manage re-runs of the script.
DELETE FROM public.hiv_risk_patient_linelist_history 
WHERE ll_quarter = EXTRACT (QUARTER FROM now())
AND ll_year = EXTRACT (YEAR FROM now());


-- Populate historical table with data
INSERT INTO public.hiv_risk_patient_linelist_history 
SELECT nextval('hiv_risk_patient_linelist_history_id_seq'), now()::date as linelist_date, 
EXTRACT (QUARTER FROM now()) as ll_quarter, EXTRACT (YEAR FROM now()) as ll_year, 
T1.hiv_risk_score, T1.highest_risk_variable, T1.second_highest_risk_variable, T1.patient_id
FROM public.hiv_risk_patient_linelist T1;


DROP TABLE IF EXISTS hiv_rpt.dphll_index_pats;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_totals;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_total_w_descrip;
DROP TABLE IF EXISTS hiv_rpt.dphll_risk_factor_rank;
DROP TABLE IF EXISTS hiv_rpt.dphll_hightest_risk_factors;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_syph_diags;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bicillin;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_bupren_naloxone_rx;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_gonorrhea;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_hiv_tests;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_std_case_2yr;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ill_drug_use_2yr;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_sexually_active;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_monkeypox;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_doxy_rx;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output_excl_prep;
DROP TABLE IF EXISTS hiv_rpt.dphll_prep_ll_output_excl_disposition;









