--415
drop table if exists kre_report.dph_prep_gte01_data;
create table kre_report.dph_prep_gte01_data as 
select T1.patient_id,
'Y'::text as risk_score_01,
hiv_risk_score::numeric, 
case when race in ( 'DECLINED TO ANSWER','HISPANIC', 'PATIENT DECL', 'None', 'UNKNOWN') or race is null then 'UNKNOWN' 
when race in ('WHITE', 'CAUCASIAN') then 'WHITE'
when race in ('NAT AMERICAN', 'AMERICAN INDIAN/ALASKAN NATIVE', 'ALASKAN' ) then 'NAT AMERICAN'
when race in ('BLACK') then 'BLACK'
when race in ('ASIAN') then 'ASIAN'
when race in ('PACIFIC ISLANDER/HAWAIIAN', 'NATIVE HAWAI') then 'PAC ISLANDER'
else 'OTHER'
end race,
race as race_raw,
CASE 
      WHEN extract(YEAR FROM age(now(), date_of_birth::date)) < 11 then '0-10'
      WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 11 and 14 then '11-14'
      WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 15 and 24 then '15-24'
      WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 25 and 34 then '25-34'
	  WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 35 and 44 then '35-44'
	  WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 45 and 54 then '45-54'
	  WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 55 and 64 then '55-64'
	  WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 65 and 74 then '65-74'
	  WHEN extract(YEAR FROM age(now(), date_of_birth::date)) between 75 and 84 then '75-84'
      WHEN extract(YEAR FROM age(now(), date_of_birth::date)) > 84 then '85+'
    END as age_group,
case when gender in ('M', 'MALE') then 'MALE'
     when gender in ('F', 'FEMALE') then 'FEMALE' end gender,
case when truvada_rx_yn = 0 then 'N' 
     when truvada_rx_yn = 1 then 'Y'end truvada_rx_2020,
case when truvada_ever = 'Y' then 'Y' else 'N' end truvada_ever,
case when truvada_ever is null then 'Y' else 'N' end truvada_never,
coalesce(hiv_test_2yr, 'N') as hiv_test_2yr,
coalesce(bicillin_1yr, 'N') as bicillin_1yr,
coalesce(bicillin_2yr, 'N') as bicillin_2yr,
coalesce(pos_gon_2yr, 'N') as pos_gon_2yr,
coalesce(chlam_tst_ever, 'N') as chlam_tst_ever,
coalesce(suboxone_2yr, 'N') as suboxone_2yr,
coalesce(hiv_tst_ever, 'N') as hiv_tst_ever,
coalesce(syph_diag_ever, 'N') as syph_diag_ever,
coalesce(vd_cont_dx_ever, 'N') as vd_cont_dx_ever,
coalesce(hiv_couns_2yr, 'N') as hiv_couns_2yr,
coalesce(english_lang, 'N') as english_lang,
coalesce(total_truvada_days, 0) as total_truvada_days,
case when total_truvada_days <= 30 then 1 -- 1 mo or less
	when total_truvada_days <= 90 then 2 -- 3 mo
	when total_truvada_days <= 180 then 3 -- 6 mo
	when total_truvada_days <= 365 then 4 -- 1 yr
	when total_truvada_days <= 730 then 5 -- 2 yr
	when total_truvada_days <= 1095 then 6 -- 3 yr
	when total_truvada_days <= 1460 then 7 -- 4 yr
	when total_truvada_days <= 1825 then 8 -- 5 yr
	when total_truvada_days <= 2190 then 9 -- 5 - 6 yr
	when total_truvada_days <= 2555 then 10 -- 6 - 7 yr
	when total_truvada_days >  2555 then 11 -- more than 7 yrs
	end truvada_time,
coalesce(truvada_rx_num, 0) as truvada_rx_num,
coalesce(gon_case, 'N') as gon_case,
coalesce(chlam_case, 'N') as chlam_case,
coalesce(syph_case, 'N') as syph_case, 
coalesce(gon_and_chlam_case, 'N') as gon_and_chlam_case
from gen_pop_tools.cc_hiv_risk_score T1
INNER JOIN emr_patient T2 on (T1.patient_id = T2.id)
LEFT JOIN (select patient_id, 'Y'::text as truvada_ever from hef_event 
		   where name in('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')) T3 on (T1.patient_id = T3.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as hiv_test_2yr from hiv_rpt.hrp_hist_index_data where t_hiv_test_2yr != 0) T4 on (T1.patient_id = T4.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as bicillin_1yr from hiv_rpt.hrp_hist_index_data where rx_bicillin_1_yr != 0) T5 on (T1.patient_id = T5.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as pos_gon_2yr from hiv_rpt.hrp_hist_index_data where t_pos_gon_test_2yr != 0) T6 on (T1.patient_id = T6.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as bicillin_2yr from hiv_rpt.hrp_hist_index_data where rx_bicillin_2yr != 0) T7 on (T1.patient_id = T7.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as suboxone_2yr from hiv_rpt.hrp_hist_index_data where rx_suboxone_2yr != 0) T8 on (T1.patient_id = T8.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as chlam_tst_ever from hiv_rpt.hrp_hist_index_data where t_chla_test_t_e != 0) T9 on (T1.patient_id = T9.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as hiv_tst_ever from hiv_rpt.hrp_hist_index_data where t_hiv_test_e != 0) T10 on (T1.patient_id = T10.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as syph_diag_ever from hiv_rpt.hrp_hist_index_data where syphilis_any_site_state_x_late_e != 0) T11 on (T1.patient_id = T11.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as vd_cont_dx_ever from hiv_rpt.hrp_hist_index_data where contact_w_or_expo_venereal_dis_e != 0) T12 on (T1.patient_id = T12.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as hiv_couns_2yr from hiv_rpt.hrp_hist_index_data where hiv_counseling_2yr != 0) T13 on (T1.patient_id = T13.patient_id)
LEFT JOIN (select patient_id, 'Y'::text as english_lang from hiv_rpt.hrp_hist_index_data where english_lang != 0) T14 on (T1.patient_id = T14.patient_id)
LEFT JOIN (SELECT patient_id, sum(number_of_days) total_truvada_days from 
	(select T1.patient_id, 
	case 
		 when start_date is not null and end_date is not null and end_date > start_date and end_date - start_date >= 28 then end_date - start_date
		 when quantity_float = 0 and refills is null then 30
		 when quantity_float = 0 and refills~E'^\\d+$' then 30 * (refills::real +1)
		 when quantity_float is not null and refills~E'^\\d+$' then quantity_float * (refills::real +1) 
		 when quantity_float is null and refills~E'^\\d+$' then 30 * (refills::real +1)  
		 when quantity_float is not null and refills is null then quantity_float * 1 
		 when quantity_float is null and refills is null then 30 
		 else 0 end number_of_days,

	T2.name, T2.date, T3.date, T2.refills, T2.quantity, T2.quantity_float, T2.start_date, T2.end_date
	FROM gen_pop_tools.cc_hiv_risk_score T1
	JOIN emr_prescription T2 on (T1.patient_id = T2.patient_id)
	JOIN hef_event T3 on (T1.patient_id = T3.patient_id and T3.name in('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine'))
	where rpt_year = '2020'
	AND T3.name in('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
	AND T2.id = T3.object_id
	order by number_of_days) T1 GROUP BY patient_id) T15 on (T1.patient_id = T15.patient_id)
LEFT JOIN (select T1.patient_id, 'Y'::text as gon_case from gen_pop_tools.cc_hiv_risk_score T1 
			JOIN nodis_case T2 on (T1.patient_id = T2.patient_id) WHERE condition = 'gonorrhea'
			AND rpt_year = '2020' GROUP BY T1.patient_id, condition) T16 on (T1.patient_id = T16.patient_id)	
LEFT JOIN (select T1.patient_id, 'Y'::text as chlam_case from gen_pop_tools.cc_hiv_risk_score T1 
			JOIN nodis_case T2 on (T1.patient_id = T2.patient_id) WHERE condition = 'chlamydia'
			AND rpt_year = '2020' GROUP BY T1.patient_id, condition) T17 on (T1.patient_id = T17.patient_id)
LEFT JOIN (select T1.patient_id, 'Y'::text as syph_case from gen_pop_tools.cc_hiv_risk_score T1 
			JOIN nodis_case T2 on (T1.patient_id = T2.patient_id) WHERE condition = 'syphilis'
			AND rpt_year = '2020' GROUP BY T1.patient_id, condition) T18 on (T1.patient_id = T18.patient_id)
LEFT JOIN (select T1.patient_id, 'Y'::text as gon_and_chlam_case from gen_pop_tools.cc_hiv_risk_score T1
			JOIN nodis_case T2 on (T1.patient_id = T2.patient_id) WHERE condition = 'gonorrhea'
			AND rpt_year = '2020'
			AND T1.patient_id in (select patient_id from nodis_case where condition = 'chlamydia')
			GROUP BY T1.patient_id, condition) T19 on (T1.patient_id = T19.patient_id)
LEFT JOIN (SELECT patient_id, count(distinct(date)) as truvada_rx_num from hef_event where name in('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') GROUP by patient_id) T20 on (T1.patient_id = T20.patient_id)
where rpt_year = '2020'
--and hiv_risk_score >= .01
GROUP BY T1.patient_id, hiv_risk_score, race, gender, age_group, truvada_rx_yn, truvada_ever, hiv_test_2yr, bicillin_1yr, pos_gon_2yr, bicillin_2yr, suboxone_2yr,
chlam_tst_ever, hiv_tst_ever, syph_diag_ever, vd_cont_dx_ever, hiv_couns_2yr, english_lang, total_truvada_days, gon_case, chlam_case, syph_case, gon_and_chlam_case, truvada_rx_num
order by hiv_risk_score;