
-- FOR BMC Mark Patients Over Age 65 as Low Risk (Per Jessica Stewart)

DROP TABLE IF EXISTS kre_report.hiv_risk_pats_to_add_dispositions;
CREATE TABLE kre_report.hiv_risk_pats_to_add_dispositions AS
SELECT DISTINCT T1.patient_id, date_part('year',age(T2.date_of_birth)) current_age
from hiv_risk_patient_linelist T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN hiv_risk_patient_dispositions T3 ON (T1.patient_id = T3.patient_id)
WHERE date_part('year',age(T2.date_of_birth)) > 65
-- only select pats without a dispo or the dispo is older than 1 year 
AND disposition_date is null or disposition_date <= now() - INTERVAL '1 year';


INSERT INTO hiv_risk_patient_dispositions (id, prep_date_entered, disposition_date, comment, created_timestamp, created_by_user, disposition_id, managing_provider_id, patient_id)
SELECT (NEXTVAL('hiv_risk_patient_dispositions_id_seq')), null, now()::date, 'Dispostion Automatically Created: Patient age >65', now(), 'esp', 1, 5109, patient_id FROM kre_report.hiv_risk_pats_to_add_dispositions;

