BUCKETS
======
STD TESTING
STD POS
STD + SUBOXONE




- Upper age limit for inclusion?
- Exclude patients that have previously had rx for Truvada?



Buckets/Categories

1. syph dx + rx for bicillin in last 2 years
2. pos gon test in last 2 years + hiv testing (any test type) in last 2 years
3. any result std testing (gon/chlam/hiv) in last 2 years + suboxone


CREATE TABLE hiv_rpt.hrp_hist_pat_sum_x_value_kretest AS
SELECT patient_id, 
(i.sex * c.sex) as sex_sum,
(i.race_black * c.race_black) as black_sum,
(i.race_caucasian * c.race_caucasian) as caucasian_sum,
(i.english_lang * c.english_lang) as english_sum,
(i.has_language_data * c.has_language_data) as lang_sum,
(i.years_of_data * c.years_of_data) as yrs_data_sum,
(i.has_1yr_data * c.has_1yr_data) as yr1_data_sum,
(i.has_2yr_data * c.has_2yr_data) as yr2_data_sum,
(i.t_hiv_rna_1_yr * c.t_hiv_rna_1_yr) as hiv_rna_1yr_sum,
(i.t_hiv_test_2yr * c.t_hiv_test_2yr) as hiv_test_2yr_sum,
(i.t_hiv_test_e * c.t_hiv_test_e) as hiv_test_e_sum,
(i.t_hiv_elisa_e * c.t_hiv_elisa_e) as hiv_elisa_e_sum,
(i.acute_hiv_test_e * c.acute_hiv_test_e) as acute_hiv_test_e_sum,
(i.acute_hiv_test_2yr * c.acute_hiv_test_2yr) as acute_hiv_test_2yr_sum,
(i.t_pos_gon_test_2yr * c.t_pos_gon_test_2yr) as pos_gon_2yr_sum,
(i.t_chla_test_t_e * c.t_chla_test_t_e) as t_chla_test_e_sum,
(i.rx_bicillin_1_yr * c.rx_bicillin_1_yr) as rx_bicill_1yr_sum,
(i.rx_bicillin_2yr * c.rx_bicillin_2yr) as rx_bicill_2_yr_sum,
(i.rx_bicillin_e * c.rx_bicillin_e) as rx_bicillin_e_sum,
(i.rx_suboxone_2yr * c.rx_suboxone_2yr) as suboxone_sum,
(i.syphilis_any_site_state_x_late_e * c.syphilis_any_site_state_x_late_e) as syph_dx_sum,
(i.contact_w_or_expo_venereal_dis_e * c.contact_w_or_expo_venereal_dis_e) as contact_sum,
(i.hiv_counseling_2yr * c.hiv_counseling_2yr) as hiv_counsel_sum
FROM hiv_rpt.hrp_hist_index_data i,
hiv_rpt.hrp_hist_coefficients c;






select T1.patient_id, hiv_risk_score, greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum),

 (1 / (1 + exp(-(greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum))))),
 
  (1 / (1 + exp(-(sex_sum + black_sum + caucasian_sum + english_sum + lang_sum + yrs_data_sum + yr1_data_sum + yr2_data_sum + hiv_rna_1yr_sum + hiv_test_2yr_sum + hiv_test_e_sum + hiv_elisa_e_sum + acute_hiv_test_e_sum +  acute_hiv_test_2yr_sum + pos_gon_2yr_sum + t_chla_test_e_sum + rx_bicill_1yr_sum + rx_bicill_2_yr_sum + rx_bicillin_e_sum + suboxone_sum + syph_dx_sum + contact_sum + hiv_counsel_sum)))),
  
   (sex_sum + black_sum + caucasian_sum + english_sum + lang_sum + yrs_data_sum + yr1_data_sum + yr2_data_sum + hiv_rna_1yr_sum + hiv_test_2yr_sum + hiv_test_e_sum + hiv_elisa_e_sum + acute_hiv_test_e_sum +  acute_hiv_test_2yr_sum + pos_gon_2yr_sum + t_chla_test_e_sum + rx_bicill_1yr_sum + rx_bicill_2_yr_sum + rx_bicillin_e_sum + suboxone_sum + syph_dx_sum + contact_sum + hiv_counsel_sum) total_sum,
  
 greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum) / (sex_sum + black_sum + caucasian_sum + english_sum + lang_sum + yrs_data_sum + yr1_data_sum + yr2_data_sum + hiv_rna_1yr_sum + hiv_test_2yr_sum + hiv_test_e_sum + hiv_elisa_e_sum + acute_hiv_test_e_sum +  acute_hiv_test_2yr_sum + pos_gon_2yr_sum + t_chla_test_e_sum + rx_bicill_1yr_sum + rx_bicill_2_yr_sum + rx_bicillin_e_sum + suboxone_sum + syph_dx_sum + contact_sum + hiv_counsel_sum) as percentage,
 
 
  (case when 
	              sex_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum) then 'sex_sum'
             when 
			      black_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum) then 'black_sum'
             when 
			      caucasian_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'caucasian_sum'
			when 
			      english_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'english_sum'
			when 
			      lang_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'lang_sum'
			when 
			      yrs_data_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'yrs_data_sum'	
			when 
			      yr1_data_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'yr1_data_sum'	
			when 
			      yr2_data_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'yr2_data_sum'					  
 			when 
			      hiv_rna_1yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'hiv_rna_1yr_sum'	   				  
			when 
			      hiv_test_2yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'hiv_test_2yr_sum'					  
			when 
			      hiv_test_e_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'hiv_test_e_sum'					  
			when 
			      hiv_elisa_e_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'hiv_elisa_e_sum'	
			when 
			      acute_hiv_test_e_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'acute_hiv_test_e_sum'	
			when 
			      acute_hiv_test_2yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'acute_hiv_test_2yr_sum'	
			when 
			      pos_gon_2yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'pos_gon_2yr_sum'					  
 			when 
			      t_chla_test_e_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 't_chla_test_e_sum'	   				  
			when 
			      rx_bicill_1yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'rx_bicill_1yr_sum'					  
			when 
			      rx_bicill_2_yr_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'rx_bicill_2_yr_sum'					  
			when 
			      rx_bicillin_e_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'rx_bicillin_e_sum'	
 			when 
			      suboxone_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'suboxone_sum'	   				  
			when 
			      syph_dx_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'syph_dx_sum'					  
			when 
			      contact_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'contact_sum'					  
			when 
			      hiv_counsel_sum = greatest(sex_sum, black_sum, caucasian_sum, english_sum, lang_sum, yrs_data_sum, yr1_data_sum, yr2_data_sum, hiv_rna_1yr_sum, hiv_test_2yr_sum, hiv_test_e_sum, hiv_elisa_e_sum, acute_hiv_test_e_sum,  acute_hiv_test_2yr_sum, pos_gon_2yr_sum, t_chla_test_e_sum, rx_bicill_1yr_sum, rx_bicill_2_yr_sum, rx_bicillin_e_sum, suboxone_sum, syph_dx_sum, contact_sum, hiv_counsel_sum)then 'hiv_counsel_sum'	
	  
        end) as highes_coeff                
from hiv_rpt.hrp_hist_pat_sum_x_value_kretest T1
INNER JOIN emr_patient T2 on T1.patient_id = T2.id
INNER JOIN hiv_rpt.dphll_prep_ll_output T3 on T2.mrn = T3.mrn
order by hiv_risk_score desc;



select T2.highes_coeff as highest_risk_factor, 
case when T2.percentage >= 1.5 then 'Weighted to Highest Risk Factor'
when T2.percentage < 1.0 then 'High Variation Across Risk Factors'
else null end as risk_factor_summary,
ill_drug_use as reported_ill_drug_use_last_2yrs,
T1.hiv_risk_score, current_age, home_language, previous_prep_order, syph_dx_and_bicillin_and_contact_dx_all, syph_dx_and_bicillin_and_contact_dx_any, syph_dx_ever, cont_w_or_expo_ven_dis_dx_ever, bicillin_24_inj_ever, bupren_naloxone_last_2yr, pos_gon_tests_last_2yr, hiv_tst_dates_last_2yr
from hiv_rpt.dphll_prep_ll_output T1
INNER JOIN hiv_rpt.hrp_hist_pat_sum_x_value_kretest2 T2 ON T1.mrn = T2.mrn
LEFT JOIN (select distinct(patient_id), 'Y' as ill_drug_use from emr_socialhistory where ill_drug_use = 'YES' and date >= '2019-04-12') T3 on T2.patient_id = T3.patient_id
order by hiv_risk_score desc




CREATE TABLE hiv_rpt.hrp_hist_pat_sum_x_value_kretest3 AS
select patient_id, sex_sum as master_sum, 'sex_sum' as description FROM 
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, black_sum as master_sum, 'black_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, caucasian_sum as master_sum, 'caucasian_sum' as description  FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, english_sum as master_sum, 'english_sum' as description  FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, lang_sum as master_sum, 'lang_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, yrs_data_sum as master_sum, 'yrs_data_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, yr1_data_sum as master_sum, 'yr1_data_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, yr2_data_sum as master_sum, 'yr2_data_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, hiv_rna_1yr_sum as master_sum, 'hiv_rna_1yr_sum' as description  FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, hiv_test_2yr_sum as master_sum, 'hiv_test_2yr_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, hiv_test_e_sum as master_sum, 'hiv_test_e_sum ' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, hiv_elisa_e_sum as master_sum, 'hiv_elisa_e_sum' as description  FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, acute_hiv_test_e_sum as master_sum, 'acute_hiv_test_e_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, acute_hiv_test_2yr_sum as master_sum, 'acute_hiv_test_2yr_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, pos_gon_2yr_sum as master_sum, 'pos_gon_2yr_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, t_chla_test_e_sum as master_sum, 't_chla_test_e_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, rx_bicill_1yr_sum as master_sum, 'rx_bicill_1yr_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, rx_bicill_2_yr_sum as master_sum, 'rx_bicill_2_yr_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, rx_bicillin_e_sum as master_sum, 'rx_bicillin_e_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, suboxone_sum as master_sum, 'suboxone_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, syph_dx_sum as master_sum, 'syph_dx_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, contact_sum as master_sum, 'contact_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest
UNION
select patient_id, hiv_counsel_sum as master_sum, 'hiv_counsel_sum' as description FROM
hiv_rpt.hrp_hist_pat_sum_x_value_kretest;


DROP TABLE IF EXISTS hiv_rpt.hrp_hist_pat_sum_x_value_kretest4;
CREATE TABLE hiv_rpt.hrp_hist_pat_sum_x_value_kretest4 
AS
select patient_id,
description, master_sum,
RANK () OVER ( 
		PARTITION BY patient_id
		ORDER BY master_sum DESC
	) risk_factor_rank
FROM 
hiv_rpt.hrp_hist_pat_sum_x_value_kretest3 T1
INNER JOIN emr_patient T2 on T1.patient_id = T2.id
INNER JOIN hiv_rpt.dphll_prep_ll_output T3 on T2.mrn = T3.mrn


select * from hiv_rpt.hrp_hist_pat_sum_x_value_kretest4 
where patient_id = 74745413

select T1.patient_id, T2.description as top_rank
from hiv_rpt.hrp_hist_pat_sum_x_value_kretest4 T1
INNER JOIN (select patient_id, description from hiv_rpt.hrp_hist_pat_sum_x_value_kretest4 where risk_factor_rank = 1) T2 on T1.patient_id = T2.patient_id 
where risk_factor_rank = 2
and (master_sum = 0 or master_sum = -0)
GROUP BY T1.patient_id, T2.description
order by T1.patient_id


select (distinct(T1.patient_id)), 1 as ill_drug_use_last_2yr
FROM hiv_rpt.dphll_index_pats T1
INNER JOIN emr_socialhistory T2 ON (T1.patient_id = T2.patient_id)
where upper(ill_drug_use) = 'YES'
date >= (now()::date - interval '2 years')
AND date <= now();




