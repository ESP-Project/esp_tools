
create table hiv_rpt.kre_bicillin_followup_all_rx as 
select 
T2.date as rx_date,
T1.*, T2.name, T2.yrs_since_rx
from hiv_rpt.kre_bicillin_followup T1
INNER JOIN hiv_rpt.hrp_hist_bicillin T2 ON (T1.patient_id = T2.patient_id);



-- rx occurs within 60 days on or after dx code
create table hiv_rpt.kre_bicillin_followup_exclude_dx_60 as 
select (T1.rx_date - T2.date) as rx_enc_days_diff, T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
from hiv_rpt.kre_bicillin_followup_all_rx T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE 
dx_code_id != 'icd9:799.9'
AND (T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
     OR T3.dx_code_id ~* 'icd10:O99.82|icd10:Z22.330')
AND (T1.rx_date - T2.date) >= 0
AND (T1.rx_date - T2.date) <= 60;


-- what is left from elimincating above
create table hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset as
select rx_date, patient_id, name from hiv_rpt.kre_bicillin_followup_all_rx
except
select rx_date, patient_id, rx_name from hiv_rpt.kre_bicillin_followup_exclude_dx_60;

--specific dx ever before prescription
create table hiv_rpt.kre_bicillin_followup_exclude_specific_dx_ever as 
select (T1.rx_date - T2.date) as rx_enc_days_diff, T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
from hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE 
dx_code_id != 'icd9:799.9'
AND (T3.dx_code_id ~* 'icd10:B95|icd10:O99.82|icd10:Z22.330')
AND (T1.rx_date - T2.date) > 0;


'Carrier of Group B streptococcus' ('icd10:Z22.330')
'Streptococcus B carrier state complicating pregnancy''icd10:O99.820'
'Streptococcus, group B, causing diseases classd elswhr' 'icd10:B95.1'

create table hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset as
select rx_date, patient_id, name from hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset
except
select rx_date, patient_id, rx_name from hiv_rpt.kre_bicillin_followup_exclude_specific_dx_ever;


--never had a syph test
create table hiv_rpt.kre_bicillin_followup_no_syph_lab_ever as 
select * from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset
where patient_id not in (select patient_id from hef_event where name ~* 'lx:rpr|lx:vdrl|lx:tppa|lx:fta-abs|lx:tp-igg|lx:tp-cia|lx:tp-igm|lx:vdrl-csf|lx:tppa-csf|lx:fta-abs-csf');


-- never had a syph diag

select patient_id from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset
where patient_id not in (
--select T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
select T1.patient_id
from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' );


-- no syph lab or dx code ever

create table hiv_rpt.kre_bicillin_followup_no_syph_lab_or_dx_ever as
select patient_id from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset
where patient_id not in (
--select T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
select T1.patient_id
from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' ) 
and patient_id not in (select patient_id from hiv_rpt.kre_bicillin_followup_no_syph_lab_ever)

----------------------------------------------------------------
-----------------------------------------------------------------


-- starting at top and dropping those that never had positive syph lab or dx code
drop table if exists hiv_rpt.kre_bicillin_followup_pats_with_syph_lab_or_dx_ever;
create table hiv_rpt.kre_bicillin_followup_pats_with_syph_lab_or_dx_ever as
select * from hiv_rpt.kre_bicillin_followup_all_rx 
--where patient_id in (select patient_id from hef_event where name ~* 'lx:rpr|lx:vdrl|lx:tppa|lx:fta-abs|lx:tp-igg|lx:tp-cia|lx:tp-igm|lx:vdrl-csf|lx:tppa-csf|lx:fta-abs-csf')
where patient_id in (select patient_id from hef_event where name in ('lx:fta-abs-csf:positive', 'lx:fta-abs:positive', 'lx:rpr:positive', 'lx:tp-cia:positive', 'lx:tp-igg:positive', 'lx:tppa-csf:positive', 'lx:tppa:positive', 'lx:vdrl-csf:positive')
) or patient_id in (
	select T1.patient_id
	from hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset T1
	INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
	INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
	WHERE dx_code_id ~ '^(icd9:091.|icd9:092.|icd9:093.|icd9:094.|icd9:095.|icd10:A51.|icd10:A52.)' )
	
	
drop table if exists hiv_rpt.kre_bicillin_followup_exclude_dx_60;
create table hiv_rpt.kre_bicillin_followup_exclude_dx_60 as 
select (T1.rx_date - T2.date) as rx_enc_days_diff, T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
from hiv_rpt.kre_bicillin_followup_pats_with_syph_lab_or_dx_ever T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE 
dx_code_id != 'icd9:799.9'
AND (T3.dx_code_id ~* 'icd10:A69.2|icd10:B95|icd10:I00|icd10:I01|icd10:I02|icd10:I05|icd10:I06|icd10:I07|icd10:I08|icd10:I09|icd10:I89.0|icd10:I97.2|icd10:I97.89|icd10:J02|icd10:J03|icd10:J36|icd10:L03|icd10:Q82.0|icd10:Z86.7|icd9:034.0|icd9:041.0|icd9:088.81|icd9:390|icd9:391|icd9:392|icd9:393|icd9:394|icd9:395|icd9:396|icd9:397|icd9:398|icd9:457|icd9:462|icd9:463|icd9:475|icd9:682|icd9:757.0|icd9:V12.5'
     OR T3.dx_code_id ~* 'icd10:O99.82|icd10:Z22.330')
AND (T1.rx_date - T2.date) >= 0
AND (T1.rx_date - T2.date) <= 60;

create table hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset as
select rx_date, patient_id, name from hiv_rpt.kre_bicillin_followup_pats_with_syph_lab_or_dx_ever
except
select rx_date, patient_id, rx_name from hiv_rpt.kre_bicillin_followup_exclude_dx_60;


drop table if exists hiv_rpt.kre_bicillin_followup_exclude_specific_dx_ever;
create table hiv_rpt.kre_bicillin_followup_exclude_specific_dx_ever as 
select (T1.rx_date - T2.date) as rx_enc_days_diff, T1.patient_id, rx_date, T2.date as enc_date, T4.name as dx_name, T1.name as rx_name, T3.dx_code_id
from hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 on (T2.id = T3.encounter_id)
INNER JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE 
dx_code_id != 'icd9:799.9'
AND (T3.dx_code_id ~* 'icd10:B95|icd10:O99.82|icd10:Z22.330')
AND (T1.rx_date - T2.date) > 0;


drop table if exists hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset;
create table hiv_rpt.kre_bicillin_followup_exclude_dx_ever_subset as
select rx_date, patient_id, name from hiv_rpt.kre_bicillin_followup_exclude_dx_60_subset
except
select rx_date, patient_id, rx_name from hiv_rpt.kre_bicillin_followup_exclude_specific_dx_ever;



+--------------------+--------------------------------------------------+----------------------------------------------------------------+--------------------+
| (No column name)   | description                                      | display_name                                                   | hv_discrete_dose   |
|--------------------+--------------------------------------------------+----------------------------------------------------------------+--------------------|
| 3                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 0.6 Million Units | .6                 |
| 536                | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 1.2 Million Units | 1.2                |
| 1                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | NULL                                                           | 2.4                |
| 55                 | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 2.4 Million Units | 2.4                |
| 1                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 150,000 Units     | 50000              |
| 1                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 156,000 Units     | 50000              |
| 1                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 180,000 Units     | 50000              |
| 1                  | PENICILLIN G BENZATHINE 1200000 UNIT/2ML IM SUSP | penicillin G benzathine (BICILLIN) injection 600,000 Units     | 600000             |
+--------------------+--------------------------------------------------+----------------------------------------------------------------+--------------------+

dosage or hv_discrete_dose


