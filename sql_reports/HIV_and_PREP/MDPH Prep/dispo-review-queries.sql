select --patient_id, 
T1.id, disposition_id, disposition_name, comment, created_timestamp::date created_date
FROM hiv_risk_patient_dispositions T1
JOIN hiv_risk_static_dispositions T2 ON (T1.disposition_id = T2.id)
WHERE T1.created_timestamp >= '2024-07-01'
order by created_timestamp;
