--risk factor - IDU	karen
--number of quarters they have appeared	karen
--do you want birth_sex	kshema




-- -- NEEDED FOR REPORTING
-- -- ONE TIME RUN ONLY
-- CREATE SEQUENCE hiv_rpt.hiv_risk_pats_masked_id_seq;


-- CREATE TABLE hiv_rpt.hiv_risk_masked_patients
-- (
    -- patient_id integer,
    -- masked_patient_id character varying(128)
-- );


-- CREATE A MASKED PATIENT_ID IF ONE DOES NOT ALREADY EXIST
-- CHA-, BMC-, ATR-, FWY-, GLFHC-

INSERT INTO hiv_rpt.hiv_risk_masked_patients
SELECT 
DISTINCT patient_id,
CONCAT(:site_abbr, NEXTVAL('hiv_rpt.hiv_risk_pats_masked_id_seq')) masked_patient_id
FROM hiv_risk_patient_linelist_history
WHERE patient_id not in (select patient_id from hiv_rpt.hiv_risk_masked_patients)
GROUP by patient_id;

DROP TABLE IF EXISTS hiv_rpt.hiv_risk_outcome_report_orig;
CREATE TABLE hiv_rpt.hiv_risk_outcome_report_orig AS
SELECT DISTINCT 
T1.patient_id as esp_pat_id, 
now()::date as report_generation_date,
T9.masked_patient_id as masked_patient_id,
date_part('year',age(T2.date_of_birth)) current_age,
CASE   
	when date_part('year', age(date_of_birth)) <= 15 then '0-15'    
	when date_part('year', age(date_of_birth)) <= 24 then '16-24'  
	when date_part('year', age(date_of_birth)) <= 34 then '25-34'
	when date_part('year', age(date_of_birth)) <= 44 then '34-44' 
	when date_part('year', age(date_of_birth)) <= 54 then '45-54'   
	when date_part('year', age(date_of_birth)) <= 64 then '55-64'  
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>= 65' 
	END age_group,
CASE 
    WHEN gender is null or gender in ('UNKNOWN', '', 'U') then 'U'
	WHEN gender in ('M', 'MALE') then 'M'
	WHEN gender in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,		
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
CASE WHEN gender_identity is null or gender_identity = '' or gender_identity ilike 'OFFICE USE ONLY%' THEN 'UNKNOWN' 
     WHEN upper(gender_identity) in ('PATIENT''S GENDER IDENTITY IS NOT LISTED', 'GENDERQUEER/GENDER NONCONFORMING NEITHER EXCLUSIVELY MALE NOR FEMALE', 'GENDER NON-CONFORMING', 'TRANSFEMININE', 'TRANSMASCULINE', 'GENDER QUEER', 'NON-BINARY') THEN 'OTHER'
	 WHEN upper(gender_identity) in ('PATIENT CHOOSES NOT TO ANSWER', 'CHOOSE NOT TO DISCLO')  THEN 'CHOOSE NOT TO DISCLOSE' 
	 ELSE upper(gender_identity) END gend_iden,
CASE WHEN sex_orientation is null or sex_orientation = '' or sex_orientation ilike 'OFF% USE ONLY%' or upper(sex_orientation) in ('NOT REPORTED', 'UNKNOWN') THEN 'UNKNOWN' 
     WHEN upper(sex_orientation) in ('PATIENT''S SEXUAL ORIENTATION IS NOT LISTED', 'SOMETHING ELSE', 'QUEER, PANSEXUAL, AND/OR QUESTIONING', 'ASEXUAL', 'PANSEXUAL', 'QUEER', 'DEMISEXUAL') THEN 'OTHER' 
	 WHEN upper(sex_orientation) in ('PATIENT CHOOSES NOT TO ANSWER', 'NOTDISCLOSED') THEN 'CHOOSE NOT TO DISCLOSE' 
	 WHEN upper(sex_orientation) in ('STRAIGHT OR HETEROSEXUAL') THEN 'STRAIGHT'
	 WHEN upper(sex_orientation) in ('GAY', 'LESBIAN', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL') THEN 'LESBIAN OR GAY'
	 WHEN upper(sex_orientation) in ('''DON''''T KNOW''') THEN 'DON''T KNOW'
	 WHEN upper(sex_orientation) in ('BI') THEN 'BISEXUAL'
	 ELSE upper(sex_orientation) END sex_or,
--concat(extract(year from min(ll_date)), ' Q', extract(quarter from min(ll_date)) ) first_linelist_quarter,
min(T1.ll_date) first_linelist_date,
max(T1.ll_date) most_recent_linelist_date,
CASE WHEN T3.patient_id is not null THEN 1 ELSE 0 END active_linelist_patient,
T6.disposition_name as disposition,
T5.comment,
(now()::date - disposition_date::date) disposition_age_in_days,
MAX(CASE WHEN T6.id in (9,10,11,12,13,14) THEN 'YES' END) prep_discussion,
MAX(CASE WHEN T6.id in (6, 14, 15) then 'YES' END) prep_per_disposition,
MAX(CASE WHEN T7.date >= T8.first_ll_date then 'YES' END) prep_per_rx,
MAX(CASE WHEN T1.ll_date = T10.last_ll_date then T1.hiv_risk_score END) most_recent_risk_score,
MAX(CASE WHEN T1.ll_date = T10.last_ll_date then T1.highest_risk_variable END) most_recent_highest_risk_variable
FROM hiv_risk_patient_linelist_history T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN hiv_risk_patient_linelist T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN (SELECT patient_id, max(id) most_recent_dispo_id FROM hiv_risk_patient_dispositions GROUP BY patient_id) T4 ON (T1.patient_id = T4.patient_id)
LEFT JOIN hiv_risk_patient_dispositions T5 ON (T1.patient_id = T5.patient_id and T4.patient_id = T5.patient_id and T4.most_recent_dispo_id = T5.id)
LEFT JOIN hiv_risk_static_dispositions T6 ON (T5.disposition_id = T6.id)
LEFT JOIN hef_event T7 ON (T1.patient_id = T7.patient_id and T7.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
'rx:hiv_cabotegravir_er_600')) 
LEFT JOIN (SELECT patient_id, min(ll_date) first_ll_date FROM hiv_risk_patient_linelist_history GROUP BY patient_id) T8 ON (T1.patient_id = T8.patient_id)
JOIN hiv_rpt.hiv_risk_masked_patients T9 ON (T1.patient_id = T9.patient_id)
LEFT JOIN (SELECT patient_id, max(ll_date) last_ll_date FROM hiv_risk_patient_linelist_history GROUP BY patient_id) T10 ON (T1.patient_id = T10.patient_id)
GROUP BY T1.patient_id, T2.date_of_birth, sex, race_group, ethnicity_group, gend_iden, sex_or, T3.patient_id, disposition_name, T9.masked_patient_id, disposition_date, T5.comment;


DROP TABLE IF EXISTS hiv_rpt.hiv_risk_outcome_report_rf;
CREATE TABLE hiv_rpt.hiv_risk_outcome_report_rf as
SELECT T1.*,
CASE WHEN T1.sex = 'M' and ( upper(ever_sex_with_male) = 'YES' or upper(past12mo_sex_with_male) = 'YES') THEN 'MSM'
     WHEN T1.sex = 'M' and ( upper(ever_sex_with_female) = 'YES' or upper(past12mo_sex_with_female) = 'YES' ) 
	                      and ( (upper(ever_sex_with_hiv) != 'YES' or ever_sex_with_hiv is null) and (upper(past12mo_sex_with_hiv) != 'YES' or past12mo_sex_with_hiv is null) ) 
						  and ( (upper(ever_sex_with_ivuser) != 'YES' or ever_sex_with_ivuser is null) and (upper(past12mo_sex_with_ivuser) != 'YES' or past12mo_sex_with_ivuser is null) )
						  and ( (upper(ever_exchange_sex) != 'YES' or ever_exchange_sex is null) and (upper(past12mo_exchange_sex) != 'YES' or past12mo_exchange_sex is null) )
						  and ( (upper(ever_sex_while_ui) != 'YES' or ever_sex_while_ui is null) and (upper(past12mo_sex_while_ui) != 'YES' or past12mo_sex_while_ui is null) )
						  THEN 'HET'
	 WHEN T1.sex = 'F' and ( upper(ever_sex_with_male) = 'YES' or upper(past12mo_sex_with_male) = 'YES' ) 
	                      and ( (upper(ever_sex_with_hiv) != 'YES' or ever_sex_with_hiv is null) and (upper(past12mo_sex_with_hiv) != 'YES' or past12mo_sex_with_hiv is null) ) 
						  and ( (upper(ever_sex_with_ivuser) != 'YES' or ever_sex_with_ivuser is null) and (upper(past12mo_sex_with_ivuser) != 'YES' or past12mo_sex_with_ivuser is null) )
						  and ( (upper(ever_exchange_sex) != 'YES' or ever_exchange_sex is null) and (upper(past12mo_exchange_sex) != 'YES' or past12mo_exchange_sex is null) )
						  and ( (upper(ever_sex_while_ui) != 'YES' or ever_sex_while_ui is null) and (upper(past12mo_sex_while_ui) != 'YES' or past12mo_sex_while_ui is null) )
						  THEN 'HET'
	 WHEN T1.sex = 'F' and ( upper(ever_sex_with_male) = 'YES' or upper(past12mo_sex_with_male) = 'YES' ) 
	                      and ( upper(ever_sex_with_hiv) = 'YES'  or upper(past12mo_sex_with_hiv) = 'YES'
                                or upper(ever_sex_with_ivuser) = 'YES' or upper(past12mo_sex_with_ivuser) = 'YES'
								or upper(ever_exchange_sex) = 'YES' or upper(past12mo_exchange_sex) = 'YES'
								or upper(ever_sex_while_ui) = 'YES' or upper(past12mo_sex_while_ui) = 'YES' )
						  THEN 'HRH'
	WHEN T1.sex = 'M' and ( upper(ever_sex_with_female) = 'YES' or upper(past12mo_sex_with_female) = 'YES' ) 
	                     and ( upper(ever_sex_with_hiv) = 'YES'  or upper(past12mo_sex_with_hiv) = 'YES'
                                or upper(ever_sex_with_ivuser) = 'YES' or upper(past12mo_sex_with_ivuser) = 'YES'
								or upper(ever_exchange_sex) = 'YES' or upper(past12mo_exchange_sex) = 'YES'
								or upper(ever_sex_while_ui) = 'YES' or upper(past12mo_sex_while_ui) = 'YES' )
						 THEN 'HRH'
	WHEN T1.sex in ('M', 'F') and ( upper(ever_sex_with_male) != 'YES' or ever_sex_with_male is null ) 
	                             and ( upper(past12mo_sex_with_male) != 'YES' or past12mo_sex_with_male is null )
								 and ( upper(ever_sex_with_female) != 'YES' or ever_sex_with_female is null )
								 and ( upper(past12mo_sex_with_female) != 'YES' or past12mo_sex_with_female is null)
								 and ( upper(ever_sex_with_hiv) = 'YES'  or upper(past12mo_sex_with_hiv) = 'YES'
                                       or upper(ever_sex_with_ivuser) = 'YES' or upper(past12mo_sex_with_ivuser) = 'YES'
								       or upper(ever_exchange_sex) = 'YES' or upper(past12mo_exchange_sex) = 'YES'
								       or upper(ever_sex_while_ui) = 'YES' or upper(past12mo_sex_while_ui) = 'YES'
                                       or upper(ever_sex_with_hepc) = 'YES' or upper(past12mo_sex_with_hepc) = 'YES')
	    				 THEN 'OTH_SEX_RISK'
	WHEN T1.sex not in ('M', 'F') and ( upper(ever_sex_with_male) = 'YES' or upper(past12mo_sex_with_male) = 'YES'
                                           or upper(ever_sex_with_female) = 'YES' or upper(past12mo_sex_with_female) = 'YES')
								     and ( upper(ever_sex_with_hiv) = 'YES'  or upper(past12mo_sex_with_hiv) = 'YES'
                                       or upper(ever_sex_with_ivuser) = 'YES' or upper(past12mo_sex_with_ivuser) = 'YES'
								       or upper(ever_exchange_sex) = 'YES' or upper(past12mo_exchange_sex) = 'YES'
								       or upper(ever_sex_while_ui) = 'YES' or upper(past12mo_sex_while_ui) = 'YES'
                                       or upper(ever_sex_with_hepc) = 'YES' or upper(past12mo_sex_with_hepc) = 'YES')
	    				 THEN 'OTH_SEX_RISK'
    END rf_sexual_risk,
CASE WHEN upper(ever_injected_nonrx_drug) = 'YES' or upper(past12mo_injected_nonrx_drug) = 'YES' THEN 'IDU'
     WHEN ( ( (upper(ever_injected_nonrx_drug) != 'YES' or ever_injected_nonrx_drug is null) and ( upper(past12mo_injected_nonrx_drug) != 'YES' or past12mo_injected_nonrx_drug is null) )
	           and ( upper(ever_nonrx_intranasal_drug_use) = 'YES' or upper(past12mo_nonrx_intranasal_drug_use) = 'YES' 
			         or upper(ever_nonrx_drug_use) = 'YES' or upper(past12mo_nonrx_drug_use) = 'YES' ) ) THEN 'NON-IDU'
	 WHEN (upper(ever_injected_nonrx_drug) != 'YES' or ever_injected_nonrx_drug is null) 
	       and (upper(past12mo_injected_nonrx_drug) != 'YES' or past12mo_injected_nonrx_drug is null)
		   and (upper(ever_nonrx_intranasal_drug_use) != 'YES' or ever_nonrx_intranasal_drug_use is null)
		   and (upper(past12mo_nonrx_intranasal_drug_use) != 'YES' or past12mo_nonrx_intranasal_drug_use is null)
		   and (upper(ever_nonrx_drug_use) != 'YES' or ever_nonrx_drug_use is null)
		   and (upper(past12mo_nonrx_drug_use) != 'YES' or past12mo_nonrx_drug_use is null)
		   and (upper(ever_share_drug_equip) = 'YES' or upper(past12mo_share_drug_equip) = 'YES')
		   THEN 'OTH_SUD_RISK'
     END rf_drug_use,
CASE WHEN upper(ever_incarcerated) = 'YES' or upper(past12mo_incarcerated) = 'YES' THEN 'YES' END as rf_incarceration_hist,
CASE WHEN upper(ever_homeless) = 'YES' or upper(past12mo_homeless) = 'YES' THEN 'YES' END as rf_homeless_hist
FROM hiv_rpt.hiv_risk_outcome_report_orig T1
LEFT JOIN (SELECT patient_id, max(encounter_date) as max_enc_date from emr_risk_factors group by patient_id) T2 ON (T1.esp_pat_id = T2.patient_id)
LEFT JOIN emr_risk_factors T3 ON (T1.esp_pat_id = T3.patient_id and T2.patient_id = T3.patient_id and T2.max_enc_date = T3.encounter_date);

-- update spec
-- review risk scores for patients that actually have risk factor info
-- max date on risk factor data?

DROP TABLE IF EXISTS hiv_rpt.hiv_risk_outcome_report;
CREATE TABLE hiv_rpt.hiv_risk_outcome_report AS
SELECT 
report_generation_date,
masked_patient_id,
age_group,
sex as gender,
race_group as race,
ethnicity_group as ethnicity,
gend_iden as gender_identity,
sex_or as sex_orientation,
first_linelist_date,
most_recent_linelist_date,
active_linelist_patient,
disposition,
comment,
disposition_age_in_days,
prep_discussion,
prep_per_disposition,
prep_per_rx,
most_recent_risk_score,
most_recent_highest_risk_variable,
rf_sexual_risk,
rf_drug_use,
rf_incarceration_hist,
rf_homeless_hist
FROM hiv_rpt.hiv_risk_outcome_report_rf;


/* \COPY (select* from hiv_rpt.hiv_risk_outcome_report) TO '/tmp/2025-01-02-hiv_risk_outcome_report.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n 2025-01-02-hiv_risk_outcome_report.csv -s "CHA HIV RISK OUTCOME REPORT"

\COPY (select* from hiv_rpt.hiv_risk_outcome_report) TO '/tmp/2025-01-02-hiv_risk_outcome_report.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n 2025-01-02-hiv_risk_outcome_report.csv -s "FWY HIV RISK OUTCOME REPORT"

\COPY (select* from hiv_rpt.hiv_risk_outcome_report) TO '/tmp/2025-01-02-hiv_risk_outcome_report.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f noreply@glfhc.org -r keberhardt@commoninf.com -p /tmp -n 2025-01-02-hiv_risk_outcome_report.csv -s "GLFHC HIV RISK OUTCOME REPORT"

\COPY (select* from hiv_rpt.hiv_risk_outcome_report) TO '/tmp/2025-01-02-hiv_risk_outcome_report.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r keberhardt@commoninf.com -p /tmp -n 2025-01-02-hiv_risk_outcome_report.csv -s "BMC HIV RISK OUTCOME REPORT"

\COPY (select* from hiv_rpt.hiv_risk_outcome_report) TO '/tmp/2025-01-02-hiv_risk_outcome_report.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n 2025-01-02-hiv_risk_outcome_report.csv -s "ATRIUS HIV RISK OUTCOME REPORT" */



	 


