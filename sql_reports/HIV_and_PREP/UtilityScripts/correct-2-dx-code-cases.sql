-- should change this so min and max dx codes are the same date for the match versus 2 as sometimes there are 3

create table kre_report.temp_possible_fp_dx as 
select T2.id as case_id, T2.status, total_hiv_dx_count, T1.patient_id, name, T1.date
from hef_event T1
JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
JOIN (SELECT patient_id, count(*) total_hiv_dx_count from hef_event where name = 'dx:hiv' group by patient_id) T3 ON (T1.patient_id = T3.patient_id and T2.patient_id = T3.patient_id)
where name = 'dx:hiv'
and condition = 'hiv'
and T1.date = T2.date
and criteria = 'Criteria 1e. >=2 ICD codes for HIV and history of prescription for >=3 HIV meds ever'
and total_hiv_dx_count = 2
group by T2.id, T1.patient_id, name, T1.date, total_hiv_dx_count
having count(*) > 1;



select case_id from kre_report.temp_possible_fp_dx;

copy output and paste in file /tmp/cases_to_delete.txt

sh
for i in `cat /tmp/cases_to_delete.txt`
do
echo $i
/srv/esp/prod/bin/esp delete_case_report $i
done

delete from nodis_case_events where case_id in (select case_id from kre_report.temp_possible_fp_dx);

delete from nodis_report_cases where case_id in (select case_id from kre_report.temp_possible_fp_dx);

delete from nodis_case where id in (select case_id from kre_report.temp_possible_fp_dx);

nohup ./bin/esp nodis hiv &

Change case status and prime them
