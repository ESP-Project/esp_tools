-- Get cases that need to be deleted (and potentially re-created)
drop table if exists temp_hiv_pax_cases;
create table temp_hiv_pax_cases AS
select DISTINCT T2.id as case_id, date, criteria, status
from nodis_case_events T1
JOIN nodis_case T2 ON (T1.case_id = T2.id)
where event_id in (select id from hef_event where name = 'rx:hiv_ritonavir' and object_id in (select id from emr_prescription where name ilike '%NIRMATRELVIR%'))
and T2.date >= '2020-01-01'
and (criteria ilike '%meds%' or criteria ilike '%prescriptions%');


-- Delete all nodis_case_events (regardless if it's a case we are deleting or not )
delete from nodis_case_events where event_id in (select id from hef_event where name = 'rx:hiv_ritonavir' and object_id in (select id from emr_prescription where name ilike '%NIRMATRELVIR%'));

-- Delete all nodis_case_events for the cases we are deleting
delete from nodis_case_events where case_id in (select case_id from temp_hiv_pax_cases);

-- nodis_reported
delete from nodis_reported where case_reported_id in (select id from nodis_casereportreported where case_report_id in 
(select id from nodis_casereport where case_id in 
(select case_id from temp_hiv_pax_cases) ) );
			
delete from nodis_casereportreported where case_report_id in (select id from nodis_casereport where case_id in (select case_id from temp_hiv_pax_cases) );
delete from nodis_casereport where case_id in (select case_id from temp_hiv_pax_cases);
delete from nodis_casestatushistory where case_id in (select case_id from temp_hiv_pax_cases);
delete from nodis_report_cases where case_id in (select case_id from temp_hiv_pax_cases);
delete from nodis_case where id in (select case_id from temp_hiv_pax_cases );

-- delete all the hef events regardless of case association 
delete from hef_event where object_id in (select id from emr_prescription where name ilike '%NIRMATRELVIR%') and name = 'rx:hiv_ritonavir';

-- update HIV Plug-In to New Version 1.20

-- run hef for the rx
-- no events should be created
nohup ./bin/esp hef prescription:hiv_ritonavir &

-- run nodis for hiv
nohup ./bin/esp nodis hiv &

-- Change transport to testing in application.ini

-- prime cases recreated
-- ASSUMES ONLY CASES QUEUED ARE THE RE_CREATED HIV CASES
nohup ./bin/esp case_report --mdph --transmit &

-- Change transport back to script in application.ini

-- Cleanup
drop table if exists temp_hiv_pax_cases;


-- ATRIUS 22 deleted, 19 re-created
-- CHA 2 deleted, 2 re-created
-- BMC  10 deleted m 10 re-created
-- FENWAY 0 deleted
-- PPLM 0 deleted
-- MLCHC 62 deleted 
-- HealthNet 3 deleted, 3 re-created
-- East Boston - 0 deleted
-- BayState  0 delted
-- FHCW 0 deleted
-- LYNN - 0 deleted







