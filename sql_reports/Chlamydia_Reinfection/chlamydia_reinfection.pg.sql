--here are all the hef events we want for pos chlamydia;
-- ~30 seconds
-- 10-28-2018 adding spec source info 
drop table if exists temp_hef_poschlamyd;
CREATE  TABLE temp_hef_poschlamyd as
  select distinct hef_event.patient_id, hef_event.provider_id, hef_event.date, 
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP)') THEN 'UROG'
ELSE specimen_source END specimen_source
 from hef_event, emr_labresult 
where name = 'lx:chlamydia:positive' 
and hef_event.object_id = emr_labresult.id;
--Delete out some dups 
--due to separate lab records with different specimen sources
--select * from temp_hef_poschlamyd
delete from temp_hef_poschlamyd first
where first.ctid <> (select min(dups.ctid) from temp_hef_poschlamyd  dups where first.patient_id=dups.patient_id and first.provider_id=dups.provider_id and first.date=dups.date);
alter table temp_hef_poschlamyd add primary key (patient_id,provider_id,date);
create index on temp_hef_poschlamyd (patient_id);
create index on temp_hef_poschlamyd (provider_id);
create index on temp_hef_poschlamyd (date);
analyze temp_hef_poschlamyd;






--I'm going to get the list of patients with chlamydia episodes -- it gets used a few times.
-- less than a second
drop table if exists temp_chlamyd_pats;
CREATE TABLE temp_chlamyd_pats as
select distinct patient_id from temp_hef_poschlamyd;
alter table temp_chlamyd_pats add primary key (patient_id);
analyze temp_chlamyd_pats;
-- select * from temp_chlamyd_pats

--here are the ALL chlamyd tests.  If there are multiple results from the same day, I take the max of pos=1, neg=0, indet=-1
-- ~10 seconds
drop table if exists temp_hef_chlamyd;
CREATE  TABLE temp_hef_chlamyd as
  select patient_id,provider_id, date, 
    max(case when substr(name,14)='positive' then 1 when substr(name,14)='negative' then 0 else -1 end) as result
  from hef_event 
  where name like 'lx:chlamydia%'
  group by patient_id, provider_id, date;
alter table temp_hef_chlamyd add primary key (patient_id,provider_id,date);
create index on temp_hef_chlamyd (date);
analyze temp_hef_chlamyd;
--select * from temp_hef_chlamyd

--Gonorrhea positive results
-- less than a second
drop table if exists temp_hef_gonpos;
CREATE  TABLE temp_hef_gonpos as
  select distinct patient_id,provider_id, date from hef_event where name = 'lx:gonorrhea:positive'  ;
alter table temp_hef_gonpos add primary key (patient_id,provider_id,date);
create index on temp_hef_gonpos (date);
analyze temp_hef_gonpos;
--select * from temp_hef_gonpos

--Added 10/28/18
--All Gonorrhea lab results
-- less than a second
drop table if exists temp_hef_allgon;
CREATE  TABLE temp_hef_allgon as
  select distinct patient_id,provider_id, date from hef_event where name like 'lx:gonorrhea%' ;
alter table temp_hef_allgon add primary key (patient_id,provider_id,date);
create index on temp_hef_allgon (date);
analyze temp_hef_allgon;
--select * from temp_hef_allgon

--Added 10/28/18
-- SYPHILIS - Gather all lab syph hef events
drop table if exists temp_hef_all_syph;
CREATE TABLE temp_hef_all_syph AS 
select distinct patient_id,provider_id, date, name from hef_event 
where name ~ '^(lx:rpr|lx:vdrl|lx:tppa|lx:fta-abs|lx:tp-igg|lx:tp-igm)';

--Added 10/28/18
-- HIV - Gather all hiv lab hef events
drop table if exists temp_hef_all_hiv;
CREATE TABLE temp_hef_all_hiv AS 
select distinct patient_id,provider_id, date, name from hef_event 
where name like 'lx:hiv%';


drop table if exists temp_hiv;
CREATE  TABLE temp_hiv as
  select patient_id, date from nodis_case where condition = 'hiv';
alter table temp_hiv add primary key (patient_id, date);
create index on temp_hiv (date);
analyze temp_hiv;
--select * from temp_hiv


--here is the preg results.
-- less than a second
--I think we need start AND end date, to determine if a patient is pregnant within 14 days of chlamydia pos result.
--Have to goose end_date where it's missing.  
--I am assuming from what little I know that missing end date can either mean ongoing pregnancy, or no info about termination/delivery
drop table if exists temp_preg;
CREATE TABLE temp_preg as
select distinct patient_id, start_date, 
  case
    when end_date is not null then end_date
    when end_date is null and start_date + interval '46 weeks' > now() then greatest(now()::date, start_date + interval '40 weeks') --40 is average gestation + 6 weeks is 3 StDevs. 
    else null
  end as end_date
from hef_timespan ht
where name='pregnancy' and exists (select null from temp_chlamyd_pats tcp where tcp.patient_id=ht.patient_id)
   and case
    when end_date is not null then end_date
    when end_date is null and start_date + interval '46 weeks' > now() then now()::date
    else null
  end is not null; 
alter table temp_preg add primary key (patient_id, start_date, end_date);
analyze temp_preg;
--select* from temp_preg where end_date > now();

--here are ICD9 results
--Updating 10/28/18 for ICD10 cods
--this is a perfect example of the utility of temp tables.  
--The encounter table and dx tables are HUGE, but by limiting this temp table to just the columns and rows we need,
--and by imposing a single restriction that uses a normal to low-cardinaltiy index on the one table and joins via a 
--foreign key relationship, the result is generated relatively quickly and is then much more efficient to use subsequently 
--I'm not bothering to join with the temp chalmydia pats, since that would add to this query's complexity and the vast
--majority of these patients will already be in the chlamyd pats table.  The result here is small enough that a 
--subsequent join to the chlamydia pats table will be very fast.  As is, this query currently takes ~15 min.
--If we only need one of these dx codes (per your original script) it's way faster.  If you tried to embed this in a larger
--query, it would become less and less efficient as you added complexity.
drop table if exists temp_dx;
create table temp_dx as
select enc.patient_id, enc.provider_id, enc.date 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  where dx.dx_code_id in ('icd9:078.88', 'icd10:A74.89', 'icd9:079.88', 'icd9:079.98','icd10:A74.9','icd9:099.5', 'icd10:A56','icd9:099.50','icd10:A56.19','icd9:099.51', 'icd10:A56.4','icd9:099.52','icd10:A56.3', 'icd9:099.53', 'icd10:A56.0', 'icd10:A56.00', 'icd10:A56.01', 'icd10:A56.02', 'icd10:A56.09','icd9:099.54','icd10:A56.1', 'icd10:A56.11', 'icd10:A56.09', 'icd9:099.55','icd10:A56.2', 'icd9:099.56','icd10:A56.8','icd9:099.59', 'icd10:A56.19', 'icd10:A74.9');
--Another trick -- deleting duplicates here (on a small table) is way faster than imposing a distinct condition in prior query
--Careful with this.  If the result is large, or the result is a substantial portion of the original table, the distinct condition is faster.
delete from temp_dx dx
where dx.ctid <> (select min(dups.ctid) from temp_dx dups where dx.patient_id=dups.patient_id and dx.provider_id=dups.provider_id and dx.date=dups.date);
alter table temp_dx add primary key (patient_id, provider_id, date);
vacuum 
analyze temp_dx;
--select * from temp_dx;


--Updating 11/18/18 for gon dx codes
drop table if exists temp_gondx;
create table temp_gondx as
select enc.patient_id, enc.date 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  where dx.dx_code_id in ('icd9:098.0', 'icd9:098.2', 'icd10:A54', 'icd10:A54.0', 'icd10:A54.00', 'icd10:A54.02', 'icd10:A54.09', 'icd10:A54.1', 'icd10:A54.2', 'icd10:A54.21', 'icd10:A54.23', 'icd10:A54.24', 'icd10:A54.29', 'icd9:098.1', 'icd9:098.3', 'icd9:098.10', 'icd9:098.30', 'icd9:098.11', 'icd9:098.31', 'icd10:A54.01', 'icd9:098.12','icd9:098.32', 'icd10:A54.22', 'icd9:098.13', 'icd9:098.33','icd9:098.14', 'icd9:098.34', 'icd9:098.15', 'icd9:098.35', 'icd10:A54.03', 'icd9:098.16', 'icd9:098.17', 'icd9:098.36', 'icd9:098.37', 'icd9:098.19', 'icd9:098.39', 'icd9.098.4',  'icd9.098.40', 'icd9.098.41', 'icd9.098.42', 'icd9.098.43', 'icd9.098.49', 'icd10:A54.3', 'icd10:A54.30', 'icd10:A54.31','icd10:A54.32',  'icd10:A54.33','icd10:A54.39','icd9:098.5', 'icd9:098.50', 'icd9:098.51', 'icd9:098.52','icd9:098.53', 'icd9:098.59', 'icd10:A54.4','icd10:A54.40','icd10:A54.41','icd10:A54.42', 'icd10:A54.43','icd10:A54.49','icd9:098.6', 'icd9:098.7',      'icd10:A54.5', 'icd10:A54.6', 'icd9:098.8','icd9:098.8', 'icd9:098.81','icd9:098.83',
'icd9:098.84','icd9:098.85', 'icd9:098.86', 'icd9:098.89','icd10:A54.8','icd10:A54.81', 'icd10:A54.82', 'icd10:A54.83','icd10:A54.84', 'icd10:A54.85','icd10:A54.86','icd10:A54.89','icd10:A54.9');
--Another trick -- deleting duplicates here (on a small table) is way faster than imposing a distinct condition in prior query
--Careful with this.  If the result is large, or the result is a substantial portion of the original table, the distinct condition is faster.
delete from temp_gondx dx
where dx.ctid <> (select min(dups.ctid) from temp_gondx dups where dx.patient_id=dups.patient_id and dx.date=dups.date);
alter table temp_gondx add primary key (patient_id, date);
vacuum 
analyze temp_gondx;


--Updating 11/18/18 for syph dx codes
drop table if exists temp_syphdx;
create table temp_syphdx as
select enc.patient_id, enc.date 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  where dx.dx_code_id in ('icd9:091', 'icd9:091.0','icd9:091.1', 'icd9:091.2','icd9:091.3','icd9:091.4','icd9:091.5',
'icd9:091.50','icd9:091.51','icd9:091.52','icd9:091.6', 'icd9:091.61','icd9:091.62','icd9:091.69','icd9:091.7', 'icd9:091.8','icd9:091.81','icd9:091.82','icd9:091.89',   'icd9:091.9','icd9:092', 'icd9:092.0','icd9:092.9','icd9:093','icd9:093.8','icd9:093.89','icd9:093.9', 'icd9:094', 'icd9:094.3','icd9:094.8', 'icd9:094.89', 'icd9:094.9','icd9:095','icd9:095.1',  'icd9:095.3','icd9:095.4','icd9:095.5', 'icd9:095.6','icd9:095.7','icd9:095.8', 'icd9:095.9', 'icd9:096','icd9:097','icd9:097.0','icd9:097.1', 'icd9:097.9','icd10:A51','icd10:A51.0', 'icd10:A51.1','icd10:A52','icd10:A51.3', 'icd10:A51.39',    'icd10:A51.4','icd10:A51.5','icd10:A51.9','icd10:A52', 'ic10:A52.0','icd10:A52.00', 'icd10:A52.05', 'icd10:A52.09', 'icd10:A52.1','icd10:A52.10', 'icd10:A52.12', 'icd10:A52.19',      'icd10:A52.19',  'icd10:A52.2',   'icd10:A52.3',  'icd10:A52.7',   'icd10:A52.72',  'icd10:A52.73',  'icd10:A52.74',  'icd10:A52.75','icd10:A52.76',  'icd10:A52.77', 'icd10:A52.78',   'icd10:A52.79','icd10:A52.8',     'icd10:A52.9', 'icd10:A53.0',  'icd10:A53','icd10:A53.9'                                                                                                      );
--Another trick -- deleting duplicates here (on a small table) is way faster than imposing a distinct condition in prior query
--Careful with this.  If the result is large, or the result is a substantial portion of the original table, the distinct condition is faster.
delete from temp_syphdx dx
where dx.ctid <> (select min(dups.ctid) from temp_syphdx dups where dx.patient_id=dups.patient_id and dx.date=dups.date);
alter table temp_syphdx add primary key (patient_id, date);
vacuum 
analyze temp_syphdx;




--now doing the similar for chlamydia reportable dx codes
--this takes about 20 minutes.  
drop table if exists temp_rep_dx;
create table temp_rep_dx as
select distinct enc.patient_id, enc.date, dx.dx_code_id 
from emr_encounter enc 
  join emr_encounter_dx_codes dx on enc.id=dx.encounter_id 
  join conf_reportabledx_code rep on dx.dx_code_id=rep.dx_code_id 
  join temp_chlamyd_pats tcp on enc.patient_id=tcp.patient_id
  where rep.condition_id='chlamydia'; 
alter table temp_rep_dx add primary key (patient_id, date, dx_code_id);
create index on temp_rep_dx (date);
analyze temp_rep_dx;
--select * from temp_rep_dx;

--here is rx
--this one takes about 12 minutes.
drop table if exists temp_rx;
create table temp_rx as
select pre.patient_id, pre.provider_id, pre.date from emr_prescription pre join temp_chlamyd_pats cpat on cpat.patient_id=pre.patient_id  
where lower(pre.name) like '%azithromycin%' and lower(pre.name) like '%chlamydia%';
delete from temp_rx rx
where rx.ctid <> (select min(dups.ctid) from temp_rx dups where rx.patient_id=dups.patient_id and rx.provider_id=dups.provider_id and rx.date=dups.date);
alter table temp_rx add primary key (patient_id, provider_id, date);
vacuum 
analyze temp_rx;
--select * from temp_rx

--here is lb for EPT order extension plus the lab results for medication type. 
--I put all the info in one row to simplify things. 
--If the order_extension table gets big this query will become inefficient.
-- ~ 20 seconds, 
-- change result date to be the encounter date
drop table if exists temp_ept_lb;
CREATE TABLE temp_ept_lb as
  select ept0.patient_id,ept0.provider_id,ept0.date, null::date as result_date --lb.result_date is not currently provided, or is provided with incorrect values.
      , string_agg(distinct lb.native_code,',' order by lb.native_code) as native_code
      from emr_order_extension ept0 
      left join emr_labresult lb on ept0.order_natural_key=lb.order_natural_key and ept0.patient_id=lb.patient_id
      group by ept0.patient_id,ept0.provider_id,ept0.date;
alter table temp_ept_lb add primary key (patient_id, provider_id, date);
analyze temp_ept_lb;
--select * from temp_ept_lb;

--Now I build most of the episode table, using the parts and pieces I assembled above.
--This would be terribly inefficient without the temp tables (small, compact, quickly queryable).
--This runs in approx ~10 seconds
--I use an anonymous block, which permits you to drop in plpgsql function/procedure language into sql code. 
--I don't know a better way to get patient-specific, dynamic temporal windows, but maybe there is. 
drop table if exists temp_chlamyd_episode;
create  table temp_chlamyd_episode 
( patient_id integer
  ,provider_id integer
  ,date date
  ,specimen_source text
  ,pstart_date date
  ,pend_date date
  ,dx_prov_id integer
  ,dx_date date
  ,rx_prov_id integer
  ,rx_date date
  ,ept_yn varchar(3)
  ,ept_date date
  ,ept_prvd varchar(3)
  ,ept_type varchar(50)
  ,ept_time varchar(20)
 ); 
--here's the anonymous block.  You could write this as a stored procedure, but unless you're going to call
-- the procedure a number of times, I prefer to see it in-line -- you don't have to go look at it elsewhere to see what it's doing
do $$
  declare
  pat_id int;
  insrtxt text;
  epi_date date;
  specimen_source text;
  dx_date date;
  cursrow record;
  pregcursrow record;
  dxcursrow record;
  dx_ept_found boolean;
  eptcursrow record;
  rxcursrow record;
  epttxt text;
  foundit integer;
  begin
    for pat_id in select patient_id from temp_chlamyd_pats 
    -- these have to match what's in temp_hef_poschlamyd, because we just built the list from there
    loop
      epi_date:=null;
      dx_date:=null;
      for cursrow in execute 'select distinct date, patient_id, provider_id, specimen_source from temp_hef_poschlamyd where patient_id=' || pat_id || ' order by date'
      loop
      
        if epi_date is null or cursrow.date > epi_date + interval '1 year' then
          dx_ept_found:=FALSE; -- set this to false just in case it could get carried over from prior loop iteration
          epi_date:=cursrow.date;
          specimen_source:=cursrow.specimen_source;
if specimen_source is null then
          insrtxt:='insert into temp_chlamyd_episode (patient_id, provider_id, date, specimen_source, pstart_date, pend_date, dx_prov_id, dx_date, rx_prov_id, rx_date, ept_yn, ept_date, ept_prvd, ept_type, ept_time) values (' ||
             pat_id || ','  || cursrow.provider_id || ',''' || to_char(epi_date,'yyyy-mm-dd') || '''::date, '  || '''' ||  '''' || ',';
else 
          insrtxt:='insert into temp_chlamyd_episode (patient_id, provider_id, date, specimen_source, pstart_date, pend_date, dx_prov_id, dx_date, rx_prov_id, rx_date, ept_yn, ept_date, ept_prvd, ept_type, ept_time) values (' ||
             pat_id || ','  || cursrow.provider_id || ',''' || to_char(epi_date,'yyyy-mm-dd') || '''::date, '  || '''' || cursrow.specimen_source || '''' || ',';
end if;

          execute 'select patient_id, start_date, end_date from temp_preg prg where ''' ||
                  to_char(epi_date,'yyyy-mm-dd') || '''::date between start_date - interval ''60 days'' and end_date + interval ''60 days''' ||
              ' and patient_id=' || pat_id || ' order by start_date limit 1' into pregcursrow;
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null::date, null::date, ';

          else
            insrtxt:=insrtxt || '''' || to_char(pregcursrow.start_date,'yyyy-mm-dd') || '''::date, ''' || 
                to_char(pregcursrow.end_date,'yyyy-mm-dd') || '''::date, ';  
            if insrtxt is null then raise notice '%', 'check line 188'; end if;


          end if;
          execute 'select provider_id, date from temp_dx where date between ''' || 
              to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''365 days''' ||
              ' and patient_id=' || pat_id || ' order by date limit 1' into dxcursrow; 
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null, null::date, ';
          else
            insrtxt:=insrtxt || dxcursrow.provider_id || ', ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || '''::date, ';
            if insrtxt is null then raise notice '%', 'check line 198';

 end if;
            --ept can be associated with dx or rx. Check for dx first.  Use plus or minus one day.  Is that OK?
            execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || ' and date between ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || 
                     '''::date - interval ''1 days'' and ''' || to_char(dxcursrow.date,'yyyy-mm-dd') || '''::date + interval ''1 days''' into eptcursrow;
            get diagnostics foundit = ROW_COUNT;
            if foundit<>1 then
              dx_ept_found:=FALSE;
            else
              dx_ept_found:=TRUE; --the ept fields get inserted after the rx fields in any case, so I just need a boolian here to let me know I have the ept already
            end if;
          end if;
          execute 'select provider_id, date from temp_rx where date between ''' || 
              to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''365 days''' ||
              ' and patient_id=' || pat_id || ' order by date limit 1' into rxcursrow; 
          get diagnostics foundit = ROW_COUNT;
          if foundit<>1 then
            insrtxt:=insrtxt || 'null, null::date, ';

            if dx_ept_found then
              if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355805--,355806--' then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
              end if;
              insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
              if insrtxt is null then raise notice '%', 'check line 223';
 end if;
            else
              execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || 'and date between ''' ||
                to_char(epi_date,'yyyy-mm-dd') || '''::date and ''' || to_char(epi_date,'yyyy-mm-dd') || '''::date + interval ''28 days''' --chlamydia heuristic recurrance interval
                 into eptcursrow; 
              get diagnostics foundit = ROW_COUNT;
              if foundit<>1 then

                insrtxt:=insrtxt || '''0'', null::date, null, null, null)';


              else
                if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                else epttxt:='0'', null, null)';
                end if;
                insrtxt:=insrtxt || '''0'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;


              end if;
            end if;
          else
            insrtxt:=insrtxt || rxcursrow.provider_id || ', ''' || to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date, ';
            if insrtxt is null then raise notice '%', 'check line 229';

 end if;
            if dx_ept_found then
              if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
              end if;
              insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
              if insrtxt is null then raise notice 'check line 237: %, %',  date_part('hour',eptcursrow.result_date); end if;
            else
              execute 'select patient_id, date,  native_code, result_date from temp_ept_lb where patient_id=' || pat_id || 'and date between ''' ||
                 to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date - interval ''1 days'' and ''' || to_char(rxcursrow.date,'yyyy-mm-dd') || '''::date + interval ''1 days'''
                 into eptcursrow;
              get diagnostics foundit = ROW_COUNT;
              if foundit<>1 then
                insrtxt:=insrtxt || '0, null::date, null, null, null)';
              else
                if eptcursrow.native_code='355805--'  then epttxt:='1'', ''Floor Meds Dispensed'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355806--' then epttxt:='1'', ''Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355805--,355806--'  then epttxt:='1'', ''Floor Meds Dispensed and Print Rx Manual Fax'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
                elsif eptcursrow.native_code='355804--' then epttxt:='0'', ''Declined'', ''' || COALESCE(to_char(date_part('hour',eptcursrow.result_date),'99'),'') || ''')';
              else epttxt:='0'', null, null)';
                end if;
                insrtxt:=insrtxt || '''1'', ''' || to_char(eptcursrow.date,'yyyy-mm-dd') || '''::date, ''' || epttxt;
                if insrtxt is null then raise notice '%', 'check line 252'; end if;
              end if;
            end if;
          end if;


         execute insrtxt;
        end if;
      end loop;
    end loop;
  end;
$$ language plpgsql;
alter table temp_chlamyd_episode add primary key (patient_id, date);
analyze temp_chlamyd_episode;
--select * from temp_chlamyd_episode where patient_id in (373490,1724046,2215954)

drop table if exists temp_post_pos_chlamyd_encs;
create  table temp_post_pos_chlamyd_encs as
select t0.patient_id, t0.date as enc_date, t1.date as chlam_date 
from emr_encounter t0
join temp_chlamyd_episode t1 on t1.patient_id=t0.patient_id
where t1.date<t0.date;
create index temp_post_pos_chlamyd_encs_pat_idx
  on temp_post_pos_chlamyd_encs (patient_id);
analyze temp_post_pos_chlamyd_encs;

drop table if exists temp_post_pos_chlamyd_enc_keydates;
create  table temp_post_pos_chlamyd_enc_keydates as
select distinct t0.patient_id, t0.chlam_date, t1.next_test_date,
   min(t0.enc_date) over (partition by t0.patient_id) as first_enc_date,
   max(case
           when t0.enc_date between t0.chlam_date + interval '3 days' 
                             and t0.chlam_date + interval '28 days'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_3d_4w,
   max(case
           when t0.enc_date between t0.chlam_date + interval '28 days' 
                             and t0.chlam_date + interval '3 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_4w_3m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '3 months' 
                             and t0.chlam_date + interval '6 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_3m_6m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '6 months' 
                             and t0.chlam_date + interval '9 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_6m_9m,
   max(case
           when t0.enc_date between t0.chlam_date + interval '9 months' 
                             and t0.chlam_date + interval '12 months'  then 1
           else 0
        end) over (partition by t0.patient_id) as enc_9m_12m,
   sum(case
          when t0.enc_date < t1.next_test_date then 1 
          when t1.next_test_date is null and t0.enc_date < t0.chlam_date + interval '1 year' then 1
          else 0
        end) over (partition by t0.patient_id) as n_encs
from temp_post_pos_chlamyd_encs t0
      left join (select t00.patient_id, min(t00.date) as next_test_date
                 from temp_hef_chlamyd t00 
                   join temp_chlamyd_episode t01 on t01.patient_id=t00.patient_id
                 where t00.date between t01.date + interval '1 days'
                                and t01.date + interval '365 days'
                 group by t00.patient_id) t1 on t0.patient_id = t1.patient_id;
Alter table temp_post_pos_chlamyd_enc_keydates 
add primary key (patient_id, chlam_date);

drop table if exists temp_post_rx; 
create  table temp_post_rx as
select t0.patient_id, t0.name, t0.date, t0.directions, t0.quantity, t0.refills, t1.date as chlmyd_date
from emr_prescription t0
  join temp_chlamyd_episode t1 on t1.patient_id=t0.patient_id  
    and t0.date between t1.date and t1.date + interval '30 days';

drop table if exists temp_post_rx_azithro;
CREATE  TABLE temp_post_rx_azithro AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as azithro_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as azithro_dose,
       date as azithro_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%zithromycin%' or name ilike '%zithromax%' or name ilike '%zmax%') 
     and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
     and not name ilike '%opht%')) t0
where t0.rnum=1;

drop table if exists temp_post_rx_doxy;
CREATE  TABLE temp_post_rx_doxy AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as doxy_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as doxy_dose,
       date as doxy_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%doxyc%' or name ilike '%adoxa%' or name ilike '%doryx%' or name ilike '%monodox%' 
       or name ilike '%oracea%' or name ilike '%periostat%' or name ilike '%targadox%' 
       or name ilike '%vibramycin%') 
       and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
       and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_post_rx_erythro;
CREATE  TABLE temp_post_rx_erythro AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as erythro_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as erythro_dose,
       date as erythro_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%erythromycin%' or name ilike '%e-mycin%' or name ilike '%eryped%'
        or name ilike '%ery-tab%' or name ilike '%erythrocin%')
  and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
  and not name ilike '%opht%' and not name ilike '%top%' and not name ilike '%ointment%')) t0
where t0.rnum=1;

drop table if exists temp_post_rx_levoflox;
CREATE  TABLE temp_post_rx_levoflox AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as levoflox_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as levoflox_dose,
       date as levoflox_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%levoflox%' or name ilike '%levaquin%')
    and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_post_rx_ciproflox;
CREATE  TABLE temp_post_rx_ciproflox AS
select * from 
(SELECT patient_id, chlmyd_date,
       '1'::varchar as ciproflox_given,
       case 
         when quantity is not null and refills is not null then
           name||', quant: '||quantity||', refills: '||refills
         else name
       end as ciproflox_dose,
       date as ciproflox_date,
       row_number() over (partition by patient_id, chlmyd_date order by date) as rnum
FROM temp_post_rx 
where ((name ilike '%cipro%' or name ilike '%proquin%' ) 
     and not name ilike '%eye %' and not name ilike '%ear %' and not name ilike '%otic%' 
     and not name ilike '%opht%')
) t0
where t0.rnum=1;

drop table if exists temp_treat;
create  table temp_treat as
select t0.patient_id, t0.date,
  t1.azithro_given, t1.azithro_dose, t1.azithro_date,
  t2.doxy_given, t2.doxy_dose, t2.doxy_date,
  t3.erythro_given, t3.erythro_dose, t3.erythro_date,
  t4.levoflox_given, t4.levoflox_dose, t4.levoflox_date,
  t5.ciproflox_given, t5.ciproflox_dose, t5.ciproflox_date,
  case 
    when t1.azithro_given is null and t2.doxy_given is null and t3.erythro_given is null 
         and t4.levoflox_given is null and t5.ciproflox_given is null then
         (select string_agg(name,',') from temp_post_rx t00 
          where t00.patient_id=t0.patient_id and t00.date=t0.date 
          group by t0.patient_id)::text 
    else null::text
  end as other_rx
from temp_chlamyd_episode t0
  left join temp_post_rx_azithro t1 on t1.patient_id=t0.patient_id and t0.date=t1.chlmyd_date
  left join temp_post_rx_doxy t2 on t2.patient_id=t0.patient_id and t0.date=t2.chlmyd_date
  left join temp_post_rx_erythro t3 on t3.patient_id=t0.patient_id and t0.date=t3.chlmyd_date
  left join temp_post_rx_levoflox t4 on t4.patient_id=t0.patient_id and t0.date=t4.chlmyd_date
  left join temp_post_rx_ciproflox t5 on t5.patient_id=t0.patient_id and t0.date=t5.chlmyd_date;
update temp_treat
set azithro_given=0 where azithro_given is null;
update temp_treat
set doxy_given=0 where doxy_given is null;
update temp_treat
set erythro_given=0 where erythro_given is null;
update temp_treat
set levoflox_given=0 where levoflox_given is null;
update temp_treat
set ciproflox_given=0 where ciproflox_given is null;
alter table temp_treat add primary key (patient_id, date);
analyze temp_treat;

--Added 10/28/18
--Number of chlamydia tests during two years before date of index positive chlamydia test
drop table if exists temp_chlpriortests;
create table temp_chlpriortests as select count(*) as numchlamtests, patient_id, epdate from (select h.patient_id, h.date as hefdate, e.date as epdate from temp_hef_chlamyd h, temp_chlamyd_episode e where h.patient_id = 
e.patient_id and  h.date < e.date and (h.date > (e.date - interval '2 year')))a group by patient_id, epdate;

--Added 10/28/18
--Number of gonorrhea tests during two years before date of index positive chlamydia test
drop table if exists temp_gonpriortests;
create table temp_gonpriortests as select count(*) as numgontests, patient_id, epdate from (select h.patient_id, h.date as hefdate, e.date as epdate from temp_hef_allgon h, temp_chlamyd_episode e where h.patient_id = 
e.patient_id and  h.date < e.date and (h.date > (e.date - interval '2 year')))a group by patient_id, epdate;

--Added 10/28/18
--Number of syph tests during two years before date of index positive chlamydia test
drop table if exists temp_syphpriortests;
create table temp_syphpriortests as select count(*) as numsyphtests, patient_id, epdate, name from (select h.patient_id, h.date as hefdate, e.date as epdate, name from temp_hef_all_syph h, temp_chlamyd_episode e where h.patient_id = 
e.patient_id and  h.date < e.date and (h.date > (e.date - interval '2 year')))a group by patient_id, epdate, name ;


--Number of syph tests during two years before date of index positive chlamydia test
drop table if exists temp_syphpriortests2;
create table temp_syphpriortests2 as select count(*) as numsyphtests, patient_id, epdate from (select h.patient_id, h.date as hefdate, e.date as epdate, name from temp_hef_all_syph h, temp_chlamyd_episode e where h.patient_id = 
e.patient_id and  h.date < e.date and (h.date > (e.date - interval '2 year')))a group by patient_id, epdate;

--Added 10/28/18
--Number of hiv tests during two years before date of index positive chlamydia test
drop table if exists temp_hivpriortests;
create table temp_hivpriortests as select count(*) as numhivtests, patient_id, epdate from (select h.patient_id, h.date as hefdate, e.date as epdate from temp_hef_all_hiv h, temp_chlamyd_episode e where h.patient_id = 
e.patient_id and  h.date < e.date and (h.date > (e.date - interval '2 year')))a group by patient_id, epdate;

--Added 11/18/18
--Count of encounters with chlam dx codes prior to episode
drop table if exists temp_priorchlamdx;
create table temp_priorchlamdx as select count(*) as numchlamdx, patient_id, epdate from (select cdx.patient_id, cdx.date as chlamdxdate, e.date as epdate from temp_dx cdx, temp_chlamyd_episode e where cdx.patient_id = 
e.patient_id and  cdx.date < e.date and (cdx.date > (e.date - interval '2 year')))a group by patient_id, epdate order by patient_id;

--Added 11/18/18
--Count of encounters with gonorrhea dx codes prior to episode
drop table if exists temp_priorgondx;
create table temp_priorgondx as select count(*) as numgondx, patient_id, epdate from (select gdx.patient_id, gdx.date as gondxdate, e.date as epdate from temp_gondx gdx, temp_chlamyd_episode e where gdx.patient_id = 
e.patient_id and  gdx.date < e.date and (gdx.date > (e.date - interval '2 year')))a group by patient_id, epdate order by patient_id;


--Added 11/18/18
--Count of encounters with syphilis dx codes prior to episode
drop table if exists temp_priorsyphdx;
create table temp_priorsyphdx as select count(*) as numsyphdx, patient_id, epdate from (select sdx.patient_id, sdx.date as syphdxdate, e.date as epdate from temp_syphdx sdx, temp_chlamyd_episode e where sdx.patient_id = 
e.patient_id and  sdx.date < e.date and (sdx.date > (e.date - interval '2 year')))a group by patient_id, epdate order by patient_id;

--Added 11/26/2018
-- prescriptions for Truvada =2 months apart (generic name Emtricitabine-Tenofovir) while HIV negative (exclude if history of positive hepatitis B DNA test ever) 
--Tenofovir) while HIV negative 
--please calculate for the two years before date of index 
---- positive chlamydia test 
--andprovide as (1=yes, 0=no) for each year 
--When counting number of prescriptions include refills
--(e.g. ----
--patient given one --prescription with 2 refills count as 
--3 prescriptions).


--NOTE as of 11/26/2018, the data at Atrius for Truvada does not require us to have the logic of
--making sure the rx is
--greater than two months
-- All applicable counts >2 with refills.
drop table if exists temp_truvada;
CREATE TABLE temp_truvada   AS 
SELECT T1.patient_id, T1.name, T1.date, T1.object_id,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1)  ELSE 1 END  refills,
1::int hiv_meds
FROM hef_event T1 
INNER JOIN emr_prescription T2 
ON ((T1.object_id = T2.id))  
WHERE T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine')
and T1.patient_id not in (select patient_id from hef_event where name = 'lx:hepatitis_b_viral_dna:positive');

--Count of truvada rx in year prior to episode
drop table if exists temp_truvadacountssummed1year ;
create table temp_truvadacountssummed1year as select tdx.patient_id, hiv_meds + refills as number,  tdx.date as tdxdate, e.date as epdate from temp_truvada tdx, temp_chlamyd_episode e where tdx.patient_id = 
e.patient_id and  tdx.date < e.date and (tdx.date > (e.date - interval '1 year'));

--Sum up better
drop table if exists temp_truvadacountssummed1year2;
create table temp_truvadacountssummed1year2 as select 
sum(number), patient_id, epdate from temp_truvadacountssummed1year group by patient_id, epdate;

--Count of truvada rx in year two years prior to episode
drop table if exists temp_truvadacountssummed2year;
create table temp_truvadacountssummed2year as select tdx.patient_id, hiv_meds + refills as number,  tdx.date as tdxdate, e.date as epdate from temp_truvada tdx, temp_chlamyd_episode e where tdx.patient_id = 
e.patient_id and  tdx.date < (e.date - interval '1 year') and (tdx.date > (e.date - interval '2 year'));

--Sum up better
drop table if exists temp_truvadacountssummed2year2;
create table temp_truvadacountssummed2year2 as select 
sum(number), patient_id, epdate from temp_truvadacountssummed2year group by patient_id, epdate;



--now build the report query from epi and the patient and provider tables.
drop table if exists temp_chlamreinfreport;
create table temp_chlamreinfreport as
select  pat.id as ESP_patid,
            epi.date as dateposchlamydia, 
       epi.specimen_source as specimen_source,
       date_part('year', age(epi.date,pat.date_of_birth)) as ageatchlamydiapos,
       pat.gender, 
       pat.race, 
       case 
         when lower(pat.race) like '%hispanic%' and lower(pat.race) not like '%non%' then 'HISPANIC' 
         when lower(pat.race) like '%hispanic%' and  lower(pat.race)  like '%non%' then 'NON-HISPANIC' 
         else '' 
       end as ethnicityvalues, -- calculated from race
       case when epi.pstart_date is not null then '1'
       else '0'
       end as pregnantwhenposchla,
       epi.pstart_date as sop,
       epi.pend_date as edd,
       epi.provider_id  as providerposchla,
       pro1.dept as providerlocposchla,
       epi.dx_date as datedxchla,
       epi.dx_prov_id as providerdxchla,
       pro2.dept as providerlocdxchla,
       epi.rx_date as daterxchla,
       epi.rx_prov_id as providerrxchla,
       pro3.dept as providerlocrxchla,
       epi.ept_yn as eptdata, 
       epi.ept_prvd as eptprovided,
       epi.ept_type as typeept, 
       epi.ept_date as dateept,
  --REMOVED 10/16/2015     'not available',--epi.ept_time as timeofdayept,
       case when gon.patient_id is not null then '1' else '0' end as positivegonorrhea, 
--Added 10/28/18
	  case when hiv.patient_id is not null then '1' else '0' 
end as knownhiv,
       case when hiv.patient_id is not null then hiv.date end as hivcasedate, 
--Number of chlamydia tests during two years 
-- before date of index positive chlamydia test
temp_chlpriortests.numchlamtests as numpriorchlamtests,
--Number chlam dx
temp_priorchlamdx.numchlamdx as numpriorchlamdx,
--Number of gonorrhea tests during two years 
-- before date of index positive chlamydia test
temp_gonpriortests.numgontests as numpriorgontests,
--Number gon dx in two years prior
temp_priorgondx.numgondx as numpriorgondx,
--Number of syph tests during two years 
-- before date of index positive chlamydia test
temp_syphpriortests2.numsyphtests as numpriorsyphtests,
--Number syph dx in two years prior
temp_priorsyphdx.numsyphdx as numpriorsyphdx,
--Number of hiv tests during two years 
-- before date of index positive chlamydia test
temp_hivpriortests.numhivtests as numpriorhivtests,
--truvada in year before chlam episode
case
when truv1.patient_id is not null then '1' else '0'
end
as truvada1year,
--truvada in the year two years before the chlam episode
case
when truv2.patient_id is not null then '1' else '0'
end
as truvada2year,
       case when rep.patient_id is not null then '1' else '0' end as repeatchlamydia,
       rep.date as daterepeatchla,
       case when rep.result=1 then 'Positive' when rep.result=0 then 'Negative' when rep.result=-1 then 'Indeterminate' else null end as resultrepeatchla,
       case when reppos.patient_id is not null then '1' else '0' end as repeatposchla,
       reppos.date as daterepeatposchla ,
       rep2.date as scndrep_date,
       case when rep2.result=1 then 'Positive' when rep2.result=0 then 'Negative' when rep2.result=-1 then 'Indeterminate' else null end as scndrep_res,
       case when rep2.edate is not null then 1 when rep2.date is not null then 0 end as scndrep_ept,
       rep2.edate as scndrep_eptdate,
       rep2.ept_type as scndrep_type,
       rep3.date as thrdrep_date,
       case when rep3.result=1 then 'Positive' when rep3.result=0 then 'Negative' when rep3.result=-1 then 'Indeterminate' else null end as thrdrep_res,
       case when rep3.edate is not null then 1  when rep3.date is not null then 0 end as thrdrep_ept,
       rep3.edate as thrdrep_eptdate,
       rep3.ept_type as thrdrep_type,
       case when symp.icd9_099_40icd10N34_1 is null then 0 else symp.icd9_099_40icd10N34_1 end icd9_099_40icd10N34_1,
       case when symp.icd9_597_80icd10N34_2 is null then 0 else symp.icd9_597_80icd10N34_2 end icd9_597_80icd10N34_2 ,
       case when symp.icd9_616_0icd10N72 is null then 0 else symp.icd9_616_0icd10N72 end icd9_616_0icd10N72,
       case when symp.icd9_616_10icd10N76s is null then 0 else symp.icd9_616_10icd10N76s end icd9_616_10icd10N76s,
       case when symp.icd9_623_5icd10N89_8 is null then 0 else  symp.icd9_623_5icd10N89_8 end  icd9_623_5icd10N89_8,
       case when symp.icd9_780_6icd10R50_9 is null then 0 else symp.icd9_780_6icd10R50_9 end icd9_780_6icd10R50_9,
       case when symp.icd9_780_60 is null then 0 else symp.icd9_780_60 end icd9_780_60,
       case when symp.icd9_788_7icd10R36 is null then 0 else symp.icd9_788_7icd10R36 end icd9_788_7icd10R36,
       case when symp.icd9_789_00icd10R10_9 is null then 0 else symp.icd9_789_00icd10R10_9 end icd9_789_00icd10R10_9,
       case when symp.icd9_789_03icd10R10_31 is null then 0 else symp.icd9_789_03icd10R10_31 end icd9_789_03icd10R10_31,
       case when symp.icd9_789_04R10_32 is null then 0 else symp.icd9_789_04R10_32 end icd9_789_04R10_32,
       case when symp.icd9_789_07icd10R10_84 is null then 0 else symp.icd9_789_07icd10R10_84 end icd9_789_07icd10R10_84,
       case when symp.icd9_789_09icd10R10_10 is null then 0 else symp.icd9_789_09icd10R10_10 end icd9_789_09icd10R10_10,
       tppcek.first_enc_date, tppcek.enc_3d_4w, tppcek.enc_4w_3m, tppcek.enc_3m_6m, 
       tppcek.enc_6m_9m, tppcek.enc_9m_12m, tppcek.n_encs,
       trt.azithro_given, trt.azithro_dose, trt.azithro_date,
       trt.doxy_given, trt.doxy_dose, trt.doxy_date,
       trt.erythro_given, trt.erythro_dose, trt.erythro_date,
       trt.levoflox_given, trt.levoflox_dose, trt.levoflox_date,
       trt.ciproflox_given, trt.ciproflox_dose, trt.ciproflox_date,
       trt.other_rx
 from emr_patient pat 
      join temp_chlamyd_pats tcp on tcp.patient_id=pat.id
      join temp_chlamyd_episode epi on tcp.patient_id=epi.patient_id
      join temp_treat trt on trt.patient_id=epi.patient_id and trt.date=epi.date
	left join temp_chlpriortests on temp_chlpriortests.patient_id = epi.patient_id and temp_chlpriortests.epdate = epi.date
	left join temp_priorchlamdx on
temp_priorchlamdx.patient_id = epi.patient_id and
temp_priorchlamdx.epdate = epi.date
	left join temp_gonpriortests on temp_gonpriortests.patient_id = epi.patient_id and temp_gonpriortests.epdate = epi.date
	left join temp_priorgondx on
temp_priorgondx.patient_id = epi.patient_id and
temp_priorgondx.epdate = epi.date
	left join temp_syphpriortests2 on temp_syphpriortests2.patient_id = epi.patient_id and temp_syphpriortests2.epdate = epi.date
	left join temp_priorsyphdx on
temp_priorsyphdx.patient_id = epi.patient_id and
temp_priorsyphdx.epdate = epi.date
	left join temp_hivpriortests on temp_hivpriortests.patient_id = epi.patient_id and temp_hivpriortests.epdate = epi.date
left join temp_truvadacountssummed1year2 truv1 
on epi.patient_id = truv1.patient_id
and truv1.epdate = epi.date
left join temp_truvadacountssummed2year2 truv2
on epi.patient_id = truv2.patient_id
and truv2.epdate = epi.date

      join emr_provider pro1 on epi.provider_id=pro1.id 
      left join temp_post_pos_chlamyd_enc_keydates tppcek on tppcek.patient_id=epi.patient_id
        and tppcek.chlam_date=epi.date
      left join emr_provider pro2 on epi.dx_prov_id=pro2.id
      left join emr_provider pro3 on epi.rx_prov_id=pro3.id
      left join (select distinct gon.patient_id, epi.date as edate from temp_hef_gonpos gon 
                  join temp_chlamyd_episode epi on gon.patient_id=epi.patient_id 
                  where gon.date between epi.date - interval '14 days' and epi.date + interval '14 days') gon 
                  on epi.patient_id=gon.patient_id and epi.date=gon.edate
--Added 10/28/2018
	left join temp_hiv hiv on 
hiv.patient_id = epi.patient_id
--
      left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id 
                  where thc.date between epi.date + interval '1 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate from row_sumry rs where rs.rn=1  --order by rs.patient_id, rs.date
                ) rep on rep.patient_id=epi.patient_id and rep.edate=epi.date
      left join (with row_sumry as
                 (select reppos.patient_id, reppos.date, epi.date as edate, 
                  row_number() over (partition by epi.patient_id, epi.date order by reppos.date) as rn
                  from temp_hef_poschlamyd reppos
                  join temp_chlamyd_episode epi on reppos.patient_id=epi.patient_id
                 where reppos.date between epi.date + interval '1 days' and epi.date + interval '365 days')
                 select rs.patient_id, rs.date, rs.edate from row_sumry rs where rs.rn=1
                 ) reppos on reppos.patient_id=epi.patient_id and reppos.edate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate,
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '1 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=2  
                ) rep2 on rep2.patient_id=epi.patient_id and rep2.epidate=epi.date
       left join (with row_sumry as 
                 (select thc.patient_id, thc.date, thc.result, ept.date as edate, epi.date as epidate, 
                  case
                    when ept.native_code='355805--'  then 'Floor Meds Dispensed'
                    when ept.native_code='355806--' then 'Print Rx Manual Fax'
                    when ept.native_code='355805--,355806--' then 'Floor Meds Dispensed and Print Rx Manual Fax'
                    when ept.native_code='355804--' then 'Declined'
                  end as ept_type,
                  row_number() over (partition by epi.patient_id, epi.date order by thc.date) as rn
                  from temp_hef_chlamyd thc 
                  join temp_chlamyd_episode epi on thc.patient_id=epi.patient_id
             left join temp_ept_lb ept on thc.patient_id=ept.patient_id and epi.date between ept.date - interval '1 days' and ept.date + interval '1 days'                   
                  where thc.date between epi.date + interval '1 days' and epi.date + interval '365 days'
                 ) select rs.patient_id, rs.date, rs.result, rs.edate, rs.ept_type, rs.epidate from row_sumry rs where rs.rn=3  
                ) rep3 on rep3.patient_id=epi.patient_id and rep3.epidate=epi.date
       left join (select trd.patient_id, epi.date as edate,
                    max(case when (trd.dx_code_id='icd9:099.40' or trd.dx_code_id = 'icd10:N34.1') then 1 else 0 end) as icd9_099_40icd10N34_1,
                    max(case when (trd.dx_code_id='icd9:597.80' or trd.dx_code_id = 'icd10:N43.2') then 1 else 0 end) as icd9_597_80icd10N34_2,
                    max(case when (trd.dx_code_id='icd9:616.0' or trd.dx_code_id = 'icd10:N72') then 1 else 0 end) as icd9_616_0icd10N72,
                    max(case when (trd.dx_code_id='icd9:616.10' 
or trd.dx_code_id = 'icd10:N76.0' or trd.dx_code_id = 'icd10:N76.1' or trd.dx_code_id = 'icd10:N76.2' or trd.dx_code_id = 'icd10:N76.3') then 1 else 0 end) as icd9_616_10icd10N76s,
                    max(case when (trd.dx_code_id='icd9:623.5' or trd.dx_code_id = 'icd10:N89.8') then 1 else 0 end) as icd9_623_5icd10N89_8,
                    max(case when (trd.dx_code_id='icd9:780.6' or trd.dx_code_id = 'icd10:R50.9') then 1 else 0 end) as icd9_780_6icd10R50_9,
                    max(case when trd.dx_code_id='icd9:780.60' then 1 else 0 end) as icd9_780_60,
                    max(case when (trd.dx_code_id='icd9:788.7' or trd.dx_code_id = 'icd10:R36.0' or trd.dx_code_id = 'icd10:R36.9') then 1 else 0 end) as icd9_788_7icd10R36,
                    max(case when (trd.dx_code_id='icd9:789.00' or trd.dx_code_id = 'icd10:R10.9') then 1 else 0 end) as icd9_789_00icd10R10_9,
                    max(case when (trd.dx_code_id='icd9:789.03' or trd.dx_code_id = 'icd10:R10.31') then 1 else 0 end) as icd9_789_03icd10R10_31,
                    max(case when (trd.dx_code_id='icd9:789.04' or trd.dx_code_id = 'icd10:R10.32') then 1 else 0 end) as icd9_789_04R10_32,
                    max(case when (trd.dx_code_id='icd9:789.07' or trd.dx_code_id = 'icd10:R10.84') then 1 else 0 end) as icd9_789_07icd10R10_84,
                    max(case when (trd.dx_code_id='icd9:789.09' or trd.dx_code_id='icd10:R10.10') then 1 else 0 end) as icd9_789_09icd10R10_10
                  from temp_rep_dx trd
                  join temp_chlamyd_episode epi on trd.patient_id=epi.patient_id 
                 where trd.date between epi.date - interval '14 days' and epi.date + interval '14 days' --from conf_conditionconfig
                 group by trd.patient_id, epi.date  order by trd.patient_id, epi.date ) symp on symp.patient_id=epi.patient_id and symp.edate=epi.date
where epi.date >= '01-01-2007'  
 order by epi.date;
 

