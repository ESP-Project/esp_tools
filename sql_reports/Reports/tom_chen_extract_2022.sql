select aa as patient_id, '2022_12' as year_month, 
  case when aw = 1 or ax = 1 then 1 else 0 end as diabetes_1_and_2, 
  bc as asthma,
  bb as smoking3,
  by as dhypert,
  bk as chypert,
  aj as prior2, 
  case 
    when t1.age>=18 and t1.age <=24 then '18-24'
    when t1.age>=25 and t1.age <=29 then '25-29'
    when t1.age>=30 and t1.age <=34 then '30-34'
    when t1.age>=35 and t1.age <=44 then '35-44'
    when t1.age>=45 and t1.age <=54 then '45-54'
    when t1.age>=55 and t1.age <=64 then '55-64'
    when t1.age>=65 and t1.age <=74 then '65-74'
    when t1.age>=75 and t1.age <=84 then '75-84'
    when t1.age>=85 then '85+'
  end as age_group
into temporary cohort 
from gen_pop_tools.tt_out t0
join gen_pop_tools.tt_pat_seq_enc t1 on t1.patient_id=t0.aa and t1.year_month=t0.af||'_'||t0.ag
where af='2022' and ag='12' and aj>=1 and t1.age>=18;

Select patient_id, date, bmi
  into temporary tmp_bmi
  from emr_encounter e
  join emr_patient p on e.patient_id=p.id
  where date <= '2022-12-31'::date 
    and bmi is not null;

select distinct on (patient_id) 
  patient_id, date as bmi_date, bmi
into temporary last_bmi from tmp_bmi
order by patient_id, date desc;

select patient_id, date, tobacco_use as smoking
into temporary tmp_smoking
from emr_socialhistory
where tobacco_use is not null and date <= '2022-12-31'::date;

select distinct on (patient_id)
 patient_id, date as smoking_date, smoking
into temporary last_smoking 
from tmp_smoking
order by patient_id, date desc;

select patient_id, smoking_date, code as smoking
into temporary last_smoking_mapped
from last_smoking
join gen_pop_tools.rs_conf_mapping cm on cm.src_field='tobacco_use' and src_value=smoking;

SELECT T1.patient_id,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I09.9%' OR T3.dx_code_id ilike 'icd10:I11.0%' OR T3.dx_code_id ilike 'icd10:I13.0%' 
				  OR T3.dx_code_id ilike 'icd10:I13.2%' OR T3.dx_code_id ilike 'icd10:I25.5%' OR T3.dx_code_id ilike 'icd10:I42.0%' 
				  OR T3.dx_code_id ilike 'icd10:I42.5%' OR T3.dx_code_id ilike 'icd10:I42.6%' OR T3.dx_code_id ilike 'icd10:I42.7%' 
				  OR T3.dx_code_id ilike 'icd10:I42.8%' OR T3.dx_code_id ilike 'icd10:I42.9%' OR T3.dx_code_id ilike 'icd10:I43.%' 
				  OR T3.dx_code_id ilike 'icd10:I50.%'  OR T3.dx_code_id ilike 'icd10:P29.0%' 
		then 2 else 0 END) as cci_chf,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:F00.%' OR T3.dx_code_id ilike 'icd10:F01.%' OR T3.dx_code_id ilike 'icd10:F02.%' 
				  OR T3.dx_code_id ilike 'icd10:F03.%' OR T3.dx_code_id ilike 'icd10:F05.1%' OR T3.dx_code_id ilike 'icd10:G30.%' 
                  OR T3.dx_code_id ilike 'icd10:G31.1%' 
		then 2 else 0 END) as cci_dementia,	
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I27.8%' OR T3.dx_code_id ilike 'icd10:I27.9%' OR T3.dx_code_id ilike 'icd10:J40.%' 
				  OR T3.dx_code_id ilike 'icd10:J41.%' OR T3.dx_code_id ilike 'icd10:J42.%' OR T3.dx_code_id ilike 'icd10:J43.%' 
				  OR T3.dx_code_id ilike 'icd10:J44.%' OR T3.dx_code_id ilike 'icd10:J45.%' OR T3.dx_code_id ilike 'icd10:J46.%' 
				  OR T3.dx_code_id ilike 'icd10:J47.%' OR T3.dx_code_id ilike 'icd10:J60.%' OR T3.dx_code_id ilike 'icd10:J61.%' 
				  OR T3.dx_code_id ilike 'icd10:J62.%' OR T3.dx_code_id ilike 'icd10:J63.%' OR T3.dx_code_id ilike 'icd10:J64.%' 
				  OR T3.dx_code_id ilike 'icd10:J65.%' OR T3.dx_code_id ilike 'icd10:J66.%' OR T3.dx_code_id ilike 'icd10:J67.%' 
				  OR T3.dx_code_id ilike 'icd10:J68.4%' OR T3.dx_code_id ilike 'icd10:J70.1%' OR T3.dx_code_id ilike 'icd10:J70.3%' 
        then 1 else 0 END) as cci_chron_pulm_dis,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:M05.%' OR T3.dx_code_id ilike 'icd10:M06.%' OR T3.dx_code_id ilike 'icd10:M31.5%' 
				  OR T3.dx_code_id ilike 'icd10:M32.%' OR T3.dx_code_id ilike 'icd10:M33.%' OR T3.dx_code_id ilike 'icd10:M34.%' 
				  OR T3.dx_code_id ilike 'icd10:M35.1%' OR T3.dx_code_id ilike 'icd10:M35.3%' OR T3.dx_code_id ilike 'icd10:M36.0%' 
        then 1 else 0 END) as cci_rheaumatic_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:B18.%' OR T3.dx_code_id ilike 'icd10:K70.0%' OR T3.dx_code_id ilike 'icd10:K70.1%' 
				  OR T3.dx_code_id ilike 'icd10:K70.2%' OR T3.dx_code_id ilike 'icd10:K70.3%' OR T3.dx_code_id ilike 'icd10:K70.9%' 
				  OR T3.dx_code_id ilike 'icd10:K71.3%' OR T3.dx_code_id ilike 'icd10:K71.4%' OR T3.dx_code_id ilike 'icd10:K71.5%' 
				  OR T3.dx_code_id ilike 'icd10:K71.7%' OR T3.dx_code_id ilike 'icd10:K73.%' OR T3.dx_code_id ilike 'icd10:K74.%' 
				  OR T3.dx_code_id ilike 'icd10:K76.0%' OR T3.dx_code_id ilike 'icd10:K76.2%' OR T3.dx_code_id ilike 'icd10:K76.3%' 
				  OR T3.dx_code_id ilike 'icd10:K76.4%' OR T3.dx_code_id ilike 'icd10:K76.8%' OR T3.dx_code_id ilike 'icd10:K76.9%' 
				  OR T3.dx_code_id ilike 'icd10:Z94.4%' 
        then 2 else 0 END) as cci_mild_liver_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:E10.2%' OR T3.dx_code_id ilike 'icd10:E10.3%' OR T3.dx_code_id ilike 'icd10:E10.4%' 
				  OR T3.dx_code_id ilike 'icd10:E10.5%' OR T3.dx_code_id ilike 'icd10:E10.7%' OR T3.dx_code_id ilike 'icd10:E11.2%' 
				  OR T3.dx_code_id ilike 'icd10:E11.3%' OR T3.dx_code_id ilike 'icd10:E11.4%' OR T3.dx_code_id ilike 'icd10:E11.5%' 
				  OR T3.dx_code_id ilike 'icd10:E11.7%' OR T3.dx_code_id ilike 'icd10:E12.2%' OR T3.dx_code_id ilike 'icd10:E12.3%' 
				  OR T3.dx_code_id ilike 'icd10:E12.4%' OR T3.dx_code_id ilike 'icd10:E12.5%' OR T3.dx_code_id ilike 'icd10:E12.7%' 
				  OR T3.dx_code_id ilike 'icd10:E13.2%' OR T3.dx_code_id ilike 'icd10:E13.3%' OR T3.dx_code_id ilike 'icd10:E13.4%' 
				  OR T3.dx_code_id ilike 'icd10:E13.5%' OR T3.dx_code_id ilike 'icd10:E13.7%' OR T3.dx_code_id ilike 'icd10:E14.2%' 
				  OR T3.dx_code_id ilike 'icd10:E14.3%' OR T3.dx_code_id ilike 'icd10:E14.4%' OR T3.dx_code_id ilike 'icd10:E14.5%' 
				  OR T3.dx_code_id ilike 'icd10:E14.7%' 
        then 1 else 0 END) as cci_diab_w_compl,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:G04.1%' OR T3.dx_code_id ilike 'icd10:G11.4%' OR T3.dx_code_id ilike 'icd10:G80.1%' 
				  OR T3.dx_code_id ilike 'icd10:G80.2%' OR T3.dx_code_id ilike 'icd10:G81.%' OR T3.dx_code_id ilike 'icd10:G82.%' 
				  OR T3.dx_code_id ilike 'icd10:G83.0%' OR T3.dx_code_id ilike 'icd10:G83.1%' OR T3.dx_code_id ilike 'icd10:G83.2%' 
				  OR T3.dx_code_id ilike 'icd10:G83.3%' OR T3.dx_code_id ilike 'icd10:G83.4%' OR T3.dx_code_id ilike 'icd10:G83.9%' 
        then 2 else 0 END) as cci_hemip_or_parap,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I12.0%' OR T3.dx_code_id ilike 'icd10:I13.1%' OR T3.dx_code_id ilike 'icd10:N03.2%' 
				  OR T3.dx_code_id ilike 'icd10:N03.3%' OR T3.dx_code_id ilike 'icd10:N03.4%' OR T3.dx_code_id ilike 'icd10:N03.5%' 
				  OR T3.dx_code_id ilike 'icd10:N03.6%' OR T3.dx_code_id ilike 'icd10:N03.7%' OR T3.dx_code_id ilike 'icd10:N05.2%' 
				  OR T3.dx_code_id ilike 'icd10:N05.3%' OR T3.dx_code_id ilike 'icd10:N05.4%' OR T3.dx_code_id ilike 'icd10:N05.5%' 
				  OR T3.dx_code_id ilike 'icd10:N05.6%' OR T3.dx_code_id ilike 'icd10:N05.7%' OR T3.dx_code_id ilike 'icd10:N18.%' 
				  OR T3.dx_code_id ilike 'icd10:N19.%' OR T3.dx_code_id ilike 'icd10:N25.0%' OR T3.dx_code_id ilike 'icd10:Z49.0%' 
				  OR T3.dx_code_id ilike 'icd10:Z49.1%' OR T3.dx_code_id ilike 'icd10:Z49.2%' OR T3.dx_code_id ilike 'icd10:Z94.0%' 
				  OR T3.dx_code_id ilike 'icd10:Z99.2%' 
        then 1 else 0 END) as cci_renal_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:C00.%' OR T3.dx_code_id ilike 'icd10:C01.%' OR T3.dx_code_id ilike 'icd10:C02.%' 
				  OR T3.dx_code_id ilike 'icd10:C03.%' OR T3.dx_code_id ilike 'icd10:C04.%' OR T3.dx_code_id ilike 'icd10:C05.%' 
				  OR T3.dx_code_id ilike 'icd10:C06.%' OR T3.dx_code_id ilike 'icd10:C07.%' OR T3.dx_code_id ilike 'icd10:C08.%' 
				  OR T3.dx_code_id ilike 'icd10:C09.%' OR T3.dx_code_id ilike 'icd10:C10.%' OR T3.dx_code_id ilike 'icd10:C11.%' 
				  OR T3.dx_code_id ilike 'icd10:C12.%' OR T3.dx_code_id ilike 'icd10:C13.%' OR T3.dx_code_id ilike 'icd10:C14.%' 
				  OR T3.dx_code_id ilike 'icd10:C15.%' OR T3.dx_code_id ilike 'icd10:C16.%' OR T3.dx_code_id ilike 'icd10:C17.%' 
				  OR T3.dx_code_id ilike 'icd10:C18.%' OR T3.dx_code_id ilike 'icd10:C19.%' OR T3.dx_code_id ilike 'icd10:C20.%' 
				  OR T3.dx_code_id ilike 'icd10:C21.%' OR T3.dx_code_id ilike 'icd10:C22.%' OR T3.dx_code_id ilike 'icd10:C23.%' 
				  OR T3.dx_code_id ilike 'icd10:C24.%' OR T3.dx_code_id ilike 'icd10:C25.%' OR T3.dx_code_id ilike 'icd10:C26.%' 
				  OR T3.dx_code_id ilike 'icd10:C30.%' OR T3.dx_code_id ilike 'icd10:C31.%' OR T3.dx_code_id ilike 'icd10:C32.%' 
				  OR T3.dx_code_id ilike 'icd10:C33.%' OR T3.dx_code_id ilike 'icd10:C34.%' OR T3.dx_code_id ilike 'icd10:C37.%' 
				  OR T3.dx_code_id ilike 'icd10:C38.%' OR T3.dx_code_id ilike 'icd10:C39.%' OR T3.dx_code_id ilike 'icd10:C40.%' 
				  OR T3.dx_code_id ilike 'icd10:C41.%' OR T3.dx_code_id ilike 'icd10:C43.%' OR T3.dx_code_id ilike 'icd10:C45.%' 
				  OR T3.dx_code_id ilike 'icd10:C46.%' OR T3.dx_code_id ilike 'icd10:C47.%' OR T3.dx_code_id ilike 'icd10:C48.%' 
				  OR T3.dx_code_id ilike 'icd10:C49.%' OR T3.dx_code_id ilike 'icd10:C50.%' OR T3.dx_code_id ilike 'icd10:C51.%' 
				  OR T3.dx_code_id ilike 'icd10:C52.%' OR T3.dx_code_id ilike 'icd10:C53.%' OR T3.dx_code_id ilike 'icd10:C54.%' 
				  OR T3.dx_code_id ilike 'icd10:C55.%' OR T3.dx_code_id ilike 'icd10:C56.%' OR T3.dx_code_id ilike 'icd10:C57.%' 
				  OR T3.dx_code_id ilike 'icd10:C58.%' OR T3.dx_code_id ilike 'icd10:C60.%' OR T3.dx_code_id ilike 'icd10:C61.%' 
				  OR T3.dx_code_id ilike 'icd10:C62.%' OR T3.dx_code_id ilike 'icd10:C63.%' OR T3.dx_code_id ilike 'icd10:C64.%' 
				  OR T3.dx_code_id ilike 'icd10:C65.%' OR T3.dx_code_id ilike 'icd10:C66.%' OR T3.dx_code_id ilike 'icd10:C67.%' 
				  OR T3.dx_code_id ilike 'icd10:C68.%' OR T3.dx_code_id ilike 'icd10:C69.%' OR T3.dx_code_id ilike 'icd10:C70.%' 
				  OR T3.dx_code_id ilike 'icd10:C71.%' OR T3.dx_code_id ilike 'icd10:C72.%' OR T3.dx_code_id ilike 'icd10:C73.%' 
				  OR T3.dx_code_id ilike 'icd10:C74.%' OR T3.dx_code_id ilike 'icd10:C75.%' OR T3.dx_code_id ilike 'icd10:C76.%' 
				  OR T3.dx_code_id ilike 'icd10:C81.%' OR T3.dx_code_id ilike 'icd10:C82.%' OR T3.dx_code_id ilike 'icd10:C83.%' 
				  OR T3.dx_code_id ilike 'icd10:C84.%' OR T3.dx_code_id ilike 'icd10:C85.%' OR T3.dx_code_id ilike 'icd10:C88.%' 
				  OR T3.dx_code_id ilike 'icd10:C90.%' OR T3.dx_code_id ilike 'icd10:C91.%' OR T3.dx_code_id ilike 'icd10:C92.%' 
				  OR T3.dx_code_id ilike 'icd10:C93.%' OR T3.dx_code_id ilike 'icd10:C94.%' OR T3.dx_code_id ilike 'icd10:C95.%' 
				  OR T3.dx_code_id ilike 'icd10:C96.%' OR T3.dx_code_id ilike 'icd10:C97.%' 
        then 2 else 0 END) as cci_any_malig,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I85.0%' OR T3.dx_code_id ilike 'icd10:I85.9%' OR T3.dx_code_id ilike 'icd10:I86.4%' 
				  OR T3.dx_code_id ilike 'icd10:I98.2%' OR T3.dx_code_id ilike 'icd10:K70.4%' OR T3.dx_code_id ilike 'icd10:K71.1%' 
				  OR T3.dx_code_id ilike 'icd10:K72.1%' OR T3.dx_code_id ilike 'icd10:K72.9%' OR T3.dx_code_id ilike 'icd10:K76.5%' 
				  OR T3.dx_code_id ilike 'icd10:K76.6%' OR T3.dx_code_id ilike 'icd10:K76.7%' 
        then 4 else 0 END) as cci_mod_or_severe_liver_disease,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:C77.%' OR T3.dx_code_id ilike 'icd10:C78.%' OR T3.dx_code_id ilike 'icd10:C79.%' 
				  OR T3.dx_code_id ilike 'icd10:C80.%' 
        then 6 else 0 END) as cci_met_solid_tumor,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:B20.%' OR T3.dx_code_id ilike 'icd10:B21.%' OR T3.dx_code_id ilike 'icd10:B22.%' 
				  OR T3.dx_code_id ilike 'icd10:B24.%' 
         then 4 else 0 END) as cci_aids_hiv
	into temporary lc_cci_icds
	FROM cohort T1
	left JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id) and T2.date <= '2022-12-31'::date
	left JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
	AND (
			T3.dx_code_id ilike 'icd10:I09.9%' 
			OR T3.dx_code_id ilike 'icd10:I11.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.2%' 
			OR T3.dx_code_id ilike 'icd10:I25.5%' 
			OR T3.dx_code_id ilike 'icd10:I42.0%' 
			OR T3.dx_code_id ilike 'icd10:I42.5%' 
			OR T3.dx_code_id ilike 'icd10:I42.6%' 
			OR T3.dx_code_id ilike 'icd10:I42.7%' 
			OR T3.dx_code_id ilike 'icd10:I42.8%' 
			OR T3.dx_code_id ilike 'icd10:I42.9%' 
			OR T3.dx_code_id ilike 'icd10:I43.%' 
			OR T3.dx_code_id ilike 'icd10:I50.%' 
			OR T3.dx_code_id ilike 'icd10:P29.0%' 
			OR T3.dx_code_id ilike 'icd10:F00.%' 
			OR T3.dx_code_id ilike 'icd10:F01.%' 
			OR T3.dx_code_id ilike 'icd10:F02.%' 
			OR T3.dx_code_id ilike 'icd10:F03.%' 
			OR T3.dx_code_id ilike 'icd10:F05.1%' 
			OR T3.dx_code_id ilike 'icd10:G30.%' 
			OR T3.dx_code_id ilike 'icd10:G31.1%' 
			OR T3.dx_code_id ilike 'icd10:I27.8%' 
			OR T3.dx_code_id ilike 'icd10:I27.9%' 
			OR T3.dx_code_id ilike 'icd10:J40.%' 
			OR T3.dx_code_id ilike 'icd10:J41.%' 
			OR T3.dx_code_id ilike 'icd10:J42.%' 
			OR T3.dx_code_id ilike 'icd10:J43.%' 
			OR T3.dx_code_id ilike 'icd10:J44.%' 
			OR T3.dx_code_id ilike 'icd10:J45.%' 
			OR T3.dx_code_id ilike 'icd10:J46.%' 
			OR T3.dx_code_id ilike 'icd10:J47.%' 
			OR T3.dx_code_id ilike 'icd10:J60.%' 
			OR T3.dx_code_id ilike 'icd10:J61.%' 
			OR T3.dx_code_id ilike 'icd10:J62.%' 
			OR T3.dx_code_id ilike 'icd10:J63.%' 
			OR T3.dx_code_id ilike 'icd10:J64.%' 
			OR T3.dx_code_id ilike 'icd10:J65.%' 
			OR T3.dx_code_id ilike 'icd10:J66.%' 
			OR T3.dx_code_id ilike 'icd10:J67.%' 
			OR T3.dx_code_id ilike 'icd10:J68.4%' 
			OR T3.dx_code_id ilike 'icd10:J70.1%' 
			OR T3.dx_code_id ilike 'icd10:J70.3%' 
			OR T3.dx_code_id ilike 'icd10:M05.%' 
			OR T3.dx_code_id ilike 'icd10:M06.%' 
			OR T3.dx_code_id ilike 'icd10:M31.5%' 
			OR T3.dx_code_id ilike 'icd10:M32.%' 
			OR T3.dx_code_id ilike 'icd10:M33.%' 
			OR T3.dx_code_id ilike 'icd10:M34.%' 
			OR T3.dx_code_id ilike 'icd10:M35.1%' 
			OR T3.dx_code_id ilike 'icd10:M35.3%' 
			OR T3.dx_code_id ilike 'icd10:M36.0%' 
			OR T3.dx_code_id ilike 'icd10:B18.%' 
			OR T3.dx_code_id ilike 'icd10:K70.0%' 
			OR T3.dx_code_id ilike 'icd10:K70.1%' 
			OR T3.dx_code_id ilike 'icd10:K70.2%' 
			OR T3.dx_code_id ilike 'icd10:K70.3%' 
			OR T3.dx_code_id ilike 'icd10:K70.9%' 
			OR T3.dx_code_id ilike 'icd10:K71.3%' 
			OR T3.dx_code_id ilike 'icd10:K71.4%' 
			OR T3.dx_code_id ilike 'icd10:K71.5%' 
			OR T3.dx_code_id ilike 'icd10:K71.7%' 
			OR T3.dx_code_id ilike 'icd10:K73.%' 
			OR T3.dx_code_id ilike 'icd10:K74.%' 
			OR T3.dx_code_id ilike 'icd10:K76.0%' 
			OR T3.dx_code_id ilike 'icd10:K76.2%' 
			OR T3.dx_code_id ilike 'icd10:K76.3%' 
			OR T3.dx_code_id ilike 'icd10:K76.4%' 
			OR T3.dx_code_id ilike 'icd10:K76.8%' 
			OR T3.dx_code_id ilike 'icd10:K76.9%' 
			OR T3.dx_code_id ilike 'icd10:Z94.4%' 
			OR T3.dx_code_id ilike 'icd10:E10.2%' 
			OR T3.dx_code_id ilike 'icd10:E10.3%' 
			OR T3.dx_code_id ilike 'icd10:E10.4%' 
			OR T3.dx_code_id ilike 'icd10:E10.5%' 
			OR T3.dx_code_id ilike 'icd10:E10.7%' 
			OR T3.dx_code_id ilike 'icd10:E11.2%' 
			OR T3.dx_code_id ilike 'icd10:E11.3%' 
			OR T3.dx_code_id ilike 'icd10:E11.4%' 
			OR T3.dx_code_id ilike 'icd10:E11.5%' 
			OR T3.dx_code_id ilike 'icd10:E11.7%' 
			OR T3.dx_code_id ilike 'icd10:E12.2%' 
			OR T3.dx_code_id ilike 'icd10:E12.3%' 
			OR T3.dx_code_id ilike 'icd10:E12.4%' 
			OR T3.dx_code_id ilike 'icd10:E12.5%' 
			OR T3.dx_code_id ilike 'icd10:E12.7%' 
			OR T3.dx_code_id ilike 'icd10:E13.2%' 
			OR T3.dx_code_id ilike 'icd10:E13.3%' 
			OR T3.dx_code_id ilike 'icd10:E13.4%' 
			OR T3.dx_code_id ilike 'icd10:E13.5%' 
			OR T3.dx_code_id ilike 'icd10:E13.7%' 
			OR T3.dx_code_id ilike 'icd10:E14.2%' 
			OR T3.dx_code_id ilike 'icd10:E14.3%' 
			OR T3.dx_code_id ilike 'icd10:E14.4%' 
			OR T3.dx_code_id ilike 'icd10:E14.5%' 
			OR T3.dx_code_id ilike 'icd10:E14.7%' 
			OR T3.dx_code_id ilike 'icd10:G04.1%' 
			OR T3.dx_code_id ilike 'icd10:G11.4%' 
			OR T3.dx_code_id ilike 'icd10:G80.1%' 
			OR T3.dx_code_id ilike 'icd10:G80.2%' 
			OR T3.dx_code_id ilike 'icd10:G81.%' 
			OR T3.dx_code_id ilike 'icd10:G82.%' 
			OR T3.dx_code_id ilike 'icd10:G83.0%' 
			OR T3.dx_code_id ilike 'icd10:G83.1%' 
			OR T3.dx_code_id ilike 'icd10:G83.2%' 
			OR T3.dx_code_id ilike 'icd10:G83.3%' 
			OR T3.dx_code_id ilike 'icd10:G83.4%' 
			OR T3.dx_code_id ilike 'icd10:G83.9%' 
			OR T3.dx_code_id ilike 'icd10:I12.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.1%' 
			OR T3.dx_code_id ilike 'icd10:N03.2%' 
			OR T3.dx_code_id ilike 'icd10:N03.3%' 
			OR T3.dx_code_id ilike 'icd10:N03.4%' 
			OR T3.dx_code_id ilike 'icd10:N03.5%' 
			OR T3.dx_code_id ilike 'icd10:N03.6%' 
			OR T3.dx_code_id ilike 'icd10:N03.7%' 
			OR T3.dx_code_id ilike 'icd10:N05.2%' 
			OR T3.dx_code_id ilike 'icd10:N05.3%' 
			OR T3.dx_code_id ilike 'icd10:N05.4%' 
			OR T3.dx_code_id ilike 'icd10:N05.5%' 
			OR T3.dx_code_id ilike 'icd10:N05.6%' 
			OR T3.dx_code_id ilike 'icd10:N05.7%' 
			OR T3.dx_code_id ilike 'icd10:N18.%' 
			OR T3.dx_code_id ilike 'icd10:N19.%' 
			OR T3.dx_code_id ilike 'icd10:N25.0%' 
			OR T3.dx_code_id ilike 'icd10:Z49.0%' 
			OR T3.dx_code_id ilike 'icd10:Z49.1%' 
			OR T3.dx_code_id ilike 'icd10:Z49.2%' 
			OR T3.dx_code_id ilike 'icd10:Z94.0%' 
			OR T3.dx_code_id ilike 'icd10:Z99.2%' 
			OR T3.dx_code_id ilike 'icd10:C00.%' 
			OR T3.dx_code_id ilike 'icd10:C01.%' 
			OR T3.dx_code_id ilike 'icd10:C02.%' 
			OR T3.dx_code_id ilike 'icd10:C03.%' 
			OR T3.dx_code_id ilike 'icd10:C04.%' 
			OR T3.dx_code_id ilike 'icd10:C05.%' 
			OR T3.dx_code_id ilike 'icd10:C06.%' 
			OR T3.dx_code_id ilike 'icd10:C07.%' 
			OR T3.dx_code_id ilike 'icd10:C08.%' 
			OR T3.dx_code_id ilike 'icd10:C09.%' 
			OR T3.dx_code_id ilike 'icd10:C10.%' 
			OR T3.dx_code_id ilike 'icd10:C11.%' 
			OR T3.dx_code_id ilike 'icd10:C12.%' 
			OR T3.dx_code_id ilike 'icd10:C13.%' 
			OR T3.dx_code_id ilike 'icd10:C14.%' 
			OR T3.dx_code_id ilike 'icd10:C15.%' 
			OR T3.dx_code_id ilike 'icd10:C16.%' 
			OR T3.dx_code_id ilike 'icd10:C17.%' 
			OR T3.dx_code_id ilike 'icd10:C18.%' 
			OR T3.dx_code_id ilike 'icd10:C19.%' 
			OR T3.dx_code_id ilike 'icd10:C20.%' 
			OR T3.dx_code_id ilike 'icd10:C21.%' 
			OR T3.dx_code_id ilike 'icd10:C22.%' 
			OR T3.dx_code_id ilike 'icd10:C23.%' 
			OR T3.dx_code_id ilike 'icd10:C24.%' 
			OR T3.dx_code_id ilike 'icd10:C25.%' 
			OR T3.dx_code_id ilike 'icd10:C26.%' 
			OR T3.dx_code_id ilike 'icd10:C30.%' 
			OR T3.dx_code_id ilike 'icd10:C31.%' 
			OR T3.dx_code_id ilike 'icd10:C32.%' 
			OR T3.dx_code_id ilike 'icd10:C33.%' 
			OR T3.dx_code_id ilike 'icd10:C34.%' 
			OR T3.dx_code_id ilike 'icd10:C37.%' 
			OR T3.dx_code_id ilike 'icd10:C38.%' 
			OR T3.dx_code_id ilike 'icd10:C39.%' 
			OR T3.dx_code_id ilike 'icd10:C40.%' 
			OR T3.dx_code_id ilike 'icd10:C41.%' 
			OR T3.dx_code_id ilike 'icd10:C43.%' 
			OR T3.dx_code_id ilike 'icd10:C45.%' 
			OR T3.dx_code_id ilike 'icd10:C46.%' 
			OR T3.dx_code_id ilike 'icd10:C47.%' 
			OR T3.dx_code_id ilike 'icd10:C48.%' 
			OR T3.dx_code_id ilike 'icd10:C49.%' 
			OR T3.dx_code_id ilike 'icd10:C50.%' 
			OR T3.dx_code_id ilike 'icd10:C51.%' 
			OR T3.dx_code_id ilike 'icd10:C52.%' 
			OR T3.dx_code_id ilike 'icd10:C53.%' 
			OR T3.dx_code_id ilike 'icd10:C54.%' 
			OR T3.dx_code_id ilike 'icd10:C55.%' 
			OR T3.dx_code_id ilike 'icd10:C56.%' 
			OR T3.dx_code_id ilike 'icd10:C57.%' 
			OR T3.dx_code_id ilike 'icd10:C58.%' 
			OR T3.dx_code_id ilike 'icd10:C60.%' 
			OR T3.dx_code_id ilike 'icd10:C61.%' 
			OR T3.dx_code_id ilike 'icd10:C62.%' 
			OR T3.dx_code_id ilike 'icd10:C63.%' 
			OR T3.dx_code_id ilike 'icd10:C64.%' 
			OR T3.dx_code_id ilike 'icd10:C65.%' 
			OR T3.dx_code_id ilike 'icd10:C66.%' 
			OR T3.dx_code_id ilike 'icd10:C67.%' 
			OR T3.dx_code_id ilike 'icd10:C68.%' 
			OR T3.dx_code_id ilike 'icd10:C69.%' 
			OR T3.dx_code_id ilike 'icd10:C70.%' 
			OR T3.dx_code_id ilike 'icd10:C71.%' 
			OR T3.dx_code_id ilike 'icd10:C72.%' 
			OR T3.dx_code_id ilike 'icd10:C73.%' 
			OR T3.dx_code_id ilike 'icd10:C74.%' 
			OR T3.dx_code_id ilike 'icd10:C75.%' 
			OR T3.dx_code_id ilike 'icd10:C76.%' 
			OR T3.dx_code_id ilike 'icd10:C81.%' 
			OR T3.dx_code_id ilike 'icd10:C82.%' 
			OR T3.dx_code_id ilike 'icd10:C83.%' 
			OR T3.dx_code_id ilike 'icd10:C84.%' 
			OR T3.dx_code_id ilike 'icd10:C85.%' 
			OR T3.dx_code_id ilike 'icd10:C88.%' 
			OR T3.dx_code_id ilike 'icd10:C90.%' 
			OR T3.dx_code_id ilike 'icd10:C91.%' 
			OR T3.dx_code_id ilike 'icd10:C92.%' 
			OR T3.dx_code_id ilike 'icd10:C93.%' 
			OR T3.dx_code_id ilike 'icd10:C94.%' 
			OR T3.dx_code_id ilike 'icd10:C95.%' 
			OR T3.dx_code_id ilike 'icd10:C96.%' 
			OR T3.dx_code_id ilike 'icd10:C97.%' 
			OR T3.dx_code_id ilike 'icd10:I85.0%' 
			OR T3.dx_code_id ilike 'icd10:I85.9%' 
			OR T3.dx_code_id ilike 'icd10:I86.4%' 
			OR T3.dx_code_id ilike 'icd10:I98.2%' 
			OR T3.dx_code_id ilike 'icd10:K70.4%' 
			OR T3.dx_code_id ilike 'icd10:K71.1%' 
			OR T3.dx_code_id ilike 'icd10:K72.1%' 
			OR T3.dx_code_id ilike 'icd10:K72.9%' 
			OR T3.dx_code_id ilike 'icd10:K76.5%' 
			OR T3.dx_code_id ilike 'icd10:K76.6%' 
			OR T3.dx_code_id ilike 'icd10:K76.7%' 
			OR T3.dx_code_id ilike 'icd10:C77.%' 
			OR T3.dx_code_id ilike 'icd10:C78.%' 
			OR T3.dx_code_id ilike 'icd10:C79.%' 
			OR T3.dx_code_id ilike 'icd10:C80.%' 
			OR T3.dx_code_id ilike 'icd10:B20.%' 
			OR T3.dx_code_id ilike 'icd10:B21.%' 
			OR T3.dx_code_id ilike 'icd10:B22.%' 
			OR T3.dx_code_id ilike 'icd10:B24.%' 
		)
	GROUP BY T1.patient_id;
	
	SELECT patient_id, coalesce((cci_chf + cci_dementia + cci_chron_pulm_dis + cci_rheaumatic_dis + cci_mild_liver_dis + 
						   cci_diab_w_compl + cci_hemip_or_parap + cci_renal_dis + cci_any_malig + cci_mod_or_severe_liver_disease +
						   cci_met_solid_tumor + cci_aids_hiv), 0) as cci_score
	into temporary lc_index
	FROM lc_cci_icds;

select t0.diabetes_1_and_2, t0.asthma, t0.chypert, 
  case when t1.bmi>=30 then 1
       when t1.bmi<30 then 0
       else null end as bmi1, 
  t2.smoking as smoking1, t0.age_group,
  case t0.smoking3 when 0 then null else t0.smoking3 end smoking3, t0.prior2,
  m1.mapped_value as race, m2.mapped_value as ethnicity, p.home_language,
  upper(substr(p.gender,1,1)) as gender, city, zip5,
  cci_score, cci_chf, cci_dementia, cci_chron_pulm_dis, cci_rheaumatic_dis, cci_mild_liver_dis, 
  cci_diab_w_compl, cci_hemip_or_parap, cci_renal_dis, cci_any_malig, cci_mod_or_severe_liver_disease,
  cci_met_solid_tumor, cci_aids_hiv
into temporary output_tchen_2022
from cohort t0 join emr_patient p on p.id=t0.patient_id and state = 'MA'
left join gen_pop_tools.rs_conf_mapping m1 on m1.src_field='race' and p.race=m1.src_value
left join gen_pop_tools.rs_conf_mapping m2 on m2.src_field='ethnicity' and p.ethnicity=m2.src_value
left join last_bmi t1 on t1.patient_id=t0.patient_id and t1.bmi_date between '2021-01-01'::date and '2022-12-31'::date
left join last_smoking_mapped t2 on t2.patient_id=t0.patient_id and t2.smoking_date <='2022-12-31'::Date
left join lc_index t3 on t3.patient_id=t0.patient_id
left join lc_cci_icds t4 on t4.patient_id=t0.patient_id;

\copy output_tchen_2022 to '/tmp/tchen_output_2022.csv' csv delimiter ',' header;



