﻿DROP TABLE IF EXISTS lyme_agg_rpt_doxy_all;
DROP TABLE IF EXISTS lyme_agg_rpt_indexpats;
DROP TABLE IF EXISTS lyme_agg_rpt_lyme_confirmed;
DROP TABLE IF EXISTS lyme_agg_rpt_labs;
DROP TABLE IF EXISTS lyme_agg_rpt_icds;
DROP TABLE IF EXISTS lyme_agg_rpt_lyme_possible;
DROP TABLE IF EXISTS lyme_agg_rpt_output;

-- 1.	Count of patients with a 1 day prescription for doxycycline between January 1, 2013 and December 31, 2015.  
--	One day of treatment can be derived from either the duration of antibiotics on the prescription, end date = start date, or quantity dispensed ≤2 pills

CREATE TABLE lyme_agg_rpt_doxy_all AS
SELECT rx.*, (end_date - start_date) date_diff
FROM  emr_prescription rx, hef_event h
WHERE rx.patient_id = h.patient_id 
AND rx.id = h.object_id 
AND h.name in ('rx:doxycycline', 'rx:doxycycline_7_day')
AND h.date BETWEEN '01-01-2013' AND '12-31-2015'
AND (refills::integer = 0 or refills::INTEGER = 1 OR refills is null OR refills!~E'^\\d+$' OR directions ilike '%1 dose%' OR directions ilike '%lyme%' OR directions ilike '%tick%')
AND (quantity_float is NOT null AND quantity_float <=2)
--and (rx.patient_class is null or rx.patient_class ilike '1.%')
order by rx.id, rx.name;

CREATE TABLE lyme_agg_rpt_indexpats AS
SELECT DISTINCT(patient_id) 
FROM lyme_agg_rpt_doxy;


-- 2.	Among those in #1, count ESP confirmed Lyme cases (i.e., meet the ESP case definition for Lyme)

CREATE TABLE lyme_agg_rpt_lyme_confirmed AS
SELECT i.patient_id
FROM lyme_agg_rpt_indexpats i, nodis_case c
WHERE i.patient_id = c.patient_id
AND c.condition = 'lyme';

-- 3.	Among those in #1, count possible Lyme cases.  Define Possible Lyme as patients with either:
--	an ICD9 or ICD10 diagnosis code for Lyme disease (see below for codes), or
--	a Lyme lab test (ELISA, WB, or PCR) with any result

-- Patients with ICD for Lyme
CREATE TABLE lyme_agg_rpt_icds AS
select i.patient_id, dx_code_id, date icd_date
FROM emr_encounter e,
emr_encounter_dx_codes dx,
lyme_agg_rpt_indexpats i
WHERE e.id = dx.encounter_id
AND i.patient_id = e.patient_id
AND dx.dx_code_id in ('icd9:088.81','icd10:A69.20','icd10:A69.23','icd10:A69.29','icd10:A69.21','icd10:A69.22')
AND e.date BETWEEN '01-01-2013' AND '12-31-2015';

-- Patients with a lab test for lyme
-- VIEW DATA FROM THIS TABLE TO CHECK FOR UNMAPPED RESULT STRINGS
CREATE TABLE lyme_agg_rpt_labs AS
select l.patient_id, native_name, test_name, l.date lab_date, l.result_string, l.specimen_source, l.order_natural_key, l.ref_high_float
FROM emr_labresult l, conf_labtestmap c, lyme_agg_rpt_indexpats i
WHERE l.native_code = c.native_code
AND  i.patient_id = l.patient_id
AND c.test_name ilike '%lyme%'
AND l.date BETWEEN '01-01-2013' AND '12-31-2015'
order by patient_id, lab_date, test_name, result_string;

CREATE TABLE lyme_agg_rpt_lyme_possible AS
SELECT distinct(patient_id) FROM lyme_agg_rpt_icds
UNION
SELECT distinct(patient_id) FROM lyme_agg_rpt_labs;

CREATE TABLE lyme_agg_rpt_output AS
SELECT count(DISTINCT(i.patient_id)) as doxy_1day_pats,
count(DISTINCT(c.patient_id)) AS lyme_confirmed_pats,
count(distinct(p.patient_id)) AS lyme_possible_pats
FROM lyme_agg_rpt_indexpats i
LEFT JOIN lyme_agg_rpt_lyme_confirmed c ON c.patient_id = i.patient_id
LEFT JOIN lyme_agg_rpt_lyme_possible p ON p.patient_id = i.patient_id;

select * from lyme_agg_rpt_output;