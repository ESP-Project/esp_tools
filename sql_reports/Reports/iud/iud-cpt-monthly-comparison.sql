
-- IUD ENCOUNTERS
WITH kre_iud_1 AS (
	SELECT patient_id, 
	date_trunc('month', date)::date as index_date
	FROM emr_laborder
	WHERE date BETWEEN '01-01-2015' and '02-28-2017'
	AND procedure_code in ('58300','58301', '11981', '11982', '11983')
	GROUP BY patient_id, index_date
	ORDER BY patient_id, index_date),

--IUD ENCOUNTERS WITH PATIENT DETAILS
kre_iud_2 AS (
	SELECT patient_id, 
	'cpt'::text as enc_type,
	index_date, 
	date_part('year', age(index_date, date_of_birth)) age,
	CASE   
	when date_part('year', age(index_date, date_of_birth)) <= 14 then '0-14'    
	when date_part('year', age(index_date, date_of_birth)) <= 15 then '15-19'  
	when date_part('year', age(index_date, date_of_birth)) <= 29 then '20-29' 
	when date_part('year', age(index_date, date_of_birth)) <= 39 then '30-39' 
	when date_part('year', age(index_date, date_of_birth)) <= 44 then '40-44'   
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '45-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 59 then '50-59' 
	when date_part('year', age(index_date, date_of_birth)) <= 69 then '60-69' 
	when date_part('year', age(index_date, date_of_birth)) <= 79 then '70-79'   
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>= 80' end age_group_10_yr,
	natural_key,
	sex,
	CASE when race_ethnicity = 6 then 'hispanic'  
	when race_ethnicity = 5 then 'white' 
	when race_ethnicity = 3 then 'black' 
	when race_ethnicity = 2 then 'asian' 
	when race_ethnicity = 1 then 'native_american' 
	when race_ethnicity = 0 then 'unknown' 
	end race
	FROM kre_iud_1 iud
	INNER JOIN public.emr_patient p ON (iud.patient_id = p.id )
	INNER JOIN esp_mdphnet.esp_demographic d on (p.natural_key = d.patid)
	WHERE date_part('year', age(index_date, date_of_birth)) BETWEEN 15 AND 44
	AND sex = 'F'),
	
-- ALL ENCOUNTERS
kre_iud_3 AS (
	SELECT patient_id, 
	'encounter'::text as enc_type,
	date_trunc('month', date)::date as index_date, 
	date_part('year', age(date_trunc('month', date)::date, date_of_birth)) age,
	CASE   
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 14 then '0-14'    
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 15 then '15-19'  
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 29 then '20-29' 
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 39 then '30-39' 
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 44 then '40-44'   
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 49 then '45-49'  
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 59 then '50-59' 
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 69 then '60-69' 
	when date_part('year', age(date_trunc('month', date)::date, date_of_birth)) <= 79 then '70-79'   
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>= 80' end age_group_10_yr,
	p.natural_key,
	sex,
	CASE when race_ethnicity = 6 then 'hispanic'  
	when race_ethnicity = 5 then 'white' 
	when race_ethnicity = 3 then 'black' 
	when race_ethnicity = 2 then 'asian' 
	when race_ethnicity = 1 then 'native_american' 
	when race_ethnicity = 0 then 'unknown' 
	end race
	FROM emr_encounter e
	INNER JOIN public.emr_patient p ON (e.patient_id = p.id )
	INNER JOIN esp_mdphnet.esp_demographic d on (p.natural_key = d.patid)
	WHERE date_part('year', age(date_trunc('month', date)::date, date_of_birth)) BETWEEN 15 AND 44
	AND sex = 'F'
	AND date BETWEEN '01-01-2015' and '02-28-2017'),
	
--UNION TOGETHER ALL ENCOUNTERS AND IUD ENCOUNTERS
kre_iud_4 AS (
	SELECT * FROM kre_iud_2
	UNION
	SELECT * FROM kre_iud_3)
-- COUNT AND PREPARE FINAL OUTPUT
SELECT
sex, age_group_10_yr, race,
count(case when  index_date = '2015-01-01' and enc_type = 'encounter' then 1 end)  jan_15_encs,
count(case when  index_date = '2015-01-01' and enc_type = 'iud' then 1 end)  jan_15_iud,
count(case when  index_date = '2015-02-01' and enc_type = 'encounter' then 1 end)  feb_15_encs,
count(case when  index_date = '2015-02-01' and enc_type = 'iud' then 1 end)  feb_15_iud,
count(case when  index_date = '2015-03-01' and enc_type = 'encounter' then 1 end)  mar_15_encs,
count(case when  index_date = '2015-03-01' and enc_type = 'iud' then 1 end)  mar_15_iud,
count(case when  index_date = '2015-04-01' and enc_type = 'encounter' then 1 end)  apr_15_encs,
count(case when  index_date = '2015-04-01' and enc_type = 'iud' then 1 end)  apr_15_iud,
count(case when  index_date = '2015-05-01' and enc_type = 'encounter' then 1 end)  may_15_encs,
count(case when  index_date = '2015-05-01' and enc_type = 'iud' then 1 end)  may_15_iud,
count(case when  index_date = '2015-06-01' and enc_type = 'encounter' then 1 end)  jun_15_encs,
count(case when  index_date = '2015-06-01' and enc_type = 'iud' then 1 end)  jun_15_iud,
count(case when  index_date = '2015-07-01' and enc_type = 'encounter' then 1 end)  jul_15_encs,
count(case when  index_date = '2015-07-01' and enc_type = 'iud' then 1 end)  jul_15_iud,
count(case when  index_date = '2015-08-01' and enc_type = 'encounter' then 1 end)  aug_15_encs,
count(case when  index_date = '2015-08-01' and enc_type = 'iud' then 1 end)  aug_15_iud,
count(case when  index_date = '2015-09-01' and enc_type = 'encounter' then 1 end)  sep_15_encs,
count(case when  index_date = '2015-09-01' and enc_type = 'iud' then 1 end)  sep_15_iud,
count(case when  index_date = '2015-10-01' and enc_type = 'encounter' then 1 end)  oct_15_encs,
count(case when  index_date = '2015-10-01' and enc_type = 'iud' then 1 end)  oct_15_iud,
count(case when  index_date = '2015-11-01' and enc_type = 'encounter' then 1 end)  nov_15_encs,
count(case when  index_date = '2015-11-01' and enc_type = 'iud' then 1 end)  nov_15_iud,
count(case when  index_date = '2015-12-01' and enc_type = 'encounter' then 1 end)  dec_15_encs,
count(case when  index_date = '2015-12-01' and enc_type = 'iud' then 1 end)  dec_15_iud,
count(case when  index_date = '2016-01-01' and enc_type = 'encounter' then 1 end)  jan_16_encs,
count(case when  index_date = '2016-01-01' and enc_type = 'iud' then 1 end)  jan_16_iud,
count(case when  index_date = '2016-02-01' and enc_type = 'encounter' then 1 end)  feb_16_encs,
count(case when  index_date = '2016-02-01' and enc_type = 'iud' then 1 end)  feb_16_iud,
count(case when  index_date = '2016-03-01' and enc_type = 'encounter' then 1 end)  mar_16_encs,
count(case when  index_date = '2016-03-01' and enc_type = 'iud' then 1 end)  mar_16_iud,
count(case when  index_date = '2016-04-01' and enc_type = 'encounter' then 1 end)  apr_16_encs,
count(case when  index_date = '2016-04-01' and enc_type = 'iud' then 1 end)  apr_16_iud,
count(case when  index_date = '2016-05-01' and enc_type = 'encounter' then 1 end)  may_16_encs,
count(case when  index_date = '2016-05-01' and enc_type = 'iud' then 1 end)  may_16_iud,
count(case when  index_date = '2016-06-01' and enc_type = 'encounter' then 1 end)  jun_16_encs,
count(case when  index_date = '2016-06-01' and enc_type = 'iud' then 1 end)  jun_16_iud,
count(case when  index_date = '2016-07-01' and enc_type = 'encounter' then 1 end)  jul_16_encs,
count(case when  index_date = '2016-07-01' and enc_type = 'iud' then 1 end)  jul_16_iud,
count(case when  index_date = '2016-08-01' and enc_type = 'encounter' then 1 end)  aug_16_encs,
count(case when  index_date = '2016-08-01' and enc_type = 'iud' then 1 end)  aug_16_iud,
count(case when  index_date = '2016-09-01' and enc_type = 'encounter' then 1 end)  sep_16_encs,
count(case when  index_date = '2016-09-01' and enc_type = 'iud' then 1 end)  sep_16_iud,
count(case when  index_date = '2016-10-01' and enc_type = 'encounter' then 1 end)  oct_16_encs,
count(case when  index_date = '2016-10-01' and enc_type = 'iud' then 1 end)  oct_16_iud,
count(case when  index_date = '2016-11-01' and enc_type = 'encounter' then 1 end)  nov_16_encs,
count(case when  index_date = '2016-11-01' and enc_type = 'iud' then 1 end)  nov_16_iud,
count(case when  index_date = '2016-12-01' and enc_type = 'encounter' then 1 end)  dec_16_encs,
count(case when  index_date = '2016-12-01' and enc_type = 'iud' then 1 end)  dec_16_iud,
count(case when  index_date = '2017-01-01' and enc_type = 'encounter' then 1 end)  jan_17_encs,
count(case when  index_date = '2017-01-01' and enc_type = 'iud' then 1 end)  jan_17_iud,
count(case when  index_date = '2017-02-01' and enc_type = 'encounter' then 1 end)  feb_17_encs,
count(case when  index_date = '2017-02-01' and enc_type = 'iud' then 1 end)  feb_17_iud
FROM kre_iud_4
GROUP BY sex, age_group_10_yr, race
ORDER by sex, age_group_10_yr, race;
