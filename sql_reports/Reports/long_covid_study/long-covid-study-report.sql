
-- -- SETUP STEPS
-- -- ONLY RUN ONCE

-- CREATE EXTENSION IF NOT EXISTS tablefunc;


-- -- Table: kre_report.lcrpt_symptoms_weights

-- -- DROP TABLE IF EXISTS kre_report.lcrpt_symptoms_weights;

-- CREATE TABLE IF NOT EXISTS kre_report.lcrpt_symptoms_weights
-- (
    -- dx_code character varying(25) COLLATE pg_catalog."default" NOT NULL,
    -- symptom character varying(50) COLLATE pg_catalog."default" NOT NULL,
	-- weight INTEGER NOT NULL,
    -- CONSTRAINT lcrpt_symptoms_weights_pk PRIMARY KEY (dx_code, symptom)
-- );

-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R43.0', 'SMELL/TASTE', 8);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R43.1', 'SMELL/TASTE', 8);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R43.2', 'SMELL/TASTE', 8);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R43.8', 'SMELL/TASTE', 8);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R43.9', 'SMELL/TASTE', 8);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R05', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R05.2', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R05.3', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R05.8', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R05.9', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:J45.991', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R06.00', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R06.02', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R06.09', 'CHRONIC COUGH', 4);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.840', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.89', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.3', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.0', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.9', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G31.84', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R29.90', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.841', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R41.844', 'BRAIN FOG', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R63.1', 'THIRST', 3);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R00.0', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R00.2', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47.1', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47.10', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47.11', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47.19', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:I47.9', 'PALPITATIONS', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.1', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.2', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.8', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.81', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.82', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.89', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R07.9', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G89.29', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G89.4', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R52', 'CHEST PAIN', 2);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G93.3', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G93.31', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G93.32', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G93.39', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R53.8', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R53.81', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R53.82', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R53.83', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R53.1', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:F51.01', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:F51.04', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.00', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.01', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.09', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.10', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.19', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G47.9', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R40.0', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R55', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd9:780.79', 'FATIGUE', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R68.82', 'SEXUAL DESIRE OR CAPACITY', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R42', 'DIZZINESS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:U09.9', 'LONG COVID', 0);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R25.9', 'ABNORMAL MOVEMENTS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G25.0', 'ABNORMAL MOVEMENTS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:G25.81', 'ABNORMAL MOVEMENTS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R25.1', 'ABNORMAL MOVEMENTS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R25.2', 'ABNORMAL MOVEMENTS', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R19.7', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R19.8', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R11.0', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R10.13', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R10.84', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K59.00', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R14.0', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K21.00', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K21.9', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K29.70', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K30', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K58.0', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K58.2', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K58.9', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K59.01', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K59.09', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:K90.49', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R10.31', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R10.32', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R10.9', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R11.2', 'GASTROINTESTINAL', 1);
-- INSERT INTO kre_report.lcrpt_symptoms_weights VALUES('icd10:R63.0', 'GASTROINTESTINAL', 1);



-- 2018 Control Line List
DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_pats;
CREATE TABLE kre_report.lcrpt_2018_control_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2018-01-01'
AND date < '2019-01-01'
AND source = 'enc'
GROUP BY patient_id;

-- 2018 Control Line List DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_dx;
CREATE TABLE kre_report.lcrpt_2018_control_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2018_control_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2018 Control Line List SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_scores;
CREATE TABLE kre_report.lcrpt_2018_control_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2018_control_pats T1
LEFT JOIN kre_report.lcrpt_2018_control_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 


DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2018_control_counts_unstrat AS
SELECT 
1 as table_num,
'2018 Control' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2018_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2018_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2018_control_pats T1
LEFT JOIN kre_report.lcrpt_2018_control_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2018_control_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2018_control_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2018_control_counts_unstrat;




DROP TABLE IF EXISTS kre_report.lcrpt_2018_control_counts_strat;
CREATE TABLE kre_report.lcrpt_2018_control_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group, 
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2018_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2018_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2018_control_pats T1
LEFT JOIN kre_report.lcrpt_2018_control_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2018_control_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;



-- 2021 No Prior Covid / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_pats;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--DROP PRIOR COVID PATIENTS
DELETE FROM kre_report.lcrpt_2021_no_covid_no_regard_pats
WHERE patient_id in (
SELECT T1.patient_id
--, T1.index_date, T2.date as hef_date
FROM kre_report.lcrpt_2021_no_covid_no_regard_pats T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
AND T2.date <= T1.index_date
ORDER BY patient_id);


-- 2021 No Prior Covid / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_dx;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_no_covid_no_regard_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 No Prior Covid / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_scores;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_no_covid_no_regard_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_covid_no_regard_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 


DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat AS
SELECT 
2 as table_num,
'2021 No Prior Covid/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_no_covid_no_regard_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_no_covid_no_regard_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_no_covid_no_regard_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_covid_no_regard_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_no_covid_no_regard_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat;


DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_covid_no_regard_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_no_covid_no_regard_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_no_covid_no_regard_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_no_covid_no_regard_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_no_covid_no_regard_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_covid_no_regard_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_no_covid_no_regard_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;


-- 2021 No Prior Covid / No COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--DROP PRIOR COVID PATIENTS
DELETE FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats
WHERE patient_id in (
SELECT T1.patient_id
--, T1.index_date, T2.date as hef_date, index_date - T2.date as datediff
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats T1
LEFT JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
AND (T2.date <= T1.index_date OR T2.date <= index_date + INTERVAL '365 days')
ORDER BY patient_id desc);

-- 2021 No Prior Covid / No COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 No Prior Covid / No COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_prior_or_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2021 No Prior Covid / No COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat AS
SELECT 
3 as table_num,
'2021 No Prior Covid/No COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_no_prior_or_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_no_prior_or_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_prior_or_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_no_prior_or_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat;

-- 2021 No Prior Covid / No COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_no_prior_or_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_no_prior_or_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_no_prior_or_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_no_prior_or_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
	SELECT T1.patient_id
	FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date);
	

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
4 as table_num,
'2021 With Prior COVID/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat;


-- 2021 With Prior COVID / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2021 Prior COVID 0-3 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date
	AND T2.date >= T1.index_date - INTERVAL '3 months'
	--ORDER BY datediff desc
	);
	
-- 2021 Prior COVID 0-3 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 Prior COVID 0-3 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2021 Prior COVID 0-3 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
5 as table_num,
'2021 Prior COVID 0-3 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat;


-- 2021 Prior COVID 0-3 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2021 Prior COVID 3-6 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '3 months'
	AND T2.date >= T1.index_date - INTERVAL '6 months'
	--ORDER BY datediff desc
	);

-- 2021 Prior COVID 3-6 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 Prior COVID 3-6 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2021 Prior COVID 3-6 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
6 as table_num,
'2021 Prior COVID 3-6 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat;


-- 2021 Prior COVID 3-6 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2021 Prior COVID 6-12 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 6-12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '6 months'
	AND T2.date >= T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);

--2021 Prior COVID 6-12 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2021 Prior COVID 6-12 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2021 Prior COVID 6-12 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
7 as table_num,
'2021 Prior COVID 6-12 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat;


--2021 Prior COVID 6-12 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2021 Prior COVID 1+ year / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN GREATER THAN 12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);
	
--2021 Prior COVID 1+ year / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2021 Prior COVID 1+ year / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2021 Prior COVID 1+ year / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
8 as table_num,
'2021 Prior COVID 1+ year/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat;


--2021 Prior COVID 1+ year / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2022 Prior COVID 0-3 months / No Regard to COVID During Follow-Up Line List 
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2022-01-01'
AND date < '2023-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date
	AND T2.date >= T1.index_date - INTERVAL '3 months'
	--ORDER BY datediff desc
	);
	
-- 2022 Prior COVID 0-3 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2022 Prior COVID 0-3 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2022 Prior COVID 0-3 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
9 as table_num,
'2022 Prior COVID 0-3 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat;


-- 2022 Prior COVID 0-3 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2022 Prior COVID 3-6 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2022-01-01'
AND date < '2023-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '3 months'
	AND T2.date >= T1.index_date - INTERVAL '6 months'
	--ORDER BY datediff desc
	);

-- 2022 Prior COVID 3-6 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2022 Prior COVID 3-6 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2022 Prior COVID 3-6 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
10 as table_num,
'2022 Prior COVID 3-6 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat;


-- 2022 Prior COVID 3-6 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2022 Prior COVID 6-12 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2022-01-01'
AND date < '2023-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 6-12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '6 months'
	AND T2.date >= T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);

--2022 Prior COVID 6-12 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2022 Prior COVID 6-12 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2022 Prior COVID 6-12 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 11 as table_num,
'2022 Prior COVID 6-12 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat;


--2022 Prior COVID 6-12 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2022 Prior COVID 1+ year / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2022-01-01'
AND date < '2023-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN GREATER THAN 12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);
	
--2022 Prior COVID 1+ year / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2022 Prior COVID 1+ year / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2022 Prior COVID 1+ year / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 12 as table_num,
'2022 Prior COVID 1+ year/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat;


--2022 Prior COVID 1+ year / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2023 Prior COVID 0-3 months / No Regard to COVID During Follow-Up Line List 
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2023-01-01'
AND date < '2024-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date
	AND T2.date >= T1.index_date - INTERVAL '3 months'
	--ORDER BY datediff desc
	);
	
-- 2023 Prior COVID 0-3 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2023 Prior COVID 0-3 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2023 Prior COVID 0-3 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
13 as table_num,
'2023 Prior COVID 0-3 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat;


-- 2023 Prior COVID 0-3 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2023 Prior COVID 3-6 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2023-01-01'
AND date < '2024-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 3 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '3 months'
	AND T2.date >= T1.index_date - INTERVAL '6 months'
	--ORDER BY datediff desc
	);

-- 2023 Prior COVID 3-6 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2023 Prior COVID 3-6 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2023 Prior COVID 3-6 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
14 as table_num,
'2023 Prior COVID 3-6 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat;


-- 2023 Prior COVID 3-6 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

--2023 Prior COVID 6-12 months / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2023-01-01'
AND date < '2024-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN 6-12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '6 months'
	AND T2.date >= T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);

--2023 Prior COVID 6-12 months / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2023 Prior COVID 6-12 months / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2023 Prior COVID 6-12 months / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 15 as table_num,
'2023 Prior COVID 6-12 months/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat;


--2023 Prior COVID 6-12 months / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;

-- 2023 Prior COVID 1+ year / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2023-01-01'
AND date < '2024-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID WITHIN GREATER THAN 12 MONTHS PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
    SELECT T1.patient_id
    --,index_date, T2.date, index_date - T2.date as datediff
	FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date - INTERVAL '12 months'
	--ORDER BY datediff desc
	);
	
--2023 Prior COVID 1+ year / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

--2023 Prior COVID 1+ year / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

--2023 Prior COVID 1+ year / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 16 as table_num,
'2023 Prior COVID 1+ year/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct AS
SELECT
table_num,
description,
denom,
smell_taste_pats,
round((smell_taste_pats::numeric/denom::numeric) * 100, 3) smell_taste_pct,
chronic_cough_pats,
round((chronic_cough_pats::numeric/denom::numeric) * 100, 3) chronic_cough_pct,
brain_fog_pats,
round((brain_fog_pats::numeric/denom::numeric) * 100, 3) brain_fog_pct,
palpitations_pats,
round((palpitations_pats::numeric/denom::numeric) * 100, 3) palpitations_pct,
chest_pain_pats,
round((chest_pain_pats::numeric/denom::numeric) * 100, 3) chest_pain_pct,
fatigue_pats,
round((fatigue_pats::numeric/denom::numeric) * 100, 3) fatigue_pct,
sex_desire_pats,
round((sex_desire_pats::numeric/denom::numeric) * 100, 3) sex_desire_pct,
dizziness_pats,
round((dizziness_pats::numeric/denom::numeric) * 100, 3) dizziness_pct,
thirst_pats,
round((thirst_pats::numeric/denom::numeric) * 100, 3) thirst_pct,
gi_pats,
round((gi_pats::numeric/denom::numeric) * 100, 3) gi_pct,
abn_mvmts_pats,
round((abn_mvmts_pats::numeric/denom::numeric) * 100, 3) abn_mvmts_pct,
long_covid_dx_pats,
round((long_covid_dx_pats::numeric/denom::numeric) * 100, 3) long_covid_dx_pct,
score_gte_12_pats,
round((score_gte_12_pats::numeric/denom::numeric) * 100, 3) score_gte_12_pct,
score_gte_12_or_long_covid_dx_pats,
round((score_gte_12_or_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_or_long_covid_dx_pct,
score_gte_12_and_long_covid_dx_pats,
round((score_gte_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_gte_12_and_long_covid_dx_pct,
score_lt_12_and_no_long_covid_dx_pats,
round((score_lt_12_and_no_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_no_long_covid_dx_pct,
score_lt_12_and_long_covid_dx_pats,
round((score_lt_12_and_long_covid_dx_pats::numeric/denom::numeric) * 100, 3) score_lt_12_and_long_covid_dx_pct,
score_0_to_2_pats,
round((score_0_to_2_pats::numeric/denom::numeric) * 100, 3) score_0_to_2_pct,
score_3_to_5_pats,
round((score_3_to_5_pats::numeric/denom::numeric) * 100, 3) score_3_to_5_pct,
score_6_to_8_pats,
round((score_6_to_8_pats::numeric/denom::numeric) * 100, 3) score_6_to_8_pct,
score_9_to_11_pats,
round((score_9_to_11_pats::numeric/denom::numeric) * 100, 3) score_9_to_11_pct,
score_12_to_14_pats,
round((score_12_to_14_pats::numeric/denom::numeric) * 100, 3) score_12_to_14_pct,
score_15_to_17_pats,
round((score_15_to_17_pats::numeric/denom::numeric) * 100, 3) score_15_to_17_pct,
score_18_to_20_pats,
round((score_18_to_20_pats::numeric/denom::numeric) * 100, 3) score_18_to_20_pct,
score_21_to_23_pats,
round((score_21_to_23_pats::numeric/denom::numeric) * 100, 3) score_21_to_23_pct,
score_24_to_26_pats,
round((score_24_to_26_pats::numeric/denom::numeric) * 100, 3) score_24_to_26_pct,
score_gte_27_pats,
round((score_gte_27_pats::numeric/denom::numeric) * 100, 3) score_gte_27_pct,
total_score_5th,
total_score_10th,
total_score_25th,
total_score_50th,
total_score_75th,
total_score_85th,
total_score_90th,
total_score_95th,
total_score_99th
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat;


--2023 Prior COVID 1+ year / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_strat AS
SELECT 
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 4 then '0-4'    
	when date_part('year', age(index_date, date_of_birth)) <= 24 then '5-24'  
	when date_part('year', age(index_date, date_of_birth)) <= 49 then '25-49'  
	when date_part('year', age(index_date, date_of_birth)) <= 64 then '50-64'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U', 'UNMAPPED', 'NOT MAPPING', 'IGNORE') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO', 'PACIFIC ISLANDER', 'NATIVE HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/REFUSED TO REPORT RACE', 'UNREPORTED/CHOOSE NOT TO DISCLOSE RACE') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL', 'MORE THAN ONE RACE') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN', 'HISPANIC, LATINO/A, OR SPANISH ORIGIN COMBINED', 'MEXICAN, MEXICAN AMERICAN, CHICANO/A', 'HISPANIC/LATINO', 'ANOTHER HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)', 'NON-HISPANIC/LATINO', 'NOT HISPANIC, LATINO/A, OR SPANISH ORIGIN') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED', 'UNREPORTED/CHOOSE NOT TO DISCLOSE ETHNICITY', 'UNREPORTED/REFUSED TO REPORT ETHNICITY') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)', 'IGNORE', 'UNMAPPED') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;


--CREATE ONE TABLE WITH ALL THE UNSTRATIFIED RESULTS
DROP TABLE IF EXISTS kre_report.lcrpt_unstrat_union;
CREATE TABLE kre_report.lcrpt_unstrat_union as 
SELECT * FROM kre_report.lcrpt_2018_control_counts_unstrat_pct
UNION 
SELECT * FROM kre_report.lcrpt_2021_no_covid_no_regard_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_no_prior_or_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2021_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2022_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2022_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2022_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2022_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2023_0_to_3_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2023_3_to_6_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2023_6_to_12_prior_no_regard_fu_covid_counts_unstrat_pct
UNION
SELECT * FROM kre_report.lcrpt_2023_gt_12_prior_no_regard_fu_covid_counts_unstrat_pct; 



select *
from kre_report.lcrpt_unstrat_union
order by table_num;

/* 
\COPY (select* from kre_report.lcrpt_unstrat_union order by table_num) TO '/tmp/ATR-2025-01-22-long-covid-study-unstrat.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR-2025-01-22-long-covid-study-unstrat.csv -s "ATR LONG COVID STUDY" 

\COPY (select* from kre_report.lcrpt_unstrat_union order by table_num) TO '/tmp/CHA-2025-01-22-long-covid-study-unstrat.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA-2025-01-22-long-covid-study-unstrat.csv -s "CHA LONG COVID STUDY"

\COPY (select* from kre_report.lcrpt_unstrat_union order by table_num) TO '/tmp/MLCHC-2025-01-22-long-covid-study-unstrat.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@commoninf.com -r keberhardt@commoninf.com -p /tmp -n MLCHC-2025-01-22-long-covid-study-unstrat.csv -s "MLCHC LONG COVID STUDY" 
*/


/* DROP TABLE IF EXISTS kre_report.long_covid_dx_freq_1;
CREATE TABLE kre_report.long_covid_dx_freq_1 AS
SELECT count(*) dx_count, dx_code_id, name,
COUNT(CASE WHEN dx_code_id = 'icd10:U09.9' THEN 1 END) as long_covid_dx_count
FROM (
    SELECT DISTINCT T1.patient_id, T2.date, T3.dx_code_id, T4.name
       FROM 
           (--All pats and dates with long covid dx
           select patient_id, date, dx_code_id
           from emr_encounter T1
           JOIN emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
           WHERE dx_code_id = 'icd10:U09.9') T1
    JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
    JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
    JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
    WHERE T3.dx_code_id not in ('icd9:799.9')
) FOO
GROUP BY dx_code_id, name
ORDER BY count(*) desc;

DROP TABLE IF EXISTS kre_report.long_covid_dx_freq_2;
CREATE TABLE kre_report.long_covid_dx_freq_2 AS
SELECT 
round((dx_count::numeric/total_long_covid_dx_count ::numeric) * 100, 3) dx_count_pct,
dx_count,
dx_code_id,
name
FROM 
(SELECT max(long_covid_dx_count) as total_long_covid_dx_count 
FROM kre_report.long_covid_dx_freq_1) T1
JOIN kre_report.long_covid_dx_freq_1 T2 ON (1=1);

DROP TABLE IF EXISTS kre_report.long_covid_dx_freq_output;
CREATE TABLE kre_report.long_covid_dx_freq_output AS
select T1.*, case when dx_code is not null then 1 else 0 end dx_in_study
from kre_report.long_covid_dx_freq_2 T1
LEFT JOIN kre_report.lcrpt_symptoms_weights T2 ON (T1.dx_code_id = T2.dx_code); */


/* 
\COPY (select* from kre_report.long_covid_dx_freq_output) TO '/tmp/ATR-2025-01-03-long-covid-same-day-dx-frequency.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR-2025-01-03-long-covid-same-day-dx-frequency.csv -s "ATR LONG COVID DX FREQ" 

\COPY (select* from kre_report.long_covid_dx_freq_output) TO '/tmp/CHA-2025-01-03-long-covid-same-day-dx-frequency.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA-2025-01-03-long-covid-same-day-dx-frequency.csv -s "CHA LONG DX FREQ"

\COPY (select* from kre_report.long_covid_dx_freq_output) TO '/tmp/MLCHC-2025-01-03-long-covid-same-day-dx-frequency.csv' WITH CSV HEADER;
/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@commoninf.com -r keberhardt@commoninf.com -p /tmp -n MLCHC-2025-01-03-long-covid-same-day-dx-frequency.csv -s "MLCHC LONG DX FREQ" 
*/

