-- 2021 Control Line List
DROP TABLE IF EXISTS kre_report.lcrpt_2021_control_pats;
CREATE TABLE kre_report.lcrpt_2021_control_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

-- 2021 Control Line List DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_control_dx;
CREATE TABLE kre_report.lcrpt_2021_control_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_control_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 Control Line List SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_control_scores;
CREATE TABLE kre_report.lcrpt_2021_control_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_control_pats T1
LEFT JOIN kre_report.lcrpt_2021_control_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

DROP TABLE IF EXISTS kre_report.lcrpt_2021_control_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_control_counts_unstrat AS
SELECT 
17 as table_num,
'2021 Control' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_control_pats T1
LEFT JOIN kre_report.lcrpt_2021_control_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_control_scores T3 ON (T1.patient_id = T3.patient_id);

DROP TABLE IF EXISTS kre_report.lcrpt_2021_control_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_control_counts_strat AS
SELECT 
'2021 Control' as description,
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 19 then '0-19'    
	when date_part('year', age(index_date, date_of_birth)) <= 39 then '20-39'  
	when date_part('year', age(index_date, date_of_birth)) <= 59 then '40-59'  
	when date_part('year', age(index_date, date_of_birth)) <= 79 then '60-79'
	when date_part('year', age(index_date, date_of_birth)) >= 80 then '>=80'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'UNKNOWN') or race is null then 'UNKNOWN'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'AMERICAN INDIAN/ALASKAN NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'BLACK') then 'BLACK' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'ASIAN') then 'ASIAN' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'PACIFIC ISLANDER/HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'CAUCASIAN') then 'WHITE' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'HISPANIC') then 'HISPANIC' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'DECLINED TO ANSWER') then 'DECLINED TO ANSWER'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'OTHER') then 'OTHER'
ELSE race END race_group,
CASE 
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity' and mapped_value = 'HISPANIC') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity' and mapped_value = 'NOT HISPANIC') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity_rpt_1' and mapped_value = 'DECLINED TO ANSWER') then 'DECLINED TO ANSWER'
	when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'HISPANIC') and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity_rpt_1' and mapped_value = 'UNKNOWN') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_control_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_control_pats T1
LEFT JOIN kre_report.lcrpt_2021_control_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_control_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;




--GENDER
SELECT max(description) description, sex, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_control_counts_strat
GROUP BY sex
ORDER BY sex;

--AGE GROUP
SELECT max(description) description, age_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_control_counts_strat
GROUP BY age_group
ORDER BY age_group;


--RACE GROUP
SELECT max(description) description, race_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_control_counts_strat
GROUP BY race_group
ORDER BY race_group;

--ETHNICITY
SELECT max(description) description, ethnicity_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_control_counts_strat
GROUP BY ethnicity_group
ORDER BY ethnicity_group;


-- 2021 With Prior COVID / No Regard to COVID During Follow-Up Line List
-- FIND ALL PATIENTS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats AS
SELECT patient_id, min(date) index_date
FROM gen_pop_tools.clin_enc
WHERE date >= '2021-01-01'
AND date < '2022-01-01'
AND source = 'enc'
GROUP BY patient_id;

--MUST HAVE COVID PRIOR TO INDEX
DELETE 
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats
WHERE patient_id not in (
	SELECT T1.patient_id
	FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.name in ('dx:covid19', 'lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
	AND T2.date < T1.index_date);
	

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up DX/SYMPTOMS
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx AS
--SELECT T1.patient_id, index_date, date as dx_date, dx_code_id, symptom, T4.weight, index_date - date as date_diff
SELECT DISTINCT T1.patient_id, symptom, T4.weight
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
JOIN kre_report.lcrpt_symptoms_weights T4 ON (T3.dx_code_id = T4.dx_code)
WHERE T2.date <= index_date + INTERVAL '365 days'
AND T2.date >= T1.index_date;

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up SCORES
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores AS
SELECT T1.patient_id, coalesce(sum(weight), 0) as total_score
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
GROUP BY T1.patient_id; 

-- 2021 With Prior COVID / No Regard to COVID During Follow-Up UNSTRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_unstrat AS
SELECT 
4 as table_num,
'2021 With Prior COVID/No Regard to COVID During Follow-Up' as description,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id);


-- 2021 With Prior COVID / No Regard to COVID During Follow-Up STRAT
DROP TABLE IF EXISTS kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat;
CREATE TABLE kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat AS
SELECT 
'2021 With Prior COVID/No Regard to COVID During Follow-Up' as description,
CASE
    when date_part('year', age(index_date, date_of_birth)) <= 19 then '0-19'    
	when date_part('year', age(index_date, date_of_birth)) <= 39 then '20-39'  
	when date_part('year', age(index_date, date_of_birth)) <= 59 then '40-59'  
	when date_part('year', age(index_date, date_of_birth)) <= 79 then '60-79'
	when date_part('year', age(index_date, date_of_birth)) >= 80 then '>=80'
	when date_of_birth is null then 'UNKNOWN AGE'
	else '>=65' 
	END age_group, 
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,	
CASE
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'UNKNOWN') or race is null then 'UNKNOWN'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'AMERICAN INDIAN/ALASKAN NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'BLACK') then 'BLACK' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'ASIAN') then 'ASIAN' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'PACIFIC ISLANDER/HAWAIIAN') then 'PACIFIC ISLANDER/HAWAIIAN'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'CAUCASIAN') then 'WHITE' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'HISPANIC') then 'HISPANIC' 
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'DECLINED TO ANSWER') then 'DECLINED TO ANSWER'
     when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race_rpt_1' and mapped_value = 'OTHER') then 'OTHER'
ELSE race END race_group,
CASE 
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity' and mapped_value = 'HISPANIC') then 'HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity' and mapped_value = 'NOT HISPANIC') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity_rpt_1' and mapped_value = 'DECLINED TO ANSWER') then 'DECLINED TO ANSWER'
	when upper(race) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='race' and mapped_value = 'HISPANIC') and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in (SELECT src_value from gen_pop_tools.rs_conf_mapping where src_table = 'emr_patient' and src_field='ethnicity_rpt_1' and mapped_value = 'UNKNOWN') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
COUNT(DISTINCT(T1.patient_id)) as denom,
COUNT(DISTINCT(CASE WHEN symptom = 'SMELL/TASTE' then T2.patient_id END)) as smell_taste_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHRONIC COUGH' then T2.patient_id END)) as chronic_cough_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'BRAIN FOG' then T2.patient_id END)) as brain_fog_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'PALPITATIONS' then T2.patient_id END)) as palpitations_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'CHEST PAIN' then T2.patient_id END)) as chest_pain_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'FATIGUE' then T2.patient_id END)) as fatigue_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'SEXUAL DESIRE OR CAPACITY' then T2.patient_id END)) as sex_desire_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'DIZZINESS' then T2.patient_id END)) as dizziness_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'THIRST' then T2.patient_id END)) as thirst_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'GASTROINTESTINAL' then T2.patient_id END)) as gi_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'ABNORMAL MOVEMENTS' then T2.patient_id END)) as abn_mvmts_pats,
COUNT(DISTINCT(CASE WHEN symptom = 'LONG COVID' then T2.patient_id END)) as long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 THEN T3.patient_id END)) as score_gte_12_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 OR symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_or_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND symptom = 'LONG COVID' THEN T1.patient_id END)) as score_gte_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id not in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_no_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score < 12 AND T1.patient_id in (select patient_id from kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx where symptom = 'LONG COVID') THEN T1.patient_id END)) as score_lt_12_and_long_covid_dx_pats,
COUNT(DISTINCT(CASE WHEN total_score <= 2 THEN T3.patient_id END)) as score_0_to_2_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 3 AND total_score <= 5 THEN T3.patient_id END)) as score_3_to_5_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 6 AND total_score <= 8 THEN T3.patient_id END)) as score_6_to_8_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 9 AND total_score <= 11 THEN T3.patient_id END)) as score_9_to_11_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 12 AND total_score <= 14 THEN T3.patient_id END)) as score_12_to_14_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 15 AND total_score <= 17 THEN T3.patient_id END)) as score_15_to_17_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 18 AND total_score <= 20 THEN T3.patient_id END)) as score_18_to_20_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 21 AND total_score <= 23 THEN T3.patient_id END)) as score_21_to_23_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 24 AND total_score <= 26 THEN T3.patient_id END)) as score_24_to_26_pats,
COUNT(DISTINCT(CASE WHEN total_score >= 27 THEN T3.patient_id END)) as score_gte_27_pats,
PERCENTILE_CONT(0.05) WITHIN GROUP(ORDER BY total_score)::numeric total_score_5th,
PERCENTILE_CONT(0.10) WITHIN GROUP(ORDER BY total_score)::numeric total_score_10th,
PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY total_score)::numeric total_score_25th,
PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY total_score)::numeric total_score_50th,
PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY total_score)::numeric total_score_75th,
PERCENTILE_CONT(0.85) WITHIN GROUP(ORDER BY total_score)::numeric total_score_85th,
PERCENTILE_CONT(0.90) WITHIN GROUP(ORDER BY total_score)::numeric total_score_90th,
PERCENTILE_CONT(0.95) WITHIN GROUP(ORDER BY total_score)::numeric total_score_95th,
PERCENTILE_CONT(0.99) WITHIN GROUP(ORDER BY total_score)::numeric total_score_99th
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_pats T1
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_dx T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_scores T3 ON (T1.patient_id = T3.patient_id)
JOIN emr_patient T4 ON (T1.patient_id = T4.id)
GROUP BY age_group, sex, race_group, ethnicity_group;


--GENDER
SELECT max(description) description, sex, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat
GROUP BY sex
ORDER BY sex;

--AGE GROUP
SELECT max(description) description, age_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat
GROUP BY age_group
ORDER BY age_group;


--RACE GROUP
SELECT max(description) description, race_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat
GROUP BY race_group
ORDER BY race_group;

--ETHNICITY
SELECT max(description) description, ethnicity_group, sum(denom) denom, sum(score_gte_12_pats) score_gte_12_pats, sum(long_covid_dx_pats) long_covid_dx_pats, sum(score_gte_12_or_long_covid_dx_pats) score_gte_12_or_long_covid_dx_pats
FROM kre_report.lcrpt_2021_yes_prior_no_regard_fu_covid_counts_strat
GROUP BY ethnicity_group
ORDER BY ethnicity_group;
