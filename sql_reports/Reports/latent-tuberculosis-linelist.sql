﻿-- We’re starting work on developing a new algorithm for latent tuberculosis as per our plan of work from BIDLS for the coming year.
-- I’d like to dive into some Atrius charts to understand the patterns of coding, lab tests, and prescribing around these patients.
-- To help me with this, can you generate a linelist of all Atrius patients with any of the following:

-- ICD9 795.5* or ICD10 R76.1*
-- Prescription for isoniazid, rifampin, or rifapentine
-- 
-- Please include MRN, last name, DOB, which of the preceding codes they have, the date the code first appeared, a yes/no variable for each of the three meds (isoniazid, rifampin, rifapentine), three fields with the dates 
-- of first prescription for each of the three meds (i.e. three fields, one date for each med), three fields with the dates of last prescription for each of the three meds, 
-- and finally if they ever met the ESP definition for active TB, and if so, when.  
-- 
-- I’m also interested in seeing the imputed duration for each of the three meds based on administration frequency and number of pills prescribed, refills, and/or end date but since I know this is a more complicated ask it’s fine 
-- to defer this for now in the interest of getting the preceding list sooner.
-- 
-- And then I’d like to explore whether / how interferon-gamma-release-assays (IGRAs) are coded in Atrius.  Possible test names to search for include:
-- Quantiferon
-- TB
-- IGRA
-- Interferon
-- 
-- If you can generate a list of all lab tests that include any of these terms then I can review to mark which are potentially pertinent.
-- 
-- Many thanks in advance!

-- 2.  Can you please add fields for Quantiferon date and result.  Use the component labeled, “QUANTIFERON TB GOLD” qualitative result (Positive, Negative, Indeterminate).  If a qualitative result is not available then define the test as positive if the QUANTIFERON TB MINUS NL component value is >=0.35 IU/mL.


DROP TABLE IF EXISTS lat_tuber_icd;
DROP TABLE IF EXISTS lat_tuber_meds_all;
DROP TABLE IF EXISTS lat_tuber_meds; 
DROP TABLE IF EXISTS lat_tuber_indexpats;
DROP TABLE IF EXISTS lat_tuber_cases;
DROP TABLE IF EXISTS lat_tuber_labs_gold;
DROP TABLE IF EXISTS lat_tuber_labs_tbminusnl;
DROP TABLE IF EXISTS lat_tuber_labs_result_interps;
DROP TABLE IF EXISTS lat_tuber_lab_results;
DROP TABLE IF EXISTS lat_tuber_output;

CREATE TABLE lat_tuber_icd AS
select e.patient_id, first_icd_date, array_agg(dx_code_id) AS dx_codes
from emr_encounter e, emr_encounter_dx_codes d, 
	(SELECT patient_id, 
	min(date) as first_icd_date
	FROM emr_encounter e, emr_encounter_dx_codes d
	WHERE e.id = d.encounter_id
	AND (dx_code_id ILIKE 'icd9:795.5%' or dx_code_id ilike 'icd10:R76.1%')
	GROUP BY patient_id) T3
WHERE e.id = d.encounter_id
AND (dx_code_id ILIKE 'icd9:795.5%' or dx_code_id ilike 'icd10:R76.1%')
AND e.date = T3.first_icd_date
AND e.patient_id = T3.patient_id
GROUP BY e.patient_id, first_icd_date
ORDER BY e.patient_id;


CREATE TABLE lat_tuber_meds_all AS
select h.patient_id, h.name hef_name, rx.name rx_name, h.date as med_date, quantity_float, refills, 
case WHEN refills~E'^\\d+$' THEN refills::real + 1 ELSE 1 END as rx_num, 
start_date, 
end_date, 
CASE WHEN (end_date - start_date) > 0 THEN end_date 
	WHEN ((end_date - start_date) < 0 or end_date is null) and refills~E'^\\d+$' THEN (start_date + (30*(refills::real + 1))::int)
	ELSE start_date + 30 END as computed_end_date, 
CASE WHEN (end_date - start_date) > 0 THEN end_date - start_date  
	WHEN refills~E'^\\d+$' THEN (30*(refills::real + 1)) 
	ELSE 30 END AS duration,
directions
FROM hef_event h,
emr_prescription rx
where h.name in ('rx:rifapentine', 'rx:isoniazid', 'rx:rifampin')
AND h.object_id = rx.id
AND h.patient_id = rx.patient_id
AND rx.name not ilike '%INHL' 
AND rx.name NOT ILIKE 'PEAK FLOW%'
ORDER BY patient_id;

CREATE TABLE lat_tuber_meds AS
SELECT patient_id, hef_name as name, sum(rx_num) total_rx_num, sum(quantity_float * rx_num)total_quantity, sum(duration)total_days, min(med_date) as first_med_date, max(med_date) as last_med_date,
array_agg(distinct(directions)) directions
FROM lat_tuber_meds_all
GROUP BY patient_id, hef_name;


CREATE TABLE lat_tuber_indexpats AS
select patient_id FROM lat_tuber_icd icd
UNION 
select patient_id FROM lat_tuber_meds
ORDER BY patient_id;

CREATE TABLE lat_tuber_cases AS
select patient_id, min(date) esp_case_date from nodis_case where condition = 'tuberculosis'
group by patient_id;

-- QUANTIFERON TB GOLD RESULTS
CREATE TABLE lat_tuber_labs_gold AS
SELECT labs.patient_id, native_name, native_code, result_string, date, result_float, 
CASE WHEN result_string in ('Negative', 'NEGATIVE', 'NOT DETECTED') THEn 'NEGATIVE'
WHEN result_string IN ('Positive', 'POSITIVE', 'DETECTED', '>10.00', '> 10.00') then 'POSITIVE'
WHEN result_string IN ('Indeterminate', 'INDETERMINATE') THEN 'INDETERMINATE'
WHEN result_float >= 0.35 then 'POSITIVE'
WHEN result_float <  0.35 THEN 'NEGATIVE'
ELSE NULL END result_interp
FROM emr_labresult labs,
lat_tuber_indexpats indexpats
WHERE labs.patient_id = indexpats.patient_id
AND native_code in (
--QUANTIFERON(R) - TB GOLD
'86480--7556',
'MR0001--7556',
-- QUANTIFERON TB GOLD
'86480--7936',
'MR0001--7936')
AND result_string not ilike '%test not performed%'
AND result_string NOT ILIKE '%TNP%'
order by patient_id;

-- USE QUANTIFERON TB MINUS NL in the absence of a TB GOLD result
CREATE TABLE lat_tuber_labs_tbminusnl AS
SELECT ltl.patient_id, l.native_name, l.native_code, l.result_string, ltl.date, l.result_float, ltl.result_interp gold_result,
CASE WHEN l.result_float > 0.35 then 'POSITIVE'
WHEN l.result_float <= 0.35 THEN 'NEGATIVE' 
WHEN l.result_string IN ('>10.00') THEN 'POSITIVE'
ELSE NULL END  result_interp
from lat_tuber_labs_gold ltl,
emr_labresult l
WHERE l.patient_id = ltl.patient_id
AND result_interp IS NULL
AND l.date = ltl.date
AND l.native_code =  '86480--10062';

-- UNIFIED TABLE OF RESULTS INTERPS
CREATE TABLE lat_tuber_labs_result_interps AS 
select g.patient_id, g.date, coalesce(g.result_interp, m.result_interp) result_interp
FROM lat_tuber_labs_gold g
LEFT JOIN lat_tuber_labs_tbminusnl m ON g.patient_id = m.patient_id
and g.date = m.date
order by patient_id;


CREATE TABLE lat_tuber_lab_results AS
SELECT labs.patient_id, first_lab.min_date first_quantif_date, 
MAX(result_interp) first_quantif_result_interp
FROM lat_tuber_labs_result_interps labs,
(SELECT patient_id, min(date) min_date from lat_tuber_labs_gold group by patient_id) first_lab
WHERE labs.patient_id = first_lab.patient_id
AND labs.date = first_lab.min_date
GROUP BY labs.patient_id, min_date
ORDER BY labs.patient_id;


CREATE TABLE lat_tuber_output AS
SELECT i.patient_id as esp_patient_id,
p.mrn,
p.last_name,
p.date_of_birth::date,
COALESCE (icd.dx_codes, NULL) as dx_code,
COALESCE (icd.first_icd_date, NULL) as first_icd_date,
max(case when med.name = 'rx:rifapentine' then 'Y' else 'N' END) as rifapentine_yn,
max(case when med.name = 'rx:rifapentine' THEN first_med_date ELSE NULL END) as rifapentine_first,
max(case when med.name = 'rx:rifapentine' then last_med_date ELSE NULL END) as rifapentine_last,
max(case when med.name = 'rx:rifapentine' then total_rx_num ELSE NULL END) as rifapentine_rx_num,
max(case when med.name = 'rx:rifapentine' then total_quantity ELSE NULL END) as rifapentine_total_quantity,
max(case when med.name = 'rx:rifapentine' then total_days ELSE NULL END) as rifapentine_total_days,
max(case when med.name = 'rx:rifapentine' then directions ELSE NULL END) as rifapentine_directions,
max(case when med.name = 'rx:isoniazid' then 'Y' else 'N' END) as isoniazid_yn,
max(case when med.name = 'rx:isoniazid' THEN first_med_date ELSE NULL END) as isoniazid_first,
max(case when med.name = 'rx:isoniazid' then last_med_date ELSE NULL END) as isoniazid_last,
max(case when med.name = 'rx:isoniazid' then total_rx_num ELSE NULL END) as isoniazid_rx_num,
max(case when med.name = 'rx:isoniazid' then total_quantity ELSE NULL END) as isoniazid_total_quantity,
max(case when med.name = 'rx:isoniazid' then total_days ELSE NULL END) as isoniazid_total_days,
max(case when med.name = 'rx:isoniazid' then directions ELSE NULL END) as isoniazid_directions,
max(case when med.name = 'rx:rifampin' then 'Y' else 'N' END) as rifampin_yn,
max(case when med.name = 'rx:rifampin' THEN first_med_date ELSE NULL END) as rifampin_first,
max(case when med.name = 'rx:rifampin' then last_med_date ELSE NULL END) as rifampin_last,
max(case when med.name = 'rx:rifampin' then total_rx_num ELSE NULL END) as rifampin_rx_num,
max(case when med.name = 'rx:rifampin' then total_quantity ELSE NULL END) as rifampin_total_quantity,
max(case when med.name = 'rx:rifampin' then total_days ELSE NULL END) as rifampin_total_days,
max(case when med.name = 'rx:rifampin'  then directions ELSE NULL END) as rifampin_directions,
max(CASE WHEN cases.esp_case_date IS NOT NULL THEN 'Y' else 'N' END) as esp_case_yn,
COALESCE (cases.esp_case_date, NULL) AS esp_case_date, 
min(date) as first_quantif_date,
max(first_quantif_result_interp) as first_quantif_result_interp,
0 dx,
0 meds,
0 dx_alone,
0 meds_alone,
0 dx_and_inh,
rand_order,
random,
(esp_case_date - first_icd_date) as active_case_date_minus_first_icd_date,
latent,
active,
neither,
ppd,
tb_spot,
cxr,
rx_for_latent,
rx_ques_unk,
comments
FROM lat_tuber_indexpats i
LEFT JOIN lat_tuber_icd icd ON i.patient_id = icd.patient_id
LEFT JOIN lat_tuber_meds med ON i.patient_id = med.patient_id
LEFT JOIN lat_tuber_cases cases ON i.patient_id = cases.patient_id
LEFT JOIN emr_patient p ON i.patient_id = p.id
LEFT JOIN lat_tuber_labs_all labs ON i.patient_id = labs.patient_id
LEFT JOIN lat_tuber_lab_results results ON i.patient_id = results.patient_id
LEFT JOIN lat_tuber_mk_notes mk ON i.patient_id = mk.esp_patient_id
GROUP BY i.patient_id, p.mrn, p.last_name, p.date_of_birth, icd.dx_codes, icd.first_icd_date, cases.esp_case_date,
dx, meds, dx_alone, meds_alone, dx_and_inh, rand_order, random, active_case_date_minus_first_icd_date, latent,
active, neither, ppd, tb_spot, cxr, rx_for_latent, rx_ques_unk, comments
order by esp_patient_id;

-- Update values for Mike

UPDATE lat_tuber_output SET dx = Case when dx_code IS NOT NULL then 1 ELSE 0 END;
UPDATE lat_tuber_output SET meds = case when rifapentine_yn = 'Y' or isoniazid_yn = 'Y' or rifampin_yn = 'Y' then 1 ELSE 0 END;
UPDATE lat_tuber_output SET dx_alone = case when dx_code is not null and rifapentine_yn = 'N' and isoniazid_yn = 'N' and rifampin_yn = 'N' then 1 ELSE 0 END;
UPDATE lat_tuber_output SET meds_alone = case when dx_code is null and (rifapentine_yn = 'Y' or isoniazid_yn = 'Y' or rifampin_yn = 'Y') then 1 ELSE 0 END;
UPDATE lat_tuber_output SET dx_and_inh = case when dx_code is not null and isoniazid_yn = 'Y' then 1 ELSE 0 END;


