 -- IDENTIFY INDEX PAIENTS, INDEX DATES, and FOLLOW UP START DATE
WITH index_pats AS (
    --DROP TABLE IF EXISTS kre_report.lc_index_pats;
    --CREATE TABLE kre_report.lc_index_pats AS
	SELECT patient_id, min(min_qual_date) as index_date, (min(min_qual_date) + INTERVAL '28 days')::date as follow_start_date,
	(min(min_qual_date) + INTERVAL '112 days')::date as follow_3mo_end_date, (min(min_qual_date) + INTERVAL '196 days')::date as follow_6mo_end_date
	      -- FIRST POS COVID TEST IN DATE RANGE
	FROM (SELECT patient_id, min(date) as min_qual_date
		  FROM public.hef_event
		  WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
		  AND date >= '2020-10-01'
		  AND date < '2021-03-01'
		  GROUP BY patient_id
		  UNION
		  -- FIRST POS COVID ICD IN DATE RANGE
		  SELECT patient_id, min(date) as min_qual_date
		  FROM public.emr_encounter T1
		  INNER JOIN public.emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
		  WHERE dx_code_id in ('icd10:Z86.16', 'icd10:U07.1')
		  AND date >= '2020-10-01'
		  AND date < '2021-03-01'
		  GROUP BY patient_id) min_dates
	GROUP by patient_id),
	
-- IDENTIFY POSITIVE PATS WITHIN 3 MONTHS OF FOLLOW-UP
pat_3mo_exclusions AS (
    --DROP TABLE IF EXISTS kre_report.lc_pat_3mo_exclusions;
    --CREATE TABLE kre_report.lc_pat_3mo_exclusions AS
	SELECT patient_id, max(exclude_3mo) exclude_from_3mo
	FROM (
		SELECT T1.patient_id, 1 as exclude_3mo
		FROM index_pats T1
		--FROM kre_report.lc_index_pats T1
		INNER JOIN public.hef_event T2 ON (T1.patient_id = T2.patient_id)
		WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
		AND date >= follow_start_date 
		AND date <= follow_start_date + INTERVAL '84 days'
		UNION
		SELECT T1.patient_id, 1 as exclude_3mo
		FROM index_pats T1
		--FROM kre_report.lc_index_pats T1
		INNER JOIN public.emr_encounter T2 ON (T1.patient_id = T2.patient_id)
		INNER JOIN public.emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
		WHERE dx_code_id in ('icd10:Z86.16', 'icd10:U07.1')
		AND date >= follow_start_date 
		AND date <= follow_start_date + INTERVAL '84 days') excl_3_months
	GROUP BY patient_id
	),
	
-- IDENTIFY POSITIVE PATS WITHIN 6 MONTHS OF FOLLOW-UP
pat_6mo_exclusions AS (
    --DROP TABLE IF EXISTS kre_report.lc_pat_6mo_exclusions;
    --CREATE TABLE kre_report.lc_pat_6mo_exclusions AS
	SELECT patient_id, max(exclude_6mo) exclude_from_6mo
	FROM (
		SELECT T1.patient_id, 1 as exclude_6mo
		FROM index_pats T1
		--FROM kre_report.lc_index_pats T1
		INNER JOIN public.hef_event T2 ON (T1.patient_id = T2.patient_id)
		WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
		AND date > follow_start_date + INTERVAL '84 days'
		AND date <= follow_start_date + INTERVAL '168 days'
		UNION
		SELECT T1.patient_id, 1 as exclude_6mo
		FROM index_pats T1
		--FROM kre_report.lc_index_pats T1
		INNER JOIN public.emr_encounter T2 ON (T1.patient_id = T2.patient_id)
		INNER JOIN public.emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
		WHERE dx_code_id in ('icd10:Z86.16', 'icd10:U07.1')
		AND date > follow_start_date + INTERVAL '84 days'
		AND date <= follow_start_date + INTERVAL '168 days') excl_6_months
	GROUP BY patient_id
	),

all_icd_codes AS (
    --DROP TABLE IF EXISTS kre_report.lc_all_icd_codes;
    --CREATE TABLE kre_report.lc_all_icd_codes AS
	SELECT DISTINCT T1.patient_id, T2.date as icd_date, dx_code_id
	FROM index_pats T1
	--FROM kre_report.lc_index_pats T1
	INNER JOIN public.emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	INNER JOIN public.emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
	WHERE 
	T2.date >= follow_start_date
	AND T2.date <= follow_6mo_end_date
	AND (
	       dx_code_id in (
		   'icd10:G93.3',
		   'icd10:R50.9',
		   'icd10:R09.02',
		   'icd10:R09.82',
		   'icd10:R07.89',
		   'icd10:R00.2',
		   'icd10:R41.840',
		   'icd10:R51.9',
		   'icd10:R43.0',
		   'icd10:R43.1',
		   'icd10:R43.8',
		   'icd10:R43.9',
		   'icd10:R43.2',
		   'icd10:R19.7',
		   'icd10:R10.9'
		   'icd10:R10.84',
		   'icd10:R25.2',
		   'icd10:R07.81',
		   'icd10:M25.5',
		   'icd10:R07.0',
		   'icd10:R07.9',
		   'icd10:R00.0',
		   'icd10:R41.89',
		   'icd10:R41.3',
		   'icd10:M54.2',
		   'icd10:M54.50',
		   'icd10:M54.59',
		   'icd10:M54.6',
		   'icd10:M54.9',
		   'icd10:G89.29',
		   'icd10:G89.4',
		   'icd10:R52',
		   'icd10:G64',
		   'icd10:R29.90')
		 OR dx_code_id ilike 'icd10:R53.8%'
		 OR dx_code_id ilike 'icd10:R53.1%'
		 OR dx_code_id ilike 'icd10:R06.0%'
		 OR dx_code_id ilike 'icd10:R05%'
		 OR dx_code_id ilike 'icd10:R10.1%'
		 OR dx_code_id ilike 'icd10:R10.3%'
		 OR dx_code_id ilike 'icd10:M25.5%'
		 OR dx_code_id ilike 'icd10:M79.1%'
		 OR dx_code_id ilike 'icd10:M62.83%'
		 OR dx_code_id ilike 'icd10:R11%'
		 OR dx_code_id ilike 'icd10:I47%'
		 OR dx_code_id ilike 'icd10:M54.8%'
		 OR dx_code_id ilike 'icd10:M79.6%'
		 OR dx_code_id ilike 'icd10:R20%'
	   ) 
	),
	
all_fever_encs AS (
	--DROP TABLE IF EXISTS kre_report.lc_all_fever_encs;
    --CREATE TABLE kre_report.lc_all_fever_encs AS
	SELECT DISTINCT T1.patient_id, T2.date as icd_date, 'icd10:R50.9'::text as dx_code_id
	FROM index_pats T1
	--FROM kre_report.lc_index_pats T1
	INNER JOIN public.hef_event T2 ON (T1.patient_id = T2.patient_id and name = 'enc:fever')
	WHERE T2.date >= follow_start_date
	AND T2.date <= follow_6mo_end_date
	),
	
all_icd_and_fever AS (
    --DROP TABLE IF EXISTS kre_report.lc_all_icd_and_fever;
    --CREATE TABLE kre_report.lc_all_icd_and_fever AS
	SELECT patient_id, icd_date, dx_code_id
	--FROM kre_report.lc_all_icd_codes
	FROM all_icd_codes
	UNION
	SELECT patient_id, icd_date, dx_code_id
	--FROM kre_report.lc_all_fever_encs
	FROM all_fever_encs
),

symptom_by_pat AS (
	--DROP TABLE IF EXISTS kre_report.lc_col_creation;
	--CREATE TABLE kre_report.lc_col_creation AS
	SELECT T1.patient_id,
	--exclude_from_3mo,
	--exclude_from_6mo,
	--age(index_date, date_of_birth) as age_on_index_date,
	index_date,
	CASE   
		when date_part('year', age(index_date, date_of_birth)) <= 9 then '0-9'    
		when date_part('year', age(index_date, date_of_birth)) <= 19 then '10-19'  
		when date_part('year', age(index_date, date_of_birth)) <= 29 then '20-29' 
		when date_part('year', age(index_date, date_of_birth)) <= 39 then '30-39' 
		when date_part('year', age(index_date, date_of_birth)) <= 49 then '40-49'   
		when date_part('year', age(index_date, date_of_birth)) <= 59 then '50-59'  
		when date_part('year', age(index_date, date_of_birth)) <= 69 then '60-69' 
		when date_part('year', age(index_date, date_of_birth)) <= 79 then '70-79' 
		when date_of_birth is null then 'UNKNOWN AGE'
		else '>= 80' 
		END age_group_10_yr,
	CASE 
	    WHEN sex is null then 'U'
		WHEN sex in ('M', 'MALE') then 'M'
		WHEN sex in ('F', 'FEMALE') then 'F'
		ELSE sex
		END sex,
	CASE 
		when race_ethnicity = 6 then 'hispanic'  
		when race_ethnicity = 5 then 'white' 
		when race_ethnicity = 3 then 'black' 
		when race_ethnicity = 2 then 'asian' 
		when race_ethnicity = 1 then 'native_american' 
		when race_ethnicity = 0 then 'unknown' 
		when race_ethnicity is null then 'unknown'
	END race,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null 
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
			 OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
			 OR dx_code_id ilike 'icd10:R53.1%') 
		then 1 else 0 END) as fatigue_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id = 'icd10:R50.9' then 1 else 0 END) as fever_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id = 'icd10:R50.9' then 1 else 0 END) as fever_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82') 
		then 1 else 0 END) as cough_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and dx_code_id in ('icd10:R07.89', 'icd10:R07.9')
		then 1 else 0 END) as chest_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and dx_code_id in ('icd10:R07.89', 'icd10:R07.9') 
		then 1 else 0 END) as chest_pain_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null 
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null 
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null 
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9') 
		then 1 else 0 END) as changes_in_taste_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null 
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9')  
		then 1 else 0 END) as changes_in_taste_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id = 'icd10:R19.7' then 1 else 0 END) as diarrhea_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id = 'icd10:R19.7' then 1 else 0 END) as diarrhea_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id ilike 'icd10:M25.5%' then 1 else 0 END) as joint_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id ilike 'icd10:M25.5%' then 1 else 0 END) as joint_pain_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2', 'icd10:R07.81') )
		then 1 else 0 END) as muscle_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2', 'icd10:R07.81') )
		then 1 else 0 END) as muscle_pain_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null and dx_code_id ilike 'icd10:R11%' then 1 else 0 END) as nausea_or_vomit_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null and dx_code_id ilike 'icd10:R11%' then 1 else 0 END) as nausea_or_vomit_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_6mo,
	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_6mo,	

	MAX(CASE WHEN icd_date >= follow_start_date and icd_date <= follow_3mo_end_date and exclude_from_3mo is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_3mo,
	MAX(CASE WHEN icd_date > follow_3mo_end_date and icd_date <= follow_6mo_end_date and exclude_from_6mo is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_6mo
	FROM index_pats T1
	--FROM kre_report.lc_index_pats T1
	LEFT JOIN pat_3mo_exclusions T2 ON (T1.patient_id = T2.patient_id)
	--LEFT JOIN kre_report.lc_pat_3mo_exclusions T2 ON (T1.patient_id = T2.patient_id)
	LEFT JOIN pat_6mo_exclusions T3 ON (T1.patient_id = T3.patient_id)
	--LEFT JOIN kre_report.lc_pat_6mo_exclusions T3 ON (T1.patient_id = T3.patient_id)
	LEFT JOIN all_icd_and_fever T4 ON (T1.patient_id = T4.patient_id)
	--LEFT JOIN kre_report.lc_all_icd_and_fever T4 ON (T1.patient_id = T4.patient_id)
	LEFT JOIN public.emr_patient T5 ON (T1.patient_id = T5.id )
	LEFT JOIN esp_mdphnet.esp_demographic T6 on (T5.natural_key = T6.patid)
	GROUP BY T1.patient_id, age_group_10_yr, sex, race_ethnicity, index_date --, exclude_from_3mo,exclude_from_6mo, age_on_index_date
),

long_covid_output AS (
	--DROP TABLE IF EXISTS kre_report.lc_output;
	--CREATE TABLE kre_report.lc_output AS
	select age_group_10_yr,
	sex, 
	race,
	count(distinct(patient_id)) total_patients,
	sum(fatigue_3mo) fatigue_3mo,
	sum(fatigue_6mo) fatigue_6mo,
	sum(fever_3mo) fever_3mo,
	sum(fever_6mo) fever_6mo,
	sum(dyspnea_3mo) dyspnea_3mo,
	sum(dyspnea_6mo) dyspnea_6mo,
	sum(cough_3mo) cough_3mo,
	sum(cough_6mo) cough_6mo,
	sum(chest_pain_3mo) chest_pain_3mo,
	sum(chest_pain_6mo) chest_pain_6mo,
	sum(palpitations_3mo) palpitations_3mo,
	sum(palpitations_6mo) palpitations_6mo,
	sum(diff_think_concen_3mo) diff_think_concen_3mo,
	sum(diff_think_concen_6mo) diff_think_concen_6mo,
	sum(headache_3mo) headache_3mo,
	sum(headache_6mo) headache_6mo,
	sum(changes_in_smell_3mo) changes_in_smell_3mo,
	sum(changes_in_smell_6mo) changes_in_smell_6mo,
	sum(changes_in_taste_3mo) changes_in_taste_3mo,
	sum(changes_in_taste_6mo) changes_in_taste_6mo,
	sum(diarrhea_3mo) diarrhea_3mo,
	sum(diarrhea_6mo) diarrhea_6mo,
	sum(ab_pain_3mo) ab_pain_3mo,
	sum(ab_pain_6mo) ab_pain_6mo,
	sum(joint_pain_3mo) joint_pain_3mo,
	sum(joint_pain_6mo) joint_pain_6mo,
	sum(muscle_pain_3mo) muscle_pain_3mo,
	sum(muscle_pain_6mo) muscle_pain_6mo,
	sum(nausea_or_vomit_3mo) nausea_or_vomit_3mo,
	sum(nausea_or_vomit_6mo) nausea_or_vomit_6mo,
	sum(sore_throat_3mo) sore_throat_3mo,
	sum(sore_throat_6mo) sore_throat_6mo,
	sum(back_pain_3mo) back_pain_3mo,
	sum(back_pain_6mo) back_pain_6mo,
	sum(other_pain_3mo) other_pain_3mo,
	sum(other_pain_6mo) other_pain_6mo,
	sum(pins_needles_3mo) pins_needles_3mo,
	sum(pins_needles_6mo) pins_needles_6mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + chest_pain_3mo +
					palpitations_3mo + diff_think_concen_3mo + headache_3mo + changes_in_smell_3mo + 
					changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + joint_pain_3mo +
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo + back_pain_3mo +
					other_pain_3mo + pins_needles_3mo) = 0 
					then patient_id else null end) )) as zero_all_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + chest_pain_3mo +
					palpitations_3mo + diff_think_concen_3mo + headache_3mo + changes_in_smell_3mo + 
					changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + joint_pain_3mo +
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo + back_pain_3mo +
					other_pain_3mo + pins_needles_3mo) BETWEEN 1 AND 2
					then patient_id else null end) )) as one_to_two_all_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + chest_pain_3mo +
					palpitations_3mo + diff_think_concen_3mo + headache_3mo + changes_in_smell_3mo + 
					changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + joint_pain_3mo +
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo + back_pain_3mo +
					other_pain_3mo + pins_needles_3mo) BETWEEN 3 AND 4
					then patient_id else null end) )) as three_to_four_all_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + chest_pain_3mo +
					palpitations_3mo + diff_think_concen_3mo + headache_3mo + changes_in_smell_3mo + 
					changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + joint_pain_3mo +
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo + back_pain_3mo +
					other_pain_3mo + pins_needles_3mo) >= 5
					then patient_id else null end) )) as five_plus_all_symp_3mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + chest_pain_6mo +
					palpitations_6mo + diff_think_concen_6mo + headache_6mo + changes_in_smell_6mo + 
					changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + joint_pain_6mo +
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo + back_pain_6mo +
					other_pain_6mo + pins_needles_6mo) = 0 
					then patient_id else null end) )) as zero_all_symp_6mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + chest_pain_6mo +
					palpitations_6mo + diff_think_concen_6mo + headache_6mo + changes_in_smell_6mo + 
					changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + joint_pain_6mo +
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo + back_pain_6mo +
					other_pain_6mo + pins_needles_6mo) BETWEEN 1 AND 2
					then patient_id else null end) )) as one_to_two_all_symp_6mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + chest_pain_6mo +
					palpitations_6mo + diff_think_concen_6mo + headache_6mo + changes_in_smell_6mo + 
					changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + joint_pain_6mo +
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo + back_pain_6mo +
					other_pain_6mo + pins_needles_6mo) BETWEEN 3 AND 4
					then patient_id else null end) )) as three_to_four_all_symp_6mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + chest_pain_6mo +
					palpitations_6mo + diff_think_concen_6mo + headache_6mo + changes_in_smell_6mo + 
					changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + joint_pain_6mo +
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo + back_pain_6mo +
					other_pain_6mo + pins_needles_6mo) >= 5
					then patient_id else null end) )) as five_plus_all_symp_6mo,	
	count(distinct((case when 
					fatigue_3mo >= 1 or fever_3mo >= 1 or dyspnea_3mo >= 1 or
					cough_3mo >= 1 or headache_3mo >= 1 or changes_in_smell_3mo >= 1 or
					changes_in_taste_3mo >= 1 or diarrhea_3mo >= 1 or ab_pain_3mo >= 1 or
					muscle_pain_3mo >= 1 or nausea_or_vomit_3mo >= 1 or sore_throat_3mo >= 1
					then patient_id else null end) ))as uk_symp_3mo,
	count(distinct((case when 
					fatigue_6mo >= 1 or fever_6mo >= 1 or dyspnea_6mo >= 1 or
					cough_6mo >= 1 or headache_6mo >= 1 or changes_in_smell_6mo >= 1 or
					changes_in_taste_6mo >= 1 or diarrhea_6mo >= 1 or ab_pain_6mo >= 1 or
					muscle_pain_6mo >= 1 or nausea_or_vomit_6mo >= 1 or sore_throat_6mo >= 1
					then patient_id else null end) ))as uk_symp_6mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + headache_3mo + 
					changes_in_smell_3mo + changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + 
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo) = 0 
					then patient_id else null end) )) as zero_uk_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + headache_3mo + 
					changes_in_smell_3mo + changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + 
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo) BETWEEN 1 AND 2
					then patient_id else null end) )) as one_to_two_uk_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + headache_3mo + 
					changes_in_smell_3mo + changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + 
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo) BETWEEN 3 AND 4
					then patient_id else null end) )) as three_to_four_uk_symp_3mo,
	count(distinct((case when 
					(fatigue_3mo + fever_3mo + dyspnea_3mo + cough_3mo + headache_3mo + 
					changes_in_smell_3mo + changes_in_taste_3mo + diarrhea_3mo + ab_pain_3mo + 
					muscle_pain_3mo + nausea_or_vomit_3mo + sore_throat_3mo) >= 5
					then patient_id else null end) )) as five_plus_uk_symp_3mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + headache_6mo + 
					changes_in_smell_6mo + changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + 
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo) = 0 
					then patient_id else null end) )) as zero_uk_symp_6mo,
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + headache_6mo + 
					changes_in_smell_6mo + changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + 
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo) BETWEEN 1 AND 2
					then patient_id else null end) )) as one_to_two_uk_symp_6mo,	
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + headache_6mo + 
					changes_in_smell_6mo + changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + 
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo) BETWEEN 3 AND 4
					then patient_id else null end) )) as three_to_four_uk_symp_6mo,	
	count(distinct((case when 
					(fatigue_6mo + fever_6mo + dyspnea_6mo + cough_6mo + headache_6mo + 
					changes_in_smell_6mo + changes_in_taste_6mo + diarrhea_6mo + ab_pain_6mo + 
					muscle_pain_6mo + nausea_or_vomit_6mo + sore_throat_6mo) >= 5
					then patient_id else null end) )) as five_plus_uk_symp_6mo		
    FROM symptom_by_pat					
	--FROM kre_report.lc_col_creation
	GROUP BY age_group_10_yr, sex, race
)

SELECT * from long_covid_output ORDER by sex, age_group_10_yr, race;
--SELECT * from kre_report.lc_output ORDER by sex, age_group_10_yr, race;


	   
	   
	   
