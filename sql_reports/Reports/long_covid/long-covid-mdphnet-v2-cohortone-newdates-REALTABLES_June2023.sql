 -- IDENTIFY INDEX PAIENTS, INDEX DATES, and FOLLOW UP START DATE
--WITH index_pats AS (
    DROP TABLE IF EXISTS kre_report.lc_1_newdates_index_pats;
    CREATE TABLE kre_report.lc_1_newdates_index_pats AS
	SELECT patient_id, min(min_qual_date) as index_date, 
	--(min(min_qual_date) + INTERVAL '112 days')::date as follow_3mo_end_date, (min(min_qual_date) + INTERVAL '196 days')::date as follow_6mo_end_date
	(min(min_qual_date) + INTERVAL '28 days')::date as fui_one_start_date, -- GREATER THAN OR EQUAL TO
	(min(min_qual_date) + INTERVAL '168 days')::date as fui_one_end_date --LESS THAN
	      -- FIRST POS COVID TEST IN DATE RANGE
	FROM (SELECT patient_id, min(date) as min_qual_date
		  FROM public.hef_event
		  WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
		  AND date >= '2022-06-01'
		  AND date < '2022-11-01'
		  GROUP BY patient_id
		  UNION
		  -- FIRST POS COVID ICD IN DATE RANGE
		  SELECT patient_id, min(date) as min_qual_date
		  FROM public.emr_encounter T1
		  INNER JOIN public.emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
		  WHERE dx_code_id in ('icd10:Z86.16', 'icd10:U07.1')
		  AND date >= '2022-06-01'
		  AND date < '2022-11-01'
		  GROUP BY patient_id) min_dates
	GROUP by patient_id;
	
-- IDENTIFY POSITIVE PATS WITHIN FIRST FOLLOW-UP INTERVAL
--pat_fui_one_exclusions AS (
    DROP TABLE IF EXISTS kre_report.lc_1_newdates_pat_fui_one_exclusions;
    CREATE TABLE kre_report.lc_1_newdates_pat_fui_one_exclusions AS
	SELECT patient_id, max(exclude_fui_one) exclude_from_fui_one, min(exclusion_date) as exclusion_date, (min(exclusion_date) + INTERVAL '28 days')::date as new_fui_two_start_date
 	FROM (
		SELECT T1.patient_id, 1 as exclude_fui_one, T2.date as exclusion_date
		--FROM index_pats T1
		FROM kre_report.lc_1_newdates_index_pats T1
		INNER JOIN public.hef_event T2 ON (T1.patient_id = T2.patient_id)
		WHERE name in ('lx:covid19_pcr:positive', 'lx:covid19_ag:positive')
		AND date >= fui_one_start_date
		AND date < fui_one_end_date
		UNION
		SELECT T1.patient_id, 1 as exclude_fui_one, T2.date as exclusion_date
		--FROM index_pats T1
		FROM kre_report.lc_1_newdates_index_pats T1
		INNER JOIN public.emr_encounter T2 ON (T1.patient_id = T2.patient_id)
		INNER JOIN public.emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
		WHERE dx_code_id in ('icd10:Z86.16', 'icd10:U07.1')
		AND date >= fui_one_start_date 
		AND date < fui_one_end_date) excl_fui_one
	GROUP BY patient_id
	;
	

--all_icd_codes AS (
    DROP TABLE IF EXISTS kre_report.lc_1_newdates_all_icd_codes;
    CREATE TABLE kre_report.lc_1_newdates_all_icd_codes AS
	SELECT DISTINCT T1.patient_id, T2.date as icd_date, dx_code_id
	--FROM index_pats T1
	FROM kre_report.lc_1_newdates_index_pats T1
	INNER JOIN public.emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	INNER JOIN public.emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
	WHERE 
	T2.date >= fui_one_start_date
	AND T2.date < fui_one_end_date
	AND (
	       dx_code_id in (
		   'icd10:G93.3',
		   'icd10:R50.9',
		   'icd10:R09.82',
		   'icd10:R07.89',
		   'icd10:R00.2',
		   'icd10:R41.840',
		   'icd10:R51.9',
		   'icd10:R43.0',
		   'icd10:R43.1',
		   'icd10:R43.2',
		   'icd10:R19.7',
		   'icd10:R10.9'
		   'icd10:R10.84',
		   'icd10:R25.2',
		   'icd10:M25.5',
		   'icd10:R07.0',
		   'icd10:R07.9',
		   'icd10:R00.0',
		   'icd10:R41.89',
		   'icd10:R41.3',
		   'icd10:M54.2',
		   'icd10:M54.50',
		   'icd10:M54.59',
		   'icd10:M54.6',
		   'icd10:M54.9',
		   'icd10:G89.29',
		   'icd10:G89.4',
		   'icd10:R52',
		   'icd10:G64',
		   'icd10:R29.90',
		   'icd10:R07.1',
		   'icd10:R07.2',
		   'icd10:R07.9'
		   'icd10:R63.1',
		   'icd10:F52.0',
		   'icd10:F52.4',
		   'icd10:F52.9')
		 OR dx_code_id ilike 'icd10:R53.8%'
		 OR dx_code_id ilike 'icd10:R53.1%'
		 OR dx_code_id ilike 'icd10:R06.0%'
		 OR dx_code_id ilike 'icd10:R05%'
		 OR dx_code_id ilike 'icd10:R10.1%'
		 OR dx_code_id ilike 'icd10:R10.3%'
		 OR dx_code_id ilike 'icd10:M25.5%'
		 OR dx_code_id ilike 'icd10:M79.1%'
		 OR dx_code_id ilike 'icd10:M62.83%'
		 OR dx_code_id ilike 'icd10:R11%'
		 OR dx_code_id ilike 'icd10:I47%'
		 OR dx_code_id ilike 'icd10:M54.8%'
		 OR dx_code_id ilike 'icd10:M79.6%'
		 OR dx_code_id ilike 'icd10:R20%'
		 OR dx_code_id ilike 'icd10:R07.8%'
		 OR dx_code_id ilike 'icd10:F52.2%'
 		 OR dx_code_id ilike 'icd10:F52.3%'
	         OR dx_code_id ilike 'icd10:F42%'
	   ) 
	;
	
--all_fever_encs AS (
	DROP TABLE IF EXISTS kre_report.lc_1_newdates_all_fever_encs;
    CREATE TABLE kre_report.lc_1_newdates_all_fever_encs AS
	SELECT DISTINCT T1.patient_id, T2.date as icd_date, 'icd10:R50.9'::text as dx_code_id
	--FROM index_pats T1
	FROM kre_report.lc_1_newdates_index_pats T1
	INNER JOIN public.hef_event T2 ON (T1.patient_id = T2.patient_id and name = 'enc:fever')
	WHERE T2.date >= fui_one_start_date
	AND T2.date < fui_one_end_date
	;
	
--all_icd_and_fever AS (
    DROP TABLE IF EXISTS kre_report.lc_1_newdates_all_icd_and_fever;
    CREATE TABLE kre_report.lc_1_newdates_all_icd_and_fever AS
	SELECT patient_id, icd_date, dx_code_id
	FROM kre_report.lc_1_newdates_all_icd_codes
	--FROM all_icd_codes
	UNION
	SELECT patient_id, icd_date, dx_code_id
	FROM kre_report.lc_1_newdates_all_fever_encs
	--FROM all_fever_encs
;


-- CHARLSON CO-MORBIDITY INDEX
--cci_icds AS (
    DROP TABLE IF EXISTS kre_report.lc_1_newdates_cci_icds;
	CREATE TABLE kre_report.lc_1_newdates_cci_icds AS
	SELECT T1.patient_id,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I09.9%' OR T3.dx_code_id ilike 'icd10:I11.0%' OR T3.dx_code_id ilike 'icd10:I13.0%' 
				  OR T3.dx_code_id ilike 'icd10:I13.2%' OR T3.dx_code_id ilike 'icd10:I25.5%' OR T3.dx_code_id ilike 'icd10:I42.0%' 
				  OR T3.dx_code_id ilike 'icd10:I42.5%' OR T3.dx_code_id ilike 'icd10:I42.6%' OR T3.dx_code_id ilike 'icd10:I42.7%' 
				  OR T3.dx_code_id ilike 'icd10:I42.8%' OR T3.dx_code_id ilike 'icd10:I42.9%' OR T3.dx_code_id ilike 'icd10:I43.%' 
				  OR T3.dx_code_id ilike 'icd10:I50.%'  OR T3.dx_code_id ilike 'icd10:P29.0%' 
		then 2 else 0 END) as cci_chf,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:F00.%' OR T3.dx_code_id ilike 'icd10:F01.%' OR T3.dx_code_id ilike 'icd10:F02.%' 
				  OR T3.dx_code_id ilike 'icd10:F03.%' OR T3.dx_code_id ilike 'icd10:F05.1%' OR T3.dx_code_id ilike 'icd10:G30.%' 
                  OR T3.dx_code_id ilike 'icd10:G31.1%' 
		then 2 else 0 END) as cci_dementia,	
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I27.8%' OR T3.dx_code_id ilike 'icd10:I27.9%' OR T3.dx_code_id ilike 'icd10:J40.%' 
				  OR T3.dx_code_id ilike 'icd10:J41.%' OR T3.dx_code_id ilike 'icd10:J42.%' OR T3.dx_code_id ilike 'icd10:J43.%' 
				  OR T3.dx_code_id ilike 'icd10:J44.%' OR T3.dx_code_id ilike 'icd10:J45.%' OR T3.dx_code_id ilike 'icd10:J46.%' 
				  OR T3.dx_code_id ilike 'icd10:J47.%' OR T3.dx_code_id ilike 'icd10:J60.%' OR T3.dx_code_id ilike 'icd10:J61.%' 
				  OR T3.dx_code_id ilike 'icd10:J62.%' OR T3.dx_code_id ilike 'icd10:J63.%' OR T3.dx_code_id ilike 'icd10:J64.%' 
				  OR T3.dx_code_id ilike 'icd10:J65.%' OR T3.dx_code_id ilike 'icd10:J66.%' OR T3.dx_code_id ilike 'icd10:J67.%' 
				  OR T3.dx_code_id ilike 'icd10:J68.4%' OR T3.dx_code_id ilike 'icd10:J70.1%' OR T3.dx_code_id ilike 'icd10:J70.3%' 
        then 1 else 0 END) as cci_chron_pulm_dis,
	MAX(CASE WHEN T3.dx_code_id ilike 'icd10:M05.%' OR T3.dx_code_id ilike 'icd10:M06.%' OR T3.dx_code_id ilike 'icd10:M31.5%' 
				  OR T3.dx_code_id ilike 'icd10:M32.%' OR T3.dx_code_id ilike 'icd10:M33.%' OR T3.dx_code_id ilike 'icd10:M34.%' 
				  OR T3.dx_code_id ilike 'icd10:M35.1%' OR T3.dx_code_id ilike 'icd10:M35.3%' OR T3.dx_code_id ilike 'icd10:M36.0%' 
        then 1 else 0 END) as cci_rheaumatic_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:B18.%' OR T3.dx_code_id ilike 'icd10:K70.0%' OR T3.dx_code_id ilike 'icd10:K70.1%' 
				  OR T3.dx_code_id ilike 'icd10:K70.2%' OR T3.dx_code_id ilike 'icd10:K70.3%' OR T3.dx_code_id ilike 'icd10:K70.9%' 
				  OR T3.dx_code_id ilike 'icd10:K71.3%' OR T3.dx_code_id ilike 'icd10:K71.4%' OR T3.dx_code_id ilike 'icd10:K71.5%' 
				  OR T3.dx_code_id ilike 'icd10:K71.7%' OR T3.dx_code_id ilike 'icd10:K73.%' OR T3.dx_code_id ilike 'icd10:K74.%' 
				  OR T3.dx_code_id ilike 'icd10:K76.0%' OR T3.dx_code_id ilike 'icd10:K76.2%' OR T3.dx_code_id ilike 'icd10:K76.3%' 
				  OR T3.dx_code_id ilike 'icd10:K76.4%' OR T3.dx_code_id ilike 'icd10:K76.8%' OR T3.dx_code_id ilike 'icd10:K76.9%' 
				  OR T3.dx_code_id ilike 'icd10:Z94.4%' 
        then 2 else 0 END) as cci_mild_liver_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:E10.2%' OR T3.dx_code_id ilike 'icd10:E10.3%' OR T3.dx_code_id ilike 'icd10:E10.4%' 
				  OR T3.dx_code_id ilike 'icd10:E10.5%' OR T3.dx_code_id ilike 'icd10:E10.7%' OR T3.dx_code_id ilike 'icd10:E11.2%' 
				  OR T3.dx_code_id ilike 'icd10:E11.3%' OR T3.dx_code_id ilike 'icd10:E11.4%' OR T3.dx_code_id ilike 'icd10:E11.5%' 
				  OR T3.dx_code_id ilike 'icd10:E11.7%' OR T3.dx_code_id ilike 'icd10:E12.2%' OR T3.dx_code_id ilike 'icd10:E12.3%' 
				  OR T3.dx_code_id ilike 'icd10:E12.4%' OR T3.dx_code_id ilike 'icd10:E12.5%' OR T3.dx_code_id ilike 'icd10:E12.7%' 
				  OR T3.dx_code_id ilike 'icd10:E13.2%' OR T3.dx_code_id ilike 'icd10:E13.3%' OR T3.dx_code_id ilike 'icd10:E13.4%' 
				  OR T3.dx_code_id ilike 'icd10:E13.5%' OR T3.dx_code_id ilike 'icd10:E13.7%' OR T3.dx_code_id ilike 'icd10:E14.2%' 
				  OR T3.dx_code_id ilike 'icd10:E14.3%' OR T3.dx_code_id ilike 'icd10:E14.4%' OR T3.dx_code_id ilike 'icd10:E14.5%' 
				  OR T3.dx_code_id ilike 'icd10:E14.7%' 
        then 1 else 0 END) as cci_diab_w_compl,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:G04.1%' OR T3.dx_code_id ilike 'icd10:G11.4%' OR T3.dx_code_id ilike 'icd10:G80.1%' 
				  OR T3.dx_code_id ilike 'icd10:G80.2%' OR T3.dx_code_id ilike 'icd10:G81.%' OR T3.dx_code_id ilike 'icd10:G82.%' 
				  OR T3.dx_code_id ilike 'icd10:G83.0%' OR T3.dx_code_id ilike 'icd10:G83.1%' OR T3.dx_code_id ilike 'icd10:G83.2%' 
				  OR T3.dx_code_id ilike 'icd10:G83.3%' OR T3.dx_code_id ilike 'icd10:G83.4%' OR T3.dx_code_id ilike 'icd10:G83.9%' 
        then 2 else 0 END) as cci_hemip_or_parap,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I12.0%' OR T3.dx_code_id ilike 'icd10:I13.1%' OR T3.dx_code_id ilike 'icd10:N03.2%' 
				  OR T3.dx_code_id ilike 'icd10:N03.3%' OR T3.dx_code_id ilike 'icd10:N03.4%' OR T3.dx_code_id ilike 'icd10:N03.5%' 
				  OR T3.dx_code_id ilike 'icd10:N03.6%' OR T3.dx_code_id ilike 'icd10:N03.7%' OR T3.dx_code_id ilike 'icd10:N05.2%' 
				  OR T3.dx_code_id ilike 'icd10:N05.3%' OR T3.dx_code_id ilike 'icd10:N05.4%' OR T3.dx_code_id ilike 'icd10:N05.5%' 
				  OR T3.dx_code_id ilike 'icd10:N05.6%' OR T3.dx_code_id ilike 'icd10:N05.7%' OR T3.dx_code_id ilike 'icd10:N18.%' 
				  OR T3.dx_code_id ilike 'icd10:N19.%' OR T3.dx_code_id ilike 'icd10:N25.0%' OR T3.dx_code_id ilike 'icd10:Z49.0%' 
				  OR T3.dx_code_id ilike 'icd10:Z49.1%' OR T3.dx_code_id ilike 'icd10:Z49.2%' OR T3.dx_code_id ilike 'icd10:Z94.0%' 
				  OR T3.dx_code_id ilike 'icd10:Z99.2%' 
        then 1 else 0 END) as cci_renal_dis,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:C00.%' OR T3.dx_code_id ilike 'icd10:C01.%' OR T3.dx_code_id ilike 'icd10:C02.%' 
				  OR T3.dx_code_id ilike 'icd10:C03.%' OR T3.dx_code_id ilike 'icd10:C04.%' OR T3.dx_code_id ilike 'icd10:C05.%' 
				  OR T3.dx_code_id ilike 'icd10:C06.%' OR T3.dx_code_id ilike 'icd10:C07.%' OR T3.dx_code_id ilike 'icd10:C08.%' 
				  OR T3.dx_code_id ilike 'icd10:C09.%' OR T3.dx_code_id ilike 'icd10:C10.%' OR T3.dx_code_id ilike 'icd10:C11.%' 
				  OR T3.dx_code_id ilike 'icd10:C12.%' OR T3.dx_code_id ilike 'icd10:C13.%' OR T3.dx_code_id ilike 'icd10:C14.%' 
				  OR T3.dx_code_id ilike 'icd10:C15.%' OR T3.dx_code_id ilike 'icd10:C16.%' OR T3.dx_code_id ilike 'icd10:C17.%' 
				  OR T3.dx_code_id ilike 'icd10:C18.%' OR T3.dx_code_id ilike 'icd10:C19.%' OR T3.dx_code_id ilike 'icd10:C20.%' 
				  OR T3.dx_code_id ilike 'icd10:C21.%' OR T3.dx_code_id ilike 'icd10:C22.%' OR T3.dx_code_id ilike 'icd10:C23.%' 
				  OR T3.dx_code_id ilike 'icd10:C24.%' OR T3.dx_code_id ilike 'icd10:C25.%' OR T3.dx_code_id ilike 'icd10:C26.%' 
				  OR T3.dx_code_id ilike 'icd10:C30.%' OR T3.dx_code_id ilike 'icd10:C31.%' OR T3.dx_code_id ilike 'icd10:C32.%' 
				  OR T3.dx_code_id ilike 'icd10:C33.%' OR T3.dx_code_id ilike 'icd10:C34.%' OR T3.dx_code_id ilike 'icd10:C37.%' 
				  OR T3.dx_code_id ilike 'icd10:C38.%' OR T3.dx_code_id ilike 'icd10:C39.%' OR T3.dx_code_id ilike 'icd10:C40.%' 
				  OR T3.dx_code_id ilike 'icd10:C41.%' OR T3.dx_code_id ilike 'icd10:C43.%' OR T3.dx_code_id ilike 'icd10:C45.%' 
				  OR T3.dx_code_id ilike 'icd10:C46.%' OR T3.dx_code_id ilike 'icd10:C47.%' OR T3.dx_code_id ilike 'icd10:C48.%' 
				  OR T3.dx_code_id ilike 'icd10:C49.%' OR T3.dx_code_id ilike 'icd10:C50.%' OR T3.dx_code_id ilike 'icd10:C51.%' 
				  OR T3.dx_code_id ilike 'icd10:C52.%' OR T3.dx_code_id ilike 'icd10:C53.%' OR T3.dx_code_id ilike 'icd10:C54.%' 
				  OR T3.dx_code_id ilike 'icd10:C55.%' OR T3.dx_code_id ilike 'icd10:C56.%' OR T3.dx_code_id ilike 'icd10:C57.%' 
				  OR T3.dx_code_id ilike 'icd10:C58.%' OR T3.dx_code_id ilike 'icd10:C60.%' OR T3.dx_code_id ilike 'icd10:C61.%' 
				  OR T3.dx_code_id ilike 'icd10:C62.%' OR T3.dx_code_id ilike 'icd10:C63.%' OR T3.dx_code_id ilike 'icd10:C64.%' 
				  OR T3.dx_code_id ilike 'icd10:C65.%' OR T3.dx_code_id ilike 'icd10:C66.%' OR T3.dx_code_id ilike 'icd10:C67.%' 
				  OR T3.dx_code_id ilike 'icd10:C68.%' OR T3.dx_code_id ilike 'icd10:C69.%' OR T3.dx_code_id ilike 'icd10:C70.%' 
				  OR T3.dx_code_id ilike 'icd10:C71.%' OR T3.dx_code_id ilike 'icd10:C72.%' OR T3.dx_code_id ilike 'icd10:C73.%' 
				  OR T3.dx_code_id ilike 'icd10:C74.%' OR T3.dx_code_id ilike 'icd10:C75.%' OR T3.dx_code_id ilike 'icd10:C76.%' 
				  OR T3.dx_code_id ilike 'icd10:C81.%' OR T3.dx_code_id ilike 'icd10:C82.%' OR T3.dx_code_id ilike 'icd10:C83.%' 
				  OR T3.dx_code_id ilike 'icd10:C84.%' OR T3.dx_code_id ilike 'icd10:C85.%' OR T3.dx_code_id ilike 'icd10:C88.%' 
				  OR T3.dx_code_id ilike 'icd10:C90.%' OR T3.dx_code_id ilike 'icd10:C91.%' OR T3.dx_code_id ilike 'icd10:C92.%' 
				  OR T3.dx_code_id ilike 'icd10:C93.%' OR T3.dx_code_id ilike 'icd10:C94.%' OR T3.dx_code_id ilike 'icd10:C95.%' 
				  OR T3.dx_code_id ilike 'icd10:C96.%' OR T3.dx_code_id ilike 'icd10:C97.%' 
        then 2 else 0 END) as cci_any_malig,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:I85.0%' OR T3.dx_code_id ilike 'icd10:I85.9%' OR T3.dx_code_id ilike 'icd10:I86.4%' 
				  OR T3.dx_code_id ilike 'icd10:I98.2%' OR T3.dx_code_id ilike 'icd10:K70.4%' OR T3.dx_code_id ilike 'icd10:K71.1%' 
				  OR T3.dx_code_id ilike 'icd10:K72.1%' OR T3.dx_code_id ilike 'icd10:K72.9%' OR T3.dx_code_id ilike 'icd10:K76.5%' 
				  OR T3.dx_code_id ilike 'icd10:K76.6%' OR T3.dx_code_id ilike 'icd10:K76.7%' 
        then 4 else 0 END) as cci_mod_or_severe_liver_disease,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:C77.%' OR T3.dx_code_id ilike 'icd10:C78.%' OR T3.dx_code_id ilike 'icd10:C79.%' 
				  OR T3.dx_code_id ilike 'icd10:C80.%' 
        then 6 else 0 END) as cci_met_solid_tumor,
    MAX(CASE WHEN T3.dx_code_id ilike 'icd10:B20.%' OR T3.dx_code_id ilike 'icd10:B21.%' OR T3.dx_code_id ilike 'icd10:B22.%' 
				  OR T3.dx_code_id ilike 'icd10:B24.%' 
         then 4 else 0 END) as cci_aids_hiv
	--FROM index_pats T1
	FROM kre_report.lc_1_newdates_index_pats T1
	JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	JOIN emr_encounter_dx_codes T3 ON (T2.id = T3.encounter_id)
	WHERE T2.date <= T1.index_date
	AND (
			T3.dx_code_id ilike 'icd10:I09.9%' 
			OR T3.dx_code_id ilike 'icd10:I11.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.2%' 
			OR T3.dx_code_id ilike 'icd10:I25.5%' 
			OR T3.dx_code_id ilike 'icd10:I42.0%' 
			OR T3.dx_code_id ilike 'icd10:I42.5%' 
			OR T3.dx_code_id ilike 'icd10:I42.6%' 
			OR T3.dx_code_id ilike 'icd10:I42.7%' 
			OR T3.dx_code_id ilike 'icd10:I42.8%' 
			OR T3.dx_code_id ilike 'icd10:I42.9%' 
			OR T3.dx_code_id ilike 'icd10:I43.%' 
			OR T3.dx_code_id ilike 'icd10:I50.%' 
			OR T3.dx_code_id ilike 'icd10:P29.0%' 
			OR T3.dx_code_id ilike 'icd10:F00.%' 
			OR T3.dx_code_id ilike 'icd10:F01.%' 
			OR T3.dx_code_id ilike 'icd10:F02.%' 
			OR T3.dx_code_id ilike 'icd10:F03.%' 
			OR T3.dx_code_id ilike 'icd10:F05.1%' 
			OR T3.dx_code_id ilike 'icd10:G30.%' 
			OR T3.dx_code_id ilike 'icd10:G31.1%' 
			OR T3.dx_code_id ilike 'icd10:I27.8%' 
			OR T3.dx_code_id ilike 'icd10:I27.9%' 
			OR T3.dx_code_id ilike 'icd10:J40.%' 
			OR T3.dx_code_id ilike 'icd10:J41.%' 
			OR T3.dx_code_id ilike 'icd10:J42.%' 
			OR T3.dx_code_id ilike 'icd10:J43.%' 
			OR T3.dx_code_id ilike 'icd10:J44.%' 
			OR T3.dx_code_id ilike 'icd10:J45.%' 
			OR T3.dx_code_id ilike 'icd10:J46.%' 
			OR T3.dx_code_id ilike 'icd10:J47.%' 
			OR T3.dx_code_id ilike 'icd10:J60.%' 
			OR T3.dx_code_id ilike 'icd10:J61.%' 
			OR T3.dx_code_id ilike 'icd10:J62.%' 
			OR T3.dx_code_id ilike 'icd10:J63.%' 
			OR T3.dx_code_id ilike 'icd10:J64.%' 
			OR T3.dx_code_id ilike 'icd10:J65.%' 
			OR T3.dx_code_id ilike 'icd10:J66.%' 
			OR T3.dx_code_id ilike 'icd10:J67.%' 
			OR T3.dx_code_id ilike 'icd10:J68.4%' 
			OR T3.dx_code_id ilike 'icd10:J70.1%' 
			OR T3.dx_code_id ilike 'icd10:J70.3%' 
			OR T3.dx_code_id ilike 'icd10:M05.%' 
			OR T3.dx_code_id ilike 'icd10:M06.%' 
			OR T3.dx_code_id ilike 'icd10:M31.5%' 
			OR T3.dx_code_id ilike 'icd10:M32.%' 
			OR T3.dx_code_id ilike 'icd10:M33.%' 
			OR T3.dx_code_id ilike 'icd10:M34.%' 
			OR T3.dx_code_id ilike 'icd10:M35.1%' 
			OR T3.dx_code_id ilike 'icd10:M35.3%' 
			OR T3.dx_code_id ilike 'icd10:M36.0%' 
			OR T3.dx_code_id ilike 'icd10:B18.%' 
			OR T3.dx_code_id ilike 'icd10:K70.0%' 
			OR T3.dx_code_id ilike 'icd10:K70.1%' 
			OR T3.dx_code_id ilike 'icd10:K70.2%' 
			OR T3.dx_code_id ilike 'icd10:K70.3%' 
			OR T3.dx_code_id ilike 'icd10:K70.9%' 
			OR T3.dx_code_id ilike 'icd10:K71.3%' 
			OR T3.dx_code_id ilike 'icd10:K71.4%' 
			OR T3.dx_code_id ilike 'icd10:K71.5%' 
			OR T3.dx_code_id ilike 'icd10:K71.7%' 
			OR T3.dx_code_id ilike 'icd10:K73.%' 
			OR T3.dx_code_id ilike 'icd10:K74.%' 
			OR T3.dx_code_id ilike 'icd10:K76.0%' 
			OR T3.dx_code_id ilike 'icd10:K76.2%' 
			OR T3.dx_code_id ilike 'icd10:K76.3%' 
			OR T3.dx_code_id ilike 'icd10:K76.4%' 
			OR T3.dx_code_id ilike 'icd10:K76.8%' 
			OR T3.dx_code_id ilike 'icd10:K76.9%' 
			OR T3.dx_code_id ilike 'icd10:Z94.4%' 
			OR T3.dx_code_id ilike 'icd10:E10.2%' 
			OR T3.dx_code_id ilike 'icd10:E10.3%' 
			OR T3.dx_code_id ilike 'icd10:E10.4%' 
			OR T3.dx_code_id ilike 'icd10:E10.5%' 
			OR T3.dx_code_id ilike 'icd10:E10.7%' 
			OR T3.dx_code_id ilike 'icd10:E11.2%' 
			OR T3.dx_code_id ilike 'icd10:E11.3%' 
			OR T3.dx_code_id ilike 'icd10:E11.4%' 
			OR T3.dx_code_id ilike 'icd10:E11.5%' 
			OR T3.dx_code_id ilike 'icd10:E11.7%' 
			OR T3.dx_code_id ilike 'icd10:E12.2%' 
			OR T3.dx_code_id ilike 'icd10:E12.3%' 
			OR T3.dx_code_id ilike 'icd10:E12.4%' 
			OR T3.dx_code_id ilike 'icd10:E12.5%' 
			OR T3.dx_code_id ilike 'icd10:E12.7%' 
			OR T3.dx_code_id ilike 'icd10:E13.2%' 
			OR T3.dx_code_id ilike 'icd10:E13.3%' 
			OR T3.dx_code_id ilike 'icd10:E13.4%' 
			OR T3.dx_code_id ilike 'icd10:E13.5%' 
			OR T3.dx_code_id ilike 'icd10:E13.7%' 
			OR T3.dx_code_id ilike 'icd10:E14.2%' 
			OR T3.dx_code_id ilike 'icd10:E14.3%' 
			OR T3.dx_code_id ilike 'icd10:E14.4%' 
			OR T3.dx_code_id ilike 'icd10:E14.5%' 
			OR T3.dx_code_id ilike 'icd10:E14.7%' 
			OR T3.dx_code_id ilike 'icd10:G04.1%' 
			OR T3.dx_code_id ilike 'icd10:G11.4%' 
			OR T3.dx_code_id ilike 'icd10:G80.1%' 
			OR T3.dx_code_id ilike 'icd10:G80.2%' 
			OR T3.dx_code_id ilike 'icd10:G81.%' 
			OR T3.dx_code_id ilike 'icd10:G82.%' 
			OR T3.dx_code_id ilike 'icd10:G83.0%' 
			OR T3.dx_code_id ilike 'icd10:G83.1%' 
			OR T3.dx_code_id ilike 'icd10:G83.2%' 
			OR T3.dx_code_id ilike 'icd10:G83.3%' 
			OR T3.dx_code_id ilike 'icd10:G83.4%' 
			OR T3.dx_code_id ilike 'icd10:G83.9%' 
			OR T3.dx_code_id ilike 'icd10:I12.0%' 
			OR T3.dx_code_id ilike 'icd10:I13.1%' 
			OR T3.dx_code_id ilike 'icd10:N03.2%' 
			OR T3.dx_code_id ilike 'icd10:N03.3%' 
			OR T3.dx_code_id ilike 'icd10:N03.4%' 
			OR T3.dx_code_id ilike 'icd10:N03.5%' 
			OR T3.dx_code_id ilike 'icd10:N03.6%' 
			OR T3.dx_code_id ilike 'icd10:N03.7%' 
			OR T3.dx_code_id ilike 'icd10:N05.2%' 
			OR T3.dx_code_id ilike 'icd10:N05.3%' 
			OR T3.dx_code_id ilike 'icd10:N05.4%' 
			OR T3.dx_code_id ilike 'icd10:N05.5%' 
			OR T3.dx_code_id ilike 'icd10:N05.6%' 
			OR T3.dx_code_id ilike 'icd10:N05.7%' 
			OR T3.dx_code_id ilike 'icd10:N18.%' 
			OR T3.dx_code_id ilike 'icd10:N19.%' 
			OR T3.dx_code_id ilike 'icd10:N25.0%' 
			OR T3.dx_code_id ilike 'icd10:Z49.0%' 
			OR T3.dx_code_id ilike 'icd10:Z49.1%' 
			OR T3.dx_code_id ilike 'icd10:Z49.2%' 
			OR T3.dx_code_id ilike 'icd10:Z94.0%' 
			OR T3.dx_code_id ilike 'icd10:Z99.2%' 
			OR T3.dx_code_id ilike 'icd10:C00.%' 
			OR T3.dx_code_id ilike 'icd10:C01.%' 
			OR T3.dx_code_id ilike 'icd10:C02.%' 
			OR T3.dx_code_id ilike 'icd10:C03.%' 
			OR T3.dx_code_id ilike 'icd10:C04.%' 
			OR T3.dx_code_id ilike 'icd10:C05.%' 
			OR T3.dx_code_id ilike 'icd10:C06.%' 
			OR T3.dx_code_id ilike 'icd10:C07.%' 
			OR T3.dx_code_id ilike 'icd10:C08.%' 
			OR T3.dx_code_id ilike 'icd10:C09.%' 
			OR T3.dx_code_id ilike 'icd10:C10.%' 
			OR T3.dx_code_id ilike 'icd10:C11.%' 
			OR T3.dx_code_id ilike 'icd10:C12.%' 
			OR T3.dx_code_id ilike 'icd10:C13.%' 
			OR T3.dx_code_id ilike 'icd10:C14.%' 
			OR T3.dx_code_id ilike 'icd10:C15.%' 
			OR T3.dx_code_id ilike 'icd10:C16.%' 
			OR T3.dx_code_id ilike 'icd10:C17.%' 
			OR T3.dx_code_id ilike 'icd10:C18.%' 
			OR T3.dx_code_id ilike 'icd10:C19.%' 
			OR T3.dx_code_id ilike 'icd10:C20.%' 
			OR T3.dx_code_id ilike 'icd10:C21.%' 
			OR T3.dx_code_id ilike 'icd10:C22.%' 
			OR T3.dx_code_id ilike 'icd10:C23.%' 
			OR T3.dx_code_id ilike 'icd10:C24.%' 
			OR T3.dx_code_id ilike 'icd10:C25.%' 
			OR T3.dx_code_id ilike 'icd10:C26.%' 
			OR T3.dx_code_id ilike 'icd10:C30.%' 
			OR T3.dx_code_id ilike 'icd10:C31.%' 
			OR T3.dx_code_id ilike 'icd10:C32.%' 
			OR T3.dx_code_id ilike 'icd10:C33.%' 
			OR T3.dx_code_id ilike 'icd10:C34.%' 
			OR T3.dx_code_id ilike 'icd10:C37.%' 
			OR T3.dx_code_id ilike 'icd10:C38.%' 
			OR T3.dx_code_id ilike 'icd10:C39.%' 
			OR T3.dx_code_id ilike 'icd10:C40.%' 
			OR T3.dx_code_id ilike 'icd10:C41.%' 
			OR T3.dx_code_id ilike 'icd10:C43.%' 
			OR T3.dx_code_id ilike 'icd10:C45.%' 
			OR T3.dx_code_id ilike 'icd10:C46.%' 
			OR T3.dx_code_id ilike 'icd10:C47.%' 
			OR T3.dx_code_id ilike 'icd10:C48.%' 
			OR T3.dx_code_id ilike 'icd10:C49.%' 
			OR T3.dx_code_id ilike 'icd10:C50.%' 
			OR T3.dx_code_id ilike 'icd10:C51.%' 
			OR T3.dx_code_id ilike 'icd10:C52.%' 
			OR T3.dx_code_id ilike 'icd10:C53.%' 
			OR T3.dx_code_id ilike 'icd10:C54.%' 
			OR T3.dx_code_id ilike 'icd10:C55.%' 
			OR T3.dx_code_id ilike 'icd10:C56.%' 
			OR T3.dx_code_id ilike 'icd10:C57.%' 
			OR T3.dx_code_id ilike 'icd10:C58.%' 
			OR T3.dx_code_id ilike 'icd10:C60.%' 
			OR T3.dx_code_id ilike 'icd10:C61.%' 
			OR T3.dx_code_id ilike 'icd10:C62.%' 
			OR T3.dx_code_id ilike 'icd10:C63.%' 
			OR T3.dx_code_id ilike 'icd10:C64.%' 
			OR T3.dx_code_id ilike 'icd10:C65.%' 
			OR T3.dx_code_id ilike 'icd10:C66.%' 
			OR T3.dx_code_id ilike 'icd10:C67.%' 
			OR T3.dx_code_id ilike 'icd10:C68.%' 
			OR T3.dx_code_id ilike 'icd10:C69.%' 
			OR T3.dx_code_id ilike 'icd10:C70.%' 
			OR T3.dx_code_id ilike 'icd10:C71.%' 
			OR T3.dx_code_id ilike 'icd10:C72.%' 
			OR T3.dx_code_id ilike 'icd10:C73.%' 
			OR T3.dx_code_id ilike 'icd10:C74.%' 
			OR T3.dx_code_id ilike 'icd10:C75.%' 
			OR T3.dx_code_id ilike 'icd10:C76.%' 
			OR T3.dx_code_id ilike 'icd10:C81.%' 
			OR T3.dx_code_id ilike 'icd10:C82.%' 
			OR T3.dx_code_id ilike 'icd10:C83.%' 
			OR T3.dx_code_id ilike 'icd10:C84.%' 
			OR T3.dx_code_id ilike 'icd10:C85.%' 
			OR T3.dx_code_id ilike 'icd10:C88.%' 
			OR T3.dx_code_id ilike 'icd10:C90.%' 
			OR T3.dx_code_id ilike 'icd10:C91.%' 
			OR T3.dx_code_id ilike 'icd10:C92.%' 
			OR T3.dx_code_id ilike 'icd10:C93.%' 
			OR T3.dx_code_id ilike 'icd10:C94.%' 
			OR T3.dx_code_id ilike 'icd10:C95.%' 
			OR T3.dx_code_id ilike 'icd10:C96.%' 
			OR T3.dx_code_id ilike 'icd10:C97.%' 
			OR T3.dx_code_id ilike 'icd10:I85.0%' 
			OR T3.dx_code_id ilike 'icd10:I85.9%' 
			OR T3.dx_code_id ilike 'icd10:I86.4%' 
			OR T3.dx_code_id ilike 'icd10:I98.2%' 
			OR T3.dx_code_id ilike 'icd10:K70.4%' 
			OR T3.dx_code_id ilike 'icd10:K71.1%' 
			OR T3.dx_code_id ilike 'icd10:K72.1%' 
			OR T3.dx_code_id ilike 'icd10:K72.9%' 
			OR T3.dx_code_id ilike 'icd10:K76.5%' 
			OR T3.dx_code_id ilike 'icd10:K76.6%' 
			OR T3.dx_code_id ilike 'icd10:K76.7%' 
			OR T3.dx_code_id ilike 'icd10:C77.%' 
			OR T3.dx_code_id ilike 'icd10:C78.%' 
			OR T3.dx_code_id ilike 'icd10:C79.%' 
			OR T3.dx_code_id ilike 'icd10:C80.%' 
			OR T3.dx_code_id ilike 'icd10:B20.%' 
			OR T3.dx_code_id ilike 'icd10:B21.%' 
			OR T3.dx_code_id ilike 'icd10:B22.%' 
			OR T3.dx_code_id ilike 'icd10:B24.%' 
		)
	GROUP BY T1.patient_id
;

--cci_compute AS (
	DROP TABLE IF EXISTS kre_report.lc_1_newdates_cci_compute;
	CREATE TABLE kre_report.lc_1_newdates_cci_compute AS
	SELECT T1.patient_id, coalesce((cci_chf + cci_dementia + cci_chron_pulm_dis + cci_rheaumatic_dis + cci_mild_liver_dis + 
						   cci_diab_w_compl + cci_hemip_or_parap + cci_renal_dis + cci_any_malig + cci_mod_or_severe_liver_disease +
						   cci_met_solid_tumor + cci_aids_hiv), 0) as cci_score
	--FROM index_pats T1
	FROM kre_report.lc_1_newdates_index_pats T1
	--LEFT JOIN cci_icds T2 ON (T1.patient_id = T2.patient_id)
	LEFT JOIN kre_report.lc_1_newdates_cci_icds T2 ON (T1.patient_id = T2.patient_id)
;

--symptom_by_pat AS (
	DROP TABLE IF EXISTS kre_report.lc_1_newdates_symptom_by_pat;
	CREATE TABLE kre_report.lc_1_newdates_symptom_by_pat AS
	SELECT T1.patient_id,
	index_date,
	MAX(CASE 
	    when cci_score = 0 then 'NONE'
		when cci_score >= 1 and cci_score <= 2 then 'MILD'
		when cci_score >= 3 and cci_score <= 4 then 'MODERATE'
		when cci_score >= 5 then 'HIGH'
		ELSE 'NONE'
		END ) as cci, 
	coalesce(exclude_from_fui_one, 0) as exclude_from_fui_one,
	CASE   
		when date_part('year', age(index_date, date_of_birth)) <= 9 then '0-9'    
		when date_part('year', age(index_date, date_of_birth)) <= 19 then '10-19'  
		when date_part('year', age(index_date, date_of_birth)) <= 29 then '20-29' 
		when date_part('year', age(index_date, date_of_birth)) <= 39 then '30-39' 
		when date_part('year', age(index_date, date_of_birth)) <= 49 then '40-49'   
		when date_part('year', age(index_date, date_of_birth)) <= 59 then '50-59'  
		when date_part('year', age(index_date, date_of_birth)) <= 69 then '60-69' 
		when date_part('year', age(index_date, date_of_birth)) <= 79 then '70-79' 
		when date_of_birth is null then 'UNKNOWN AGE'
		else '>= 80' 
		END age_group_10_yr,
	CASE 
	    WHEN sex is null then 'U'
		WHEN sex in ('M', 'MALE') then 'M'
		WHEN sex in ('F', 'FEMALE') then 'F'
		ELSE sex
		END sex,
	CASE 
		when race_ethnicity = 6 then 'hispanic'  
		when race_ethnicity = 5 then 'white' 
		when race_ethnicity = 3 then 'black' 
		when race_ethnicity = 2 then 'asian' 
		when race_ethnicity = 1 then 'native_american' 
		when race_ethnicity = 0 then 'unknown' 
		when race_ethnicity is null then 'unknown'
	END race,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id = 'icd10:R50.9' then 1 else 0 END) as fever_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_fui_one,
        MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_or_taste_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
	    and dx_code_id = 'icd10:R19.7' 
		then 1 else 0 END) as diarrhea_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2') )
		then 1 else 0 END) as muscle_pain_fui_one,
    MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
	    and dx_code_id ilike 'icd10:R11%' 
		then 1 else 0 END) as nausea_or_vomit_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_fui_one,		
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_fui_one,				
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:R07.1', 'icd10:R07.2', 'icd10:R07.9')
			 OR dx_code_id ilike 'icd10:R07.8%')
		then 1 else 0 END) as chest_pain_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_fui_one,
    MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
     	and dx_code_id ilike 'icd10:M25.5%' 
		then 1 else 0 END) as arthralgia_fui_one,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_fui_one, 
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R63.1')
		then 1 else 0 END) as excessive_thirst_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:F52.0', 'icd10:F52.4', 'icd10:F52.9')
			 OR dx_code_id ilike 'icd10:F52.2%'
			 OR dx_code_id ilike 'icd10:F52.3%')
		then 1 else 0 END) as sexdesire_or_cap_fui_one,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id ilike 'icd10:F42.2%')
		then 1 else 0 END) as dizziness_fui_one				
	--FROM index_pats T1
	FROM kre_report.lc_1_newdates_index_pats T1
	--LEFT JOIN pat_fui_one_exclusions T2 ON (T1.patient_id = T2.patient_id)
	LEFT JOIN kre_report.lc_1_newdates_pat_fui_one_exclusions T2 ON (T1.patient_id = T2.patient_id)
	--LEFT JOIN pat_fui_two_exclusions T3 ON (T1.patient_id = T3.patient_id)
	--LEFT JOIN all_icd_and_fever T4 ON (T1.patient_id = T4.patient_id)
	LEFT JOIN kre_report.lc_1_newdates_all_icd_and_fever T4 ON (T1.patient_id = T4.patient_id)
	LEFT JOIN public.emr_patient T5 ON (T1.patient_id = T5.id )
	LEFT JOIN esp_mdphnet.esp_demographic T6 on (T5.natural_key = T6.patid)
	--LEFT JOIN cci_compute T9 ON (T1.patient_id = T9.patient_id)
	LEFT JOIN kre_report.lc_1_newdates_cci_compute T9 ON (T1.patient_id = T9.patient_id)
	GROUP BY T1.patient_id, age_group_10_yr, sex, race_ethnicity, index_date, exclude_from_fui_one;

--long_covid_output AS (
	DROP TABLE IF EXISTS kre_report.lc_1_newdates_output;
	CREATE TABLE kre_report.lc_1_newdates_output AS
	select age_group_10_yr,
	sex, 
	race,
	cci,
	count(distinct(patient_id)) total_patients,
	count(distinct(case when (fatigue_fui_one > 0) then patient_id else null end)) fatigue_any_time,
	count(distinct(case when (fever_fui_one > 0 ) then patient_id else null end)) fever_any_time,
	count(distinct(case when (dyspnea_fui_one > 0) then patient_id else null end)) dyspnea_any_time,
	count(distinct(case when (cough_fui_one > 0) then patient_id else null end)) cough_any_time,
	count(distinct(case when (headache_fui_one > 0) then patient_id else null end)) headache_any_time,
	count(distinct(case when (changes_in_smell_or_taste_fui_one > 0) then patient_id else null end)) changes_in_smell_or_taste_any_time,
	count(distinct(case when (diarrhea_fui_one > 0 ) then patient_id else null end)) diarrhea_any_time,
	count(distinct(case when (ab_pain_fui_one > 0) then patient_id else null end)) ab_pain_any_time,
	count(distinct(case when (muscle_pain_fui_one > 0) then patient_id else null end)) muscle_pain_any_time,
	count(distinct(case when (nausea_or_vomit_fui_one > 0) then patient_id else null end)) nausea_or_vomit_any_time,
	count(distinct(case when (sore_throat_fui_one > 0) then patient_id else null end)) sore_throat_any_time,
	count(distinct(case when (back_pain_fui_one > 0 ) then patient_id else null end)) back_pain_any_time,
	count(distinct(case when (other_pain_fui_one > 0 ) then patient_id else null end)) other_pain_any_time,
	count(distinct(case when (pins_needles_fui_one > 0 ) then patient_id else null end)) pins_needles_any_time,
	count(distinct(case when (chest_pain_fui_one > 0 ) then patient_id else null end)) chest_pain_any_time,
	count(distinct(case when (palpitations_fui_one > 0 ) then patient_id else null end)) palpitations_any_time,
	count(distinct(case when (arthralgia_fui_one > 0 ) then patient_id else null end)) arthralgia_any_time,
	count(distinct(case when (diff_think_concen_fui_one > 0) then patient_id else null end)) diff_think_concen_any_time,
	count(distinct(case when (excessive_thirst_fui_one > 0) then patient_id else null end)) excessive_thirst_any_time,
	count(distinct(case when (sexdesire_or_cap_fui_one > 0) then patient_id else null end)) sexdesire_or_cap_any_time,
	count(distinct(case when (dizziness_fui_one > 0) then patient_id else null end)) dizziness_any_time,
	sum(fatigue_fui_one) fatigue_fui_one,
	sum(fever_fui_one) fever_fui_one,
	sum(dyspnea_fui_one) dyspnea_fui_one,
	sum(cough_fui_one) cough_fui_one,
	sum(headache_fui_one) headache_fui_one,
	sum(changes_in_smell_or_taste_fui_one) changes_in_smell_or_taste_fui_one,
	sum(diarrhea_fui_one) diarrhea_fui_one,
	sum(ab_pain_fui_one) ab_pain_fui_one,
	sum(muscle_pain_fui_one) muscle_pain_fui_one,
	sum(nausea_or_vomit_fui_one) nausea_or_vomit_fui_one,
	sum(sore_throat_fui_one) sore_throat_fui_one,
	sum(back_pain_fui_one) back_pain_fui_one,
	sum(other_pain_fui_one) other_pain_fui_one,
	sum(pins_needles_fui_one) pins_needles_fui_one,
	sum(chest_pain_fui_one) chest_pain_fui_one,
	sum(palpitations_fui_one) palpitations_fui_one,	
	sum(arthralgia_fui_one) arthralgia_fui_one,
	sum(diff_think_concen_fui_one) diff_think_concen_fui_one,
	sum(excessive_thirst_fui_one) excessive_thirst_fui_one,
	sum(sexdesire_or_cap_fui_one) sexdesire_or_cap_fui_one,
	sum(dizziness_fui_one) dizziness_fui_one,
	count(distinct(case when (fatigue_fui_one  +  
	                          fever_fui_one  +  
				  dyspnea_fui_one +   
				  cough_fui_one + 
				  headache_fui_one +
				  changes_in_smell_or_taste_fui_one +  
				  diarrhea_fui_one +
				  ab_pain_fui_one +   
				  muscle_pain_fui_one + 
				  nausea_or_vomit_fui_one + 
				  sore_throat_fui_one + 
				  back_pain_fui_one + 
				  other_pain_fui_one +
				  pins_needles_fui_one +
				  chest_pain_fui_one + 
				  palpitations_fui_one + 
				  arthralgia_fui_one +  
				  diff_think_concen_fui_one +
 				  excessive_thirst_fui_one +
 				  sexdesire_or_cap_fui_one +
 			          dizziness_fui_one 
				) = 0 
		  then patient_id else null end)) zero_all_symp_any_time,
  	count(distinct(case when (fatigue_fui_one + 
	                          fever_fui_one + 
				  dyspnea_fui_one + 
				  cough_fui_one + 
				  headache_fui_one + 
				  changes_in_smell_or_taste_fui_one + 
				  diarrhea_fui_one + 
				  ab_pain_fui_one + 
				  muscle_pain_fui_one + 
				  nausea_or_vomit_fui_one + 
				  sore_throat_fui_one + 
				  back_pain_fui_one + 
				  other_pain_fui_one + 
				  pins_needles_fui_one + 
				  chest_pain_fui_one + 
				  palpitations_fui_one + 
				  arthralgia_fui_one + 
				  diff_think_concen_fui_one +
				  excessive_thirst_fui_one +
 				  sexdesire_or_cap_fui_one +
 			          dizziness_fui_one 
				  ) = 1 
		  then patient_id else null end)) one_all_symp_any_time,
	count(distinct(case when (fatigue_fui_one + 
	                          fever_fui_one + 
				  dyspnea_fui_one + 
				  cough_fui_one + 
				  headache_fui_one + 
				  changes_in_smell_or_taste_fui_one + 
				  diarrhea_fui_one + 
				  ab_pain_fui_one + 
				  muscle_pain_fui_one + 
				  nausea_or_vomit_fui_one +
				  sore_throat_fui_one + 
				  back_pain_fui_one + 
				  other_pain_fui_one + 
				  pins_needles_fui_one + 
				  chest_pain_fui_one + 
				  palpitations_fui_one +
				  arthralgia_fui_one + 
				  diff_think_concen_fui_one +
				  excessive_thirst_fui_one +
 				  sexdesire_or_cap_fui_one +
 			          dizziness_fui_one 
				  ) = 2 
		  then patient_id else null end)) two_all_symp_any_time,
	count(distinct(case when (fatigue_fui_one + 
	                          fever_fui_one + 
				  dyspnea_fui_one + 
				  cough_fui_one + 
				  headache_fui_one + 
				  changes_in_smell_or_taste_fui_one +
				  diarrhea_fui_one +
				  ab_pain_fui_one + 
				  muscle_pain_fui_one + 
				  nausea_or_vomit_fui_one +
				  sore_throat_fui_one + 
				  back_pain_fui_one + 
				  other_pain_fui_one + 
				  pins_needles_fui_one + 
				  chest_pain_fui_one + 
				  palpitations_fui_one + 
				  arthralgia_fui_one + 
				  diff_think_concen_fui_one +
				  excessive_thirst_fui_one +
 				  sexdesire_or_cap_fui_one +
 			          dizziness_fui_one 
				  ) = 3
		  then patient_id else null end)) three_all_symp_any_time,	
	count(distinct(case when (fatigue_fui_one + 
	                          fever_fui_one + 
				  dyspnea_fui_one +
				  cough_fui_one + 
				  headache_fui_one + 
				  changes_in_smell_or_taste_fui_one + 
				  diarrhea_fui_one + 
				  ab_pain_fui_one + 
				  muscle_pain_fui_one + 
				  nausea_or_vomit_fui_one + 
				  sore_throat_fui_one + 
				  back_pain_fui_one +
				  other_pain_fui_one + 
				  pins_needles_fui_one + 
				  chest_pain_fui_one + 
				  palpitations_fui_one + 
				  arthralgia_fui_one + 
				  diff_think_concen_fui_one +
				  excessive_thirst_fui_one +
 				  sexdesire_or_cap_fui_one +
 			          dizziness_fui_one 
				  ) >= 4 
		  then patient_id else null end)) four_plus_all_symp_any_time,		
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_or_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one +
							  excessive_thirst_fui_one + sexdesire_or_cap_fui_one + dizziness_fui_one) = 0 
		  then patient_id else null end)) zero_all_symp_fui_one,
   	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_or_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one +
							  excessive_thirst_fui_one + sexdesire_or_cap_fui_one + dizziness_fui_one) = 1 
		  then patient_id else null end)) one_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_or_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one +
							  excessive_thirst_fui_one + sexdesire_or_cap_fui_one + dizziness_fui_one) = 2 
		  then patient_id else null end)) two_all_symp_fui_one,
  	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_or_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one + 
							  excessive_thirst_fui_one + sexdesire_or_cap_fui_one + dizziness_fui_one) = 3 
		  then patient_id else null end)) three_all_symp_fui_one,
   	count(distinct(case when (fatigue_fui_one + fever_fui_one + dyspnea_fui_one + cough_fui_one + 
							  headache_fui_one + changes_in_smell_or_taste_fui_one + 
							  diarrhea_fui_one + ab_pain_fui_one + muscle_pain_fui_one + nausea_or_vomit_fui_one + 
							  sore_throat_fui_one + back_pain_fui_one + other_pain_fui_one + pins_needles_fui_one + 
							  chest_pain_fui_one + palpitations_fui_one + arthralgia_fui_one + diff_think_concen_fui_one +
							  excessive_thirst_fui_one + sexdesire_or_cap_fui_one + dizziness_fui_one) >= 4 
		  then patient_id else null end)) four_plus_all_symp_fui_one,
    count(distinct(case when (exclude_from_fui_one > 0 ) then patient_id else null end)) reinfected_any_time,
    sum(exclude_from_fui_one) reinfected_fui_one
    --FROM symptom_by_pat					
	FROM kre_report.lc_1_newdates_symptom_by_pat
	GROUP BY age_group_10_yr, sex, race, cci;

--SELECT * from long_covid_output ORDER by sex, age_group_10_yr, race, cci;
SELECT * from kre_report.lc_1_newdates_output ORDER by sex, age_group_10_yr, race, cci;

