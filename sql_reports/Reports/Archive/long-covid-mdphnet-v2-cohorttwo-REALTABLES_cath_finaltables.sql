	CREATE TABLE kre_report.lc_symptom_by_pat_cath AS
	SELECT T1.patient_id,
	index_date,
	MAX(CASE 
	    when cci_score = 0 then 'NONE'
		when cci_score >= 1 and cci_score <= 2 then 'MILD'
		when cci_score >= 3 and cci_score <= 4 then 'MODERATE'
		when cci_score >= 5 then 'HIGH'
		ELSE 'NONE'
		END ) as cci, 
	coalesce(max(pos_in_fui_one), 0) as pos_in_fui_one,
	max(case when pos_in_fui_one = 1 then 0
		when pos_in_fui_two is null then 0
		else pos_in_fui_two end) pos_in_fui_two,

	max(case when pos_in_fui_one = 1 then 0
		when pos_in_fui_two = 1 then 0
		when pos_in_fui_three is null then 0
		else pos_in_fui_three end) pos_in_fui_three,
		
	max(case when pos_in_fui_one = 1 then 0
		when pos_in_fui_two = 1 then 0
		when pos_in_fui_three = 1 then 0
		when pos_in_fui_four is null then 0
		else pos_in_fui_four end) pos_in_fui_four,
	CASE   
		when date_part('year', age(index_date, date_of_birth)) <= 9 then '0-9'    
		when date_part('year', age(index_date, date_of_birth)) <= 19 then '10-19'  
		when date_part('year', age(index_date, date_of_birth)) <= 29 then '20-29' 
		when date_part('year', age(index_date, date_of_birth)) <= 39 then '30-39' 
		when date_part('year', age(index_date, date_of_birth)) <= 49 then '40-49'   
		when date_part('year', age(index_date, date_of_birth)) <= 59 then '50-59'  
		when date_part('year', age(index_date, date_of_birth)) <= 69 then '60-69' 
		when date_part('year', age(index_date, date_of_birth)) <= 79 then '70-79' 
		when date_of_birth is null then 'UNKNOWN AGE'
		else '>= 80' 
		END age_group_10_yr,
	CASE 
	    WHEN sex is null then 'U'
		WHEN sex in ('M', 'MALE') then 'M'
		WHEN sex in ('F', 'FEMALE') then 'F'
		ELSE sex
		END sex,
	CASE 
		when race_ethnicity = 6 then 'hispanic'  
		when race_ethnicity = 5 then 'white' 
		when race_ethnicity = 3 then 'black' 
		when race_ethnicity = 2 then 'asian' 
		when race_ethnicity = 1 then 'native_american' 
		when race_ethnicity = 0 then 'unknown' 
		when race_ethnicity is null then 'unknown'
	END race,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
			 OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null 
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
			 OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null 
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
		 	OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null 
		and (dx_code_id = 'icd10:G93.3' OR dx_code_id ilike 'icd10:R53.8%' 
			 OR dx_code_id ilike 'icd10:R53.1%')
		then 1 else 0 END) as fatigue_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id = 'icd10:R50.9' then 1 else 0 END) as fever_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null and dx_code_id = 'icd10:R50.9'	then 1 else 0 END) as fever_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null and dx_code_id = 'icd10:R50.9'	then 1 else 0 END) as fever_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null and dx_code_id = 'icd10:R50.9'	then 1 else 0 END) as fever_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null and dx_code_id ilike 'icd10:R06.0%' then 1 else 0 END) as dyspnea_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id ilike 'icd10:R05%' or dx_code_id = 'icd10:R09.82')
		then 1 else 0 END) as cough_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_fui_two,	
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null and dx_code_id = 'icd10:R51.9' then 1 else 0 END) as headache_fui_four,
    MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_fui_one,	
    MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_fui_two,	
    MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_fui_three,		
    MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and dx_code_id in ('icd10:R43.0', 'icd10:R43.1', 'icd10:R43.8', 'icd10:R43.9')
		then 1 else 0 END) as changes_in_smell_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9') 
		then 1 else 0 END) as changes_in_taste_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9') 
		then 1 else 0 END) as changes_in_taste_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9') 
		then 1 else 0 END) as changes_in_taste_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and dx_code_id in ('icd10:R43.2', 'icd10:R43.8', 'icd10:R43.9') 
		then 1 else 0 END) as changes_in_taste_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
	    and dx_code_id = 'icd10:R19.7' 
		then 1 else 0 END) as diarrhea_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
	    and dx_code_id = 'icd10:R19.7' 
		then 1 else 0 END) as diarrhea_fui_two,	
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
	    and dx_code_id = 'icd10:R19.7' 
		then 1 else 0 END) as diarrhea_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
	    and dx_code_id = 'icd10:R19.7' 
		then 1 else 0 END) as diarrhea_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null 
		and ( dx_code_id in ('icd10:R10.9', 'icd10:R10.84') 
			  or dx_code_id ilike 'icd10:R10.1%'
			  or dx_code_id ilike 'icd10:R10.3%' )
			  then 1 else 0 END ) as ab_pain_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2') )
		then 1 else 0 END) as muscle_pain_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2') )
		then 1 else 0 END) as muscle_pain_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2') )
		then 1 else 0 END) as muscle_pain_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id ilike 'icd10:M79.1%' 
			OR dx_code_id ilike 'icd10:M62.83%' 
			OR dx_code_id in ('icd10:R25.2') )
		then 1 else 0 END) as muscle_pain_fui_four,
    MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
	    and dx_code_id ilike 'icd10:R11%' 
		then 1 else 0 END) as nausea_or_vomit_fui_one,
    MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null 
	    and dx_code_id ilike 'icd10:R11%' 
		then 1 else 0 END) as nausea_or_vomit_fui_two,
    MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null 
	    and dx_code_id ilike 'icd10:R11%' 
		then 1 else 0 END) as nausea_or_vomit_fui_three,
    MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null 
	    and dx_code_id ilike 'icd10:R11%' 
		then 1 else 0 END) as nausea_or_vomit_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null 
		and dx_code_id in ('icd10:R07.0', 'icd10:R09.82') 
		then 1 else 0 END) as sore_throat_fui_four,		
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id in ('icd10:M54.2', 'icd10:M54.50', 'icd10:M54.59', 'icd10:M54.6', 'icd10:M54.9')
			 OR dx_code_id ilike 'icd10:M54.8%')
		then 1 else 0 END) as back_pain_fui_four,
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id in ('icd10:G89.29', 'icd10:G89.4', 'icd10:R52')
			 OR dx_code_id ilike 'icd10:M79.6%')
		then 1 else 0 END) as other_pain_fui_four,				
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_fui_two,		
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id in ('icd10:G64', 'icd10:R29.90')
			 OR dx_code_id ilike 'icd10:R20%')
		then 1 else 0 END) as pins_needles_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:R07.1', 'icd10:R07.2', 'icd10:R07.9')
			 OR dx_code_id ilike 'icd10:R07.8%')
		then 1 else 0 END) as chest_pain_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id in ('icd10:R07.1', 'icd10:R07.2', 'icd10:R07.9')
			 OR dx_code_id ilike 'icd10:R07.8%')
		then 1 else 0 END) as chest_pain_fui_two,	
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id in ('icd10:R07.1', 'icd10:R07.2', 'icd10:R07.9')
			 OR dx_code_id ilike 'icd10:R07.8%')
		then 1 else 0 END) as chest_pain_fui_three,				
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id in ('icd10:R07.1', 'icd10:R07.2', 'icd10:R07.9')
			 OR dx_code_id ilike 'icd10:R07.8%')
		then 1 else 0 END) as chest_pain_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_fui_one,
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_fui_two,
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_fui_three,
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and (dx_code_id in ('icd10:R00.2', 'icd10:R00.0')
			 OR dx_code_id ilike 'icd10:I47%')
		then 1 else 0 END) as palpitations_fui_four,
    MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
     	and dx_code_id ilike 'icd10:M25.5%' 
		then 1 else 0 END) as arthralgia_fui_one,
    MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
     	and dx_code_id ilike 'icd10:M25.5%' 
		then 1 else 0 END) as arthralgia_fui_two,
    MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
     	and dx_code_id ilike 'icd10:M25.5%' 
		then 1 else 0 END) as arthralgia_fui_three,
    MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
     	and dx_code_id ilike 'icd10:M25.5%' 
		then 1 else 0 END) as arthralgia_fui_four,	
	MAX(CASE WHEN icd_date >= fui_one_start_date and icd_date < fui_one_end_date and exclude_from_fui_one is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_fui_one,	
	MAX(CASE WHEN icd_date >= fui_two_start_date and icd_date < fui_two_end_date and exclude_from_fui_two is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_fui_two,	
	MAX(CASE WHEN icd_date >= fui_three_start_date and icd_date < fui_three_end_date and exclude_from_fui_three is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_fui_three,	
	MAX(CASE WHEN icd_date >= fui_four_start_date and icd_date < fui_four_end_date and exclude_from_fui_four is null
		and dx_code_id in ('icd10:R41.840', 'icd10:R41.89', 'icd10:R41.3')
		then 1 else 0 END) as diff_think_concen_fui_four				
	--FROM index_pats_cath T1
	FROM kre_report.lc_index_pats_cath T1
	--LEFT JOIN pat_fui_one_exclusions T2 ON (T1.patient_id = T2.patient_id)
	LEFT JOIN kre_report.lc_pat_fui_one_exclusions_cath T2 ON (T1.patient_id = T2.patient_id)
	--LEFT JOIN pat_fui_two_exclusions T3 ON (T1.patient_id = T3.patient_id)
	LEFT JOIN kre_report.lc_pat_fui_two_exclusions_cath T3 ON (T1.patient_id = T3.patient_id)
	--LEFT JOIN pat_fui_three_exclusions T7 ON (T1.patient_id = T7.patient_id)
	LEFT JOIN kre_report.lc_pat_fui_three_exclusions_cath T7 ON (T1.patient_id = T7.patient_id)
	--LEFT JOIN pat_fui_four_exclusions T8 ON (T1.patient_id = T8.patient_id)
	LEFT JOIN kre_report.lc_pat_fui_four_exclusions_cath T8 ON (T1.patient_id = T8.patient_id)
	--LEFT JOIN all_icd_and_fever T4 ON (T1.patient_id = T4.patient_id)
	LEFT JOIN kre_report.lc_all_icd_and_fever_cath T4 ON (T1.patient_id = T4.patient_id)
	LEFT JOIN public.emr_patient T5 ON (T1.patient_id = T5.id )
	LEFT JOIN esp_mdphnet.esp_demographic T6 on (T5.natural_key = T6.patid)
	--LEFT JOIN cci_compute T9 ON (T1.patient_id = T9.patient_id)
	LEFT JOIN kre_report.lc_cci_compute_cath T9 ON (T1.patient_id = T9.patient_id)
	GROUP BY T1.patient_id, age_group_10_yr, sex, race_ethnicity, index_date; --, exclude_from_fui_one,	exclude_from_fui_two, exclude_from_fui_three, exclude_from_fui_four;

	
    DROP TABLE IF EXISTS kre_report.lc_output_cath;
	CREATE TABLE kre_report.lc_output_cath AS
	select age_group_10_yr,
	sex, 
	race,
	cci,
	count(distinct(patient_id)) total_patients,
    --FROM symptom_by_pat					
	FROM kre_report.lc_symptom_by_pat_cath
	GROUP BY age_group_10_yr, sex, race, cci;
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	

			
