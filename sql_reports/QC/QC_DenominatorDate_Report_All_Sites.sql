


SET client_min_messages TO WARNING;


-- # of encounters (all encounter types)
-- KRE: 04/21 Removed join to static_enc_type_lookup table
drop table IF EXISTS temp_all_enc;
create table temp_all_enc as select count(*) raw_enc_count, date_trunc('month', date)::date datemonth 
from emr_encounter e
where date >= '01-01-2010' 
and date <= now()::date  
group by  datemonth  order by datemonth asc;

-- # of encounters (MDPH encounter types)
drop table IF EXISTS temp_mdph_enc;
create table temp_mdph_enc as select count(*) mdph_enc_count, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s 
where date >= '01-01-2010' 
and date <= now()::date  
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by  datemonth  order by datemonth asc;

-- # of encounters (clin_enc encounter types)
drop table IF EXISTS temp_clinenc_enc;
create table temp_clinenc_enc as select count(*) clinenc_enc_count, date_trunc('month', date)::date datemonth 
from gen_pop_tools.clin_enc
where date >= '01-01-2010' 
and date <= now()::date  
and source = 'enc'
group by  datemonth  order by datemonth asc;


-- # combine raw, rs-mdph, and clin_enc encounter tables
drop table IF EXISTS temp_allencounters;
create table temp_allencounters as select r.raw_enc_count, m.mdph_enc_count, c.clinenc_enc_count, r.datemonth
from temp_all_enc r,temp_mdph_enc m, temp_clinenc_enc c
where r.datemonth = m.datemonth
and r.datemonth = c.datemonth;

-- # cleanup constituent tables
drop table IF EXISTS temp_all_enc;
drop table IF EXISTS temp_mdph_enc;
drop table IF EXISTS temp_clinenc_enc;

-- # of laboratory results (all values)
-- KRE: 04/21 changed this to use the date field instead of the result_date so it would better align with clin_enc
-- For MLCHC -- use the same lab date logic that is used in gen_pop_tools.clin_enc function 
-- Using a django table that we don't use to identify the site
drop table IF EXISTS temp_alllabresults;
create table temp_alllabresults as 
select count(*) countlabres, 
case when domain = 'mlchc' then date_trunc('month', coalesce(collection_date::date, result_date::date, date))
     else date_trunc('month', date) END datemonth 
from emr_labresult l
left join public.django_site on (domain in ('mlchc'))
where 
case when domain = 'mlchc' then coalesce(collection_date::date, result_date::date, date) >= '01-01-2010' and coalesce(collection_date::date, result_date::date, date) <= now()::date
     else date >= '01-01-2010' and date <= now()::date end
group by datemonth  order by datemonth asc;

-- difference in lab counts from prior month
drop table IF EXISTS temp_lab_count_diff;
create table temp_lab_count_diff as
select (countlabres - prev_month_count) lab_diff_from_prior_month, datemonth::date, prev_month
from temp_alllabresults T1,
(select 
	countlabres prev_month_count, current_month, prev_month
	from temp_alllabresults T1,
	(select datemonth::date current_month, (datemonth::date - interval '1 month')::date prev_month
	from temp_alllabresults
	) T2 
	where T1.datemonth::date = prev_month) T2
where T1.datemonth::date = T2.current_month;


--# of prescriptions (all Rxs)
drop table IF EXISTS temp_allrx;
create table temp_allrx as select count(*) countrx, date_trunc('month', date)::date datemonth 
from emr_prescription 
where date >= '01-01-2010' 
and date <= now()::date  
group by datemonth  order by datemonth asc;

--difference in rx counts from prior month
drop table IF EXISTS temp_rx_count_diff;
create table temp_rx_count_diff as
select (countrx - prev_month_count) rx_diff_from_prior_month, datemonth::date, prev_month
from temp_allrx T1,
(select 
	countrx prev_month_count, current_month, prev_month
	from temp_allrx T1,
	(select datemonth::date current_month, (datemonth::date - interval '1 month')::date prev_month
	from temp_allrx) T2
	where T1.datemonth::date = prev_month) T2
where T1.datemonth::date = T2.current_month;

--# of immunizations (all immunizations)
drop table IF EXISTS temp_allimmun;
create table temp_allimmun as select count(*) countimm, date_trunc('month', date)::date datemonth 
from emr_immunization 
where date >= '01-01-2010' 
and date <= now()::date  
group by datemonth  order by datemonth asc;

--difference in immunization counts from prior month
drop table IF EXISTS temp_immun_count_diff;
create table temp_immun_count_diff as
select (countimm - prev_month_count) imm_diff_from_prior_month, datemonth::date, prev_month
from temp_allimmun T1,
(select 
	countimm prev_month_count, current_month, prev_month
	from temp_allimmun T1,
	(select datemonth::date current_month, (datemonth::date - interval '1 month')::date prev_month
	from temp_allimmun) T2
	where T1.datemonth::date = prev_month) T2
where T1.datemonth::date = T2.current_month;

--# of all MDPH encounters where weight is populated
drop table IF EXISTS temp_hasweight_mdph;
create table temp_hasweight_mdph as select count(*) countweight_mdph, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s 
where date >= '01-01-2010' 
and date <= now()::date  
and weight is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by datemonth  order by datemonth asc;


-- KRE: 04/21 changed this to be clinical encounters versus ambulatory.
-- First getting unique patients by date as clinical encounters only counts one source per date
drop table IF EXISTS temp_hasweight_clinenc_enc;
create table temp_hasweight_clinenc_enc as
select count(patient_id) countweight_clinenc_enc, date_trunc('month', date)::date datemonth 
FROM (
	select distinct e.patient_id, e.date 
	from emr_encounter e
	join gen_pop_tools.clin_enc c on (e.patient_id = c.patient_id and e.date = c.date and c.source = 'enc' )
	where e.date >= '01-01-2010' 
    and e.date <= now()::date 
	and weight is not null) weight_per_pat_per_date
group by datemonth  order by datemonth asc;

--# combine rs-mdph and clin_enc weight tables
-- KRE: 04/21 changed from ambulatory to clin_enc
drop table IF EXISTS temp_hasweight;
create table temp_hasweight as select m.countweight_mdph, c.countweight_clinenc_enc, c.datemonth
from temp_hasweight_mdph m, temp_hasweight_clinenc_enc c 
where m.datemonth = c.datemonth;

--# cleanup constituent tables
drop table IF EXISTS temp_hasweight_mdph;
drop table IF EXISTS temp_hasweight_clinenc_enc;

-- % of encounters where weight is populated
-- KRE: changed to reference clinical encounter number as the denom for all encounters with a weight.
drop table IF EXISTS temp_hasweight_pct;
create table temp_hasweight_pct as 
select round((countweight_mdph::decimal / mdph_enc_count::decimal) * 100, 2)::text || '%' as mdph_weightpct, round((countweight_clinenc_enc::decimal / clinenc_enc_count::decimal) * 100, 2)::text || '%' as clinenc_enc_weightpct, e.datemonth 
from temp_allencounters e, temp_hasweight h
where e.datemonth = h.datemonth
order by datemonth asc;

--# of all MDPH encounters where height is populated
drop table IF EXISTS temp_hasheight_mdph;
create table temp_hasheight_mdph as select count(*) countheight_mdph, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s 
where date >= '01-01-2010' 
and date <= now()::date  
and height is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by datemonth  order by datemonth asc;

-- KRE: 04/21 changed this to be clinical encounters versus ambulatory.
-- First getting unique patients by date as clinical encounters only counts one source per date
drop table IF EXISTS temp_hasheight_clinenc_enc;
create table temp_hasheight_clinenc_enc as
select count(patient_id) countheight_clinenc_enc, date_trunc('month', date)::date datemonth 
FROM (
	select distinct e.patient_id, e.date 
	from emr_encounter e
	join gen_pop_tools.clin_enc c on (e.patient_id = c.patient_id and e.date = c.date and c.source = 'enc' )
	where e.date >= '01-01-2010' 
    and e.date <= now()::date 
	and height is not null) height_per_pat_per_date
group by datemonth  order by datemonth asc;


--# combine rs-mdph and clinenc_enc height tables
drop table IF EXISTS temp_hasheight;
create table temp_hasheight as select m.countheight_mdph, c.countheight_clinenc_enc, c.datemonth
from temp_hasheight_mdph m, temp_hasheight_clinenc_enc c 
where m.datemonth = c.datemonth;

--# cleanup constituent tables
drop table IF EXISTS temp_hasheight_mdph;
drop table IF EXISTS temp_hasheight_clinenc_enc;

--% of encounters where height is populated
drop table IF EXISTS temp_hasheight_pct;
create table temp_hasheight_pct as 
select round((countheight_mdph::decimal / mdph_enc_count::decimal) * 100, 2)::text || '%' as mdph_heightpct, round((countheight_clinenc_enc::decimal / clinenc_enc_count::decimal) * 100, 2)::text || '%' as clinenc_enc_heightpct, e.datemonth 
from temp_allencounters e, temp_hasheight h
where e.datemonth = h.datemonth
order by datemonth asc;

--# of all MDPH encounters where bmi is populated
drop table IF EXISTS temp_hasbmi_mdph;
create table temp_hasbmi_mdph as select count(*) countbmi_mdph, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s 
where date >= '01-01-2010' 
and date <= now()::date  
and bmi is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by datemonth  order by datemonth asc;

--# of clinical encounters where bmi is populated
drop table IF EXISTS temp_hasbmi_clinenc_enc;
create table temp_hasbmi_clinenc_enc as
select count(patient_id) countbmi_clinenc_enc, date_trunc('month', date)::date datemonth 
FROM (
	select distinct e.patient_id, e.date 
	from emr_encounter e
	join gen_pop_tools.clin_enc c on (e.patient_id = c.patient_id and e.date = c.date and c.source = 'enc' )
	where e.date >= '01-01-2010' 
    and e.date <= now()::date 
	and bmi is not null) bmi_per_pat_per_date
group by datemonth  order by datemonth asc;


--# combine rs-mdph and clinical bmi tables
drop table IF EXISTS temp_hasbmi;
create table temp_hasbmi as select m.countbmi_mdph, c.countbmi_clinenc_enc, c.datemonth
from temp_hasbmi_mdph m, temp_hasbmi_clinenc_enc c 
where m.datemonth = c.datemonth;

--# cleanup constituent tables
drop table IF EXISTS temp_hasbmi_mdph;
drop table IF EXISTS temp_hasbmi_clinenc_enc;


--% of encounters where bmi is populated
drop table IF EXISTS temp_hasbmi_pct;
create table temp_hasbmi_pct as 
select round((countbmi_mdph::decimal / mdph_enc_count::decimal) * 100, 2)::text || '%' as mdph_bmipct, round((countbmi_clinenc_enc::decimal / clinenc_enc_count::decimal) * 100, 2)::text || '%' as clinenc_enc_bmipct, e.datemonth 
from temp_allencounters e, temp_hasbmi h
where e.datemonth = h.datemonth
order by datemonth asc;


--# of all MDPH encounters where temp is populated
drop table IF EXISTS temp_hastemp_mdph;
create table temp_hastemp_mdph as select count(*) counttemp_mdph, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s 
where date >= '01-01-2010' 
and date <= now()::date  
and temperature is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by datemonth  order by datemonth asc;

--# of clinical encounters where temp is populated
drop table IF EXISTS temp_hastemp_clinenc_enc;
create table temp_hastemp_clinenc_enc as
select count(patient_id) counttemp_clinenc_enc, date_trunc('month', date)::date datemonth 
FROM (
	select distinct e.patient_id, e.date 
	from emr_encounter e
	join gen_pop_tools.clin_enc c on (e.patient_id = c.patient_id and e.date = c.date and c.source = 'enc' )
	where e.date >= '01-01-2010' 
    and e.date <= now()::date 
	and temperature is not null) temp_per_pat_per_date
group by datemonth  order by datemonth asc;


--# combine rs-mdph and clinical temp tables
drop table IF EXISTS temp_hastemp;
create table temp_hastemp as select m.counttemp_mdph, c.counttemp_clinenc_enc, c.datemonth
from temp_hastemp_mdph m, temp_hastemp_clinenc_enc c
where m.datemonth = c.datemonth;

--# cleanup constituent tables
drop table IF EXISTS temp_hastemp_mdph;
drop table IF EXISTS temp_hastemp_clinenc_enc;

--% of encounters where temp is populated
drop table IF EXISTS temp_hastemp_pct;
create table temp_hastemp_pct as 
select round((counttemp_mdph::decimal / mdph_enc_count::decimal) * 100, 2)::text || '%' as mdph_temppct, round((counttemp_clinenc_enc::decimal / clinenc_enc_count::decimal) * 100, 2)::text || '%' as clinenc_enc_temppct, e.datemonth 
from temp_allencounters e, temp_hastemp h
where e.datemonth = h.datemonth
order by datemonth asc;


--# of patients who have non null tobacco use records in a month
drop table IF EXISTS temp_patientswithtobuse;
create table temp_patientswithtobuse as select count(distinct patient_id) countpatientstobuse, date_trunc('month',date) datemonth from emr_socialhistory
where date >= '01-01-2010' 
and date <= now()::date
and tobacco_use is not null  
and tobacco_use != ''
group by  datemonth  order by datemonth asc;

--# of distinct patients patients with >=1 encounter during the month (MDPH filter)
drop table IF EXISTS temp_patientswithvisits_mdph;
create table temp_patientswithvisits_mdph as select count(distinct patient_id) countpatients_mdph, date_trunc('month',date) datemonth 
from emr_encounter e, static_enc_type_lookup s
where date >= '01-01-2010' 
and date <= now()::date  
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.rs_mdphnet = 1)
group by datemonth  order by datemonth asc;

--# of distinct patients patients with >=1 clinical "enc" encounter during the month 
drop table IF EXISTS temp_patientswithvisits_clinenc_enc;
create table temp_patientswithvisits_clinenc_enc as select count(distinct patient_id) countpatients_clinenc_enc, date_trunc('month',date) datemonth 
from gen_pop_tools.clin_enc
where date >= '01-01-2010' 
and date <= now()::date 
and source = 'enc'
group by datemonth  order by datemonth asc;

--# combine rs-mdph and clinical enc temp tables
drop table IF EXISTS temp_patientswithvisits;
create table temp_patientswithvisits as select m.countpatients_mdph, c.countpatients_clinenc_enc, c.datemonth
from temp_patientswithvisits_mdph m, temp_patientswithvisits_clinenc_enc c 
where m.datemonth = c.datemonth;

--# cleanup constituent tables
drop table IF EXISTS temp_patientswithvisits_mdph;
drop table IF EXISTS temp_patientswithvisits_clinenc_enc;


-- The "gap" analysis is to find months where the clinical encounter script 
-- needs to be re-run for records that arrive outside of the normal range 
-- that the script runs.

-- Identify labs where a clin_enc record does not exist
-- For MLCHC -- use the same lab date logic that is used in gen_pop_tools.clin_enc function 
-- Using a django table that we don't use to identify the site
drop table IF EXISTS temp_clinenc_lab_gap;
create table temp_clinenc_lab_gap as
select count(*) as clinenc_lab_gap, date_trunc('month', date)::date datemonth 
from (
	select distinct patient_id, case when domain = 'mlchc' then coalesce(collection_date::date, result_date::date, date)
         else date end as date
	from emr_labresult
	left join public.django_site on (domain in ('mlchc'))
	where case when domain = 'mlchc' then coalesce(collection_date::date, result_date::date, date) >= '01-01-2010' and coalesce(collection_date::date, result_date::date, date) <= now()::date
          else date >= '01-01-2010' and date <= now()::date end
	except 
	select patient_id, date
	from gen_pop_tools.clin_enc
	where date >= '01-01-2010' 
	and date <= now()::date 
	and source = 'lx') sub1
group by datemonth order by datemonth asc;



-- Identify encounters where a clin_enc record does not exist
-- For MLCHC -- use the same lab date logic that is used in gen_pop_tools.clin_enc function (excludes dental)
drop table IF EXISTS temp_clinenc_enc_gap;
create table temp_clinenc_enc_gap as
select count(*) as clinenc_enc_gap, date_trunc('month', date)::date datemonth 
from (
    select distinct patient_id, date
    from emr_encounter e
	left join public.django_site on (domain in ('mlchc'))
    where 
	case when domain = 'mlchc' then raw_encounter_type not ilike '%dental%' else true end
	and date >= '01-01-2010' 
	and date <= now()::date 
	and ((e.weight>0 or e.height>0 or e.bp_systolic>0 or e.bp_diastolic>0 or e.temperature>0 or e.pregnant=TRUE or e.edd is not null)
         or exists (select null from emr_encounter_dx_codes dx where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9') )
    except
 	select patient_id, date
	from gen_pop_tools.clin_enc
	where date >= '01-01-2010' 
	and date <= now()::date 
	and source = 'enc') sub1
group by datemonth order by datemonth asc;


-- Identify prescriptions where a clin_enc record does not exist
drop table IF EXISTS temp_clinenc_rx_gap;
create table temp_clinenc_rx_gap as
select count(*) as clinenc_rx_gap, date_trunc('month', date)::date datemonth 
from (
	select distinct patient_id, date
	from emr_prescription
	where date >= '01-01-2010' 
	and date <= now()::date 
	except 
	select patient_id, date
	from gen_pop_tools.clin_enc
	where date >= '01-01-2010' 
	and date <= now()::date 
	and source = 'rx') sub1
group by  datemonth  order by datemonth asc;

-- Identify immunizations where a clin_enc record does not exist
drop table IF EXISTS temp_clinenc_imm_gap;
create table temp_clinenc_imm_gap as
select count(*) as clinenc_imm_gap, date_trunc('month', date)::date datemonth 
from (
	select distinct patient_id, date
	from emr_immunization
	where date >= '01-01-2010' 
	and date <= now()::date 
	except 
	select patient_id, date
	from gen_pop_tools.clin_enc
	where date >= '01-01-2010' 
	and date <= now()::date 
	and source = 'imu') sub1
group by  datemonth  order by datemonth asc;


-- This is to identify the overall number of patients 
-- where a clinical encounter record does not exist in 
-- a given month (for any source). 
-- Essentially the denominator impact for gaps in the clinical encounter records.

drop table if exists temp_clinenc_pat_gap_for_month;
create table temp_clinenc_pat_gap_for_month as
select count(*) as clinenc_pat_gap_for_month, datemonth 
from (
	select distinct patient_id, case when domain = 'mlchc' then date_trunc('month', coalesce(collection_date::date, result_date::date, date))::date
	        else date_trunc('month', date)::date end datemonth 
	from emr_labresult
	left join public.django_site on (domain in ('mlchc'))
	where case when domain = 'mlchc' then coalesce(collection_date::date, result_date::date, date) >= '01-01-2010' and coalesce(collection_date::date, result_date::date, date) <= now()::date
          else date >= '01-01-2010' and date <= now()::date end
	UNION
	select distinct patient_id, date_trunc('month', date)::date datemonth 
	from emr_prescription
	where date >= '01-01-2010' 
	and date <= now()::date
	UNION
	select distinct patient_id, date_trunc('month', date)::date datemonth 
	from emr_immunization
	where date >= '01-01-2010' 
	and date <= now()::date
	UNION
    select distinct patient_id, date_trunc('month', date)::date datemonth 
    from emr_encounter e
	left join public.django_site on (domain in ('mlchc'))
    where 
	case when domain = 'mlchc' then raw_encounter_type not ilike '%dental%' else true end
	and date >= '01-01-2010' 
	and date <= now()::date 
	and ((e.weight>0 or e.height>0 or e.bp_systolic>0 or e.bp_diastolic>0 or e.temperature>0 or e.pregnant=TRUE or e.edd is not null)
         or exists (select null from emr_encounter_dx_codes dx where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9') )
	EXCEPT
	select distinct patient_id, date_trunc('month', date)::date datemonth 
	from gen_pop_tools.clin_enc
	where date >= '01-01-2010' 
	and date <= now()::date  ) sub1
group by datemonth order by datemonth asc;


-- combine the clinical encounter "gap" tables
drop table IF EXISTS temp_clinenc_gaps;
create table temp_clinenc_gaps as 
select coalesce(l.clinenc_lab_gap, 0) as clinenc_lab_gap, 
coalesce(e.clinenc_enc_gap, 0) as clinenc_enc_gap,
coalesce(r.clinenc_rx_gap, 0) as clinenc_rx_gap, 
coalesce(i.clinenc_imm_gap, 0) as clinenc_imm_gap, 
coalesce(p.clinenc_pat_gap_for_month, 0) as clinenc_pat_gap_for_month,
a.datemonth
from temp_allencounters a
left join temp_clinenc_lab_gap l on (a.datemonth = l.datemonth)
left join temp_clinenc_rx_gap r on (a.datemonth = r.datemonth)
left join temp_clinenc_imm_gap i on (a.datemonth = i.datemonth)
left join temp_clinenc_enc_gap e on (a.datemonth = e.datemonth)
left join temp_clinenc_pat_gap_for_month p on (a.datemonth = p.datemonth)
order by a.datemonth;


--# cleanup constituent tables
drop table if exists temp_clinenc_lab_gap;
drop table if exists temp_clinenc_enc_gap; 
drop table if exists temp_clinenc_rx_gap; 
drop table if exists temp_clinenc_imm_gap;
drop table if exists temp_clinenc_pat_gap_for_month;



drop table IF EXISTS qc_denominator_data;
create table qc_denominator_data as 
select enc.datemonth::date, raw_enc_count, mdph_enc_count, clinenc_enc_count, countlabres, lab_diff_from_prior_month, countrx, rx_diff_from_prior_month, countimm, imm_diff_from_prior_month, countweight_mdph, mdph_weightpct, countweight_clinenc_enc, clinenc_enc_weightpct, countheight_mdph, mdph_heightpct, countheight_clinenc_enc, clinenc_enc_heightpct, countbmi_mdph, mdph_bmipct, countbmi_clinenc_enc, clinenc_enc_bmipct, counttemp_mdph, mdph_temppct, counttemp_clinenc_enc, clinenc_enc_temppct, countpatientstobuse, countpatients_mdph, countpatients_clinenc_enc,
clinenc_lab_gap, clinenc_enc_gap, clinenc_rx_gap, clinenc_imm_gap, clinenc_pat_gap_for_month
from temp_allencounters enc
join temp_alllabresults lab 
on lab.datemonth = enc.datemonth
join temp_lab_count_diff labc
on labc.datemonth = enc.datemonth
join temp_allrx rx 
on rx.datemonth = enc.datemonth 
join temp_rx_count_diff rxc
on rxc.datemonth = enc.datemonth
join temp_allimmun imm
on imm.datemonth = enc.datemonth
join temp_immun_count_diff immc
on immc.datemonth = enc.datemonth
join temp_hasweight wt
on wt.datemonth = enc.datemonth
join temp_hasweight_pct wtp
on wtp.datemonth = enc.datemonth
join temp_hasheight ht
on ht.datemonth = enc.datemonth 
join temp_hasheight_pct htp
on htp.datemonth = enc.datemonth 
join temp_hasbmi bmi
on bmi.datemonth = enc.datemonth 
join temp_hasbmi_pct bmip
on bmip.datemonth = enc.datemonth 
join temp_hastemp temp
on temp.datemonth = enc.datemonth 
join temp_hastemp_pct tempp
on tempp.datemonth = enc.datemonth 
join temp_patientswithvisits patwithvis
on patwithvis.datemonth = enc.datemonth
join temp_patientswithtobuse
on temp_patientswithtobuse.datemonth = enc.datemonth
left join temp_clinenc_gaps
on temp_clinenc_gaps.datemonth = enc.datemonth;

-- drop table IF EXISTS temp_allencounters;
-- drop table IF EXISTS temp_alllabresults;
-- drop table IF EXISTS temp_allrx;
-- drop table IF EXISTS temp_allimmun;
-- drop table IF EXISTS temp_immun_count_diff;
-- drop table IF EXISTS temp_hasweight;
-- drop table IF EXISTS temp_hasheight;
-- drop table IF EXISTS temp_hasbmi;
-- drop table IF EXISTS temp_hastemp;
-- drop table IF EXISTS temp_patientswithtobuse;
-- drop table IF EXISTS temp_patientswithvisits;
-- drop table IF EXISTS temp_hasweight_pct;
-- drop table IF EXISTS temp_hasheight_pct;
-- drop table IF EXISTS temp_hasbmi_pct;
-- drop table IF EXISTS temp_hastemp_pct;
-- drop table IF EXISTS temp_lab_count_diff;
-- drop table IF EXISTS temp_rx_count_diff;
-- drop table IF EXISTS temp_clinenc_lab_gap;
-- drop table IF EXISTS temp_clinenc_gaps;

select * from qc_denominator_data;


