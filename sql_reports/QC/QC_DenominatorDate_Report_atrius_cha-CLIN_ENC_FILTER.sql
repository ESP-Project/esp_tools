--# of encounters (clinical encounters)
drop table IF EXISTS temp_allencounters;
create table temp_allencounters as select count(*) countenc, date_trunc('month', date)::date datemonth 
from emr_patient p, gen_pop_tools.clin_enc ce
where date >= '01-01-2010' 
and date <= now()::date  
and  p.id = ce.patient_id
group by  datemonth  order by datemonth asc;

--# of laboratory results (all values; use results date)
drop table IF EXISTS temp_alllabresults;
create table temp_alllabresults as select count(*) countlabres, date_trunc('month', result_date) datemonth 
from emr_labresult 
where result_date >= '01-01-2010' 
and result_date <= now()::date 
group by datemonth  order by datemonth asc;


--difference in lab counts from prior month
drop table IF EXISTS temp_lab_count_diff;
create table temp_lab_count_diff as
select (countlabres - prev_month_count) lab_diff_from_prior_month, datemonth::date, prev_month
from temp_alllabresults T1,
(select 
	countlabres prev_month_count, current_month, prev_month
	from temp_alllabresults T1,
	(select datemonth::date current_month, (datemonth::date - interval '1 month')::date prev_month
	from temp_alllabresults
	) T2
	where T1.datemonth::date = prev_month) T2
where T1.datemonth::date = T2.current_month;


--# of prescriptions (all Rxs)
drop table IF EXISTS temp_allrx;
create table temp_allrx as select count(*) countrx, date_trunc('month', date)::date datemonth 
from emr_prescription 
where date >= '01-01-2010' 
and date <= now()::date  
group by datemonth  order by datemonth asc;

--difference in rx counts from prior month
drop table IF EXISTS temp_rx_count_diff;
create table temp_rx_count_diff as
select (countrx - prev_month_count) rx_diff_from_prior_month, datemonth::date, prev_month
from temp_allrx T1,
(select 
	countrx prev_month_count, current_month, prev_month
	from temp_allrx T1,
	(select datemonth::date current_month, (datemonth::date - interval '1 month')::date prev_month
	from temp_allrx
	) T2
	where T1.datemonth::date = prev_month) T2
where T1.datemonth::date = T2.current_month;

--# of clinical encounters with a source of "encounter" where weight is populated
drop table IF EXISTS temp_hasweight;
create table temp_hasweight as select count(*) countweight, date_trunc('month', date)::date datemonth 
from emr_encounter e,  gen_pop_tools.clin_enc ce
where source = 'enc'
and e.patient_id = ce.patient_id
and e.date = ce.date
and e.date >= '01-01-2010' 
and e.date <= now()::date  
and weight is not null and weight != ''
group by datemonth  order by datemonth asc;

--% of encounters where weight is populated
drop table IF EXISTS temp_hasweight_pct;
create table temp_hasweight_pct as 
select round((countweight::decimal / countenc::decimal) * 100, 2)::text || '%' as weightpct,
e.datemonth 
from temp_allencounters e, temp_hasweight h
where e.datemonth = h.datemonth
order by datemonth asc;

--# of encounters where height is populated
drop table IF EXISTS temp_hasheight;
create table temp_hasheight as select count(*) countheight, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s
where date >= '01-01-2010' 
and date <= now()::date  
and height is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.ambulatory = 1)
group by datemonth  order by datemonth asc;

--% of encounters where height is populated
drop table IF EXISTS temp_hasheight_pct;
create table temp_hasheight_pct as 
select round((countheight::decimal / countenc::decimal) * 100, 2)::text || '%' as heightpct,
e.datemonth 
from temp_allencounters e, temp_hasheight h
where e.datemonth = h.datemonth
order by datemonth asc;

--# of encounters where bmi is populated
drop table IF EXISTS temp_hasbmi;
create table temp_hasbmi as select count(*) countbmi, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s
where date >= '01-01-2010' 
and date <= now()::date  
and bmi is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.ambulatory = 1)
group by datemonth  order by datemonth asc;

--% of encounters where bmi is populated
drop table IF EXISTS temp_hasbmi_pct;
create table temp_hasbmi_pct as 
select round((countbmi::decimal / countenc::decimal) * 100, 2)::text || '%' as bmipct,
e.datemonth 
from temp_allencounters e, temp_hasbmi h
where e.datemonth = h.datemonth
order by datemonth asc;


--# of encounters where temp is populated
drop table IF EXISTS temp_hastemp;
create table temp_hastemp as select count(*) counttemp, date_trunc('month', date)::date datemonth 
from emr_encounter e, static_enc_type_lookup s
where date >= '01-01-2010' 
and date <= now()::date  
and temperature is not null 
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.ambulatory = 1)
group by datemonth  order by datemonth asc;

--% of encounters where temp is populated
drop table IF EXISTS temp_hastemp_pct;
create table temp_hastemp_pct as 
select round((counttemp::decimal / countenc::decimal) * 100, 2)::text || '%' as temppct,
e.datemonth 
from temp_allencounters e, temp_hastemp h
where e.datemonth = h.datemonth
order by datemonth asc;

--# of patients who have non null tobacco use records in a month
drop table IF EXISTS temp_patientswithtobuse;
create table temp_patientswithtobuse as select count(distinct patient_id) countpatientstobuse, date_trunc('month',date) datemonth from emr_socialhistory
where date >= '01-01-2010' 
and date <= now()::date
and tobacco_use is not null  group by  datemonth  order by datemonth asc;

--# of distinct patients patients with >=1 encounter during the month
drop table IF EXISTS temp_patientswithvisits;
create table temp_patientswithvisits as select count(distinct patient_id) countpatients, date_trunc('month',date) datemonth 
from emr_encounter e, static_enc_type_lookup s
where date >= '01-01-2010' 
and date <= now()::date  
and e.raw_encounter_type = s.raw_encounter_type 
and (e.raw_encounter_type is NULL or s.ambulatory = 1)
group by datemonth  order by datemonth asc;

drop table IF EXISTS qc_denominator_data;
create table qc_denominator_data as 
select enc.datemonth::date, countenc, countlabres, lab_diff_from_prior_month, countrx, rx_diff_from_prior_month, countweight, weightpct, countheight, heightpct, countbmi, bmipct, counttemp, temppct, countpatientstobuse, countpatients
from temp_allencounters enc
join temp_alllabresults lab 
on lab.datemonth = enc.datemonth
join temp_lab_count_diff labc
on labc.datemonth = enc.datemonth
join temp_allrx rx 
on rx.datemonth = enc.datemonth 
join temp_rx_count_diff rxc
on rxc.datemonth = enc.datemonth
join temp_hasweight wt
on wt.datemonth = enc.datemonth
join temp_hasweight_pct wtp
on wtp.datemonth = enc.datemonth
join temp_hasheight ht
on ht.datemonth = enc.datemonth 
join temp_hasheight_pct htp
on htp.datemonth = enc.datemonth 
join temp_hasbmi bmi
on bmi.datemonth = enc.datemonth 
join temp_hasbmi_pct bmip
on bmip.datemonth = enc.datemonth 
join temp_hastemp temp
on temp.datemonth = enc.datemonth 
join temp_hastemp_pct tempp
on tempp.datemonth = enc.datemonth 
join temp_patientswithvisits patwithvis
on patwithvis.datemonth = enc.datemonth
join temp_patientswithtobuse
on temp_patientswithtobuse.datemonth = enc.datemonth;

-- drop table IF EXISTS temp_allencounters;
-- drop table IF EXISTS temp_alllabresults;
-- drop table IF EXISTS temp_allrx;
-- drop table IF EXISTS temp_hasweight;
-- drop table IF EXISTS temp_hasheight;
-- drop table IF EXISTS temp_hasbmi;
-- drop table IF EXISTS temp_hastemp;
-- drop table IF EXISTS temp_patientswithtobuse;
-- drop table IF EXISTS temp_patientswithvisits;
-- drop table IF EXISTS temp_hasweight_pct;
-- drop table IF EXISTS temp_hasheight_pct;
-- drop table IF EXISTS temp_hasbmi_pct;
-- drop table IF EXISTS temp_hastemp_pct;
-- drop table IF EXISTS temp_lab_count_diff;
-- drop table IF EXISTS temp_rx_count_diff;

select * from qc_denominator_data;


