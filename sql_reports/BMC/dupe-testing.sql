-- IDENTIFY DUPE PATIENTS
-- DUPES ARE 
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, ADDRESS1, and DOB MATCH
-- OR
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, SSN AND DOB MATCH 
-- ADDED MATCH FOR FIRST_NAME, LAST_NAME, DOB - BUT CAN BE REMOVED


-- first, name, last_name, and ssn 

drop table if exists kre_report.dupe_patients_index;
create table kre_report.dupe_patients_index as
select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, upper(address1) as addr_or_ssn, date_of_birth::date, 'address' as type_of_match
from emr_patient
where upper(last_name) not in ('UNKNOWN', 'UNK', 'TEST', 'UNKOWN', 'UNKNOW', 'UKNOWN', 'UNK.', 'U')
and upper(last_name) not ilike ('UNKNOWN%')
and upper(first_name) not in ('UNKNOWN', 'STUDY', 'UNK')
and upper(address1) not in ('UNKNOWN')
and ( upper(first_name) not in ('JOHN') and upper(last_name) not in ('DOE') )
group by upper(first_name), upper(last_name), upper(address1), date_of_birth::date
having count(*) > 1



UNION
select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, ssn as addr_or_ssn, date_of_birth::date, 'ssn' as type_of_match
from emr_patient
where ssn not in ('999-99-9999')
and ssn not ilike ('000%')
group by upper(first_name), upper(last_name), ssn, date_of_birth::date
having count(*) >1


UNION
select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, date_of_birth::date::text as addr_or_ssn, date_of_birth::date, 'dob' as type_of_match
from emr_patient
where upper(last_name) not in ('UNKNOWN', 'UNK', 'TEST', 'UNKOWN', 'UNKNOW', 'UKNOWN', 'UNK.', 'U')
and upper(last_name) not ilike ('UNKNOWN%')
and upper(first_name) not in ('UNKNOWN', 'STUDY', 'UNK')
and ( upper(first_name) not in ('JOHN') and upper(last_name) not in ('DOE') )
and date_of_birth >= '1901-01-05'::date
group by  upper(first_name), upper(last_name), date_of_birth::date
having count(*) > 1;









-- just want to do for dob matches
-- trying to find rows that should be dropped
-- ssn from one does not match the other
select T1.*
from kre_report.dupe_patients_details_test T1
JOIN kre_report.dupe_patients_index T2 on (T1.last_name = T2.last_name and T1.first_name = T2.first_name and T1.date_of_birth::date = T2.date_of_birth)
JOIN kre_report.dupe_patients_details_test T3 on (T1.last_name = T3.last_name and T1.first_name = T3.first_name and T1.date_of_birth::date = T3.date_of_birth::date)
where 
T2.type_of_match = 'dob'
AND T1.ssn != T3.ssn
AND T1.ssn not in ('000-00-0000', '999-99-9999')
AND T3.ssn not in ('000-00-0000', '999-99-9999')
order by T1.last_name, T1.first_name

