--Duplicate Identification:
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, ADDRESS1, and DOB MATCH
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, SSN AND DOB MATCH 
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, DOB MATCH (**Patients that match on first, last, and dob, but have a valid SSN that does NOT match are not included)

--Selection of which to keep and which to drop
-- If more than a month between create and update and updated witihin the last year, keep the most recently updated id
-- If it has been less than a month since the id was created use the lowest/oldest id.

--Processing
-- Only process if the patient_id to be dropped has hef_events associated.

--Deletions/Tracking
-- Patients will be processed if they meet the criteria above, however the patient record will only be deleted from emr_patient if they have not been previously deleted within the last 35 days.
-- This is to prevent churn and allow time to re-match on selection if initial choice of patient_id to keep or delete was incorrect.


-- Run these once to create infrastructure tables.

-- CREATE TABLE kre_report.dupe_patients_history
-- (
    -- kept_pat_id integer NOT NULL,
    -- deleted_pat_id integer NOT NULL,
    -- kept_mrn character varying COLLATE pg_catalog."default",
    -- deleted_mrn character varying COLLATE pg_catalog."default",
    -- date_processed date NOT NULL,
    -- CONSTRAINT dupe_patients_history_pkey PRIMARY KEY (kept_pat_id, deleted_pat_id, date_processed)
-- );

-- ALTER TABLE kre_report.dupe_patients_history
    -- OWNER to esp;
	
drop table if exists kre_report.dupe_patients_index;
drop table if exists kre_report.dupe_patients_details;
drop table if exists kre_report.dupe_patients_pats_to_keep_1;
drop table if exists kre_report.dupe_patients_pats_to_keep_2;
drop table if exists kre_report.dupe_patients_pats_to_keep_union;
drop table if exists kre_report.dupe_patients_id_keep_drop_prep;
drop table if exists kre_report.dupe_patients_id_to_process;
drop table if exists kre_report.dupe_patients_keep_cases_to_delete;
drop table if exists kre_report.dupe_patients_hef_nodis_to_delete;


-- IDENTIFY DUPE PATIENTS
-- DUPES ARE 
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, ADDRESS1, and DOB MATCH
-- OR
-- PATIENTS WHERE FIRST_NAME, LAST_NAME, SSN AND DOB MATCH 
-- ADDED MATCH FOR FIRST_NAME, LAST_NAME, DOB - BUT CAN BE REMOVED

drop table if exists kre_report.dupe_patients_index;
create table kre_report.dupe_patients_index as
select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, upper(address1) as addr_or_ssn, date_of_birth::date, 'address' as type_of_match
from emr_patient
where upper(last_name) not in ('UNKNOWN', 'UNK', 'TEST', 'UNKOWN', 'UNKNOW', 'UKNOWN', 'UNK.', 'U')
and upper(last_name) not ilike ('UNKNOWN%')
and upper(first_name) not in ('UNKNOWN', 'STUDY', 'UNK')
and upper(address1) not in ('UNKNOWN')
and ( upper(first_name) not in ('JOHN') and upper(last_name) not in ('DOE') )
group by upper(first_name), upper(last_name), upper(address1), date_of_birth::date
having count(*) > 1
UNION
select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, ssn as addr_or_ssn, date_of_birth::date, 'ssn' as type_of_match
from emr_patient
where ssn not in ('999-99-9999')
and ssn not ilike ('000%')
group by upper(first_name), upper(last_name), ssn, date_of_birth::date
having count(*) >1;
-- COMMENTING OUT MATCH ON FIRST NAME, LAST NAME, AND DOB AS IT IS NOT SPECIFIC ENOUGH
-- UNION
-- select count(*) as pat_count, upper(first_name) first_name, upper(last_name) last_name, date_of_birth::date::text as addr_or_ssn, date_of_birth::date, 'dob' as type_of_match
-- from emr_patient
-- where upper(last_name) not in ('UNKNOWN', 'UNK', 'TEST', 'UNKOWN', 'UNKNOW', 'UKNOWN', 'UNK.', 'U')
-- and upper(last_name) not ilike ('UNKNOWN%')
-- and upper(first_name) not in ('UNKNOWN', 'STUDY', 'UNK')
-- and ( upper(first_name) not in ('JOHN') and upper(last_name) not in ('DOE') )
-- and date_of_birth >= '1901-01-05'::date
-- group by  upper(first_name), upper(last_name), date_of_birth::date
-- having count(*) > 1;


-- GET ADDITIONAL DETAILS FOR DUPLICATE PATIENTS 
drop table if exists kre_report.dupe_patients_details;
create table kre_report.dupe_patients_details as
select DISTINCT id, mrn, created_timestamp, 
updated_timestamp, 
upper(T1.first_name) first_name, 
upper(T1.last_name) last_name, 
T1.date_of_birth,
T1.ssn
FROM emr_patient T1
INNER JOIN kre_report.dupe_patients_index T2 on 
	(upper(T1.first_name) = T2.first_name 
	and upper(T1.last_name) = T2.last_name 
	and (upper(T1.address1) = T2.addr_or_ssn or T1.ssn = T2.addr_or_ssn or T1.date_of_birth::date::text = T2.addr_or_ssn)
	and T1.date_of_birth::date = T2.date_of_birth::date)
GROUP BY id, mrn, created_timestamp, updated_timestamp, T1.first_name, T1.last_name, T1.date_of_birth
ORDER BY last_name, first_name, date_of_birth;

-- DON"T INCLUDE PATIENTS THAT HAVE A NAME/DOB MATCH BUT HAVE DIFFERENT SSN's
DELETE FROM kre_report.dupe_patients_details
WHERE id in (
    -- These are the patients that match on first_name, last_name, dob, BUT NOT SSN
	select T1.id
	from kre_report.dupe_patients_details T1
	JOIN kre_report.dupe_patients_index T2 on (T1.last_name = T2.last_name and T1.first_name = T2.first_name and T1.date_of_birth::date = T2.date_of_birth)
	JOIN kre_report.dupe_patients_details T3 on (T1.last_name = T3.last_name and T1.first_name = T3.first_name and T1.date_of_birth::date = T3.date_of_birth::date)
	where 
	T2.type_of_match = 'dob'
	AND T1.ssn != T3.ssn
	AND T1.ssn not in ('000-00-0000', '999-99-9999')
	AND T3.ssn not in ('000-00-0000', '999-99-9999')
	order by T1.last_name, T1.first_name
);


-- if more than a month between create and update and updated witihin the last year, keep the most recently updated id
drop table if exists kre_report.dupe_patients_pats_to_keep_1;
create table kre_report.dupe_patients_pats_to_keep_1 as
select id as pat_id_to_keep, mrn as mrn_to_keep, T1.first_name, T1.last_name, T1.date_of_birth
from kre_report.dupe_patients_details T1
INNER JOIN (select max(updated_timestamp) recent_timestamp, first_name, last_name, date_of_birth
from  kre_report.dupe_patients_details
where updated_timestamp >= created_timestamp + INTERVAL '1 month'
and   updated_timestamp >= now() - INTERVAL '1 year'
group by first_name, last_name, date_of_birth) T2 ON (T1.updated_timestamp = T2.recent_timestamp 
													and T1.first_name = T2.first_name
													and T1.last_name = T2.last_name
													and T1.date_of_birth = T2.date_of_birth);

-- if has been less than a month since the id was created use the lowest/oldest id
drop table if exists kre_report.dupe_patients_pats_to_keep_2;
create table kre_report.dupe_patients_pats_to_keep_2 as
select id as pat_id_to_keep, mrn as mrn_to_keep, T1.first_name, T1.last_name, T1.date_of_birth
from kre_report.dupe_patients_details T1
INNER JOIN (SELECT min(id) as lowest_id, first_name, last_name, date_of_birth
from kre_report.dupe_patients_details
where updated_timestamp < created_timestamp + INTERVAL '1 month'
and concat(first_name, last_name, date_of_birth) not in (select concat(first_name, last_name, date_of_birth) from kre_report.dupe_patients_pats_to_keep_1)
group by first_name, last_name, date_of_birth) T2 ON (T1.id = T2.lowest_id 
													and T1.first_name = T2.first_name
													and T1.last_name = T2.last_name
													and T1.date_of_birth = T2.date_of_birth);


-- bring together
drop table if exists kre_report.dupe_patients_pats_to_keep_union;
create table kre_report.dupe_patients_pats_to_keep_union as
select * from kre_report.dupe_patients_pats_to_keep_1
union
select * from kre_report.dupe_patients_pats_to_keep_2;


-- identify specific id's and mrns to keep and drop
drop table if exists kre_report.dupe_patients_id_keep_drop_prep;
create table kre_report.dupe_patients_id_keep_drop_prep as
select T2.pat_id_to_keep, T2.mrn_to_keep, 
case when id != T2.pat_id_to_keep then id end as pat_id_to_delete,
case when id != T2.pat_id_to_keep then mrn end as mrn_to_delete,
T1.first_name,
T2.last_name,
T2.date_of_birth
from kre_report.dupe_patients_details T1
LEFT JOIN kre_report.dupe_patients_pats_to_keep_union T2 ON (T1.first_name = T2.first_name
														  and T1.last_name = T2.last_name
														  and T1.date_of_birth = T2.date_of_birth);
		
		
-- clean up for processing
-- only process if patient to delete has events associated
-- only process if updated timestamp is not the same for both patients	
-- drop table if exists kre_report.dupe_patients_id_to_process;										  
-- create table kre_report.dupe_patients_id_to_process as 
-- select * 
-- FROM kre_report.dupe_patients_id_keep_drop_prep
-- where pat_id_to_delete is not null
-- and pat_id_to_delete in (select patient_id from hef_event);


-- clean up for processing
-- only process if patient to delete has events associated
-- only process if diff between updated timestamps is more than 7 days. This is to eliminate records that have not been merged at BMC (both are likely still in the extract) 12/2023
-- KRE --CHANGE THIS TO GEN_POP_TOOLS VS HEF_EVENT?!?!
drop table if exists kre_report.dupe_patients_id_to_process;										  
create table kre_report.dupe_patients_id_to_process as 
SELECT T1.*, T2.updated_timestamp::date as keep_updated_date, T3.updated_timestamp::date as delete_updated_date 
FROM kre_report.dupe_patients_id_keep_drop_prep T1
INNER JOIN 	kre_report.dupe_patients_details T2 ON (T2.id = pat_id_to_keep)
INNER JOIN 	kre_report.dupe_patients_details T3 ON (T3.id = pat_id_to_delete)
WHERE abs(T2.updated_timestamp::date - T3.updated_timestamp::date) >= 7
AND pat_id_to_delete is not null
and pat_id_to_delete in (select patient_id from hef_event);
--AND pat_id_to_delete in (select patient_id from gen_pop_tools.clin_enc);



-- -- don't process when both id's are still in the extract
-- -- or the id to keep has not been updated in the past year
-- drop table if exists kre_report.dupe_patients_id_to_process;										  
-- create table kre_report.dupe_patients_id_to_process as 
-- select DISTINCT T1.* 
-- from kre_report.dupe_patients_clean T1
-- INNER JOIN 	kre_report.dupe_patients_details T2 ON (T2.id = pat_id_to_keep)
-- INNER JOIN 	kre_report.dupe_patients_details T3 ON (T3.id = pat_id_to_delete)									 
-- -- both patients are still in the extract so don't try to fix those
-- WHERE T3.created_timestamp::date != T2.updated_timestamp::date	
-- AND T3.updated_timestamp::date != T2.updated_timestamp::date
-- -- don't update if the id to keep has not been updated in over a year
-- AND T2.updated_timestamp >= now() - INTERVAL '1 year'
-- -- if same entry has been processed within the last 30 days skip it
-- UNION
-- select T1.*
-- -- from kre_report.dupe_patients_id_to_process T1
-- from kre_report.dupe_patients_clean T1
-- INNER JOIN kre_report.dupe_patients_history T2 ON (pat_id_to_keep = kept_pat_id and mrn_to_delete = deleted_mrn)
-- WHERE date_processed >= now() + INTERVAL '30 days'
;


-- these are the patients that have the same condition under the "keep" patient_id as the "drop" patient_id
-- we need to delete the cases for these conditions for the keep patient
drop table if exists kre_report.dupe_patients_keep_cases_to_delete;
create table kre_report.dupe_patients_keep_cases_to_delete as 
select T2.patient_id, T2.id as case_id_to_delete, T1.pat_id_to_keep, T1.pat_id_to_delete, T2.condition as keep_condition, T3.condition as drop_condition
from kre_report.dupe_patients_id_to_process T1
JOIN nodis_case T2 ON (T1.pat_id_to_keep = T2.patient_id)
JOIN nodis_case T3 ON (T1.pat_id_to_delete = T3.patient_id)
AND T2.condition = T3.condition
order by pat_id_to_keep;

----------------------------------------------------------------------------
--THIS IS PROCESSING FOR CASES FOR "KEEP" PATIENTS THAT NEED TO BE DELETED 
---------------------------------------------------------------------------

-- First you need to delete all of the nodis_case_events for the case
delete from nodis_case_events 
where case_id in (select case_id_to_delete from kre_report.dupe_patients_keep_cases_to_delete);

-- Next delete the nodis_report_cases
delete from nodis_report_cases where case_id in (select case_id_to_delete from kre_report.dupe_patients_keep_cases_to_delete);

-- Next delete from nodis_reported
delete from nodis_reported where case_reported_id in 
	(select id from nodis_casereportreported  where case_report_id in 
		(select id from nodis_casereport T1, kre_report.dupe_patients_keep_cases_to_delete T2 
	 	where T1.case_id = T2.case_id_to_delete));

-- Next delete from nodis_casereportreported
delete from nodis_casereportreported 
where case_report_id in 
	(select id from nodis_casereport T1, kre_report.dupe_patients_keep_cases_to_delete T2 
	 where T1.case_id = T2.case_id_to_delete);

-- Next delete from nodis_report
delete from nodis_casereport 
where case_id in (select case_id_to_delete from kre_report.dupe_patients_keep_cases_to_delete);


-- Delete from nodis_caseactivehistory
delete from nodis_caseactivehistory
where case_id in (select case_id_to_delete from kre_report.dupe_patients_keep_cases_to_delete);


-- Next delete the cases
delete from nodis_case 
where id in (select case_id_to_delete from kre_report.dupe_patients_keep_cases_to_delete);



----------------------------------------------------
--THIS IS WHERE PROCESSING STARTS FOR THE DUPES
----------------------------------------------------

drop table if exists kre_report.dupe_patients_hef_nodis_to_delete;
create table kre_report.dupe_patients_hef_nodis_to_delete as
select T1.id as hef_id, T2.id as nce_id, T3.id as nodis_case, T1.patient_id
from hef_event T1
JOIN nodis_case_events T2 on (T1.id = T2.event_id)
JOIN nodis_case T3 on (T3.id = T2.case_id)
where T1.patient_id in (select pat_id_to_delete from kre_report.dupe_patients_id_to_process);

-- First you need to delete all of the nodis_case_events for the case
delete from nodis_case_events 
where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Next delete the nodis_report_cases
delete from nodis_report_cases where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Next delete from nodis_reported
delete from nodis_reported where case_reported_id in 
	(select id from nodis_casereportreported  where case_report_id in 
		(select id from nodis_casereport T1, kre_report.dupe_patients_hef_nodis_to_delete T2 
	 	where T1.case_id = T2.nodis_case));

-- Next delete from nodis_casereportreported
delete from nodis_casereportreported 
where case_report_id in 
	(select id from nodis_casereport T1, kre_report.dupe_patients_hef_nodis_to_delete T2 
	 where T1.case_id = T2.nodis_case);

-- Next delete from nodis_report
delete from nodis_casereport 
where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);


-- Delete from nodis_caseactivehistory
delete from nodis_caseactivehistory
where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Delete from nodis_casetimespans
delete from nodis_case_timespans
where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Delete from nodis_casestatushistory
delete from nodis_casestatushistory
where case_id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Next delete the cases
delete from nodis_case 
where id in (select nodis_case from kre_report.dupe_patients_hef_nodis_to_delete);

-- Next delete the COVID suspect "cases"
delete from covid19_report.suspect_nodis_case
where patient_id in (select pat_id_to_delete from kre_report.dupe_patients_id_to_process);


-- Delete hef timespan events tied to the patient that is to be deleted
delete from hef_timespan_events
where event_id in (select id from hef_event where patient_id in (select pat_id_to_delete from kre_report.dupe_patients_id_to_process));

-- Next delete ALL the hef_events. Not just ones associated to cases.
delete from hef_event 
where patient_id in (select pat_id_to_delete from kre_report.dupe_patients_id_to_process);

-- Update lab_results
UPDATE emr_labresult
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Update encounters
UPDATE emr_encounter
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Update prescriptions
UPDATE emr_prescription
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Update risk factors
UPDATE emr_risk_factors
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Update social_history
UPDATE emr_socialhistory
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Update emr_problem
UPDATE emr_problem
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;

-- Update emr_laborder
UPDATE emr_laborder
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;

--Update hef_timespan
UPDATE hef_timespan
SET patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;

--Update emr_allergy
UPDATE emr_allergy
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;

--Update emr_pregnancy
UPDATE emr_pregnancy
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;

--Update emr_immunization
UPDATE emr_immunization
SET mrn = mrn_to_keep,
patient_id = pat_id_to_keep
FROM kre_report.dupe_patients_id_to_process d
WHERE d.pat_id_to_delete = patient_id;


-- Now delete the old patient_id
-- but only if the mrn has not been deleted in the last 35 days
delete from emr_patient where id in
(select pat_id_to_delete 
	from kre_report.dupe_patients_id_to_process 
	where mrn_to_delete not in (select deleted_mrn from kre_report.dupe_patients_history where date_processed >= now() - INTERVAL '35 days'));


-- Insert in to a table that keeps track of "merged" patients
insert into kre_report.dupe_patients_history
(select pat_id_to_keep, pat_id_to_delete, mrn_to_keep, mrn_to_delete, now() from kre_report.dupe_patients_id_to_process 
		where mrn_to_delete not in (select deleted_mrn from kre_report.dupe_patients_history where date_processed >= now() - INTERVAL '35 days'));


-----------------------------------------------
-- NOW WE NEED TO RUN HEF & NODIS 
-----------------------------------------------







