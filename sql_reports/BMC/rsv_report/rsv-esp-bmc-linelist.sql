--All RSV labs since 2018 for patients less than 6 years old on lab test date
DROP TABLE IF EXISTS kre_report.rsv_all_rsv_labs_with_nat_key;
CREATE TABLE kre_report.rsv_all_rsv_labs_with_nat_key AS
SELECT DISTINCT T1.patient_id, 
T2.date as index_rsv_lab_test_date, 
date_of_birth::date as patient_dob, 
--age(T2.date, date_of_birth) age_on_test_date, 
ROUND((EXTRACT(epoch FROM (T2.date - date_of_birth)) / (3600 * 24))::numeric, 0) as age_on_test_date,
MAX(CASE WHEN name ilike '%pos%' THEN 1
     WHEN name ilike '%neg%' THEN 0
	 WHEN name ilike '%ind%' THEN -1
	 END) as index_rsv_lab_test_result, 
T1.order_natural_key as lab_nat_key,
T1.facility_provider_id
FROM emr_labresult T1
JOIN hef_event T2 ON (T1.patient_id = T2.patient_id and T1.id = T2.object_id)
JOIN emr_patient T3 ON (T1.patient_id = T3.id)
WHERE name ilike 'lx:rsv%'
AND T2.date >= '2018-01-01'
AND age(T2.date, date_of_birth) < '6 years'
GROUP BY T1.patient_id, index_rsv_lab_test_date, date_of_birth::date, age_on_test_date, T1.order_natural_key, T1.facility_provider_id;

DROP TABLE IF EXISTS kre_report.rsv_all_rsv_labs;
CREATE TABLE kre_report.rsv_all_rsv_labs AS
SELECT DISTINCT patient_id, 
index_rsv_lab_test_date, 
patient_dob, 
age_on_test_date,
max(index_rsv_lab_test_result) as index_rsv_lab_test_result
FROM kre_report.rsv_all_rsv_labs_with_nat_key
GROUP BY patient_id, index_rsv_lab_test_date, patient_dob, age_on_test_date;


-- Get all labs for the conditions/pathogens of interest
DROP TABLE IF EXISTS kre_report.rsv_other_pathogen_pos_tests;
CREATE TABLE kre_report.rsv_other_pathogen_pos_tests AS
SELECT DISTINCT T1.patient_id, date as pathogen_test_date, index_rsv_lab_test_date, abs(index_rsv_lab_test_date - date) as datediff, index_rsv_lab_test_result,
CASE WHEN name = 'lx:rsv:positive' AND T1.date = T2.index_rsv_lab_test_date THEN 'rsv'
     ELSE null END as condition,
name as event_name
FROM hef_event T1
JOIN kre_report.rsv_all_rsv_labs T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike 'lx:rsv:%'
      OR name ilike 'lx:parainfluenza:%'
	  OR name ilike 'lx:adenovirus:%'
	  OR name ilike 'lx:rhino_entero_virus:%'
	  OR name ilike 'lx:coronavirus_non19:%'
	  OR name ilike 'lx:influenza:%'
	  OR name ilike 'lx:influenza_culture:%'
	  OR name ilike 'lx:rapid_flu:%'
	  OR name ilike 'lx:h_metapneumovirus:%'
	  OR name ilike 'lx:m_pneumoniae_igm:%'
	  OR name ilike 'lx:m_pneumoniae_pcr:%'
	  OR name ilike 'lx:covid19_pcr:%'
	  OR name ilike 'lx:covid19_ag:%')
AND T1.date >= index_rsv_lab_test_date - INTERVAL '2 days'
AND T1.date <= index_rsv_lab_test_date + INTERVAL '2 days';


DROP TABLE IF EXISTS kre_report.rsv_other_pathogen_eval;
CREATE TABLE kre_report.rsv_other_pathogen_eval AS
SELECT DISTINCT patient_id, index_rsv_lab_test_date,
MAX(CASE WHEN condition = 'rsv' THEN 1 ELSE 0 END) as rsv_rav_case_lab_yn,
MAX(CASE WHEN event_name ilike 'lx:adenovirus:pos%' THEN 1 
         WHEN event_name ilike 'lx:adenovirus:neg%' THEN 0
		 WHEN event_name ilike 'lx:adenovirus:ind%' THEN -1 ELSE -2 END) as adenovirus_rav_pos_lab,
MAX(CASE WHEN event_name ilike 'lx:coronavirus_non19:pos%' THEN 1 
         WHEN event_name ilike 'lx:coronavirus_non19:neg%' THEN 0
         WHEN event_name ilike 'lx:coronavirus_non19:ind%' THEN -1 ELSE -2 END) as corona_non19_rav_pos_lab,
MAX(CASE WHEN (event_name ilike 'lx:covid19_pcr:pos%' or event_name ilike 'lx:covid19_ag:pos%') THEN 1 
         WHEN (event_name ilike 'lx:covid19_pcr:neg%%' or event_name ilike 'lx:covid19_ag:neg%') THEN 0
         WHEN (event_name ilike 'lx:covid19_pcr:ind%' or event_name ilike 'lx:covid19_ag:ind%') THEN -1  ELSE -2 END) as covid19_rav_pos_lab,
MAX(CASE WHEN event_name ilike 'lx:h_metapneumovirus:pos%' THEN 1 
         WHEN event_name ilike 'lx:h_metapneumovirus:neg%' THEN 0
		 WHEN event_name ilike 'lx:h_metapneumovirus:ind%' THEN -1 ELSE -2 END) as h_metap_rav_pos_lab,
MAX(CASE WHEN (event_name ilike 'lx:influenza:pos%' or event_name ilike 'lx:influenza_culture:pos%' or event_name ilike 'lx:rapid_flu:pos%') THEN 1 
         WHEN (event_name ilike 'lx:influenza:neg%' or event_name ilike 'lx:influenza_culture:neg%' or event_name ilike 'lx:rapid_flu:neg%') THEN 0
		 WHEN (event_name ilike 'lx:influenza:ind%' or event_name ilike 'lx:influenza_culture:ind%' or event_name ilike 'lx:rapid_flu:ind%') THEN -1 ELSE -2 END) as flu_rav_pos_lab,
MAX(CASE WHEN (event_name ilike 'lx:m_pneumoniae_igm:pos%' or event_name ilike 'lx:m_pneumoniae_pcr:pos%') THEN 1 
         WHEN (event_name ilike 'lx:m_pneumoniae_igm:neg%' or event_name ilike 'lx:m_pneumoniae_pcr:neg%') THEN 0
		 WHEN (event_name ilike 'lx:m_pneumoniae_igm:ind%' or event_name ilike 'lx:m_pneumoniae_pcr:ind%') THEN -1 ELSE -2 END) as mycoplas_rav_pos_lab,
MAX(CASE WHEN event_name ilike 'lx:parainfluenza:pos%' THEN 1 
         WHEN event_name ilike 'lx:parainfluenza:neg%' THEN 0
		 WHEN event_name ilike 'lx:parainfluenza:ind%' THEN -1 ELSE -2 END) as paraflu_rav_pos_lab,
MAX(CASE WHEN event_name ilike 'lx:rhino_entero_virus:pos%' THEN 1 
         WHEN event_name ilike 'lx:rhino_entero_virus:neg%' THEN 0
		 WHEN event_name ilike 'lx:rhino_entero_virus:ind%' THEN -1 ELSE -2 END) as rhino_entero_rav_pos_lab
FROM kre_report.rsv_other_pathogen_pos_tests
GROUP BY patient_id, index_rsv_lab_test_date;


-- Get all encounters that match one of the ravioli dx codes
DROP TABLE IF EXISTS kre_report.rsv_all_ravioli_dx;
CREATE TABLE kre_report.rsv_all_ravioli_dx AS
SELECT DISTINCT T2.patient_id, T2.date as encounter_date, T3.dx_code, T3.condition, index_rsv_lab_test_date, abs(index_rsv_lab_test_date - T2.date) as datediff,
index_rsv_lab_test_result
FROM emr_encounter_dx_codes T1
INNER JOIN emr_encounter T2 ON (T1.encounter_id = T2.id)
INNER JOIN public.ravioli_rpt_dx_codes T3 ON (T1.dx_code_id = T3.dx_code)
INNER JOIN kre_report.rsv_all_rsv_labs T4 ON (T2.patient_id = T4.patient_id)
WHERE 
T2.date >= index_rsv_lab_test_date - INTERVAL '2 days'
and T2.date <= index_rsv_lab_test_date + INTERVAL '2 days'
and T1.dx_code_id != 'icd9:799.9';

DROP TABLE IF EXISTS kre_report.rsv_dx_eval;
CREATE TABLE kre_report.rsv_dx_eval AS
SELECT DISTINCT patient_id, index_rsv_lab_test_date,
MAX(CASE WHEN condition = 'rsv' THEN 1 ELSE 0 END) as rsv_rav_case_dx_yn,
MAX(CASE WHEN condition = 'adenovirus' THEN 1 ELSE 0 END) as adenovirus_rav_dx,
MAX(CASE WHEN condition = 'coronavirus_non19' THEN 1 ELSE 0 END) as corona_non19_rav_dx,
MAX(CASE WHEN condition = 'covid19' THEN 1 ELSE 0 END) as covid19_rav_dx,
MAX(CASE WHEN condition = 'h_metapneumovirus' THEN 1 ELSE 0 END) as h_metap_rav_dx,
MAX(CASE WHEN condition = 'influenza' THEN 1 ELSE 0 END) as flu_rav_dx,
MAX(CASE WHEN condition = 'm_pneumoniae' THEN 1 ELSE 0 END) as mycoplas_rav_dx,
MAX(CASE WHEN condition = 'parainfluenza' THEN 1 ELSE 0 END) as paraflu_rav_dx,
MAX(CASE WHEN condition = 'rhino_entero_virus' THEN 1 ELSE 0 END) as rhino_entero_rav_dx,
MAX(CASE WHEN condition = 'ravioli_other' THEN 1 ELSE 0 END) as non_spec_rav_dx
FROM kre_report.rsv_all_ravioli_dx
GROUP BY patient_id, index_rsv_lab_test_date;

DROP TABLE IF EXISTS kre_report.rsv_lab_and_dx;
CREATE TABLE kre_report.rsv_lab_and_dx AS
SELECT T1.patient_id, T1.index_rsv_lab_test_date, 1::int as rsv_rav_case_lab_and_dx_yn
FROM kre_report.rsv_other_pathogen_eval T1
JOIN kre_report.rsv_dx_eval T2 ON (T1.patient_id = T2.patient_id and T1.index_rsv_lab_test_date = T2.index_rsv_lab_test_date)
WHERE rsv_rav_case_lab_yn = 1
AND rsv_rav_case_dx_yn = 1;
 

DROP TABLE IF EXISTS kre_report.rsv_rav_case_dates;
CREATE TABLE kre_report.rsv_rav_case_dates AS
SELECT patient_id, index_rsv_lab_test_date, min(lab_or_dx_date) as rsv_rav_case_date
FROM (
--INDEX DATE SHOULD ALWAYS MATCH POS TEST DATE (Don't use min date otherwise for pats with multiple + it will get confused)
	SELECT DISTINCT patient_id, index_rsv_lab_test_date, pathogen_test_date as lab_or_dx_date 
	FROM kre_report.rsv_other_pathogen_pos_tests WHERE condition = 'rsv'
	UNION
	SELECT patient_id, index_rsv_lab_test_date, min(encounter_date) as lab_or_dx_date 
	FROM kre_report.rsv_all_ravioli_dx WHERE condition = 'rsv'
	GROUP BY patient_id, index_rsv_lab_test_date
) T1
GROUP BY patient_id, index_rsv_lab_test_date;

DROP TABLE IF EXISTS kre_report.rsv_nirsev_rx;
CREATE TABLE kre_report.rsv_nirsev_rx AS
SELECT DISTINCT T1.patient_id, T1.index_rsv_lab_test_date, 1::int as nirsevimab_rx, count(distinct(T2.date)) as num_nirsevimab_rx_dates,
min(T2.date) as earliest_nirsevimab_rx_date, 
max(T2.date) as latest_nirsevimab_rx_date
FROM kre_report.rsv_all_rsv_labs T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike '%nirsevimab%' or name ilike '%Beyfortus%')
AND T2.date <= T1.index_rsv_lab_test_date
GROUP BY T1.patient_id, T1.index_rsv_lab_test_date;

DROP TABLE IF EXISTS kre_report.rsv_paliv_rx;
CREATE TABLE kre_report.rsv_paliv_rx AS
SELECT DISTINCT T1.patient_id, T1.index_rsv_lab_test_date, 1::int as palivizumab_rx, count(distinct(T2.date)) as num_palivizumab_rx_dates,
min(T2.date) as earliest_palivizumab_rx_date, 
max(T2.date) as latest_palivizumab_rx_date
FROM kre_report.rsv_all_rsv_labs T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike '%palivizumab%' or name ilike '%Synagis%')
AND T2.date <= T1.index_rsv_lab_test_date
GROUP BY T1.patient_id, T1.index_rsv_lab_test_date;

-- GET THE DEPT FOR INDEX LAB TEST
-- IF THERE ARE MULTIPLE TESTS WITH SAME RESULT ON THE SAME DATE THEN PICK LAST ONE BASED ON LARGEST NAT KEY
DROP TABLE IF EXISTS kre_report.rsv_test_dept;
CREATE TABLE kre_report.rsv_test_dept AS
SELECT S1.patient_id, S1.index_rsv_lab_test_date, max_lab_nat_key, S2.facility_provider_id, 
CASE WHEN dept is not null then dept ELSE 'UNKNOWN' END as index_test_dept_name
FROM (
	SELECT T1.patient_id, T1.index_rsv_lab_test_date, T1.index_rsv_lab_test_result, max(lab_nat_key) max_lab_nat_key
	FROM kre_report.rsv_all_rsv_labs T1
    JOIN kre_report.rsv_all_rsv_labs_with_nat_key T2 ON (T1.patient_id = T2.patient_id
                                                     AND T1.index_rsv_lab_test_date = T1.index_rsv_lab_test_date
													 AND T1.index_rsv_lab_test_result = T2.index_rsv_lab_test_result)
    GROUP BY T1.patient_id, T1.index_rsv_lab_test_date, T1.index_rsv_lab_test_result) S1
JOIN kre_report.rsv_all_rsv_labs_with_nat_key S2 ON (S1.max_lab_nat_key = lab_nat_key)
LEFT JOIN emr_provider S3 ON (S2.facility_provider_id = S3.id);

DROP TABLE IF EXISTS kre_report.rsv_pat_weight;
CREATE TABLE kre_report.rsv_pat_weight AS
SELECT S1.patient_id, S1.index_rsv_lab_test_date, ROUND(S2.weight::numeric, 1) as patient_weight
FROM 
	(SELECT T1.patient_id, T1.index_rsv_lab_test_date, max(T2.natural_key) max_nat_key
	FROM kre_report.rsv_all_rsv_labs T1
	JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.date <= T1.index_rsv_lab_test_date
	AND T2.weight is not null
	AND T2.date >= T1.index_rsv_lab_test_date - INTERVAL '6 months'
	GROUP BY T1.patient_id, T1.index_rsv_lab_test_date) S1
JOIN emr_encounter S2 ON (S1.max_nat_key = S2.natural_key);

DROP TABLE IF EXISTS kre_report.rsv_pat_height;
CREATE TABLE kre_report.rsv_pat_height AS
SELECT S1.patient_id, S1.index_rsv_lab_test_date, ROUND(S2.height::numeric, 0) as patient_height
FROM 
	(SELECT T1.patient_id, T1.index_rsv_lab_test_date, max(T2.natural_key) max_nat_key
	FROM kre_report.rsv_all_rsv_labs T1
	JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.date <= T1.index_rsv_lab_test_date
	AND T2.height is not null
	AND T2.date >= T1.index_rsv_lab_test_date - INTERVAL '6 months'
	GROUP BY T1.patient_id, T1.index_rsv_lab_test_date) S1
JOIN emr_encounter S2 ON (S1.max_nat_key = S2.natural_key);


DROP TABLE IF EXISTS kre_report.rsv_dx_before;
CREATE TABLE kre_report.rsv_dx_before AS
SELECT T1.patient_id, index_rsv_lab_test_date, array_agg(array[dx_code_id||';'||date] ORDER BY DATE) as patient_dx_array_1yr_prior
FROM emr_encounter T1
JOIN emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
JOIN kre_report.rsv_all_rsv_labs T3 ON (T1.patient_id = T3.patient_id)
AND dx_code_id not in ('icd9:799.9', 'icd9:6606')
AND date >= index_rsv_lab_test_date - INTERVAL '1 year'
AND date < index_rsv_lab_test_date
GROUP BY T1.patient_id, index_rsv_lab_test_date;

DROP TABLE IF EXISTS kre_report.rsv_dx_after;
CREATE TABLE kre_report.rsv_dx_after AS
SELECT T1.patient_id, index_rsv_lab_test_date, array_agg(array[dx_code_id||';'||date] ORDER BY DATE) as patient_dx_array_1yr_on_after
FROM emr_encounter T1
JOIN emr_encounter_dx_codes T2 ON (T1.id = T2.encounter_id)
JOIN kre_report.rsv_all_rsv_labs T3 ON (T1.patient_id = T3.patient_id)
AND dx_code_id not in ('icd9:799.9', 'icd9:6606')
AND date <= index_rsv_lab_test_date + INTERVAL '1 year'
AND date >= index_rsv_lab_test_date
GROUP BY T1.patient_id, index_rsv_lab_test_date;


-- -- 15689
-- DROP TABLE IF EXISTS kre_report.rsv_hospital;
-- CREATE TABLE kre_report.rsv_hospital AS
-- SELECT T1.patient_id, index_rsv_lab_test_date, --date, hosp_admit_dt, hosp_dschrg_dt
-- MAX(CASE WHEN hosp_admit_dt is not null OR hosp_dschrg_dt is not null THEN 1 ELSE 0 END) hosp_admission,
-- MAX(CASE WHEN hosp_dschrg_dt is not null THEN 1 ELSE 0 END) as hosp_discharge
-- FROM kre_report.rsv_all_rsv_labs T1
-- JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
-- WHERE T2.date >= index_rsv_lab_test_date - INTERVAL '7 days'
-- AND T2.date <= index_rsv_lab_test_date + INTERVAL '7 days'
-- AND (hosp_admit_dt is not null or hosp_dschrg_dt is not null)
-- GROUP BY T1.patient_id, index_rsv_lab_test_date;

DROP TABLE IF EXISTS kre_report.rsv_hospital;
CREATE TABLE kre_report.rsv_hospital AS
SELECT T1.patient_id, index_rsv_lab_test_date, --date, hosp_admit_dt, hosp_dschrg_dt
MAX(CASE WHEN hosp_admit_dt is not null OR hosp_dschrg_dt is not null THEN 1 ELSE 0 END) hosp_admission,
MAX(CASE WHEN hosp_dschrg_dt is not null THEN 1 ELSE 0 END) as hosp_discharge,
ARRAY_AGG (DISTINCT(hosp_admit_dt) ORDER BY hosp_admit_dt) hosp_admit_dates,
ARRAY_AGG (DISTINCT(hosp_dschrg_dt) ORDER BY hosp_dschrg_dt) hosp_dschrg_dates,
--SUM(CASE WHEN hosp_dschrg_dt is not null and hosp_admit_dt is not null THEN (hosp_dschrg_dt - hosp_admit_dt) +1 ELSE 1 END) total_stay_days,
ARRAY_AGG (DISTINCT(site_name) ORDER BY site_name) hosp_site_names,
ARRAY_AGG (DISTINCT(raw_encounter_type) ORDER BY raw_encounter_type) hosp_encounter_types
FROM kre_report.rsv_all_rsv_labs T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.date >= index_rsv_lab_test_date - INTERVAL '7 days'
AND T2.date <= index_rsv_lab_test_date + INTERVAL '7 days'
AND (

      (hosp_admit_dt is not null and hosp_dschrg_dt is not null)
	  OR
	  (hosp_admit_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
	  OR
	  (hosp_dschrg_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
	)
--hosp_admit_dt also has to be within 7 days of index test
AND (hosp_admit_dt is null or (hosp_admit_dt >= index_rsv_lab_test_date - INTERVAL '7 days' AND hosp_admit_dt <= index_rsv_lab_test_date + INTERVAL '7 days'))
GROUP BY T1.patient_id, index_rsv_lab_test_date;


--BUILD THE UNIQUE ADMIT DATES
DROP TABLE IF EXISTS kre_report.rsv_hosp_admit_dates;
CREATE TABLE kre_report.rsv_hosp_admit_dates AS
SELECT DISTINCT
T1.patient_id,
index_rsv_lab_test_date,
date,
CASE WHEN hosp_admit_dt is null then hosp_dschrg_dt
ELSE hosp_admit_dt END as hosp_admit_dt
FROM kre_report.rsv_all_rsv_labs T1
JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.date >= index_rsv_lab_test_date - INTERVAL '7 days'
AND T2.date <= index_rsv_lab_test_date + INTERVAL '7 days'
AND (

      (hosp_admit_dt is not null and hosp_dschrg_dt is not null)
	  OR
	  (hosp_admit_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
	  OR
	  (hosp_dschrg_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
	)
AND (hosp_admit_dt is null or (hosp_admit_dt >= index_rsv_lab_test_date - INTERVAL '7 days' AND hosp_admit_dt <= index_rsv_lab_test_date + INTERVAL '7 days')) ;

--BUILD THE UNIQUE DISCHARGE DATES & GET THE MAX FOR DATE
DROP TABLE IF EXISTS kre_report.rsv_hosp_dschrg_dates;
CREATE TABLE kre_report.rsv_hosp_dschrg_dates AS
SELECT 
patient_id,
index_rsv_lab_test_date,
date,
MAX(hosp_dschrg_dt) hosp_dschrg_dt
FROM (

	SELECT DISTINCT 
	T1.patient_id,
	index_rsv_lab_test_date,
	date,
	CASE WHEN hosp_dschrg_dt is null then hosp_admit_dt
	ELSE hosp_dschrg_dt END as hosp_dschrg_dt
	FROM kre_report.rsv_all_rsv_labs T1
	JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
	WHERE T2.date >= index_rsv_lab_test_date - INTERVAL '7 days'
	AND T2.date <= index_rsv_lab_test_date + INTERVAL '7 days'
	AND (

		  (hosp_admit_dt is not null and hosp_dschrg_dt is not null)
		  OR
		  (hosp_admit_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
		  OR
		  (hosp_dschrg_dt is not null and raw_encounter_type = 'HOSPITAL ENCOUNTER')
		)
	AND (hosp_admit_dt is null or (hosp_admit_dt >= index_rsv_lab_test_date - INTERVAL '7 days' AND hosp_admit_dt <= index_rsv_lab_test_date + INTERVAL '7 days'))
) S1
GROUP BY patient_id, index_rsv_lab_test_date, date;

--COMPUTE LENGTH OF HOSPITAL STAY
DROP TABLE IF EXISTS kre_report.rsv_hosp_stay_compute;
CREATE TABLE kre_report.rsv_hosp_stay_compute AS
SELECT 
patient_id,
index_rsv_lab_test_date,
SUM(stay_days) as total_stay_days FROM
(
	SELECT T1.patient_id,
	T1.index_rsv_lab_test_date,
	T1.date,
	(hosp_dschrg_dt - MIN(hosp_admit_dt)) + 1 as stay_days
	FROM kre_report.rsv_hosp_admit_dates T1
	JOIN kre_report.rsv_hosp_dschrg_dates T2 ON (T1.patient_id = T2.patient_id and T1.index_rsv_lab_test_date = T2.index_rsv_lab_test_date and T1.date = T2.date)
	GROUP BY T1.patient_id, T1.index_rsv_lab_test_date, T1.date, hosp_dschrg_dt
) S1
GROUP BY patient_id, index_rsv_lab_test_date;




--BRING TOGETHER ALL OF THE DATA AND ADD DEMOG INFO
DROP TABLE IF EXISTS kre_report.rsv_output_patid;
CREATE TABLE kre_report.rsv_output_patid AS
SELECT T1.patient_id,
T1.patient_dob,
CASE 
    WHEN upper(gender) is null or gender in ('UNKNOWN', '', 'X', 'U', 'O', 'S', '', 'T') then 'U'
	WHEN upper(gender) in ('M', 'MALE') then 'M'
	WHEN upper(gender) in ('F', 'FEMALE') then 'F'
	ELSE upper(gender)
END sex,
CASE
    when upper(race) in ('NONE', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'PT IS UNSURE', 'I DON''T KNOW MY RACE', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'NON-HISPANIC', 'U') or race is null then 'UNKNOWN'
	when upper(race) in ('AM INDIAN', 'AMERICAN INDIAN OR ALASKA NATIVE', 'AMERICAN INDIAN/ALASKA NATIVE') then 'AMERICAN INDIAN/ALASKAN NATIVE'
	when upper(race) in ('BLACK OR AFRICAN AMERICAN', 'BLACK/AFRICAN AMERICAN', 'B', 'BLACK/HISPANIC') then 'BLACK'
	when upper(race) in ('ASIAN INDIAN', 'CHINESE', 'FILIPINO', 'JAPANESE', 'KOREAN', 'VIETNAMESE', 'OTHER ASIAN', 'A') then 'ASIAN'
	when upper(race) in ('GUAM/CHAMORR', 'NATIVE HAWAI', 'OTHER PACIFI', 'PAC ISLANDER', 'SAMOAN', 'SOMOAN', 'NATIVE HAWAIIAN OR OTHER PACIFIC ISLANDER', 'OTHER PACIFIC ISLANDER', 'GUAMANIAN OR CHAMORRO') then 'PACIFIC ISLANDER/HAWAIIAN'
	when upper(race) in ('MIDDLE EAST', 'CAUCASIAN', 'W', 'WHITE/CAUCASIAN', 'WHITE/HISPANIC') then 'WHITE'
	when upper(race) in ('HISP', 'HISPANIC/LAT', 'HISPANIC OR LATINO') then 'HISPANIC'
	when upper(race) in ('PT REFUSED', 'DECLINED', 'DECLINED/REFUSED', 'DECLINE TO ANSWER', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('OTHER RACE', 'NOT LISTED', 'ASHKENAZI JEWISH', 'INDIAN', 'I', 'MULTIRACIAL') then 'OTHER'
    ELSE upper(race) END race_group,
CASE 
	when upper(ethnicity) in ('HISPANIC', 'HISPANIC/LAT', 'HISP', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'ANOTHER HISPANIC, LATINO/A OR SPANISH ORIGIN', 'ANOTHER HISPANIC,LATINO/A OR SPANISH ORIGIN', 'CUBAN', 'MEXICAN, MEXICAN AMERICAN, CHICANNO/A', 'PUERTO RICO', 'CENTRAL AMERICAN', 'CHILEAN', 'COLOMBIAN', 'DOMINICAN', 'ECUADORIAN', 'GUATEMALAN', 'GUATEMALA (UDS)', 'GUATEMALIAN', 'HAITIAN', 'HAITIAN (UDS)', 'HISPANIC (UDS)', 'HONDURAN', 'MEXICAN', 'MEXICAN AMERICAN', 'PANAMANIAN', 'PERUVIAN', 'PUERTO RICAN', 'SALVADORAN', 'SALVADORIAN','SALVADORIAN (UDS)', 'SOUTH AMERICAN', 'VENEZUELAN' ) then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NON HISPANIC', 'NON-HISPANIC', 'NOT HISPANIC', 'NOT HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'CAUCASIAN', 'NOT HISPANIC,LATINO/A OR SPANISH ORIGIN', 'AFRICAN', 'AFRICAN AMERICAN (UDS)', 'AFRICAN BORN (UDS)', 'AFRICAN (UDS)', 'ALBANIAN', 'ALBANIAN (UDS)', 'ARABIC', 'ARABIC (UDS)', 'ASIAN', 'ASIAN (UDS)', 'BLACK (UDS)', 'BOSNIAN', 'BRAZILIAN', 'BRAZILIAN (UDS)', 'CAMBODIAN', 'CAMBODIAN (UDS)', 'CANADIAN', 'CAPE VERDEAN', 'CHINESE', 'CHINESE (UDS)', 'ENGLISH (UDS)', 'EUROPEAN', 'EUROPEAN (UDS)', 'FILIPINO', 'FRENCH', 'GERMAN', 'GREEK', 'GREEK (UDS)', 'INDIAN FROM INDIA', 'INDIAN FROM INDIA (UDS)', 'IRAQI', 'IRISH', 'ITALIAN', 'JAMAICAN', 'JAMAICAN (UDS)', 'KOREAN', 'LAOTIAN', 'LAOTIAN (UDS)', 'LEBANESE', 'LEBANESE (UDS)', 'MIDDLE EASTERN', 'MIDDLE EASTERN (UDS)', 'MIXED', 'MIXED (UDS)', 'NATIVE AMERICAN/AMERICAN INDIAN', 'NATIVE AMERICAN (UDS)', 'NEPALI', 'NORTH AMERICAN (UDS)', 'PAKISTANI', 'POLISH', 'PORTUGESE', 'PORTUGESE (UDS)', 'ROMANIAN', 'RUSSIAN', 'RUSSIAN (UDS)', 'SOMALI', 'SPANISH (UDS)', 'THAI', 'VIETNAMESE', 'VIETNAMESE (UDS)', 'WEST INDIAN', 'WEST INDIAN (UDS)', 'WHITE PUERTO RICAN (UDS)', 'WHITE (UDS)') then 'NOT HISPANIC OR LATINO'
	when upper(ethnicity) in ('PT REFUSED', 'DECLINED', 'PATIENT DECLINED', 'CHOOSE NOT TO ANSWER', 'I CHOOSE NOT TO ANSWER', 'PATIENT CHOOSES NOT TO ANSWER', 'DECLINED/REFUSED', 'PATIENT REFUSED', 'UNREPORTED/CHOSE NOT TO DISCLOSE RACE AND ETHNICITY', 'UNREPORTED/REFUSED TO REPORT', 'UNREPORTED/REFUSED') then 'DECLINED TO ANSWER'
	when upper(race) in ('HISPANIC', 'HISP', 'HISPANIC/LAT', 'HISPANIC, LATINO, LATINA, LATINX, OR OF SPANISH OR LATIN AMERICAN ORIGIN', 'BLACK/HISPANIC', 'WHITE/HISPANIC' ) and (ethnicity is null or ethnicity = '')  then 'HISPANIC OR LATINO'
	when upper(ethnicity) in ('NONE', 'NO SELECTION', 'OTHER OR UNDETERMINED', 'U', 'NOT REPORTED', 'I AM NOT SURE / I DON''T KNOW', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'OTHER', 'OTHER (UDS)', 'UNKNOWN / NOT REPORTED', 'UNKNOWN (UDS)') or ethnicity is null then 'UNKNOWN'
	ELSE upper(ethnicity) END ethnicity_group,
T2.home_language,
T1.age_on_test_date,
T1.index_rsv_lab_test_date,
T1.index_rsv_lab_test_result,
coalesce(T3.rsv_rav_case_lab_yn, 0) rsv_rav_case_lab_yn,
coalesce(T4.rsv_rav_case_dx_yn, 0) rsv_rav_case_dx_yn,
coalesce(T5.rsv_rav_case_lab_and_dx_yn, 0) rsv_rav_case_lab_and_dx_yn,
coalesce(T6.rsv_rav_case_date, null) rsv_rav_case_date,
coalesce(T3.adenovirus_rav_pos_lab, -2) adenovirus_rav_pos_lab,
coalesce(T4.adenovirus_rav_dx, 0) adenovirus_rav_dx,
coalesce(T3.corona_non19_rav_pos_lab, -2) corona_non19_rav_pos_lab,
coalesce(T4.corona_non19_rav_dx, 0) corona_non19_rav_dx,
coalesce(T3.covid19_rav_pos_lab, -2) covid19_rav_pos_lab,
coalesce(T4.covid19_rav_dx, 0) covid19_rav_dx,
coalesce(T3.h_metap_rav_pos_lab, -2) h_metap_rav_pos_lab,
coalesce(T4.h_metap_rav_dx, 0) h_metap_rav_dx,
coalesce(T3.flu_rav_pos_lab, -2) flu_rav_pos_lab,
coalesce(T4.flu_rav_dx, 0) flu_rav_dx,
coalesce(T3.mycoplas_rav_pos_lab, -2) mycoplas_rav_pos_lab,
coalesce(T4.mycoplas_rav_dx, 0) mycoplas_rav_dx,
coalesce(T3.paraflu_rav_pos_lab, -2) paraflu_rav_pos_lab,
coalesce(T4.paraflu_rav_dx, 0) paraflu_rav_dx,
coalesce(T3.rhino_entero_rav_pos_lab, -2) rhino_entero_rav_pos_lab,
coalesce(T4.rhino_entero_rav_dx, 0) rhino_entero_rav_dx,
coalesce(T4.non_spec_rav_dx, 0) non_spec_rav_dx,
coalesce(T7.nirsevimab_rx, 0) nirsevimab_rx,
coalesce(T7.num_nirsevimab_rx_dates, 0) num_nirsevimab_rx_dates,
coalesce(T7.earliest_nirsevimab_rx_date, null) earliest_nirsevimab_rx_date,
coalesce(T7.latest_nirsevimab_rx_date, null) latest_nirsevimab_rx_date,
coalesce(T8.palivizumab_rx, 0) palivizumab_rx,
coalesce(T8.num_palivizumab_rx_dates, 0) num_palivizumab_rx_dates,
coalesce(T8.earliest_palivizumab_rx_date, null) earliest_palivizumab_rx_date,
coalesce(T8.latest_palivizumab_rx_date, null) latest_palivizumab_rx_date,
coalesce(T9.index_test_dept_name, 'UNKNOWN') index_test_dept_name,
coalesce(T10.patient_weight, null) patient_weight,
coalesce(T11.patient_height, null) patient_height,
T12.patient_dx_array_1yr_prior,
T13.patient_dx_array_1yr_on_after,
coalesce(T14.hosp_admission, 0) hosp_admission,
coalesce(T14.hosp_discharge, 0) hosp_discharge,
T14.hosp_admit_dates,
T14.hosp_dschrg_dates,
T15.total_stay_days,
T14.hosp_site_names,
T14.hosp_encounter_types
FROM kre_report.rsv_all_rsv_labs T1
JOIN emr_patient T2 ON (T1.patient_id = T2.id)
LEFT JOIN kre_report.rsv_other_pathogen_eval T3 ON (T1.patient_id = T3.patient_id and T1.index_rsv_lab_test_date = T3.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_dx_eval T4 ON (T1.patient_id = T4.patient_id and T1.index_rsv_lab_test_date = T4.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_lab_and_dx T5 ON (T1.patient_id = T5.patient_id and T1.index_rsv_lab_test_date = T5.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_rav_case_dates T6 ON (T1.patient_id = T6.patient_id and T1.index_rsv_lab_test_date = T6.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_nirsev_rx T7 ON (T1.patient_id = T7.patient_id and T1.index_rsv_lab_test_date = T7.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_paliv_rx T8 ON (T1.patient_id = T8.patient_id and T1.index_rsv_lab_test_date = T8.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_test_dept T9 ON (T1.patient_id = T9.patient_id and T1.index_rsv_lab_test_date = T9.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_pat_weight T10 ON (T1.patient_id = T10.patient_id and T1.index_rsv_lab_test_date = T10.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_pat_height T11 ON (T1.patient_id = T11.patient_id and T1.index_rsv_lab_test_date = T11.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_dx_before T12 ON (T1.patient_id = T12.patient_id and T1.index_rsv_lab_test_date = T12.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_dx_after T13 ON (T1.patient_id = T13.patient_id and T1.index_rsv_lab_test_date = T13.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_hospital T14 ON (T1.patient_id = T14.patient_id and T1.index_rsv_lab_test_date = T14.index_rsv_lab_test_date)
LEFT JOIN kre_report.rsv_hosp_stay_compute T15 ON (T1.patient_id = T15.patient_id and T1.index_rsv_lab_test_date = T15.index_rsv_lab_test_date)
ORDER BY T1.patient_id, T1.index_rsv_lab_test_date;


-- -- NEEDED FOR REPORTING
-- -- ONE TIME RUN ONLY
-- CREATE SEQUENCE IF NOT EXISTS  kre_report.rsv_masked_id_seq;


-- CREATE TABLE kre_report.rsv_masked_patients
-- (
    -- patient_id integer,
    -- masked_patient_id character varying(128)
-- );

INSERT INTO kre_report.rsv_masked_patients
SELECT 
DISTINCT T1.patient_id,
CONCAT(:site_abbr, '-', NEXTVAL('kre_report.rsv_masked_id_seq')) masked_patient_id
FROM kre_report.rsv_output_patid T1
WHERE patient_id not in (SELECT patient_id from kre_report.rsv_masked_patients WHERE masked_patient_id ilike CONCAT('%', :site_abbr, '%'))
GROUP by T1.patient_id;

DROP TABLE IF EXISTS kre_report.rsv_output_final;
CREATE TABLE kre_report.rsv_output_final AS
SELECT 
:site_abbr as site,
masked_patient_id,
patient_dob,
sex,
race_group,
ethnicity_group,
home_language,
age_on_test_date,
index_rsv_lab_test_date,
index_rsv_lab_test_result,
rsv_rav_case_lab_yn,
rsv_rav_case_dx_yn,
rsv_rav_case_lab_and_dx_yn,
rsv_rav_case_date,
adenovirus_rav_pos_lab,
adenovirus_rav_dx,
corona_non19_rav_pos_lab,
corona_non19_rav_dx,
covid19_rav_pos_lab,
covid19_rav_dx,
h_metap_rav_pos_lab,
h_metap_rav_dx,
flu_rav_pos_lab,
flu_rav_dx,
mycoplas_rav_pos_lab,
mycoplas_rav_dx,
paraflu_rav_pos_lab,
paraflu_rav_dx,
rhino_entero_rav_pos_lab,
rhino_entero_rav_dx,
non_spec_rav_dx,
nirsevimab_rx,
num_nirsevimab_rx_dates,
earliest_nirsevimab_rx_date,
latest_nirsevimab_rx_date,
palivizumab_rx,
num_palivizumab_rx_dates,
earliest_palivizumab_rx_date,
latest_palivizumab_rx_date,
index_test_dept_name,
patient_weight,
patient_height,
patient_dx_array_1yr_prior,
patient_dx_array_1yr_on_after,
hosp_admission,
hosp_discharge,
hosp_admit_dates,
hosp_dschrg_dates,
total_stay_days,
hosp_site_names,
hosp_encounter_types
FROM kre_report.rsv_output_patid T1
JOIN kre_report.rsv_masked_patients T2 ON (T1.patient_id = T2.patient_id);


--NOTES FOR RUNNING IT

-- delete from kre_report.rsv_masked_patients;

--nohup psql -d esp -f rsv-esp-bmc-linelist.sql -v site_abbr="'BMC'" &

--\COPY (SELECT * FROM kre_report.rsv_output_final) TO '/tmp/2024-11-14-BMC-RSV-Study-V1.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r "jeffrey.campbell@bmc.org" -p /tmp -n 2024-11-14-BMC-RSV-Study-V1.csv -s "BMC RSV Study Data V1"


--\COPY (SELECT * FROM kre_report.rsv_output_final) TO '/tmp/2024-11-22-BMC-RSV-Study-V2.csv' WITH CSV HEADER;
--zip 2024-11-22-BMC-RSV-Study-V2.zip 2024-11-22-BMC-RSV-Study-V2.csv

--mail -s "BMC RSV Study Data V2" -a 2024-11-22-BMC-RSV-Study-V2.zip jeffrey.campbell@bmc.org
--Version 2   -Karen
--.






  

	
	
	
	
