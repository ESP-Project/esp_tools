UPDATE emr_patient
SET date_of_death = (select TO_TIMESTAMP(cdate_of_death,'YYYYMMDDHH24MI') )
WHERE  id in (
         SELECT id
         FROM   emr_patient
         WHERE  cdate_of_death is not null and cdate_of_death != ''
	     and date_of_death is null
         LIMIT  500
         )