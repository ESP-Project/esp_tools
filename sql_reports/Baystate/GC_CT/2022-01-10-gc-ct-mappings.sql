
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1115241693--567447207', 'c_pneumoniae_pcr', 'exact', 'both', true, null, '10828004', '260385009', '42425007' ) ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '567421764--567447207', 'c_pneumoniae_pcr', 'exact', 'both', true, null, '10828004', '260385009', '42425007' ) ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '579116523--579143860', 'c_pneumoniae_pcr', 'exact', 'both', true, null, '10828004', '260385009', '42425007' ) ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '926802809--926808963', 'c_pneumoniae_pcr', 'exact', 'both', true, null, '10828004', '260385009', '42425007' ) ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '926802999--567447207', 'c_pneumoniae_pcr', 'exact', 'both', true, null, '10828004', '260385009', '42425007' ) ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '134625936--135142000', 'chlamydia', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '134626344--135142000', 'chlamydia', 'exact', 'both', true, '20993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914774--1976544', 'chlamydia', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914777--1976549', 'chlamydia', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914800--1976544', 'chlamydia', 'exact', 'both', true, '20993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914800--1976547', 'chlamydia', 'exact', 'both', true, '20993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914805--1976549', 'chlamydia', 'exact', 'both', true, '20993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '569050633--569078030', 'chlamydia', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '569050639--569078040', 'chlamydia', 'exact', 'both', true, '20993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '596271042--596602129', 'chlamydia', 'exact', 'both', true, '4993-2', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '822135809--824133219', 'chlamydia', 'exact', 'both', true, '6349-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '925529989--1976544', 'chlamydia', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '134625936--135142001', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '134626128--135142001', 'gonorrhea', 'exact', 'both', true, '23908-7', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914774--1976545', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914777--1976550', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914814--1976545', 'gonorrhea', 'exact', 'both', true, '23908-7', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914814--1976548', 'gonorrhea', 'exact', 'both', true, '23908-7', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '1914831--1976550', 'gonorrhea', 'exact', 'both', true, '23908-7', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '569050633--569078035', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '569050639--569078045', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '596271042--596602134', 'gonorrhea', 'exact', 'both', true, '24111-7', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '885059--711179', 'gonorrhea', 'exact', 'both', true, '691-6', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_labtestmap(id, native_code, test_name, code_match_type, record_type, reportable, output_code, snomed_pos, snomed_neg, snomed_ind) VALUES (default, '925529989--1976545', 'gonorrhea', 'exact', 'both', true, '36902-5', '10828004', '260385009', '42425007') ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '1115225501--1118785759' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '129250698--570928828' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '1304687753--1305008577' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '1304688851--1305008609' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '513997063--515499373' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '513997063--515499378' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '564896164--564930254' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '570902862--570928763' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '574055748--574063957' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '579116523--579143855' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483724--586410062' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483838--586410122' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483838--586410127' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483838--586410132' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483934--586410167' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483934--586410177' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585483934--586410187' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585484029--586410167' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585484029--586410177' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585484029--586410187' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '585487383--586410147' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '674150251--674233505' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '706365047--706380029' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '732179153--733601389' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '732179419--733601125' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '732179419--733601133' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '789444--710222' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '791067--710308' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '822135809--824133229' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '907948--711218' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '966316--711325' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537231' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537241' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537251' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537271' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537281' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '972224269--972537291' ) ON CONFLICT DO NOTHING;


--MODIFICATIONS BASED ON REVIEW ON 01/18/2022

delete from conf_labtestmap where native_code in ('1914800--1976547', '1914814--1976548');
insert into conf_ignoredcode VALUES(default, '1914814--1976548' ) ON CONFLICT DO NOTHING;
insert into conf_ignoredcode VALUES(default, '1914800--1976547' ) ON CONFLICT DO NOTHING;



-- UPDATES TO LOINC/SNOMEDS 01/25/2022
-- gonorrhea
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 18;
update conf_labtestmap set output_code = '24111-7', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 19;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 20;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 21;
update conf_labtestmap set output_code = 'MDPH-304', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R182' WHERE id = 25;
update conf_labtestmap set output_code = '45072-6', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R182' WHERE id = 26;
update conf_labtestmap set output_code = '24111-7', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 27;
update conf_labtestmap set output_code = '698-1', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = null WHERE id = 28;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '68704007', snomed_neg = 'MDPH-R442', snomed_ind = 'MDPH-R572' WHERE id = 29;

-- gon 
-- MDPH wanted 24111-7 but no Indeterminate SNOMED exists
-- Reverting to 5026-6 LOINC for now per DPH. They will advise when the other one is available.
update conf_labtestmap set output_code = '5028-6', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 22;
update conf_labtestmap set output_code = '5028-6', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 24;

-- chlamydia
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 6;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 7;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 8;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 9;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 10;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 12;
update conf_labtestmap set output_code = 'MDPH-304', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = '42425007' WHERE id = 13;
update conf_labtestmap set output_code = '45072-6', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = '42425007' WHERE id = 14;
update conf_labtestmap set output_code = '4993-2', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = 'MDPH-R182' WHERE id = 15;
update conf_labtestmap set output_code = '64017-7', snomed_pos = '63938009', snomed_neg = 'MDPH-R443', snomed_ind = 'MDPH-R571' WHERE id = 17;

--chlam
-- MDPH wanted 11475-1 but it does not have a negative SNOMED.
-- Changed it to 6349-5 until DPH advises differently.
update conf_labtestmap set output_code = '6349-5', snomed_pos = '63938009', snomed_neg = '260385009', snomed_ind = null WHERE id = 16;







