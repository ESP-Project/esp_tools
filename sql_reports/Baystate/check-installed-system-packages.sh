#!/bin/sh
pkg_list=(git git224 gcc httpd httpd24u rh-python36 rh-python36-mod_wsgi wget rh-python36-python-virtualenv rh-python36-python-setuptools rh-python36-python-devel postgresql13 postgresql13-server postgresql13-libs postgresql13-contrib)
for i in  ${pkg_list[*]}
 do
  isinstalled=$(rpm -q $i)
  if [ !  "$isinstalled" == "package $i is not installed" ];
   then
    echo Package  $i already installed
  else
    echo $i is NOT installed. 
  fi
done