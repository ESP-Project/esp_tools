SELECT count(*), test_name, T1.native_code, native_name, result_string
FROM emr_labresult T1
JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE T2.test_name ilike 'hepatitis_c%'
AND result_string ilike '6.369'
GROUP BY test_name, T1.native_code, native_name, result_string;
