-- clean up ignored
delete from conf_ignoredcode where native_code in (select native_code from conf_labtestmap);

-- add mappings for result strings
insert into conf_resultstring VALUES (default, 'Inconclusive', 'ind', 'istartswith', 'true') ON CONFLICT DO NOTHING;
insert into conf_resultstring VALUES (default, 'Undetec', 'neg', 'istartswith', 'true') ON CONFLICT DO NOTHING;

