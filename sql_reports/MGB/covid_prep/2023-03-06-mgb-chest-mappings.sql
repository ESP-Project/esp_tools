insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.3D.CHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.3D.AGCHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.3D.AGCAP', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.3D.AGAVP', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.VA.CHESTAG', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.VA.CHABDPEL', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.VA.CHESTAG@', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.VA.CSTABDPL2', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.VA.CHESTAG+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHTES-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.H010009', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHESTX', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHESTO', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHESTPE', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.H010010', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHEST@', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHEST+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABDPL3', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTNECK1', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHEST-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTNECK2', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABD+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABD-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABPEL@', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABPEL+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CSTABPEL-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.H000031', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.H000035', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHABPL', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NECHT-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NECK+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NCKCST+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NCKCST-', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NCKCSTAB+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.NKCTABPL+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.H000105', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.TH.CHESTPT', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'CT.XS.NKCHAPPT+', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestct', 'OUTSIDE.CTCHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5906', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5566', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5374', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5902', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5722', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5904', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5562', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5375', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5616', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5426', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5427', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'PRO5724', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'FL.TH.CHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'FL.TH.FLCHT2', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'OUTSIDE.XRCHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.AB.ABDCHE', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHEST', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHEST1', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHEST1B', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHSTAL', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHEST4', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHSTOB', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHTLOR', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHSTDC', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHSTIE', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHOUTX', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHOUTO', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CHEST2', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CXRPOR', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CXRPR1', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.TH.CXRPR2', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.AB.BABY', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.MS.RIB3/L', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.MS.RIB3/R', 'exact', 'both', 'false');
insert into conf_labtestmap (id, test_name, native_code, code_match_type, record_type, reportable) VALUES(default, 'chestxray', 'XR.MS.RIB4/B', 'exact', 'both', 'false');


