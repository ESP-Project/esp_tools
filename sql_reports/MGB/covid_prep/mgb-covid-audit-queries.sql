-- FOR GOING FORWARD AFTER HISTORICAL
SELECT distinct(patient_id) as esp_patient_id,
mrn,
CONCAT('covid-surv-MGB-',CAST(date_trunc('week', sent_timestamp::date) AS DATE) - 2, '.zip' ) as filename
FROM covid19_report.suspect_nodis_case T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
-- date historical run completed. do not change
WHERE sent_timestamp::date > '2021-06-25';
--optional to specify exact sending date
--AND sent_timestamp::date = '2021-06-29'::date;


-- FOR HISTORICAL RUN
SELECT distinct(patient_id) as esp_patient_id,
mrn,
CONCAT('covid-surv-MGB-',CAST(date_trunc('week', date::date) AS DATE) + 5, '.zip' ) as filename
FROM covid19_report.suspect_nodis_case T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
-- date historical run completed. do not change
WHERE sent_timestamp::date <= '2021-06-25';
-- optional to specify exact report week
--AND CAST(date_trunc('week', date::date) AS DATE) + 5 = '2020-08-08';



covid19_report.surv_pats_masked_patients


n July 2021, we set up the "covid19_report.suspect_send_audit" table (specifically for MGB). This was after the launch in June 2021 so the first historical reporting sends would not be in this file (but we could approximate that date on when we started the covid run...or we might have populated it with existing data when we rolled it out...cannot recall), but all subsequent weeks as well as the latest historical sends would have entries in this table.

It has the following fields:
esp_patient_id
masked_patient_id
mrn
index_date
sent_date





