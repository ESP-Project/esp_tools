select patient_id, date, 'enc'::varchar(3) as source, id as encounter_id
from emr_encounter e
where date='2023-09-26'
and e.weight is null
and e.height is null
and e.bp_systolic is null 
and e.bp_diastolic is null
and e.temperature is null
and e.pregnant=FALSE
and e.edd is null
and not exists (select null from emr_encounter_dx_codes dx
                where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9');
				

SELECT pg_size_pretty( pg_total_relation_size('emr_labresult')) as emr_labresult_size ;		
				

				
DROP TABLE IF EXISTS kre_report.tmp_search_string_filter;
CREATE TABLE kre_report.tmp_search_string_filter
(
  search_string text
)
WITH (
  OIDS=FALSE
);

-- search string values
INSERT INTO kre_report.tmp_search_string_filter(search_string)
VALUES 
( '%a1c%'),
( '%adenovirus%'),
( '%afb%'),
( '%agglutination%'),
( '%alt%'),
( '%aminotrans%'),
( '%anaplasma%%'),
( '%ast%'),
( '%babesia%'),
( '%babesiosis%'),
( '%bili%'),
( '%bloodsmear%'),
( '%borr%'),
( '%burg%'),
( '%cd4%'),
( '%chlam%'),
( '%chol%'),
( '%corona%'),
( '%cov%'),
( '%covid%'),
( '%creat%'), 
( '%crp%'),
( '%cult%'),
( '%diab%'),
( '%dimer%'),
( '%enterovirus%'),
( '%feron%'),
( '%flu%'), 
( '%fta%'), 
( '%gad65%'),
( '%gc%'),
( '%genotype%'),
( '%gluc%'),
( '%glyc%'),
( '%gon%'),
( '%hbc%'),
( '%hbsag%'),
( '%hbv%'),
( '%hct%'),
( '%hcv%'),
( '%hdl%'),
( '%helper%'),
( '%hematocrit%'), 
( '%hep%'),
( '%hge%'),
( '%hiv%'),
( '%ica512%'),
( '%igra%'),
( '%immuno%'),
( '%influenza%'),
( '%insulin%'),
( '%islet%'),
( '%ldl%'),
( '%lipoprotein%'),
( '%lyme%'),
( '%lymp%'),
( '%mttt%'),
( '%mycob%'),
( '%ogtt%'),
( '%pallidum%'),
( '%paraflu%'),
( '%parapertussis%'),
( '%partial%'),
( '%pcr%'),
( '%peptide%'),
( '%per%'),
( '%phago%'),
( '%platelet%'),
( '%plt%'),
( '%pneumo%'),
( '%po2%'),
( '%qft%'),
( '%rapid plasma reagin%'),
( '%rbc%'),
( '%reactive%'),
( '%red bl%'),
( '%rhinovirus%'),
( '%rpr%'),
( '%rsv%'),
( '%sars%'),
( '%serol%'),
( '%sgot%'),
( '%sgpt%'),
( '%signal%'),
( '%syncitial%'),
( '%synctial%'),
( '%syncytial%'),
( '%syph%'),
( '%t-spot%'),
( '%tb%'),
( '%tbil%'),
( '%tp%'),
( '%trac%'),
( '%trep%'),
( '%trig%'),
( '%tuber%'),
( '%vdrl%'),
( '%wbc%'),
( '%white bl%'),
( '%zeus%');


-- Identify labs that can be deleted. 
-- native_name does not match search string and native_code is not a mapped lab

drop table if exists kre_report.tmp_labs_for_deletion_prep;
create table kre_report.tmp_labs_for_deletion_prep as 
select T1.*
from emr_labresult T1
LEFT JOIN (select array_agg(search_string) search_string from kre_report.tmp_search_string_filter) f on (1=1)
where native_code not in (select native_code from conf_labtestmap)
and native_name NOT ILIKE ALL(search_string)
and date >= '2008-01-01'
and date < '2008-06-01';

-- drop table if exists kre_report.tmp_labs_for_deletion_mapped;
-- create table kre_report.tmp_labs_for_deletion_mapped as 
-- select DISTINCT T1.patient_id, T1.date
-- from kre_report.tmp_labs_for_deletion_prep T1
-- INNER JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date)
-- INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code);

-- Find mapped labs where the month matches the same month as a lab marked for deletion

drop table if exists kre_report.tmp_labs_for_deletion_mapped;
create table kre_report.tmp_labs_for_deletion_mapped as 
select DISTINCT T1.patient_id, to_char(T1.date, 'YYYY-MM') mapped_lab_month
from kre_report.tmp_labs_for_deletion_prep T1
INNER JOIN  emr_labresult T2 ON (T1.patient_id = T2.patient_id and to_char(T1.date, 'YYYY-MM') = to_char(T2.date, 'YYYY-MM') ) 
INNER JOIN conf_labtestmap T3 ON (T2.native_code = T3.native_code);


-- -- It should be safe to delete labs for patients that have a mapped lab on the same date
-- drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete;
-- create table kre_report.tmp_labs_for_deletion_safe_to_delete as 
-- select T1.* 
-- FROM kre_report.tmp_labs_for_deletion_prep T1
-- INNER JOIN kre_report.tmp_labs_for_deletion_mapped T2 ON (T1.patient_id = T2.patient_id and T1.date = T2.date);


-- It should be safe to delete labs for patients that have a mapped lab in the same month as a lab that can be deleted
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete;
create table kre_report.tmp_labs_for_deletion_safe_to_delete as 
select T1.* 
FROM kre_report.tmp_labs_for_deletion_prep T1
INNER JOIN kre_report.tmp_labs_for_deletion_mapped T2 ON (T1.patient_id = T2.patient_id and to_char(T1.date, 'YYYY-MM') = mapped_lab_month );


-- Identify patients with another clinical_encounter type (not lx) in the same month

drop table if exists kre_report.tmp_labs_for_deletion_clinenc_other;
create table kre_report.tmp_labs_for_deletion_clinenc_other as 
select DISTINCT T1.patient_id, to_char(T1.date, 'YYYY-MM') clin_enc_month
from kre_report.tmp_labs_for_deletion_prep T1
INNER JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id and to_char(T1.date, 'YYYY-MM') = to_char(T2.date, 'YYYY-MM') ) 
WHERE source != 'lx';	

-- It should be safe to delete labs for patients that have a clin enc of another type in the same month
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete_2;
create table kre_report.tmp_labs_for_deletion_safe_to_delete_2 as 
select T1.* 
FROM kre_report.tmp_labs_for_deletion_prep T1
INNER JOIN kre_report.tmp_labs_for_deletion_clinenc_other T2 ON ( T1.patient_id = T2.patient_id and to_char(T1.date, 'YYYY-MM') = clin_enc_month );

-- Bring together all labs that can be deleted
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete_final;
create table kre_report.tmp_labs_for_deletion_safe_to_delete_final as
select * from kre_report.tmp_labs_for_deletion_safe_to_delete
UNION
select * from kre_report.tmp_labs_for_deletion_safe_to_delete_2;


--- ADD DELETE HERE
--- DELETE FROM emr_labresult where id in (select id from kre_report.tmp_labs_for_deletion_safe_to_delete_final);
---

drop table if exists kre_report.tmp_labs_for_deletion_prep;
drop table if exists kre_report.tmp_labs_for_deletion_mapped;
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete;
drop table if exists kre_report.tmp_labs_for_deletion_clinenc_other;
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete_2;
drop table if exists kre_report.tmp_labs_for_deletion_safe_to_delete_final;
