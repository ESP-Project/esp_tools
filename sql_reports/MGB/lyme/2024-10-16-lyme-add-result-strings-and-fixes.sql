
update conf_labtestmap set test_name = 'lyme_mttta_conf' where test_name = 'lyme_mttt_conf';

--INDETERMINATE DEV
INSERT INTO conf_labtestmap_extra_indeterminate_strings 
SELECT NEXTVAL('conf_labtestmap_extra_indeterminate_strings_id_seq'), id, 237
FROM conf_labtestmap where test_name ilike 'lyme%';

INSERT INTO conf_labtestmap_extra_indeterminate_strings 
SELECT NEXTVAL('conf_labtestmap_extra_indeterminate_strings_id_seq'), id, 260
FROM conf_labtestmap where test_name ilike 'lyme%';

--NEGATIVE BOTH PROD AND DEV
INSERT INTO conf_labtestmap_extra_negative_strings 
SELECT NEXTVAL('conf_labtestmap_extra_negative_strings_id_seq'), id, 257
FROM conf_labtestmap where test_name ilike 'lyme%';

INSERT INTO conf_labtestmap_extra_negative_strings 
SELECT NEXTVAL('conf_labtestmap_extra_negative_strings_id_seq'), id, 258
FROM conf_labtestmap where test_name ilike 'lyme%';

INSERT INTO conf_labtestmap_extra_negative_strings 
SELECT NEXTVAL('conf_labtestmap_extra_negative_strings_id_seq'), id, 259
FROM conf_labtestmap where test_name ilike 'lyme%';

--POSITIVE BOTH PROD AND DEV

INSERT INTO conf_labtestmap_extra_positive_strings 
SELECT NEXTVAL('conf_labtestmap_extra_positive_strings_id_seq'), id, 113
FROM conf_labtestmap where test_name ilike 'lyme%';

INSERT INTO conf_labtestmap_extra_positive_strings 
SELECT NEXTVAL('conf_labtestmap_extra_positive_strings_id_seq'), id, 261
FROM conf_labtestmap where test_name ilike 'lyme%';


--INDETERMINATE PROD
INSERT INTO conf_labtestmap_extra_indeterminate_strings 
SELECT NEXTVAL('conf_labtestmap_extra_indeterminate_strings_id_seq'), id, 238
FROM conf_labtestmap where test_name ilike 'lyme%';

INSERT INTO conf_labtestmap_extra_indeterminate_strings 
SELECT NEXTVAL('conf_labtestmap_extra_indeterminate_strings_id_seq'), id, 260
FROM conf_labtestmap where test_name ilike 'lyme%';

