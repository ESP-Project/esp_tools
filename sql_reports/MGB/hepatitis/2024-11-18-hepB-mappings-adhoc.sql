

UPDATE conf_labtestmap 
SET output_code = '16933-4',
snomed_pos = '10828004',
snomed_neg = '260385009',
snomed_ind = '42425007'
WHERE test_name = 'hepatitis_b_core_ab';


UPDATE conf_labtestmap 
SET output_code = '31204-1',
snomed_pos = '10828004',
snomed_neg = '260385009',
snomed_ind = '42425007'
WHERE test_name = 'hepatitis_b_core_antigen_igm_antibody';

UPDATE conf_labtestmap 
SET output_code = '13954-3',
snomed_pos = '10828004',
snomed_neg = '260385009',
snomed_ind = '42425007'
WHERE test_name = 'hepatitis_b_e_antigen';

UPDATE conf_labtestmap 
SET output_code = '42595-9',
snomed_pos = '10828004',
snomed_neg = '260385009',
snomed_ind = '42425007',
threshold = 10
WHERE test_name = 'hepatitis_b_viral_dna';

UPDATE conf_labtestmap
SET threshold = 20
where test_name = 'hepatitis_b_viral_dna'
and native_code in (
'84999--5020038063',
'84999--504117',
'84999--5200006542',
'84999--5200006545',
'84999--7001000035');

UPDATE conf_labtestmap 
SET output_code = '5195-3',
snomed_pos = '10828004',
snomed_neg = '260385009',
snomed_ind = '42425007'
WHERE test_name = 'hepatitis_b_surface_antigen';

--NEED TO UPDATE OUTPUT CODE FOR CONF/NEUT TESTS

UPDATE conf_labtestmap 
SET output_code = '7905-3'
WHERE test_name = 'hepatitis_b_surface_antigen'
AND native_code in (
'84999--5200006485',
'84999--52000157598',
'84999--52000162783',
'84999--5207000000',
'84999--5290006604',
'84999--70000016942',
'84999--7000002695');









