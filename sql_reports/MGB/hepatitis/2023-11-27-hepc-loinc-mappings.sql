delete from conf_labtestmap where native_code = '84999--70000044325';

update conf_labtestmap set output_code = '13955-0', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = '42425007'
where test_name = 'hepatitis_c_elisa';

update conf_labtestmap set output_code = '32286-7' where test_name = 'hepatitis_c_genotype';

update conf_labtestmap set output_code = '13955-0', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = '42425007'
where test_name = 'hepatitis_c_riba';

update conf_labtestmap set threshold = 15 where native_code in ('84999--5210006565', '84999--5300002505', '84999--7000003556', '84999--7002002561');

update conf_labtestmap set threshold = null where native_code in ('84999--70000025550', '84999--70000047690');

update conf_labtestmap set output_code = '5012-0', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = '42425007'
where test_name = 'hepatitis_c_rna' and threshold is null;

update conf_labtestmap set output_code = '29609-5', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = '42425007'
where test_name = 'hepatitis_c_rna' and threshold is not null;

update conf_labtestmap set output_code = 'MDPH-144', snomed_pos = '10828004', snomed_neg = '260385009', snomed_ind = '42425007'
where test_name = 'hepatitis_c_signal_cutoff';

update conf_labtestmap set reportable = false where test_name in ('alt', 'ast', 'bilirubin_direct', 'bilirubin_indirect', 'bilirubin_total');









