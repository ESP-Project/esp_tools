-- change to ab_diff
--'84999--70000018309' -- HIV-1
--'84999--70000018310' -- HIV2
-- change to ag_ab
--'84999--70000018308'  


-- 1. 
-- If you get a nodis_case_events ERROR when running this one, DO NOT PROCEED and please let me know.

delete from hef_event
where name = 'lx:hiv_elisa:positive' and object_id in (select id from emr_labresult where native_code in ('84999--70000018308') );

-- 2. 
-- If you get a nodis_case_events ERROR when running this one, DO NOT PROCEED and please let me know.
delete from hef_event
where name ilike 'lx:hiv_ag_ab:%' and object_id in (select id from emr_labresult where native_code in ('84999--70000018309', '84999--70000018310' ) );

-- 3.
update hef_event
set name = REPLACE(name, 'elisa', 'ag_ab' )
where object_id in (select id from emr_labresult where native_code in ('84999--70000018308') )
and name ilike 'lx:hiv_elisa%';

-- 4.
update conf_labtestmap set test_name = 'hiv_ab_diff', output_code = '68961-2' where native_code = '84999--70000018309';
update conf_labtestmap set test_name = 'hiv_ab_diff', output_code = '81641-3' where native_code = '84999--70000018310';
update conf_labtestmap set test_name = 'hiv_ag_ab', output_code = '56888-1' where native_code = '84999--70000018308';