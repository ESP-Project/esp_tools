select count(*), T1.native_code, test_name, T1.result_string, snomed_pos, snomed_neg, snomed_ind
from emr_labresult T1
JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE output_code = '80203-3'
GROUP BY T1.native_code, test_name, T1.result_string, snomed_pos, snomed_neg, snomed_ind
ORDER BY native_code, result_string;