select count(distinct(case_id)) FROM 
(
select T1.patient_id, T1.id as case_id, T1.date as case_date, T4.native_code, T4.result_string
FROM nodis_case T1
INNER JOIN nodis_case_events T2 ON (T1.id = T2.case_id)
INNER JOIN hef_event T3 ON (T2.event_id = T3.id)
INNER JOIN emr_labresult T4 ON (T3.object_id = T4.id and T1.patient_id = T4.patient_id)
WHERE condition = 'hiv'
AND T4.native_code in (select native_code from conf_labtestmap where test_name = 'hiv_ab_diff')
AND T1.date >= '2021-08-01'
AND T1.date <= '2022-05-12') S1;


--FIND CASES WITH EVENTS SINCE 2022 THAT ARE ASSOCIATED 
--WITH CASES THAT HAVE NOT BEEN SENT

select count(distinct(case_id)), extract(year from case_date) as case_year
FROM (
	select T1.case_id, T1.event_id, T2.date as hef_date, T2.name as hef_name, T3.id, T3.date as case_date, T3.status
	from nodis_case_events T1
	INNER JOIN hef_event T2 ON (T1.event_id = T2.id)
	INNER JOIN nodis_case T3 ON (T1.case_id = T3.id)
	WHERE condition = 'hiv' 
	AND status not in ('S', 'RS')
	AND T2.date >= '2022-01-01' ) S1
GROUP BY case_year;