
CREATE TABLE temp_hepb_check_log_copies_mix AS
select count(*), T1.native_code, native_name, procedure_name, result_string, ref_unit
from emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE test_name = 'hepatitis_b_viral_dna'
AND (native_name ilike '%log%'
	OR procedure_name ilike '%log%'
	OR result_string ilike '%log%'
	OR ref_unit ilike '%log%'
	OR (result_string ilike '%.%' and result_float < 50) )
GROUP BY T1.native_code, native_name, procedure_name, result_string, ref_unit
ORDER BY T1.native_code, native_name, procedure_name, ref_unit, result_string;

SELECT * from temp_hepb_check_log_copies_mix;

-- GET EVERYTHING

DROP TABLE IF EXISTS temp_hepb_check_log_copies_all;
CREATE TABLE temp_hepb_check_log_copies_all AS
select count(*), T1.native_code, native_name, procedure_name, result_string, ref_unit
from emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
WHERE test_name = 'hepatitis_b_viral_dna'
GROUP BY T1.native_code, native_name, procedure_name, result_string, ref_unit
ORDER BY T1.native_code, native_name, procedure_name, ref_unit, result_string;

SELECT * from temp_hepb_check_log_copies_all;

-------------------------------------
-------------------------------------
-------------------------------------



-- GENERAL QUERIES TO IDENTIFY LOG LABS
select count(*), native_name, native_code from emr_labresult where result_string ilike '%log%' group by native_name, native_code;
select count(*), native_code, native_name, ref_unit from emr_labresult where ref_unit ilike '%log%' group by native_code, native_name, ref_unit;


-- NOTES ON MANUAL FIXES AFTER CHANGES TO DAILY_BATCH (VARIES BY SITE AND FIX NEEDED) 
-- THESE ARE EXAMPLES ONLY

--1. Manually update the native_code for the log results:

update emr_labresult set native_code = concat(native_code, ' ', ref_unit) 
where native_code = 'HEPATITIS B DNA, QUANTITATIVE, SERUM--HEPATITIS B VIRUS DNA'
and ref_unit ilike '%log%';

update emr_labresult set native_code = concat(native_code, ' ', 'LOG IU/ML')
where native_code = '137231--MLI-137231'
and result_string ilike '%log%';


update emr_labresult set native_code = concat(native_code, ' ', 'LOG IU/ML')
where native_code = '11194--MLI-11194'
and ref_high_string = '<1.30';

update emr_labresult set native_code = concat(native_code, ' ', 'LOG IU/ML')
where native_code = '11194--MLI-11194'
and result_string ilike '%.%'
and result_string not ilike '>%';


--2. Delete the hef_events (and cases)




create table kre_heb_b_cases_to_delete as 
select distinct case_id from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hepatitis_b_viral_dna%');

delete from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hepatitis_b_viral_dna%');
delete from nodis_case_events where case_id in (select case_id from kre_heb_b_cases_to_delete);
delete from nodis_caseactivehistory where case_id in (select case_id from kre_heb_b_cases_to_delete);

-- manually deleted case_report
delete from nodis_casereport where case_id in (select case_id from kre_heb_b_cases_to_delete);
delete from nodis_report_cases where case_id in (select case_id from kre_heb_b_cases_to_delete);
delete from nodis_case where id in (select case_id from kre_heb_b_cases_to_delete);
delete from hef_event where name ilike 'lx:hepatitis_b_viral_dna%';
drop table kre_heb_b_cases_to_delete;



create table kre_heb_c_cases_to_delete as 
select distinct case_id from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hepatitis_c_rna%');
delete from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hepatitis_c_rna%');
delete from nodis_case_events where case_id in (select case_id from kre_heb_c_cases_to_delete);
delete from nodis_caseactivehistory where case_id in (select case_id from kre_heb_c_cases_to_delete);

create table kre_heb_c_case_reports_to_delete as
select T1.id as case_id,
T2.id as nodis_casereport_id,
T3.id as nodis_casereportreported_id,
T4.id as nodis_reported_id
from nodis_case T1
INNER JOIN nodis_casereport T2 on (T1.id = T2.case_id)
INNER JOIN nodis_casereportreported T3 on (T2.id = T3.case_report_id)
LEFT JOIN nodis_reported T4 on (T3.id = T4.case_reported_id)
WHERE T1.id in (select case_id from kre_heb_c_cases_to_delete);

delete from nodis_reported where id in (select nodis_reported_id from kre_heb_c_case_reports_to_delete);
delete from nodis_casereportreported where id in (select nodis_casereportreported_id from kre_heb_c_case_reports_to_delete);
delete from nodis_casereport where id in (select nodis_casereport_id from kre_heb_c_case_reports_to_delete);

delete from nodis_report_cases where case_id in (select case_id from kre_heb_c_cases_to_delete);
delete from nodis_case where id in (select case_id from kre_heb_c_cases_to_delete);
delete from hef_event where name ilike 'lx:hepatitis_c_rna%';
drop table kre_heb_c_cases_to_delete;
drop table kre_heb_c_case_reports_to_delete;


create table kre_hiv_cases_to_delete as 
select distinct case_id from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hiv_rna_viral%');
delete from nodis_case_events where event_id in (select id from hef_event where name ilike 'lx:hiv_rna_viral%')
delete from nodis_case_events where case_id in (select case_id from kre_hiv_cases_to_delete);
delete from nodis_case where id in (select case_id from kre_hiv_cases_to_delete);
delete from hef_event where name ilike 'lx:hiv_rna_viral%';
drop table kre_hiv_cases_to_delete;


-- 3. Run concordance

-- 4. map new labs

IGNORED
HEPATITIS C VIRUS RNA, QUANT, PCR, SERUM OR PLASMA--HCV RNA, QUANTITATIVE REAL TIME PCR LOG IU/ML
137231--MLI-137231 LOG IU/ML
HEPATITIS B DNA, QUANTITATIVE, SERUM--HEPATITIS B VIRUS DNA LOG IU/ML
HEPATITIS B VIRUS, DNA, QUANT, VIRAL LOAD, PCR, SERUM OR PLASMA--HEPATITIS B VIRUS DNA Log IU/mL
HEPATITIS B VIRUS, DNA, QUANT, VIRAL LOAD, PCR, SERUM OR PLASMA--HEPATITIS B VIRUS DNA LOG IU/ML
HEPATITIS C VIRUS RNA, QUANT, PCR, SERUM OR PLASMA--HEPATITS C RNA VIRAL LOAD Log IU/mL
HEPATITIS B VIRUS DNA, VIRAL LOAD, PCR, SERUM--HEPATITIS B VIRUS DNA Log IU/mL
HEPATITIS B VIRUS DNA, VIRAL LOAD, PCR, SERUM--HEPATITIS B VIRUS DNA LOG IU/ML
HEPATITIS C VIRUS RNA, QUANT, PCR, SERUM OR PLASMA--HEPATITS C RNA VIRAL LOAD LOG IU/ML
HEPATITIS C VIRUS RNA, QUANT, PCR, UNSPECIFIED SPECIMEN--HEPATITS C RNA VIRAL LOAD log IU/mL
HEPATITIS C VIRUS RNA, QUANT, PCR, UNSPECIFIED SPECIMEN--HEPATITS C RNA VIRAL LOAD Log IU/mL
HEPATITIS C VIRUS RNA, QUANT, PCR, UNSPECIFIED SPECIMEN--HEPATITS C RNA VIRAL LOAD LOG IU/ML
URINALYSIS, COMPLETE--HEPATITS C RNA VIRAL LOAD Log IU/mL
HIV-1 RNA, QUANTITATIVE, PCR, SERUM OR PLASMA--HIV 1 RNA PCR Log cps/mL
HIV-1 RNA, QUANTITATIVE, PCR, SERUM OR PLASMA--HIV 1 RNA PCR LOG CPS/ML
HIV 1 RNA, VIRAL LOAD, PCR, UNSPECIFIED SPECIMEN--HIV 1 RNA PCR Log cps/mL
HIV 1 RNA, VIRAL LOAD, PCR, UNSPECIFIED SPECIMEN--HIV 1 RNA PCR LOG CPS/ML
203162--MLI-203162
46361--MLI-46361


MAPPED TO hepatitis_b_viral_dna
HEPATITIS B VIRUS, DNA, QUANT, VIRAL LOAD, PCR, SERUM OR PLASMA--HEPATITIS B VIRUS DNA


-- 5. Re-create hef_events

-- 6. Run check for unmapped result_strings (I cleared out a lot of the mappings for the already mapped labs)

--We really should only be mapping positive labs **or** absolutely negative for hiv_rna_viral
-- Just let the others flow through. Let's see how this goes and what DPH says. It may cause some issues for those events with "copies/mL" in the result string itself.
-- We could add these to never_send_config if we needed to?!?!?!


-- 7. Run hef again









