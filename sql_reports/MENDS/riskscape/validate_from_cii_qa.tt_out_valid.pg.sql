select t0.counts as old_counts, 
       t1.counts as new_counts,
       t0.field_name as old_field_name,
       t1.field_name as new_field_name,
       t0.field_level as old_field_level,
       t1.field_level as new_field_level,
       t0.year as old_year,
       t1.year as new_year
from cii_qa.tt_out_valid_5 t0
full outer join cii_qa.tt_out_valid_6t1 
   on t0.field_name=t1.field_name and
      t0.field_level=t1.field_level and
      t0.year=t1.year
where abs(t0.counts-t1.counts) / 
order by t0.field_name, t0.field_level, t0.field_level, t0.year;
       
select t0.counts as old_counts, 
       t1.counts as new_counts,
       t0.field_name as old_field_name,
       t1.field_name as new_field_name,
       t0.field_level as old_field_level,
       t1.field_level as new_field_level,
       t0.year as old_year,
       t1.year as new_year
from cii_qa.tt_out_valid_5 t0
full outer join cii_qa.tt_out_valid_6 t1 
   on t0.field_name=t1.field_name and
      t0.field_level=t1.field_level and
      t0.year=t1.year
where t1.field_name='hypertension' or t0.field_name='hypertension';
       