-- first check patient_id by year, month
-- this should be an empty result set 
-- there should never be more than 1 row per patient (aa) per year (af) per month (ag)
select 'If patient, year, month is not unique, there will be counts here';
select count(*), aa_seq, af, ag
from gen_pop_tools.tt_out2
group by aa_seq, af, ag
having count(*)>1;

select 'this looks at total counts of patient records per year and month';
select 'If the min max difference is large, this must be investigated';
select count(*) as counts, af, ag
from gen_pop_tools.tt_out2
group by af, ag order by af, ag;

--select 'here are counts for sex groups by year.  Same drill';
select count(*) as counts, af, ag, ab
from gen_pop_tools.tt_out
group by af, ag, ab
order by ab, af, ag;

--select 'here are counts for race-ethnicity groups by year.';
select count(*) as counts, af, ag, ac
from gen_pop_tools.tt_out
group by af, ag, ac
order by ac, af, ag;

select 'here are counts for age groups by year.';
select count(*) as counts, af, ag, ah
from gen_pop_tools.tt_out
group by af, ag, ah
order by ah, af, ag;

select 'here are counts for hypertension diagnosed and control status groups by year.';
select count(*) as counts, af, ag, by
from gen_pop_tools.tt_out
group by af, ag, by
order by by, af, ag;

select 'here are counts for clinical hypertension groups by year.';
select count(*) as counts, af, ag, bk
from gen_pop_tools.tt_out
group by af, ag, bk
order by bk, af, ag;
