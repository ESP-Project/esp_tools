select count(*) as rx_counts, count(distinct patient_id) as pat_counts, to_char(date,'yyyy') as year
from public.hef_event h 
where h.name IN ('rx:albuterol', 'rx:albuterol-ipratropium:generic','rx:albuterol-ipratropium:trade',
                 'rx:arformoterol',
                 'rx:beclomethasone',
                 'rx:benralizumab',
                 'rx:budesonide-formoterol:generic','rx::budesonide-formoterol:trade','rx:budesonide-inh',
                 'rx:ciclesonide-inh',
                 'rx:cromolyn-inh',
                 'rx:flunisolide-inh',
                 'rx:fluticasone-salmeterol:generic','rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:generic','rx:fluticasone-vilanterol:trade','rx:fluticasone-inh',
                 'rx:formoterol',
                 'rx:indacaterol',
                 'rx:ipratropium',
                 'rx:levalbuterol',
                 'rx:mepolizumab',
                 'rx:mometasone-formoterol:generic','rx:mometasone-formoterol:trade','rx:mometasone-inh',
                 'rx:montelukast',
                 'rx:omalizumab',
                 'rx:pirbuterol',
                 'rx:reslizumab',
                 'rx:salmeterol',
                 'rx:theophylline',
                 'rx:tiotropium',
                 'rx:zafirlukast',
                 'rx:zileuton')
group by to_char(date,'yyyy');

select count(*) as rx_counts, count(distinct p.patient_id) as pat_counts, p.name as drug_name
from emr_prescription p left join hef_event as h on h.object_id=p.id left join django_content_type ct on ct.id=h.content_type_id and ct.model='prescription'
where h.id is null and (
  p.name ilike '%admonair%'
  or p.name ilike '%ellipta%'
  or p.name ilike '%airduo%'
  or p.name ilike '%air supra%'
  or p.name ilike '%breyna%'
  or p.name ilike '%wixela%'
  or p.name ilike '%trelegy%'
  or (p.name ilike '%budesonide%' and p.name ilike '%albuterol%')
  or (p.name ilike '%fluticasone%' and p.name ilike '%umeclidunium%' and p.name ilike '%vilanterol%')
  )
group by p.name;

select count(*) as rx_counts, count(distinct p.patient_id) as pat_counts, p.name as drug_name
from emr_prescription p left join hef_event as h on h.object_id=p.id left join django_content_type ct on ct.id=h.content_type_id and ct.model='prescription'
where h.id is null and (
  p.name ilike '%duplilumab%'
  or p.name ilike '%dupixent%'
  or p.name ilike '%tezepelumab%'
  or p.name ilike '%tezspire%'
  or p.name ilike '%breyna%'
  )
group by p.name;