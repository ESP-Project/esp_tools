CREATE TEMP TABLE asthma_results (
    natural_key TEXT,
    status TEXT,
    race TEXT,
    ethnicity TEXT,
    date_of_birth TEXT,
    gender TEXT,
    case_date TEXT,
    RN INTEGER,
    category TEXT
); 

-- 2 >= asthma dx, age 4-16, with additional checks on nodis_case and selecting the newest nc.date
WITH T0 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma dx, age 4-16' as category
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id
JOIN
    public.nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma' 
    AND (nc.criteria like 'Criteria #1:%')
    AND nc.date <= '2023-03-31'
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) BETWEEN 4 AND 16
)
INSERT INTO asthma_results
SELECT * FROM T0
WHERE
    rn=1
    AND
    status <> 'D'
ORDER BY 
    RANDOM()
LIMIT 6;


-- 2 >= asthma rx, age 4-16, event date <= March 31, 2023, and additional checks on nodis_case
WITH T1 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma rx, age 4-16' as category
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id
JOIN
    public.nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma' 
    AND (nc.criteria like 'Criteria #2:%')
    AND nc.date <= '2023-03-31'
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) BETWEEN 4 AND 16
)
INSERT INTO asthma_results
SELECT * FROM T1
WHERE
    rn=1
    AND
    status <> 'D'
ORDER BY 
    RANDOM()
LIMIT 6;


-- 2 >= asthma dx, age > 16, with additional checks on nodis_case and selecting the newest nc.date
WITH T2 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma dx, age > 16' as category
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id
JOIN
    public.nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) > 16
    AND (nc.criteria like 'Criteria #1:%')
    AND nc.date <= '2023-03-31'
)
INSERT INTO asthma_results
SELECT * FROM T2
WHERE
    rn=1
    AND
    status <> 'D'
ORDER BY 
    RANDOM()
LIMIT 6;




-- 2 >= asthma rx, age > 16, event date <= March 31, 2023, and additional checks on nodis_case
WITH T3 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma rx, age > 16' as category
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id
JOIN
    public.nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) > 16
    AND (nc.criteria like 'Criteria #2:%')
    AND nc.date <= '2023-03-31'
)
INSERT INTO asthma_results
SELECT * FROM T3
WHERE
    rn=1
    AND
    status <> 'D'
ORDER BY 
    RANDOM()
LIMIT 7;


-- No Asthma (One DX or One RX)
WITH T4_0 AS
(
    SELECT
        h.patient_id, count(*) as counts
    FROM
        hef_event h
    WHERE 
        h.name = 'dx:asthma'
        AND
        h.date < '2023-03-31'
    GROUP BY
        h.patient_id
),
T4_1 AS 
(SELECT
        h.patient_id, count(*) as counts
    FROM
        hef_event h
    WHERE 
        LOWER(h.name) IN
('rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol','rx:formoterol','rx:indacaterol',
'rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh','rx:alvesco',
'rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh',
'rx:intal','rx:omalizumab','rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab',
'rx:theophylline','rx:fluticasone-salmeterol:generic','rx:fluticasone-vilanterol:generic',
'rx:albuterol-ipratropium:generic', 'rx:mometasone-formoterol:generic','rx:budesonide-formoterol:generic',
'rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade','rx:albuterol-ipratropium:trade', 
'rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade') 
        AND
         h.date < '2023-03-31'
     GROUP BY
        h.patient_id
),
T4 AS
(
    SELECT
                p.natural_key,
                p.race,
                p.ethnicity,
                p.date_of_birth::Date,
                UPPER(p.gender) AS gender,
                'One rx or one dx' as category
        FROM
            emr_patient p
        LEFT JOIN
            T4_0 ON T4_0.patient_id = p.id
        LEFT JOIN
            T4_1 ON T4_1.patient_id = p.id
        LEFT JOIN
            nodis_case nc ON p.id = nc.patient_id
        WHERE
            coalesce(T4_0.counts, 0) <=1 and coalesce(T4_1.patient_id,0) <= 1
            AND
            EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) > 4
            and nc.id is null
)
INSERT INTO asthma_results
SELECT natural_key,
    'not a case' as status,
    race,
    ethnicity,
    date_of_birth,
    gender,
    null::date case_date, 
    null::integer as RN,
    category 
FROM T4
ORDER BY 
    RANDOM()
LIMIT 9;

-- No Asthma
WITH T5 AS
(
SELECT distinct
    p.natural_key,
    'NULL' as status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    '2023-03-31'::date as case_date, -- use as-of date
    1 as RN,
    'No Asthma' as category
FROM 
    public.emr_patient p
left JOIN 
    public.hef_event he ON p.id = he.patient_id and he.name in ('dx:asthma',
'rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol','rx:formoterol','rx:indacaterol',
'rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh','rx:alvesco',
'rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh',
'rx:intal','rx:omalizumab','rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab',
'rx:theophylline','rx:fluticasone-salmeterol:generic','rx:fluticasone-vilanterol:generic',
'rx:albuterol-ipratropium:generic', 'rx:mometasone-formoterol:generic','rx:budesonide-formoterol:generic',
'rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade','rx:albuterol-ipratropium:trade', 
'rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade')
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) > 4 and he.id is null
)
INSERT INTO asthma_results
SELECT * FROM T5
ORDER BY 
    RANDOM()
LIMIT 8;

-- Discontinued Asthma
WITH T6 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth::date,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    'Discontinued Asthma' as category
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id AND nc.date < '2023-03-31'
JOIN
    public.nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma' and cah.date < '2023-03-31'
WHERE 
    EXTRACT(YEAR FROM AGE('2023-03-31', p.date_of_birth)) > 4
)
INSERT INTO asthma_results
SELECT * FROM T6
WHERE
    rn=1
    AND
    status = 'D'
ORDER BY 
    RANDOM()
LIMIT 8;


\COPY asthma_results TO '/srv/esp/esp_tools/sql_reports/MENDS/Asthma/stage3.csv' DELIMITER ',' CSV HEADER;



