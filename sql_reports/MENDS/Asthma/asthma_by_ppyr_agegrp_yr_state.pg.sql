with tt_asthma_primary_payer_by_year as
--subquery
(select pse.patient_id, 
  max(tta.asthma) as asthma,
  mode() within group (order by cm.mapped_value) as primary_payer, 
  case 
     when max(pse.age) between 0 and 17 then '17 or less'
     when max(pse.age)>=18 then '18 or more'
  end as age_group,
  substr(pse.year_month,1,4) as year,
  p.state
  --into gen_pop_tools.tt_asthma_primary_payer_by_year
  from gen_pop_tools.tt_pat_seq_enc pse
  join emr_patient p on p.id=pse.patient_id
  left join gen_pop_tools.tt_primary_payer pp on pp.year_month=pse.year_month and pse.patient_id=pp.patient_id
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='primary_payer' and pp.primary_payer=code
  left join gen_pop_tools.tt_asthma tta on tta.patient_id=pse.patient_id and tta.year_month=pse.year_month
  where pse.prior2>=1
  group by pse.patient_id, state, substr(pse.year_month,1,4))
--main query
select count(distinct case when asthma=1 then patient_id end) as asthma_pats, 
       count(distinct patient_id)as all_pats , 
       case when grouping(year)=1 then 'All years' else year end as year, 
       case when grouping(primary_payer)=1 then 'All payers' else primary_payer end as primary_payer, 
       case when grouping(age_group)=1 then 'All ages' else age_group end as age_group, 
       case when grouping(state)=1 then 'All states' else state end as state
--into temp_asthma_results
from (select patient_id, asthma, 
         case when nullif(trim(primary_payer),'') is null then 'Other - Unknown - Self-Pay' else primary_payer end as primary_payer,
         case when nullif(trim(age_group),'') is null then 'Null age' else age_group end as age_group, year, 
         case when nullif(trim(state),'') is null then 'Null state' else state end as state 
      from tt_asthma_primary_payer_by_year where year>='2017') t0
group by grouping sets (
  (year),
  (state),
  (age_group),
  (primary_payer),
  (year, primary_payer),  
  (year, age_group),
  (state, primary_payer ),
  (state, age_group),
  (state, year),
  (age_group, primary_payer),
  (year, age_group, primary_payer));
