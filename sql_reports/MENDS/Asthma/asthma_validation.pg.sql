SELECT 
    p.natural_key,
    p.race,
    p.ethnicity,
    UPPER(p.gender) AS gender,
    p.date_of_birth,
    EXTRACT(YEAR FROM AGE(current_date, p.date_of_birth)) AS age,
    cah.status,
    cah.date as history_date,
    h.date as event_date,
    nc.condition,
    nc.criteria,
    CASE 
        WHEN cah.status = 'D' THEN 'Yes'
        ELSE 'No'
    END AS deactivated,
    h.name AS hef_event_name
FROM 
    public.emr_patient p
JOIN 
    public.nodis_case nc ON p.id = nc.patient_id
JOIN
    public.nodis_caseactivehistory cah ON nc.id = cah.case_id
LEFT JOIN
    public.hef_event h ON p.id = h.patient_id AND h.name IN ('dx:asthma', 'rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
WHERE 
    p.natural_key = 'your_natural_key_value';  -- Replace 'your_natural_key_value' with the actual natural key value of the patient
