DROP TABLE asthma_results;
CREATE TABLE asthma_results (
    natural_key TEXT,
    status TEXT,
    race TEXT,
    ethnicity TEXT,
    dob datetime,
    gender TEXT,
    case_date date,
    RN INTEGER,
    category TEXT
); 

-- 2 >= asthma dx, age 4-16, with additional checks on nodis_case and selecting the newest nc.date
WITH T0 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma dx, age 4-16' as category
FROM 
    emr_patient p
JOIN 
    nodis_case nc ON p.id = nc.patient_id and nc.condition='asthma'
	AND nc.date <= '2023-03-31' 
	and nc.criteria='Criteria #1: Asthma >=2 diagnoses within two years'
JOIN
    nodis_caseactivehistory  cah on nc.id = cah.case_id 
     AND nc.condition = 'asthma' and cah.date<= '2023-03-31'
WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 16
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 6 natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category
	FROM T0
WHERE 
	RN = 1
	AND
	status <> 'D'
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);

	--select * from tempdb.INFORMATION_SCHEMA.COLUMNS
	--WHERE table_name like '#asthma_results'

	--exec sp_columns '#asthma_results'

	--EXEC tempdb.dbo.sp_help @objname = N'#asthma_results';

-- 2 >= asthma rx, age 4-16, event date <= March 31, 2023, and additional checks on nodis_case
WITH T1 AS
(
	SELECT
    	p.natural_key,
	cah.status,
    	p.race,
    	p.ethnicity,
    	p.date_of_birth,
    	UPPER(p.gender) as gender,
		nc.date as case_date, -- Get the newest case date
    	ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
        '2 >= Asthma rx, age 4-16' as category
FROM 
    emr_patient p
JOIN 
    nodis_case nc ON p.id = nc.patient_id and nc.condition='asthma'
        AND nc.date <= '2023-03-31'
	and nc.criteria='Criteria #2: Asthma >=2 prescriptions within two years'
JOIN
    nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
      and cah.date<= '2023-03-31'
WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 16
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 6
	natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category
	FROM T1
WHERE 
	RN = 1
	AND
	status <> 'D'
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);

-- 2 >= asthma dx, age > 16, with additional checks on nodis_case and selecting the newest nc.date
WITH T2 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    '2 >= Asthma dx, age > 16' as category
FROM 
    emr_patient p
JOIN 
    nodis_case nc ON p.id = nc.patient_id AND nc.condition = 'asthma'
        AND nc.date <= '2023-03-31'
	        and nc.criteria='Criteria #1: Asthma >=2 diagnoses within two years'
JOIN
    nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
      and cah.date<= '2023-03-31'

WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 16
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 6 
	natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category FROM T2
WHERE
    RN=1
    AND
    status <> 'D'
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);


-- 2 >= asthma rx, age > 16, event date <= March 31, 2023, and additional checks on nodis_case
WITH T3 AS
(
	SELECT
    	p.natural_key,
		cah.status,
    	p.race,
    	p.ethnicity,
    	p.date_of_birth,
    	UPPER(p.gender) as gender,
		nc.date as case_date, -- Get the newest case date
    	ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
        '2 >= Asthma Rx, age > 16' as category
FROM 
    emr_patient p
JOIN 
    nodis_case nc ON p.id = nc.patient_id AND nc.condition = 'asthma'
        AND nc.date <= '2023-03-31'
	and nc.criteria='Criteria #2: Asthma >=2 prescriptions within two years'
JOIN
    nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
      and cah.date<= '2023-03-31'
WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 > 16
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 7 
	natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category
	FROM T3
WHERE 
	RN = 1
	AND
	status <> 'D'
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);


-- No Asthma (One DX or One RX)
WITH T4_0 AS
(
	SELECT
		h.patient_id
	FROM
		hef_event h
	WHERE 
		h.name = 'dx:asthma'
		AND
		h.date < '2023-03-31'
	GROUP BY
		h.patient_id
	HAVING
		count(*) = 1
),
T4_1 AS 
(SELECT
		h.patient_id
	FROM
		hef_event h
	WHERE 
		LOWER(h.name) IN ('rx:albuterol', 'rx:levalbuterol',
	'rx:pirbuterol','rx:arformoterol','rx:formoterol','rx:indacaterol',
        'rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort',
        'rx:ciclesonide-inh','rx:alvesco','rx:flunisolide-inh','rx:aerobid',
        'rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
		            'rx:montelukast','rx:zafirlukast',
		            'rx:zileuton','rx:ipratropium',
		            'rx:tiotropium',
		            'rx:cromolyn-inh',
		            'rx:intal',
		            'rx:omalizumab',
		            'rx:benralizumab',
		            'rx:mepolizumab',
		            'rx:reslizumab',
		            'rx:dupilumab',
		            'rx:theophylline',
		'rx:fluticasone-salmeterol:generic',
	             'rx:fluticasone-vilanterol:generic',
	             'rx:albuterol-ipratropium:generic', 
	             'rx:mometasone-formoterol:generic',
	             'rx:budesonide-formoterol:generic',
	             'rx:fluticasone-salmeterol:trade',
	             'rx:fluticasone-vilanterol:trade',
	             'rx:albuterol-ipratropium:trade', 
	             'rx:mometasone-formoterol:trade',
	             'rx:budesonide-formoterol:trade')
		AND
 		h.date < '2023-03-31'
 	GROUP BY
		h.patient_id
	HAVING
		count(*) = 1
),
T4 AS
(
	SELECT 
		p.natural_key,
		p.race,
		p.ethnicity,
		p.gender,
		null as status,
		p.date_of_birth as dob,
		null as case_date,
                'One rx or one dx' as category,
		CASE
			WHEN T4_0.patient_id > 0
				THEN '1dx'
			WHEN T4_1.patient_id > 0
				THEN '1rx'
			END AS singleton_type
		FROM
			emr_patient p
		LEFT JOIN
			T4_0 ON T4_0.patient_id = p.id
		LEFT JOIN
			T4_1 ON T4_1.patient_id = p.id
		WHERE
			T4_0.patient_id > 0 OR T4_1.patient_id > 0
            AND
			(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 > 4
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 9 
	natural_key,
    status,
    race,
    ethnicity,
    cast(dob as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    null as RN,
	category
	FROM T4
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);

-- No Asthma
WITH T5 AS
(
SELECT
    p.natural_key,
    null as status,
    p.race,
    p.ethnicity,
    p.date_of_birth,
    UPPER(p.gender) AS gender,
    null as case_date, -- Get the newest case date
    null as RN,
    'No Asthma' as category
FROM 
    emr_patient p
left JOIN 
    nodis_case nc ON p.id = nc.patient_id and nc.condition='asthma'
left join hef_event he on p.id=he.patient_id and
                LOWER(he.name) IN ('rx:albuterol', 'rx:levalbuterol',
        'rx:pirbuterol','rx:arformoterol','rx:formoterol','rx:indacaterol',
        'rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort',
        'rx:ciclesonide-inh','rx:alvesco','rx:flunisolide-inh','rx:aerobid',
        'rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
                            'rx:montelukast','rx:zafirlukast',
                            'rx:zileuton','rx:ipratropium',
                            'rx:tiotropium',
                            'rx:cromolyn-inh',
                            'rx:intal',
                            'rx:omalizumab',
                            'rx:benralizumab',
                            'rx:mepolizumab',
                            'rx:reslizumab',
                            'rx:dupilumab',
                            'rx:theophylline',
                'rx:fluticasone-salmeterol:generic',
                     'rx:fluticasone-vilanterol:generic',
                     'rx:albuterol-ipratropium:generic',
                     'rx:mometasone-formoterol:generic',
                     'rx:budesonide-formoterol:generic',
                     'rx:fluticasone-salmeterol:trade',
                     'rx:fluticasone-vilanterol:trade',
                     'rx:albuterol-ipratropium:trade',
                     'rx:mometasone-formoterol:trade',
                     'rx:budesonide-formoterol:trade')  
WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 > 4
  and he.id is null and nc.id is null
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 8 natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category
	FROM T5
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);

-- Discontinued Asthma
WITH T6 AS
(
SELECT
    p.natural_key,
    cah.status,
    p.race,
    p.ethnicity,
    p.date_of_birth,
    UPPER(p.gender) AS gender,
    nc.date as case_date, -- Get the newest case date
    ROW_NUMBER() OVER (PARTITION BY p.id ORDER BY p.id, cah.date DESC) as RN,
    'Discontinued Asthma' as category
FROM 
    emr_patient p
JOIN 
    nodis_case nc ON p.id = nc.patient_id AND nc.date < '2023-03-31'
JOIN
    nodis_caseactivehistory cah on nc.id = cah.case_id AND nc.condition = 'asthma'
      and cah.date < '2023-03-31'
WHERE 
	(0 + 20230331 - convert(char(8),date_of_birth,112))/10000 > 4
)
INSERT INTO asthma_results (natural_key, status, race, ethnicity, dob, gender, case_date, RN, category)
SELECT TOP 8 natural_key,
    status,
    race,
    ethnicity,
    cast(date_of_birth as date) as dob,
    UPPER(gender) AS gender,
    case_date,
    RN,
	category 
	FROM T6
WHERE
    RN=1
    AND
    status = 'D'
ORDER BY 
    ABS(CAST(CAST(NEWID() AS VARBINARY) AS INT)) / CAST(0x7FFFFFFF AS INT);

SELECT * FROM asthma_results

--\COPY asthma_results TO '/srv/esp/esp_tools/sql_reports/MENDS/Asthma/stage3.csv' DELIMITER ',' CSV HEADER;

