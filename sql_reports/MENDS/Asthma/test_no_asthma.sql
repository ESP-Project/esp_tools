--test the no asthma group.
select distinct(h.name) 
from hef_event h
join emr_patient p on p.id=h.patient_id
where p.natural_key in ()
 and h.name in ('dx:asthma',
        'rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol',
        'rx:formoterol','rx:indacaterol','rx:salmeterol','rx:beclomethasone',
        'rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh','rx:alvesco',
        'rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent',
        'rx:mometasone-inh','rx:asmanex','rx:montelukast','rx:zafirlukast',
        'rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh','rx:intal',
        'rx:omalizumab','rx:benralizumab','rx:mepolizumab','rx:reslizumab',
        'rx:dupilumab','rx:theophylline','rx:fluticasone-salmeterol:generic',
        'rx:fluticasone-vilanterol:generic','rx:albuterol-ipratropium:generic', 
        'rx:mometasone-formoterol:generic','rx:budesonide-formoterol:generic',
        'rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade',
        'rx:albuterol-ipratropium:trade','rx:mometasone-formoterol:trade',
        'rx:budesonide-formoterol:trade')
;