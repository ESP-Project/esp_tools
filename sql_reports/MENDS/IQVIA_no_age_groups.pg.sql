/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 8/03/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Branched development from the ESP version to IQVIA.  Implemented IQVIA logic.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

--Patient immediately qualifies as diabetic if they have 1 inpatient diagnosis of codes 250.x, 357.2, 366.41, 362.01-362.07)
	
SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO TEMPORARY 
	inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';
	
 --drop table inpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO TEMPORARY
	inpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types') and date<='2023-12-31'::date
AND EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
--drop table a1c_results;
select 
	patient_id,
	date,
	'a1c' as event_type
into temporary
	a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5';
    
--drop table fasting_glucose;
select 
	patient_id,
	date,
	'fasting glucose' as event_type
into temporary
	fasting_glucose
from
	hef_event h 
WHERE 
	name in ('lx:glucose-fasting:threshold:gte:126','lx:ogtt50-fasting:threshold:gte:126','lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126');
    
--drop table random_glucose;
select 
	patient_id,
	date,
	'random glucose' as event_type
into temporary
	random_glucose
from
	hef_event h 
where 
	name in ('lx:glucose-random:threshold:gte:200','lx:ogtt50-random:threshold:gte:200');
 
 --drop table outpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO TEMPORARY
	outpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types')
AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
 
--drop table medication_disp 
SELECT patient_id, 
	date,
	name
INTO TEMPORARY
	medication_disp
FROM 
	hef_event AS h 
WHERE 
	lower(name)
	IN 
	(
	'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
	);
    
--drop table diab_2_events;
 select patient_id, min(date) as date
 into temporary diab_2_events
 from (
 select t0.*, lead(date) over (partition by patient_id order by patient_id, date) as next_date,
    lead(crit) over (partition by patient_id order by patient_id, date, crit) as next_crit
 from (
 select patient_id, date, 'a1c' as crit from a1c_results
 union select patient_id, date, 'fast_gluc' as crit from fasting_glucose
 union select patient_id, date, 'rand_gluc' as crit from random_glucose
 union select patient_id, date, 'outp_diag' as crit from outpatient_dx
 union select patient_id, date, 'antihyperg' as crit from medication_disp) t0) t00
 where (date_part('Year',next_date)-date_part('Year',date))*12+(date_part('Year',next_date)-date_part('Year',date)) between 0 and 24
 and (date<next_date or (date<=next_date and crit<>next_crit))
 and next_date<='2022-12-31'::date
 group by patient_id;
 
 --drop table diab_pats;
select patient_id, min(date) as date, 'diabetic' as type
 into temporary diab_pats
 from (
 select patient_id, date from diab_2_events
 union select patient_id, date from inpatient_dx) t0
 group by patient_id;
 
select distinct on (patient_id) patient_id, date, name
into temporary hef_a1c
from hef_event
where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7','lx:a1c:range:gte:7:lt:8')
order by patient_id, date desc,
  case
    when name='lx:a1c:threshold:lt:7' then 1
    when name='lx:a1c:range:gte:7:lt:8' then 2
    when name='lx:a1c:range:gte:8:lte:9' then 3
    when name='lx:a1c:threshold:gt:9' then 4
  end;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id, DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, coalesce(type, 'Not diabetic'::varchar(50)) condition,
    coalesce(name,'Not tested') control
into temporary diab_assessed_control
from pat_w_age t01
left join diab_pats t0 on t0.patient_id=t01.patient_id
left join last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;


-- Creating lab_over_65 temporary table
SELECT DISTINCT patient_id, date, 'lab results > 6.5'
INTO TEMPORARY lab_over_65
FROM emr_labresult t0
JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE result_float >= 6.5 AND date < '2022-12-31'::Date;

-- Query without age group breakdown, corrected for the proper source of 'type'
SELECT
    COUNT(*) AS counts,
    condition,
    control,
    COUNT(CASE WHEN lo65_patient_id IS NOT NULL THEN 1 END) AS "a1c >= 6.5"
FROM 
    (
        SELECT 
            t0.patient_id,
            COALESCE(t0.type, 'Not diabetic') AS condition,
            COALESCE(t1.name, 'Not tested') AS control,
            lo65.patient_id AS lo65_patient_id
        FROM 
            diab_pats t0
        LEFT JOIN 
            last_hef_a1c t1 ON t0.patient_id = t1.patient_id
        LEFT JOIN
            lab_over_65 lo65 ON t0.patient_id = lo65.patient_id
    ) subquery
GROUP BY 
    condition, control
ORDER BY 
    CASE
        WHEN control = 'lx:a1c:threshold:lt:7' THEN 1
        WHEN control = 'lx:a1c:range:gte:7:lt:8' THEN 2
        WHEN control = 'lx:a1c:range:gte:8:lte:9' THEN 3
        WHEN control = 'lx:a1c:threshold:gt:9' THEN 4
        WHEN control = 'Not tested' THEN 5
    END;
