WITH age_data AS (
  SELECT
    dob,
    EXTRACT(YEAR FROM AGE(dob, '2023-09-30')) AS age
  FROM tmp_dob
)

SELECT
  age_group,
  COUNT(*) AS counts
FROM (
  SELECT
    CASE
      WHEN age BETWEEN 0 AND 7 THEN '0-7'
      WHEN age BETWEEN 8 AND 19 THEN '8-19'
      WHEN age BETWEEN 20 AND 34 THEN '20-34'
      WHEN age BETWEEN 35 AND 54 THEN '35-54'
      WHEN age BETWEEN 55 AND 74 THEN '55-74'
      WHEN age >= 75 THEN 'over 75'
    END AS age_group,
    age
  FROM age_data
) AS age_group_data
GROUP BY age_group
ORDER BY age_group;

/* 
SELECT COUNT(*) AS counts,
  CASE
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 0 AND 7 THEN '0-7'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 8 AND 19 THEN '8-19'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 20 AND 34 THEN '20-34'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 35 AND 54 THEN '35-54'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 55 AND 74 THEN '55-74'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) >= 75 THEN 'over 75'
  END AS age_groups
FROM emr_patient
GROUP BY
  CASE
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 0 AND 7 THEN '0-7'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 8 AND 19 THEN '8-19'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 20 AND 34 THEN '20-34'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 35 AND 54 THEN '35-54'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) BETWEEN 55 AND 74 THEN '55-74'
    WHEN date_part('year', AGE(date_of_birth, '2023-09-30')) >= 75 THEN 'over 75'
  END;



WITH age_data AS (
  SELECT
    date_of_birth,
    DATEDIFF(YEAR, date_of_birth, '2023-09-30') AS age
  FROM emr_patient
)

SELECT
  age_group,
  COUNT(*) AS counts
FROM (
  SELECT
    CASE
      WHEN age BETWEEN 0 AND 7 THEN '0-7'
      WHEN age BETWEEN 8 AND 19 THEN '8-19'
      WHEN age BETWEEN 20 AND 34 THEN '20-34'
      WHEN age BETWEEN 35 AND 54 THEN '35-54'
      WHEN age BETWEEN 55 AND 74 THEN '55-74'
      WHEN age >= 75 THEN 'over 75'
    END AS age_group,
    age
  FROM age_data
) AS age_group_data
GROUP BY age_group
ORDER BY age_group;


*/
