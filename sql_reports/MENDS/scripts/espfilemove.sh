#!/bin/bash
## Simple schell script to move and rename files from INITIAL_DIR to DATA_DIR 

INITIAL_DIR=/srv/esp/data/epic/espin/
DATA_DIR=/srv/esp/data/epic/incoming
runtime=`date +%Y%m%d_%H%M`
LOGFILE=/srv/esp/logs/espfile_move.log.$runtime

exec 5>&1 6>&2 >>$LOGFILE 2>&1
echo "\n\n\nStarting script"
echo "Incoming dir is $INITIAL_DIR"
echo "Destination dir is $DATA_DIR"

oldFiles=()
while IFS= read -r -d $'\0' foundFile; do
    oldFiles+=("$foundFile")
done < <(find "$INITIAL_DIR" -maxdepth 1 -type f -name "epic*" -print0 2> /dev/null)


if [[ ${#oldFiles[@]} -ne 0 ]]; then
    for file in "${oldFiles[@]}"; do
    ## extract date from file and reformat if needed
	##rundate=`echo  "$file" | cut -f 2 -d "_" | cut -f 1 -d "." | awk -F "" '{print $5$6$7$8$1$2$3$4}'`
	## Standard date is YYYYMMDD
	##rundate=`echo  "$file" | cut -f 2 -d "_" | cut -f 1 -d "." | awk -F "" '{print $5$6$7$8$1$2$3$4}'`
     echo "File is $file"
     echo "Rundate is $rundate"
     case $file in
	*Member*)
		echo "Member file - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*Visit*)
		echo "Visit file - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		#sed -i "s/ICD10/icd10/g" $DATA_DIR/epicvis.esp.$rundate
		;;
	*Immunization*)
		echo "Immunization file! - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*Lab*)
		echo "LabResult file! - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*Medication*)
		echo "Medication file! - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*Social*)
		echo "Social file! - $file"
		mv $file $DATA_DIR
		# next line will remove the first record - use it to remove a header row if needed
		#sed -i 1d $DATA_DIR/epicsoc.esp.$rundate 
		# the next line will remove all double-quotes
		#sed -i "s/\"//g" $DATA_DIR/epicsoc.esp.$rundate
		;;
	*)
		echo "Unknown File Found!! - $file"
		;;
     esac
    done
fi

echo "Exiting script \n\n"
exit

