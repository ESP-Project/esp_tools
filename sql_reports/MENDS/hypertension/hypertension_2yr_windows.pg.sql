--end stage renal events
select distinct on (patient_id) patient_id, date, id as event_id 
into temporary tmp_esr_diag 
from hef_event e0 
where name='dx:endstagerenal' 
order by patient_id, date, id;
--pregnancy 
with tmp_preg as (
  select patient_id, date, id as event_id from hef_event e 
  where name='dx:pregnancy'),
t00 as (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
              lag(date) over (partition by patient_id order by date) dt_i 
        from tmp_preg 
        group by patient_id, date),  
t0 as (select patient_id, date, i, dt_i, 
         sum(case when dt_i + interval '1 year' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
       from  t00)
select patient_id, min(date) as start_date, max(date) + interval '1 year' as end_date 
into temporary tmp_preg_seqs 
from  t0 
group by patient_id, grp order by patient_id, start_date;
--inpatient encounters
select e.id, patient_id, date 
into temporary temp_inpat_encs 
from emr_encounter e 
join gen_pop_tools.rs_conf_mapping cm on cm.src_value=e.raw_encounter_type 
  and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
group by e.id, patient_id, date;
--hypertension diagnoses, not after esr diagnosis or during pregnancies
Select id as event_id, object_id, patient_id, date 
into temporary tmp_hypert_diag 
from hef_event e 
where not exists (select null 
        from temp_inpat_encs ie 
        where ie.id=e.object_id) 
      and e.name = 'dx:hypertension'
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date);
--antihypertensive Rx not on inpatient days and not after ESR diagnosis or during pregnancy
select patient_id, date, id as event_id 
into temporary tmp_antihypert_presc 
from hef_event e 
where name in ('rx:hydrochlorothiazide', 'rx:chlorthalidone', 'rx:indapamide', 'rx:amlodipine', 'rx:clevidipine', 'rx:felodipine', 
'rx:isradipine', 'rx:nicardipine', 'rx:nifedipine', 'rx:nisoldipine', 'rx:diltiazem', 'rx:verapamil', 'rx:acebutelol', 'rx:atenolol', 
'rx:betaxolol', 'rx:bisoprolol', 'rx:carvedilol', 'rx:labetalol', 'rx:metoprolol', 'rx:nadolol', 'rx:nebivolol', 'rx:pindolol', 
'rx:propranolol', 'rx:benazepril', 'rx:captopril', 'rx:enalapril', 'rx:fosinopril', 'rx:lisinopril', 'rx:moexipril', 'rx:perindopril', 
'rx:quinapril', 'rx:ramipril', 'rx:trandolapril', 'rx:candesartan', 'rx:eprosartan', 'rx:irbesartan', 'rx:losartan', 'rx:olmesartan', 
'rx:telmisartan', 'rx:valsartan', 'rx:clonidine', 'rx:doxazosin', 'rx:guanfacine', 'rx:methyldopa', 'rx:prazosin', 'rx:terazosin', 
'rx:eplerenone', 'rx:spironolactone', 'rx:aliskiren', 'rx:hydralazine') 
  and not exists (select null from temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date);
create index tmp_antihypert_presc_patid_date_idx on tmp_antihypert_presc (patient_id, date);
analyse tmp_antihypert_presc;
--high BP days not from inpatient encounters, not after esr or during pregnancy
select patient_id, date, id as event_id 
into temporary tmp_highbp 
from hef_event e where name='enc:highbp' 
  and not exists (select null from temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date) 
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date);
--2 high-bp
Select distinct patient_id, date, event_id 
into temporary tmp_2highbp 
from (select patient_id, date, lag(date) over (partition by patient_id order by date) as first_date, event_id 
      from tmp_highbp) t0 where date_part('year', age(date,first_date)) = 0;
      
--Here are the case-producing events.
select patient_id, date, criteria 
into temporary tmp_ccases 
from (select *, row_number() over (partition by patient_id order by date) as rownumber 
      from (select patient_id, date, criteria 
  from (select patient_id, date, 'Criteria #3: Rx for  anithypertensive'::varchar(2000) as criteria 
        from tmp_antihypert_presc 
        Union
        select patient_id, date, 'Criteria #2: Dx hypertension'::varchar(2000) as criteria 
        from tmp_hypert_diag 
        union select patient_id, date, 'Criteria #1: SBP>=140 or DBP>=90 on 2 or more occasions in a year'::varchar(2000) as criteria 
        from tmp_2highbp) t0 ) t00) t000;
        
with t00 as (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
              lag(date) over (partition by patient_id order by date) dt_i 
        from tmp_ccases 
        group by patient_id, date)
        ,  t0 as (
        select patient_id, date, i, dt_i, 
         sum(case when dt_i + interval '2 years' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
       from  t00 order by patient_id, date)
select patient_id, min(date) as start_date, 
       case 
         when max(date)+interval '23 months' >= current_date then current_date 
         else max(date)+interval '23 months' 
       end as end_date 
into temporary tmp_case_timespans 
from  t0 where date <= '2024-03-31'::date
group by patient_id, grp order by patient_id, start_date;
--here are not high BPs not after esr, or during pregnancy, or taken on inpatient days
select patient_id, date, id as event_id 
into temporary tmp_not_highbp 
from hef_event e where name in ('enc:not_highbp_c','enc:not_highbp_w') 
  and not exists (select null from temp_inpat_encs ie 
        where ie.patient_id=e.patient_id and ie.date=e.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date);
--all bps collected
select * 
into temporary tmp_bp from (
Select patient_id, date, 'C' as status 
from tmp_not_highbp
union 
Select patient_id, date, 'U' as status 
from tmp_highbp) t0;

select patient_id, to_char(date,'yyyy_mm') as year_month
into temporary bp_encs_ym
from emr_encounter e 
where not exists (select null from temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date)
  and (bp_diastolic>0 and bp_systolic>0)
group by patient_id, to_char(date,'yyyy_mm');
create unique index bp_encs_by_month_idx on bp_encs_ym (patient_id, year_month);
analyze bp_encs_ym;

--hypertension by month
select distinct on (t0.patient_id, t0.year_month)
  t0.patient_id, case when t2.patient_id is null then 'NH' else 'H' end as status, t0.year_month
into temporary hypert_by_month
from gen_pop_tools.tt_pat_seq_enc t0
left join tmp_case_timespans t2 on t2.patient_id=t0.patient_id 
  and t0.year_month between to_char(t2.start_date,'yyyy_mm') and to_char(t2.end_date,'yyyy_mm')
where exists (select null from bp_encs_ym be 
              where to_date(be.year_month,'yyyy_mm') between to_date(t0.year_month,'yyyy_mm') - interval '23 months' and to_date(t0.year_month,'yyyy_mm') 
              and be.patient_id=t0.patient_id)
      and t0.age>=20
order by t0.patient_id, t0.year_month, case when t2.patient_id is null then 'NH' else 'H' end;
create unique index hypert_by_month_idx on hypert_by_month (patient_id, year_month);
analyze hypert_by_month;

drop table if exists gen_pop_tools.hypert_2yrwindow;
select distinct on (t0.patient_id, t0.year_month)
  t0.patient_id, case when t2.status is not null and t0.status = 'H' then t2.status 
                      when t2.status is null and t0.status = 'H' then 'UK'
                      else t0.status 
                 end as status, t0.year_month
into gen_pop_tools.hypert_2yrwindow
from hypert_by_month t0
left join tmp_bp t2 on t2.patient_id=t0.patient_id and t2.date between to_date(t0.year_month,'yyyy_mm') - interval '23 months' and to_date(t0.year_month,'yyyy_mm')
order by t0.patient_id, t0.year_month, t2.date desc; -- 50,034,683

drop table if exists gen_pop_tools.hypert_2yrwindow_by_crit;
select distinct on (t0.patient_id, t0.year_month)
  t0.patient_id, t0.year_month, t1.criteria, t1.date
into gen_pop_tools.hypert_2yrwindow_by_crit
from gen_pop_tools.hypert_2yrwindow t0
left join tmp_ccases t1 on t1.patient_id=t0.patient_id and t1.date<=to_date(year_month,'yyyy_mm')+interval '1 month'
 and t0.status<>'NH'
order by t0.patient_id, t0.year_month, t1.date desc;

select count(*), year_month, status
into temporary hypert_2yrwindow_counts
from gen_pop_tools.hypert_2yrwindow
group by year_month, status
order by year_month, status;

select (t0.count + t1.count + t2.count)::float / (t1.count + t1.count + t2.count + t3.count)::float as prevalence,
       t0.year_month
from (select * from hypert_2yrwindow_counts where status='C') t0
join (select * from hypert_2yrwindow_counts where status='U') t1 on t0.year_month=t1.year_month
join (select * from hypert_2yrwindow_counts where status='UK') t2 on t0.year_month=t2.year_month
join (select * from hypert_2yrwindow_counts where status='NH') t3 on t0.year_month=t3.year_month;

select count(*), year_month, criteria
into temporary hypert_2yrwindow_counts_source
from gen_pop_tools.hypert_2yrwindow_by_crit
group by year_month, criteria
order by year_month, criteria;

select t0.year_month, t0.count as rx_counts, t1.count as dx_counts, t2.count as bp2_counts, 
 t0.count + t1.count + t2.count + t3.count as cohort_counts
from (select * from hypert_2yrwindow_counts_source where criteria='Criteria #3: Rx for  anithypertensive') t0
join (select * from hypert_2yrwindow_counts_source where criteria='Criteria #2: Dx hypertension') t1
  on t0.year_month=t1.year_month
join (select * from hypert_2yrwindow_counts_source where criteria='Criteria #1: SBP>=140 or DBP>=90 on 2 or more occasions in a year') t2
  on t0.year_month=t2.year_month
join (select * from hypert_2yrwindow_counts_source where criteria is null) t3
  on t0.year_month=t3.year_month
order by year_month;


select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct e.patient_id) as dx_pat_count,
       count(dx_id) as dx_count,
       to_char(coalesce(ce.date,e.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select e.patient_id, dx.id as dx_id, e.date 
           from emr_encounter e 
           join emr_encounter_dx_codes dx on dx.encounter_id=e.id) e
on e.patient_id=ce.patient_id and e.date=ce.date and ce.source='enc'
group by to_char(coalesce(ce.date,e.date),'yyyy');

select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct e.patient_id) as bp_pat_count,
       count(e.id) as bp_count,
       to_char(coalesce(ce.date,e.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select e.patient_id, e.id, e.date 
           from emr_encounter e 
           where bp_systolic is not null and bp_diastolic is not null) e
on e.patient_id=ce.patient_id and e.date=ce.date and ce.source='enc'
group by to_char(coalesce(ce.date,e.date),'yyyy');

select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct p.patient_id) as rx_pat_count,
       count(p.id) as rx_count,
       to_char(coalesce(ce.date,p.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select p.patient_id, p.id, p.date 
           from emr_prescription p) p
on p.patient_id=ce.patient_id and p.date=ce.date and ce.source='rx'
group by to_char(coalesce(ce.date,p.date),'yyyy');