--The algorithm specifies that we don't want pregnant patients, or patients with end stage renal disease.
--We identify those patients and periods first.
--pregnancy timespans.  We use the VSAC icd10 list for the CMS eCQM 165
select encounter_id into temporary tmp_pregdx from emr_encounter_dx_codes dx 
             where dx_code_id in 
             ('icd10:O09.00','icd10:O09.01','icd10:O09.02','icd10:O09.03','icd10:O09.10','icd10:O09.11','icd10:O09.12','icd10:O09.13',
'icd10:O09.211','icd10:O09.212','icd10:O09.213','icd10:O09.219','icd10:O09.291','icd10:O09.292','icd10:O09.293',
'icd10:O09.299','icd10:O09.30','icd10:O09.31','icd10:O09.32','icd10:O09.33','icd10:O09.40','icd10:O09.41','icd10:O09.42',
'icd10:O09.43','icd10:O09.511','icd10:O09.512','icd10:O09.513','icd10:O09.519','icd10:O09.521','icd10:O09.522',
'icd10:O09.523','icd10:O09.529','icd10:O09.611','icd10:O09.612','icd10:O09.613','icd10:O09.619','icd10:O09.621',
'icd10:O09.622','icd10:O09.623','icd10:O09.629','icd10:O09.70','icd10:O09.71','icd10:O09.72','icd10:O09.73',
'icd10:O09.811','icd10:O09.812','icd10:O09.813','icd10:O09.819','icd10:O09.821','icd10:O09.822','icd10:O09.823',
'icd10:O09.829','icd10:O09.891','icd10:O09.892','icd10:O09.893','icd10:O09.899','icd10:O09.90','icd10:O09.91',
'icd10:O09.92','icd10:O09.93','icd10:O09.A0','icd10:O09.A1','icd10:O09.A2','icd10:O09.A3','icd10:O10.011','icd10:O10.012',
'icd10:O10.013','icd10:O10.019','icd10:O10.111','icd10:O10.112','icd10:O10.113','icd10:O10.119','icd10:O10.211',
'icd10:O10.212','icd10:O10.213','icd10:O10.219','icd10:O10.311','icd10:O10.312','icd10:O10.313','icd10:O10.319',
'icd10:O10.411','icd10:O10.412','icd10:O10.413','icd10:O10.419','icd10:O10.911','icd10:O10.912','icd10:O10.913',
'icd10:O10.919','icd10:O11.1','icd10:O11.2','icd10:O11.3','icd10:O11.9','icd10:O12.00','icd10:O12.01','icd10:O12.02',
'icd10:O12.03','icd10:O12.10','icd10:O12.11','icd10:O12.12','icd10:O12.13','icd10:O12.20','icd10:O12.21','icd10:O12.22',
'icd10:O12.23','icd10:O13.1','icd10:O13.2','icd10:O13.3','icd10:O13.9','icd10:O14.00','icd10:O14.02','icd10:O14.03',
'icd10:O14.10','icd10:O14.12','icd10:O14.13','icd10:O14.20','icd10:O14.22','icd10:O14.23','icd10:O14.90','icd10:O14.92',
'icd10:O14.93','icd10:O15.00','icd10:O15.02','icd10:O15.03','icd10:O16.1','icd10:O16.2','icd10:O16.3','icd10:O16.9',
'icd10:O20.0','icd10:O20.8','icd10:O20.9','icd10:O21.0','icd10:O21.1','icd10:O21.2','icd10:O21.8','icd10:O21.9',
'icd10:O22.00','icd10:O22.01','icd10:O22.02','icd10:O22.03','icd10:O22.10','icd10:O22.11','icd10:O22.12','icd10:O22.13',
'icd10:O22.20','icd10:O22.21','icd10:O22.22','icd10:O22.23','icd10:O22.30','icd10:O22.31','icd10:O22.32','icd10:O22.33',
'icd10:O22.40','icd10:O22.41','icd10:O22.42','icd10:O22.43','icd10:O22.50','icd10:O22.51','icd10:O22.52','icd10:O22.53',
'icd10:O22.8X1','icd10:O22.8X2','icd10:O22.8X3','icd10:O22.8X9','icd10:O22.90','icd10:O22.91','icd10:O22.92',
'icd10:O22.93','icd10:O23.00','icd10:O23.01','icd10:O23.02','icd10:O23.03','icd10:O23.10','icd10:O23.11','icd10:O23.12',
'icd10:O23.13','icd10:O23.20','icd10:O23.21','icd10:O23.22','icd10:O23.23','icd10:O23.30','icd10:O23.31','icd10:O23.32',
'icd10:O23.33','icd10:O23.40','icd10:O23.41','icd10:O23.42','icd10:O23.43','icd10:O23.511','icd10:O23.512','icd10:O23.513',
'icd10:O23.519','icd10:O23.521','icd10:O23.522','icd10:O23.523','icd10:O23.529','icd10:O23.591','icd10:O23.592',
'icd10:O23.593','icd10:O23.599','icd10:O23.90','icd10:O23.91','icd10:O23.92','icd10:O23.93','icd10:O24.011',
'icd10:O24.012','icd10:O24.013','icd10:O24.019','icd10:O24.111','icd10:O24.112','icd10:O24.113','icd10:O24.119',
'icd10:O24.311','icd10:O24.312','icd10:O24.313','icd10:O24.319','icd10:O24.410','icd10:O24.414','icd10:O24.415',
'icd10:O24.419','icd10:O24.811','icd10:O24.812','icd10:O24.813','icd10:O24.819','icd10:O24.911','icd10:O24.912',
'icd10:O24.913','icd10:O24.919','icd10:O25.10','icd10:O25.11','icd10:O25.12','icd10:O25.13','icd10:O26.00','icd10:O26.01',
'icd10:O26.02','icd10:O26.03','icd10:O26.10','icd10:O26.11','icd10:O26.12','icd10:O26.13','icd10:O26.20','icd10:O26.21',
'icd10:O26.22','icd10:O26.23','icd10:O26.30','icd10:O26.31','icd10:O26.32','icd10:O26.33','icd10:O26.40','icd10:O26.41',
'icd10:O26.42','icd10:O26.43','icd10:O26.50','icd10:O26.51','icd10:O26.52','icd10:O26.53','icd10:O26.611','icd10:O26.612',
'icd10:O26.613','icd10:O26.619','icd10:O26.711','icd10:O26.712','icd10:O26.713','icd10:O26.719','icd10:O26.811',
'icd10:O26.812','icd10:O26.813','icd10:O26.819','icd10:O26.821','icd10:O26.822','icd10:O26.823','icd10:O26.829',
'icd10:O26.831','icd10:O26.832','icd10:O26.833','icd10:O26.839','icd10:O26.841','icd10:O26.842','icd10:O26.843',
'icd10:O26.849','icd10:O26.851','icd10:O26.852','icd10:O26.853','icd10:O26.859','icd10:O26.86','icd10:O26.872',
'icd10:O26.873','icd10:O26.879','icd10:O26.891','icd10:O26.892','icd10:O26.893','icd10:O26.899','icd10:O26.90',
'icd10:O26.91','icd10:O26.92','icd10:O26.93','icd10:O28.0','icd10:O28.1','icd10:O28.2','icd10:O28.3','icd10:O28.4',
'icd10:O28.5','icd10:O28.8','icd10:O28.9','icd10:O29.011','icd10:O29.012','icd10:O29.013','icd10:O29.019','icd10:O29.021',
'icd10:O29.022','icd10:O29.023','icd10:O29.029','icd10:O29.091','icd10:O29.092','icd10:O29.093','icd10:O29.099',
'icd10:O29.111','icd10:O29.112','icd10:O29.113','icd10:O29.119','icd10:O29.121','icd10:O29.122','icd10:O29.123',
'icd10:O29.129','icd10:O29.191','icd10:O29.192','icd10:O29.193','icd10:O29.199','icd10:O29.211','icd10:O29.212',
'icd10:O29.213','icd10:O29.219','icd10:O29.291','icd10:O29.292','icd10:O29.293','icd10:O29.299','icd10:O29.3X1',
'icd10:O29.3X2','icd10:O29.3X3','icd10:O29.3X9','icd10:O29.40','icd10:O29.41','icd10:O29.42','icd10:O29.43','icd10:O29.5X1',
'icd10:O29.5X2','icd10:O29.5X3','icd10:O29.5X9','icd10:O29.60','icd10:O29.61','icd10:O29.62','icd10:O29.63','icd10:O29.8X1',
'icd10:O29.8X2','icd10:O29.8X3','icd10:O29.8X9','icd10:O29.90','icd10:O29.91','icd10:O29.92','icd10:O29.93','icd10:O30.001',
'icd10:O30.002','icd10:O30.003','icd10:O30.009','icd10:O30.011','icd10:O30.012','icd10:O30.013','icd10:O30.019',
'icd10:O30.021','icd10:O30.022','icd10:O30.023','icd10:O30.029','icd10:O30.031','icd10:O30.032','icd10:O30.033',
'icd10:O30.039','icd10:O30.041','icd10:O30.042','icd10:O30.043','icd10:O30.049','icd10:O30.091','icd10:O30.092',
'icd10:O30.093','icd10:O30.099','icd10:O30.101','icd10:O30.102','icd10:O30.103','icd10:O30.109','icd10:O30.111',
'icd10:O30.112','icd10:O30.113','icd10:O30.119','icd10:O30.121','icd10:O30.122','icd10:O30.123','icd10:O30.129',
'icd10:O30.131','icd10:O30.132','icd10:O30.133','icd10:O30.139','icd10:O30.191','icd10:O30.192','icd10:O30.193',
'icd10:O30.199','icd10:O30.201','icd10:O30.202','icd10:O30.203','icd10:O30.209','icd10:O30.211','icd10:O30.212',
'icd10:O30.213','icd10:O30.219','icd10:O30.221','icd10:O30.222','icd10:O30.223','icd10:O30.229','icd10:O30.231',
'icd10:O30.232','icd10:O30.233','icd10:O30.239','icd10:O30.291','icd10:O30.292','icd10:O30.293','icd10:O30.299',
'icd10:O30.801','icd10:O30.802','icd10:O30.803','icd10:O30.809','icd10:O30.811','icd10:O30.812','icd10:O30.813',
'icd10:O30.819','icd10:O30.821','icd10:O30.822','icd10:O30.823','icd10:O30.829','icd10:O30.831','icd10:O30.832',
'icd10:O30.833','icd10:O30.839','icd10:O30.891','icd10:O30.892','icd10:O30.893','icd10:O30.899','icd10:O30.90','icd10:O30.91',
'icd10:O30.92','icd10:O30.93','icd10:O31.00X0','icd10:O31.00X1','icd10:O31.00X2','icd10:O31.00X3','icd10:O31.00X4',
'icd10:O31.00X5','icd10:O31.00X9','icd10:O31.01X0','icd10:O31.01X1','icd10:O31.01X2','icd10:O31.01X3','icd10:O31.01X4',
'icd10:O31.01X5','icd10:O31.01X9','icd10:O31.02X0','icd10:O31.02X1','icd10:O31.02X2','icd10:O31.02X3','icd10:O31.02X4',
'icd10:O31.02X5','icd10:O31.02X9','icd10:O31.03X0','icd10:O31.03X1','icd10:O31.03X2','icd10:O31.03X3','icd10:O31.03X4',
'icd10:O31.03X5','icd10:O31.03X9','icd10:O31.10X0','icd10:O31.10X1','icd10:O31.10X2','icd10:O31.10X3','icd10:O31.10X4',
'icd10:O31.10X5','icd10:O31.10X9','icd10:O31.11X0','icd10:O31.11X1','icd10:O31.11X2','icd10:O31.11X3','icd10:O31.11X4',
'icd10:O31.11X5','icd10:O31.11X9','icd10:O31.12X0','icd10:O31.12X1','icd10:O31.12X2','icd10:O31.12X3','icd10:O31.12X4',
'icd10:O31.12X5','icd10:O31.12X9','icd10:O31.13X0','icd10:O31.13X1','icd10:O31.13X2','icd10:O31.13X3','icd10:O31.13X4',
'icd10:O31.13X5','icd10:O31.13X9','icd10:O31.20X0','icd10:O31.20X1','icd10:O31.20X2','icd10:O31.20X3','icd10:O31.20X4',
'icd10:O31.20X5','icd10:O31.20X9','icd10:O31.21X0','icd10:O31.21X1','icd10:O31.21X2','icd10:O31.21X3','icd10:O31.21X4',
'icd10:O31.21X5','icd10:O31.21X9','icd10:O31.22X0','icd10:O31.22X1','icd10:O31.22X2','icd10:O31.22X3','icd10:O31.22X4',
'icd10:O31.22X5','icd10:O31.22X9','icd10:O31.23X0','icd10:O31.23X1','icd10:O31.23X2','icd10:O31.23X3','icd10:O31.23X4',
'icd10:O31.23X5','icd10:O31.23X9','icd10:O31.30X0','icd10:O31.30X1','icd10:O31.30X2','icd10:O31.30X3','icd10:O31.30X4',
'icd10:O31.30X5','icd10:O31.30X9','icd10:O31.31X0','icd10:O31.31X1','icd10:O31.31X2','icd10:O31.31X3','icd10:O31.31X4',
'icd10:O31.31X5','icd10:O31.31X9','icd10:O31.32X0','icd10:O31.32X1','icd10:O31.32X2','icd10:O31.32X3','icd10:O31.32X4',
'icd10:O31.32X5','icd10:O31.32X9','icd10:O31.33X0','icd10:O31.33X1','icd10:O31.33X2','icd10:O31.33X3','icd10:O31.33X4',
'icd10:O31.33X5','icd10:O31.33X9','icd10:O31.8X10','icd10:O31.8X11','icd10:O31.8X12','icd10:O31.8X13','icd10:O31.8X14',
'icd10:O31.8X15','icd10:O31.8X19','icd10:O31.8X20','icd10:O31.8X21','icd10:O31.8X22','icd10:O31.8X23','icd10:O31.8X24',
'icd10:O31.8X25','icd10:O31.8X29','icd10:O31.8X30','icd10:O31.8X31','icd10:O31.8X32','icd10:O31.8X33','icd10:O31.8X34',
'icd10:O31.8X35','icd10:O31.8X39','icd10:O31.8X90','icd10:O31.8X91','icd10:O31.8X92','icd10:O31.8X93','icd10:O31.8X94',
'icd10:O31.8X95','icd10:O31.8X99','icd10:O32.0XX0','icd10:O32.0XX1','icd10:O32.0XX2','icd10:O32.0XX3','icd10:O32.0XX4',
'icd10:O32.0XX5','icd10:O32.0XX9','icd10:O32.1XX0','icd10:O32.1XX1','icd10:O32.1XX2','icd10:O32.1XX3','icd10:O32.1XX4',
'icd10:O32.1XX5','icd10:O32.1XX9','icd10:O32.2XX0','icd10:O32.2XX1','icd10:O32.2XX2','icd10:O32.2XX3','icd10:O32.2XX4',
'icd10:O32.2XX5','icd10:O32.2XX9','icd10:O32.3XX0','icd10:O32.3XX1','icd10:O32.3XX2','icd10:O32.3XX3','icd10:O32.3XX4',
'icd10:O32.3XX5','icd10:O32.3XX9','icd10:O32.4XX0','icd10:O32.4XX1','icd10:O32.4XX2','icd10:O32.4XX3','icd10:O32.4XX4',
'icd10:O32.4XX5','icd10:O32.4XX9','icd10:O32.6XX0','icd10:O32.6XX1','icd10:O32.6XX2','icd10:O32.6XX3','icd10:O32.6XX4',
'icd10:O32.6XX5','icd10:O32.6XX9','icd10:O32.8XX0','icd10:O32.8XX1','icd10:O32.8XX2','icd10:O32.8XX3','icd10:O32.8XX4',
'icd10:O32.8XX5','icd10:O32.8XX9','icd10:O32.9XX0','icd10:O32.9XX1','icd10:O32.9XX2','icd10:O32.9XX3','icd10:O32.9XX4',
'icd10:O32.9XX5','icd10:O32.9XX9','icd10:O33.0','icd10:O33.1','icd10:O33.2','icd10:O33.3XX0','icd10:O33.3XX1','icd10:O33.3XX2',
'icd10:O33.3XX3','icd10:O33.3XX4','icd10:O33.3XX5','icd10:O33.3XX9','icd10:O33.4XX0','icd10:O33.4XX1','icd10:O33.4XX2',
'icd10:O33.4XX3','icd10:O33.4XX4','icd10:O33.4XX5','icd10:O33.4XX9','icd10:O33.5XX0','icd10:O33.5XX1','icd10:O33.5XX2',
'icd10:O33.5XX3','icd10:O33.5XX4','icd10:O33.5XX5','icd10:O33.5XX9','icd10:O33.6XX0','icd10:O33.6XX1','icd10:O33.6XX2',
'icd10:O33.6XX3','icd10:O33.6XX4','icd10:O33.6XX5','icd10:O33.6XX9','icd10:O33.7XX0','icd10:O33.7XX1','icd10:O33.7XX2',
'icd10:O33.7XX3','icd10:O33.7XX4','icd10:O33.7XX5','icd10:O33.7XX9','icd10:O33.8','icd10:O33.9','icd10:O34.00','icd10:O34.01',
'icd10:O34.02','icd10:O34.03','icd10:O34.10','icd10:O34.11','icd10:O34.12','icd10:O34.13','icd10:O34.211','icd10:O34.212',
'icd10:O34.219','icd10:O34.29','icd10:O34.30','icd10:O34.31','icd10:O34.32','icd10:O34.33','icd10:O34.40','icd10:O34.41',
'icd10:O34.42','icd10:O34.43','icd10:O34.511','icd10:O34.512','icd10:O34.513','icd10:O34.519','icd10:O34.521','icd10:O34.522',
'icd10:O34.523','icd10:O34.529','icd10:O34.531','icd10:O34.532','icd10:O34.533','icd10:O34.539','icd10:O34.591',
'icd10:O34.592','icd10:O34.593','icd10:O34.599','icd10:O34.60','icd10:O34.61','icd10:O34.62','icd10:O34.63','icd10:O34.70',
'icd10:O34.71','icd10:O34.72','icd10:O34.73','icd10:O34.80','icd10:O34.81','icd10:O34.82','icd10:O34.83','icd10:O34.90',
'icd10:O34.91','icd10:O34.92','icd10:O34.93','icd10:O35.0XX0','icd10:O35.0XX1','icd10:O35.0XX2','icd10:O35.0XX3',
'icd10:O35.0XX4','icd10:O35.0XX5','icd10:O35.0XX9','icd10:O35.1XX0','icd10:O35.1XX1','icd10:O35.1XX2','icd10:O35.1XX3',
'icd10:O35.1XX4','icd10:O35.1XX5','icd10:O35.1XX9','icd10:O35.2XX0','icd10:O35.2XX1','icd10:O35.2XX2','icd10:O35.2XX3',
'icd10:O35.2XX4','icd10:O35.2XX5','icd10:O35.2XX9','icd10:O35.3XX0','icd10:O35.3XX1','icd10:O35.3XX2','icd10:O35.3XX3',
'icd10:O35.3XX4','icd10:O35.3XX5','icd10:O35.3XX9','icd10:O35.4XX0','icd10:O35.4XX1','icd10:O35.4XX2','icd10:O35.4XX3',
'icd10:O35.4XX4','icd10:O35.4XX5','icd10:O35.4XX9','icd10:O35.5XX0','icd10:O35.5XX1','icd10:O35.5XX2','icd10:O35.5XX3',
'icd10:O35.5XX4','icd10:O35.5XX5','icd10:O35.5XX9','icd10:O35.6XX0','icd10:O35.6XX1','icd10:O35.6XX2','icd10:O35.6XX3',
'icd10:O35.6XX4','icd10:O35.6XX5','icd10:O35.6XX9','icd10:O35.7XX0','icd10:O35.7XX1','icd10:O35.7XX2','icd10:O35.7XX3',
'icd10:O35.7XX4','icd10:O35.7XX5','icd10:O35.7XX9','icd10:O35.8XX0','icd10:O35.8XX1','icd10:O35.8XX2','icd10:O35.8XX3',
'icd10:O35.8XX4','icd10:O35.8XX5','icd10:O35.8XX9','icd10:O35.9XX0','icd10:O35.9XX1','icd10:O35.9XX2','icd10:O35.9XX3',
'icd10:O35.9XX4','icd10:O35.9XX5','icd10:O35.9XX9','icd10:O36.0110','icd10:O36.0111','icd10:O36.0112','icd10:O36.0113',
'icd10:O36.0114','icd10:O36.0115','icd10:O36.0119','icd10:O36.0120','icd10:O36.0121','icd10:O36.0122','icd10:O36.0123',
'icd10:O36.0124','icd10:O36.0125','icd10:O36.0129','icd10:O36.0130','icd10:O36.0131','icd10:O36.0132','icd10:O36.0133',
'icd10:O36.0134','icd10:O36.0135','icd10:O36.0139','icd10:O36.0190','icd10:O36.0191','icd10:O36.0192','icd10:O36.0193',
'icd10:O36.0194','icd10:O36.0195','icd10:O36.0199','icd10:O36.0910','icd10:O36.0911','icd10:O36.0912','icd10:O36.0913',
'icd10:O36.0914','icd10:O36.0915','icd10:O36.0919','icd10:O36.0920','icd10:O36.0921','icd10:O36.0922','icd10:O36.0923',
'icd10:O36.0924','icd10:O36.0925','icd10:O36.0929','icd10:O36.0930','icd10:O36.0931','icd10:O36.0932','icd10:O36.0933',
'icd10:O36.0934','icd10:O36.0935','icd10:O36.0939','icd10:O36.0990','icd10:O36.0991','icd10:O36.0992','icd10:O36.0993',
'icd10:O36.0994','icd10:O36.0995','icd10:O36.0999','icd10:O36.1110','icd10:O36.1111','icd10:O36.1112','icd10:O36.1113',
'icd10:O36.1114','icd10:O36.1115','icd10:O36.1119','icd10:O36.1120','icd10:O36.1121','icd10:O36.1122','icd10:O36.1123',
'icd10:O36.1124','icd10:O36.1125','icd10:O36.1129','icd10:O36.1130','icd10:O36.1131','icd10:O36.1132','icd10:O36.1133',
'icd10:O36.1134','icd10:O36.1135','icd10:O36.1139','icd10:O36.1190','icd10:O36.1191','icd10:O36.1192','icd10:O36.1193',
'icd10:O36.1194','icd10:O36.1195','icd10:O36.1199','icd10:O36.1910','icd10:O36.1911','icd10:O36.1912','icd10:O36.1913',
'icd10:O36.1914','icd10:O36.1915','icd10:O36.1919','icd10:O36.1920','icd10:O36.1921','icd10:O36.1922','icd10:O36.1923',
'icd10:O36.1924','icd10:O36.1925','icd10:O36.1929','icd10:O36.1930','icd10:O36.1931','icd10:O36.1932','icd10:O36.1933',
'icd10:O36.1934','icd10:O36.1935','icd10:O36.1939','icd10:O36.1990','icd10:O36.1991','icd10:O36.1992','icd10:O36.1993',
'icd10:O36.1994','icd10:O36.1995','icd10:O36.1999','icd10:O36.20X0','icd10:O36.20X1','icd10:O36.20X2','icd10:O36.20X3',
'icd10:O36.20X4','icd10:O36.20X5','icd10:O36.20X9','icd10:O36.21X0','icd10:O36.21X1','icd10:O36.21X2','icd10:O36.21X3',
'icd10:O36.21X4','icd10:O36.21X5','icd10:O36.21X9','icd10:O36.22X0','icd10:O36.22X1','icd10:O36.22X2','icd10:O36.22X3',
'icd10:O36.22X4','icd10:O36.22X5','icd10:O36.22X9','icd10:O36.23X0','icd10:O36.23X1','icd10:O36.23X2','icd10:O36.23X3',
'icd10:O36.23X4','icd10:O36.23X5','icd10:O36.23X9','icd10:O36.4XX0','icd10:O36.4XX1','icd10:O36.4XX2','icd10:O36.4XX3',
'icd10:O36.4XX4','icd10:O36.4XX5','icd10:O36.4XX9','icd10:O36.5110','icd10:O36.5111','icd10:O36.5112','icd10:O36.5113',
'icd10:O36.5114','icd10:O36.5115','icd10:O36.5119','icd10:O36.5120','icd10:O36.5121','icd10:O36.5122','icd10:O36.5123',
'icd10:O36.5124','icd10:O36.5125','icd10:O36.5129','icd10:O36.5130','icd10:O36.5131','icd10:O36.5132','icd10:O36.5133',
'icd10:O36.5134','icd10:O36.5135','icd10:O36.5139','icd10:O36.5190','icd10:O36.5191','icd10:O36.5192','icd10:O36.5193',
'icd10:O36.5194','icd10:O36.5195','icd10:O36.5199','icd10:O36.5910','icd10:O36.5911','icd10:O36.5912','icd10:O36.5913',
'icd10:O36.5914','icd10:O36.5915','icd10:O36.5919','icd10:O36.5920','icd10:O36.5921','icd10:O36.5922','icd10:O36.5923',
'icd10:O36.5924','icd10:O36.5925','icd10:O36.5929','icd10:O36.5930','icd10:O36.5931','icd10:O36.5932','icd10:O36.5933',
'icd10:O36.5934','icd10:O36.5935','icd10:O36.5939','icd10:O36.5990','icd10:O36.5991','icd10:O36.5992','icd10:O36.5993',
'icd10:O36.5994','icd10:O36.5995','icd10:O36.5999','icd10:O36.60X0','icd10:O36.60X1','icd10:O36.60X2','icd10:O36.60X3',
'icd10:O36.60X4','icd10:O36.60X5','icd10:O36.60X9','icd10:O36.61X0','icd10:O36.61X1','icd10:O36.61X2','icd10:O36.61X3',
'icd10:O36.61X4','icd10:O36.61X5','icd10:O36.61X9','icd10:O36.62X0','icd10:O36.62X1','icd10:O36.62X2','icd10:O36.62X3',
'icd10:O36.62X4','icd10:O36.62X5','icd10:O36.62X9','icd10:O36.63X0','icd10:O36.63X1','icd10:O36.63X2','icd10:O36.63X3',
'icd10:O36.63X4','icd10:O36.63X5','icd10:O36.63X9','icd10:O36.70X0','icd10:O36.70X1','icd10:O36.70X2','icd10:O36.70X3',
'icd10:O36.70X4','icd10:O36.70X5','icd10:O36.70X9','icd10:O36.71X0','icd10:O36.71X1','icd10:O36.71X2','icd10:O36.71X3',
'icd10:O36.71X4','icd10:O36.71X5','icd10:O36.71X9','icd10:O36.72X0','icd10:O36.72X1','icd10:O36.72X2','icd10:O36.72X3',
'icd10:O36.72X4','icd10:O36.72X5','icd10:O36.72X9','icd10:O36.73X0','icd10:O36.73X1','icd10:O36.73X2','icd10:O36.73X3',
'icd10:O36.73X4','icd10:O36.73X5','icd10:O36.73X9','icd10:O36.8120','icd10:O36.8121','icd10:O36.8122','icd10:O36.8123',
'icd10:O36.8124','icd10:O36.8125','icd10:O36.8129','icd10:O36.8130','icd10:O36.8131','icd10:O36.8132','icd10:O36.8133',
'icd10:O36.8134','icd10:O36.8135','icd10:O36.8139','icd10:O36.8190','icd10:O36.8191','icd10:O36.8192','icd10:O36.8193',
'icd10:O36.8194','icd10:O36.8195','icd10:O36.8199','icd10:O36.8210','icd10:O36.8211','icd10:O36.8212','icd10:O36.8213',
'icd10:O36.8214','icd10:O36.8215','icd10:O36.8219','icd10:O36.8220','icd10:O36.8221','icd10:O36.8222','icd10:O36.8223',
'icd10:O36.8224','icd10:O36.8225','icd10:O36.8229','icd10:O36.8230','icd10:O36.8231','icd10:O36.8232','icd10:O36.8233',
'icd10:O36.8234','icd10:O36.8235','icd10:O36.8239','icd10:O36.8290','icd10:O36.8291','icd10:O36.8292','icd10:O36.8293',
'icd10:O36.8294','icd10:O36.8295','icd10:O36.8299','icd10:O36.8310','icd10:O36.8311','icd10:O36.8312','icd10:O36.8313',
'icd10:O36.8314','icd10:O36.8315','icd10:O36.8319','icd10:O36.8320','icd10:O36.8321','icd10:O36.8322','icd10:O36.8323',
'icd10:O36.8324','icd10:O36.8325','icd10:O36.8329','icd10:O36.8330','icd10:O36.8331','icd10:O36.8332','icd10:O36.8333',
'icd10:O36.8334','icd10:O36.8335','icd10:O36.8339','icd10:O36.8390','icd10:O36.8391','icd10:O36.8392','icd10:O36.8393',
'icd10:O36.8394','icd10:O36.8395','icd10:O36.8399','icd10:O36.8910','icd10:O36.8911','icd10:O36.8912','icd10:O36.8913',
'icd10:O36.8914','icd10:O36.8915','icd10:O36.8919','icd10:O36.8920','icd10:O36.8921','icd10:O36.8922','icd10:O36.8923',
'icd10:O36.8924','icd10:O36.8925','icd10:O36.8929','icd10:O36.8930','icd10:O36.8931','icd10:O36.8932','icd10:O36.8933',
'icd10:O36.8934','icd10:O36.8935','icd10:O36.8939','icd10:O36.8990','icd10:O36.8991','icd10:O36.8992','icd10:O36.8993',
'icd10:O36.8994','icd10:O36.8995','icd10:O36.8999','icd10:O36.90X0','icd10:O36.90X1','icd10:O36.90X2','icd10:O36.90X3',
'icd10:O36.90X4','icd10:O36.90X5','icd10:O36.90X9','icd10:O36.91X0','icd10:O36.91X1','icd10:O36.91X2','icd10:O36.91X3',
'icd10:O36.91X4','icd10:O36.91X5','icd10:O36.91X9','icd10:O36.92X0','icd10:O36.92X1','icd10:O36.92X2','icd10:O36.92X3',
'icd10:O36.92X4','icd10:O36.92X5','icd10:O36.92X9','icd10:O36.93X0','icd10:O36.93X1','icd10:O36.93X2','icd10:O36.93X3',
'icd10:O36.93X4','icd10:O36.93X5','icd10:O36.93X9','icd10:O40.1XX0','icd10:O40.1XX1','icd10:O40.1XX2','icd10:O40.1XX3',
'icd10:O40.1XX4','icd10:O40.1XX5','icd10:O40.1XX9','icd10:O40.2XX0','icd10:O40.2XX1','icd10:O40.2XX2','icd10:O40.2XX3',
'icd10:O40.2XX4','icd10:O40.2XX5','icd10:O40.2XX9','icd10:O40.3XX0','icd10:O40.3XX1','icd10:O40.3XX2','icd10:O40.3XX3',
'icd10:O40.3XX4','icd10:O40.3XX5','icd10:O40.3XX9','icd10:O40.9XX0','icd10:O40.9XX1','icd10:O40.9XX2','icd10:O40.9XX3',
'icd10:O40.9XX4','icd10:O40.9XX5','icd10:O40.9XX9','icd10:O41.00X0','icd10:O41.00X1','icd10:O41.00X2','icd10:O41.00X3',
'icd10:O41.00X4','icd10:O41.00X5','icd10:O41.00X9','icd10:O41.01X0','icd10:O41.01X1','icd10:O41.01X2','icd10:O41.01X3',
'icd10:O41.01X4','icd10:O41.01X5','icd10:O41.01X9','icd10:O41.02X0','icd10:O41.02X1','icd10:O41.02X2','icd10:O41.02X3',
'icd10:O41.02X4','icd10:O41.02X5','icd10:O41.02X9','icd10:O41.03X0','icd10:O41.03X1','icd10:O41.03X2','icd10:O41.03X3',
'icd10:O41.03X4','icd10:O41.03X5','icd10:O41.03X9','icd10:O41.1010','icd10:O41.1011','icd10:O41.1012','icd10:O41.1013',
'icd10:O41.1014','icd10:O41.1015','icd10:O41.1019','icd10:O41.1020','icd10:O41.1021','icd10:O41.1022','icd10:O41.1023',
'icd10:O41.1024','icd10:O41.1025','icd10:O41.1029','icd10:O41.1030','icd10:O41.1031','icd10:O41.1032','icd10:O41.1033',
'icd10:O41.1034','icd10:O41.1035','icd10:O41.1039','icd10:O41.1090','icd10:O41.1091','icd10:O41.1092','icd10:O41.1093',
'icd10:O41.1094','icd10:O41.1095','icd10:O41.1099','icd10:O41.1210','icd10:O41.1211','icd10:O41.1212','icd10:O41.1213',
'icd10:O41.1214','icd10:O41.1215','icd10:O41.1219','icd10:O41.1220','icd10:O41.1221','icd10:O41.1222','icd10:O41.1223',
'icd10:O41.1224','icd10:O41.1225','icd10:O41.1229','icd10:O41.1230','icd10:O41.1231','icd10:O41.1232','icd10:O41.1233',
'icd10:O41.1234','icd10:O41.1235','icd10:O41.1239','icd10:O41.1290','icd10:O41.1291','icd10:O41.1292','icd10:O41.1293',
'icd10:O41.1294','icd10:O41.1295','icd10:O41.1299','icd10:O41.1410','icd10:O41.1411','icd10:O41.1412','icd10:O41.1413',
'icd10:O41.1414','icd10:O41.1415','icd10:O41.1419','icd10:O41.1420','icd10:O41.1421','icd10:O41.1422','icd10:O41.1423',
'icd10:O41.1424','icd10:O41.1425','icd10:O41.1429','icd10:O41.1430','icd10:O41.1431','icd10:O41.1432','icd10:O41.1433',
'icd10:O41.1434','icd10:O41.1435','icd10:O41.1439','icd10:O41.1490','icd10:O41.1491','icd10:O41.1492','icd10:O41.1493',
'icd10:O41.1494','icd10:O41.1495','icd10:O41.1499','icd10:O41.8X10','icd10:O41.8X11','icd10:O41.8X12','icd10:O41.8X13',
'icd10:O41.8X14','icd10:O41.8X15','icd10:O41.8X19','icd10:O41.8X20','icd10:O41.8X21','icd10:O41.8X22','icd10:O41.8X23',
'icd10:O41.8X24','icd10:O41.8X25','icd10:O41.8X29','icd10:O41.8X30','icd10:O41.8X31','icd10:O41.8X32','icd10:O41.8X33',
'icd10:O41.8X34','icd10:O41.8X35','icd10:O41.8X39','icd10:O41.8X90','icd10:O41.8X91','icd10:O41.8X92','icd10:O41.8X93',
'icd10:O41.8X94','icd10:O41.8X95','icd10:O41.8X99','icd10:O41.90X0','icd10:O41.90X1','icd10:O41.90X2','icd10:O41.90X3',
'icd10:O41.90X4','icd10:O41.90X5','icd10:O41.90X9','icd10:O41.91X0','icd10:O41.91X1','icd10:O41.91X2','icd10:O41.91X3',
'icd10:O41.91X4','icd10:O41.91X5','icd10:O41.91X9','icd10:O41.92X0','icd10:O41.92X1','icd10:O41.92X2','icd10:O41.92X3',
'icd10:O41.92X4','icd10:O41.92X5','icd10:O41.92X9','icd10:O41.93X0','icd10:O41.93X1','icd10:O41.93X2','icd10:O41.93X3',
'icd10:O41.93X4','icd10:O41.93X5','icd10:O41.93X9','icd10:O42.00','icd10:O42.011','icd10:O42.012','icd10:O42.013',
'icd10:O42.019','icd10:O42.02','icd10:O42.10','icd10:O42.111','icd10:O42.112','icd10:O42.113','icd10:O42.119','icd10:O42.12',
'icd10:O42.90','icd10:O42.911','icd10:O42.912','icd10:O42.913','icd10:O42.919','icd10:O42.92','icd10:O43.011','icd10:O43.012',
'icd10:O43.013','icd10:O43.019','icd10:O43.021','icd10:O43.022','icd10:O43.023','icd10:O43.029','icd10:O43.101',
'icd10:O43.102','icd10:O43.103','icd10:O43.109','icd10:O43.111','icd10:O43.112','icd10:O43.113','icd10:O43.119',
'icd10:O43.121','icd10:O43.122','icd10:O43.123','icd10:O43.129','icd10:O43.191','icd10:O43.192','icd10:O43.193',
'icd10:O43.199','icd10:O43.211','icd10:O43.212','icd10:O43.213','icd10:O43.219','icd10:O43.221','icd10:O43.222',
'icd10:O43.223','icd10:O43.229','icd10:O43.231','icd10:O43.232','icd10:O43.233','icd10:O43.239','icd10:O43.811',
'icd10:O43.812','icd10:O43.813','icd10:O43.819','icd10:O43.891','icd10:O43.892','icd10:O43.893','icd10:O43.899',
'icd10:O43.90','icd10:O43.91','icd10:O43.92','icd10:O43.93','icd10:O44.00','icd10:O44.01','icd10:O44.02','icd10:O44.03',
'icd10:O44.10','icd10:O44.11','icd10:O44.12','icd10:O44.13','icd10:O44.20','icd10:O44.21','icd10:O44.22','icd10:O44.23',
'icd10:O44.30','icd10:O44.31','icd10:O44.32','icd10:O44.33','icd10:O44.40','icd10:O44.41','icd10:O44.42','icd10:O44.43',
'icd10:O44.50','icd10:O44.51','icd10:O44.52','icd10:O44.53','icd10:O45.001','icd10:O45.002','icd10:O45.003','icd10:O45.009',
'icd10:O45.011','icd10:O45.012','icd10:O45.013','icd10:O45.019','icd10:O45.021','icd10:O45.022','icd10:O45.023',
'icd10:O45.029','icd10:O45.091','icd10:O45.092','icd10:O45.093','icd10:O45.099','icd10:O45.8X1','icd10:O45.8X2',
'icd10:O45.8X3','icd10:O45.8X9','icd10:O45.90','icd10:O45.91','icd10:O45.92','icd10:O45.93','icd10:O46.001','icd10:O46.002',
'icd10:O46.003','icd10:O46.009','icd10:O46.011','icd10:O46.012','icd10:O46.013','icd10:O46.019','icd10:O46.021',
'icd10:O46.022','icd10:O46.023','icd10:O46.029','icd10:O46.091','icd10:O46.092','icd10:O46.093','icd10:O46.099',
'icd10:O46.8X1','icd10:O46.8X2','icd10:O46.8X3','icd10:O46.8X9','icd10:O46.90','icd10:O46.91','icd10:O46.92','icd10:O46.93',
'icd10:O47.00','icd10:O47.02','icd10:O47.03','icd10:O47.1','icd10:O47.9','icd10:O48.0','icd10:O48.1','icd10:O60.00',
'icd10:O60.02','icd10:O60.03','icd10:O71.00','icd10:O71.02','icd10:O71.03','icd10:O88.011','icd10:O88.012','icd10:O88.013',
'icd10:O88.019','icd10:O88.111','icd10:O88.112','icd10:O88.113','icd10:O88.119','icd10:O88.211','icd10:O88.212',
'icd10:O88.213','icd10:O88.219','icd10:O88.311','icd10:O88.312','icd10:O88.313','icd10:O88.319','icd10:O88.811',
'icd10:O88.812','icd10:O88.813','icd10:O88.819','icd10:O90.3','icd10:O91.011','icd10:O91.012','icd10:O91.013',
'icd10:O91.019','icd10:O91.111','icd10:O91.112','icd10:O91.113','icd10:O91.119','icd10:O91.211','icd10:O91.212',
'icd10:O91.213','icd10:O91.219','icd10:O92.011','icd10:O92.012','icd10:O92.013','icd10:O92.019','icd10:O92.111',
'icd10:O92.112','icd10:O92.113','icd10:O92.119','icd10:O92.20','icd10:O92.29','icd10:O98.011','icd10:O98.012',
'icd10:O98.013','icd10:O98.019','icd10:O98.111','icd10:O98.112','icd10:O98.113','icd10:O98.119','icd10:O98.211',
'icd10:O98.212','icd10:O98.213','icd10:O98.219','icd10:O98.311','icd10:O98.312','icd10:O98.313','icd10:O98.319',
'icd10:O98.411','icd10:O98.412','icd10:O98.413','icd10:O98.419','icd10:O98.511','icd10:O98.512','icd10:O98.513',
'icd10:O98.519','icd10:O98.611','icd10:O98.612','icd10:O98.613','icd10:O98.619','icd10:O98.711','icd10:O98.712',
'icd10:O98.713','icd10:O98.719','icd10:O98.811','icd10:O98.812','icd10:O98.813','icd10:O98.819','icd10:O98.911',
'icd10:O98.912','icd10:O98.913','icd10:O98.919','icd10:O99.011','icd10:O99.012','icd10:O99.013','icd10:O99.019',
'icd10:O99.111','icd10:O99.112','icd10:O99.113','icd10:O99.119','icd10:O99.210','icd10:O99.211','icd10:O99.212',
'icd10:O99.213','icd10:O99.280','icd10:O99.281','icd10:O99.282','icd10:O99.283','icd10:O99.310','icd10:O99.311',
'icd10:O99.312','icd10:O99.313','icd10:O99.320','icd10:O99.321','icd10:O99.322','icd10:O99.323','icd10:O99.330',
'icd10:O99.331','icd10:O99.332','icd10:O99.333','icd10:O99.340','icd10:O99.341','icd10:O99.342','icd10:O99.343',
'icd10:O99.350','icd10:O99.351','icd10:O99.352','icd10:O99.353','icd10:O99.411','icd10:O99.412','icd10:O99.413',
'icd10:O99.419','icd10:O99.511','icd10:O99.512','icd10:O99.513','icd10:O99.519','icd10:O99.611','icd10:O99.612',
'icd10:O99.613','icd10:O99.619','icd10:O99.711','icd10:O99.712','icd10:O99.713','icd10:O99.719','icd10:O99.810',
'icd10:O99.820','icd10:O99.830','icd10:O99.840','icd10:O99.841','icd10:O99.842','icd10:O99.843','icd10:O9A.111',
'icd10:O9A.112','icd10:O9A.113','icd10:O9A.119','icd10:O9A.211','icd10:O9A.212','icd10:O9A.213','icd10:O9A.219',
'icd10:O9A.311','icd10:O9A.312','icd10:O9A.313','icd10:O9A.319','icd10:O9A.411','icd10:O9A.412','icd10:O9A.413',
'icd10:O9A.419','icd10:O9A.511','icd10:O9A.512','icd10:O9A.513','icd10:O9A.519','icd10:Z33.1','icd10:Z33.3','icd10:Z34.00',
'icd10:Z34.01','icd10:Z34.02','icd10:Z34.03','icd10:Z34.80','icd10:Z34.81','icd10:Z34.82','icd10:Z34.83','icd10:Z34.90',
'icd10:Z34.91','icd10:Z34.92','icd10:Z34.93','icd10:Z3A.01','icd10:Z3A.08','icd10:Z3A.09','icd10:Z3A.10','icd10:Z3A.11',
'icd10:Z3A.12','icd10:Z3A.13','icd10:Z3A.14','icd10:Z3A.15','icd10:Z3A.16','icd10:Z3A.17','icd10:Z3A.18','icd10:Z3A.19',
'icd10:Z3A.20','icd10:Z3A.21','icd10:Z3A.22','icd10:Z3A.23','icd10:Z3A.24','icd10:Z3A.25','icd10:Z3A.26',
'icd10:Z3A.27','icd10:Z3A.28','icd10:Z3A.29','icd10:Z3A.30','icd10:Z3A.31','icd10:Z3A.32','icd10:Z3A.33','icd10:Z3A.34','icd10:Z3A.35',
'icd10:Z3A.36','icd10:Z3A.37','icd10:Z3A.38','icd10:Z3A.39','icd10:Z3A.40','icd10:Z3A.41','icd10:Z3A.42','icd10:Z3A.49')
group by encounter_id;
--these are the pregnancy Dx events
SELECT 'dx:pregnancy'::varchar(128) AS name, 'urn:x-esphealth:heuristic:commoninf:enc:highbp:v1'::varchar(250) AS source, e.date, 
 current_timestamp AS timestamp, e.id as object_id, ct.id AS content_type_id, e.patient_id, e.provider_id 
into temporary temp_preg 
FROM tmp_pregdx pdx 
join emr_encounter e on e.id=pdx.encounter_id
JOIN django_content_type ct on ct.model = 'encounter';
insert into hef_event (name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) 
select name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id 
from temp_preg prg
where not exists (select null from hef_event he 
where he.name='dx:pregnancy' and he.patient_id=prg.patient_id and he.object_id=prg.object_id);
--now collect the just added events along with the ID from the hef_event table
--this gets used below
select patient_id, date, id as event_id, object_id 
into temporary tmp_preg 
from hef_event e 
  where name='dx:pregnancy';
--now build the pregnancy time spans.  The eCQM for controlled hypertension excludes patients who are pregnant during
-- the period under analysis, which is specified as a year.  We take each pregnancy dx, find if another dx occurred within 1 year, 
-- then combine overlapping dxs and subtracting 1 year from the earlies dx date in a series and adding 1 year to the latest
-- using islands and gaps approach
with t000 as ( select distinct on (patient_id, date) patient_id, date, event_id --distinct on takes the first from a set if there are many.  
   --we don't care, so long as we have only 1 qualifying event for a particular date.
   from tmp_preg
   order by patient_id, date, event_id)
   , t00 as (
  select patient_id, date, row_number() over (partition by patient_id order by date) i, event_id,
   lag(date) over (partition by patient_id order by date) as dt_i
   from t000)
  , t0 as (
  select patient_id, date, i, dt_i, event_id, 
    sum(case when coalesce(dt_i,date) + interval '1 year' < date then 0 else 1 end) over (partition by patient_id order by patient_id, i) grp  
    --the sum() over (window frame) is the core of the islands and gaps approach. 
    --When an order by is included in the window frame definition, the frame only includes prior rows within a partition
    --In this case, if the duration between the prior date (dt_i) and current date is less than 1 year, it's the same pregnancy group, 
    --so 0 is added to the sum for that row    
  from t00)
  , t1 as (
  select patient_id, (min(date)- interval '1 year')::date as start_date, (max(date) + interval '1 year')::date as end_date 
  from t0 
  group by patient_id, grp order by patient_id, grp)
select t1.patient_id, t1.start_date, t1.end_date, t01.event_id as start_id, t02.event_id as end_id
into temporary tmp_preg_seqs 
from t1 
join t0 as t01 on t1.patient_id=t01.patient_id and t1.start_date=t01.date - interval '1 year'
join t0 as t02 on t1.patient_id=t02.patient_id and t1.end_date=t02.date + interval '1 year';

--end stage renal events
select encounter_id into temporary temp_esr 
from emr_encounter_dx_codes dx 
where (dx_code_id like 'icd10:N18.6%' or dx_code_id like 'icd9:585.6%') 
group by encounter_id;
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id) 
select 'dx:endstagerenal'::varchar(128) as name, 'urn:x-esphealth:heuristic:commoninf:dx:endstagerenal:v1'::varchar(250) as source, 
  e.date, current_timestamp as timestamp, 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, 
  e.patient_id, e.provider_id 
from emr_encounter e 
join temp_esr dx on e.id=dx.encounter_id 
join django_content_type ct on app_label='emr' and model='encounter'
where not exists (select null from hef_event he where he.name='dx:endstagerenal' and he.patient_id=e.patient_id and he.object_id=e.id);
--only interested in the first esr diagnosis
select distinct on (patient_id) patient_id, id as event_id, object_id, date 
into temporary tmp_esr_diag
from hef_event
where name='dx:endstagerenal'
order by patient_id, date, id ;

--also need to know when inpatient encounters occur in order to exclude those.
--the following query depends on ESP infrastructure for site-specific data.
--The table gen_pop_tools.rs_conf_mapping specifies the set of site-specific raw_encounter_type values 
-- that correspond to inpatient encounters.  In this way "inpatient days" can be determined, and Rx orders  
-- from those days can be excluded.
Select patient_id, date
into temporary temp_inpatient_days
from emr_encounter e
join gen_pop_tools.rs_conf_mapping cm 
  on cm.src_value=e.raw_encounter_type and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
group by patient_id, date; --group by more efficient than SELECT DISTINCT for large tables

--Now collect specific Dx or Rx or BP records into "events"
--make hypertension dx events
select encounter_id into temporary temp_new_dx from emr_encounter_dx_codes dx 
where (dx_code_id like 'icd10:I10%' or dx_code_id like 'icd10:I11%'  or dx_code_id like 'icd10:I12%' 
         or dx_code_id like 'icd10:I13%' or dx_code_id like 'icd10:I15%')
--or dx_code_id like 'icd9:401.%' or dx_code_id like 'icd9:405.%') 
group by encounter_id;
--ESP is built using the Python-Django infrastructure, which includes a concept "generic foreign key"
--the django_content_type table contains a list of data model tables with a PK id for each
--so using that ID combined with the record ID from the relevant health record table provides source data lookup
--the hef_event table is where ESP stores algorithm-relevant "events"
--NB: the hef_event table has an "identity" PK that auto-generates a unique integer ID from a linked sequence.
--    so hef_event.id is autogenerated and does not appear in the insert below.
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id) 
select 'dx:hypertension'::varchar(128) as name, 'urn:x-esphealth:heuristic:commoninf:dx:hypertension:v1'::varchar(250) as source, e.date, 
    current_timestamp as timestamp, 'raw sql method'::text as note, e.id as object_id, ct.id as content_type_id, e.patient_id, e.provider_id 
from emr_encounter e join temp_new_dx dx on e.id=dx.encounter_id 
join django_content_type ct on app_label='emr' and model='encounter'
where not exists (select null from hef_event he where he.name='dx:hypertension' and he.patient_id=e.patient_id and he.object_id=e.id );
--now collect the just added events along with the ID from the hef_event table
--excluding for esr, preg and inpat (we use these same events in other analyses without the same filters.
--this will get used in several spots below
Select id as event_id, object_id, patient_id, date 
into temporary tmp_hypert_diag 
from hef_event he where he.name = 'dx:hypertension'
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=he.patient_id and he.date>esr.date) --not after esr
  and not exists (select null from tmp_preg_seqs prg
                  where prg.patient_id=he.patient_id and he.date between prg.start_date and prg.end_date) --not when pregnant
  and not exists (select null from emr_encounter e 
                  join gen_pop_tools.rs_conf_mapping cm 
                      on cm.src_value=e.raw_encounter_type and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
                  where e.id=he.object_id);  --not inpatient data

--here are antihypertensive Rx values
--the ESP static_drugsynonym tables contains generic and trade names for drugs with a given generic ingredient
with rx_names as
  (select lower(other_name) as other_name, lower(generic_name) as generic_name from static_drugsynonym
   where lower(generic_name) in 
  ('hydrochlorothiazide','chlorthalidone','indapamide','amlodipine','clevidipine','diltiazem','felodipine','isradipine',
  'nicardipine','nifedipine','nisoldipine','verapamil','acebutolol','atenolol','betaxolol','bisoprolol','carvedilol','labetolol',
  'metoprolol','nadolol','nebivolol','pindolol','propranolol','benazepril','catopril','enalapril','fosinopril','lisinopril',
  'moexipril','perindopril','quinapril','ramipril','trandolapril','candesartan','eprosartan','irbesartan','losartan','olmesartan',
  'telmisartan','valsartan','clonidine','doxazosin','guanfacine','methyldopa','prazosin','terazosin','eplerenone','sprinolactone',
  'aliskiren','hydralazine'))
select patient_id, date, id as object_id, generic_name as rx_name, provider_id
into temporary tmp_antihypert_rx
from emr_prescription rx join rx_names on 1=1
where name ilike '%'||other_name||'%'
group by patient_id, date, id, generic_name; --group by is more efficient that SELECT DISTINCT with large tables
insert into hef_event (name, source, date, timestamp, note, object_id, content_type_id, patient_id, provider_id) 
select 'rx:'||t0.rx_name::varchar(128) as name, 'urn:x-esphealth:heuristic:commoninf:rx'||rx_name||':v1'::varchar(250) as source, 
    t0.date, current_timestamp as timestamp, 'raw sql method'::text as note, t0.object_id, ct.id as content_type_id, t0.patient_id, 
    t0.provider_id 
from tmp_antihypert_rx t0
join django_content_type ct on app_label='emr' and model='encounter'
where not exists (select null from hef_event he where he.name='rx:'||t0.rx_name::varchar(128) and he.patient_id=t0.patient_id 
  and he.object_id=t0.object_id );
--now collect the just added events along with the ID from the hef_event table
--this will get used in several spots below
select patient_id, date, id as event_id
into temporary tmp_antihypert_presc 
from hef_event he
where name in ('rx:hydrochlorothiazide','rx:chlorthalidone','rx:indapamide','rx:amlodipine','rx:clevidipine','rx:diltiazem',
  'rx:felodipine','rx:isradipine','rx:nicardipine','rx:nifedipine','rx:nisoldipine','rx:verapamil','rx:acebutolol','rx:atenolol',
  'rx:betaxolol','rx:bisoprolol','rx:carvedilol','rx:labetolol','rx:metoprolol','rx:nadolol','rx:nebivolol','rx:pindolol',
  'rx:propranolol','rx:benazepril','rx:catopril','rx:enalapril','rx:fosinopril','rx:lisinopril','rx:moexipril','rx:perindopril',
  'rx:quinapril','rx:ramipril','rx:trandolapril','rx:candesartan','rx:eprosartan','rx:irbesartan','rx:losartan','rx:olmesartan',
  'rx:telmisartan','rx:valsartan','rx:clonidine','rx:doxazosin','rx:guanfacine','rx:methyldopa','rx:prazosin','rx:terazosin',
  'rx:eplerenone','rx:sprinolactone','rx:aliskiren','rx:hydralazine')
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=he.patient_id and he.date>esr.date) --not after esr
  and not exists (select null from tmp_preg_seqs prg 
                  where prg.patient_id=he.patient_id and he.date between prg.start_date and prg.end_date) --not when pregnant
  and not exists (select null from emr_encounter e 
                  join gen_pop_tools.rs_conf_mapping cm 
                      on cm.src_value=e.raw_encounter_type and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
                  where e.date=he.date);  --not inpatient data.  Rx data in ESP does not have an encounter ID, so we just use inpat enc dates.
create index tmp_antihypert_presc_patid_date_idx on tmp_antihypert_presc (patient_id, date);

--BP events.  
--here are all valid BPs 
select patient_id, date, max(id) as object_id, round(avg(bp_systolic))::int bp_systolic, round(avg(bp_diastolic))::int bp_diastolic
into temporary temp_allbp
from emr_encounter e
where bp_systolic between 30 and 300 and bp_diastolic between 20 and 150
group by patient_id, date;
--these are the high BP events
SELECT 'enc:highbp'::varchar(128) AS name, 'urn:x-esphealth:heuristic:commoninf:enc:highbp:v1'::varchar(250) AS source, abp.date, 
 current_timestamp AS timestamp, abp.object_id, ct.id AS content_type_id, abp.patient_id, 1 as provider_id 
into temporary temp_highbp 
FROM temp_allbp abp 
JOIN django_content_type ct on ct.model = 'encounter' 
WHERE (abp.bp_systolic >= 140 OR abp.bp_diastolic >= 90);
insert into hef_event (name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) 
select name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id 
from temp_highbp hbp
where not exists (select null from hef_event he where he.name='enc:highbp' and he.patient_id=hbp.patient_id and he.object_id=hbp.object_id);
--now collect the just added events along with the ID from the hef_event table
--this gets used below
select patient_id, date, id as event_id
into temporary tmp_highbp 
from hef_event  he
where name='enc:highbp'
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=he.patient_id and he.date>esr.date) --not after esr
  and not exists (select null from tmp_preg_seqs prg 
                  where prg.patient_id=he.patient_id and he.date between prg.start_date and prg.end_date) --not when pregnant
  and not exists (select null from emr_encounter e 
                  join gen_pop_tools.rs_conf_mapping cm 
                      on cm.src_value=e.raw_encounter_type and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
                  where e.id=he.object_id);  --not inpatient data;

--Now we build the cases.
--Here is two or more high BPs in a year
Select patient_id, date, event_id
into temporary tmp_2highbp
from (select hbp.patient_id, hbp.date, lag(hbp.date) over (partition by hbp.patient_id order by hbp.date) as first_date, hbp.event_id
         from tmp_highbp hbp) t0   
where date_part('day', age(date,first_date)) > 0 and date_part('year', age(date, first_date))=0   
group by patient_id, date, event_id
order by patient_id, date;
--the other criteria are 1 or more hypertension Dx or one or more antihypertensive Rx
select patient_id, date, criteria 
  into temporary tmp_ccases 
  from (select *, row_number() over (partition by patient_id order by date) as rownumber 
        from (select patient_id, min(date) as date, criteria 
              from (select patient_id, date, 'Criteria --2: Dx hypertension'::varchar(2000) as criteria from tmp_hypert_diag 
                    union select patient_id, date, 'Criteria --3: Rx for  anithypertensive'::varchar(2000) as criteria 
                    from tmp_antihypert_presc 
                    union select patient_id, date, 'Criteria --1: SBP>=140 or DBP>=90 on 2 or more occasions in a year'::varchar(2000) as criteria 
                    from tmp_2highbp) t0 
              group by patient_id, criteria) t00) t000 
  where rownumber=1;
insert into nodis_case (patient_id, condition, date, criteria, source, status, notes, reportables, created_timestamp, 
  updated_timestamp, sent_timestamp, followup_sent, provider_id, inactive_date, isactive) 
  select t0.patient_id, 'hypertension' as condition, t0.date, t0.criteria as criteria, 
 'urn:x-esphealth:disease:commoninf:hypertension:sql' as source, 'AR' as status,  null::text as notes, 
 null::text as reportables, current_timestamp as created_timestamp, current_timestamp as updated_timestamp, 
 null::timestamp as sent_timestamp, FALSE as followup_sent, 1 as provider_id, null::date as inactive_date, 
 TRUE as isactive 
from tmp_ccases t0 
where not exists (select null from nodis_case c where c.condition='hypertension' and c.patient_id=t0.patient_id);
select patient_id, id as case_id, date into temporary tmp_ccase_ids from nodis_case where condition='hypertension';

--now that we have cases with start dates, we identify controlled BP events.  
SELECT 'enc:not_highbp_c'::varchar(128) AS name, 
  'urn:x-esphealth:heuristic:commoninf:enc:not_highbp_controlled:v1'::varchar(250) AS source, e.date, 
  current_timestamp AS timestamp, e.object_id, ct.id AS content_type_id, e.patient_id, 1 as provider_id 
into temporary temp_highbp_c 
FROM temp_allbp e 
JOIN django_content_type ct on ct.model = 'encounter' 
JOIN emr_patient p on e.patient_id = p.id 
join tmp_ccase_ids tde on tde.patient_id=e.patient_id and tde.date<=e.date 
WHERE e.bp_systolic < 140 AND e.bp_diastolic < 90 and (e.bp_systolic>=130 or e.bp_diastolic>=80)
group by e.date, 
  current_timestamp, e.object_id, ct.id, e.patient_id;
SELECT 'enc:not_highbp_w'::varchar(128) AS name, 
  'urn:x-esphealth:heuristic:commoninf:enc:not_highbp_well_controlled:v1'::varchar(250) AS source, e.date, 
  current_timestamp AS timestamp, e.object_id, ct.id AS content_type_id, e.patient_id, 1 as provider_id
into temporary temp_highbp_w 
FROM temp_allbp e 
JOIN django_content_type ct on ct.model = 'encounter' 
JOIN emr_patient p on e.patient_id = p.id 
WHERE e.bp_systolic < 130 and e.bp_diastolic< 80
group by e.date, 
  current_timestamp, e.object_id, ct.id, e.patient_id;
insert into hef_event (name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) 
select name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id 
from (select * 
      from temp_highbp_c
      union
      select *
      from temp_highbp_w) hbp
where not exists (select null from hef_event h where h.object_id=hbp.object_id and h.name=hbp.name);
select patient_id, id as event_id, date
into temporary tmp_not_highbp
from hef_event he
where name in ('enc:not_highbp_c','enc:not_highbp_w')
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=he.patient_id and he.date>esr.date) --not after esr
  and not exists (select null from tmp_preg_seqs prg 
                  where prg.patient_id=he.patient_id and he.date between prg.start_date and prg.end_date) --not when pregnant
  and not exists (select null from emr_encounter e 
                  join gen_pop_tools.rs_conf_mapping cm 
                      on cm.src_value=e.raw_encounter_type and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
                  where e.id=he.object_id);

--Now we collect histories
--here are all the events to use in building the caseactivehistory (some of these are not hef events, such as birthdays)
select patient_id, date as case_date, hdate, event_id, content_type_id, etype 
 into temporary tmp_events 
 from (Select distinct c.patient_id, c.date, d.date as hdate, d.event_id, ct.id as content_type_id, 'hdx' etype 
       from tmp_ccase_ids c 
       join tmp_hypert_diag d on c.patient_id=d.patient_id and d.date between c.date and current_date 
       join django_content_type ct on app_label='hef' and model='event' 
       union Select distinct c.patient_id, c.date, hbp.date as hdate, hbp.event_id, ct.id as content_type_id, 'hbp' etype 
       from tmp_ccase_ids c 
       join tmp_highbp hbp on c.patient_id=hbp.patient_id and hbp.date between c.date and current_date 
       join django_content_type ct on app_label='hef' and model='event' 
       union Select distinct c.patient_id, c.date, nhbp.date as hdate, nhbp.event_id, ct.id as content_type_id, 'cbp' etype 
       from tmp_ccase_ids c 
       join tmp_not_highbp nhbp on c.patient_id=nhbp.patient_id and nhbp.date between c.date and current_date 
       join django_content_type ct on app_label='hef' and model='event' 
       union Select distinct c.patient_id, c.date, esr.date as hdate, esr.event_id, ct.id as content_type_id, 'esr' etype 
       from tmp_ccase_ids c 
       join tmp_esr_diag esr on c.patient_id=esr.patient_id and esr.date > c.date and esr.date <= current_date 
       join django_content_type ct on app_label='hef' and model='event' 
       union Select distinct c.patient_id, c.date, prg.date as hdate, prg.event_id, ct.id as content_type_id, 'prg' etype 
       from tmp_ccase_ids c 
       join tmp_preg prg on c.patient_id=prg.patient_id and prg.date between c.date and current_date 
       join django_content_type ct on app_label='hef' and model='event' 
       union select distinct c.patient_id, c.date, rx.date as hdate, rx.event_id, ct.id as content_type_id, 'rxs' etype 
       from tmp_ccase_ids c 
       join tmp_antihypert_presc rx on c.patient_id=rx.patient_id and rx.date between c.date and current_date 
       join django_content_type ct on app_label='hef' and model='event' ) t0; 
create index tmp_event_etypes on tmp_events (etype);
analyse tmp_events;

insert into nodis_case_events (case_id, event_id) 
  select t0.case_id, t1.event_id 
  from tmp_ccase_ids t0 
  join tmp_events t1 on t1.patient_id=t0.patient_id where  
  not exists (select null from nodis_case_events t00 where t00.case_id=t0.case_id and t00.event_id=t1.event_id)
  group by t0.case_id, t1.event_id;
  
--Now we implement the reversion rules.  
-- 1. If a patient's case is due to 2 high-bps in a year, and no subsequent Dx or Rx events appear, and there are other encounters , but no high BP 
-- and at least 1 BP in the controlled range, then revert to not hypertensive at the first evidence-free encounter after 1 year 
--     (case history status is set to "D" for discontinued.)
-- 2. If a patient's case does involve Dx or Rx events, and there is no evidence of hypertension for 2 years (no Dx, no Rx, no HighBp), then 
--    revert to not hypertensive at the first evidence-free encounter after 2 years.
-- 3. Once reverted, a case can be reactivated when hypetension criteria is met again.
--
--To meet the "and there are other encounters" rule for discontinuation, we look at other clinical encounters, but exclude inpatient days.
--The MENDS data only has inpatient days using the ENCOUNTER table.  The CLIN_ENC table has dates for any clinical encounter (Rx order, Lab test, etc)
--We use this table, but exclude inpatient days
--

select te.patient_id, hdate, te.event_id, t2hbp.event_id as hbp_2_id,
  case when etype in ('rxs','hdx') or t2hbp.event_id is not null then hdate else null::date end as qdate, --this provides dates where patient met algorithm
  case when t2hbp2.event_id is not null then hdate else null::date end as oneyr_dt, --this provides dates for 2highbp events without prior dx or rx 
  --these patients are important for reversion rule following
  Coalesce(lead(hdate) over (partition by te.patient_id order by hdate), current_date) as next_date --if no next record, then use current date.
into temporary rvrt_events
from tmp_events te
left join tmp_2highbp t2hbp on t2hbp.patient_id=te.patient_id and t2hbp.date=te.hdate and t2hbp.event_id=te.event_id
  and te.etype='hbp'
left join tmp_2highbp t2hbp2 on t2hbp2.patient_id=te.patient_id and t2hbp2.date=te.hdate and t2hbp2.event_id=te.event_id
  and te.etype='hbp' and not exists (select null from tmp_events t0 where t0.patient_id=t2hbp2.patient_id and t0.hdate<=t2hbp2.date)
where te.etype in ('rxs','hdx','hbp');

select patient_id, date 
  into temporary tmp_clin_enc 
  from gen_pop_tools.clin_enc ce 
  where exists (select null from tmp_ccase_ids ci 
                where ci.patient_id=ce.patient_id and ci.date<ce.date) 
  and not exists (select null from temp_inpatient_days ie 
                  where ie.patient_id=ce.patient_id and ie.date=ce.date)
  and not exists (select null from tmp_preg_seqs prg 
                  where prg.patient_id=ce.patient_id and ce.date between prg.start_date and prg.end_date) --cant revert when pregnant -- already excluded
  group by patient_id, date;
CREATE INDEX tmp_clin_enc_pat_date_idx ON tmp_clin_enc USING btree (patient_id, date);
Analyze tmp_clin_enc;

--identify the reverters
--reversion rule is: if you have clinical encounters, but no evidence of ongoing hypertension, then you revert at next encounter after two years,
-- EXCEPT for individuals whose only evidence for hypertension is two or more high BP measures in a year. 
-- Those patients revert after 1 year.
select ce.patient_id, rmindate, next_date, min(ce.date) as reversion_date 
  into temporary tmp_reverters 
  from (select t0.patient_id, 
               Case 
                 when oneyr_dt is null then t0.hdate + interval '2 years'  
                 else t0.hdate + interval '1 year' 
               end as rmindate, t0.next_date
        from rvrt_events t0
        where date_part('year', age(t0.next_date,t0.hdate)) > case when oneyr_dt is null then 1 else 0 end 
             --next qualifying event is over 1 or 2 years from current event, depending on BP only rule.
        group by t0.patient_id, t0.hdate, t0.next_date, oneyr_dt ) t00 
  join tmp_clin_enc ce on t00.patient_id=ce.patient_id and ce.date >= t00.rmindate and ce.date < t00.next_date
  --next clinical encounter after "revertable" date, but before next event date, is the reversion date
  where rmindate < next_date 
  group by ce.patient_id, rmindate, next_date;
--the reversion is based on the first clinical encounter after the reversion opening starts (1 or 2 years, depending...)
--but now we need to get the event_id for that clinical encounter, or if there are several on the same date, the first in the list
--I'm using the min and max aggregators on ID here, since it doesn't really matter what event ID we get here.  Min within a 
--table should be the first, max across the set of set of tables should pull the ID from the encounter group that gets the
--most data generally.
select patient_id, reversion_date, max(event_id) as event_id, etype
into temporary tmp_rvrt_events
from (
  select rvrt.patient_id, reversion_date, min(enc.id) as event_id, 'rvt' as etype
  from tmp_reverters rvrt
  join emr_encounter enc on enc.patient_id=rvrt.patient_id and rvrt.reversion_date=enc.date
  group by rvrt.patient_id, reversion_date
  union
  select rvrt.patient_id, reversion_date, min(rx.id) as event_id, 'rvt' as etype
  from tmp_reverters rvrt
  join emr_prescription rx on rx.patient_id=rvrt.patient_id and rvrt.reversion_date=rx.date
  group by rvrt.patient_id, reversion_date
  union
  select rvrt.patient_id, reversion_date, min(lb.id) as event_id, 'rvt' as etype
  from tmp_reverters rvrt
  join emr_labresult lb on lb.patient_id=rvrt.patient_id and rvrt.reversion_date=lb.date
  group by rvrt.patient_id, reversion_date
  union
  select rvrt.patient_id, reversion_date, min(vx.id) as event_id, 'rvt' as etype
  from tmp_reverters rvrt
  join emr_immunization vx on vx.patient_id=rvrt.patient_id and rvrt.reversion_date=vx.date
  group by rvrt.patient_id, reversion_date) t0
group by patient_id, reversion_date, etype;

--Now we build case histories
--Its useful to think of a hypertension case at two levels:
--  at the first level, a case is either "hypertensive", "not currently hypertensive" or "no longer hypertensive" 
--  So we build that layer first. 
select patient_id, hdate, event_id, etype
into temporary tmp_hevents
from tmp_events 
where etype in ('rxs','hdx') --prescriptions or diagnosis events start or restart a case
union 
select patient_id, date as hdate, event_id, '2bp' etype --2 high BPs within a year start or restart a case
from tmp_2highbp 
union 
select patient_id, reversion_date as hdate, event_id, etype
from tmp_rvrt_events --these are the reversion events.  These end a case, but it can be restarted
union
select patient_id, date as hdate, event_id, 'esr' as etype 
from tmp_esr_diag --end stage renal events end a case, and it cannot be restarted
union 
select patient_id, start_date as hdate, start_id as event_id, 'prgst' as etype
from tmp_preg_seqs --pregnancy start ends a case.  This needs a bit of discussion, as the algorithm says to exclude cases 
-- within 1 year of pregnancy Dx.  In our data we have a preg Dx start and end date, so we will restart a case below 
-- at the end of the pregnancy black-out period unless there is also a reversion record.
union
select patient_id, end_date as hdate, end_id as event_id, 'prgend' as etype
from tmp_preg_seqs;

--you can have multiple events on a given day.  Here is the hierachy of precedence
with t0 as (
  select *,
         case etype 
           when 'esr' then 1
           when 'prgst' then 2
           when 'prgend' then 3
           when 'rvt' then 4
           when 'hdx' then 5
           when 'rxs' then 6
           when '2bp' then 7
         end as precendence
   from tmp_hevents)
select distinct on (patient_id, hdate) patient_id, hdate, event_id, etype
into tmp_distinct_hevents
from t0
order by patient_id, hdate, precendence;

--now find hypertension islands and gaps underlay
With t0 as (
  select *,
         case 
           when etype in ('esr','prgst','rvt') then 'D' --discontinue
           when etype in ('prgend','hdx','rxs','2bp') then 'H' -- hypertensive
         end as topo
  from tmp_distinct_hevents)
  , t00 as (
  select *, lag(topo) over (partition by patient_id order by patient_id, hdate) as prior_topo
  from t0)
  , t000 as (
  select *,
       sum(case when coalesce(prior_topo,topo)<>topo then 1 else 0 end) 
           over (partition by patient_id order by patient_id, hdate) as topo_grp
       --In this case, if the prior topography does not equal the current topography, it's its a different topo group, 
       --so 1 is added to the sum for that row   
  from t00)
  , t0000 as (
  select patient_id, max(topo) as topo, min(hdate) as date, max(hdate) as latest_event_date
  from t000
  group by patient_id, topo_grp)
select t0000.*, tdh.event_id 
into temporary tmp_hypert_underlay
from t0000 
join tmp_distinct_hevents tdh on tdh.patient_id=t0000.patient_id and tdh.hdate=t0000.date;

--now get control overlay.  Hypertension islands can have topography of "controlled" "uncontrolled" or "unknown"
--here are the BP events
select patient_id, hdate, event_id, content_type_id, etype::varchar(3) etype 
  into temporary tmp_bp_evnts 
  from tmp_events where etype in ('hbp','cbp');
--now join the hypertension underlaying topography to the bp overlay, and get the next date within a patient hypert topo
with t0 as (
  select *, 
         lead(date) over (partition by patient_id order by patient_id, date) as next_date
  from tmp_hypert_underlay)
select t0.*, t1.hdate, t1.etype  
into temporary tmp_hypert_bp_overlay
from t0
left join tmp_bp_evnts t1 on t1.patient_id=t0.patient_id and t0.topo='H' 
   and t1.hdate between t0.date and t0.next_date - interval '1 day';
--now the hypertension islands have an overlay set of rows for controlled or uncontrolled BP
--but we need one more set of rows to indicate any periods of hypertension where we don't know control 
--these are periods at the start of hypertension cases where the patient is hypertensive due to a Dx or Rx, 
--but there is no accompanying bp measure. To find start of hypertension island groups, we partition by patient and topo, 
--and find where prior date is null
with t0 as (
  select *,
    lag(hdate) over (partition by patient_id, date order by patient_id, date, hdate) as prior_hdate
  from tmp_hypert_bp_overlay
  order by patient_id, date, hdate)
  , t00 as (
  select patient_id, topo, date, latest_event_date, event_id, next_date, date as hdate, 'unk' as etype
  from t0
  where topo='H' and prior_hdate is null and date<hdate)
select patient_id, topo, date, latest_event_date, event_id, next_date, 
  coalesce(hdate,date) hdate, 
  case 
    when etype is null and topo='D' then 'dsc'
    when etype is null and topo='H' then 'unk'
	else etype
  end as etype
into tmp_hypert_bp_unk_overlay
from
(select * from tmp_hypert_bp_overlay
 union select * from t00) t000;
 
 delete from nodis_caseactivehistory cah 
  where exists (select null from nodis_case c where c.id=cah.case_id and c.condition='hypertension');

--all done.  Insert into caseactivehistory, then update the case table with any currently discontinued hypertension cases.
insert into nodis_caseactivehistory (status, change_reason, date, latest_event_date, object_id, case_id, content_type_id) 
  select case 
    when t0.etype='unk' then 'UK' 
    when t0.etype='hbp' then 'U' 
    when t0.etype='cbp' then 'C' 
    when t0.etype='dsc' then 'D' 
 end status, '' change_reason, t0.hdate, 
t0.latest_event_date, t0.event_id, t1.case_id, t2.id as content_type_id 
  from tmp_hypert_bp_unk_overlay t0 
  join tmp_ccase_ids t1 on t1.patient_id=t0.patient_id
  join django_content_type t2 on t2.model='event';

update nodis_case set isactive=FALSE, inactive_date=t000.date 
  from (select t0.case_id, t0.date from nodis_caseactivehistory t0 
  join (select t00.case_id, max(t00.date) as maxdate 
  from nodis_caseactivehistory t00 
  join tmp_ccase_ids t01 on t00.case_id=t01.case_id 
  group by t00.case_id) t1 on t1.case_id=t0.case_id and t0.status='D' and t0.date=t1.maxdate 
  join nodis_case t2 on t0.case_id=t2.id and t2.condition='hypertension') t000 
  where nodis_case.id=t000.case_id;
