--end stage renal events
select distinct on (patient_id) patient_id, date, id as event_id 
into temporary tmp_esr_diag 
from hef_event e0 
where name='dx:endstagerenal' 
order by patient_id, date, id;
--pregnancy 
with tmp_preg as (
  select patient_id, date, id as event_id from hef_event e 
  where name='dx:pregnancy'),
t00 as (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
              lag(date) over (partition by patient_id order by date) dt_i 
        from tmp_preg 
        group by patient_id, date),  
t0 as (select patient_id, date, i, dt_i, 
         sum(case when dt_i + interval '1 year' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
       from  t00)
select patient_id, min(date) as start_date, max(date) + interval '1 year' as end_date 
into temporary tmp_preg_seqs 
from  t0 
group by patient_id, grp order by patient_id, start_date;
--inpatient encounters
select e.id, patient_id, date 
into temporary temp_inpat_encs 
from emr_encounter e 
join gen_pop_tools.rs_conf_mapping cm on cm.src_value=e.raw_encounter_type 
  and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
group by e.id, patient_id, date;
--Total Rx by year
select count(t0.id) as rx_counts, count(distinct t1.id) as pat_counts, to_char(date,'yyyy') as year
from emr_prescription t0
right join emr_patient t1 on t1.id=t0.patient_id
group by to_char(date, 'yyyy');
--filtered Rx by year (no ESR, no preg, no inpat days)
select count(t0.id) as rx_counts, count(distinct t1.id) as pat_counts, to_char(date,'yyyy') as year
from emr_prescription t0
right join emr_patient t1 on t1.id=t0.patient_id
where not exists (select null from temp_inpat_encs ie where ie.patient_id=t0.patient_id and ie.date=t0.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=t0.patient_id and esr.date<=t0.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=t0.patient_id and t0.date between pseq.start_date and pseq.end_date)
group by to_char(date, 'yyyy')
;

select count(t0.id) as hypert_rx_counts, count(distinct t1.id) as pat_counts, to_char(t0.date,'yyyy') as year
from emr_patient t1
left join hef_event t0 on t0.patient_id=t1.id and t0.name in ('rx:hydrochlorothiazide','rx:chlorthalidone', 
  'rx:indapamide','rx:amlodipine','rx:clevidipine','rx:diltiazem', 'rx:felodipine','rx:isradipine',
  'rx:nicardipine','rx:nifedipine','rx:nisoldipine','rx:verapamil','rx:acebutolol','rx:atenolol',
  'rx:betaxolol','rx:bisoprolol','rx:carvedilol','rx:labetolol','rx:metoprolol','rx:nadolol','rx:nebivolol',
  'rx:pindolol', 'rx:propranolol','rx:benazepril','rx:catopril','rx:enalapril','rx:fosinopril','rx:lisinopril',
  'rx:moexipril','rx:perindopril','rx:quinapril','rx:ramipril','rx:trandolapril','rx:candesartan','rx:eprosartan',
  'rx:irbesartan','rx:losartan','rx:olmesartan', 'rx:telmisartan','rx:valsartan','rx:clonidine','rx:doxazosin',
  'rx:guanfacine','rx:methyldopa','rx:prazosin','rx:terazosin', 'rx:eplerenone','rx:sprinolactone','rx:aliskiren',
  'rx:hydralazine')
where not exists (select null from temp_inpat_encs ie where ie.patient_id=t0.patient_id and ie.date=t0.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=t0.patient_id and esr.date<=t0.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=t0.patient_id and t0.date between pseq.start_date and pseq.end_date)
group by to_char(date, 'yyyy');

with t00 as (
select patient_id from gen_pop_tools.hypert_2yrwindow_by_crit t0
where criteria='Criteria #3: Rx for  anithypertensive' and year_month='2020_06'
  and not exists (select null from gen_pop_tools.hypert_2yrwindow_by_crit t1 
                  where year_month >= '2021_03' and criteria='Criteria #3: Rx for  anithypertensive'
                  and t1.patient_id=t0.patient_id)
  and exists (select null from gen_pop_tools.clin_enc t2 
              where t0.patient_id=t2.patient_id and t2.date>='2021-03-31'::date))
select natural_key 
into temporary rx_pats_of_interest
from emr_patient p
join t00 on t00.patient_id=p.id;

\copy rx_pats_of_interest to '\tmp\rx_pats_of_interest.csv' csv delimiter ',' header;

with t00 as (
select distinct patient_id from gen_pop_tools.hypert_2yrwindow_by_crit t0
where criteria='Criteria #3: Rx for  anithypertensive' and year_month='2020_06'
  and not exists (select null from gen_pop_tools.hypert_2yrwindow_by_crit t1 
                  where year_month >= '2021_03' and criteria='Criteria #3: Rx for  anithypertensive'
                  and t1.patient_id=t0.patient_id)
  and exists (select null from gen_pop_tools.clin_enc t2 
              where t0.patient_id=t2.patient_id and t2.date>='2021-03-31'::date))
select p.natural_key, to_char(ce.date,'yyyy') as year
into temporary rx_poi_with_ce_dates
from emr_patient p
join t00 on t00.patient_id=p.id
join gen_pop_tools.clin_enc ce on t00.patient_id=ce.patient_id
group by p.natural_key, to_char(ce.date,'yyyy');

\copy rx_pats_of_interest to '\tmp\rx_pats_of_interest.csv' csv delimiter ',' header;

--copy this to your home directory
--on the regenstrief OMOP db:
create temporary table pers_of_interest (person_id bigint);
\copy pers_of_interest from '~/rx_pats_of_interest.csv' csv delimiter ',' header;

with t0 as (
select distinct d.person_id, 
  d.drug_exposure_start_date as start_date,
  case 
    when d.drug_exposure_end_date is not null and refills >= 0
      then d.drug_exposure_start_date + (d.drug_exposure_end_date-d.drug_exposure_start_date)*(refills+1)
    else d.drug_exposure_end_date 
  end as end_date,
  lead(d.drug_exposure_start_date) 
    over (partition by d.person_id, d.drug_type_concept_id 
          order by d.drug_exposure_start_date) as next_date,
  d.drug_type_concept_id as drug_id
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
join pers_of_interest poi on poi.person_id=d.person_id
where drug_source_value like '9999^%')
, t00 as (
Select *,
  sum(case
        when end_date <= coalesce(next_date,end_date) then 0 else 1 end) 
  over (partition by person_id, drug_id order by start_date) as grp
  from t0)
, t000 as (
select person_id, min(start_date) as start_date, max(end_date) as end_date, drug_id, grp
from t00
group by person_id, drug_id, grp)
, t001 as (
select year from string_to_table('2018,2019,2020,2021,2022,2023',',') year )
select count(*) as counts, t001.year
from t000
join t001 on t001.year between to_char(t000.start_date,'yyyy') and to_char(t000.end_date,'yyyy')
group by t001.year
order by t001.year;

with t0 as (
select distinct d.person_id, 
  d.drug_exposure_start_date as start_date,
  d.drug_exposure_end_date as end_date,
  d.drug_type_concept_id as drug_id
from drug_exposure d 
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
join pers_of_interest poi on poi.person_id=d.person_id)
select count(*) as counts, to_char(start_date,'yyyy') as year
from t0 where start_date>end_date
group by to_char(start_date,'yyyy');

select distinct person_id, drug_exposure_start_date,  from drug_exposure d join pers_of_interest poi on poi.person_id=d.person_id
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
where to_char(drug_exposure_start_date,'yyyy')='2023';


select distinct d.person_id, 
  concat(d.person_id,'-',to_char(d.drug_exposure_start_date, 'yyyyMMdd'),'-',
         md5(concat(d.sig,source.concept_code,to_char(d.quantity,'9999999999'),to_char(d.refills,'999999999'),
                    to_char(d.drug_exposure_end_date, 'yyyyMMdd'),route.concept_name))) as natural_key,
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') as ddate,
  d.sig, source.concept_code,
  source.concept_name, d.quantity, d.refills,
  to_char(d.drug_exposure_start_date, 'yyyyMMdd') AS start_date, to_char(d.drug_exposure_end_date, 'yyyyMMdd') AS end_date,
  route.concept_name as route
into temporary mends_drug_subset
from drug_exposure d
join concept source on d.drug_concept_id=source.concept_id and source.vocabulary_id='RxNorm'
left join concept route on d.route_concept_id = route.concept_id
WHERE d.drug_source_value like '9999^%'
  and d.drug_exposure_start_date < '2024-09-01'::date AND d.drug_exposure_start_date >= '2017-01-01'::date;


