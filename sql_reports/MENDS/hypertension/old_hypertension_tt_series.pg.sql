--end stage renal events
select distinct on (patient_id) patient_id, date, id as event_id 
into temporary tmp_esr_diag 
from hef_event e0 
where name='dx:endstagerenal' 
order by patient_id, date, id;
--pregnancy 
with tmp_preg as (
  select patient_id, date, id as event_id from hef_event e 
  where name='dx:pregnancy'),
t00 as (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
              lag(date) over (partition by patient_id order by date) dt_i 
        from tmp_preg 
        group by patient_id, date),  
t0 as (select patient_id, date, i, dt_i, 
         sum(case when dt_i + interval '1 year' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
       from  t00)
select patient_id, min(date) as start_date, max(date) + interval '1 year' as end_date 
into temporary tmp_preg_seqs 
from  t0 
group by patient_id, grp order by patient_id, start_date;
--inpatient encounters
select e.id, patient_id, date 
into temporary temp_inpat_encs 
from emr_encounter e 
join gen_pop_tools.rs_conf_mapping cm on cm.src_value=e.raw_encounter_type 
  and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
group by e.id, patient_id, date;

select patient_id, to_char(date,'yyyy_mm') as year_month
into temporary bp_encs_ym
from emr_encounter e 
where not exists (select null from temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date)
  and (bp_diastolic>0 and bp_systolic>0)
group by patient_id, to_char(date,'yyyy_mm');
create unique index bp_encs_by_month_idx on bp_encs_ym (patient_id, year_month);
analyze bp_encs_ym;

with t0 as (
 select count(*) as counts,
   case (hypertension)
     when '1' then 'Hypertensive'
     when '2' then 'Not hypertensive'
     when '3' then 'Hypertensive'
     when '4' then 'Hypertensive'
     else 'Not hypertensive'
   end as hypertgrp,
   year_month
 from gen_pop_tools.tt_pat_seq_enc seq
 left gen_pop_tools.tt_hypertension hyp on hyp.patient_id=seq.patient_id and hyp.year_month=seq.year_month
 where exists (select null from bp_encs_ym be
              where to_date(be.year_month,'yyyy_mm')
                  between to_date(seq.year_month,'yyyy_mm') - interval '23 months' and to_date(seq.year_month,'yyyy_mm')
              and be.patient_id=seq.patient_id)
      and not exists (select null from tmp_esr_diag esr where esr.patient_id=seq.patient_id and esr.date<=to_date(seq.year_month,'yyyy_mm'))
      and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=seq.patient_id and to_date(seq.year_month,'yyyy_mm') between pseq.start_date and pseq.end_date)
      and seq.age>=20
 group by hypertgrp, year_month)
select (t00.counts)::float / (t00.counts + t01.counts)::float as prevelance, t00.year_month
from t0 as t00 join t0 as t01 on t00.year_month=t01.year_month 
   and t00.hypertgrp='Hypertensive' and t01.hypertgrp='Not hypertensive';
