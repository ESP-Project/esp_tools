--end stage renal events
select distinct on (patient_id) patient_id, date, id as event_id 
into temporary tmp_esr_diag 
from hef_event e0 
where name='dx:endstagerenal' 
order by patient_id, date, id;
--pregnancy 
with tmp_preg as (
  select patient_id, date, id as event_id from hef_event e 
  where name='dx:pregnancy'),
t00 as (select patient_id, date, row_number() over (partition by patient_id order by date) i, 
              lag(date) over (partition by patient_id order by date) dt_i 
        from tmp_preg 
        group by patient_id, date),  
t0 as (select patient_id, date, i, dt_i, 
         sum(case when dt_i + interval '1 year' >= date then 0 else 1 end) over (partition by patient_id order by i) grp 
       from  t00)
select patient_id, min(date) as start_date, max(date) + interval '1 year' as end_date 
into temporary tmp_preg_seqs 
from  t0 
group by patient_id, grp order by patient_id, start_date;
--inpatient encounters
select e.id, patient_id, date 
into temporary temp_inpat_encs 
from emr_encounter e 
join gen_pop_tools.rs_conf_mapping cm on cm.src_value=e.raw_encounter_type 
  and cm.src_field='raw_encounter_type' and cm.mapped_value='inpatient'
group by e.id, patient_id, date;

select patient_id, to_char(date,'yyyy_mm') as year_month
into temporary bp_encs_ym
from emr_encounter e 
where not exists (select null from temp_inpat_encs ie where ie.patient_id=e.patient_id and ie.date=e.date)
  and not exists (select null from tmp_esr_diag esr where esr.patient_id=e.patient_id and esr.date<=e.date)
  and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=e.patient_id and e.date between pseq.start_date and pseq.end_date)
  and (bp_diastolic>0 and bp_systolic>0)
group by patient_id, to_char(date,'yyyy_mm');
create unique index bp_encs_by_month_idx on bp_encs_ym (patient_id, year_month);
analyze bp_encs_ym;

drop table if exists mendshypert_by_month;
with t0 as (
  select distinct on (patient_id, to_char(cah.date,'yyyy_mm'))
     c.patient_id, to_char(cah.date,'yyyy_mm') as year_month, cah.date as hdate, cah.status
  from nodis_case c
  join nodis_caseactivehistory cah on cah.case_id=c.id and c.condition='mendshypertension'
  --where cah.status <> 'D'
  order by patient_id, to_char(cah.date,'yyyy_mm'), cah.date desc),
t1 as (
  select patient_id, status, year_month start_hdate,
     to_char(Coalesce(
       (lead(hdate) over (partition by patient_id order by patient_id, hdate) ) - interval '1 month' ,
       current_date),'yyyy_mm') as end_hdate
  from t0)
select seq.patient_id, coalesce(case when t1.status = 'D' then 'NH' else t1.status end, 'NH') as status, seq.year_month
into temporary mendshypert_bymonth
from gen_pop_tools.tt_pat_seq_enc seq
left join t1 on t1.patient_id = seq.patient_id
  and seq.year_month between t1.start_hdate and t1.end_hdate
where exists (select null from bp_encs_ym be
              where to_date(be.year_month,'yyyy_mm')
                  between to_date(seq.year_month,'yyyy_mm') - interval '23 months' and to_date(seq.year_month,'yyyy_mm')
              and be.patient_id=seq.patient_id)
      and not exists (select null from tmp_esr_diag esr where esr.patient_id=seq.patient_id and esr.date<=to_date(seq.year_month,'yyyy_mm'))
      and not exists (select null from tmp_preg_seqs pseq where pseq.patient_id=seq.patient_id and to_date(seq.year_month,'yyyy_mm') between pseq.start_date and pseq.end_date)
      and seq.age>=20;


create index mendshypert_bymo_patid_yrmo_idx on mendshypert_bymonth (patient_id, year_month, status);
analyse mendshypert_bymonth;
 
select count(*) as counts, year_month, status
into temporary hypert_period_counts
from mendshypert_bymonth
group by year_month, status
order by year_month, status;



select (t0.counts + t1.counts + t2.counts)::float / (t1.counts + t1.counts + t2.counts + t3.counts)::float as prevalence,
       t0.year_month
from (select * from hypert_period_counts where status='C') t0
join (select * from hypert_period_counts where status='U') t1 on t0.year_month=t1.year_month
join (select * from hypert_period_counts where status='UK') t2 on t0.year_month=t2.year_month
join (select * from hypert_period_counts where status='NH') t3 on t0.year_month=t3.year_month;

select count(*), year_month, criteria
into temporary hypert_2yrwindow_counts_source
from gen_pop_tools.hypert_2yrwindow_by_crit
group by year_month, criteria
order by year_month, criteria;

select t0.year_month, t0.count as rx_counts, t1.count as dx_counts, t2.count as bp2_counts, 
 t0.count + t1.count + t2.count + t3.count as cohort_counts
from (select * from hypert_2yrwindow_counts_source where criteria='Criteria #3: Rx for  anithypertensive') t0
join (select * from hypert_2yrwindow_counts_source where criteria='Criteria #2: Dx hypertension') t1
  on t0.year_month=t1.year_month
join (select * from hypert_2yrwindow_counts_source where criteria='Criteria #1: SBP>=140 or DBP>=90 on 2 or more occasions in a year') t2
  on t0.year_month=t2.year_month
join (select * from hypert_2yrwindow_counts_source where criteria is null) t3
  on t0.year_month=t3.year_month
order by year_month;


select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct e.patient_id) as dx_pat_count,
       count(dx_id) as dx_count,
       to_char(coalesce(ce.date,e.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select e.patient_id, dx.id as dx_id, e.date 
           from emr_encounter e 
           join emr_encounter_dx_codes dx on dx.encounter_id=e.id) e
on e.patient_id=ce.patient_id and e.date=ce.date and ce.source='enc'
group by to_char(coalesce(ce.date,e.date),'yyyy');

select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct e.patient_id) as bp_pat_count,
       count(e.id) as bp_count,
       to_char(coalesce(ce.date,e.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select e.patient_id, e.id, e.date 
           from emr_encounter e 
           where bp_systolic is not null and bp_diastolic is not null) e
on e.patient_id=ce.patient_id and e.date=ce.date and ce.source='enc'
group by to_char(coalesce(ce.date,e.date),'yyyy');

select count(distinct ce.patient_id) as all_pat_count, 
       count(distinct p.patient_id) as rx_pat_count,
       count(p.id) as rx_count,
       to_char(coalesce(ce.date,p.date),'yyyy') as year
from gen_pop_tools.clin_enc ce
full outer join (select p.patient_id, p.id, p.date 
           from emr_prescription p) p
on p.patient_id=ce.patient_id and p.date=ce.date and ce.source='rx'
group by to_char(coalesce(ce.date,p.date),'yyyy');