with bpea as (select patient_id from gen_pop_tools.tt_bp_encs_amb bpea where year_month >= '2022_02' group by patient_id)
, p as (select p.id, coalesce(t2.mapped_value, 'null sex') as gender,
               coalesce(t3.mapped_value, 'null race') as race,
               coalesce(t4.mapped_value, 'null erthnicity') as ethnicity,
               case
                 when date_part('year', age('2024-02-01'::timestamp, p.date_of_birth)) between 20 and 40 then '20-40'
                 when date_part('year', age('2024-02-01'::timestamp, p.date_of_birth)) between 41 and 60 then '41-60'
                 when date_part('year', age('2024-02-01'::timestamp, p.date_of_birth)) between 61 and 85 then '61-85'
               end as age_group
        from emr_patient p
        left join gen_pop_tools.rs_conf_mapping t2 on t2.src_field='gender' and t2.src_value=p.gender
        left join gen_pop_tools.rs_conf_mapping t3 on t3.src_field='race' and t3.src_value=p.race
        left join gen_pop_tools.rs_conf_mapping t4 on t4.src_field='ethnicity_new' and t4.src_value=p.ethnicity
        where date_part('year', age('2024-02-01'::timestamp, date_of_birth)) >=20
        and date_part('year', age('2024-02-01'::timestamp, date_of_birth)) <= 85),
t0 as (select distinct on (patient_id) patient_id, cah.date, cah.status
           from nodis_case c
           join nodis_caseactivehistory cah on cah.case_id=c.id and c.condition='hypertension'
           order by patient_id, cah.date desc)
select count(*), case when t0.status is null then 'NH' else t0.status end as hypertension, 
  p.gender, p.race, p.ethnicity, p.age_group
from bpea
join p on p.id=bpea.patient_id
left join t0 on t0.patient_id=bpea.patient_id 
group by grouping sets ((case when t0.status is null then 'NH' else t0.status end), 
  (case when t0.status is null then 'NH' else t0.status end, age_group), 
   (case when t0.status is null then 'NH' else t0.status end, race), 
   (case when t0.status is null then 'NH' else t0.status end, gender),
   (case when t0.status is null then 'NH' else t0.status end, ethnicity));