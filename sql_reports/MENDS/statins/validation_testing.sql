-- Step 0:  Ensure temp tables do not exist before creating them
IF OBJECT_ID('tempdb..#TempPatients') IS NOT NULL DROP TABLE #TempPatients;
IF OBJECT_ID('tempdb..#TempStatinPrescriptions') IS NOT NULL DROP TABLE #TempStatinPrescriptions;
IF OBJECT_ID('tempdb..#TempStatinLabs') IS NOT NULL DROP TABLE #TempStatinLabs;
IF OBJECT_ID('tempdb..#TempStatinDiagnosis') IS NOT NULL DROP TABLE #TempStatinDiagnosis;
IF OBJECT_ID('tempdb..#TempStatinPatients') IS NOT NULL DROP TABLE #TempStatinPatients;

-- Step 1:  Copy statin patients from nodis_case into a temp table
CREATE TABLE #TempStatinPatients
(
    ID INT PRIMARY KEY,
	PATIENT_ID INT,
    CONDITION VARCHAR(MAX),
    CRITERIA NVARCHAR(MAX),
	ISACTIVE BIT
); 

INSERT INTO #TempStatinPatients (id, patient_id, condition, criteria)
SELECT id, patient_id, condition, criteria
FROM nodis_case
WHERE condition like '%statin%';

-- Step 2: Copy all statin patient labwork into temp table
CREATE TABLE #TempStatinLabs
(
    ID INT PRIMARY KEY,
	NATIVE_CODE VARCHAR(20),
    NATIVE_NAME VARCHAR(MAX),
    RESULT_STRING VARCHAR(50),
	PATIENT_ID INT
); 

INSERT INTO #TempStatinLabs (id, native_code, native_name, result_string, patient_id)
SELECT LR.id, native_code, native_name, result_string, LR.patient_id
FROM emr_labresult LR
JOIN #TempStatinPatients SP ON SP.PATIENT_ID = LR.patient_id


-- Step 3: Copy all statin patient Prescription info into temp table
CREATE TABLE #TempStatinPrescriptions
(
    PrescriptionID INT PRIMARY KEY,
    PatientID INT,
    Name NVARCHAR(MAX)
);

INSERT INTO #TempStatinPrescriptions (PrescriptionID, PatientID, Name)
SELECT p.id, p.patient_id, p.name
FROM emr_prescription p
JOIN #TempStatinPatients SP ON SP.PATIENT_ID = p.patient_id 
WHERE p.name NOT IN ('atorvastatin', 'lipitor', 'fluvastatin', 'lescol', 'lovastatin', 
					 'mevinolin', 'mevacor', 'altoprev', 'pitavastatin', 'livalo', 'zypitamag',
					 'nikita', 'pravastatin', 'pravachol', 'simvastatin', 'zocor')
			 



-- Step 4: Copy all statin patient Diagnosis info into temp table
CREATE TABLE #TempStatinDiagnosis
(
	DiagnosisID INT PRIMARY KEY,
	dx_code_id nvarchar(20),
	Encounter_id INT,
);

ALTER TABLE #TempStatinDiagnosis
  ALTER COLUMN dx_code_id
    VARCHAR(20) COLLATE Latin1_General_CS_AS NOT NULL

INSERT INTO #TempStatinDiagnosis (DiagnosisID, dx_code_id, Encounter_id)
SELECT d.id, d.dx_code_id, d.encounter_id
FROM emr_encounter_dx_codes d
JOIN emr_encounter e ON e.id = d.encounter_id
JOIN #TempStatinPatients SP ON SP.PATIENT_ID = e.patient_id
WHERE d.dx_code_id 

SELECT distinct sdc.name FROM #TempStatinDiagnosis tsd
JOIN static_dx_code sdc
ON tsd.dx_code_id = sdc.combotypecode
ORDER BY name

SELECT * FROM #TempStatinPatients
SELECT * FROM #TempStatinDiagnosis
SELECT * FROM #TempStatinLabs
SELECT * FROM #TempStatinPrescriptions


SELECT top 100 * FROM static_dx_code where name like '%statin%'
SELECT top 100 * FROM #TempStatinDiagnosis
