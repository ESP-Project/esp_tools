/* 

I.  Utility Queries:


a. List non-nullable columns for a table in the public schema
----------------------------------------------------------
SELECT column_name
FROM information_schema.columns
WHERE table_schema = 'public'
AND table_name = 'nodis_case'
AND is_nullable = 'NO';


b. List all possible values for INSERT INTOs
------------------------------------------
select distinct id from emr_provider;
select distinct provenance_id from emr_patient;
select distinct name from hef_event;


c. SELECTS for tables referenced
-----------------------------
SELECT * FROM emr_prescription;
SELECT * FROM emr_encounter;
SELECT * FROM emr_encounter_dx_codes;
SELECT * FROM static_dx_code WHERE code like 'I%';
SELECT * from nodis_case;
SELECT * FROM nodis_caseactivehistory;
SELECT * FROM hef_event;
SELECT * FROM emr_patient;


d. Clear Data Not Related To This Test
*** DO NOT RUN THIS ON A PROD SERVER ***
----------------------------------------
	--DELETE FROM emr_specimen WHERE laborder_id < 9300;
	--DELETE FROM emr_laborder WHERE patient_id < 9300;
	--DELETE FROM emr_encounter_dx_codes WHERE encounter_id < 9300;
	--DELETE FROM emr_encounter WHERE patient_id < 9500;
	--DELETE FROM emr_prescription WHERE patient_id < 9503;
	--DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id < 9500);
	--DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id < 9500);
	--DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id < 9500);
	--DELETE FROM nodis_case WHERE patient_id < 9500;
	--DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id < 9500);
	--DELETE FROM hef_timespan WHERE patient_id < 9500;
	--DELETE FROM hef_event WHERE patient_id < 9500;
	--DELETE FROM emr_patient WHERE id < 9500;
	--DELETE FROM emr_labresult WHERE patient_id < 9500;
	--DELETE FROM emr_laborder WHERE patient_id < 9500;
	

II. Testing Schedule:


Test #1:
--------
1	9501	"Countraindicated"
1	9502	"ASCVD no rx"
1	9503	"ESR or aged out"
1	9504	"Hyperchol moderate"
1	9505	"Hyperchol no rx"
1	9506	"Hyperchol high"
1   9507	"ASCVD high"
1 	9508	"ASCVD moderate"
1 	9509	"Diabetes no Rx"
1	9510	"Diabetes moderate"
1	9511	"Diabetes high"

Test #2:
--------
1	9507	"ASCVD"
1	9508	"ASCVD"
1	9509	"Diabetes"
1	9510	"Diabetes"
1	9504	"Hyperchol"
1	9506	"Hyperchol"


Test #3:
--------
1	"ASCVD no rx"
1	"Diabetes no rx"
1	"Hyperchol no rx"



Test #4:
--------
1	"H"
1	"N"
1	"O"


Test #5:
--------


Test #6:
--------


Test #7:
--------


Test #8:
--------


Test #9:
--------


Test #10:
--------


Test #11:
--------


Test #12:
--------


Test #13:
--------


Test #14:
--------


Test #15:
--------

III.  Test Patient Data
-- Create Test Patients 1 - 8
	
	
	** Create Test Patient #1 **
	-- Age: 53
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  True
	-- Patient_id:  9501
	-- Expected Test Results
		--Test 1:  
			Expected:  Contraindicated due to Pregnancy
			Actual:  Contraindicated due to Pregnancy
		--Test 2:  
			Expected:  Not Present
			Actual:  Not Present
		--Test 3:

	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9501, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9301, 9501, 'atorvastatin', '10mg', '2015-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9301, NOW(), NOW(), '2020-01-01', 3, true, 1, 9501);
	
	INSERT INTO nodis_case (id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, patient_id, provider_id, isactive) VALUES
	(9301, 'diabetes:type-2', '2020-01-01', 'urn:x-esphealth:disease:channing:diabetes:v1', 'AR', NOW(), NOW(), false, 9501, 1, true);
	
	INSERT INTO hef_timespan(id, name, source, start_date, end_date, timestamp, pattern, patient_id) VALUES
	(1, 'pregnancy', 'urn:x-esphealth:heuristic:commoninf:timespan:statin:v1', '2020-01-01', NULL, now(), 'onset:dx_code eop:currently_pregnant', 9501);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9301, 'dx:diabetes:type-1-not-stated-icd10', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2012-05-05', NOW(), 9501, 66, 9501, 1);

    ** Teardown Test Patient #1 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9301;
	DELETE FROM emr_encounter WHERE patient_id = 9501;
	DELETE FROM emr_prescription WHERE patient_id = 9501;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9501);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9501);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9501);
	DELETE FROM nodis_case WHERE patient_id = 9501;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9501);
	DELETE FROM hef_timespan WHERE patient_id = 9501;
	DELETE FROM hef_event WHERE patient_id = 9501;
	DELETE FROM emr_patient WHERE id = 9501;

	** Create Test Patient #2 **
	-- Age: 53
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  True
	-- Patient ID:  9502
	-- EMR Encounter DX Codes: ICD10:I25.73 (ASCVD)
	-- Expected Test Results:
		Test #1:
			Expected:  ASCVD (high)
			Actual:  ASCVD (No RX)   <--- Discussed with Bob on 9/29
		Test #2:
			Expected: 
			Actual:  
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9502, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9302, 9502, 'atorvastatin', '10mg', '2018-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9302, NOW(), NOW(), '2015-01-01', 3, true, 1, 9502);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(2, 'dx:ascvd', 'urn:x-esphealth:heuristic:channing:encounter:edd:v1', '2012-05-05', NOW(), 9502, 66, 9502, 1);
	
	INSERT INTO emr_encounter_dx_codes (id, encounter_id, dx_code_id) VALUES
	(11, 9302, 'icd10:I25.73');

    ** Teardown Patient #2 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9302;
	DELETE FROM emr_encounter WHERE patient_id = 9502;
	DELETE FROM emr_prescription WHERE patient_id = 9502;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9502);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9502);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9502);
	DELETE FROM nodis_case WHERE patient_id = 9502;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9502);
	DELETE FROM hef_timespan WHERE patient_id = 9502;
	DELETE FROM hef_event WHERE patient_id = 9502;
	DELETE FROM emr_patient WHERE id = 9502;
	
	
	** Create Test Patient #3 **
	-- Age: 90
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Fluvastatin
	-- Pregnant:  False
	-- Patient ID:  9503
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  ESR or Aged Out
			Actual:  ESR or Aged Out
		Test #2:
			Expected:  Not Present
			Actual:  Not Present
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9503, NOW(), NOW(), 1, '1933-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9303, 9503, 'fluvastatin', '10mg', '2018-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9303, NOW(), NOW(), '2020-01-01', 3, true, 1, 9503);
	
	INSERT INTO nodis_case (id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, patient_id, provider_id, isactive) VALUES
	(9303, 'diabetes:type-2', '2001-01-01', 'urn:x-esphealth:disease:channing:diabetes:v1', 'AR', NOW(), NOW(), false, 9503, 1, true);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9303, 'dx:diabetes:all-types', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2020-01-01', NOW(), 9503, 66, 9503, 1);
	
	INSERT INTO hef_timespan(id, name, source, start_date, end_date, timestamp, pattern, patient_id) VALUES
	(9303, 'statin', 'urn:x-esphealth:heuristic:channing:timespan:pregnancy:v1', '2001-01-01', NULL, now(), 'onset:dx_code eop:currently_pregnant', 9503);

    ** Teardown Patient #3 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9303;
	DELETE FROM emr_encounter WHERE patient_id = 9503;
	DELETE FROM emr_prescription WHERE patient_id = 9503;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9503);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9503);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9503);
	DELETE FROM nodis_case WHERE patient_id = 9503;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9503);
	DELETE FROM hef_timespan WHERE patient_id = 9503;
	DELETE FROM hef_event WHERE patient_id = 9503;
	DELETE FROM emr_patient WHERE id = 9503;
	
	
	** Create Test Patient #4 **
	-- Age: 60
	-- Previous Hef Events:  LDL>=190
	-- Statin Prescription:  Fluvastatin
	-- Pregnant:  False
	-- Patient ID:  9504
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Hyperchol (Moderate)
			Actual:  Hyperchol (Moderate)
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9504, NOW(), NOW(), 1, '1962-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9304, 9504, 'fluvastatin', '10mg', '2018-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9304, NOW(), NOW(), '2020-01-01', 3, false, 1, 9504);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9304, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:commoninf:lx:cholesterol-ldl:threshold:gte:190:v1', '2020-01-01', NOW(), 9504, 66, 9504, 1);

    ** Teardown Patient #4 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9304;
	DELETE FROM emr_encounter WHERE patient_id = 9504;
	DELETE FROM emr_prescription WHERE patient_id = 9504;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9504);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9504);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9504);
	DELETE FROM nodis_case WHERE patient_id = 9504;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9504);
	DELETE FROM hef_timespan WHERE patient_id = 9504;
	DELETE FROM hef_event WHERE patient_id = 9504;
	DELETE FROM emr_patient WHERE id = 9504;
	
	
	** Create Test Patient #5 **
	-- Age: 60
	-- Previous Hef Events:  LDL>=190
	-- Statin Prescription:  Fluvastatin
	-- Pregnant:  False
	-- Patient ID:  9505
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Hyperchol (No Rx)
			Actual:  Hyperchol (No Rx)
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9505, NOW(), NOW(), 1, '1962-01-01');
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9305, NOW(), NOW(), '2020-01-01', 3, false, 1, 9505);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9305, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:commoninf:lx:cholesterol-ldl:threshold:gte:190:v1', '2020-01-01', NOW(), 9505, 66, 9505, 1);

    ** Teardown Patient #5 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9305;
	DELETE FROM emr_encounter WHERE patient_id = 9505;
	DELETE FROM emr_prescription WHERE patient_id = 9505;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9505);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9505);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9505);
	DELETE FROM nodis_case WHERE patient_id = 9505;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9505);
	DELETE FROM hef_timespan WHERE patient_id = 9505;
	DELETE FROM hef_event WHERE patient_id = 9505;
	DELETE FROM emr_patient WHERE id = 9505;
	
	
	** Create Test Patient #6 **
	-- Age: 60
	-- Previous Hef Events:  LDL>=190
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9506
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Hyperchol High
			Actual:  Hyperchol High
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9506, NOW(), NOW(), 1, '1962-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9306, 9506, 'atorvastatin', '10mg', '2018-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9306, NOW(), NOW(), '2020-01-01', 3, false, 1, 9506);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9306, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:commoninf:lx:cholesterol-ldl:threshold:gte:190:v1', '2020-01-01', NOW(), 9506, 66, 9506, 1);

    ** Teardown Patient #6 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9306;
	DELETE FROM emr_encounter WHERE patient_id = 9506;
	DELETE FROM emr_prescription WHERE patient_id = 9506;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9506);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9506);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9506);
	DELETE FROM nodis_case WHERE patient_id = 9506;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9506);
	DELETE FROM hef_timespan WHERE patient_id = 9506;
	DELETE FROM hef_event WHERE patient_id = 9506;
	DELETE FROM emr_patient WHERE id = 9506;
	
	
	** Create Test Patient #7 **
	-- Age: 60
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9507
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  ASCVD High
			Actual:  ASCVD High
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9507, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9307, 9507, 'atorvastatin', '10mg', '2012-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9307, NOW(), NOW(), '2012-01-01', 3, false, 1, 9507);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(7, 'dx:ascvd', 'urn:x-esphealth:heuristic:channing:encounter:edd:v1', '2012-05-05', NOW(), 9507, 66, 9507, 1);
	
	INSERT INTO emr_encounter_dx_codes (id, encounter_id, dx_code_id) VALUES
	(17, 9307, 'icd10:I25.73');

    ** Teardown Patient #7 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9307;
	DELETE FROM emr_encounter WHERE patient_id = 9507;
	DELETE FROM emr_prescription WHERE patient_id = 9507;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9507);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9507);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9507);
	DELETE FROM nodis_case WHERE patient_id = 9507;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9507);
	DELETE FROM hef_timespan WHERE patient_id = 9507;
	DELETE FROM hef_event WHERE patient_id = 9507;
	DELETE FROM emr_patient WHERE id = 9507;
	
	
	** Create Test Patient #8 **
	-- Age: 60
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9507
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  ASCVD High
			Actual:  ASCVD High
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9508, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9308, 9508, 'fluvastatin', '10mg', '2012-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9308, NOW(), NOW(), '2015-01-01', 3, false, 1, 9508);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(8, 'dx:ascvd', 'urn:x-esphealth:heuristic:channing:encounter:edd:v1', '2012-05-05', NOW(), 9508, 66, 9508, 1);
	
	INSERT INTO emr_encounter_dx_codes (id, encounter_id, dx_code_id) VALUES
	(18, 9308, 'icd10:I25.73');

    ** Teardown Patient #8 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9308;
	DELETE FROM emr_encounter WHERE patient_id = 9508;
	DELETE FROM emr_prescription WHERE patient_id = 9508;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9508);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9508);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9508);
	DELETE FROM nodis_case WHERE patient_id = 9508;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9508);
	DELETE FROM hef_timespan WHERE patient_id = 9508;
	DELETE FROM hef_event WHERE patient_id = 9508;
	DELETE FROM emr_patient WHERE id = 9508;
	
	
	** Create Test Patient #9 **
	-- Age: 60
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9509
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Diabetes high
			Actual:  Diabetes high
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9509, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9309, 9509, 'atorvastatin', '10mg', '2012-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9309, NOW(), NOW(), '2015-01-01', 3, false, 1, 9509);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(9, 'dx:diabetes:type-1-not-stated-icd10', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2012-05-05', NOW(), 9509, 66, 9509, 1);
	
	INSERT INTO nodis_case (id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, patient_id, provider_id, isactive) VALUES
	(9309, 'diabetes:type-2', '2020-01-01', 'urn:x-esphealth:disease:channing:diabetes:v1', 'AR', NOW(), NOW(), false, 9509, 1, true);

    ** Teardown Patient #9 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9309;
	DELETE FROM emr_encounter WHERE patient_id = 9509;
	DELETE FROM emr_prescription WHERE patient_id = 9509;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9509);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9509);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9509);
	DELETE FROM nodis_case WHERE patient_id = 9509;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9509);
	DELETE FROM hef_timespan WHERE patient_id = 9509;
	DELETE FROM hef_event WHERE patient_id = 9509;
	DELETE FROM emr_patient WHERE id = 9509;
	
	
	** Create Test Patient #10 **
	-- Age: 60
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9510
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Diabetes moderate
			Actual:  Diabetes moderate
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9510, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_prescription (id, patient_id, name, dose, date, updated_timestamp, created_timestamp, order_natural_key, provenance_id, provider_id) VALUES
	(9310, 9510, 'atorvastatin', '10mg', '2012-01-01', NOW(), NOW(), '1', 1, 1);
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9310, NOW(), NOW(), '2015-01-01', 3, false, 1, 9510);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(10, 'dx:diabetes:type-1-not-stated-icd10', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2012-05-05', NOW(), 9510, 66, 9510, 1);
	
	INSERT INTO nodis_case (id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, patient_id, provider_id, isactive) VALUES
	(9310, 'diabetes:type-2', '2020-01-01', 'urn:x-esphealth:disease:channing:diabetes:v1', 'AR', NOW(), NOW(), false, 9510, 1, true);

    ** Teardown Patient #10 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9310;
	DELETE FROM emr_encounter WHERE patient_id = 9510;
	DELETE FROM emr_prescription WHERE patient_id = 9510;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9510);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9510);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9510);
	DELETE FROM nodis_case WHERE patient_id = 9510;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9510);
	DELETE FROM hef_timespan WHERE patient_id = 9510;
	DELETE FROM hef_event WHERE patient_id = 9510;
	DELETE FROM emr_patient WHERE id = 9510;
	
	
	** Create Test Patient #11 **
	-- Age: 60
	-- Previous Hef Events:  Diabetes
	-- Statin Prescription:  Atorvastatin
	-- Pregnant:  False
	-- Patient ID:  9511
	-- EMR Encounter DX Codes:
	-- Expected Test Results:
		Test #1:
			Expected:  Diabetes no rx
			Actual:  Pending
	
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id, date_of_birth) VALUES
	(9511, NOW(), NOW(), 1, '1970-01-01');
	
	INSERT INTO emr_encounter (id, created_timestamp, updated_timestamp, date, priority, pregnant, provenance_id, patient_id) VALUES
	(9311, NOW(), NOW(), '2015-01-01', 3, false, 1, 9511);
	
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(11, 'dx:diabetes:type-1-not-stated-icd10', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2012-05-05', NOW(), 9511, 66, 9511, 1);
	
	INSERT INTO nodis_case (id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, patient_id, provider_id, isactive) VALUES
	(9311, 'diabetes:type-2', '2020-01-01', 'urn:x-esphealth:disease:channing:diabetes:v1', 'AR', NOW(), NOW(), false, 9511, 1, true);

    ** Teardown Patient #11 **
	DELETE FROM emr_encounter_dx_codes WHERE encounter_id = 9311;
	DELETE FROM emr_encounter WHERE patient_id = 9511;
	DELETE FROM emr_prescription WHERE patient_id = 9511;
	DELETE FROM nodis_case_events WHERE case_id IN (SELECT Id FROM nodis_case WHERE patient_id = 9511);
	DELETE FROM nodis_case_timespans WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9511);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (SELECT id FROM nodis_case WHERE patient_id = 9511);
	DELETE FROM nodis_case WHERE patient_id = 9511;
	DELETE FROM hef_timespan_events WHERE timespan_id IN (SELECT id FROM hef_timespan WHERE patient_id = 9511);
	DELETE FROM hef_timespan WHERE patient_id = 9511;
	DELETE FROM hef_event WHERE patient_id = 9511;
	DELETE FROM emr_patient WHERE id = 9511;
*/


-- Code from Bob on 9/28 call
--1
select 365 as dx_prevalence, 365 as statin_prevalence, 210 as statin_avg_period, 
       183 as pregnancy_prevalence, 75 as max_diabetes_age 
into temporary temp_timedelta_parms;

select e.patient_id, e.date, e.object_id, e.id, parm.*
into temporary temp_statin_rx_high
from hef_event e
join temp_timedelta_parms parm on 1=1
where name in ('rx:atorvastatin', 'rx:rosuvastatin');

SELECT * FROM temp_statin_rx_high;

select e.*, coalesce(p.start_date, e.date) start_date, 
  case when p.end_date is not null then p.end_date + interval '1 day' * e.statin_prevalence
       when p.dose is not null and p.quantity_float is not null and p.dose ~ '^[0-9\.]+$' then
           p.start_date + interval '1 day' * (round(e.statin_prevalence + (p.quantity_float/p.dose::numeric)::numeric*coalesce(p.refills::numeric,1)))
       else p.start_date + interval '1 day' *  (e.statin_prevalence+e.statin_avg_period)
  end end_date, 'intensity:h'::varchar(11) intensity
into temporary temp_statin_rx_high_spans
from temp_statin_rx_high e 
join emr_prescription p on p.id=e.object_id;

SELECT * FROM temp_statin_rx_high_spans;

select p.start_date, p.end_date, e.statin_prevalence, p.dose, 

    p.quantity_float, p.refills, e.statin_avg_period

from temp_statin_rx_high e 

join emr_prescription p on p.id=e.object_id;


SELECT * FROM hef_timespan;
SELECT * FROM nodis_case;


--2
select c.id, c.patient_id, c.condition as name, c.date,                               
	c.provider_id, ct.id content_type_id, 'diabetes' as crit                           
	from nodis_case c join django_content_type ct on ct.model='case' and ct.app_label='nodis'                           
	join emr_patient p on p.id=c.patient_id                           
	where condition in ('diabetes:type-1','diabetes:type-2')                             
	and date_part('year', age(c.date,p.date_of_birth)) between 40 and 75
	order by patient_id, date, crit desc
	
	select * from emr_pregnancy;

-- Code from Bob for Timespans 9/29


select 365 as dx_prevalence, 365 as statin_prevalence, 210 as statin_avg_period, 
       183 as pregnancy_prevalence, 75 as max_diabetes_age 
into temporary temp_timedelta_parms;

select id, patient_id, start_date, end_date, pattern
into temporary temp_hef_rx_spans
from hef_timespan
where name='statin';

--inspect temp_hef_rx_spans
select * from temp_hef_rx_spans;

select patient_id, start_date, end_date, rx.id as object_id, ct.id as content_type_id, pattern as htype 
into temporary statin_rx_obj
from temp_hef_rx_spans rx
join django_content_type ct on app_label='hef' and model='timespan';

--inspect statin_rx_obj
select * from statin_rx_obj;

with statin_island_starts as 
  (select *, 
     case when start_date-lag(end_date) over 
             (partition by patient_id, htype order by start_date, end_date, object_id) 
             is null then 1
          when start_date-lag(end_date) over 
             (partition by patient_id, htype order by start_date, end_date, object_id) 
              > 0 then 1
          else 0
      end island_start,
      row_number() over (partition by patient_id, htype order by start_date, end_date, object_id) rownumber
    from statin_rx_obj),
statin_island_grps as
  (select *, 
     sum(island_start) over 
        (partition by patient_id, htype order by start_date, end_date, object_id) as island
   from statin_island_starts),
statin_islands as
  (select patient_id, htype, min(start_date) as start_date, 
     max(coalesce(end_date,current_date)) end_date,
     max(rownumber) maxrow
   from statin_island_grps
   group by patient_id, htype, island)
select t0.patient_id, t0.start_date, t0.end_date, t1.object_id, t1.content_type_id, t1.htype,
  lead(t0.start_date) over (partition by t0.patient_id order by t0.start_date, t0.end_date) as next_start
into temporary statin_islands_last_object
from statin_islands t0 
join statin_island_grps t1 on t1.patient_id=t0.patient_id and t1.htype=t0.htype 
and t1.rownumber=t0.maxrow;

--inspect statin_island_last_object
select * from statin_islands_last_object;
--may need to break up the above query to inspect the WITH component tables

select patient_id, start_date, 
  case when end_date>next_start then next_start else end_date end as end_date, 
  object_id, content_type_id, htype 
into temporary statin_islands 
from statin_islands_last_object;

--inspect statin_islands
select * from statin_islands;


/*
	
