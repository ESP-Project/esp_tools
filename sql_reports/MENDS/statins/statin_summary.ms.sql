--Query #1
SELECT COUNT(*),
    CASE 
        WHEN status = 'C_' THEN 'Countraindicated'
        WHEN status = 'D_' THEN 'ESR or aged out'
        WHEN status = 'HA' THEN 'ASCVD high'
        WHEN status = 'HD' THEN 'Diabetes high'
        WHEN status = 'HH' THEN 'Hyperchol high'
        WHEN status = 'NA' THEN 'ASCVD no rx'
        WHEN status = 'ND' THEN 'Diabetes no rx'
        WHEN status = 'NH' THEN 'Hyperchol no rx'
        WHEN status = 'OA' THEN 'ASCVD moderate'
        WHEN status = 'OD' THEN 'Diabetes moderate'
        WHEN status = 'OH' THEN 'Hyperchol moderate'
    END AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        t0.patient_id, t1.status, t1.date
    FROM nodis_case t0
    JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'
    ORDER BY t0.patient_id, t1.date DESC
) t00
GROUP BY status
ORDER BY status;

--Query #2
SELECT COUNT(*),
    CASE 
        WHEN SUBSTRING(status, 2, 1) = 'A' THEN 'ASCVD'
        WHEN SUBSTRING(status, 2, 1) = 'D' THEN 'Diabetes'
        WHEN SUBSTRING(status, 2, 1) = 'H' THEN 'Hyperchol'
    END AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        t0.patient_id, t1.status, t1.date, t1.case_id
    FROM nodis_case t0
    JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'
    ORDER BY t0.patient_id, t1.date DESC
) t00
WHERE SUBSTRING(status, 2, 1) IN ('A', 'D', 'H')
    AND EXISTS (
        SELECT NULL
        FROM nodis_caseactivehistory t2
        WHERE t2.case_id = t00.case_id AND SUBSTRING(t2.status, 1, 1) IN ('O', 'H')
    )
GROUP BY SUBSTRING(status, 2, 1)
ORDER BY SUBSTRING(status, 2, 1);

--Query #3
SELECT COUNT(*),
    CASE 
        WHEN status = 'NA' THEN 'ASCVD no rx'
        WHEN status = 'ND' THEN 'Diabetes no rx'
        WHEN status = 'NH' THEN 'Hyperchol no rx'
    END AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        t0.patient_id, t1.status
    FROM nodis_case t0
    JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'
    ORDER BY t0.patient_id, t1.date DESC
) t00
WHERE status IN ('NA', 'ND', 'NH')
    AND NOT EXISTS (
        SELECT NULL
        FROM emr_prescription p
        WHERE p.patient_id = t00.patient_id
    )
GROUP BY status;

--Query #4
SELECT COUNT(*) AS counts, SUBSTRING(status, 1, 1)
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c.patient_id, cah.status
    FROM hef_event lr
    JOIN emr_patient p ON p.id = lr.patient_id AND YEAR(GETDATE()) - YEAR(p.date_of_birth) >= 20
    JOIN nodis_case c ON c.patient_id = lr.patient_id AND c.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c.id
    WHERE lr.name = 'lx:cholesterol-ldl:threshold:gte:190'
    ORDER BY c.patient_id, cah.date DESC
) t00
GROUP BY SUBSTRING(status, 1, 1);

--Query #5
SELECT COUNT(*) AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c.patient_id, cah.status
    FROM hef_event lr
    JOIN emr_patient p ON p.id = lr.patient_id AND YEAR(GETDATE()) - YEAR(p.date_of_birth) >= 20
    JOIN nodis_case c ON c.patient_id = lr.patient_id AND c.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c.id
    WHERE lr.name = 'lx:cholesterol-ldl:threshold:gte:190'
    ORDER BY c.patient_id, cah.date DESC
) t00
WHERE status IN ('NA', 'ND', 'NH')
    AND NOT EXISTS (
        SELECT NULL
        FROM emr_prescription p
        WHERE p.patient_id = t00.patient_id
    );

--Query #6
SELECT COUNT(*) AS counts, SUBSTRING(status, 1, 1)
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c.patient_id, cah.status, cah.case_id
    FROM hef_event lr
    JOIN emr_patient p ON p.id = lr.patient_id AND YEAR(GETDATE()) - YEAR(p.date_of_birth) >= 20
    JOIN nodis_case c ON c.patient_id = lr.patient_id AND c.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c.id
    WHERE lr.name = 'lx:cholesterol-ldl:threshold:gte:190'
    ORDER BY c.patient_id, cah.date DESC
) t00
WHERE EXISTS (
    SELECT NULL
    FROM hef_event t2
    WHERE t2.patient_id = t00.patient_id AND t2.name LIKE 'rx:%statin'
)
GROUP BY SUBSTRING(status, 1, 1);

--Query #7
SELECT COUNT(*) AS counts, SUBSTRING(status, 1, 1)
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c1.patient_id, cah.status
    FROM nodis_case c0
    JOIN emr_patient p ON p.id = c0.patient_id AND YEAR(GETDATE()) - YEAR(c0.date) BETWEEN 40 AND 75
    JOIN nodis_case c1 ON c1.patient_id = c0.patient_id AND c1.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c1.id
    WHERE c0.condition IN ('diabetes:type-1', 'diabetes:type-2')
    ORDER BY c1.patient_id, cah.date DESC
) t00
GROUP BY SUBSTRING(status, 1, 1);

--Query #8
SELECT COUNT(*) AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c1.patient_id, cah.status
    FROM nodis_case c0
    JOIN emr_patient p ON p.id = c0.patient_id AND YEAR(GETDATE()) - YEAR(c0.date) BETWEEN 40 AND 75
    JOIN nodis_case c1 ON c1.patient_id = c0.patient_id AND c1.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c1.id
    WHERE c0.condition IN ('diabetes:type-1', 'diabetes:type-2')
    ORDER BY c1.patient_id, cah.date DESC
) t00
WHERE status IN ('NA', 'ND', 'NH')
AND NOT EXISTS (
    SELECT NULL
    FROM emr_prescription p
    WHERE p.patient_id = t00.patient_id
);

--Query #9
SELECT COUNT(*) AS counts, SUBSTRING(status, 1, 1)
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        c1.patient_id, cah.status, cah.case_id
    FROM nodis_case c0
    JOIN emr_patient p ON p.id = c0.patient_id AND YEAR(GETDATE()) - YEAR(c0.date) BETWEEN 40 AND 75
    JOIN nodis_case c1 ON c1.patient_id = c0.patient_id AND c1.condition = 'statin'
    JOIN nodis_caseactivehistory cah ON cah.case_id = c1.id
    WHERE c0.condition IN ('diabetes:type-1', 'diabetes:type-2')
    ORDER BY c1.patient_id, cah.date DESC
) t00
WHERE EXISTS (
    SELECT NULL
    FROM hef_event t2
    WHERE t2.patient_id = t00.patient_id AND t2.name LIKE 'rx:%statin'
)
GROUP BY SUBSTRING(status, 1, 1);

--Query #10
SELECT COUNT(*) AS counts, pattern
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        p.id AS patient_id, ht.pattern
    FROM emr_patient p
    LEFT JOIN nodis_case c ON p.id = c.patient_id AND YEAR('2022-12-31') - YEAR(p.date_of_birth) BETWEEN 20 AND 85
    LEFT JOIN hef_timespan ht ON ht.patient_id = p.id AND ht.name = 'statin'
    WHERE c.id IS NULL
    ORDER BY p.id, ht.start_date DESC
) t00
GROUP BY pattern;

--Query #11
SELECT COUNT(*)
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        p.id AS patient_id, ht.pattern
    FROM emr_patient p
    LEFT JOIN nodis_case c ON p.id = c.patient_id AND YEAR('2022-12-31') - YEAR(p.date_of_birth) BETWEEN 20 AND 85
    LEFT JOIN hef_timespan ht ON ht.patient_id = p.id AND ht.name = 'statin'
    WHERE c.id IS NULL AND ht.patient_id IS NULL
    ORDER BY p.id, ht.start_date DESC
) t00
WHERE NOT EXISTS (
    SELECT NULL
    FROM emr_prescription p
    WHERE p.patient_id = t00.patient_id
);

--Query #12
SELECT name, COUNT(*) AS count
FROM emr_prescription p
WHERE EXISTS (
    SELECT NULL
    FROM (
        SELECT DISTINCT TOP 100 PERCENT
            t0.patient_id, t1.status
        FROM nodis_case t0
        JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
        WHERE t1.date <= '2022-12-31'
        ORDER BY t0.patient_id, t1.date DESC
    ) t00
    WHERE status IN ('NA', 'ND', 'NH') AND t00.patient_id = p.patient_id
)
GROUP BY name
ORDER BY COUNT(*) DESC
LIMIT 55;

--Query #13
SELECT patient_id INTO #chol_res
FROM emr_labresult l
JOIN conf_labtestmap lm ON lm.native_code = l.native_code
WHERE lm.test_name IN ('cholesterol-hdl', 'triglycerides', 'cholesterol-ldl', 'cholesterol-total');

SELECT COUNT(*),
    CASE 
        WHEN status = 'C_' THEN 'Countraindicated'
        WHEN status = 'D_' THEN 'ESR or aged out'
        WHEN status = 'HA' THEN 'ASCVD high'
        WHEN status = 'HD' THEN 'Diabetes high'
        WHEN status = 'HH' THEN 'Hyperchol high'
        WHEN status = 'NA' THEN 'ASCVD no rx'
        WHEN status = 'ND' THEN 'Diabetes no rx'
        WHEN status = 'NH' THEN 'Hyperchol no rx'
        WHEN status = 'OA' THEN 'ASCVD moderate'
        WHEN status = 'OD' THEN 'Diabetes moderate'
        WHEN status = 'OH' THEN 'Hyperchol moderate'
    END AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        t0.patient_id, t1.status, t1.date
    FROM nodis_case t0
    JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'
    ORDER BY t0.patient_id, t1.date DESC
) t00
WHERE EXISTS (
    SELECT NULL
    FROM #chol_res l
    WHERE l.patient_id = t00.patient_id
)
GROUP BY t00.status
ORDER BY t00.status;

--Query #14
SELECT COUNT(*),
    CASE 
        WHEN status = 'C_' THEN 'Countraindicated'
        WHEN status = 'D_' THEN 'ESR or aged out'
        WHEN status = 'HA' THEN 'ASCVD high'
        WHEN status = 'HD' THEN 'Diabetes high'
        WHEN status = 'HH' THEN 'Hyperchol high'
        WHEN status = 'NA' THEN 'ASCVD no rx'
        WHEN status = 'ND' THEN 'Diabetes no rx'
        WHEN status = 'NH' THEN 'Hyperchol no rx'
        WHEN status = 'OA' THEN 'ASCVD moderate'
        WHEN status = 'OD' THEN 'Diabetes moderate'
        WHEN status = 'OH' THEN 'Hyperchol moderate'
    END AS counts
FROM (
    SELECT DISTINCT TOP 100 PERCENT
        t0.patient_id, t1.status, t1.date
    FROM nodis_case t0
    JOIN nodis_caseactivehistory t1 ON t1.case_id = t0.id AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'
    ORDER BY t0.patient_id, t1.date DESC
) t00
WHERE EXISTS (
    SELECT NULL
    FROM emr_prescription p
    WHERE t00.patient_id = p.patient_id
)
GROUP BY status
ORDER BY status;

--Query #15
