#!/bin/bash
psql -c "delete from nodis_caseactivehistory t0 where exists (select null from nodis_case c where c.id=t0.case_id and c.condition='statin')"
psql -c " delete from nodis_case_events t0 where exists (select null from nodis_case c where c.id=t0.case_id and c.condition='statin')"
psql -c " delete from nodis_case_timespans t0 where exists (select null from nodis_case c where c.id=t0.case_id and c.condition='statin')"
psql -c " delete from nodis_case t0 where condition='statin'"
psql -c " delete from hef_timespan_events te where exists (select null from hef_timespan t where t.id=te.timespan_id and t.name='statin')"
psql -c " delete from hef_timespan where name='statin'"
psql -c " delete from hef_event where name like 'rx:%statin%'"
cd /srv/esp/prod 
bin/esp hef rx:statin && \
bin/esp hef timespan:statin &&\
bin/esp nodis statin