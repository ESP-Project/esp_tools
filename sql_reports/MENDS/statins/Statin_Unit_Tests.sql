/* Utility Queries:

-- List non-nullable columns for a table in the public schema
SELECT column_name
FROM information_schema.columns
WHERE table_schema = 'public'
AND table_name = 'hef_event'
AND is_nullable = 'NO';

-- List all possible values for INSERT INTOs
select distinct id from emr_provider;
select distinct provenance_id from emr_patient limit 100;

--Query #0
	Create Test Patients
	INSERT INTO emr_patient (id, created_timestamp, updated_timestamp, provenance_id) VALUES
	(2001, NOW(), NOW(), 1),
	(2002, NOW(), NOW(), 1),
	(2003, NOW(), NOW(), 1),
	(2004, NOW(), NOW(), 1);	
*/

SELECT * FROM emr_patient where id = 2001;

--Query #1 (Validated on 9-21)
    --Setup:
	
    --- Insert test data into nodis_case data
    INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
    (999999999, 2001, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999998, 2002, 'statin', '2022-12-29', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999997, 2003, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999996, 2004, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true);

    --- Insert test data into nodis_caseactivehistory
    INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
    (999999999, 'C_', '2022-12-30', 79, 123, NOW(), 'DIAB-NEW'), -- Contraindicated for patient 2001
    (999999998, 'HA', '2022-12-29', 79, 123, NOW(), 'DIAB-NEW'), -- ASCVD high for patient 2002
    (999999998, 'D_', '2022-12-01', 79, 123, NOW(), 'DIAB-NEW'), -- Shouldn't be counted for patient 2002 because 'HA' is more recent
    (999999997, 'OA', '2022-12-28', 79, 123, NOW(), 'DIAB-NEW'), -- ASCVD moderate for patient 2003
    (999999996, 'OH', '2022-12-27', 79, 123, NOW(), 'DIAB-NEW'); -- Hyperchol moderate for patient 2004

	--Run query #1
		select count(*), 
  		case when status='C_' then 'Countraindicated'
       	when status='D_' then 'ESR or aged out'
       	when status='HA' then 'ASCVD high'
       	when status='HD' then 'Diabetes high'
       	when status='HH' then 'Hyperchol high'
       	when status='NA' then 'ASCVD no rx'
       	when status='ND' then 'Diabetes no rx'
       	when status='NH' then 'Hyperchol no rx'
       	when status='OA' then 'ASCVD moderate'
       	when status='OD' then 'Diabetes moderate'
       	when status='OH' then 'Hyperchol moderate'
       	end counts
       from (
  			select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  			from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  			where t1.date<='2022-12-31'::date
  			order by t0.patient_id, t1.date desc) t00
  			group by status order by status;

	
    /*  Assert:
        Expected result table:
    | count | counts             |
    |-------|--------------------|
    | 1     | Contraindicated    |
    | 1     | ASCVD high         |
    | 1     | ASCVD moderate     |
    | 1     | Hyperchol moderate |
    */
	
    --Clean up test data
    DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999999, 999999998, 999999997, 999999996, 999999995, 999999994, 999999993, 999999992);
    DELETE FROM nodis_case WHERE patient_id IN (2001, 2002, 2003, 2004);

	
-- Query #2 (Validated on 9-21)
	-- Setup:
	--- Insert test data into nodis_case data
	INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
	(999999999, 2001, 'statin', '2022-12-15', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999998, 2002, 'statin', '2022-12-16', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999997, 2003, 'statin', '2022-12-17', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999996, 2004, 'statin', '2022-12-18', 42, 'test', NOW(), NOW(), true, 1, true);

	--- Insert test data into nodis_caseactivehistory
	INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
	(999999999, 'OA', '2022-12-15', 79, 123, NOW(), 'DIAB-NEW'), -- ASCVD for patient 2101
	(999999998, 'HD', '2022-12-16', 79, 123, NOW(), 'DIAB-NEW'), -- Diabetes for patient 2102
	(999999998, 'O_', '2022-12-01', 79, 123, NOW(), 'DIAB-NEW'), -- Should count for patient 2102 because of 'O' status
	(999999997, 'HH', '2022-12-17', 79, 123, NOW(), 'DIAB-NEW'), -- Hyperchol for patient 2103
	(999999996, 'OD', '2022-12-18', 79, 123, NOW(), 'DIAB-NEW');  -- Diabetes for patient 2104, but shouldn't be counted since no 'O' or 'H' status associated
	
	-- Run Query #2:  
	select count(*), 
	case substr(status, 2,1) 
    when 'A' then 'ASCVD'
    when 'D' then 'Diabetes'
    when 'H' then 'Hyperchol'
    end counts
	from (
    select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date, t1.case_id
    from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
    where t1.date<='2022-12-31'::date  
    order by t0.patient_id, t1.date desc) t00
	where substr(status, 2,1) in ('A','D','H') 
    and exists (select null from nodis_caseactivehistory t2 where t2.case_id=t00.case_id and substr(t2.status,1,1) in ('O','H'))
	group by substr(status,2,1) order by substr(status,2,1);

	/* 
	Assert:
	Expected result table:
	| count | counts     |
	|-------|------------|
	| 1     | ASCVD      |
	| 2     | Diabetes   |
	| 1     | Hyperchol  |
	*/

	-- Clean up test data
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999999, 999999998, 999999997, 999999996);
	DELETE FROM nodis_case WHERE id IN (999999999, 999999998, 999999997, 999999996);
	
--Query #3 (Validated on 9/24)
    --Setup:
    --- Insert test data into nodis_case
    INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
    (999999999, 2001, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true), -- Patient 2001
    (999999998, 2002, 'statin', '2022-12-29', 42, 'test', NOW(), NOW(), true, 1, true), -- Patient 2002
    (999999997, 2003, 'statin', '2022-12-28', 42, 'test', NOW(), NOW(), true, 1, true), -- Patient 2003
    (999999996, 2004, 'statin', '2022-12-27', 42, 'test', NOW(), NOW(), true, 1, true); -- Patient 2004

    --- Insert test data into nodis_caseactivehistory
    INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
    (999999999, 'NA', '2022-12-30', 79, 123, NOW(), 'DIAB-NEW'), -- ASCVD no rx for patient 2001
    (999999998, 'ND', '2022-12-29', 79, 123, NOW(), 'DIAB-NEW'), -- Diabetes no rx for patient 2002
    (999999997, 'NH', '2022-12-28', 79, 123, NOW(), 'DIAB-NEW'), -- Hyperchol no rx for patient 2003
    (999999996, 'OA', '2022-12-27', 79, 123, NOW(), 'DIAB-NEW'); -- ASCVD moderate for patient 2004 (won't be counted)

    -- Run query #3 here
    SELECT 
        count(*), 
        case 
            when status='NA' then 'ASCVD no rx'
            when status='ND' then 'Diabetes no rx'
            when status='NH' then 'Hyperchol no rx'
        end as counts
    FROM (
    -- Subquery to identify the most recent status for each patient as of the end of 2022.
    -- This ensures that even if a patient had multiple statuses during the year, only the latest is considered.
    SELECT DISTINCT ON (t0.patient_id) 
        t0.patient_id, 
        t1.status
    FROM nodis_case t0 
    JOIN nodis_caseactivehistory t1 
    ON t1.case_id = t0.id 
    AND t0.condition = 'statin'
    WHERE t1.date <= '2022-12-31'::date
    ORDER BY t0.patient_id, t1.date DESC
    ) t00
    -- Filter to include only relevant statuses.
    WHERE status IN ('NA','ND','NH')
    -- Ensure that the patient does not have any prescription record.
    AND NOT EXISTS (
        SELECT NULL 
        FROM emr_prescription p 
        WHERE p.patient_id = t00.patient_id
    )
    GROUP BY status;



    /*  Assert:
        Expected result table:
    | count | counts             |
    |-------|--------------------|
    | 1     | ASCVD no rx        |
    | 1     | Diabetes no rx     |
    | 1     | Hyperchol no rx    |
    */

    -- Clean up test data
    DELETE FROM hef_event WHERE patient_id IN (2001, 2002, 2003, 2004);
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999999, 999999998, 999999997, 999999996);
    DELETE FROM nodis_case WHERE id IN (999999999, 999999998, 999999997, 999999996);
	DELETE FROM emr_patient WHERE id IN (2001, 2002, 2003, 2004);
	
--Query #4 (Validated on 9/24)
    --Setup:
	--- Insert test data into emr_patient
	INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
	(2001, '2000-01-01', NOW(), NOW(), 1),
	(2002, '2002-01-01', NOW(), NOW(), 1),
	(2003, '2004-01-01', NOW(), NOW(), 1),
	(2004, '1990-01-01', NOW(), NOW(), 1);	

    --- Insert high LDL cholesterol events into hef_event
    INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
    (34, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-01', now(), 11, 49, 2001, 1),
    (35, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-02', now(), 12, 49, 2002, 1),
    (36, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-03', now(), 13, 49, 2003, 1),
    (37, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-04', now(), 14, 49, 2004, 1);


    --- Insert test data into nodis_case
    INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent,provider_id, isactive) VALUES
    (999999999, 2001, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999998, 2002, 'statin', '2022-12-29', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999997, 2003, 'statin', '2022-12-28', 42, 'test', NOW(), NOW(), true, 1, true),
    (999999996, 2004, 'statin', '2022-12-27', 42, 'test', NOW(), NOW(), true, 1, true);

    --- Insert test data into nodis_caseactivehistory
    INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
    (999999999, 'OA', '2022-12-30', 79, 123, NOW(), 'DIAB-NEW'), -- OA for patient 2001
    (999999998, 'HD', '2022-12-29', 79, 123, NOW(), 'DIAB-NEW'), -- HD for patient 2002
    (999999997, 'NH', '2022-12-28', 79, 123, NOW(), 'DIAB-NEW'), -- NH for patient 2003
    (999999996, 'OD', '2022-12-27', 79, 123, NOW(), 'DIAB-NEW'); -- OD for patient 2004

    -- Run query #4 here
	select count(*) as counts, substr(status,1,1) from (
  	select distinct on (c.patient_id) c.patient_id, cah.status
 	from hef_event lr 
  	join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  	join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  	join nodis_caseactivehistory cah on cah.case_id=c.id
  	where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  	order by c.patient_id, cah.date desc) t00
  	group by substr(status,1,1);

	
    /*  Assert:
        Expected result table (based on the statuses):
    | count | substr |
    |-------|--------|
    | 2     | O      |
    | 1     | H      |
    */

    -- Clean up test data
    DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999999, 999999998, 999999997, 999999996);
    DELETE FROM nodis_case WHERE id IN (999999999, 999999998, 999999997, 999999996);
    DELETE FROM hef_event WHERE patient_id IN (2001, 2002, 2003, 2004);
    DELETE FROM emr_patient WHERE id IN (2001, 2002, 2003, 2004);


--Query #5 (Validated on 9/24)
    --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (2001, '2000-01-01', NOW(), NOW(), 1),
        (2002, '2002-01-01', NOW(), NOW(), 1),
        (2003, '2004-01-01', NOW(), NOW(), 1),
        (2004, '1990-01-01', NOW(), NOW(), 1);	

        --- Insert high LDL cholesterol events into hef_event
        INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
        (34, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-01', now(), 11, 49, 2001, 1),
        (35, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-02', now(), 12, 49, 2002, 1),
        (36, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-03', now(), 13, 49, 2003, 1),
        (37, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-04', now(), 14, 49, 2004, 1);

        --- Insert test data into nodis_case
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999999, 2001, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999998, 2002, 'statin', '2022-12-29', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999997, 2003, 'statin', '2022-12-28', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999996, 2004, 'statin', '2022-12-27', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory for Query #5
        INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
        (999999999, 'NA', '2022-12-30', 79, 123, NOW(), 'DIAB-NEW'), -- NA for patient 2001
        (999999998, 'ND', '2022-12-29', 79, 123, NOW(), 'DIAB-NEW'), -- ND for patient 2002
        (999999997, 'NH', '2022-12-28', 79, 123, NOW(), 'DIAB-NEW'), -- NH for patient 2003, but we won't count this because of the patient's age.
        (999999996, 'OD', '2022-12-27', 79, 123, NOW(), 'DIAB-NEW'); -- OD for patient 2004, but we won't count this because the status is not among ('NA','ND','NH')

	  --Run Query #5
	  select count(*) as counts from (
	  select distinct on (c.patient_id) c.patient_id, cah.status
	  from hef_event lr 
	  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
	  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
	  join nodis_caseactivehistory cah on cah.case_id=c.id
	  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
	  order by c.patient_id, cah.date desc) t00
	  where status in ('NA','ND','NH')
		and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);
	
	/*  Assert:
        Expected result table (based on the statuses):
    | counts |
    |-------|
    |   2  	|
    */

	-- Clean up test data
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999999, 999999998, 999999997, 999999996);
	DELETE FROM nodis_case WHERE id IN (999999999, 999999998, 999999997, 999999996);
	DELETE FROM hef_event WHERE patient_id IN (2001, 2002, 2003, 2004);
	DELETE FROM emr_patient WHERE id IN (2001, 2002, 2003, 2004);

--Query #6 (Discrepency in results, discuss with Bob)
    -- Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (2001, '2000-01-01', NOW(), NOW(), 1),
        (2002, '2002-01-01', NOW(), NOW(), 1),
        (2003, '2002-01-01', NOW(), NOW(), 1), -- Modified to make the patient 20 years old
        (2004, '1990-01-01', NOW(), NOW(), 1);	

    --- Insert high LDL cholesterol events into hef_event
        INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
        (38, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-01', now(), 15, 49, 2001, 1),
        (39, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-02', now(), 16, 49, 2002, 1),
        (40, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-03', now(), 17, 49, 2003, 1),
        (41, 'lx:cholesterol-ldl:threshold:gte:190', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-04', now(), 18, 49, 2004, 1);

    --- Insert statin prescriptions for three patients into hef_event
		INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
		(42, 'rx:atorvastatin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-01-01', now(), 19, 49, 2001, 1), -- statin for patient 2001
		(43, 'rx:rosuvastatin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-01-02', now(), 20, 49, 2002, 1), -- statin for patient 2002
		(44, 'rx:simvastatin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-01-03', now(), 21, 49, 2004, 1);  -- statin for patient 2004

    --- Insert test data into nodis_case
		INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
		(999999995, 2001, 'statin', '2022-12-30', 42, 'test', NOW(), NOW(), true, 1, true),
		(999999994, 2002, 'statin', '2022-12-29', 42, 'test', NOW(), NOW(), true, 1, true),
		(999999993, 2003, 'statin', '2022-12-28', 42, 'test', NOW(), NOW(), true, 1, true),
		(999999992, 2004, 'statin', '2022-12-27', 42, 'test', NOW(), NOW(), true, 1, true);

    --- Insert test data into nodis_caseactivehistory
		INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
		(999999995, 'OA', '2022-12-30', 79, 123, NOW(), 'DIAB-NEW'), -- OA for patient 2001
		(999999994, 'HD', '2022-12-29', 79, 123, NOW(), 'DIAB-NEW'), -- HD for patient 2002
		(999999993, 'NH', '2022-12-28', 79, 123, NOW(), 'DIAB-NEW'), -- NH for patient 2003
		(999999992, 'OD', '2022-12-27', 79, 123, NOW(), 'DIAB-NEW'); -- OD for patient 2004

	-- Run query #6 here
	  select count(*) as counts, substr(status,1,1) from (
	  select distinct on (c.patient_id) c.patient_id, cah.status, cah.case_id
	  from hef_event lr 
	  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
	  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
	  join nodis_caseactivehistory cah on cah.case_id=c.id
	  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
	  order by c.patient_id, cah.date desc) t00
	  where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
	  group by substr(status,1,1);

	/* Assert:
	 Expected results
	------------------
	| count | substr |
	|   1   |   O    |
	|   1   |   H    |
	|   1   |   D    |
	
	 Actual Results:
	 ----------------
	 |  2   |   O   |
	 |  1   |   H   |
	*/
	
	
	select lr.patient_id, p.date_of_birth, lr.name as event_name, c.condition, cah.status
	from hef_event lr
	join emr_patient p ON p.id=lr.patient_id and date_part('year', age(lr.date, p.date_of_birth)) >=20
	join nodis_case c ON c.patient_id=lr.patient_id AND c.condition='statin'
	join nodis_caseactivehistory cah ON cah.case_id=c.id
	where lr.name='lx:cholesterol-ldl:threshold:gte:190'
	and exists (select null from hef_event t2 where t2.patient_id=lr.patient_id and t2.name ilike 'rx:%statin');
	-- Clean up test data
	DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999995, 999999994, 999999993, 999999992);
	DELETE FROM nodis_case WHERE id IN (999999995, 999999994, 999999993, 999999992);
	DELETE FROM hef_event WHERE id IN (38, 39, 40, 41, 42, 43, 44);
	DELETE FROM emr_patient WHERE id IN (2001, 2002, 2003, 2004);

--Query #7 (Validated 9/24/23)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (3001, '1980-01-01', NOW(), NOW(), 1),  -- 42 years old in 2022
        (3002, '1965-01-01', NOW(), NOW(), 1),  -- 57 years old in 2022
        (3003, '1950-01-01', NOW(), NOW(), 1),  -- 72 years old in 2022
        (3004, '1995-01-01', NOW(), NOW(), 1);  -- 27 years old in 2022

        --- Insert data into nodis_case for diabetes conditions
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999995, 3001, 'diabetes:type-1', '2022-12-15', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999994, 3002, 'diabetes:type-2', '2022-12-16', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999993, 3003, 'diabetes:type-1', '2022-12-17', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999992, 3004, 'diabetes:type-2', '2022-12-18', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert data into nodis_case for statin condition
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999991, 3001, 'statin', '2022-12-19', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999990, 3002, 'statin', '2022-12-20', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999989, 3003, 'statin', '2022-12-21', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999988, 3004, 'statin', '2022-12-22', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory
        INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
        (999999991, 'OA', '2022-12-19', 79, 123, NOW(), 'DIAB-NEW'),  -- OA for patient 3001
        (999999990, 'HD', '2022-12-20', 79, 123, NOW(), 'DIAB-NEW'),  -- HD for patient 3002
        (999999989, 'NH', '2022-12-21', 79, 123, NOW(), 'DIAB-NEW'),  -- NH for patient 3003
        (999999988, 'OD', '2022-12-22', 79, 123, NOW(), 'DIAB-NEW');  -- OD for patient 3004

    -- Run query #7 here
    select count(*) as counts, substr(status,1,1) from (
    select distinct on (c1.patient_id) c1.patient_id, cah.status
    from nodis_case c0 
    join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
    join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
    join nodis_caseactivehistory cah on cah.case_id=c1.id
    where c0.condition in ('diabetes:type-1','diabetes:type-2')
    order by c1.patient_id, cah.date desc) t00
    group by substr(status,1,1);


    /* Assert:
        -- Expected result table (based on the patients' age and statuses):
        -- | count | substr |
        -- |-------|--------|
        -- | 1     | O      |
        -- | 1     | H      |
        -- | 1     | N      |
    */

    -- Clean up test data
        DELETE FROM nodis_caseactivehistory WHERE case_id IN (999999991, 999999990, 999999989, 999999988);
        DELETE FROM nodis_case WHERE id BETWEEN 999999992 AND 999999995;
        DELETE FROM nodis_case WHERE id BETWEEN 999999988 AND 999999991;
        DELETE FROM emr_patient WHERE id BETWEEN 3001 AND 3004;


--Query #8 (Discrepency in expected vs actual, discuss with Bob)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (4001, '1980-01-01', NOW(), NOW(), 1),  -- 42 years old in 2022
        (4002, '1965-01-01', NOW(), NOW(), 1),  -- 57 years old in 2022
        (4003, '1950-01-01', NOW(), NOW(), 1),  -- 72 years old in 2022
        (4004, '1995-01-01', NOW(), NOW(), 1);  -- 27 years old in 2022

        --- Insert data into nodis_case for diabetes conditions
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999987, 4001, 'diabetes:type-1', '2022-12-15', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999986, 4002, 'diabetes:type-2', '2022-12-16', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999985, 4003, 'diabetes:type-1', '2022-12-17', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999984, 4004, 'diabetes:type-2', '2022-12-18', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert data into nodis_case for statin condition
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999983, 4001, 'statin', '2022-12-19', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999982, 4002, 'statin', '2022-12-20', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999981, 4003, 'statin', '2022-12-21', 42, 'test', NOW(), NOW(), true, 1, true),
        (999999980, 4004, 'statin', '2022-12-22', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory
        INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
        (999999983, 'NA', '2022-12-19', 79, 123, NOW(), 'DIAB-NEW'),  -- NA for patient 4001
        (999999982, 'NH', '2022-12-20', 79, 123, NOW(), 'DIAB-NEW'),  -- NH for patient 4002
        (999999981, 'ND', '2022-12-21', 79, 123, NOW(), 'DIAB-NEW'),  -- ND for patient 4003
        (999999980, 'OD', '2022-12-22', 79, 123, NOW(), 'DIAB-NEW');  -- OD for patient 4004

        --- Note: No data for emr_prescription as the query is meant to find those without prescriptions

    -- Run query #8 here
        select count(*) as counts from (
        select distinct on (c1.patient_id) c1.patient_id, cah.status
        from nodis_case c0 
        join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
        join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
        join nodis_caseactivehistory cah on cah.case_id=c1.id
        where c0.condition in ('diabetes:type-1','diabetes:type-2')
        order by c1.patient_id, cah.date desc) t00
        where status in ('NA','ND','NH')
        and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);
    /* Assert:
     Expected result table:
     | count |
     |-------|
     | 3     |
	
	 Actual result table:
	 | counts |
	 |--------|
	 | 4      |
	*/


    -- Clean up test data
    DELETE FROM nodis_caseactivehistory WHERE case_id BETWEEN 999999980 AND 999999983;
    DELETE FROM nodis_case WHERE id BETWEEN 999999984 AND 999999987;
    DELETE FROM nodis_case WHERE id BETWEEN 999999980 AND 999999983;
    DELETE FROM emr_patient WHERE id BETWEEN 4001 AND 4004;


--Query #9 (Validated on 9/24/23)
	--Setup:
	--- Insert test data into emr_patient
	INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
	(5001, '1980-01-01', NOW(), NOW(), 1),  -- 42 years old in 2022 (Should qualify on age)
	(5002, '1965-01-01', NOW(), NOW(), 1),  -- 57 years old in 2022 (Should qualify on age)
	(5003, '1950-01-01', NOW(), NOW(), 1),  -- 72 years old in 2022 (Should qualify on age)
	(5004, '1995-01-01', NOW(), NOW(), 1);  -- 27 years old in 2022 (Should disqualify on age)

	--- Insert data into nodis_case for diabetes conditions
	INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
	(999999977, 5001, 'diabetes:type-1', '2022-12-15', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999976, 5002, 'diabetes:type-2', '2022-12-16', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999975, 5003, 'diabetes:type-1', '2022-12-17', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999974, 5004, 'diabetes:type-2', '2022-12-18', 42, 'test', NOW(), NOW(), true, 1, true);

	--- Insert data into nodis_case for statin condition
	INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
	(999999973, 5001, 'statin', '2022-12-19', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999972, 5002, 'statin', '2022-12-20', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999971, 5003, 'statin', '2022-12-21', 42, 'test', NOW(), NOW(), true, 1, true),
	(999999970, 5004, 'statin', '2022-12-22', 42, 'test', NOW(), NOW(), true, 1, true);

	--- Insert test data into nodis_caseactivehistory
	INSERT INTO nodis_caseactivehistory (case_id, status, date, content_type_id, object_id, latest_event_date, change_reason) VALUES
	(999999973, 'NA', '2022-12-19', 79, 123, NOW(), 'DIAB-NEW'),  -- NA for patient 5001
																  -- 5002 Missing case_activehistory entry (Should disqualify)
	(999999971, 'NH', '2022-12-21', 79, 123, NOW(), 'DIAB-NEW'),  -- ND for patient 5003
	(999999970, 'OD', '2022-12-22', 79, 123, NOW(), 'DIAB-NEW');  -- OD for patient 5004
	
	--- Insert test data into hef_event for prescription of statin
	INSERT INTO hef_event (id, name, source, date, timestamp, object_id, content_type_id, patient_id, provider_id) VALUES
	(34, 'rx:statin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-25', now(), 11, 49, 5001, 1),
	(35, 'rx:another_drug', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-25', now(), 12, 49, 5002, 1),
	(36, 'rx:statin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-25', now(), 13, 49, 5003, 1),
	(37, 'rx:statin', 'urn:x-esphealth:heuristic:channing:diagnosis:v1', '2022-12-25', now(), 14, 49, 5004, 1);
	
	-- Run query #9
	select count(*) as counts, substr(status,1,1) from (
	select distinct on (c1.patient_id) c1.patient_id, cah.status, cah.case_id
	from nodis_case c0 
	join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
	join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
	join nodis_caseactivehistory cah on cah.case_id=c1.id
	where c0.condition in ('diabetes:type-1','diabetes:type-2')
	order by c1.patient_id, cah.date desc) t00
	where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
	group by substr(status,1,1);

	/* Assert:
	 Expected result table (based on the patients' age, statuses, and prescription of statin):
	 | counts | status |
	 |--------|--------|
	 | 2      | N      |
	*/

	-- Clean up test data
	DELETE FROM hef_event WHERE patient_id BETWEEN 5001 AND 5004;
	DELETE FROM nodis_caseactivehistory WHERE case_id BETWEEN 999999970 AND 999999973;
	DELETE FROM nodis_case WHERE id BETWEEN 999999974 AND 999999977;
	DELETE FROM nodis_case WHERE id BETWEEN 999999970 AND 999999973;
	DELETE FROM emr_patient WHERE id BETWEEN 5001 AND 5004;


--Query #10 (Need to discuss with Bob)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (6001, '1980-01-01', NOW(), NOW(), 1),  -- 42 years old in 2022
        (6002, '1995-01-01', NOW(), NOW(), 1),  -- 27 years old in 2022
        (6003, '2005-01-01', NOW(), NOW(), 1),  -- 17 years old in 2022 (Should disqualify based on aged)
        (6004, '1937-01-01', NOW(), NOW(), 1);  -- 85 years old in 2022

        --- Insert test data into nodis_case 
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999983, 6002, 'another_condition', '2022-12-20', 42, 'test', NOW(), NOW(), true, 1, true); --(Should disqualify based on existence) 

        --- Insert test data into hef_timespan for statin pattern
        INSERT INTO hef_timespan (id, name, source, start_date, end_date, timestamp, pattern, patient_id) VALUES
        (999999999, 'statin', 79, '2022-12-19', '2023-01-01', now(), 'intensity:h', 6001),
        (999999998, 'statin', 79, '2022-12-20', '2023-01-01', now(), 'intensity:h', 6002),
        (999999997, 'statin', 79, '2022-12-21', '2023-01-01', now(), 'intensity:m', 6003);
		
    -- Run query #10 here
    select count(*) as counts, pattern from (
    select distinct on (p.id) p.id patient_id, ht.pattern
    from emr_patient p 
    left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
    left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
    where c.id is null
    order by p.id, ht.start_date desc) t00
    group by pattern;

    /* Assert:
        Expected result table (based on the patients' age and absence of nodis_case entries for the condition 'statin' but presence in hef_timespan):
    | counts | pattern     |
    |--------|-------------|
    | 1      | intensity:h |
    | 1      | intensity:m |
    | 1      | NULL        |  (For patient 6004, since they have neither a nodis_case entry for 'statin' nor a pattern in hef_timespan)
    */

    -- Clean up test data
    DELETE FROM hef_timespan WHERE patient_id BETWEEN 6001 AND 6003;
    DELETE FROM nodis_case WHERE id = 999999983;
    DELETE FROM emr_patient WHERE id BETWEEN 6001 AND 6004;

--Query #11 (Unit Test)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (7001, '1980-01-01', NOW(), NOW(), 1),  -- 42 years old in 2022
        (7002, '1995-01-01', NOW(), NOW(), 1),  -- 27 years old in 2022
        (7003, '2005-01-01', NOW(), NOW(), 1),  -- 17 years old in 2022
        (7004, '1937-01-01', NOW(), NOW(), 1);  -- 85 years old in 2022

        --- Insert test data into nodis_case 
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (999999993, 7002, 'another_condition', '2022-12-20', 42, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into emr_prescription
        INSERT INTO emr_prescription (id, patient_id, drug_name, created_timestamp, updated_timestamp, status) VALUES
        (8001, 7001, 'drugX', NOW(), NOW(), 'active'),
        (8002, 7003, 'drugY', NOW(), NOW(), 'inactive');

    -- Run query #11 
    Select count(*) from (
    select distinct on (p.id) p.id patient_id, ht.pattern
    from emr_patient p 
    left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
    left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
    where c.id is null  and ht.patient_id is null
    order by p.id, ht.start_date desc) t00
    where not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);
    /* Assert:
        Expected result table:
        Only patient 7004 should be counted as:
        7001 has a prescription, 
        7002 has a nodis_case but for 'another_condition', 
        and 7003 is below the age bracket and also has a prescription.

        Result should be:
        | count |
        |-------|
        | 1     |
    */
    --Clean up test data
    DELETE FROM emr_prescription WHERE id BETWEEN 8001 AND 8002;
    DELETE FROM nodis_case WHERE id = 999999993;
    DELETE FROM emr_patient WHERE id BETWEEN 7001 AND 7004;


--Query #12 (Unit Test)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (8001, '1980-01-01', NOW(), NOW(), 1), 
        (8002, '1985-01-01', NOW(), NOW(), 1);

        --- Insert test data into nodis_case 
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (1000000001, 8001, 'statin', '2022-12-20', 43, 'test', NOW(), NOW(), true, 1, true),
        (1000000002, 8002, 'statin', '2022-12-20', 44, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory
        INSERT INTO nodis_caseactivehistory (id, case_id, status, date, source) VALUES
        (10001, 1000000001, 'NA', '2022-12-30', 43),
        (10002, 1000000002, 'NH', '2022-12-30', 44);

        --- Insert test data into emr_prescription
        INSERT INTO emr_prescription (id, patient_id, drug_name, created_timestamp, updated_timestamp, status) VALUES
        (9001, 8001, 'drugX', NOW(), NOW(), 'active'),
        (9002, 8001, 'drugY', NOW(), NOW(), 'active'),
        (9003, 8002, 'drugX', NOW(), NOW(), 'inactive');

    -- Run the main query #12 here
    select name, count(*) as count
    from emr_prescription p
    where exists (select null from (
    select distinct on (t0.patient_id) t0.patient_id, t1.status
    from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
    where t1.date<='2022-12-31'::date
    order by t0.patient_id, t1.date desc) t00
    where status in ('NA','ND','NH') and t00.patient_id=p.patient_id)
    group by name order by count(*) desc limit 55;
    
    /* Assert:
     Expected result table:
     Both patients 8001 and 8002 are statin-eligible with 'NA' and 'NH' status respectively.
     Hence, their prescriptions should be counted.
     | name  | count |
     |-------|-------|
     | drugX | 2     |
     | drugY | 1     |
    */
    
    -- Clean up test data
    DELETE FROM emr_prescription WHERE id BETWEEN 9001 AND 9003;
    DELETE FROM nodis_caseactivehistory WHERE id BETWEEN 10001 AND 10002;
    DELETE FROM nodis_case WHERE id BETWEEN 1000000001 AND 1000000002;
    DELETE FROM emr_patient WHERE id BETWEEN 8001 AND 8002;

--Query #13 (Unit Test)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (9001, '1980-01-01', NOW(), NOW(), 1), 
        (9002, '1985-01-01', NOW(), NOW(), 1);

        --- Insert test data into nodis_case 
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (2000000001, 9001, 'statin', '2022-12-20', 53, 'test', NOW(), NOW(), true, 1, true),
        (2000000002, 9002, 'statin', '2022-12-20', 54, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory
        INSERT INTO nodis_caseactivehistory (id, case_id, status, date, source) VALUES
        (11001, 2000000001, 'NA', '2022-12-30', 53),
        (11002, 2000000002, 'NH', '2022-12-30', 54);

        --- Insert test data into emr_labresult
        INSERT INTO emr_labresult (id, patient_id, native_code, value, created_timestamp) VALUES
        (9101, 9001, 'A1', 50, NOW()),
        (9102, 9002, 'A2', 70, NOW());

        --- Insert test data into conf_labtestmap
        INSERT INTO conf_labtestmap (id, native_code, test_name, created_timestamp) VALUES
        (12001, 'A1', 'cholesterol-hdl', NOW()),
        (12002, 'A2', 'cholesterol-ldl', NOW());

    -- Run query #13 here
    select count(*), 
    case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_labresult l join conf_labtestmap lm on lm.native_code=l.native_code where l.patient_id=t00.patient_id and lm.test_name in ('cholesterol-hdl','triglycerides','cholesterol-ldl','cholesterol-total'))
  group by status order by status;

    /* Assert:
     Expected result table:
     Both patients 9001 and 9002 have undergone cholesterol tests ('cholesterol-hdl' and 'cholesterol-ldl' respectively).
     Their statuses 'NA' and 'NH' mean 'ASCVD no rx' and 'Hyperchol no rx', respectively.
     | count | counts           |
     |-------|------------------|
     | 1     | ASCVD no rx      |
     | 1     | Hyperchol no rx  |
    */

    -- Clean up test data
    DELETE FROM conf_labtestmap WHERE id BETWEEN 12001 AND 12002;
    DELETE FROM emr_labresult WHERE id BETWEEN 9101 AND 9102;
    DELETE FROM nodis_caseactivehistory WHERE id BETWEEN 11001 AND 11002;
    DELETE FROM nodis_case WHERE id BETWEEN 2000000001 AND 2000000002;
    DELETE FROM emr_patient WHERE id BETWEEN 9001 AND 9002;

--Query #14 (Unit Test)
    --Setup:
    --- Insert test data into emr_patient
    INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
    (9501, '1970-01-01', NOW(), NOW(), 1), 
    (9502, '1975-01-01', NOW(), NOW(), 1);

    --- Insert test data into nodis_case 
    INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
    (2100000001, 9501, 'statin', '2022-12-10', 55, 'test', NOW(), NOW(), true, 1, true),
    (2100000002, 9502, 'statin', '2022-12-10', 56, 'test', NOW(), NOW(), true, 1, true);

    --- Insert test data into nodis_caseactivehistory
    INSERT INTO nodis_caseactivehistory (id, case_id, status, date, source) VALUES
    (11501, 2100000001, 'NA', '2022-12-15', 55),
    (11502, 2100000002, 'NH', '2022-12-15', 56);

    --- Insert test data into emr_prescription
    INSERT INTO emr_prescription (id, patient_id, name, dosage, created_timestamp) VALUES
    (9301, 9501, 'Medication A', '10mg', NOW()),
    (9302, 9502, 'Medication B', '20mg', NOW());

-- Run query #14 
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_prescription p where t00.patient_id=p.patient_id)
  group by status order by status;

/* 
    Assert:
    Expected result table:
    Both patients 9501 and 9502 have prescriptions (Medication A and Medication B respectively).
    Their statuses 'NA' and 'NH' translate to 'ASCVD no rx' and 'Hyperchol no rx', respectively.
    | count | counts           |
    |-------|------------------|
    | 1     | ASCVD no rx      |
    | 1     | Hyperchol no rx  |
*/

-- Clean up test data
DELETE FROM emr_prescription WHERE id BETWEEN 9301 AND 9302;
DELETE FROM nodis_caseactivehistory WHERE id BETWEEN 11501 AND 11502;
DELETE FROM nodis_case WHERE id BETWEEN 2100000001 AND 2100000002;
DELETE FROM emr_patient WHERE id BETWEEN 9501 AND 9502;

--Query #15 (Unit Test)
    --Setup:
        --- Insert test data into emr_patient
        INSERT INTO emr_patient (id, date_of_birth, created_timestamp, updated_timestamp, provenance_id) VALUES
        (9601, '1970-01-01', NOW(), NOW(), 1), 
        (9602, '1975-01-01', NOW(), NOW(), 1);

        --- Insert test data into nodis_case 
        INSERT INTO nodis_case (id, patient_id, condition, date, source, status, created_timestamp, updated_timestamp, followup_sent, provider_id, isactive) VALUES
        (2200000001, 9601, 'statin', '2022-12-10', 57, 'test', NOW(), NOW(), true, 1, true),
        (2200000002, 9602, 'statin', '2022-12-10', 58, 'test', NOW(), NOW(), true, 1, true);

        --- Insert test data into nodis_caseactivehistory
        INSERT INTO nodis_caseactivehistory (id, case_id, status, date, source) VALUES
        (11601, 2200000001, 'NA', '2022-12-15', 57),
        (11602, 2200000002, 'NH', '2022-12-15', 58);

        --- Insert test data into emr_encounter
        INSERT INTO emr_encounter (id, patient_id, raw_encounter_type, date) VALUES
        (9401, 9601, 'Inpatient Test', '2022-12-01'),
        (9402, 9602, 'Inpatient Test', '2022-01-10');

        --- Insert test data into gen_pop_tools.rs_conf_mapping
        INSERT INTO gen_pop_tools.rs_conf_mapping (src_field, src_value, mapped_value) VALUES
        ('raw_encounter_type', 'Inpatient Test', 'inpatient');

    -- Run query #15
    select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_encounter e 
                join gen_pop_tools.rs_conf_mapping m on m.src_field='raw_encounter_type' and m.src_value=e.raw_encounter_type and m.mapped_value='inpatient'
                where t00.patient_id=e.patient_id and e.date between '2021-01-01'::date and '2022-12-31'::Date)
  group by status order by status;



    /* Assert:
     Expected result table:
     Both patients 9601 and 9602 had inpatient encounters in the specified date range.
     Their statuses 'NA' and 'NH' translate to 'ASCVD no rx' and 'Hyperchol no rx', respectively.
     | count | counts           |
     |-------|------------------|
     | 1     | ASCVD no rx      |
     | 1     | Hyperchol no rx  |
    */
    -- Clean up test data
    DELETE FROM gen_pop_tools.rs_conf_mapping WHERE src_field = 'raw_encounter_type' AND src_value = 'Inpatient Test';
    DELETE FROM emr_encounter WHERE id BETWEEN 9401 AND 9402;
    DELETE FROM nodis_caseactivehistory WHERE id BETWEEN 11601 AND 11602;
    DELETE FROM nodis_case WHERE id BETWEEN 2200000001 AND 2200000002;
    DELETE FROM emr_patient WHERE id BETWEEN 9601 AND 9602;
