/* Query #1:  
   Retrieves a summary of distinct patient counts, as of the end of 2022, 
   for the 'statin' condition. The summary is categorized by the status of the patients, 
   with each status code being translated to its descriptive meaning. 
   The results are sorted by the status code. */
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  group by status order by status;


/* Query #2:
   Retrieves a summary of distinct patient counts, as of the end of 2022, 
   who have been prescribed a statin. Patients are categorized based on a specific 
   character of their status ('ASCVD', 'Diabetes', or 'Hyperchol'), and additionally, 
   these patients must have a related history record with a status starting with 'O' or 'H'. 
   The results are sorted based on the aforementioned character from the status. */
select count(*), 
  case substr(status, 2,1) 
       when 'A' then 'ASCVD'
       when 'D' then 'Diabetes'
       when 'H' then 'Hyperchol'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date, t1.case_id
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date  
  order by t0.patient_id, t1.date desc) t00
  where substr(status, 2,1) in ('A','D','H') 
    and exists (select null from nodis_caseactivehistory t2 where t2.case_id=t00.case_id and substr(t2.status,1,1) in ('O','H'))
  group by substr(status,2,1) order by substr(status,2,1);


/* Query #3: 
   Statin Eligible Patients without Statin Prescription and NO Prescription Records
   This query fetches and aggregates the counts of patients eligible for statin therapy, based on specific statuses,
   but who haven't received a statin prescription and importantly, have no records at all in the prescription table. 
   */
SELECT 
  count(*), 
  
  case when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       end as counts
       
FROM (
  -- Subquery to identify the most recent status for each patient as of the end of 2022.
  -- This ensures that even if a patient had multiple statuses during the year, only the latest is considered.
  SELECT DISTINCT ON (t0.patient_id) 
    t0.patient_id, 
    t1.status
  FROM nodis_case t0 
  JOIN nodis_caseactivehistory t1 
    ON t1.case_id = t0.id 
    AND t0.condition = 'statin'
  WHERE t1.date <= '2022-12-31'::date
  ORDER BY t0.patient_id, t1.date DESC
) t00
-- Filter to include only relevant statuses.
WHERE status IN ('NA','ND','NH')
-- Ensure that the patient does not have any prescription record.
AND NOT EXISTS (
  SELECT NULL 
  FROM emr_prescription p 
  WHERE p.patient_id = t00.patient_id
)
GROUP BY status;
  
  
  
/* Query #4: Count of LDL patients by high-level status
 This query identifies patients with a high LDL cholesterol level (greater than or equal to 190 mg/dL) 
 who were 20 years or older at the time of the cholesterol event.
 It fetches the most recent statin-related status for these patients and aggregates the counts
 based on the first character of their status code. 
 
 <-- Maps to workbook R3CF-H -->
 */
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c.patient_id) c.patient_id, cah.status
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  group by substr(status,1,1);
  
  /* Query #5: This query identifies patients who have an LDL cholesterol level greater than or equal to 190, are aged 20 or
     older, have been flagged for statin therapy, have a certain status indicating they're not on a prescription (e.g., 'NA', 'ND', 
     or 'NH'), and have no records in the prescription table.
     
     <-- Maps to workbook R3CE -->
     */
  select count(*) as counts from (
  select distinct on (c.patient_id) c.patient_id, cah.status
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  where status in ('NA','ND','NH')
    and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);

/* Query #6 Summary:

This query identifies patients who:
1. Have an LDL cholesterol event that surpasses a specific threshold, indicating high LDL cholesterol levels.
2. Are at least 20 years old at the time of their cholesterol event.
3. Have a 'statin' condition recorded in the system.
4. Have ever been prescribed a medication that contains the term 'statin' (indicative of a class of drugs that reduce cholesterol).

The final output aggregates the count of these patients based on the first character of their status, which broadly categorizes them into various health conditions. 

<-- Maps to Spreadsheet R3CI? -->
*/
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c.patient_id) c.patient_id, cah.status, cah.case_id
  from hef_event lr 
  join emr_patient p on p.id=lr.patient_id and date_part('year', age(lr.date,p.date_of_birth)) >=20
  join nodis_case c on c.patient_id=lr.patient_id and c.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c.id
  where lr.name='lx:cholesterol-ldl:threshold:gte:190'
  order by c.patient_id, cah.date desc) t00
  where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
  group by substr(status,1,1);
  

 /* Query #7 Summary:
    This query identifies patients who:
    1. Have a recorded condition of either type-1 or type-2 diabetes.
    2. Are between the ages of 40 and 75 during the time 
    
    <-- Maps to R6CF-H
    */
  select count(*) as counts, substr(status,1,1) from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  group by substr(status,1,1);

/* Query #8 Summary:

  This query identifies patients who:
  1. Have a recorded condition of either type-1 or type-2 diabetes.
  2. Are between the ages of 40 and 75 during the time of their diabetes diagnosis.
  3. Also have a recorded condition for 'statin'.
  4. Have specific statuses of 'NA', 'ND', or 'NH' from the latest case active history.
  5. Do NOT have any recorded prescriptions in the system.*/
  select count(*) as counts from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  where status in ('NA','ND','NH')
  and not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);

/* Query #9 Summary:

This query is designed to identify patients who:
1. Are diagnosed with either type-1 or type-2 diabetes.
2. Are between the ages of 40 and 75 during the time of their diabetes diagnosis.
3. Also have a recorded condition for 'statin'.
4. Are further verified by their presence in the hef_event table, where they have been prescribed a medication that matches the pattern 'rx:%statin'.

The result set is grouped by the first character of the status of their latest nodis_caseactivehistory record, providing a count for each status type.
This allows us to understand how many diabetes patients, within the specified age range, have been prescribed statins and their respective status distributions.*/
select count(*) as counts, substr(status,1,1) from (
  select distinct on (c1.patient_id) c1.patient_id, cah.status, cah.case_id
  from nodis_case c0 
  join emr_patient p on p.id=c0.patient_id and date_part('year', age(c0.date,p.date_of_birth)) between 40 and 75
  join nodis_case c1 on c1.patient_id=c0.patient_id and c1.condition='statin'
  join nodis_caseactivehistory cah on cah.case_id=c1.id
  where c0.condition in ('diabetes:type-1','diabetes:type-2')
  order by c1.patient_id, cah.date desc) t00
  where exists (select null from hef_event t2 where t2.patient_id=t00.patient_id and t2.name ilike 'rx:%statin')
  group by substr(status,1,1);
 
 
/*
Query #10 Summary:

This query identifies patients who:
1. Are between the ages of 20 and 85 as of December 31, 2022.
2. Do NOT have a nodis_case entry for the condition 'statin'.
3. Are cross-referenced with the hef_timespan table for any 'statin' patterns.

The resultant dataset enumerates such patients grouped by the 'pattern' attribute from the hef_timespan table. This means, the query essentially categorizes the non-statin eligible patients based on the patterns of their 'statin' timespans. 

It helps to identify how many patients, within the age bracket, are not explicitly marked for statin treatment but have a certain pattern associated with statin in the timespan data.
*/
  select count(*) as counts, pattern from (
  select distinct on (p.id) p.id patient_id, ht.pattern
  from emr_patient p 
  left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
  left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
  where c.id is null
  order by p.id, ht.start_date desc) t00
  group by pattern;
  
/* Query #11 Summary:

The query counts patients who are:
1. Aged between 20 and 85 as of December 31, 2022.
2. Have NOT been documented with a nodis_case entry for the condition 'statin'.
3. Do NOT have a corresponding 'statin' pattern in the hef_timespan table.
4. Have NOT been prescribed any medication (no entries in emr_prescription for the patient).

In essence, the query identifies patients within the age range who are not statin-eligible based on the nodis_case and hef_timespan data, and further refines this list by excluding any patient who has received a prescription. 

<-- Maps to workbook R8CB -->
*/
  Select count(*) from (
  select distinct on (p.id) p.id patient_id, ht.pattern
  from emr_patient p 
  left join nodis_case c on p.id=c.patient_id and date_part('year', age('2022-12-31'::date,p.date_of_birth)) between 20 and 85
  left join hef_timespan ht on ht.patient_id=p.id and ht.name='statin'
  where c.id is null  and ht.patient_id is null
  order by p.id, ht.start_date desc) t00
  where not exists (select null from emr_prescription p where p.patient_id=t00.patient_id);
  
  /* Query #12 Summary:

The query retrieves the 55 most common prescriptions for patients who are considered "statin-eligible" as of December 31, 2022. 

1. Initially, it identifies patients who have a 'statin' entry in the nodis_case table and further refines this list by looking into the nodis_caseactivehistory table to ensure the status is one of 'NA', 'ND', or 'NH'.
2. For these identified patients, the query then fetches all their prescriptions from the emr_prescription table.
3. It aggregates these prescriptions by drug name and counts their occurrences.
4. The result set is ordered in descending order of the count of prescriptions, with a maximum limit of 55 prescriptions displayed.*/
  select name, count(*) as count
  from emr_prescription p
  where exists (select null from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where status in ('NA','ND','NH') and t00.patient_id=p.patient_id)
  group by name order by count(*) desc limit 55;

/* Query #13 Summary:

The purpose of this query is to evaluate and categorize patients based on their specific 'statin' status and to check if they have undergone any cholesterol tests, specifically tests related to 'cholesterol-hdl', 'triglycerides', 'cholesterol-ldl', and 'cholesterol-total'.

1. Initially, the query identifies patients with a 'statin' entry in the nodis_case table and further evaluates this list by looking into the nodis_caseactivehistory table for records as of December 31, 2022.
2. For these identified patients, the query verifies if they have had cholesterol tests done by checking against the emr_labresult table and the conf_labtestmap.
3. Each patient's 'statin' status is then translated into a descriptive label using a CASE statement.
4. Finally, the results are aggregated based on the descriptive label, providing a count of patients for each label.

The outcome provides a categorized summary of patients based on their 'statin' status who have undergone cholesterol tests.


*/

select patient_id INTO TEMPORARY chol_res 
  from emr_labresult l 
  join conf_labtestmap lm on lm.native_code=l.native_code 
  where lm.test_name in ('cholesterol-hdl','triglycerides','cholesterol-ldl','cholesterol-total');
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from chol_res l where l.patient_id=t00.patient_id)
  group by t00.status order by t00.status;

/*
--Query #14 Summary:

The intent of this query is to determine and categorize patients based on their specific 'statin' status and assess if they have any prescriptions recorded in the database.

1. Initially, the query recognizes patients with a 'statin' entry in the nodis_case table and further scrutinizes this list by referencing the nodis_caseactivehistory table for records until December 31, 2022.
2. For the patients identified, the query checks if they have a prescription entry in the emr_prescription table.
3. Subsequently, each patient's 'statin' status is decoded into a descriptive label using a CASE statement.
4. Lastly, the results are grouped by the descriptive label, yielding a count of patients for each label.

The outcome provides a categorized tally of patients based on their 'statin' status who have prescriptions registered in the system.
*/
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_prescription p where t00.patient_id=p.patient_id)
  group by status order by status;
  
/*
--Query #15 Summary:

This query aims to identify and categorize patients based on their specific 'statin' status who also had more than one inpatient encounter in the past two years (between January 1, 2021, and December 31, 2022).

1. The query initially identifies patients with a 'statin' entry in the nodis_case table and references the nodis_caseactivehistory table for records up to December 31, 2022.
2. For the identified patients, the query then verifies if they had an inpatient encounter in the past two years. This determination is made using the emr_encounter table and ensuring that the encounter type matches the 'inpatient' type as defined in the gen_pop_tools.rs_conf_mapping.
3. Each patient's 'statin' status is then decoded into a descriptive label using a CASE statement.
4. Finally, the results are grouped by the descriptive label, giving a count of patients for each label.

The result provides a tally of patients categorized by their 'statin' status who also had inpatient encounters within the two-year period.
*/
select count(*), 
  case when status='C_' then 'Countraindicated'
       when status='D_' then 'ESR or aged out'
       when status='HA' then 'ASCVD high'
       when status='HD' then 'Diabetes high'
       when status='HH' then 'Hyperchol high'
       when status='NA' then 'ASCVD no rx'
       when status='ND' then 'Diabetes no rx'
       when status='NH' then 'Hyperchol no rx'
       when status='OA' then 'ASCVD moderate'
       when status='OD' then 'Diabetes moderate'
       when status='OH' then 'Hyperchol moderate'
       end counts
       from (
  select distinct on (t0.patient_id) t0.patient_id, t1.status, t1.date
  from nodis_case t0 join nodis_caseactivehistory t1 on t1.case_id=t0.id and t0.condition='statin'
  where t1.date<='2022-12-31'::date
  order by t0.patient_id, t1.date desc) t00
  where exists (select null from emr_encounter e 
                join gen_pop_tools.rs_conf_mapping m on m.src_field='raw_encounter_type' and m.src_value=e.raw_encounter_type and m.mapped_value='inpatient'
                where t00.patient_id=e.patient_id and e.date between '2021-01-01'::date and '2022-12-31'::Date)
  group by status order by status;




  
