package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class SummaryTest {
    
    /**
     * summaryTest - generates SQL for Summary test.
     * Expects input file, summary_in.txt, to have one value for each test:
     *   esp_column, in the form table.column
     *   
     *   NOTE (2021-02-16) For emr_encounter, added a constraint that date be between 2015-01-01 and sysdatetime
     */
    public static void summaryTest () {

        System.out.println("Starting SummaryTest");
        try {
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            
            Scanner in = new Scanner(Path.of("summary_in.txt"));
            PrintWriter out = new PrintWriter("summary." + rdbms + ".sql", StandardCharsets.UTF_8);
            
            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + "- gen)");
            out.println(" * Summary Test (ungrouped)...");
            out.println("*/"); 
            out.println("");           
            
             // Drop and create output table
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.summary', 'U') is not null drop table cii_qa.summary;");
                out.println("create table cii_qa.summary ");
                out.println("(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)");
                out.println(", n_total integer, n_missing integer, pct_missing float, n_empty integer");
                out.println(", pct_empty float, n_null integer, pct_null float);");
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.summary;");
                out.println("create table cii_qa.summary ");
                out.println("(test_name text, esp_table text, esp_column text");
                out.println(", n_total integer, n_missing integer, pct_missing float, n_empty integer");
                out.println(", pct_empty float, n_null integer, pct_null float);");                
            }
            
            // Loop through the input, generating SQL for each input line
            while (in.hasNext()) {
                /* TabColSpec is the information about the column.  It is in the form
                 * table.column,
                 * where 
                 *   table is the table name
                 *   column is the column name
                 * Use these to populate TableName, ColumnName, ColumnExp
                 */
                String TabColSpec = in.next();               
                String TableName = TabColSpec.substring(0,TabColSpec.indexOf("."));
                String ColumnString = TabColSpec.substring(TabColSpec.indexOf(".")+1);
                ColumnString = ColumnString.toLowerCase();
                String ColumnName = "";
                String ColumnExp = "";
                ColumnName = ColumnString;
                ColumnExp = ColumnString;
                
                // Special case for emr_patient.age_yrs_derived
                if (TableName.equalsIgnoreCase("emr_patient")
                    && ColumnName.equalsIgnoreCase("age_yrs_derived")) {
                        if (rdbms.equals("ms")) {
                            ColumnExp = "datediff(year, date_of_birth, sysdatetime())";
                        } else if (rdbms.equals("pg")) {
                            ColumnExp = "(extract(year from current_date) - extract(year from date_of_birth))";
                        }
                }
                
                String IncludedValueMin = in.next();
                String IncludedValueMax = in.next();
                
                System.out.println("TableName: " + TableName + ", ColumnName: " + ColumnName + ", ColumnExp: " + ColumnExp);
                System.out.println("Min and Max column values: " + IncludedValueMin +", " + IncludedValueMax);
               
                out.println("");
                out.println("/* " + TableName + "." + ColumnName + " */");
                out.println("insert into cii_qa.summary (");
                out.println("  test_name, esp_table, esp_column");
                out.println(", n_total, n_missing, pct_missing, n_empty");
                out.println(", pct_empty, n_null, pct_null)");
                out.println("select");
                out.println("    'Summary' as test_name");
                out.println("  , '" + TableName + "' as esp_table");
                out.println("  , '" + ColumnName + "' as esp_column");
                out.println("  , count(*) as n_total");
                out.println("  , sum(is_empty) + (count(*) - count(" + ColumnName + ")) as n_missing");
                out.println("  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(" + ColumnName + ")))/count(*),2) else null end as pct_missing");
                out.println("  , sum(is_empty) as n_empty");
                out.println("  , round(100.0*sum(is_empty)/count(*),2) as pct_empty"); 
                out.println("  , count(*) - count(" + ColumnName + ") as n_null");
                out.println("  , case when count(*) > 0 then round((100.0*(count(*) - count(" + ColumnName + "))/count(*)),2) else null end as pct_null");
                out.println("from (");
                out.println("  select ");
                out.println("    " + ColumnExp + " as " + ColumnName);
                if (rdbms.equals("ms")) {
                    out.println("  , case when cast(" + ColumnExp + " as varchar(MAX)) = '' then 1 else 0 end as is_empty");                    
                } else if (rdbms.equals("pg")) {
                    out.println("  , case when cast(" + ColumnExp + " as text) = '' then 1 else 0 end as is_empty");
                }
                if (TableName.equalsIgnoreCase("emr_encounter") && ColumnName.equalsIgnoreCase("pregnant")) {
                    out.println("  from (");
                    out.println("    select ts.name as event_name");
                    out.println("    , case ");
                    out.println("      when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'");
                    out.println("      else 'n'");
                    out.println("      end as pregnant");
                    out.println("    from emr_encounter enc");
                    out.println("    left outer join hef_timespan ts");
                    out.println("    on ts.patient_id = enc.patient_id");               
                } else {
                    out.println("  from " + TableName);
                }
                out.println("  where 1 = 1"); // dummy to initiate produce a valid where clause if no further predicates.
                
                // Apply Vitals Exclusions. "-999" means no limit on vitals values
                if (!IncludedValueMin.equals("-999") && !IncludedValueMax.equals("-999")){
                    out.print("  and " + ColumnExp + " between " + IncludedValueMin + " and " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion lower and upper limits");                    
                } else if (!IncludedValueMin.equals("-999") && IncludedValueMax.equals("-999")) {
                    out.print("  and " + ColumnExp + " >= " + IncludedValueMin);
                    out.println("  -- Apply Vitals Exclusion lower limit");                    
                } else if (IncludedValueMin.equals("-999") && !IncludedValueMax.equals("-999")) {
                    out.print("  and " + ColumnExp + " <= " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion upper limit");
                }
                
                // Apply date exclusion
                if (TableName.equalsIgnoreCase("emr_encounter")) {
                    if (rdbms.equals("ms")) {
                        out.println("  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()");
                    } else if (rdbms.equals("pg")) {
                        out.println("  and date between to_date('2015-01-01','YYYY-MM-DD') and current_date");
                    }
                }
                
                // Exclude dx_code_id = 'icd9:799.9' (Task v0.17:2)
                if (TableName.equalsIgnoreCase("emr_encounter_dx_codes") && ColumnName.equalsIgnoreCase("dx_code_id")) {
                    out.println("  and lower(dx_code_id) <> 'icd9:799.9'");
                }
                
                out.println(") p");
                if (TableName.equalsIgnoreCase("emr_encounter") && ColumnName.equalsIgnoreCase("pregnant")) {
                    out.println(") q");
                }
                out.println(";");
               
                // Collect any trailing fields on the line
                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                    if (!junk.isEmpty()) {
                    System.out.println("junk: " + junk);
                    }
                }
            
            }  // while (in.hasNextLine())
            
            // Finished the loop. Write out order by statement for the union. Clean up.
            out.println();
            out.println("select * from cii_qa.summary");
            out.println("order by esp_table, esp_column;");
            out.println("/* ... end Summary Test (ungrouped) */");
            out.flush();
            out.close();
            System.out.println("Finishing SummaryTest");
            
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
    public static void main(String[] args) {
        SummaryTest.summaryTest();
    }

}

