package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;
import java.time.*;

/**
 * Generator for Count by Date Bin queries.  For a column containing dates or timestamps, creates SQL to break 
 * the range between minimum and maximum date into bins of size 6 months, starting in 1/1/2017, 
 * and count the number of records in each bin. 
 * Bins are:
 *   17H1: 2017-01-01 through 2017-06-30
 *   17H2: 2017-07-01 through 2017-12-31
 *    ...
 *   21H1: 2021-01-01 through 2021-06-30
 *   
 *   21H2 will be added when this program is run after 2021-06-30.
 *   
 * @author dkacher
 *
 */
public class CountByDateBin {
    
    /**
     * countByDateBinTest - generates SQL for Count by Date Bin test.
     * Expects input file, count_by_date_bin_in.txt, to have four values for each test, separated by whitespace:
     *   - ESP_Column, in the form table.column
     *   - Data_type of the date column, either "date" or "timestamp"
     *   - YearMin - earliest year on which to report
     *   - YearMax - latest year on which to report
     */

    public static void countByDateBin () {
        System.out.println("Starting countByDateBin");

        
        try {
            Scanner in = new Scanner(Path.of("count_by_date_bin_in.txt"));
            
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            PrintWriter out = new PrintWriter("count_by_date_bin." + rdbms + ".sql", StandardCharsets.UTF_8);
            
            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + ")");
            out.println(" * Counts by Date bin (ungrouped)...");
            out.println("*/"); 
            out.println("");
            
            // Drop and create result table 
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.count_by_date_bin', 'U') is not null drop table cii_qa.count_by_date_bin;");
                out.println("create table cii_qa.count_by_date_bin ");
                out.println("(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)");
                out.println(", bin varchar(MAX), count integer);");                
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.count_by_date_bin;");
                out.println("create table cii_qa.count_by_date_bin");
                out.println("(test_name text, esp_table text, esp_column text");
                out.println(", bin text, count integer);");
            }
            

            
            // Loop through the input, generating SQL for each input line
            while (in.hasNextLine()) {
                // ESP_Column is table.col.  Read it, and split it into components TableName and ColumnName
                String ESP_Column = in.next();
                if(ESP_Column.startsWith("#")) {
                    continue; // Column has been commented out. Skip it
                }
                String TableName = ESP_Column.substring(0,ESP_Column.indexOf("."));
                String ColumnName = ESP_Column.substring(ESP_Column.indexOf(".")+1);
                
                // Get the data type of the column
                String date_data_type = in.next();
                System.out.println("ESP_Column: " + ESP_Column + "; TableName: "+TableName+"; ColumnName: "+ColumnName+"; data_type: " + date_data_type);
                
                // Report to log, and skip, if date_data_type hasn't got an expected value
                if (!((date_data_type.equals("date") || 
                    date_data_type.equals("timestamp")))) {
                    System.out.println("ERROR: Unknown date data type: " + date_data_type + " for " + ESP_Column + ". Skipping."); 
                    continue;
                }

                out.println("");
                out.println("");
                out.println("/* Column: " + ESP_Column + "*/");
                                
                out.println("insert into cii_qa.count_by_date_bin (");
                out.println("  test_name, esp_table, esp_column, bin, count)");
                out.println("select");
                out.println("  'count by date bin' as test_name, '" + TableName + "' as esp_table, '" + ColumnName + "' as esp_column");
                out.println(", bin, count(*) as count from (");
                out.println("select");
                out.println("  case");
                out.println("  when " + ColumnName + " is null then 'NULL'");
                if (rdbms.equals("ms")) {
                    out.println("  when " + ColumnName + " < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'");
                    out.println("  when " + ColumnName + " between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'");
                    out.println("  when " + ColumnName + " > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'");
                } else if (rdbms.equals("pg")) {
                    out.println("  when " + ColumnName + " < to_date('2017-01-01','yyyy-mm-dd') then 'b00_lt_2017H1'");
                    out.println("  when " + ColumnName + " between to_date('2017-01-01','yyyy-mm-dd') and to_date('2017-06-30','yyyy-mm-dd') then 'b01_2017H1'");
                    out.println("  when " + ColumnName + " between to_date('2017-07-01','yyyy-mm-dd') and to_date('2017-12-31','yyyy-mm-dd') then 'b02_2017H2'");
                    out.println("  when " + ColumnName + " between to_date('2018-01-01','yyyy-mm-dd') and to_date('2018-06-30','yyyy-mm-dd') then 'b03_2018H1'");
                    out.println("  when " + ColumnName + " between to_date('2018-07-01','yyyy-mm-dd') and to_date('2018-12-31','yyyy-mm-dd') then 'b04_2018H2'");
                    out.println("  when " + ColumnName + " between to_date('2019-01-01','yyyy-mm-dd') and to_date('2019-06-30','yyyy-mm-dd') then 'b05_2019H1'");
                    out.println("  when " + ColumnName + " between to_date('2019-07-01','yyyy-mm-dd') and to_date('2019-12-31','yyyy-mm-dd') then 'b06_2019H2'");
                    out.println("  when " + ColumnName + " between to_date('2020-01-01','yyyy-mm-dd') and to_date('2020-06-30','yyyy-mm-dd') then 'b07_2020H1'");
                    out.println("  when " + ColumnName + " between to_date('2020-07-01','yyyy-mm-dd') and to_date('2020-12-31','yyyy-mm-dd') then 'b08_2020H2'");
                    out.println("  when " + ColumnName + " between to_date('2021-01-01','yyyy-mm-dd') and to_date('2021-06-30','yyyy-mm-dd') then 'b09_2021H1'");
                    out.println("  when " + ColumnName + " between to_date('2021-07-01','yyyy-mm-dd') and to_date('2021-12-31','yyyy-mm-dd') then 'b10_2021H2'");
                    out.println("  when " + ColumnName + " > to_date('2021-12-31','yyyy-mm-dd') then 'b11_gt_2021H2'");
                }

                out.println("  else 'OTHER'");
                out.println("  end as bin");
                out.println("from " + TableName + ") a");
                out.println("group by bin");
                out.println(";");
                out.println("");
                
                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                    if (!junk.isEmpty()) {
                        System.out.println("junk: " + junk);
                    }
                }
            
            }  // while (in.hasNextLine())

            // Finished the loop. Clean up.
            out.println("");
            
            // Inspect the results
            out.println("/* Inspect the results */");
            out.println("select * from cii_qa.count_by_date_bin");
            out.println("order by esp_table, esp_column, bin");
            out.println(";");
            
            out.println("/* ...end of Count by Date Bin (ungrouped) */");            
            out.flush();
            out.close();    
            
            System.out.println("Finishing countByDateBinTest");

        } catch (IOException e) {
            e.printStackTrace();
        }          
    }


    public static void main(String[] args) {
        countByDateBin ();

    }

}
