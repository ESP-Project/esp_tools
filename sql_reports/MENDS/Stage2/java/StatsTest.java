package esp_mends_stage2_scripts;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.Scanner;

public class StatsTest {
    /**
     * statsTest - generates SQL for Stats test, ms and pg).
     * Expects input file, stats_in.txt, to have two lines for each test:
     *   ESP_Column, in the form table.column
     *   WhiskerMultiplier: multiply Inter-quartile Range by this to determine whisker size
     *   
     *   NOTE (2021-02-16) For data from emr_encounter, added a constraint that encounter_date >= 2015-01-01
     */
    public static void statsTest () {

        System.out.println("Starting Stats Test");
        try {
            Scanner in = new Scanner(Path.of("stats_in.txt"));  
            
            // Determine which rdbms to write a script for
            String rdbms;
            ParamReader pr = new ParamReader();
            rdbms = pr.getRdbms();
            System.out.println("rdbms: " + rdbms);
            
            PrintWriter out = new PrintWriter("stats." + rdbms + ".sql", StandardCharsets.UTF_8);
            
            //Header comments
            out.println("");
            out.println("");
            out.println("/* MENDS Stage 2 Tests (" + rdbms + " gen)");
            out.println(" * Stats tests (ungrouped)...");
            out.println("*/"); 
            out.println("");
            
            // Drop and create result table 
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.stats', 'U') is not null drop table cii_qa.stats;");
                out.println("create table cii_qa.stats (");
                out.println("test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)");
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.stats;");
                out.println("create table cii_qa.stats (");
                out.println("test_name text, esp_table text, esp_column text");
            }
            out.println(", whisker_multiplier float");
            out.println(", min float, lower_fence float, p10 float, p25 float, median float");
            out.println(", p75 float, p90 float, upper_fence float, max float");
            out.println(", mean float, stddev float, iqr float");
            out.println(", n_lower_outlier integer, n_q1 integer, n_iqr integer, n_q4 integer");
            out.println(", n_upper_outlier integer, n_undefined integer, n_defined integer, n_total integer");
            out.println(");");
            
            // Loop through the input, generating SQL for each line
            while (in.hasNext()) {
                /* TabColSpec is the information about the column.  It is in the form
                 * table.column,
                 * where 
                 *   table is the table name
                 *   column is the column name
                 * Use these to populate TableName, ColumnName, ColumnExp
                 */
                String TabColSpec = in.next();               
                String TableName = TabColSpec.substring(0,TabColSpec.indexOf("."));
                String ColumnString = TabColSpec.substring(TabColSpec.indexOf(".")+1);
                String ColumnName = "";
                String ColumnExp = "";
                String ColumnExpNoAs = "";
                
                ColumnName = ColumnString;
                ColumnExp = ColumnString;
                ColumnExpNoAs = ColumnExp;
                
                if (TableName.equalsIgnoreCase("emr_patient")) {
                    if (rdbms.equals("ms")) {
                        ColumnExp = "datediff(year, date_of_birth, sysdatetime()) as age_yrs_derived";
                    }
                    else if (rdbms.equals("pg")) {
                        // ColumnExp = "(cast(current_date as date) - cast(date_of_birth as date)) as age_yrs_derived";                        
                        ColumnExp = "(trunc((cast(current_date as date) - cast(date_of_birth as date)))/365.25) as age_yrs_derived";                        
                    }
                    ColumnExpNoAs = ColumnExp.substring(0,ColumnExp.indexOf(") as")+1);
                }
                
                System.out.println("TableName: " + TableName + ", ColumnName: " + ColumnName + ", ColumnExp: " + ColumnExp);
                if (!ColumnExpNoAs.isEmpty()) {
                    System.out.println("ColumnExpNoAs: " + ColumnExpNoAs);
                }
                String WhiskerMultiplier = in.next();
                System.out.println("WhiskerMultiplier: " + WhiskerMultiplier);
                
                String IncludedValueMin = in.next();
                String IncludedValueMax = in.next();
                
                System.out.println("TableName: " + TableName + ", ColumnName: " + ColumnName + ", ColumnExp: " + ColumnExp);
                System.out.println("Min and Max column values: " + IncludedValueMin +", " + IncludedValueMax);


               
                out.println("/* Stats (wide version, with cutpoints and counts */");
                out.println("");
                out.println("/* " + TableName + "." + ColumnName + " */");
                out.println("");
                out.println("/* Determine cutpoints of ranges */");
                if (rdbms.equals("ms")) {
                    out.println("if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;");
                } else if (rdbms.equals("pg")) {
                    out.println("drop table if exists cii_qa.stats_tmp_cutpoints;");                    
                }
                out.println("create table cii_qa.stats_tmp_cutpoints (");
                out.println("whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float");
                out.println(", mean float, stddev float, iqr float, lower_fence float, upper_fence float);");
                out.println("");
                out.println("insert into cii_qa.stats_tmp_cutpoints (");
                out.println("  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev");
                out.println(", iqr, lower_fence, upper_fence");
                out.println(")");
                out.println("select");
                out.println(WhiskerMultiplier);
                out.println(", max(min) as min");
                out.println(", max(p10) as p10");
                out.println(", max(p25) as p25");
                out.println(", max(median) as median");
                out.println(", max(p75) as p75");
                out.println(", max(p90) as p90");
                out.println(", max(max) as max");
                out.println(", max(mean) as mean");
                out.println(", max(stddev) as stddev");
                if (rdbms.equals("ms")) { 
                    // These probably don't work for SSQL either, but check
                    out.println(", iqr = max(p75) - max(p25)");
                    out.println(", lower_fence = max(p25) - " + WhiskerMultiplier + "*(max(p75) - max(p25))");
                    out.println(", upper_fence = max(p75) + " + WhiskerMultiplier + "*(max(p75) - max(p25))");
                } else if (rdbms.equals("pg")) {
                    out.println(", (max(p75) - max(p25)) as iqr");
                    out.println(", (max(p25) - " + WhiskerMultiplier + "*(max(p75) - max(p25))) as lower_fence");
                    out.println(", (max(p75) + " + WhiskerMultiplier + "*(max(p75) - max(p25))) as upper_fence");                   
                }
                out.println("from (");
                out.println("   select");
                if (rdbms.equals("ms")) {
                    out.println("     min(" + ColumnName + ") over () as min");
                    out.println("   , percentile_disc(0.1) within group (order by " + ColumnName + ") over ( ) as p10");
                    out.println("   , percentile_disc(0.25) within group (order by " + ColumnName + ") over ( ) as p25");
                    out.println("   , percentile_disc(0.5) within group (order by " + ColumnName + ") over ( ) as median");
                    out.println("   , percentile_disc(0.75) within group (order by " + ColumnName + ") over ( ) as p75");
                    out.println("   , percentile_disc(0.9) within group (order by " + ColumnName + ") over ( ) as p90");
                    out.println("   , max(" + ColumnName + ") over ( ) as max");
                    out.println("   , avg(" + ColumnName + ") over ( ) as mean");
                    out.println("   , stdev(" + ColumnName + ") over ( ) as stddev");                
                } else if (rdbms.equals("pg")) {
                    out.println("     min(" + ColumnName + ") as min");
                    out.println("   , percentile_disc(0.1) within group (order by " + ColumnName + ") as p10");
                    out.println("   , percentile_disc(0.25) within group (order by " + ColumnName + ") as p25");
                    out.println("   , percentile_disc(0.5) within group (order by " + ColumnName + ") as median");
                    out.println("   , percentile_disc(0.75) within group (order by " + ColumnName + ") as p75");
                    out.println("   , percentile_disc(0.9) within group (order by " + ColumnName + ") as p90");
                    out.println("   , max(" + ColumnName + ") as max");
                    out.println("   , avg(" + ColumnName + ") as mean");
                    out.println("   , stddev(" + ColumnName + ") as stddev");                    
                }
                out.println("   from (select  " + ColumnExp + " from " + TableName);
                out.println("where 1 = 1"); // Dummy to initialize where clause, so that other optional clauses can follow.
                if (TableName.equalsIgnoreCase("emr_encounter")) {
                    if (rdbms.equals("ms")) {
                        out.println("and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()"); 
                    } else if (rdbms.equals("pg")) {
                        out.println("and date between to_date('2015-01-01', 'YYYY-MM-DD') and current_date");                         
                    }
                }
                
                // Apply Vitals Exclusions. "-999" means no limit on vitals values
                if (!IncludedValueMin.equals("-999") && !IncludedValueMax.equals("-999")){
                    out.print("  and " + ColumnExp + " between " + IncludedValueMin + " and " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion lower and upper limits");                    
                } else if (!IncludedValueMin.equals("-999") && IncludedValueMax.equals("-999")) {
                    out.print("  and " + ColumnExp + " >= " + IncludedValueMin);
                    out.println("  -- Apply Vitals Exclusion lower limit");                    
                } else if (IncludedValueMin.equals("-999") && !IncludedValueMax.equals("-999")) {
                    out.print("  and " + ColumnExp + " <= " + IncludedValueMax);
                    out.println("  -- Apply Vitals Exclusion upper limit");
                }

                out.println(") b");
                out.println(") c");
                out.println(";");
                out.println("");
                
                out.println("/* Calculate the count in each range, store results in tall table rc.  */");
                if (rdbms.equals("ms")) {
                    out.println("if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;");
                } else if (rdbms.equals("pg")) {
                    out.println("drop table if exists cii_qa.stats_tmp_rc;");                    
                }
                out.println("create table cii_qa.stats_tmp_rc");
                if (rdbms.equals("ms")) {
                    out.println("(id integer, range varchar(MAX), cnt integer);");
                } else if (rdbms.equals("pg")) {
                    out.println("(id integer, range text, cnt integer);");                    
                }
                out.println("");
                out.println("insert into cii_qa.stats_tmp_rc (");
                out.println("  id, range, cnt");
                out.println("  )");
                out.println("select '1' as id, range, count(*) cnt");
                out.println("from (");
                out.println("select " + ColumnExp + ", ts.*");
                out.println(", case");
                out.println("  when " + ColumnExpNoAs + " < lower_fence then 'a_lower_outlier'");
                out.println("  when " + ColumnExpNoAs + " >= lower_fence and " + ColumnExpNoAs + " < p25 then 'b_q1'");
                out.println("  when " + ColumnExpNoAs + " between p25 and p75 then 'c_iqr'");
                out.println("  when " + ColumnExpNoAs + " > p75 and " + ColumnExpNoAs + " <= upper_fence then 'd_q4'");
                out.println("  when " + ColumnExpNoAs + " > upper_fence then 'e_upper_outlier'");
                if (rdbms.equals("ms")) {
                    out.println("  else 'f_undefined'");
                }
                else if (rdbms.equals("pg")) {
                    out.println("  when " + ColumnExpNoAs + " is null then 'f_undefined'");
                }
                out.println("  end as range");
                out.println("from cii_qa.stats_tmp_cutpoints ts");
                out.println("cross join " + TableName + " raw");
                out.println(") a");
                out.println("group by range");
                out.println(";");
                out.println("");
                out.println("-- select * from cii_qa.stats_tmp_rc;");
                out.println("");
                
                out.println("/* Transpose range counts table,  writing tmp_stats_x_rc_trans */");
                if (rdbms.equals("ms")) {
                    // use pivot in SQL Server
                    out.println("if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;");
                    out.println("create table cii_qa.stats_tmp_rc_trans (");
                    out.println("range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer");
                    out.println(", n_q4 integer, n_upper_outlier integer, n_undefined integer);");
                    out.println("insert into cii_qa.stats_tmp_rc_trans (");
                    out.println("  range, n_lower_outlier, n_q1, n_iqr");
                    out.println(", n_q4, n_upper_outlier, n_undefined) ");
                    out.println("select 'cnt' as range, ");
                    out.println("a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr");
                    out.println(", d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined");
                    out.println("from");
                    out.println("(select cnt, range");
                    out.println(" from cii_qa.stats_tmp_rc) as SourceTable");
                    out.println(" pivot");
                    out.println("(");
                    out.println("avg(cnt)");
                    out.println("for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)");
                    out.println(") as PivotTable;");    
                } else if (rdbms.equals("pg")) {                 
                    // transpose cii_qa.stats_tmp_rc, to populate stats_tmp_rc_trans, doing joins by hand. Couldn't get crosstab to work.
                    out.println("drop table if exists cii_qa.stats_tmp_rc_trans;");
                    out.println("create table cii_qa.stats_tmp_rc_trans (");
                    out.println("n_lower_outlier integer, n_q1 integer, n_iqr integer");
                    out.println(", n_q4 integer, n_upper_outlier integer, n_undefined integer);");   
                    out.println("insert into cii_qa.stats_tmp_rc_trans (");
                    out.println("  n_lower_outlier, n_q1, n_iqr");
                    out.println(", n_q4, n_upper_outlier, n_undefined) ");

                    out.println("select distinct");
                    out.println("a.cnt as n_lower_outlier, b.cnt as n_q1, c.cnt as n_iqr, d.cnt as n_q4, e.cnt as n_upper_outlier, f.cnt as n_undefined");
                    out.println("from cii_qa.stats_tmp_rc a");
                    out.println("join cii_qa.stats_tmp_rc b on b.id = a.id");
                    out.println("join cii_qa.stats_tmp_rc c on c.id = a.id");
                    out.println("join cii_qa.stats_tmp_rc d on d.id = a.id");
                    out.println("join cii_qa.stats_tmp_rc e on e.id = a.id");
                    out.println("join cii_qa.stats_tmp_rc f on f.id = a.id");
                    out.println("where a.range like 'a%'");
                    out.println("and   b.range like 'b%'");
                    out.println("and   c.range like 'c%'");
                    out.println("and   d.range like 'd%'");
                    out.println("and   e.range like 'e%'");
                    out.println("and   f.range like 'f%'");
                    out.println(";");
                }
                out.println("--select * from cii_qa.stats_tmp_rc_trans;");
                out.println("");
                out.println("");
                
                out.println("/* Join range limits and count in ranges. Add resulting record to stats table. */");
                out.println("insert into cii_qa.stats (");
                out.println("  test_name, esp_table, esp_column");
                out.println(", whisker_multiplier, min, lower_fence, p10, p25, median");
                out.println(", p75, p90, upper_fence, max");
                out.println(", mean, stddev, iqr");
                out.println(", n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined");
                out.println(", n_defined, n_total");
                out.println(") ");
                out.println("select 'stats' as test_name, '" + TableName + "' as esp_table, '" + ColumnName + "' as esp_column");
                out.println(", whisker_multiplier");
                out.println(", min");
                out.println(", lower_fence");
                out.println(", p10, p25, median, p75, p90");
                out.println(", upper_fence, max");
                out.println(", mean, stddev, iqr");
                out.println(", n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined");
                out.println(", coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + ");
                out.println("  coalesce(n_upper_outlier,0) as n_defined");
                out.println(", coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + ");
                out.println("  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total");
                out.println("from cii_qa.stats_tmp_cutpoints      s");
                out.println("cross join cii_qa.stats_tmp_rc_trans t");
                out.println(";");
                out.println("");
                out.println("-- select * from cii_qa.stats_tmp_wide_results;");
                out.println("");
                out.println("");
                          
                // Collect any trailing fields on the line
                if (in.hasNextLine()) {
                    String junk = in.nextLine();
                    if (!junk.isEmpty()) {
                    System.out.println("junk: " + junk);
                    }
                }
            
            }  // while (in.hasNextLine())
            
            // Finished the loop. 
            
            // Clean up temporary tables
            out.println("/* drop temp tables */");
            if (rdbms.equals("ms")) {
                out.println("if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;");
                out.println("if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;");
                out.println("if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;");
            } else if (rdbms.equals("pg")) {
                out.println("drop table if exists cii_qa.stats_tmp_cutpoints;");
                out.println("drop table if exists cii_qa.stats_tmp_rc;");
                out.println("drop table if exists cii_qa.stats_tmp_rc_trans;");                    
            }  
            
            // Inspect the end result.
            out.println("select * from cii_qa.stats");
            out.println("order by test_name, esp_table, esp_column");
            out.println("/* ...end Stats tests (ungrouped) */");
            
            // Clean up
            out.flush();
            out.close();
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    System.out.println("Finishing StatsTest");

    }
    public static void main(String[] args) {
        StatsTest.statsTest();
    }

}   
