package esp_mends_stage2_scripts;
import java.time.*;


public class TestDate {

    public static void main(String[] args) {
        LocalDate todaysDate = LocalDate.now();
        System.out.println( "today : " + todaysDate );
        System.out.println("year: " + todaysDate.getYear());
        int endYear = todaysDate.getYear();
        int monthValue = todaysDate.getMonthValue();
        System.out.println("month: " + monthValue);
        // int endHalf = Math.floorMod(monthValue,6);
        int endHalf = (monthValue/2);
        for (int i=1; i<13; i++) {
            endHalf = ((i-1)/6) + 1;  // uses implicit truncation
            System.out.println("i: " + i + ", h: " + endHalf);
        }
        

    }

}
