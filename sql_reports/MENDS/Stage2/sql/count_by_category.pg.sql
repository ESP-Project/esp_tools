

/* MENDS Stage 2 Tests (pg)
 * Counts by Category (ungrouped)...
*/

drop table if exists cii_qa.count_by_category;
create table cii_qa.count_by_category 
(test_name text, esp_table text, esp_column text
, category text, count integer);
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'state' as ESP_Column
, case 
  when cast(state as text) like '--%' then '''' || cast(state as text)
  else cast(state as text)
  end as category, count(*) as count
from emr_patient
group by state
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'zip' as ESP_Column
, case 
  when cast(zip as text) like '--%' then '''' || cast(zip as text)
  else cast(zip as text)
  end as category, count(*) as count
from emr_patient
group by zip
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'race' as ESP_Column
, case 
  when cast(race as text) like '--%' then '''' || cast(race as text)
  else cast(race as text)
  end as category, count(*) as count
from emr_patient
group by race
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'ethnicity' as ESP_Column
, case 
  when cast(ethnicity as text) like '--%' then '''' || cast(ethnicity as text)
  else cast(ethnicity as text)
  end as category, count(*) as count
from emr_patient
group by ethnicity
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'gender' as ESP_Column
, case 
  when cast(gender as text) like '--%' then '''' || cast(gender as text)
  else cast(gender as text)
  end as category, count(*) as count
from emr_patient
group by gender
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_encounter' as ESP_Table, 'pregnant' as ESP_Column
, case 
  when cast(pregnant as text) like '--%' then '''' || cast(pregnant as text)
  else cast(pregnant as text)
  end as category, count(*) as count
from (
  select 
    pregnant as pregnant
  , case when cast(pregnant as text) = '' then 1 else 0 end as is_empty
  from (
    select ts.name as event_name
    , case 
      when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
      else 'n'
      end as pregnant
    from emr_encounter enc
    left outer join hef_timespan ts
    on ts.patient_id = enc.patient_id
  ) p
) q
group by pregnant
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'name' as ESP_Column
, case 
  when cast(name as text) like '--%' then '''' || cast(name as text)
  else cast(name as text)
  end as category, count(*) as count
from emr_prescription
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'quantity' as ESP_Column
, case 
  when cast(quantity as text) like '--%' then '''' || cast(quantity as text)
  else cast(quantity as text)
  end as category, count(*) as count
from emr_prescription
group by quantity
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'refills' as ESP_Column
, case 
  when cast(refills as text) like '--%' then '''' || cast(refills as text)
  else cast(refills as text)
  end as category, count(*) as count
from emr_prescription
group by refills
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'route' as ESP_Column
, case 
  when cast(route as text) like '--%' then '''' || cast(route as text)
  else cast(route as text)
  end as category, count(*) as count
from emr_prescription
group by route
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'dose' as ESP_Column
, case 
  when cast(dose as text) like '--%' then '''' || cast(dose as text)
  else cast(dose as text)
  end as category, count(*) as count
from emr_prescription
group by dose
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_labresult' as ESP_Table, 'native_code' as ESP_Column
, case 
  when cast(native_code as text) like '--%' then '''' || cast(native_code as text)
  else cast(native_code as text)
  end as category, count(*) as count
from emr_labresult
group by native_code
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_socialhistory' as ESP_Table, 'tobacco_use (mapped)' as ESP_Column
, category, count(*) as count
from (
select coalesce(mapped_value, a.tobacco_use) as category -- v0.17:1
from emr_socialhistory a
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use
) p
group by category
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_immunization' as ESP_Table, 'name' as ESP_Column
, case 
  when cast(name as text) like '--%' then '''' || cast(name as text)
  else cast(name as text)
  end as category, count(*) as count
from emr_immunization
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'hef_event' as ESP_Table, 'name' as ESP_Column
, case 
  when cast(name as text) like '--%' then '''' || cast(name as text)
  else cast(name as text)
  end as category, count(*) as count
from hef_event
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'hef_timespan' as ESP_Table, 'name' as ESP_Column
, case 
  when cast(name as text) like '--%' then '''' || cast(name as text)
  else cast(name as text)
  end as category, count(*) as count
from hef_timespan
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'nodis_case' as ESP_Table, 'condition' as ESP_Column
, case 
  when cast(condition as text) like '--%' then '''' || cast(condition as text)
  else cast(condition as text)
  end as category, count(*) as count
from nodis_case
group by condition
;

select * from cii_qa.count_by_category
order by esp_table, esp_column, category
;
/* ...end of Counts by Category (ungrouped) */
