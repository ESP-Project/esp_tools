SELECT 
    'Albuterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('albuterol', 'accuneb', 'ventolin')
    AND LOWER(name) NOT LIKE '%levalbuterol%'
    AND LOWER(name) NOT LIKE '%ipratropium%'
    AND LOWER(name) NOT LIKE '%xopenex%'
UNION ALL
SELECT 
    'Levalbuterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('levalbuterol', 'xopenex')
UNION ALL
SELECT 
    'Pirbuterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('pirbuterol', 'maxair')
UNION ALL
SELECT 
    'Arformoterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('arformoterol', 'brovana')
SELECT 
    'Formoterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('formoterol', 'foradil', 'perforomist', 'oxeze')
    AND LOWER(name) NOT LIKE '%aformoterol%'
    AND LOWER(name) NOT LIKE '%mometasone%'
    AND LOWER(name) NOT LIKE '%budesonide%'
    AND LOWER(name) NOT LIKE '%brovana%'
UNION ALL
SELECT 
    'Indacaterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('indacaterol', 'arcapta')
UNION ALL
SELECT 
    'Salmeterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('salmeterol', 'serevent')
    AND LOWER(name) NOT LIKE '%fluticasone%'
UNION ALL
SELECT 
    'Beclomethasone' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) IN ('beclomethasone', 'qvar')
UNION ALL
SELECT 
    'Budesonide INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%budesonide%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '% neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%formoterol%'
    AND LOWER(name) NOT LIKE '%pulmicort%'
UNION ALL
SELECT 
    'Ciclesonide INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%ciclesonide%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '%neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%alvesco%'
UNION ALL
SELECT 
    'Flunisolide INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%flunisolide%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '%neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%aerobid%' AND LOWER(name) NOT LIKE '%aerospan%'
UNION ALL
SELECT 
    'Fluticasone INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%fluticasone%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '%neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%salmeterol%' AND LOWER(name) NOT LIKE '%flovent%' AND LOWER(name) NOT LIKE '%vilanterol%'
UNION ALL
SELECT 
    'Mometasone INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%mometasone%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '%neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%formoterol%' AND LOWER(name) NOT LIKE '%dulera%' AND LOWER(name) NOT LIKE '%zenhale%' AND LOWER(name) NOT LIKE '%asmanex%'
UNION ALL
SELECT 
    'Montelukast' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%montelukast%'
UNION ALL
SELECT 
    'Zafirlukast' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%zafirlukast%'
UNION ALL
SELECT 
    'Zileuton' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%zileuton%'
UNION ALL
SELECT 
    'Ipratropium' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%ipratropium%'
    AND LOWER(name) NOT LIKE '%albuterol%'
UNION ALL
SELECT 
    'Tiotropium' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%tiotropium%'
UNION ALL
SELECT 
    'Cromolyn INH' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%cromolyn%'
    AND (LOWER(name) LIKE '%inh%' OR LOWER(name) LIKE '%neb%' OR LOWER(name) LIKE '%aer%')
    AND LOWER(name) NOT LIKE '%intal%' AND LOWER(name) NOT LIKE '%gastrocrom%' AND LOWER(name) NOT LIKE '%nalcrom%'
UNION ALL
SELECT 
    'Omalizumab' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%omalizumab%'
UNION ALL
SELECT 
    'Fluticasone + Salmeterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    (LOWER(name) LIKE '%fluticasone%' AND LOWER(name) LIKE '%salmeterol%')
    AND LOWER(name) NOT LIKE '%advair%'
UNION ALL
SELECT 
    'Advair' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%advair%'
UNION ALL
SELECT 
    'Fluticasone + Vilanterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    (LOWER(name) LIKE '%fluticasone%' AND LOWER(name) LIKE '%vilanterol%')
    AND LOWER(name) NOT LIKE '%breo%'
UNION ALL
SELECT 
    'Breo' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%breo%'
UNION ALL
SELECT 
    'Albuterol + Ipratropium' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    (LOWER(name) LIKE '%albuterol%' AND LOWER(name) LIKE '%ipratropium%')
    AND LOWER(name) NOT LIKE '%combivent%' AND LOWER(name) NOT LIKE '%duoneb%'
UNION ALL
SELECT 
    'Combivent' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%combivent%'
UNION ALL
SELECT 
    'Mometasone + Formoterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    (LOWER(name) LIKE '%mometasone%' AND LOWER(name) LIKE '%formoterol%')
    AND LOWER(name) NOT LIKE '%dulera%' AND LOWER(name) NOT LIKE '%zenhale%'
UNION ALL
SELECT 
    'Dulera' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%dulera%'
UNION ALL
SELECT 
    'Budesonide + Formoterol' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    (LOWER(name) LIKE '%budesonide%' AND LOWER(name) LIKE '%formoterol%')
    AND LOWER(name) NOT LIKE '%symbicort%'
UNION ALL
SELECT 
    'Symbicort' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%symbicort%'
UNION ALL
SELECT 
    'Benralizumab' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%benralizumab%'
UNION ALL
SELECT 
    'Mepolizumab' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%mepolizumab%'
UNION ALL
SELECT 
    'Reslizumab' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%reslizumab%'
UNION ALL
SELECT 
    'Theophylline' AS DrugName,
    COUNT(*) AS Count
FROM dbo.emr_prescription
WHERE 
    LOWER(name) LIKE '%theophylline%'
ORDER BY DrugName;
