/* MENDS Stage 2 Tests (T-SQL)
 * Counts by Category (ungrouped)...
*/

IF OBJECT_ID('dbo.cii_qa.count_by_asthma_category') IS NOT NULL
    DROP TABLE dbo.cii_qa.count_by_asthma_category;

CREATE TABLE dbo.cii_qa.count_by_asthma_category 
(
    test_name NVARCHAR(MAX), 
    esp_table NVARCHAR(MAX), 
    esp_column NVARCHAR(MAX),
    category NVARCHAR(MAX), 
    count INT
);

INSERT INTO dbo.cii_qa.count_by_asthma_category
(test_name, esp_table, esp_column, category, count) 
SELECT
'Count by category' as test_name
, 'hef_event' as ESP_Table, 'name' as ESP_Column
, CASE 
  WHEN CAST(h.name AS NVARCHAR(MAX)) LIKE '--%' THEN '''' + CAST(h.name AS NVARCHAR(MAX))
  ELSE CAST(h.name AS NVARCHAR(MAX))
  END as category, COUNT(*) as count
FROM dbo.hef_event h
WHERE h.name IN ('dx:asthma','rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol',
            'rx:formoterol','rx:indacaterol','rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh',
            'rx:alvesco','rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
            'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh','rx:intal','rx:omalizumab',
            'rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab','rx:theophylline','rx:fluticasone-salmeterol:generic',
             'rx:fluticasone-vilanterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formoterol:generic',
             'rx:budesonide-formoterol:generic','rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade', 
             'rx:albuterol-ipratropium:trade','rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade')
GROUP BY h.name;

SELECT * FROM dbo.cii_qa.count_by_asthma_category;
