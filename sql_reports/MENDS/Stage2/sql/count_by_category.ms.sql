

/* MENDS Stage 2 Tests (ms)
 * Counts by Category (ungrouped)...
*/

if object_id('cii_qa.count_by_category', 'U') is not null drop table cii_qa.count_by_category;
create table cii_qa.count_by_category 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)
, category varchar(MAX), count integer);
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'state' as ESP_Column
, cast(state as varchar(MAX)) as category, count(*) as count
from emr_patient
group by state
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'zip' as ESP_Column
, cast(zip as varchar(MAX)) as category, count(*) as count
from emr_patient
group by zip
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'race' as ESP_Column
, cast(race as varchar(MAX)) as category, count(*) as count
from emr_patient
group by race
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'ethnicity' as ESP_Column
, cast(ethnicity as varchar(MAX)) as category, count(*) as count
from emr_patient
group by ethnicity
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_patient' as ESP_Table, 'gender' as ESP_Column
, cast(gender as varchar(MAX)) as category, count(*) as count
from emr_patient
group by gender
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_encounter' as ESP_Table, 'pregnant' as ESP_Column
, cast(pregnant as varchar(MAX)) as category, count(*) as count
from (
  select 
    pregnant as pregnant
  , case when cast(pregnant as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from (
    select ts.name as event_name
    , case 
      when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
      else 'n'
      end as pregnant
    from emr_encounter enc
    left outer join hef_timespan ts
    on ts.patient_id = enc.patient_id
  ) p
) q
group by pregnant
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'name' as ESP_Column
, cast(name as varchar(MAX)) as category, count(*) as count
from emr_prescription
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'quantity' as ESP_Column
, cast(quantity as varchar(MAX)) as category, count(*) as count
from emr_prescription
group by quantity
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'refills' as ESP_Column
, cast(refills as varchar(MAX)) as category, count(*) as count
from emr_prescription
group by refills
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'route' as ESP_Column
, cast(route as varchar(MAX)) as category, count(*) as count
from emr_prescription
group by route
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_prescription' as ESP_Table, 'dose' as ESP_Column
, cast(dose as varchar(MAX)) as category, count(*) as count
from emr_prescription
group by dose
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_labresult' as ESP_Table, 'native_code' as ESP_Column
, cast(native_code as varchar(MAX)) as category, count(*) as count
from emr_labresult
group by native_code
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_socialhistory' as ESP_Table, 'tobacco_use (mapped)' as ESP_Column
, category, count(*) as count
from (
select coalesce(mapped_value, a.tobacco_use) as category -- v0.17:1
from emr_socialhistory a
left outer join (
  select src_value, mapped_value from gen_pop_tools.rs_conf_mapping where src_field = 'tobacco_use'
  ) b on b.src_value = a.tobacco_use
) p
group by category
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'emr_immunization' as ESP_Table, 'name' as ESP_Column
, cast(name as varchar(MAX)) as category, count(*) as count
from emr_immunization
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'hef_event' as ESP_Table, 'name' as ESP_Column
, cast(name as varchar(MAX)) as category, count(*) as count
from hef_event
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'hef_timespan' as ESP_Table, 'name' as ESP_Column
, cast(name as varchar(MAX)) as category, count(*) as count
from hef_timespan
group by name
;
insert into cii_qa.count_by_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'nodis_case' as ESP_Table, 'condition' as ESP_Column
, cast(condition as varchar(MAX)) as category, count(*) as count
from nodis_case
group by condition
;

select * from cii_qa.count_by_category
order by esp_table, esp_column, category
;
/* ...end of Counts by Category (ungrouped) */
