-- Drop table if it exists
IF OBJECT_ID('cii_qa.xtab_age_group_by_asthma_status', 'U') IS NOT NULL
    DROP TABLE cii_qa.xtab_age_group_by_asthma_status;

-- Create table
CREATE TABLE cii_qa.xtab_age_group_by_asthma_status
(
    age_group NVARCHAR(MAX),
    asthma_criteria1_count INT,
    asthma_criteria2_count INT,
    asthma_one_of_each_count INT,
    no_asthma_diagnoses_count INT
);

-- Insert data into the table
;WITH T0 AS (
    SELECT DISTINCT T1.patient_id 
    FROM hef_event AS T1
    JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
    WHERE T1.name = 'dx:asthma' 
    AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', ...)
    AND NOT EXISTS (
        SELECT * 
        FROM nodis_case c 
        WHERE c.patient_id = T1.patient_id AND c.condition = 'asthma'
    )
),
age_calc AS (
    SELECT
        p.id,
        DATEDIFF(YEAR, p.date_of_birth, GETDATE()) AS age
    FROM emr_patient p
),
asthma AS (
    -- Definition for asthma CTE
),
asthma2 AS (
    -- Definition for asthma2 CTE
),
asthma3 AS (
    SELECT
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END AS age_group,
        COUNT(*) AS counts
    FROM age_calc ac
    JOIN T0 ON ac.id = T0.patient_id
    GROUP BY 
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END
),
no_asthma AS (
    -- Definition for no_asthma CTE
)
INSERT INTO cii_qa.xtab_age_group_by_asthma_status (age_group, asthma_criteria1_count, asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
SELECT 
    COALESCE(asthma.age_group, asthma2.age_group, asthma3.age_group, no_asthma.age_group) AS age_group, 
    ISNULL(asthma.counts, 0) AS asthma_criteria1_count,
    ISNULL(asthma2.counts, 0) AS asthma_criteria2_count,
    ISNULL(asthma3.counts, 0) AS asthma_one_of_each_count,
    ISNULL(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM asthma
FULL OUTER JOIN asthma2 ON asthma.age_group = asthma2.age_group
FULL OUTER JOIN asthma3 ON COALESCE(asthma.age_group, asthma2.age_group) = asthma3.age_group
FULL OUTER JOIN no_asthma ON COALESCE(asthma.age_group, asthma2.age_group, asthma3.age_group) = no_asthma.age_group;

-- Retrieve the data
SELECT * FROM cii_qa.xtab_age_group_by_asthma_status ORDER BY age_group;
