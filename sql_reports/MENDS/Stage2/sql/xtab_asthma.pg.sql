-- Race by dx (new) with no asthma as column
-- Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_race_by_asthma_status;
CREATE TABLE cii_qa.xtab_race_by_asthma_status
(
    race TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
	asthma_one_of_each_count INTEGER,
	no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_race_by_asthma_status (race, asthma_criteria1_count, 
asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
	SELECT DISTINCT t1.patient_id FROM hef_event AS T1
	JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
	WHERE T1.name = 'dx:asthma' 
	 AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
	 AND NOT EXISTS (SELECT NULL FROM NODIS_CASE c WHERE c.patient_id = t1.patient_id AND c.condition = 'asthma') 	
)
SELECT 
    COALESCE(asthma.race, no_asthma.race) AS race, 
    COALESCE(asthma.counts, 0) AS asthma_criteria1_count,
	COALESCE(asthma2.counts, 0) AS asthma_criteria2_count,
	COALESCE(asthma3.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    (
        SELECT 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END AS race,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END
    ) asthma
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END AS race,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #2%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END
	) asthma2
ON asthma.race = asthma2.race
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END AS race,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            t0 ON p.id = t0.patient_id
        GROUP BY 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END
	) asthma3
ON asthma.race = asthma3.race
FULL OUTER JOIN 
    (
        SELECT 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END AS race,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        WHERE 
            NOT EXISTS (
                SELECT 1
                FROM public.nodis_case nc 
                WHERE p.id = nc.patient_id and nc.condition = 'asthma'
            )
		AND NOT EXISTS (SELECT NULL FROM T0 WHERE T0.patient_id = p.id)
        GROUP BY 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END
    ) no_asthma
ON asthma.race = no_asthma.race;

-- Retrieve the data
SELECT * FROM cii_qa.xtab_race_by_asthma_status ORDER BY race;


--new2
-- gender rx (new) Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_race_by_medication;
CREATE TABLE cii_qa.xtab_race_by_medication
(
    race TEXT,
    medication TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
    asthma_one_of_each_count INTEGER,
    no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_race_by_medication (race, medication, asthma_criteria1_count, asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
    SELECT DISTINCT t1.patient_id FROM hef_event AS T1
	JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
	WHERE T1.name = 'dx:asthma' 
	 AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
	 AND NOT EXISTS (SELECT NULL FROM NODIS_CASE c WHERE c.patient_id = t1.patient_id AND c.condition = 'asthma') 	
),
medications as (
    SELECT DISTINCT name AS medication FROM public.hef_event WHERE name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
),
races as (
    SELECT DISTINCT CASE WHEN race IS NULL OR race = '' THEN 'UNK' ELSE race END AS race FROM public.emr_patient
)
SELECT 
    races.race,
    medications.medication,
    COALESCE(criteria1.counts, 0) AS asthma_criteria1_count,
    COALESCE(criteria2.counts, 0) AS asthma_criteria2_count,
    COALESCE(one_of_each.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    races
CROSS JOIN 
    medications
LEFT JOIN 
    -- Revised subquery for asthma criteria 1
    (SELECT 
            CASE WHEN p.race IS NULL OR p.race = '' THEN 'UNK' ELSE p.race END AS race,
            h.name AS medication,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        JOIN
            public.hef_event h ON p.id = h.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
            AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
        GROUP BY 
            race, h.name
    ) criteria1
ON races.race = criteria1.race AND medications.medication = criteria1.medication
-- Repeat similar joins for criteria2, one_of_each, and no_asthma
LEFT JOIN 
    -- Revised subquery for asthma criteria 2
    (SELECT 
            CASE WHEN p.race IS NULL OR p.race = '' THEN 'UNK' ELSE p.race END AS race,
            h.name AS medication,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        JOIN
            public.hef_event h ON p.id = h.patient_id
        WHERE 
            nc.criteria like 'Criteria #2%' and nc.condition = 'asthma'
            AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
        GROUP BY 
            race, h.name
    ) criteria2
ON races.race = criteria2.race AND medications.medication = criteria2.medication
LEFT JOIN
(
	
	SELECT 
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END AS race,
			h.name as medication,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            t0 ON p.id = t0.patient_id
		JOIN
			hef_event h ON h.patient_id = p.id
        GROUP BY 
			h.name,
            CASE 
                WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
                ELSE p.race 
            END
    ) one_of_each
ON races.race = one_of_each.race AND medications.medication = one_of_each.medication
LEFT JOIN 
    (
    SELECT 
        CASE 
            WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
            ELSE p.race 
        END AS race,
        COUNT(*) AS counts
    FROM 
        public.emr_patient p
    WHERE 
        NOT EXISTS (
            SELECT 1
            FROM public.nodis_case nc 
            WHERE p.id = nc.patient_id AND nc.condition = 'asthma'
        )
        AND NOT EXISTS (
            SELECT 1
            FROM public.hef_event h
            WHERE p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', ...)
        )
    GROUP BY 
        CASE 
            WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
            ELSE p.race 
        END
    ) no_asthma
ON races.race = no_asthma.race AND medications.medication = no_asthma.medication;
-- Retrieve the data
SELECT * FROM cii_qa.xtab_race_by_medication ORDER BY race, medication;

-- Race by dx (old) with no asthma dx as row
-- Crosstab of emr_patient.race with and without asthma diagnoses/medications
DROP TABLE IF EXISTS cii_qa.xtab_race_by_asthma_status;
CREATE TABLE cii_qa.xtab_race_by_asthma_status
(
    race TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO cii_qa.xtab_race_by_asthma_status (race, counts)
SELECT 
    race, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
            ELSE p.race
        END AS race
    FROM 
        public.emr_patient p
    JOIN 
        public.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    race

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS race, 
    COUNT(*) AS counts
FROM 
    public.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM public.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM cii_qa.xtab_race_by_asthma_status ORDER BY race;

-- gender rx (new) Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_race_by_medication;
CREATE TABLE cii_qa.xtab_race_by_medication
(
    race TEXT,
    medication TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
    asthma_one_of_each_count INTEGER,
    no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_race_by_medication (race, medication, asthma_criteria1_count, asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
    -- your existing T0 subquery
),
medications as (
    SELECT DISTINCT name AS medication FROM public.hef_event WHERE name IN ('rx:albuterol', 'rx:levalbuterol', ...)
),
races as (
    SELECT DISTINCT CASE WHEN race IS NULL OR race = '' THEN 'UNK' ELSE race END AS race FROM public.emr_patient
)
SELECT 
    races.race,
    medications.medication,
    COALESCE(criteria1.counts, 0) AS asthma_criteria1_count,
    COALESCE(criteria2.counts, 0) AS asthma_criteria2_count,
    COALESCE(one_of_each.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    races
CROSS JOIN 
    medications
LEFT JOIN 
    -- Revised subquery for asthma criteria 1
    (SELECT 
            CASE WHEN p.race IS NULL OR p.race = '' THEN 'UNK' ELSE p.race END AS race,
            h.name AS medication,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        JOIN
            public.hef_event h ON p.id = h.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
            AND h.name IN ('rx:albuterol', 'rx:levalbuterol', ...)
        GROUP BY 
            race, h.name
    ) criteria1
ON races.race = criteria1.race AND medications.medication = criteria1.medication
-- Repeat similar joins for criteria2, one_of_each, and no_asthma
-- ...

-- Retrieve the data
SELECT * FROM cii_qa.xtab_race_by_medication ORDER BY race, medication;


-- Ethnicity by dx (new) with no asthma as column
-- Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_ethnicity_by_asthma_status;
CREATE TABLE cii_qa.xtab_ethnicity_by_asthma_status
(
    ethnicity TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
	asthma_one_of_each_count INTEGER,
	no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_ethnicity_by_asthma_status (ethnicity, asthma_criteria1_count, 
asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
	SELECT DISTINCT t1.patient_id FROM hef_event AS T1
	JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
	WHERE T1.name = 'dx:asthma' 
	 AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
	 AND NOT EXISTS (SELECT NULL FROM NODIS_CASE c WHERE c.patient_id = t1.patient_id AND c.condition = 'asthma') 	
)
SELECT 
    COALESCE(asthma.ethnicity, no_asthma.ethnicity) AS ethnicity, 
    COALESCE(asthma.counts, 0) AS asthma_criteria1_count,
	COALESCE(asthma2.counts, 0) AS asthma_criteria2_count,
	COALESCE(asthma3.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    (
        SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
    ) asthma
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #2%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
	) asthma2
ON asthma.ethnicity = asthma2.ethnicity
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            t0 ON p.id = t0.patient_id
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
	) asthma3
ON asthma.ethnicity = asthma3.ethnicity
FULL OUTER JOIN 
    (
        SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        WHERE 
            NOT EXISTS (
                SELECT 1
                FROM public.nodis_case nc 
                WHERE p.id = nc.patient_id and nc.condition = 'asthma'
            )
		AND NOT EXISTS (SELECT NULL FROM T0 WHERE T0.patient_id = p.id)
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity
            END
    ) no_asthma
ON asthma.ethnicity = no_asthma.ethnicity;

-- Retrieve the data
SELECT * FROM cii_qa.xtab_ethnicity_by_asthma_status ORDER BY ethnicity;


--Ethnicity by dx (old) 
-- Crosstab of emr_patient.race with and without asthma diagnoses/medications
DROP TABLE IF EXISTS cii_qa.xtab_ethnicty_by_asthma_status;
CREATE TABLE cii_qa.xtab_ethnicity_by_asthma_status
(
    ethnicity TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO cii_qa.xtab_ethnicity_by_asthma_status (ethnicity, counts)
SELECT 
    ethnicity, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
            ELSE p.ethnicity
        END AS ethnicity
    FROM 
        public.emr_patient p
    JOIN 
        public.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    ethnicity

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS ethnicity, 
    COUNT(*) AS counts
FROM 
    public.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM public.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM cii_qa.xtab_ethnicity_by_asthma_status ORDER BY ethnicity;


-- Ethnicity by Rx
-- Crosstab of emr_patient.ethnicity by medication with hef_event filtering, including non-medicated count
DROP TABLE IF EXISTS cii_qa.xtab_ethnicity_by_medication;
CREATE TABLE cii_qa.xtab_ethnicity_by_medication
(
    ethnicity TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_ethnicity_by_medication (ethnicity, medication, counts)
SELECT 
    ethnicity, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
            ELSE p.ethnicity
        END AS ethnicity,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        public.emr_patient p
    LEFT JOIN 
        public.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    ethnicity, 
    medication
;

SELECT * FROM cii_qa.xtab_ethnicity_by_medication ORDER BY ethnicity, medication;



-- Age Group by Rx
-- Crosstab of emr_patient age group by medication with hef_event filtering, including non-medicated count
DROP TABLE IF EXISTS cii_qa.xtab_age_group_by_medication;
CREATE TABLE cii_qa.xtab_age_group_by_medication
(
    age_group TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_age_group_by_medication (age_group, medication, counts)
SELECT 
    age_group, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END AS age_group,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        (SELECT 
            p.id, 
            EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
            p.date_of_birth 
         FROM public.emr_patient p
        ) p
    LEFT JOIN 
        public.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    age_group, 
    medication
;

SELECT * FROM cii_qa.xtab_age_group_by_medication ORDER BY age_group, medication;


-- Age Group by dx
-- Crosstab of emr_patient age group with and without asthma diagnoses
DROP TABLE IF EXISTS cii_qa.xtab_age_group_by_asthma_status;
CREATE TABLE cii_qa.xtab_age_group_by_asthma_status
(
    age_group TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses
INSERT INTO cii_qa.xtab_age_group_by_asthma_status (age_group, counts)
SELECT 
    age_group, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END AS age_group
    FROM 
        (SELECT 
            p.id, 
            EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
            p.date_of_birth 
         FROM public.emr_patient p
        ) p
    JOIN 
        public.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma'
) x
GROUP BY 
    age_group

UNION ALL

-- Insert patients without asthma diagnoses
SELECT 
    'No asthma dx' AS age_group, 
    COUNT(*) AS counts
FROM 
    public.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM public.hef_event h 
        WHERE p.id = h.patient_id AND h.name = 'dx:asthma'
    )
;

SELECT * FROM cii_qa.xtab_age_group_by_asthma_status ORDER BY age_group;




-- Gender by dx
-- Crosstab of emr_patient.gender with and without asthma diagnoses/medications
DROP TABLE IF EXISTS cii_qa.xtab_gender_by_asthma_status;
CREATE TABLE cii_qa.xtab_gender_by_asthma_status
(
    gender TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO cii_qa.xtab_gender_by_asthma_status (gender, counts)
SELECT 
    gender, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.gender IS NULL OR p.gender = '' THEN 'UNK' 
            ELSE UPPER(p.gender)
        END AS gender
    FROM 
        public.emr_patient p
    JOIN 
        public.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    gender

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS gender, 
    COUNT(*) AS counts
FROM 
    public.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM public.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM cii_qa.xtab_gender_by_asthma_status ORDER BY gender;



-- Gender by Rx
-- Crosstab of emr_patient.gender by medication with hef_event filtering, including non-medicated count
DROP TABLE IF EXISTS cii_qa.xtab_gender_by_medication;
CREATE TABLE cii_qa.xtab_gender_by_medication
(
    gender TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_gender_by_medication (gender, medication, counts)
SELECT 
    gender, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.gender IS NULL OR p.gender = '' THEN 'UNK' 
            ELSE UPPER(p.gender)
        END AS gender,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        public.emr_patient p
    LEFT JOIN 
        public.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    gender, 
    medication
;

SELECT * FROM cii_qa.xtab_gender_by_medication ORDER BY gender, medication;
