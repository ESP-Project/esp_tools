

/* MENDS Stage 2 Tests (ms- gen)
 * Summary Test (ungrouped)...
*/

if object_id('cii_qa.summary', 'U') is not null drop table cii_qa.summary;
create table cii_qa.summary 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)
, n_total integer, n_missing integer, pct_missing float, n_empty integer
, pct_empty float, n_null integer, pct_null float);

/* emr_patient.state */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'state' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(state)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(state)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(state) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(state))/count(*)),2) else null end as pct_null
from (
  select 
    state as state
  , case when cast(state as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.zip */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'zip' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(zip)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(zip)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(zip) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(zip))/count(*)),2) else null end as pct_null
from (
  select 
    zip as zip
  , case when cast(zip as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.date_of_birth */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'date_of_birth' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date_of_birth)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date_of_birth)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date_of_birth) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date_of_birth))/count(*)),2) else null end as pct_null
from (
  select 
    date_of_birth as date_of_birth
  , case when cast(date_of_birth as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.age_yrs_derived */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'age_yrs_derived' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(age_yrs_derived)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(age_yrs_derived)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(age_yrs_derived) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(age_yrs_derived))/count(*)),2) else null end as pct_null
from (
  select 
    datediff(year, date_of_birth, sysdatetime()) as age_yrs_derived
  , case when cast(datediff(year, date_of_birth, sysdatetime()) as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.race */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'race' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(race)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(race)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(race) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(race))/count(*)),2) else null end as pct_null
from (
  select 
    race as race
  , case when cast(race as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.ethnicity */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'ethnicity' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(ethnicity)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(ethnicity)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(ethnicity) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(ethnicity))/count(*)),2) else null end as pct_null
from (
  select 
    ethnicity as ethnicity
  , case when cast(ethnicity as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_patient.gender */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_patient' as esp_table
  , 'gender' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(gender)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(gender)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(gender) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(gender))/count(*)),2) else null end as pct_null
from (
  select 
    gender as gender
  , case when cast(gender as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_patient
  where 1 = 1
) p
;

/* emr_encounter.date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date))/count(*)),2) else null end as pct_null
from (
  select 
    date as date
  , case when cast(date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.pregnant */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'pregnant' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(pregnant)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(pregnant)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(pregnant) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(pregnant))/count(*)),2) else null end as pct_null
from (
  select 
    pregnant as pregnant
  , case when cast(pregnant as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from (
    select ts.name as event_name
    , case 
      when ts.name = 'pregnancy' and (enc.date between ts.start_date and ts.end_date) then 'y'
      else 'n'
      end as pregnant
    from emr_encounter enc
    left outer join hef_timespan ts
    on ts.patient_id = enc.patient_id
  where 1 = 1
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
) q
;

/* emr_encounter.edd */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'edd' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(edd)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(edd)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(edd) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(edd))/count(*)),2) else null end as pct_null
from (
  select 
    edd as edd
  , case when cast(edd as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.temperature */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'temperature' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(temperature)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(temperature)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(temperature) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(temperature))/count(*)),2) else null end as pct_null
from (
  select 
    temperature as temperature
  , case when cast(temperature as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.weight */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'weight' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(weight)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(weight)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(weight) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(weight))/count(*)),2) else null end as pct_null
from (
  select 
    weight as weight
  , case when cast(weight as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and weight between 0 and 453.6  -- Apply Vitals Exclusion lower and upper limits
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.height */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'height' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(height)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(height)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(height) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(height))/count(*)),2) else null end as pct_null
from (
  select 
    height as height
  , case when cast(height as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and height between 0 and 228.6  -- Apply Vitals Exclusion lower and upper limits
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.bp_systolic */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'bp_systolic' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(bp_systolic)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(bp_systolic)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(bp_systolic) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(bp_systolic))/count(*)),2) else null end as pct_null
from (
  select 
    bp_systolic as bp_systolic
  , case when cast(bp_systolic as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and bp_systolic between 20 and 300  -- Apply Vitals Exclusion lower and upper limits
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.bp_diastolic */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'bp_diastolic' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(bp_diastolic)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(bp_diastolic)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(bp_diastolic) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(bp_diastolic))/count(*)),2) else null end as pct_null
from (
  select 
    bp_diastolic as bp_diastolic
  , case when cast(bp_diastolic as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and bp_diastolic between 5 and 200  -- Apply Vitals Exclusion lower and upper limits
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter.bmi */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter' as esp_table
  , 'bmi' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(bmi)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(bmi)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(bmi) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(bmi))/count(*)),2) else null end as pct_null
from (
  select 
    bmi as bmi
  , case when cast(bmi as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter
  where 1 = 1
  and bmi between 12 and 70  -- Apply Vitals Exclusion lower and upper limits
  and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
) p
;

/* emr_encounter_dx_codes.dx_code_id */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_encounter_dx_codes' as esp_table
  , 'dx_code_id' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(dx_code_id)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(dx_code_id)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(dx_code_id) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(dx_code_id))/count(*)),2) else null end as pct_null
from (
  select 
    dx_code_id as dx_code_id
  , case when cast(dx_code_id as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_encounter_dx_codes
  where 1 = 1
  and lower(dx_code_id) <> 'icd9:799.9'
) p
;

/* emr_prescription.date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date))/count(*)),2) else null end as pct_null
from (
  select 
    date as date
  , case when cast(date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.name */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'name' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(name)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(name)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(name) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(name))/count(*)),2) else null end as pct_null
from (
  select 
    name as name
  , case when cast(name as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.quantity */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'quantity' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(quantity)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(quantity)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(quantity) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(quantity))/count(*)),2) else null end as pct_null
from (
  select 
    quantity as quantity
  , case when cast(quantity as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.refills */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'refills' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(refills)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(refills)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(refills) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(refills))/count(*)),2) else null end as pct_null
from (
  select 
    refills as refills
  , case when cast(refills as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.start_date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'start_date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(start_date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(start_date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(start_date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(start_date))/count(*)),2) else null end as pct_null
from (
  select 
    start_date as start_date
  , case when cast(start_date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.end_date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'end_date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(end_date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(end_date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(end_date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(end_date))/count(*)),2) else null end as pct_null
from (
  select 
    end_date as end_date
  , case when cast(end_date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.route */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'route' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(route)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(route)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(route) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(route))/count(*)),2) else null end as pct_null
from (
  select 
    route as route
  , case when cast(route as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_prescription.dose */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_prescription' as esp_table
  , 'dose' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(dose)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(dose)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(dose) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(dose))/count(*)),2) else null end as pct_null
from (
  select 
    dose as dose
  , case when cast(dose as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_prescription
  where 1 = 1
) p
;

/* emr_labresult.date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date))/count(*)),2) else null end as pct_null
from (
  select 
    date as date
  , case when cast(date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_labresult.result_date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'result_date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(result_date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(result_date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(result_date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(result_date))/count(*)),2) else null end as pct_null
from (
  select 
    result_date as result_date
  , case when cast(result_date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_labresult.native_code */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'native_code' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(native_code)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(native_code)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(native_code) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(native_code))/count(*)),2) else null end as pct_null
from (
  select 
    native_code as native_code
  , case when cast(native_code as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_labresult.native_name */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'native_name' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(native_name)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(native_name)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(native_name) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(native_name))/count(*)),2) else null end as pct_null
from (
  select 
    native_name as native_name
  , case when cast(native_name as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_labresult.result_string */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'result_string' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(result_string)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(result_string)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(result_string) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(result_string))/count(*)),2) else null end as pct_null
from (
  select 
    result_string as result_string
  , case when cast(result_string as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_labresult.result_float */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_labresult' as esp_table
  , 'result_float' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(result_float)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(result_float)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(result_float) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(result_float))/count(*)),2) else null end as pct_null
from (
  select 
    result_float as result_float
  , case when cast(result_float as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_labresult
  where 1 = 1
) p
;

/* emr_socialhistory.tobacco_use */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_socialhistory' as esp_table
  , 'tobacco_use' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(tobacco_use)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(tobacco_use)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(tobacco_use) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(tobacco_use))/count(*)),2) else null end as pct_null
from (
  select 
    tobacco_use as tobacco_use
  , case when cast(tobacco_use as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_socialhistory
  where 1 = 1
) p
;

/* emr_socialhistory.date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_socialhistory' as esp_table
  , 'date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date))/count(*)),2) else null end as pct_null
from (
  select 
    date as date
  , case when cast(date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_socialhistory
  where 1 = 1
) p
;

/* emr_immunization.name */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_immunization' as esp_table
  , 'name' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(name)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(name)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(name) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(name))/count(*)),2) else null end as pct_null
from (
  select 
    name as name
  , case when cast(name as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_immunization
  where 1 = 1
) p
;

/* emr_immunization.date */
insert into cii_qa.summary (
  test_name, esp_table, esp_column
, n_total, n_missing, pct_missing, n_empty
, pct_empty, n_null, pct_null)
select
    'Summary' as test_name
  , 'emr_immunization' as esp_table
  , 'date' as esp_column
  , count(*) as n_total
  , sum(is_empty) + (count(*) - count(date)) as n_missing
  , case when count(*) > 0 then round(100.0*(sum(is_empty) + (count(*) - count(date)))/count(*),2) else null end as pct_missing
  , sum(is_empty) as n_empty
  , round(100.0*sum(is_empty)/count(*),2) as pct_empty
  , count(*) - count(date) as n_null
  , case when count(*) > 0 then round((100.0*(count(*) - count(date))/count(*)),2) else null end as pct_null
from (
  select 
    date as date
  , case when cast(date as varchar(MAX)) = '' then 1 else 0 end as is_empty
  from emr_immunization
  where 1 = 1
) p
;

select * from cii_qa.summary
order by esp_table, esp_column;
/* ... end Summary Test (ungrouped) */
