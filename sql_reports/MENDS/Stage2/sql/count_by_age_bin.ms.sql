/* Create fixed-size bins for current age (in years). 
 *   <5, 5-9, �80-85, 85+
 * Calculate age for each patient. 
 * Count number of patients in each bin.
 */

if object_id('cii_qa.count_by_age_bin', 'U') is not null drop table cii_qa.count_by_age_bin;
create table cii_qa.count_by_age_bin 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)
, bin varchar(MAX), count integer);

insert into cii_qa.count_by_age_bin (
test_name, esp_table, esp_column, bin, count )
select 
  'count by date bin' as test_name, 'emr_patient' as esp_table, 'current age (yrs)' as esp_column
, bin, count(*) as count from (
select 
  case
  when datediff(year, date_of_birth, getdate()) <  5 then 'b01_lt5'
  when datediff(year, date_of_birth, getdate()) < 10 then 'b02_ge5_lt10'
  when datediff(year, date_of_birth, getdate()) < 15 then 'b03_ge10_lt15'
  when datediff(year, date_of_birth, getdate()) < 20 then 'b04_ge15_lt20'
  when datediff(year, date_of_birth, getdate()) < 25 then 'b05_ge20_lt25'
  when datediff(year, date_of_birth, getdate()) < 30 then 'b06_ge25_lt30'
  when datediff(year, date_of_birth, getdate()) < 35 then 'b07_ge30_lt35'
  when datediff(year, date_of_birth, getdate()) < 40 then 'b08_ge35_lt40'
  when datediff(year, date_of_birth, getdate()) < 45 then 'b09_ge40_lt45'
  when datediff(year, date_of_birth, getdate()) < 50 then 'b10_ge45_lt50'
  when datediff(year, date_of_birth, getdate()) < 55 then 'b11_ge50_lt55'
  when datediff(year, date_of_birth, getdate()) < 60 then 'b12_ge55_lt60'
  when datediff(year, date_of_birth, getdate()) < 65 then 'b13_ge60_lt65'
  when datediff(year, date_of_birth, getdate()) < 70 then 'b14_ge65_lt70'
  when datediff(year, date_of_birth, getdate()) < 75 then 'b16_ge70_lt75'
  when datediff(year, date_of_birth, getdate()) < 80 then 'b17_ge75_lt80'
  when datediff(year, date_of_birth, getdate()) < 85 then 'b18_ge80_lt85'
  when datediff(year, date_of_birth, getdate()) >= 85 then 'b19_ge85'
  else null
  end as bin
from emr_patient pt
) a
group by bin;
select * from cii_qa.count_by_age_bin order by bin;
