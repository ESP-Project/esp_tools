-- Race by dx
-- Crosstab of emr_patient.race with and without asthma diagnoses/medications
DROP TABLE IF OBJECT_ID cii_qa.xtab_race_by_asthma_status;
CREATE TABLE dbo. cii_qa.xtab_race_by_asthma_status
(
    race TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO cii_qa.xtab_race_by_asthma_status (race, counts)
SELECT 
    race, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
            ELSE p.race
        END AS race
    FROM 
        dbo.emr_patient p
    JOIN 
        dbo.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    race

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS race, 
    COUNT(*) AS counts
FROM 
    dbo.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM dbo.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM cii_qa.xtab_race_by_asthma_status ORDER BY race;

-- Race by Rx
-- Crosstab of emr_patient.race by medication with hef_event filtering, including non-medicated count
DROP TABLE IF OBJECT_ID cii_qa.xtab_race_by_medication;
CREATE TABLE dbo. cii_qa.xtab_race_by_medication
(
    race TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_race_by_medication (race, medication, counts)
SELECT 
    race, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.race IS NULL OR p.race = '' THEN 'UNK' 
            ELSE p.race
        END AS race,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        dbo.emr_patient p
    LEFT JOIN 
        dbo.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    race, 
    medication
;

SELECT * FROM cii_qa.xtab_race_by_medication ORDER BY race, medication;


--Ethnicity by dx
-- Crosstab of emr_patient.race with and without asthma diagnoses/medications
DROP TABLE IF OBJECT_ID cii_qa.xtab_ethnicty_by_asthma_status;
CREATE TABLE dbo. cii_qa.xtab_ethnicity_by_asthma_status
(
    ethnicity TEXT,
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO cii_qa.xtab_ethnicity_by_asthma_status (ethnicity, counts)
SELECT 
    ethnicity, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
            ELSE p.ethnicity
        END AS ethnicity
    FROM 
        dbo.emr_patient p
    JOIN 
        dbo.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    ethnicity

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS ethnicity, 
    COUNT(*) AS counts
FROM 
    dbo.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM dbo.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM cii_qa.xtab_ethnicity_by_asthma_status ORDER BY ethnicity;


-- Ethnicity by Rx
-- Crosstab of emr_patient.ethnicity by medication with hef_event filtering, including non-medicated count
DROP TABLE IF OBJECT_ID cii_qa.xtab_ethnicity_by_medication;
CREATE TABLE dbo. cii_qa.xtab_ethnicity_by_medication
(
    ethnicity TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_ethnicity_by_medication (ethnicity, medication, counts)
SELECT 
    ethnicity, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
            ELSE p.ethnicity
        END AS ethnicity,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        dbo.emr_patient p
    LEFT JOIN 
        dbo.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    ethnicity, 
    medication
;

SELECT * FROM cii_qa.xtab_ethnicity_by_medication ORDER BY ethnicity, medication;



-- Age Group by Rx
-- Crosstab of emr_patient age group by medication with hef_event filtering, including non-medicated count
IF OBJECT_ID('dbo.cii_qa.xtab_age_group_by_medication') IS NOT NULL
    DROP TABLE dbo.cii_qa.xtab_age_group_by_medication;

CREATE TABLE dbo.cii_qa.xtab_age_group_by_medication
(
    age_group NVARCHAR(MAX),
    medication NVARCHAR(MAX),
    counts INT
);

INSERT INTO dbo.cii_qa.xtab_age_group_by_medication (age_group, medication, counts)
SELECT 
    age_group, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END AS age_group,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        (
            SELECT 
                p.id, 
                DATEDIFF(YEAR, p.date_of_birth, GETDATE()) - 
                CASE 
                    WHEN DATEADD(YEAR, DATEDIFF(YEAR, p.date_of_birth, GETDATE()), p.date_of_birth) > GETDATE() 
                    THEN 1 
                    ELSE 0 
                END AS age,
                p.date_of_birth 
            FROM 
                dbo.emr_patient p
        ) p
    LEFT JOIN 
        dbo.hef_event h ON p.id = h.patient_id 
        AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    age_group, 
    medication
;

SELECT * FROM dbo.cii_qa.xtab_age_group_by_medication ORDER BY age_group, medication;


-- Age Group by dx
-- Crosstab of emr_patient age group with and without asthma diagnoses
IF OBJECT_ID('tempdb..#xtab_age_group_by_asthma_status') IS NOT NULL
    DROP TABLE #xtab_age_group_by_asthma_status;

CREATE TABLE #xtab_age_group_by_asthma_status
(
    age_group NVARCHAR(MAX),
    counts INT
);

-- Insert patients with asthma diagnoses
INSERT INTO #xtab_age_group_by_asthma_status (age_group, counts)
SELECT 
    age_group, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN age <= 7 THEN '0-7'
            WHEN age > 7 AND age <= 19 THEN '8-19'
            WHEN age > 19 AND age <= 34 THEN '20-34'
            WHEN age > 34 AND age <= 54 THEN '35-54'
            WHEN age > 54 AND age <= 74 THEN '55-74'
            ELSE '75 and over'
        END AS age_group
    FROM 
        (
            SELECT 
                p.id, 
                CASE 
                    WHEN DATEADD(YEAR, DATEDIFF(YEAR, p.date_of_birth, GETDATE()), p.date_of_birth) > GETDATE() 
                    THEN DATEDIFF(YEAR, p.date_of_birth, GETDATE()) - 1
                    ELSE DATEDIFF(YEAR, p.date_of_birth, GETDATE()) 
                END AS age
            FROM 
                dbo.emr_patient p
        ) p
    JOIN 
        dbo.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma'
) x
GROUP BY 
    age_group

UNION ALL

-- Insert patients without asthma diagnoses
SELECT 
    'No asthma dx' AS age_group, 
    COUNT(*) AS counts
FROM 
    dbo.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM dbo.hef_event h 
        WHERE p.id = h.patient_id AND h.name = 'dx:asthma'
    )
;

SELECT * FROM #xtab_age_group_by_asthma_status ORDER BY age_group;



-- Gender by dx
-- Crosstab of emr_patient.gender with and without asthma diagnoses/medications
DROP TABLE IF exists #xtab_gender_by_asthma_status;
CREATE TABLE #xtab_gender_by_asthma_status
(
    gender nvarchar(max),
    counts INTEGER
);

-- Insert patients with asthma diagnoses/medications
INSERT INTO #xtab_gender_by_asthma_status (gender, counts)
SELECT 
    gender, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
                p.id, 
                CASE 
                    WHEN DATEADD(YEAR, DATEDIFF(YEAR, p.date_of_birth, GETDATE()), p.date_of_birth) > GETDATE() 
                    THEN DATEDIFF(YEAR, p.date_of_birth, GETDATE()) - 1
                    ELSE DATEDIFF(YEAR, p.date_of_birth, GETDATE()) 
                END AS gender
            FROM 
                dbo.emr_patient p
    JOIN 
        dbo.hef_event h ON p.id = h.patient_id
    WHERE 
        h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    gender

UNION ALL

-- Insert patients without asthma diagnoses/medications
SELECT 
    'No asthma dx or rx' AS gender, 
    COUNT(*) AS counts
FROM 
    dbo.emr_patient p
WHERE 
    NOT EXISTS (
        SELECT 1
        FROM dbo.hef_event h 
        WHERE p.id = h.patient_id AND (h.name = 'dx:asthma' OR h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol'))
    )
;

SELECT * FROM #xtab_gender_by_asthma_status ORDER BY gender;



-- Gender by Rx
-- Crosstab of emr_patient.gender by medication with hef_event filtering, including non-medicated count
DROP TABLE IF OBJECT_ID cii_qa.xtab_gender_by_medication;
CREATE TABLE dbo. cii_qa.xtab_gender_by_medication
(
    gender TEXT,
    medication TEXT,
    counts INTEGER
);

INSERT INTO cii_qa.xtab_gender_by_medication (gender, medication, counts)
SELECT 
    gender, 
    medication, 
    COUNT(*) AS counts
FROM 
(
    SELECT 
        CASE 
            WHEN p.gender IS NULL OR p.gender = '' THEN 'UNK' 
            ELSE UPPER(p.gender)
        END AS gender,
        CASE 
            WHEN h.name IS NULL THEN 'None'
            ELSE h.name 
        END AS medication
    FROM 
        dbo.emr_patient p
    LEFT JOIN 
        dbo.hef_event h ON p.id = h.patient_id AND h.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
) x
GROUP BY 
    gender, 
    medication
;

SELECT * FROM cii_qa.xtab_gender_by_medication ORDER BY gender, medication;

