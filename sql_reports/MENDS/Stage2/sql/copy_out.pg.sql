-- the copy command with the backslash prefix enables any user to copy a file to the local system
-- to any directory where the user has write access.
-- this template assumes that the user is working on a linux system and has write access to /srv/esp/data/validation
-- modify to suit the needs of the current working system
\copy (select * from cii_qa.summary order by esp_table, esp_column) to '/srv/esp/data/validation/summary.csv' delimiter ',' header csv;
\copy (select * from cii_qa.count_by_category order by esp_table, esp_column, category) to '/srv/esp/data/validation/count_by_category.csv' delimiter ',' header csv;
\copy (select * from cii_qa.count_by_date_bin order by esp_table, esp_column, bin) to '/srv/esp/data/validation/count_by_date_bin.csv' delimiter ',' header csv;
\copy (select * from cii_qa.count_by_age_bin order by bin) to '/srv/esp/data/validation/count_by_age_bin.csv' delimiter ',' header csv;
\copy (select * from cii_qa.stats order by test_name, esp_table, esp_column) to '/srv/esp/data/validation/stats.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_by_year order by esp_column, year) to '/srv/esp/data/validation/grouped_by_year.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_by_year_v015 order by esp_column, year) to '/srv/esp/data/validation/grouped_by_year_v015.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_result_float_by_labtest_name order by labtest_name, native_code) to '/srv/esp/data/validation/grouped_result_float_by_labtest_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_rx_by_generic_name order by lower(generic_name)) to '/srv/esp/data/validation/grouped_rx_by_generic_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_hef_evt_dt_bin_by_name order by name, bin) to '/srv/esp/data/validation/grouped_hef_evt_dt_bin_by_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.grouped_hef_tspan_start_bin_by_name order by name, bin) to '/srv/esp/data/validation/grouped_hef_tspan_start_bin_by_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_state_by_gender order by state, gender) to '/srv/esp/data/validation/xtab_state_by_gender.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_state_by_pregnant order by state, is_pregnant) to '/srv/esp/data/validation/.xtab_state_by_pregnant.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_state_by_dx_code_id order by state, dx_code_id) to '/srv/esp/data/validation/xtab_state_by_dx_code_id.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_state_by_imm_name order by state, immunization_name) to '/srv/esp/data/validation/xtab_state_by_imm_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_race_by_gender order by race, gender) to '/srv/esp/data/validation/xtab_race_by_gender.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_race_by_pregnant order by race, is_pregnant) to '/srv/esp/data/validation/xtab_race_by_pregnant.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_race_by_dx_code_id order by race, dx_code_id) to '/srv/esp/data/validation/xtab_race_by_dx_code_id.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_race_by_tobacco_use order by race, tobacco_use) to '/srv/esp/data/validation/xtab_race_by_tobacco_use.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_race_by_imm_name order by race, immunization_name) to '/srv/esp/data/validation/xtab_race_by_imm_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_ethn_by_gender order by ethnicity, gender) to '/srv/esp/data/validation/xtab_ethn_by_gender.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_ethn_by_pregnant order by ethnicity, is_pregnant) to '/srv/esp/data/validation/xtab_ethn_by_pregnant.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_ethn_by_dx_code_id order by ethnicity, dx_code_id) to '/srv/esp/data/validation/xtab_ethn_by_dx_code_id.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_ethn_by_tobacco_use order by ethnicity, tobacco_use) to '/srv/esp/data/validation/xtab_ethn_by_tobacco_use.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_ethn_by_imm_name order by ethnicity, immunization_name) to '/srv/esp/data/validation/xtab_ethn_by_imm_name.csv' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_gender_by_pregnant order by gender, is_pregnant) to '/srv/esp/data/validation/xtab_gender_by_pregnant.csv ' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_gender_by_dx_code_id order by gender, dx_code_id) to '/srv/esp/data/validation/xtab_gender_by_dx_code_id.csv ' delimiter ',' header csv;
\copy (select * from cii_qa.xtab_gender_by_tobacco_use order by gender, tobacco_use) to '/srv/esp/data/validation/xtab_gender_by_tobacco_use.csv' delimiter ',' header csv;
--\copy (select * from cii_qa.xtab_gender_by_imm_name order by gender, immunization_name) to '/srv/esp/data/validation/xtab_gender_by_imm_name.csv' delimiter ',' header csv;
--\copy (select * from cii_qa.xtab_pregnant_by_dx_code_id order by pregnant, dx_code_id) to '/srv/esp/data/validation/xtab_pregnant_by_dx_code_id.csv' delimiter ',' header csv;
--\copy (select * from cii_qa.xtab_pregnant_by_tobacco_use order by pregnant, tobacco_use) to '/srv/esp/data/validation/xtab_pregnant_by_tobacco_use.csv' delimiter ',' header csv;
--\copy (select * from cii_qa.xtab_pregnant_by_imm_name order by pregnant, immunization_name) to '/srv/esp/data/validation/xtab_pregnant_by_imm_name order.csv' delimiter ',' header csv;
--\copy (select * from cii_qa.xtab_tobacco_use_by_imm_name order by tobacco_use, immunization_name) to '/srv/esp/data/validation/xtab_tobacco_use_by_imm_name.csv' delimiter ',' header csv;
