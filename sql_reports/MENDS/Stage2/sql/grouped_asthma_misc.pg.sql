-- Test: Stats for emr_labresult.result_float grouped by labtestmap.test_name
--
-- Writes table: grouped_result_float_by_test_name

drop table if exists cii_qa.grouped_rx_by_generic_name;

create table cii_qa.grouped_rx_by_generic_name 
(test_name text, esp_column text
, grouping_column_name text, generic_name text, count integer);

insert into cii_qa.grouped_rx_by_generic_name (
test_name, esp_column, grouping_column_name, generic_name, count
)
select
  'Stats for emr_prescription.name by drugsynonym.generic_name' as test_name
, 'emr_prescription.name' as esp_column
, 'drugsynonym.generic_name' as grouping_column_name
,lower(ds.generic_name) as generic_name
, count(*) as count
from public.emr_prescription p
join (values 
	  ('Albuterol', ARRAY['AccuNeb', 'Ventolin', 'Proventil', 'ProAir', 'VoSpire', 
'Airomir', 'Asmavent', 'salbutamol']), 
	  ('Levalbuterol', ARRAY['Xopenex']), 
	  ('Pirbuterol', ARRAY['Maxair']),
	  ('Arformoterol', ARRAY['Brovana']),
	  ('Formoterol', ARRAY['Foradil', 'Perforomist', 'Oxeze']),
	  ('Indacaterol', ARRAY['Arcapta']),
	  ('Salmeterol', ARRAY['Serevent']),
	  ('Beclomethasone', ARRAY['Qvar']),
	  ('Budesonide INH', ARRAY['Pulmicort']),
	  ('Ciclesonide INH', ARRAY['Alvesco']),
	  ('Flunisolide INH', ARRAY['Aerobid', 'Aerospan']),
	  ('Fluticasone INH', ARRAY['Flovent']),
	  ('Mometasone INH', ARRAY['Asmanex']),
	  ('Montelukast', ARRAY['Singulair']),
	  ('Zafirlukast', ARRAY['Accolate']),
	  ('Zileuton', ARRAY['Zyflo']),
	  ('Ipratropium', ARRAY['Atrovent']),
	  ('Tiotropium', ARRAY['Spiriva']),
	  ('Cromolyn INH', ARRAY['Intal', 'Gastrocrom', 'Nalcrom']),
	  ('Omalizumab', ARRAY['Xolair']),
	  ('Fluticasone + Salmeterol', ARRAY['Advair']),
	  ('Albuterol + Ipratropium', ARRAY['Combivent', 'Duoneb']),
	  ('Mometasone + Formoterol', ARRAY['Dulera', 'Zenhale']),
	  ('Budesonide + Formoterol', ARRAY['Symbicort']),
	  ('Benralizumab', ARRAY['Fasenra']),
	  ('Mepolizumab', ARRAY['Nucala']),
	  ('Reslizumab', ARRAY['Cinqair']),
	  ('Theophylline', ARRAY['Theochron', 'Elixophyllin', 'Theo-24', 'Theo-Dur', 'Uniphyl', 'Uni-Dur', 'Aminophylline']),
	  ('Fluticasone + Vilanterol', ARRAY['Breo'])
	 )
	  	ds (generic_name, other_name)
on p.name ~* (array_to_string(other_name, '|') || '|' || generic_name)

where (lower(ds.generic_name) = 'albuterol'
      	and p.name !~* '(levalbuterol|ipratropium|xopenex)')
      or
      (lower(ds.generic_name) = 'levalbuterol')
      or
      (lower(ds.generic_name) = 'pirbuterol')
	  or
	  (lower(ds.generic_name) = 'arformoterol')
	  or
	  (lower(ds.generic_name) = 'formoterol'
	  	and p.name !~* '(aformoterol|mometasone|budesonide|brovana)')
	  or
	  (lower(ds.generic_name) = 'indacaterol')
	  or
	  (lower(ds.generic_name) = 'salmeterol'
	  	and p.name !~* 'fluticasone')
	  or
	  (lower(ds.generic_name) = 'beclomethasone')
	  or
      (lower(ds.generic_name) = 'budesonide'
       and p.name ~* '(inh| neb|aer)'
       and p.name !~* '(formoterol|pulmicort)')
	  or
	  (lower(ds.generic_name) = 'pulmicort')
	  or
	  (lower(ds.generic_name) = 'ciclesonide'
	  	and p.name ~* '(inh| neb|aer)'
	  	and p.name !~* '(alvesco)')
	  or
	  (lower(ds.generic_name) = 'alvesco')
	  or
      (lower(ds.generic_name) = 'flunisolide'
       and p.name ~* '(inh| neb|aer)'
       and p.name !~* '(aerobid|aerospan)')
	  or
	  (lower(ds.generic_name) = 'aerobid')
	  or
      (lower(ds.generic_name) = 'fluticasone' or lower(ds.generic_name) = 'salmeterol'
       and p.name ~* '(inh| neb|aer)'
       and p.name !~* '(salmeterol|flovent|vilanterol)')
	  or
	  (lower(ds.generic_name) = 'flovent')
	  or
      (lower(ds.generic_name) = 'mometasone'
       and p.name ~* '(inh| neb|aer)'
       and p.name !~* '(formoterol|dulera|zenhale|asmanex)')
	  or
	  (lower(ds.generic_name) = 'asmanex')
	  or
	  (lower(ds.generic_name) = 'montelukast')
	  or
	  (lower(ds.generic_name) = 'zafirlukast')
	  or
	  (lower(ds.generic_name) = 'zileuton')
	  or
	  (lower(ds.generic_name) = 'ipratropium'
	   and p.name !~* '(albuterol)')
	  or
	  (lower(ds.generic_name) = 'tiotropium')
	  or
      (lower(ds.generic_name) = 'cromolyn'
       and p.name ~* '(inh| neb|aer)'
       and p.name !~* '(intal|gastrocrom|nalcrom)')
	  or
	  (lower(ds.generic_name) = 'intal')
	  or
	  (lower(ds.generic_name) = 'omalizumab')
	   or
	  (lower(ds.generic_name) = 'benralizumab') 
      or
	  (lower(ds.generic_name) = 'mepolizumab')
	  or
	  (lower(ds.generic_name) = 'reslizumab')
	  or
	  (lower(ds.generic_name) = 'dupilumab')
	  or
	  (lower(ds.generic_name) = 'theophylline'
	  	)
	  --Combinations
	  or
	  (lower(ds.generic_name) = 'fluticasone'
	   and p.name ~* '(salmeterol|fluticasone)'
	   and p.name !~* 'advair')
	  or
	  (lower(ds.generic_name) = 'advair')
	  or
	  (lower(ds.generic_name) = 'fluticasone'
	  	and p.name ~* '(fluticasone|vilanterol)'
	  	and p.name !~* 'breo')
	  or
	  (lower(ds.generic_name) = 'breo')
	  or
	  (lower(ds.generic_name) = 'albuterol'
	  	and p.name ~* '(albuterol|ipratropium)'
	  	and p.name !~* 'combivent|duoneb')
	  or
	  (lower(ds.generic_name) = 'combivent')
	  or
	  (lower(ds.generic_name) = 'mometasone'
	   	and p.name ~* '(mometasone|formoterol)'
	   	and p.name !~* '(dulera|zenhale)')
	  or
	  (lower(ds.generic_name) = 'dulera')
	  or
	  (lower(ds.generic_name) = 'budesonide'
	   	and p.name ~* '(budesonide|formoterol)'
	   	and p.name !~* '(symb)')
	  or
	  (lower(ds.generic_name) = 'symbicort')
	  GROUP BY
	  	test_name,
		esp_column, 
		grouping_column_name,
		generic_name;
		
		
/* Inspect results */
select * from cii_qa.grouped_rx_by_generic_name
order by lower(generic_name);
