#!/bin/bash
#modify these to suit your server
ESP_TOOLS=/srv/esp/esp_tools/sql_reports/MENDS/Stage2/sql
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/summary.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/count_by_category.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/count_by_date_bin.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/count_by_age_bin.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/stats.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/grouped_by_year.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/grouped_by_year_v0.15.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/grouped_by_year_counts.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/grouped_misc.pg.sql && \
psql -h esp-db.regenstrief.org -d esp -U esp_app_user -f $ESP_TOOLS/xtabs.pg.sql 
