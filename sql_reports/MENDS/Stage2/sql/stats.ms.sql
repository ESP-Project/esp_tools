

/* MENDS Stage 2 Tests (ms gen)
 * Stats tests (ungrouped)...
*/

if object_id('cii_qa.stats', 'U') is not null drop table cii_qa.stats;
create table cii_qa.stats (
test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)
, whisker_multiplier float
, min float, lower_fence float, p10 float, p25 float, median float
, p75 float, p90 float, upper_fence float, max float
, mean float, stddev float, iqr float
, n_lower_outlier integer, n_q1 integer, n_iqr integer, n_q4 integer
, n_upper_outlier integer, n_undefined integer, n_defined integer, n_total integer
);
/* Stats (wide version, with cutpoints and counts */

/* emr_patient.age_yrs_derived */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(age_yrs_derived) over () as min
   , percentile_disc(0.1) within group (order by age_yrs_derived) over ( ) as p10
   , percentile_disc(0.25) within group (order by age_yrs_derived) over ( ) as p25
   , percentile_disc(0.5) within group (order by age_yrs_derived) over ( ) as median
   , percentile_disc(0.75) within group (order by age_yrs_derived) over ( ) as p75
   , percentile_disc(0.9) within group (order by age_yrs_derived) over ( ) as p90
   , max(age_yrs_derived) over ( ) as max
   , avg(age_yrs_derived) over ( ) as mean
   , stdev(age_yrs_derived) over ( ) as stddev
   from (select  datediff(year, date_of_birth, sysdatetime()) as age_yrs_derived from emr_patient
where 1 = 1
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select datediff(year, date_of_birth, sysdatetime()) as age_yrs_derived, ts.*
, case
  when datediff(year, date_of_birth, sysdatetime()) < lower_fence then 'a_lower_outlier'
  when datediff(year, date_of_birth, sysdatetime()) >= lower_fence and datediff(year, date_of_birth, sysdatetime()) < p25 then 'b_q1'
  when datediff(year, date_of_birth, sysdatetime()) between p25 and p75 then 'c_iqr'
  when datediff(year, date_of_birth, sysdatetime()) > p75 and datediff(year, date_of_birth, sysdatetime()) <= upper_fence then 'd_q4'
  when datediff(year, date_of_birth, sysdatetime()) > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_patient raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_patient' as esp_table, 'age_yrs_derived' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.temperature */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(temperature) over () as min
   , percentile_disc(0.1) within group (order by temperature) over ( ) as p10
   , percentile_disc(0.25) within group (order by temperature) over ( ) as p25
   , percentile_disc(0.5) within group (order by temperature) over ( ) as median
   , percentile_disc(0.75) within group (order by temperature) over ( ) as p75
   , percentile_disc(0.9) within group (order by temperature) over ( ) as p90
   , max(temperature) over ( ) as max
   , avg(temperature) over ( ) as mean
   , stdev(temperature) over ( ) as stddev
   from (select  temperature from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and temperature between 68 and 113  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select temperature, ts.*
, case
  when temperature < lower_fence then 'a_lower_outlier'
  when temperature >= lower_fence and temperature < p25 then 'b_q1'
  when temperature between p25 and p75 then 'c_iqr'
  when temperature > p75 and temperature <= upper_fence then 'd_q4'
  when temperature > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'temperature' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.weight */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(weight) over () as min
   , percentile_disc(0.1) within group (order by weight) over ( ) as p10
   , percentile_disc(0.25) within group (order by weight) over ( ) as p25
   , percentile_disc(0.5) within group (order by weight) over ( ) as median
   , percentile_disc(0.75) within group (order by weight) over ( ) as p75
   , percentile_disc(0.9) within group (order by weight) over ( ) as p90
   , max(weight) over ( ) as max
   , avg(weight) over ( ) as mean
   , stdev(weight) over ( ) as stddev
   from (select  weight from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and weight between 0 and 453.6  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select weight, ts.*
, case
  when weight < lower_fence then 'a_lower_outlier'
  when weight >= lower_fence and weight < p25 then 'b_q1'
  when weight between p25 and p75 then 'c_iqr'
  when weight > p75 and weight <= upper_fence then 'd_q4'
  when weight > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'weight' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.height */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(height) over () as min
   , percentile_disc(0.1) within group (order by height) over ( ) as p10
   , percentile_disc(0.25) within group (order by height) over ( ) as p25
   , percentile_disc(0.5) within group (order by height) over ( ) as median
   , percentile_disc(0.75) within group (order by height) over ( ) as p75
   , percentile_disc(0.9) within group (order by height) over ( ) as p90
   , max(height) over ( ) as max
   , avg(height) over ( ) as mean
   , stdev(height) over ( ) as stddev
   from (select  height from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and height between 0 and 228.6  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select height, ts.*
, case
  when height < lower_fence then 'a_lower_outlier'
  when height >= lower_fence and height < p25 then 'b_q1'
  when height between p25 and p75 then 'c_iqr'
  when height > p75 and height <= upper_fence then 'd_q4'
  when height > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'height' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.bp_systolic */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(bp_systolic) over () as min
   , percentile_disc(0.1) within group (order by bp_systolic) over ( ) as p10
   , percentile_disc(0.25) within group (order by bp_systolic) over ( ) as p25
   , percentile_disc(0.5) within group (order by bp_systolic) over ( ) as median
   , percentile_disc(0.75) within group (order by bp_systolic) over ( ) as p75
   , percentile_disc(0.9) within group (order by bp_systolic) over ( ) as p90
   , max(bp_systolic) over ( ) as max
   , avg(bp_systolic) over ( ) as mean
   , stdev(bp_systolic) over ( ) as stddev
   from (select  bp_systolic from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and bp_systolic between 20 and 300  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select bp_systolic, ts.*
, case
  when bp_systolic < lower_fence then 'a_lower_outlier'
  when bp_systolic >= lower_fence and bp_systolic < p25 then 'b_q1'
  when bp_systolic between p25 and p75 then 'c_iqr'
  when bp_systolic > p75 and bp_systolic <= upper_fence then 'd_q4'
  when bp_systolic > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'bp_systolic' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.bp_diastolic */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(bp_diastolic) over () as min
   , percentile_disc(0.1) within group (order by bp_diastolic) over ( ) as p10
   , percentile_disc(0.25) within group (order by bp_diastolic) over ( ) as p25
   , percentile_disc(0.5) within group (order by bp_diastolic) over ( ) as median
   , percentile_disc(0.75) within group (order by bp_diastolic) over ( ) as p75
   , percentile_disc(0.9) within group (order by bp_diastolic) over ( ) as p90
   , max(bp_diastolic) over ( ) as max
   , avg(bp_diastolic) over ( ) as mean
   , stdev(bp_diastolic) over ( ) as stddev
   from (select  bp_diastolic from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and bp_diastolic between 5 and 200  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select bp_diastolic, ts.*
, case
  when bp_diastolic < lower_fence then 'a_lower_outlier'
  when bp_diastolic >= lower_fence and bp_diastolic < p25 then 'b_q1'
  when bp_diastolic between p25 and p75 then 'c_iqr'
  when bp_diastolic > p75 and bp_diastolic <= upper_fence then 'd_q4'
  when bp_diastolic > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'bp_diastolic' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* Stats (wide version, with cutpoints and counts */

/* emr_encounter.bmi */

/* Determine cutpoints of ranges */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
create table cii_qa.stats_tmp_cutpoints (
whisker_multiplier float, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float, iqr float, lower_fence float, upper_fence float);

insert into cii_qa.stats_tmp_cutpoints (
  whisker_multiplier, min, p10, p25, median, p75, p90, max, mean, stddev
, iqr, lower_fence, upper_fence
)
select
1.5
, max(min) as min
, max(p10) as p10
, max(p25) as p25
, max(median) as median
, max(p75) as p75
, max(p90) as p90
, max(max) as max
, max(mean) as mean
, max(stddev) as stddev
, iqr = max(p75) - max(p25)
, lower_fence = max(p25) - 1.5*(max(p75) - max(p25))
, upper_fence = max(p75) + 1.5*(max(p75) - max(p25))
from (
   select
     min(bmi) over () as min
   , percentile_disc(0.1) within group (order by bmi) over ( ) as p10
   , percentile_disc(0.25) within group (order by bmi) over ( ) as p25
   , percentile_disc(0.5) within group (order by bmi) over ( ) as median
   , percentile_disc(0.75) within group (order by bmi) over ( ) as p75
   , percentile_disc(0.9) within group (order by bmi) over ( ) as p90
   , max(bmi) over ( ) as max
   , avg(bmi) over ( ) as mean
   , stdev(bmi) over ( ) as stddev
   from (select  bmi from emr_encounter
where 1 = 1
and date between CONVERT(varchar, '2015-01-01',23) and sysdatetime()
  and bmi between 12 and 70  -- Apply Vitals Exclusion lower and upper limits
) b
) c
;

/* Calculate the count in each range, store results in tall table rc.  */
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
create table cii_qa.stats_tmp_rc
(id integer, range varchar(MAX), cnt integer);

insert into cii_qa.stats_tmp_rc (
  id, range, cnt
  )
select '1' as id, range, count(*) cnt
from (
select bmi, ts.*
, case
  when bmi < lower_fence then 'a_lower_outlier'
  when bmi >= lower_fence and bmi < p25 then 'b_q1'
  when bmi between p25 and p75 then 'c_iqr'
  when bmi > p75 and bmi <= upper_fence then 'd_q4'
  when bmi > upper_fence then 'e_upper_outlier'
  end as range
from cii_qa.stats_tmp_cutpoints ts
cross join emr_encounter raw
) a
group by range
;

-- select * from cii_qa.stats_tmp_rc;

/* Transpose range counts table,  writing tmp_stats_x_rc_trans */
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
create table cii_qa.stats_tmp_rc_trans (
range varchar(MAX), n_lower_outlier integer, n_q1 integer, n_iqr integer
, n_q4 integer, n_upper_outlier integer, n_undefined integer);
insert into cii_qa.stats_tmp_rc_trans (
  range, n_lower_outlier, n_q1, n_iqr
, n_q4, n_upper_outlier, n_undefined) 
select 'cnt' as range, 
a_lower_outlier as n_lower_outlier, b_q1 as n_q1, c_iqr as n_iqr
, d_q4 as n_q4, e_upper_outlier as n_upper_outlier, undefined as n_undefined
from
(select cnt, range
 from cii_qa.stats_tmp_rc) as SourceTable
 pivot
(
avg(cnt)
for range in (a_lower_outlier, b_q1, c_iqr, d_q4, e_upper_outlier, undefined)
) as PivotTable;
--select * from cii_qa.stats_tmp_rc_trans;


/* Join range limits and count in ranges. Add resulting record to stats table. */
insert into cii_qa.stats (
  test_name, esp_table, esp_column
, whisker_multiplier, min, lower_fence, p10, p25, median
, p75, p90, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, n_undefined
, n_defined, n_total
) 
select 'stats' as test_name, 'emr_encounter' as esp_table, 'bmi' as esp_column
, whisker_multiplier
, min
, lower_fence
, p10, p25, median, p75, p90
, upper_fence, max
, mean, stddev, iqr
, n_lower_outlier, n_q1, n_iqr, n_q4, n_upper_outlier, coalesce(n_undefined,0) as n_undefined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) as n_defined
, coalesce(n_lower_outlier,0) + coalesce(n_q1,0) + coalesce(n_iqr,0) + coalesce(n_q4,0) + 
  coalesce(n_upper_outlier,0) + coalesce(n_undefined,0) as n_total
from cii_qa.stats_tmp_cutpoints      s
cross join cii_qa.stats_tmp_rc_trans t
;

-- select * from cii_qa.stats_tmp_wide_results;


/* drop temp tables */
if object_id('cii_qa.stats_tmp_cutpoints', 'U') is not null drop table cii_qa.stats_tmp_cutpoints;
if object_id('cii_qa.stats_tmp_rc', 'U') is not null drop table cii_qa.stats_tmp_rc;
if object_id('cii_qa.stats_tmp_rc_trans','U') is not null drop table cii_qa.stats_tmp_rc_trans;
select * from cii_qa.stats
order by test_name, esp_table, esp_column
/* ...end Stats tests (ungrouped) */
