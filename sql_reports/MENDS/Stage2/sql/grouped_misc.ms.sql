-- Test: Stats for emr_labresult.result_float grouped by labtestmap.test_name
-- Writes table: grouped_result_float_by_test_name
if object_id('cii_qa.grouped_result_float_by_labtest_name','U') is not null drop table cii_qa.grouped_result_float_by_labtest_name;

/* Stats for emr_labresult.result_float grouped by labtestmap.test_name */
/* Part 1: one record per labtest name, summing across native names */
create table cii_qa.grouped_result_float_by_labtest_name
(test_name varchar(MAX), esp_column varchar(MAX)
, grouping_column_name varchar(MAX), labtest_name varchar(MAX), native_code varchar(MAX)
, count integer, min float, max float, mean float, median float);

insert into cii_qa.grouped_result_float_by_labtest_name (
  test_name, esp_column, grouping_column_name, labtest_name, native_code
, count, min, max, mean, median)
select 
  'Stats for emr_labresult.result_float grouped by labtestmap.test_name' as test_name
, 'emr_labresult.result_float' as esp_column
, 'labtestmap.test_name' as grouping_column_name
, m.test_name as labtest_name
, concat('} Values across all native codes for labtest "', m.test_name,'"') as native_code
, count(r.result_float) as count
, round(cast(min(r.result_float) as decimal),2) as min
, round(cast(max(r.result_float) as decimal),2) as max
, round(cast(avg(r.result_float) as decimal),2) as mean
, round(cast(avg(u.median) as decimal),2) as median
from dbo.emr_labresult r
join dbo.conf_labtestmap m
  on r.native_code=m.native_code
join (
  select v.test_name, max(median) as median, count(*) as count
  from (
  select t.test_name as test_name, s.native_code as native_code
  , percentile_disc(0.5) within group (order by s.result_float) over (partition by t.test_name) as median
  from  dbo.emr_labresult s
  join dbo.conf_labtestmap t
    on t.native_code=s.native_code
	) v
group by v.test_name
  ) u
on u.test_name = m.test_name
group by m.test_name
union
/* Part 2: one record per native name */
select 
  'Stats for emr_labresult.result_float grouped by labtestmap.test_name' as test_name
, 'emr_labresult.result_float' as esp_column
, 'labtestmap.test_name' as grouping_column_name
, m.test_name as labtest_name
, ' ' + m.native_code as native_code -- ' ' forces sort to put cross-native_code values last
, count(r.result_float) as count
, round(cast(min(r.result_float) as decimal),2) as min
, round(cast(max(r.result_float) as decimal),2) as max
, round(cast(avg(r.result_float) as decimal),2) as mean
, round(cast(avg(u.median) as decimal),2) as median
from dbo.emr_labresult r
join dbo.conf_labtestmap m
  on r.native_code=m.native_code
join (
select v.test_name, v.native_code, max(median) as median, count(*) as count
from (  
  select t.test_name as test_name, s.native_code as native_code
  , percentile_disc(0.5) within group (order by s.result_float) over (partition by t.test_name, t.native_code) as median
  from  dbo.emr_labresult s
  join dbo.conf_labtestmap t
    on t.native_code=s.native_code
	) v
group by v.test_name, v.native_code
  ) u
on u.test_name = m.test_name and u.native_code = m.native_code
group by m.test_name, m.native_code
;

/* Inspect results */
select * from cii_qa.grouped_result_float_by_labtest_name
order by labtest_name, native_code;

-- Test: Stats for emr_labresult.result_float grouped by labtestmap.test_name
-- Writes table: grouped_result_float_by_test_name

if object_id('cii_qa.grouped_result_float_by_labtest_name','U') is not null drop table cii_qa.grouped_result_float_by_labtest_name;

/* Stats for emr_labresult.result_float grouped by labtestmap.test_name */
/* Part 1: one record per labtest name, summing across native names */
create table cii_qa.grouped_result_float_by_labtest_name
(test_name varchar(MAX), esp_column varchar(MAX)
, grouping_column_name varchar(MAX), labtest_name varchar(MAX), native_code varchar(MAX)
, count integer, min float, p25 float, median float, p75 float, max float, mean float, stddev float);

insert into cii_qa.grouped_result_float_by_labtest_name (
  test_name, esp_column, grouping_column_name, labtest_name, native_code
, count, min, p25, median, p75, max, mean, stddev)
select 
  'Stats for emr_labresult.result_float grouped by labtestmap.test_name' as test_name
, 'emr_labresult.result_float' as esp_column
, 'labtestmap.test_name' as grouping_column_name
, m.test_name as labtest_name
, concat('} Values across all native codes for labtest "', m.test_name,'"') as native_code
, count(r.result_float) as count
, round(min(r.result_float),2) as min
, round(max(p25), 2) as p25
, round(avg(u.median),2) as median
, round(max(p75), 2) as p75
, round(max(r.result_float),2) as max
, round(avg(r.result_float),2) as mean
, round(stdev(r.result_float),2) as stddev
from dbo.emr_labresult r
join dbo.conf_labtestmap m
  on r.native_code=m.native_code
join (
  select v.test_name, max(p25) as p25, max(median) as median, max(p75) as p75, count(*) as count
  from (
  select t.test_name as test_name, s.native_code as native_code
  , percentile_disc(0.25) within group (order by s.result_float) over (partition by t.test_name) as p25
  , percentile_disc(0.5) within group (order by s.result_float) over (partition by t.test_name) as median
  , percentile_disc(0.75) within group (order by s.result_float) over (partition by t.test_name) as p75
  from  dbo.emr_labresult s
  join dbo.conf_labtestmap t
    on t.native_code=s.native_code
	) v
group by v.test_name
  ) u
on u.test_name = m.test_name
group by m.test_name
union
/* Part 2: one record per native name */
select 
  'Stats for emr_labresult.result_float grouped by labtestmap.test_name' as test_name
, 'emr_labresult.result_float' as esp_column
, 'labtestmap.test_name' as grouping_column_name
, m.test_name as labtest_name
, ' ' + m.native_code as native_code -- ' ' forces sort to put cross-native_code values last
, count(r.result_float) as count
, round(min(r.result_float),2) as min
, round(avg(u.p25),2) as p25
, round(avg(u.median),2) as median
, round(avg(u.p75),2) as p75
, round(max(r.result_float),2) as max
, round(avg(r.result_float),2) as mean
, round(stdev(r.result_float),2) as stddev
from dbo.emr_labresult r
join dbo.conf_labtestmap m
  on r.native_code=m.native_code
join (
select v.test_name, v.native_code, max(p25) as p25, max(median) as median, max(p75) as p75, count(*) as count
from (  
  select t.test_name as test_name, s.native_code as native_code
  , percentile_disc(0.25) within group (order by s.result_float) over (partition by t.test_name, t.native_code) as p25
  , percentile_disc(0.5) within group (order by s.result_float) over (partition by t.test_name, t.native_code) as median
  , percentile_disc(0.75) within group (order by s.result_float) over (partition by t.test_name, t.native_code) as p75
  from  dbo.emr_labresult s
  join dbo.conf_labtestmap t
    on t.native_code=s.native_code
	) v
group by v.test_name, v.native_code
  ) u
on u.test_name = m.test_name and u.native_code = m.native_code
group by m.test_name, m.native_code
;

/* Inspect results */
select * from cii_qa.grouped_result_float_by_labtest_name
order by labtest_name, native_code;


-- Test: Stats for emr_prescription.name by drugsynonym.generic_name
-- Writes table: grouped_rx_by_generic_name
-- 6 hours
 
if object_id('cii_qa.grouped_rx_by_generic_name','U') is not null drop table cii_qa.grouped_rx_by_generic_name;

create table cii_qa.grouped_rx_by_generic_name 
(test_name varchar(MAX), esp_column varchar(MAX)
, grouping_column_name varchar(MAX), generic_name varchar(MAX), count integer);

insert into cii_qa.grouped_rx_by_generic_name (
test_name, esp_column, grouping_column_name, generic_name, count
)
select
  'Stats for emr_prescription.name by drugsynonym.generic_name' as test_name
, 'emr_prescription.name' as esp_column
, 'drugsynonym.generic_name' as grouping_column_name
,lower(ds.generic_name) as generic_name
, count(*) as count
from dbo.emr_prescription p
join dbo.static_drugsynonym ds on lower(p.name) like '%'+ lower(ds.other_name) + '%'
/* 
where lower(ds.generic_name) in ('acebutolol','aliskiren','amlodipine','atenolol','benazepril','betaxolol','bisoprolol'
,'candesartan','captopril','carvedilol','chlorthalidone','clevidipine','clonidine','diltiazem','doxazosin'
,'enalapril','eplerenone','eprosartan','felodipine','fosinopril','guanfacine','hydralazine','hydrochlorothiazide'
,'indapamide','irbesartan','isradipine','labetalol','lisinopril','losartan','methyldopa','metoprolol','moexipril'
,'nadolol','nebivolol','nicardipine','nifedipine','nisoldipine','olmesartan'
,'perindopril','pindolol','prazosin','propranolol','quinapril','ramipril','spironolactone'
,'telmisartan','terazosin','trandolapril','valsartan','verapamil')
*/
/* List extended with generic prescription names from hef_event_date_by_bin_name (V0.17:3) */
where lower(ds.generic_name) in (
'acebutolol','acetone','aerobid','albuterol','albuterol-ipratropium','aliskiren','amlodipine','arformoterol'
,'atenolol','beclomethasone','benazepril','betaxolol','bisoprolol','bisoprolol' ,'budesonide-formoterol'
,'budesonide-inh','bupropion','candesartan','captopril','carvedilol','ceftriaxone_1_or_2_gram','chlorthalidone'
,'ciclesonide-inh','citalopram','clevidipine','clomipramine','clonidine','cromolyn-inh','desipramine','desvenlafaxine'
,'diltiazem','doxazosin','doxazosin' ,'doxepin','duloxetine','enalapril','eplerenone','eprosartan','escitalopram'
,'exenatide','felodipine','flovent','flunisolide-inh','fluoxetine','fluticasone-inh','fluticasone-salmeterol'
,'fluvoxamine','formoterol','fosinopril','glimepiride','glipizide','glucagon','glyburide','guanfacine','hydralazine'
,'hydrochlorothiazide','hydrochlorothiazide' ,'imipramine','indacaterol','indapamide','insulin','ipratropium'
,'irbesartan','isradipine','labetalol','lancets','levalbuterol','lisinopril','losartan','metformin','methyldopa'
,'metoprolol','miglitol','mirtazapine','moexipril','moexipril' ,'mometasone-formoterol','mometasone-inh'
,'montelukast','nadolol','nateglinide','nebivolol','nicardipine','nifedipine','nisoldipine','nortriptyline'
,'olmesartan','olmesartan' ,'omalizumab','paroxetine','penicillin_g','perindopril','phenelzine','pindolol'
,'pioglitazone','pirbuterol','pramlintide','prazosin','propranolol','protriptyline','pulmicort','quinapril'
,'ramipril','repaglinide','salmeterol','selegiline','sertraline','sitagliptin','spironolactone','spironolactone'
,'telmisartan','terazosin','test-strips','tiotropium','trandolapril','tranylcypromine','trazodone','valsartan'
,'venlafaxine','verapamil','zafirlukast','zileuton')
group by lower(ds.generic_name);

/* Inspect results */
select * from cii_qa.grouped_rx_by_generic_name
order by lower(generic_name);




-- Test: Date bins for hef_event date by hef event name
-- Write table: grouped_hef_evt_dt_bin_by_nam

/* Define constants YEAR_MIN and YEAR_MAX. These determine what range of dates are considered. */
declare @HEF_EVT_DT_YEAR_MIN as date = convert(DATETIME, '2015-01-01', 102);
-- declare @HEF_EVT_DT_YEAR_MAX as integer = year(getdate());
declare @HEF_EVT_DT_YEAR_MAX as date = getdate(); -- Use full dates for limits

if object_id('cii_qa.grouped_hef_evt_dt_bin_by_name','U') is not null 
drop table cii_qa.grouped_hef_evt_dt_bin_by_name;

create table cii_qa.grouped_hef_evt_dt_bin_by_name 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX), name varchar(MAX)
-- , year_min integer, year_max integer -- Use full dates for limits
, year_min date, year_max date
, bin integer, min_bin_date varchar(MAX), max_bin_date varchar(MAX), span_days integer, count integer);


/* Determine bin values for hef_event.hef_event_name */
if object_id('cii_qa.tmp_bin_size', 'U') is not null drop table cii_qa.tmp_bin_size;
create table cii_qa.tmp_bin_size (
  min_date date, max_date date, diff_days integer, bin_width_days integer
  );

insert into cii_qa.tmp_bin_size (min_date, max_date, diff_days, bin_width_days)
select  min(date) as min_date, max(date) as max_date, datediff(day, min(date), max(date)) as diff_days
, datediff(day, min(date), max(date))/10 as bin_width_days
from hef_event
-- where year(date) between @HEF_EVT_DT_YEAR_MIN and @HEF_EVT_DT_YEAR_MAX /* Year_min, Year_max */  -- Use full dates for limits
where date between @HEF_EVT_DT_YEAR_MIN and @HEF_EVT_DT_YEAR_MAX /* Year_min, Year_max */
;

-- select * from cii_qa.tmp_bin_size;

/* Figure out start and end of each bin */
if object_id('cii_qa.tmp_bin_extremes', 'U') is not null drop table cii_qa.tmp_bin_extremes;
create table cii_qa.tmp_bin_extremes (bin integer, min_bin_date date, max_bin_date date);

insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 0, dateadd(day,0*bin_width_days, min_date) as min_bin_date, dateadd(day,(1*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 1, dateadd(day,1*bin_width_days, min_date) as min_bin_date, dateadd(day,(2*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 2, dateadd(day,2*bin_width_days, min_date) as min_bin_date, dateadd(day,(3*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 3, dateadd(day,3*bin_width_days, min_date) as min_bin_date, dateadd(day,(4*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 4, dateadd(day,4*bin_width_days, min_date) as min_bin_date, dateadd(day,(5*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 5, dateadd(day,5*bin_width_days, min_date) as min_bin_date, dateadd(day,(6*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 6, dateadd(day,6*bin_width_days, min_date) as min_bin_date, dateadd(day,(7*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 7, dateadd(day,7*bin_width_days, min_date) as min_bin_date, dateadd(day,(8*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 8, dateadd(day,8*bin_width_days, min_date) as min_bin_date, dateadd(day,(9*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 9, dateadd(day,9*bin_width_days, min_date) as min_bin_date, max_date as max_bin_date 
from cii_qa.tmp_bin_size;

-- select *, datediff(day, min_bin_date, max_bin_date) as span_days from cii_qa.tmp_bin_extremes order by bin;

/* Insert into result table */
insert into cii_qa.grouped_hef_evt_dt_bin_by_name (
test_name, esp_table, esp_column, name, year_min, year_max, bin, min_bin_date, max_bin_date
, span_days, count )
select 
  'grouped hef event date bin by name' as test_name, 'hef_event' as esp_table, 'date' as esp_column
, name
, @HEF_EVT_DT_YEAR_MIN as year_min, @HEF_EVT_DT_YEAR_MAX as year_max  /* Year_min, Year_max */
, e.bin, e.min_bin_date, e.max_bin_date
, datediff(day, min_bin_date, max_bin_date) as span_days, d.count
from (
  select name, bin, count(*) count
  from (
    select a.name, date, datediff(day, min_date, date)/bin_width_days as bin
    from hef_event a
    cross join cii_qa.tmp_bin_size b
    -- where year(date) between @HEF_EVT_DT_YEAR_MIN and @HEF_EVT_DT_YEAR_MAX  -- exclude unwanted dates -- Use full dates for limits
    where date between @HEF_EVT_DT_YEAR_MIN and @HEF_EVT_DT_YEAR_MAX  -- exclude unwanted dates
    ) c
  group by name, bin) d
  right join cii_qa.tmp_bin_extremes e
on e.bin = d.bin
-- order by hef_event_name, bin
;

/* Inspect results */
select * from cii_qa.grouped_hef_evt_dt_bin_by_name
order by esp_table, esp_column, name, bin;


-- Test: Date bins for hef_timespan start date by hef timespan name
-- Writes table: grouped_hef_tspan_start_bin_by_name

/* Define constants YEAR_MIN and YEAR_MAX. These determine what range of dates are considered. */
-- declare @HEF_TS_STRT_YEAR_MIN as integer = 2015; -- Use full date for limits
declare @HEF_TS_STRT_YEAR_MIN as date = convert(DATETIME, '2015-01-01', 102);
-- declare @HEF_TS_STRT_YEAR_MAX as integer = year(getdate()); -- Use full date for limits
declare @HEF_TS_STRT_YEAR_MAX as date = getdate();

if object_id('cii_qa.grouped_hef_tspan_start_bin_by_name','U') is not null 
drop table cii_qa.grouped_hef_tspan_start_bin_by_name;

create table cii_qa.grouped_hef_tspan_start_bin_by_name 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX), name varchar(MAX)
, year_min date, year_max date
, bin integer, min_bin_date varchar(MAX), max_bin_date varchar(MAX), span_days integer, count integer);


/* Determine bin values for hef_event.hef_event_name */
if object_id('cii_qa.tmp_bin_size', 'U') is not null drop table cii_qa.tmp_bin_size;
create table cii_qa.tmp_bin_size (
  min_date date, max_date date, diff_days integer, bin_width_days integer
  );

insert into cii_qa.tmp_bin_size (min_date, max_date, diff_days, bin_width_days)
select  min(start_date) as min_date, max(start_date) as max_date, datediff(day, min(start_date), max(start_date)) as diff_days
, datediff(day, min(start_date), max(start_date))/10 as bin_width_days
from hef_timespan
-- where year(start_date) between @HEF_TS_STRT_YEAR_MIN and @HEF_TS_STRT_YEAR_MAX /* Year_min, Year_max */ -- Use full date for limit
where start_date between @HEF_TS_STRT_YEAR_MIN and @HEF_TS_STRT_YEAR_MAX /* Year_min, Year_max */

;

-- select * from cii_qa.tmp_bin_size;

/* Figure out start and end of each bin */
if object_id('cii_qa.tmp_bin_extremes', 'U') is not null drop table cii_qa.tmp_bin_extremes;
create table cii_qa.tmp_bin_extremes (bin integer, min_bin_date date, max_bin_date date);

insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 0, dateadd(day,0*bin_width_days, min_date) as min_bin_date, dateadd(day,(1*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 1, dateadd(day,1*bin_width_days, min_date) as min_bin_date, dateadd(day,(2*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 2, dateadd(day,2*bin_width_days, min_date) as min_bin_date, dateadd(day,(3*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 3, dateadd(day,3*bin_width_days, min_date) as min_bin_date, dateadd(day,(4*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 4, dateadd(day,4*bin_width_days, min_date) as min_bin_date, dateadd(day,(5*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 5, dateadd(day,5*bin_width_days, min_date) as min_bin_date, dateadd(day,(6*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 6, dateadd(day,6*bin_width_days, min_date) as min_bin_date, dateadd(day,(7*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 7, dateadd(day,7*bin_width_days, min_date) as min_bin_date, dateadd(day,(8*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 8, dateadd(day,8*bin_width_days, min_date) as min_bin_date, dateadd(day,(9*bin_width_days)-1, min_date) as max_bin_date 
from cii_qa.tmp_bin_size;
insert into cii_qa.tmp_bin_extremes( bin, min_bin_date, max_bin_date)
select 9, dateadd(day,9*bin_width_days, min_date) as min_bin_date, max_date as max_bin_date 
from cii_qa.tmp_bin_size;

-- select *, datediff(day, min_bin_date, max_bin_date) as span_days from cii_qa.tmp_bin_extremes order by bin;

/* Insert into result table */
insert into cii_qa.grouped_hef_tspan_start_bin_by_name (
test_name, esp_table, esp_column, name, year_min, year_max, bin, min_bin_date, max_bin_date
, span_days, count )
select 
  'grouped hef event date bin by name' as test_name, 'hef_event' as esp_table, 'date' as esp_column
, name
, @HEF_TS_STRT_YEAR_MIN as year_min, @HEF_TS_STRT_YEAR_MAX as year_max  /* Year_min, Year_max */
, e.bin, e.min_bin_date, e.max_bin_date
, datediff(day, min_bin_date, max_bin_date) as span_days, d.count
from (
  select name, bin, count(*) count
  from (
    select a.name, a.start_date, datediff(day, min_date, start_date)/bin_width_days as bin
    from hef_timespan a
    cross join cii_qa.tmp_bin_size b
    -- where year(a.start_date) between @HEF_TS_STRT_YEAR_MIN and @HEF_TS_STRT_YEAR_MAX  -- exclude unwanted dates. -- Use full dates for limit
    where a.start_date between @HEF_TS_STRT_YEAR_MIN and @HEF_TS_STRT_YEAR_MAX  -- exclude unwanted dates.
    ) c
  group by name, bin) d
  right join cii_qa.tmp_bin_extremes e
on e.bin = d.bin
-- order by hef_event_name, bin
;

/* Inspect results */
select * from cii_qa.grouped_hef_tspan_start_bin_by_name
order by esp_table, esp_column, name, bin;






