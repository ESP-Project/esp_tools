/* MENDS Stage 2 Tests (pg)
 * Counts by Category (ungrouped)...
*/

drop table if exists cii_qa.count_by_asthma_category;
create table cii_qa.count_by_asthma_category 
(test_name text, esp_table text, esp_column text
, category text, count integer);

insert into cii_qa.count_by_asthma_category
(test_name, esp_table, esp_column, category, count) 
select
'Count by category' as test_name
, 'hef_event' as ESP_Table, 'name' as ESP_Column
, case 
  when cast(name as text) like '--%' then '''' || cast(name as text)
  else cast(name as text)
  end as category, count(*) as count
from hef_event
where name in ('dx:asthma','rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol',
            'rx:formoterol','rx:indacaterol','rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh',
            'rx:alvesco','rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
            'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh','rx:intal','rx:omalizumab',
            'rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab','rx:theophylline','rx:fluticasone-salmeterol:generic',
             'rx:fluticasone-vilanterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formoterol:generic',
             'rx:budesonide-formoterol:generic','rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade', 
			 'rx:albuterol-ipratropium:trade','rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade')
group by name;

select * from cii_qa.count_by_asthma_category;
