

/* MENDS Stage 2 Tests (ms)
 * Counts by Date bin (ungrouped)...
*/

if object_id('cii_qa.count_by_date_bin', 'U') is not null drop table cii_qa.count_by_date_bin;
create table cii_qa.count_by_date_bin 
(test_name varchar(MAX), esp_table varchar(MAX), esp_column varchar(MAX)
, bin varchar(MAX), count integer);


/* Column: emr_encounter.date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_encounter' as esp_table, 'date' as esp_column
, bin, count(*) as count from (
select
  case
  when date is null then null
  when date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_encounter) a
group by bin
;



/* Column: emr_encounter.edd*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_encounter' as esp_table, 'edd' as esp_column
, bin, count(*) as count from (
select
  case
  when edd is null then null
  when edd < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when edd between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when edd between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when edd between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when edd between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when edd between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when edd between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when edd between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when edd between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when edd between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when edd between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when edd > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_encounter) a
group by bin
;



/* Column: emr_prescription.date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_prescription' as esp_table, 'date' as esp_column
, bin, count(*) as count from (
select
  case
  when date is null then null
  when date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_prescription) a
group by bin
;



/* Column: emr_prescription.start_date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_prescription' as esp_table, 'start_date' as esp_column
, bin, count(*) as count from (
select
  case
  when start_date is null then null
  when start_date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when start_date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when start_date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when start_date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when start_date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when start_date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when start_date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when start_date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when start_date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when start_date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when start_date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when start_date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_prescription) a
group by bin
;



/* Column: emr_prescription.end_date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_prescription' as esp_table, 'end_date' as esp_column
, bin, count(*) as count from (
select
  case
  when end_date is null then null
  when end_date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when end_date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when end_date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when end_date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when end_date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when end_date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when end_date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when end_date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when end_date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when end_date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when end_date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when end_date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_prescription) a
group by bin
;



/* Column: emr_labresult.date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_labresult' as esp_table, 'date' as esp_column
, bin, count(*) as count from (
select
  case
  when date is null then null
  when date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_labresult) a
group by bin
;



/* Column: emr_labresult.result_date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_labresult' as esp_table, 'result_date' as esp_column
, bin, count(*) as count from (
select
  case
  when result_date is null then null
  when result_date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when result_date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when result_date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when result_date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when result_date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when result_date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when result_date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when result_date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when result_date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when result_date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when result_date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when result_date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_labresult) a
group by bin
;



/* Column: emr_socialhistory.date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_socialhistory' as esp_table, 'date' as esp_column
, bin, count(*) as count from (
select
  case
  when date is null then null
  when date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_socialhistory) a
group by bin
;



/* Column: emr_immunization.date*/
insert into cii_qa.count_by_date_bin (
  test_name, esp_table, esp_column, bin, count)
select
  'count by date bin' as test_name, 'emr_immunization' as esp_table, 'date' as esp_column
, bin, count(*) as count from (
select
  case
  when date is null then null
  when date < convert(datetime, '2017-01-01', 102) then 'b00_lt_2017H1'
  when date between convert(datetime, '2017-01-01', 102) and convert(datetime, '2017-06-30', 102) then 'b01_2017H1'
  when date between convert(datetime, '2017-07-01', 102) and convert(datetime, '2017-12-31', 102) then 'b02_2017H2'
  when date between convert(datetime, '2018-01-01', 102) and convert(datetime, '2018-06-30', 102) then 'b03_2018H1'
  when date between convert(datetime, '2018-07-01', 102) and convert(datetime, '2018-12-31', 102) then 'b04_2018H2'
  when date between convert(datetime, '2019-01-01', 102) and convert(datetime, '2019-06-30', 102) then 'b05_2019H1'
  when date between convert(datetime, '2019-07-01', 102) and convert(datetime, '2019-12-31', 102) then 'b06_2019H2'
  when date between convert(datetime, '2020-01-01', 102) and convert(datetime, '2020-06-30', 102) then 'b07_2020H1'
  when date between convert(datetime, '2020-07-01', 102) and convert(datetime, '2020-12-31', 102) then 'b08_2020H2'
  when date between convert(datetime, '2021-01-01', 102) and convert(datetime, '2021-06-30', 102) then 'b09_2021H1'
  when date between convert(datetime, '2021-07-01', 102) and convert(datetime, '2021-12-31', 102) then 'b10_2021H2'
  when date > convert(datetime, '2021-12-31', 102) then 'b11_gt_2021H2'
  else 'OTHER'
  end as bin
from emr_immunization) a
group by bin
;


/* Inspect the results */
select * from cii_qa.count_by_date_bin
order by esp_table, esp_column, bin
;
/* ...end of Count by Date Bin (ungrouped) */
