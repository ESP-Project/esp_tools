/* Obtain difference between counts before and after applying value exclusions */
drop table if exists cii_qa.grouped_by_year_counts;
create table cii_qa.grouped_by_year_counts as
select w_exc.test_name, w_exc.esp_column, w_exc.grouping_column_name, w_exc.year
, no_exc.count as count_before, w_exc.count as count_after
, no_exc.count - w_exc.count as count_diff
, round(cast(100*(no_exc.count - w_exc.count)/no_exc.count as numeric),2) as pct_excluded
from cii_qa.grouped_by_year      w_exc
join cii_qa.grouped_by_year_v015 no_exc
on  no_exc.esp_column = w_exc.esp_column 
and no_exc.grouping_column_name = w_exc.grouping_column_name
and no_exc.year = w_exc.year
order by w_exc.esp_column, w_exc.grouping_column_name, w_exc.year; 

select * from cii_qa.grouped_by_year_counts
order by test_name, esp_column, grouping_column_name, year;