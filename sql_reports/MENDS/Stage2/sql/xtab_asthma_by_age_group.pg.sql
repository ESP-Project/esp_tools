

-- age_group by dx (new) with no asthma as column
-- Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_age_group_by_asthma_status;
CREATE TABLE cii_qa.xtab_age_group_by_asthma_status
(
    age_group TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
	asthma_one_of_each_count INTEGER,
	no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_age_group_by_asthma_status (age_group, asthma_criteria1_count, 
asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
	SELECT DISTINCT t1.patient_id FROM hef_event AS T1
	JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
	WHERE T1.name = 'dx:asthma' 
	 AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
	 AND NOT EXISTS (SELECT NULL FROM NODIS_CASE c WHERE c.patient_id = t1.patient_id AND c.condition = 'asthma') 	
)
SELECT 
    COALESCE(asthma.age_group, no_asthma.age_group) AS age_group, 
    COALESCE(asthma.counts, 0) AS asthma_criteria1_count,
	COALESCE(asthma2.counts, 0) AS asthma_criteria2_count,
	COALESCE(asthma3.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    (
        SELECT 
            CASE 
                WHEN age <= 7 THEN '0-7'
   		        WHEN age > 7 AND age <= 19 THEN '8-19'
            	WHEN age > 19 AND age <= 34 THEN '20-34'
            	WHEN age > 34 AND age <= 54 THEN '35-54'
            	WHEN age > 54 AND age <= 74 THEN '55-74'
            	ELSE '75 and over'
            	END AS age_group,
            COUNT(*) AS counts
        FROM 
            (SELECT
			 	p.id,
			 	EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
			 	p.date_of_birth
			 FROM public.emr_patient p
			) p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
        GROUP BY 
           age_group
    ) asthma
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN age <= 7 THEN '0-7'
   		        WHEN age > 7 AND age <= 19 THEN '8-19'
            	WHEN age > 19 AND age <= 34 THEN '20-34'
            	WHEN age > 34 AND age <= 54 THEN '35-54'
            	WHEN age > 54 AND age <= 74 THEN '55-74'
            	ELSE '75 and over'
            	END AS age_group,
            COUNT(*) AS counts
        FROM 
           (SELECT
			 	p.id,
			 	EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
			 	p.date_of_birth
			 FROM public.emr_patient p
			) p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #2%' and nc.condition = 'asthma'
        GROUP BY 
            age_group
	) asthma2
ON asthma.age_group = asthma2.age_group
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN age <= 7 THEN '0-7'
   		        WHEN age > 7 AND age <= 19 THEN '8-19'
            	WHEN age > 19 AND age <= 34 THEN '20-34'
            	WHEN age > 34 AND age <= 54 THEN '35-54'
            	WHEN age > 54 AND age <= 74 THEN '55-74'
            	ELSE '75 and over'
            	END AS age_group,
            COUNT(*) AS counts
        FROM 
            (SELECT
			 	p.id,
			 	EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
			 	p.date_of_birth
			 FROM public.emr_patient p
			) p
        JOIN 
            t0 ON p.id = t0.patient_id
        GROUP BY 
           age_group
	) asthma3
ON asthma.age_group = asthma3.age_group
FULL OUTER JOIN 
    (
        SELECT 
            CASE 
                WHEN age <= 7 THEN '0-7'
   		        WHEN age > 7 AND age <= 19 THEN '8-19'
            	WHEN age > 19 AND age <= 34 THEN '20-34'
            	WHEN age > 34 AND age <= 54 THEN '35-54'
            	WHEN age > 54 AND age <= 74 THEN '55-74'
            	ELSE '75 and over'
            	END AS age_group,
            COUNT(*) AS counts
        FROM 
            (SELECT
			 	p.id,
			 	EXTRACT(YEAR FROM AGE(CURRENT_DATE, p.date_of_birth)) AS age,
			 	p.date_of_birth
			 FROM public.emr_patient p
			) p
        WHERE 
            NOT EXISTS (
                SELECT 1
                FROM public.nodis_case nc 
                WHERE p.id = nc.patient_id and nc.condition = 'asthma'
            )
		AND NOT EXISTS (SELECT NULL FROM T0 WHERE T0.patient_id = p.id)
        GROUP BY 
           age_group
    ) no_asthma
ON asthma.age_group = no_asthma.age_group;

-- Retrieve the data
SELECT * FROM cii_qa.xtab_age_group_by_asthma_status ORDER BY age_group;
