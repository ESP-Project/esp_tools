-- Ethnicity by dx (new) with no asthma as column
-- Create table for the crosstab
DROP TABLE IF EXISTS cii_qa.xtab_ethnicity_by_asthma_status;
CREATE TABLE cii_qa.xtab_ethnicity_by_asthma_status
(
    ethnicity TEXT,
    asthma_criteria1_count INTEGER,
    asthma_criteria2_count INTEGER,
	asthma_one_of_each_count INTEGER,
	no_asthma_diagnoses_count INTEGER
);

-- Insert data into the table
INSERT INTO cii_qa.xtab_ethnicity_by_asthma_status (ethnicity, asthma_criteria1_count, 
asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
WITH T0 as (
	SELECT DISTINCT t1.patient_id FROM hef_event AS T1
	JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
	WHERE T1.name = 'dx:asthma' 
	 AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
	 AND NOT EXISTS (SELECT NULL FROM NODIS_CASE c WHERE c.patient_id = t1.patient_id AND c.condition = 'asthma') 	
)
SELECT 
    COALESCE(asthma.ethnicity, no_asthma.ethnicity) AS ethnicity, 
    COALESCE(asthma.counts, 0) AS asthma_criteria1_count,
	COALESCE(asthma2.counts, 0) AS asthma_criteria2_count,
	COALESCE(asthma3.counts, 0) AS asthma_one_of_each_count,
    COALESCE(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM 
    (
        SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #1%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
    ) asthma
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            public.nodis_case nc ON p.id = nc.patient_id
        WHERE 
            nc.criteria like 'Criteria #2%' and nc.condition = 'asthma'
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
	) asthma2
ON asthma.ethnicity = asthma2.ethnicity
FULL OUTER JOIN
(
	SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        JOIN 
            t0 ON p.id = t0.patient_id
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity 
            END
	) asthma3
ON asthma.ethnicity = asthma3.ethnicity
FULL OUTER JOIN 
    (
        SELECT 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity
            END AS ethnicity,
            COUNT(*) AS counts
        FROM 
            public.emr_patient p
        WHERE 
            NOT EXISTS (
                SELECT 1
                FROM public.nodis_case nc 
                WHERE p.id = nc.patient_id and nc.condition = 'asthma'
            )
		AND NOT EXISTS (SELECT NULL FROM T0 WHERE T0.patient_id = p.id)
        GROUP BY 
            CASE 
                WHEN p.ethnicity IS NULL OR p.ethnicity = '' THEN 'UNK' 
                ELSE p.ethnicity
            END
    ) no_asthma
ON asthma.ethnicity = no_asthma.ethnicity;

-- Retrieve the data
SELECT * FROM cii_qa.xtab_ethnicity_by_asthma_status ORDER BY ethnicity;
