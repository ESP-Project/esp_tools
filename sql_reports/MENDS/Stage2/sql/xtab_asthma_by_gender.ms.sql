-- Drop table if it exists
IF OBJECT_ID('cii_qa.xtab_gender_by_asthma_status', 'U') IS NOT NULL
    DROP TABLE cii_qa.xtab_gender_by_asthma_status;

-- Create table
CREATE TABLE cii_qa.xtab_gender_by_asthma_status
(
    gender NVARCHAR(MAX),
    asthma_criteria1_count INT,
    asthma_criteria2_count INT,
    asthma_one_of_each_count INT,
    no_asthma_diagnoses_count INT
);

-- Insert data into the table
;WITH T0 AS (
    SELECT DISTINCT T1.patient_id 
    FROM hef_event AS T1
    JOIN hef_event AS T2 ON T1.patient_id = T2.patient_id AND T1.id <> T2.id
    WHERE T1.name = 'dx:asthma' 
    AND T2.name IN ('rx:albuterol', 'rx:levalbuterol', 'rx:pirbuterol', 'rx:arformoterol', 'rx:formoterol', 'rx:indacaterol', 'rx:salmeterol', 
					'rx:beclomethasone', 'rx:budesonide INH', 'rx:ciclesonide INH', 'rx:flunisolide INH', 'rx:fluticasone INH', 
					'rx:mometasone INH', 'rx:montelukast', 'rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium', 'rx:tiotropium', 'rx:cromolyn INH', 
					'rx:omalizumab', 'rx:fluticasone + salmeterol', 'rx:albuterol + ipratropium', 'rx:mometasone + formoterol', 'rx:budesonide + formoterol', 
					'rx:benralizumab', 'rx:mepolizumab', 'rx:reslizumab', 'rx:theophylline', 'rx:fluticasone + vilanterol')
    AND NOT EXISTS (
        SELECT * 
        FROM nodis_case c 
        WHERE c.patient_id = T1.patient_id AND c.condition = 'asthma'
    )
),
asthma AS (
    SELECT 
        ISNULL(NULLIF(p.gender, ''), 'UNK') AS gender,
        COUNT(*) AS counts
    FROM .emr_patient p
    JOIN .nodis_case nc ON p.id = nc.patient_id
    WHERE nc.criteria LIKE 'Criteria #1%' AND nc.condition = 'asthma'
    GROUP BY ISNULL(NULLIF(p.gender, ''), 'UNK')
),
asthma2 AS (
    SELECT 
        ISNULL(NULLIF(p.gender, ''), 'UNK') AS gender,
        COUNT(*) AS counts
    FROM .emr_patient p
    JOIN .nodis_case nc ON p.id = nc.patient_id
    WHERE nc.criteria LIKE 'Criteria #2%' AND nc.condition = 'asthma'
    GROUP BY ISNULL(NULLIF(p.gender, ''), 'UNK')
	),
asthma3 AS (
    SELECT
		ISNULL(NULLIF(p.gender, ''), 'UNK') AS gender,
		COUNT(*) AS counts
	FROM .emr_patient p
	JOIN T0 ON p.id = T0.patient_id
	GROUP BY ISNULL(NULLIF(p.gender, ''), 'UNK')
),
no_asthma AS (
    SELECT
		ISNULL(NULLIF(p.gender, ''), 'UNK') AS gender,
		COUNT(*) AS counts
	FROM emr_patient p
	WHERE NOT EXISTS (
		SELECT 1
		FROM nodis_case nc
		WHERE p.id = nc.patient_id AND nc.condition = 'asthma'
		)
	AND NOT EXISTS (SELECT 1 FROM T0 WHERE T0.patient_id = p.id)
	GROUP BY ISNULL(NULLIF(p.gender, ''), 'UNK')
)

INSERT INTO cii_qa.xtab_gender_by_asthma_status (gender, asthma_criteria1_count, asthma_criteria2_count, asthma_one_of_each_count, no_asthma_diagnoses_count)
SELECT 
    COALESCE(asthma.gender, asthma2.gender, asthma3.gender, no_asthma.gender) AS gender,
    ISNULL(asthma.counts, 0) AS asthma_criteria1_count,
    ISNULL(asthma2.counts, 0) AS asthma_criteria2_count,
    ISNULL(asthma3.counts, 0) AS asthma_one_of_each_count,
    ISNULL(no_asthma.counts, 0) AS no_asthma_diagnoses_count
FROM asthma
FULL OUTER JOIN asthma2 ON asthma.gender = asthma2.gender
FULL OUTER JOIN asthma3 ON COALESCE(asthma.gender, asthma2.gender) = asthma3.gender
FULL OUTER JOIN no_asthma ON COALESCE(asthma.gender, asthma2.gender, asthma3.gender) = no_asthma.gender;

-- Retrieve the data
SELECT * 
FROM cii_qa.xtab_gender_by_asthma_status 
ORDER BY gender;

