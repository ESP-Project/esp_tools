

/* MENDS Stage 2 Tests (pg gen)
 * Counts Grouped by Year tests ...
*/

/* This version of this script produces cii_qa.grouped_by_year_v015,
 * V0.15 was the last version in which Vitals Exclusions were not performed.
 * Use the results of this script to generate grouped_by_year_counts
 */


drop table if exists cii_qa.grouped_by_year_v015;
create table cii_qa.grouped_by_year_v015 
(test_name text,  esp_column text
, grouping_column_name text, year_min integer, year_max integer, year integer
, min float, p10 float, p25 float, median float, p75 float, p90 float, max float
, mean float, stddev float
, pct_null float, count integer);

/* emr_patient.age_yrs_derived */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_patient.age_yrs_derived' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by age_yrs_derived) as min
, percentile_disc(0.10) within group (order by age_yrs_derived) as p10
, percentile_disc(0.25) within group (order by age_yrs_derived) as p25
, percentile_disc(0.50) within group (order by age_yrs_derived) as median
, percentile_disc(0.75) within group (order by age_yrs_derived) as p75
, percentile_disc(0.90) within group (order by age_yrs_derived) as p90
, percentile_disc(1.0) within group (order by age_yrs_derived) as max
, avg(age_yrs_derived)  as mean
, stddev(age_yrs_derived) as stddev
, 100*((count(*) - count(age_yrs_derived))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from created_timestamp) as year
, trunc((current_date - cast(date_of_birth as date))/365.25)
as age_yrs_derived
from emr_patient
  where extract(year from created_timestamp) between 2015 and 9999
  -- and trunc((current_date - cast(date_of_birth as date))/365.25) between 0 and 999  -- Apply Vitals Exclusion
  and trunc((current_date - cast(date_of_birth as date))/365.25) is not null
) a
group by year
;

/* emr_encounter.temperature */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.temperature' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by temperature) as min
, percentile_disc(0.10) within group (order by temperature) as p10
, percentile_disc(0.25) within group (order by temperature) as p25
, percentile_disc(0.50) within group (order by temperature) as median
, percentile_disc(0.75) within group (order by temperature) as p75
, percentile_disc(0.90) within group (order by temperature) as p90
, percentile_disc(1.0) within group (order by temperature) as max
, avg(temperature)  as mean
, stddev(temperature) as stddev
, 100*((count(*) - count(temperature))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, temperature
from emr_encounter
  where extract(year from date) between 2015 and 9999
  -- and temperature between 68 and 113  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and temperature is not null
) a
group by year
;

/* emr_encounter.weight */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.weight' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by weight) as min
, percentile_disc(0.10) within group (order by weight) as p10
, percentile_disc(0.25) within group (order by weight) as p25
, percentile_disc(0.50) within group (order by weight) as median
, percentile_disc(0.75) within group (order by weight) as p75
, percentile_disc(0.90) within group (order by weight) as p90
, percentile_disc(1.0) within group (order by weight) as max
, avg(weight)  as mean
, stddev(weight) as stddev
, 100*((count(*) - count(weight))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, weight
from emr_encounter
  where extract(year from date) between 2015 and 9999
  --and weight between 0.01 and 399.99  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and weight is not null
) a
group by year
;

/* emr_encounter.height */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.height' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by height) as min
, percentile_disc(0.10) within group (order by height) as p10
, percentile_disc(0.25) within group (order by height) as p25
, percentile_disc(0.50) within group (order by height) as median
, percentile_disc(0.75) within group (order by height) as p75
, percentile_disc(0.90) within group (order by height) as p90
, percentile_disc(1.0) within group (order by height) as max
, avg(height)  as mean
, stddev(height) as stddev
, 100*((count(*) - count(height))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, height
from emr_encounter
  where extract(year from date) between 2015 and 9999
  -- and height between 0.01 and 249.99  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and height is not null
) a
group by year
;

/* emr_encounter.bp_systolic */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bp_systolic' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by bp_systolic) as min
, percentile_disc(0.10) within group (order by bp_systolic) as p10
, percentile_disc(0.25) within group (order by bp_systolic) as p25
, percentile_disc(0.50) within group (order by bp_systolic) as median
, percentile_disc(0.75) within group (order by bp_systolic) as p75
, percentile_disc(0.90) within group (order by bp_systolic) as p90
, percentile_disc(1.0) within group (order by bp_systolic) as max
, avg(bp_systolic)  as mean
, stddev(bp_systolic) as stddev
, 100*((count(*) - count(bp_systolic))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, bp_systolic
from emr_encounter
  where extract(year from date) between 2015 and 9999
  --and bp_systolic between 30.01 and 299.99  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bp_systolic is not null
) a
group by year
;

/* emr_encounter.bp_diastolic */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bp_diastolic' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by bp_diastolic) as min
, percentile_disc(0.10) within group (order by bp_diastolic) as p10
, percentile_disc(0.25) within group (order by bp_diastolic) as p25
, percentile_disc(0.50) within group (order by bp_diastolic) as median
, percentile_disc(0.75) within group (order by bp_diastolic) as p75
, percentile_disc(0.90) within group (order by bp_diastolic) as p90
, percentile_disc(1.0) within group (order by bp_diastolic) as max
, avg(bp_diastolic)  as mean
, stddev(bp_diastolic) as stddev
, 100*((count(*) - count(bp_diastolic))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, bp_diastolic
from emr_encounter
  where extract(year from date) between 2015 and 9999
  -- and bp_diastolic between 20.01 and 149.99  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bp_diastolic is not null
) a
group by year
;

/* emr_encounter.bmi */
insert into cii_qa.grouped_by_year_v015 (
  test_name, esp_column, grouping_column_name, year_min, year_max, year
, min, p10, p25, median, p75, p90, max
, mean, stddev, pct_null, count)
select
 'grouped by year' as test_name
, 'emr_encounter.bmi' as esp_column
, 'year of record creation' as grouping_column_mame
, 2015 as year_min, 9999 as year_max
, year
, percentile_disc(0.0) within group (order by bmi) as min
, percentile_disc(0.10) within group (order by bmi) as p10
, percentile_disc(0.25) within group (order by bmi) as p25
, percentile_disc(0.50) within group (order by bmi) as median
, percentile_disc(0.75) within group (order by bmi) as p75
, percentile_disc(0.90) within group (order by bmi) as p90
, percentile_disc(1.0) within group (order by bmi) as max
, avg(bmi)  as mean
, stddev(bmi) as stddev
, 100*((count(*) - count(bmi))/count(*)) as pct_null
  , count(*) as count
from (
select 
  extract(year from date) as year
, bmi
from emr_encounter
  where extract(year from date) between 2015 and 9999
  -- and bmi between 12 and 70  -- Apply Vitals Exclusion
  and date between to_date('2015-01-01', 'yyyy-mm-dd') and current_date  -- Apply exclusion of dates before 1/1/2015 and after current date for encounter table.
  and bmi is not null
) a
group by year
;

select * from cii_qa.grouped_by_year_v015
order by esp_column, year;
/* ...end of Counts Grouped by Year tests */


