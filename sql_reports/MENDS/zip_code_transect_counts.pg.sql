select count( distinct id) as pat_counts, 
  zip, gender, race, ethnicity
from (
select t0.id,
  coalesce(t0.zip,'NULL') as zip,
  coalesce(t1.mapped_value, 'NULL') as gender,
  coalesce(t2.mapped_value, 'NULL') as race,
  coalesce(t3.mapped_value, 'NULL') as ethnicity
from emr_patient t0
join gen_pop_tools.tt_pat_seq_enc t00 on t0.id=t00.patient_id
  and t00.year_month like '2024%' 
  and t00.prior2>0
left join gen_pop_tools.rs_conf_mapping t1 on t1.src_value=t0.gender and t1.src_field='gender'
left join gen_pop_tools.rs_conf_mapping t2 on t2.src_value=t0.race and t2.src_field='race'
left join gen_pop_tools.rs_conf_mapping t3 on t3.src_value=t0.ethnicity and t3.src_field='ethnicity') t000
group by grouping sets (
  (zip), 
  (zip, gender), 
  (zip, race),
  (zip, ethnicity));