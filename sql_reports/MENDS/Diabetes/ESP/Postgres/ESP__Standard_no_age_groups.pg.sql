--this gets all patients with type1 and type2 diabetes as identified by the ESP diabetes plugin algorithm.
select patient_id, date, condition
into temporary esp_diab
from nodis_case c
where condition in ('diabetes:type-1','diabetes:type-2')
  and (date<='2022-12-31'::date
       or exists
             (select null
              from nodis_case_events ce
              join hef_event h on h.id=ce.event_id
              where ce.case_id=c.id and h.name<>'rx:metformin' and h.date<='2022-12-31'::date)
       or exists
             (select null from nodis_case_events ce
              join hef_event h on h.id=ce.event_id
              where ce.case_id=c.id and c.condition='diabetes:type-1'
                and h.date<='2022-12-31'::date
		and h.name in ('lx:a1c:threshold:gte:6.5',
                               'lx:glucose-fasting:threshold:gte:126',
                               'lx:ogtt50-fasting:threshold:gte:126',
                               'lx:ogtt75-fasting:threshold:gte:126',
                               'lx:ogtt100-fasting:threshold:gte:126',
                               'rx:glyburide',
                               'rx:gliclazide',
                               'rx:glipizide',
                               'rx:glimepiride',
                               'rx:pioglitazone',
                               'rx:rosiglitizone',
                               'rx:repaglinide',
                               'rx:nateglinide',
                               'rx:meglitinide',
                               'rx:sitagliptin',
                               'rx:exenatide',
                               'rx:pramlintide',
                               'rx:insulin'))
         or exists
               (select null
                from nodis_case_events ce
                join hef_event h on h.id=ce.event_id
                where ce.case_id=c.id and h.date<='2022-12-31'::date
                and h.name like 'dx:diabetes:all-types'
                group by patient_id having count(*)>1)
           );


select distinct on (patient_id) patient_id, date, name
into temporary hef_a1c
from hef_event
where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7','lx:a1c:range:gte:7:lt:8')
order by patient_id, date desc,
  case
    when name='lx:a1c:threshold:lt:7' then 1
    when name='lx:a1c:range:gte:7:lt:8' then 2
    when name='lx:a1c:range:gte:8:lte:9' then 3
    when name='lx:a1c:threshold:gt:9' then 4
  end;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id,
       DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) as AGE,
       case
         when DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 17 then '4-17'
         when DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 18 and 44 then '18-44'
         when DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 45 and 64 then '45-64'
         else '65+'
       end as age_group
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, coalesce(condition, 'Not diabetic'::varchar(50)) condition,
  case
    when condition is null and name is not null and name <> 'lx:a1c:threshold:lt:7' then 'Not tested'
    else coalesce(name,'Not tested')
  end control
into temporary diab_assessed_control
from pat_w_age t01
left join esp_diab t0 on t0.patient_id=t01.patient_id
left join last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;


select count(*) as counts, condition, control
from diab_assessed_control dac
join pat_w_age pwa on dac.patient_id = pwa.patient_id
group by condition, control
order by condition,
  case
    when control='lx:a1c:threshold:lt:7' then 1
    when control='lx:a1c:range:gte:7:lt:8' then 2
    when control='lx:a1c:range:gte:8:lte:9' then 3
    when control='lx:a1c:threshold:gt:9' then 4
    when control='Not tested' then 5
  end;
