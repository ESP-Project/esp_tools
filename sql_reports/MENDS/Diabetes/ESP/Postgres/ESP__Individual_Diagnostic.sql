DROP FUNCTION IF EXISTS check_patient_results;
DROP FUNCTION IF EXISTS check_type1_results;

CREATE OR REPLACE FUNCTION check_patient_results(input_patient_id INTEGER)
RETURNS TABLE
(
    patient_mrn INTEGER,
    a1c_found BOOLEAN,
    fasting_glucose_found BOOLEAN,
    random_glucose_found BOOLEAN,
    outpatient_code_found BOOLEAN,
    insulin_outside_preg_found BOOLEAN,
    ogtt_found BOOLEAN,
    anti_hyperglycemic_medication_found BOOLEAN
) AS $$
BEGIN
    -- Check if the patient has NO inpatient visits
    IF NOT EXISTS (
        SELECT 1
        FROM emr_encounter AS enc
        JOIN gen_pop_tools.rs_conf_mapping AS gpt ON enc.raw_encounter_type = gpt.src_value AND gpt.src_field = 'raw_encounter_type'
        WHERE enc.patient_id = input_patient_id AND gpt.mapped_value = 'inpatient'
    ) THEN 
        RETURN QUERY
        SELECT
            h.patient_id AS patient_mrn,
            COALESCE(MAX(CASE WHEN h.name = 'lx:a1c:threshold:gte:6.5' THEN 1 ELSE 0 END), 0) = 1 AS a1c_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:ogtt50-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS ogtt_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS fasting_glucose_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-random:threshold:gte:200' THEN 1 ELSE 0 END), 0) = 1 AS random_glucose_found,
            COALESCE(MAX(CASE WHEN oc.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS outpatient_code_found,
            COALESCE(MAX(CASE WHEN i.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS insulin_outside_preg_found,
            COALESCE(MAX(CASE WHEN h.name IN (
                'rx:metformin',
                'rx:glyburide',
                'rx:gliclazide',
                'rx:glipizide',
                'rx:glimepiride',
                'rx:pioglitazone',
                'rx:rosiglitizone',
                'rx:repaglinide',
                'rx:nateglinide',
                'rx:meglitinide',
                'rx:sitagliptin',
                'rx:exenatide',
                'rx:pramlintide',
                'rx:miglitol'
            ) THEN 1 ELSE 0 END), 0) = 1 AS anti_hyperglycemic_medication_found
        FROM
            hef_event h
        LEFT JOIN (
            SELECT DISTINCT
                patient_id
            FROM
                hef_event
            WHERE
                name IN (
                    'dx:diabetes:type-1-uncontrolled',
                    'dx:diabetes:unspecified',
                    'dx:diabetes:other',
                    'dx:diabetes:type-1-not-stated-icd9',
                    'dx:diabetes:type-1-not-stated-icd10',
                    'dx:diabetes:type-2-not-stated-icd9',
                    'dx:diabetes:type-2-not-stated-icd10',
                    'dx:diabetes:all-types',
                    'dx:diabetes:type-2-uncontrolled'
                )
        ) AS oc ON h.patient_id = oc.patient_id
        LEFT JOIN (
            SELECT DISTINCT
                pre.patient_id
            FROM
                emr_prescription AS pre
            JOIN
                static_drugsynonym AS syn
            ON
                lower(pre.name) LIKE CONCAT('%', lower(syn.other_name), '%')
            WHERE
                lower(syn.generic_name) = 'insulin'
                AND NOT EXISTS (
                    SELECT NULL FROM hef_timespan AS ts 
                    WHERE ts.patient_id = pre.patient_id 
                    AND ts.name = 'pregnancy'
                    AND pre.date BETWEEN ts.start_date 
                    AND COALESCE(ts.end_date, NOW())
                )
        ) AS i ON h.patient_id = i.patient_id
        WHERE
            h.patient_id = input_patient_id
        GROUP BY
            h.patient_id;
    ELSE
        -- If the patient has inpatient visits, return NULL values for all fields
        RETURN QUERY
        SELECT
            input_patient_id AS patient_mrn,
            false AS a1c_found,
            false AS ogtt_found,
            false AS fasting_glucose_found,
            false AS random_glucose_found,
            false AS outpatient_code_found,
            false AS insulin_outside_preg_found,
            false AS anti_hyperglycemic_medication_found;
    END IF;

    RETURN;
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS check_patient_results;
DROP FUNCTION IF EXISTS check_type1_results;

CREATE OR REPLACE FUNCTION check_patient_results(input_patient_id INTEGER)
RETURNS TABLE
(
    patient_mrn INTEGER,
    a1c_found BOOLEAN,
    fasting_glucose_found BOOLEAN,
    random_glucose_found BOOLEAN,
    outpatient_code_found BOOLEAN,
    insulin_outside_preg_found BOOLEAN,
    ogtt_found BOOLEAN,
    anti_hyperglycemic_medication_found BOOLEAN
) AS $$
BEGIN
    -- Check if the patient has NO inpatient visits
    IF NOT EXISTS (
        SELECT 1
        FROM emr_encounter AS enc
        JOIN gen_pop_tools.rs_conf_mapping AS gpt ON enc.raw_encounter_type = gpt.src_value AND gpt.src_field = 'raw_encounter_type'
        WHERE enc.patient_id = input_patient_id AND gpt.mapped_value = 'inpatient'
    ) THEN 
        RETURN QUERY
        SELECT
            h.patient_id AS patient_mrn,
            COALESCE(MAX(CASE WHEN h.name = 'lx:a1c:threshold:gte:6.5' THEN 1 ELSE 0 END), 0) = 1 AS a1c_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:ogtt50-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS ogtt_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-fasting:threshold:gte:126' THEN 1 ELSE 0 END), 0) = 1 AS fasting_glucose_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:glucose-random:threshold:gte:200' THEN 1 ELSE 0 END), 0) = 1 AS random_glucose_found,
            COALESCE(MAX(CASE WHEN oc.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS outpatient_code_found,
            COALESCE(MAX(CASE WHEN i.patient_id IS NOT NULL THEN 1 ELSE 0 END), 0) = 1 AS insulin_outside_preg_found,
            COALESCE(MAX(CASE WHEN h.name IN (
                'rx:metformin',
                'rx:glyburide',
                'rx:gliclazide',
                'rx:glipizide',
                'rx:glimepiride',
                'rx:pioglitazone',
                'rx:rosiglitizone',
                'rx:repaglinide',
                'rx:nateglinide',
                'rx:meglitinide',
                'rx:sitagliptin',
                'rx:exenatide',
                'rx:pramlintide',
                'rx:miglitol'
            ) THEN 1 ELSE 0 END), 0) = 1 AS anti_hyperglycemic_medication_found
        FROM
            hef_event h
        LEFT JOIN (
            SELECT DISTINCT
                patient_id
            FROM
                hef_event
            WHERE
                name IN (
                    'dx:diabetes:type-1-uncontrolled',
                    'dx:diabetes:unspecified',
                    'dx:diabetes:other',
                    'dx:diabetes:type-1-not-stated-icd9',
                    'dx:diabetes:type-1-not-stated-icd10',
                    'dx:diabetes:type-2-not-stated-icd9',
                    'dx:diabetes:type-2-not-stated-icd10',
                    'dx:diabetes:all-types',
                    'dx:diabetes:type-2-uncontrolled'
                )
        ) AS oc ON h.patient_id = oc.patient_id
        LEFT JOIN (
            SELECT DISTINCT
                pre.patient_id
            FROM
                emr_prescription AS pre
            JOIN
                static_drugsynonym AS syn
            ON
                lower(pre.name) LIKE CONCAT('%', lower(syn.other_name), '%')
            WHERE
                lower(syn.generic_name) = 'insulin'
                AND NOT EXISTS (
                    SELECT NULL FROM hef_timespan AS ts 
                    WHERE ts.patient_id = pre.patient_id 
                    AND ts.name = 'pregnancy'
                    AND pre.date BETWEEN ts.start_date 
                    AND COALESCE(ts.end_date, NOW())
                )
        ) AS i ON h.patient_id = i.patient_id
        WHERE
            h.patient_id = input_patient_id
        GROUP BY
            h.patient_id;
    ELSE
        -- If the patient has inpatient visits, return NULL values for all fields
        RETURN QUERY
        SELECT
            input_patient_id AS patient_mrn,
            false AS a1c_found,
            false AS ogtt_found,
            false AS fasting_glucose_found,
            false AS random_glucose_found,
            false AS outpatient_code_found,
            false AS insulin_outside_preg_found,
            false AS anti_hyperglycemic_medication_found;
    END IF;

    RETURN;
END;
$$ LANGUAGE plpgsql;

-- Define a function to check for Type-1 Diabetes factors
CREATE OR REPLACE FUNCTION check_type1_results(input_patient_id INTEGER)
RETURNS TABLE
(
    patient_mrn INTEGER,
    cpeptide_found BOOLEAN,
    ab1_found BOOLEAN,
    ab2_found BOOLEAN,
    ab3_found BOOLEAN,
    ab4_found BOOLEAN,
    ab6_found BOOLEAN,
    acetone_found BOOLEAN,
    has_type1_diabetes BOOLEAN,
    -- Add more columns for additional Type-1 Diabetes factors as needed
    ratio_greater_than_50 BOOLEAN,
    glucagon_prescription_found BOOLEAN
) AS $$
DECLARE
    ratio float;
BEGIN
    -- Check if the patient is not an inpatient
    IF NOT EXISTS (
        SELECT 1
        FROM emr_encounter AS enc
        JOIN gen_pop_tools.rs_conf_mapping AS gpt ON enc.raw_encounter_type = gpt.src_value AND gpt.src_field = 'raw_encounter_type'
        WHERE enc.patient_id = input_patient_id AND gpt.mapped_value = 'inpatient'
    ) THEN
        RETURN QUERY
        SELECT
            h.patient_id AS patient_mrn,
            COALESCE(MAX(CASE WHEN h.name = 'lx:c-peptide:threshold:lt:0.8' THEN 1 ELSE 0 END), 0) = 1 AS cpeptide_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:gad65:threshold:gt:1' THEN 1 ELSE 0 END), 0) = 1 AS ab1_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:ica512:threshold:gt:0.8' THEN 1 ELSE 0 END), 0) = 1 AS ab2_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:islet-cell-antibody:positive' THEN 1 ELSE 0 END), 0) = 1 AS ab3_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:islet-cell-antibody:threshold:gte:1.25' THEN 1 ELSE 0 END), 0) = 1 AS ab4_found,
            COALESCE(MAX(CASE WHEN h.name = 'lx:insulin-antibody:threshold:gt:0.8' THEN 1 ELSE 0 END), 0) = 1 AS ab6_found,
            COALESCE(MAX(CASE WHEN h.name = 'rx:acetone' THEN 1 ELSE 0 END), 0) = 1 AS acetone_found
            -- Add more columns for additional Type-1 Diabetes factors as needed
        FROM
            hef_event h
        LEFT JOIN (
            SELECT DISTINCT
                patient_id
            FROM
                hef_event
            WHERE
                name IN (
                    'dx:diabetes:type-1-uncontrolled',
                    'dx:diabetes:unspecified',
                    'dx:diabetes:other',
                    'dx:diabetes:type-1-not-stated-icd9',
                    'dx:diabetes:type-1-not-stated-icd10',
                    'dx:diabetes:type-2-not-stated-icd9',
                    'dx:diabetes:type-2-not-stated-icd10',
                    'dx:diabetes:all-types',
                    'diabetes:type-2-uncontrolled'
                )
        ) AS oc ON h.patient_id = oc.patient_id
        LEFT JOIN (
            SELECT DISTINCT
                pre.patient_id
            FROM
                emr_prescription AS pre
            JOIN
                static_drugsynonym AS syn
            ON
                lower(pre.name) LIKE CONCAT('%', lower(syn.other_name), '%')
            WHERE
                lower(syn.generic_name) = 'insulin'
                AND NOT EXISTS (
                    SELECT NULL FROM hef_timespan AS ts 
                    WHERE ts.patient_id = pre.patient_id 
                    AND ts.name = 'pregnancy'
                    AND pre.date BETWEEN ts.start_date 
                    AND COALESCE(ts.end_date, NOW())
                )
        ) AS i ON h.patient_id = i.patient_id;

        -- Logic for checking ratio greater than 50% and glucagon prescription
        SELECT
            t1.patient_id,
            COALESCE(t2.type2_count, 0) AS type2_count,
            (t1.type1_count::float / NULLIF(t1.type1_count + COALESCE(t2.type2_count, 0), 0)) AS ratio
        INTO
            patient_mrn,
            ratio_greater_than_50
        FROM
            (
                SELECT
                    patient_id,
                    COUNT(*) as type1_count
                FROM
                    hef_event
                WHERE
                    name = ANY(ARRAY['dx:diabetes:type-1-uncontrolled', 'dx:diabetes:type-1-not-stated-icd9','dx:diabetes:type-1-not-stated-icd10'])
                GROUP BY
                    patient_id
            ) AS t1
        LEFT JOIN
            (
                SELECT
                    patient_id,
                    COUNT(*) as type2_count
                FROM
                    hef_event
                WHERE
                    name = ANY(ARRAY['dx:diabetes:type-2-not-stated-icd9', 'dx:diabetes:type-2-not-stated-icd10','diabetes:type-2-uncontrolled'])
                GROUP BY
                    patient_id
            ) AS t2
        ON
            t1.patient_id = t2.patient_id
        WHERE
            t1.patient_id = input_patient_id;

        glucagon_prescription_found := EXISTS (
            SELECT 1
            FROM hef_event h
            WHERE 
                h.patient_id = input_patient_id 
                AND 
                h.name = 'rx:glucagon'
                AND
                NOT EXISTS (
                    SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
                )
        );

        -- Check if the patient meets the criteria for Type-1 Diabetes
        has_type1_diabetes := ratio_greater_than_50 AND glucagon_prescription_found;

    ELSE
        -- If the patient is an inpatient, return NULL values for all fields
        RETURN QUERY
        SELECT
            input_patient_id AS patient_mrn,
            false AS cpeptide_found,
            false AS ab1_found,
            false AS ab2_found,
            false AS ab3_found,
            false AS ab4_found,
            false AS ab6_found,
            false AS acetone_found,
            false AS has_type1_diabetes,
            false AS ratio_greater_than_50,
            false AS glucagon_prescription_found
            -- Add more columns for additional Type-1 Diabetes factors as needed
        ;
    END IF;

    RETURN;
END;
$$ LANGUAGE plpgsql;

