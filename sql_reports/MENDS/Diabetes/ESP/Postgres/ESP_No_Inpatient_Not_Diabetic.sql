/*Last Revision: 9/20/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Added row labels
	V7				RJA			 Added filtering to show total number of non-diabetic patients
	V8				RJA			 Updated inpatient/no-inpatient logic to no longer be inverted; reworked fiftypercent_glucagon
								 and fiftypercent_nohypoglycemic code.
	V9 				RJA			 Combined Standard Algorithm and No_Inpatient algorithm into a single file.  Renamed tables
								 so that the no_inpatient tables are saving to their own tables, in the interest of doing 										 side-by-side comparisons of data.
	V10				RJA			 Implemented OGTT Hef Events into criteria for Frank; Reworked ICD codes; Reworked Acetone and 									 CPeptide so that they should now be included correctly into counts.
	V11				RJA			 Reworked type1:type2 > 50% functionality.  Added logic that should resolve an issue where
								 a large number of patients were being identified as Frank Diabetic but not Type-1 or Type-2.
								 
PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/


/**********************************
* Step 0: Identify inpatient days *
**********************************/
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS a1c_results;
	DROP TABLE IF EXISTS fasting_glucose;
	DROP TABLE IF EXISTS random_glucose;
	DROP TABLE IF EXISTS medication_disp;
	DROP TABLE IF EXISTS outpatient_code;
	DROP TABLE IF EXISTS frank_diabetes;
	DROP TABLE IF EXISTS ab1;
 	DROP TABLE IF EXISTS ab2;
	DROP TABLE IF EXISTS ab3;
	DROP TABLE IF EXISTS acetone;
	DROP TABLE IF EXISTS fiftypercent_nohypo;
	DROP TABLE IF EXISTS Frank_Diabetes;
	DROP TABLE IF EXISTS fiftypercent_glucagon;
	DROP TABLE IF EXISTS Type1_Summary;
	DROP TABLE IF EXISTS Type2_Summary;
	DROP TABLE IF EXISTS final_counts;
	DROP TABLE IF EXISTS inpat_days;
	DROP TABLE IF EXISTS insulin_disp;
	DROP TABLE IF EXISTS cpeptide;
	DROP TABLE IF EXISTS ab4;
	DROP TABLE IF EXISTS ab5;
	DROP TABLE IF EXISTS ab6;
	DROP TABLE IF EXISTS ogtt;
	DROP TABLE IF EXISTS esp_diab_noin;
	DROP TABLE IF EXISTS hef_a1c_gte_noin;
	DROP TABLE IF EXISTS hef_a1c_lt_noin;
	DROP TABLE IF EXISTS hef_a1c_noin;
	DROP TABLE IF EXISTS last_hef_a1c_noin;
	DROP TABLE IF EXISTS pat_w_age_noin;
	DROP TABLE IF EXISTS diab_assessed_control_noin;
	DROP TABLE IF EXISTS final_noin_counts;
SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO TEMPORARY 
	inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';

/***************************************
* Step 1:  Classify as Frank Diabetes  *
***************************************/

--a1c where glucose >= 6.5
--uncomment 'l.result_float < 6.5' to obtain results for patients where a1c is under 6.5

--drop table a1c_results;
select 
	patient_id,
	date,
	'a1c' as event_type
into temporary
	a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

--fasting plasma glucose concentration >= 126 mg/dl
--drop table fasting_glucose;
select 
	patient_id,
	date,
	'fasting glucose' as event_type
into temporary
	fasting_glucose
from
	hef_event h 
WHERE 
	name = 'lx:glucose-fasting:threshold:gte:126'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

--random plasma glucose concentration >= 200 mg/dl
--drop table random_glucose;
select 
	patient_id,
	date,
	'random glucose' as event_type
into temporary
	random_glucose
from
	hef_event h 
where 
	name = 'lx:glucose-random:threshold:gte:200'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

--outpatient diagnosis code for diabetes
--drop table outpatient_code;	
SELECT --distinct
	h.patient_id, 
	h.date,
	'dx' as event_type
INTO TEMPORARY
	outpatient_code
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types')
AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

--any dispensation of antihyperglycemic medication
--drop table medication_disp;
SELECT patient_id, 
	date,
	name
INTO TEMPORARY
	medication_disp
FROM 
	hef_event AS h 
WHERE 
	lower(name)
	IN 
	(
	'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
	)
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);


--need metformin for error in diabetes plugin
--drop table metformin_disp;
SELECT patient_id,
        date,
        name
INTO TEMPORARY
        metformin_disp
FROM
        hef_event AS h
WHERE
        lower(name)='rx:metformin'
        AND NOT EXISTS
        (SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

	
--Insulin outside of pregnancy
--drop table insulin_disp;
SELECT 
	patient_id, 
	name, 
	date
INTO TEMPORARY
	insulin_disp
FROM 
	hef_event AS h 
WHERE name='rx:insulin' 
	AND NOT EXISTS 
	(SELECT NULL FROM hef_timespan AS ts 
	 	WHERE ts.patient_id = h.patient_id 
		AND ts.name = 'pregnancy'
		AND h.date > ts.start_date 
		AND h.date < COALESCE(ts.end_date, NOW()))
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
--ogtt fasting
--drop table ogtt_fasting;
SELECT 
	patient_id, 
	name, 
	date
INTO TEMPORARY
	ogtt_fasting
FROM 
	hef_event AS h 
WHERE name in ('lx:ogtt50-fasting:threshold:gte:126','lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126')
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
--ogtt random
--drop table ogtt_random;
SELECT 
	patient_id, 
	name, 
	date
INTO TEMPORARY
	ogtt_random
FROM 
	hef_event AS h 
WHERE name = 'lx:ogtt50-random:threshold:gte:200'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- age calculation    
select id as patient_id, DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);
    
-- Union all tables into a single collection of Frank Diabetic patients
--drop table frank_diabetes;
WITH t1
AS
(SELECT 
 	patient_id, date 
 FROM 
 	a1c_results),
t2 AS
(SELECT 
 	patient_id, date 
 FROM 
 	fasting_glucose),
t0_3 AS
(SELECT 
 	patient_id, 
 	row_number() OVER(PARTITION BY patient_id ORDER BY patient_id, date) AS row_n, 
 	date 
 FROM 
 	random_glucose),
t3 AS
(SELECT 
 	patient_id, date 
 FROM 
 	t0_3 
 WHERE 
 	row_n = 2),
t4 AS
(SELECT 
 	patient_id, date 
 FROM 
 	insulin_disp),
--t0_5 AS
--(SELECT 
-- 	patient_id, 
-- 	row_number() OVER(partition by patient_id ORDER BY date) as row_n, 
-- 	date
-- FROM 
-- 	outpatient_code),
t5 AS
(SELECT 
 	patient_id, min(date) as date
 FROM
 	outpatient_code
 group by patient_id 
 having count(*)>1 and min(date) <= '2023-12-31'::Date),
--(SELECT 
-- 	patient_id, date 
-- FROM
-- 	t0_5 
-- WHERE 
-- 	row_n = 2),
t6 AS
(
 SELECT 
	patient_id, date 
 FROM 
	medication_disp),
t7 AS
(SELECT
	patient_id, date
FROM
	ogtt_fasting
),
t0_8 AS
(
 SELECT
    patient_id,
    row_number() OVER(partition by patient_id ORDER BY date) as row_n,
    date
 FROM
    ogtt_random),
t8 AS
(SELECT
    patient_id, date
 FROM
    t0_8
 WHERE
    row_n = 2),
t9 AS
(SELECT 
 	patient_id, date, 'a1c' as dtype
 FROM 
 	t1
 UNION
 SELECT 
 	patient_id, date, 'fasting glucose' as dtype
 FROM 
 	t2
 UNION
 SELECT 
 	patient_id, date, 'random glucose' as dtype
 FROM t3
UNION
SELECT 
 	patient_id, date, 'insulin' as dtype 
 FROM 
 	t4
UNION
SELECT 
 	patient_id, date, 'diabetes dx' as dtype
FROM 
 	t5
UNION
SELECT 
 	patient_id, date, 'meds' as dtype
FROM 
 	t6
UNION
SELECT
	patient_id, date, 'fasting ogtt' as dtype
FROM
	t7
UNION
SELECT
	patient_id, date, 'random ogtt' as dtype
FROM
	t8
)
SELECT 
	distinct on (t9.patient_id)
	t9.patient_id, t9.date, t9.dtype
INTO 
	frank_diabetes
FROM 
	t9 
join pat_w_age pwa on t9.patient_id=pwa.patient_id
where t9.date<='2022-12-31'::date
 --metformin error rule
      or exists (select null from metformin_disp md
	         where md.date<='2022-12-31'::date
		  and md.patient_id=t9.patient_id)
order BY t9.patient_id, t9.date ;

/**************************************************
* STEP 2: Differentiate Type 1 or Type 2 Diabetes *
**************************************************/

--C-peptide test <0.8

SELECT 
	patient_id, 
	date
INTO TEMPORARY 
	cpeptide
FROM 
	hef_event
WHERE 
	name = 'lx:c-peptide:threshold:lt:0.8'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = patient_id AND date BETWEEN indays.start_date AND indays.end_date);
	
--Diabetes auto-antibodies positive

	--> Antibodies #1:  GAD-65 AB > 1.0
	select 
		count(*),
		patient_id,
		date,
		'ab1' as event_type
	into temporary
		ab1
	from
		hef_event h
	where 
		name = 'lx:gad65:threshold:gt:1'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
	--> Antibodies #2:  ICA-512 Autoantibodies > 0.8
	select 
		count(*),
		patient_id,
		date,
		'ab2' as event_type
	into temporary
		ab2
	from
		hef_event h
	where 
		name = 'lx:ica512:threshold:gt:0.8'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;

	-- Antibodies #3:  Query to separate numeric values from titre string and return true if titre ratio is greater than 1:4
	CREATE TEMPORARY TABLE 
		ab3 
	AS
	SELECT 
		patient_id, 
		date,
		'ab3' as event_type
	FROM 
		hef_event h
	WHERE 
		name = 'lx:islet-cell-antibody:positive'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
	--> Antibodies #4:  Islet Cell Antibody Titer >= 1.25*/
	select 
		count(*),
		patient_id,
		date,
		'ab4' as event_type
	into temporary
		ab4
	from
		hef_event h
	where 
		name = 'lx:islet-cell-antibody:threshold:gte:1.25'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
		
	/*--> Antibodies #5:  Insulin Auto Antibody >= .4
	CREATE TEMPORARY TABLE 
		ab5 
	AS SELECT 
		l.patient_id, 
		l.date, 
		l.result_float
	FROM 
		emr_labresult l 
	JOIN 
		conf_labtestmap ltm 
	ON 
		ltm.native_code = l.native_code  
	WHERE 
		ltm.test_name = 'insulin-antibody' 
	AND 
		l.result_float >= 0.4
	AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = l.patient_id AND l.date BETWEEN indays.start_date AND indays.end_date);*/
	-->Antibodies #6:  Insulin AB >= .8 
	select 
		count(*),
		patient_id,
		date,
		'ab6' as event_type
	into temporary
		ab6
	from
		hef_event h
	where 
		name = 'lx:insulin-antibody:threshold:gt:0.8'
		AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
	group by 
		patient_id,
		date;
		
--Prescription for URINE ACETONE TEST STRIPS (search on keyword: ACETONE)

SELECT 
	patient_id, 
	date, 
	'acetone' as event_type
INTO TEMPORARY
	acetone
FROM 
	hef_event h
WHERE
	name = 'rx:acetone'
	AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY
	patient_id,
	date;

--50% ratio and glucagon prescription

CREATE TEMP TABLE fiftypercent_glucagon (
	patient_id INTEGER,
	date date
);
	---Count of Type-1 codes for each patient
	WITH Type1_Count AS (
	SELECT patient_id, date, COUNT(*) as type1_count
	FROM hef_event	
	WHERE name = ANY(ARRAY['dx:diabetes:type-1-uncontrolled', 'dx:diabetes:type-1-not-stated-icd9','dx:diabetes:type-1-not-stated-icd10'])
	GROUP BY patient_id, date
	),
	---Count of Type-2 codes for each patient
	Type2_Count AS (
	SELECT patient_id, date, COUNT(*) as type2_count
	FROM hef_event
	WHERE name = ANY(ARRAY['dx:diabetes:type-2-not-stated-icd9', 'dx:diabetes:type-2-not-stated-icd10','diabetes:type-2-uncontrolled'])
	GROUP BY patient_id, date
	),
	---Calculate Ratios
	Ratios AS (
		SELECT
			t1.patient_id,
			t1.date,
			t1.type1_count,
			COALESCE(t2.type2_count, 0) AS type2_count,
			(t1.type1_count::float / NULLIF(t1.type1_count + COALESCE(t2.type2_count, 0), 0)) AS ratio
		FROM
			Type1_Count t1 
		LEFT JOIN 
			Type2_Count t2
		ON
			t1.patient_id = t2.patient_id
	)
	INSERT INTO 
		fiftypercent_glucagon(patient_id, date)
	SELECT 
		r.patient_id,
		r.date
	FROM 
		Ratios r 
	WHERE 
		r.ratio > 0.5 AND exists (
			SELECT 1
			FROM hef_event h
			WHERE 
				h.patient_id = r.patient_id 
				AND 
				h.name = 'rx:glucagon'
				AND
				NOT EXISTS
				(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
		);

--50% ratio and no oral hypoglycemic

	--- Create a temporary table for storing patients with > 50% Type 1 and no anti-hypoglycemic medications
		CREATE TEMP TABLE fiftypercent_nohypo (
  		patient_id INTEGER,
  		date date
		);

	--- Count of Type-1 codes for each patient
	WITH Type1_Count AS (
  		SELECT patient_id, date, COUNT(*) as type1_count
  		FROM hef_event  
  		WHERE name = ANY (ARRAY['dx:diabetes:type-1-uncontrolled', 'dx:diabetes:type-1-not-stated-icd9','dx:diabetes:type-1-not-stated-icd10'])
		GROUP BY patient_id, date
	),
	--- Count of Type-2 codes for each patient
	Type2_Count AS (
  		SELECT patient_id, date, COUNT(*) as type2_count
  		FROM hef_event
  		WHERE name = ANY (ARRAY['dx:diabetes:type-2-not-stated-icd9', 'dx:diabetes:type-2-not-stated-icd10','diabetes:type-2-uncontrolled'])
  		GROUP BY patient_id, date
),
	--- Calculate Ratios
	Ratios AS (
  		SELECT
    		t1.patient_id,
    		t1.date,
    		t1.type1_count,
    	COALESCE(t2.type2_count, 0) AS type2_count,
    		(t1.type1_count::float / NULLIF(t1.type1_count + COALESCE(t2.type2_count, 0), 0)) AS ratio
  		FROM
    		Type1_Count t1 
  		LEFT JOIN 
    		Type2_Count t2
  		ON
    		t1.patient_id = t2.patient_id
)
	--- Insert patients who have a ratio greater than 0.5 and no anti-hypoglycemic medication
	INSERT INTO 
		fiftypercent_nohypo (patient_id, date)
	SELECT 
		r.patient_id, r.date
	FROM 
		Ratios r
	WHERE 
		r.ratio > 0.5 
	AND NOT EXISTS (
  		SELECT 1
  		FROM hef_event h
  		WHERE h.patient_id = r.patient_id AND h.name = ANY(ARRAY['rx:metformin',
        'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide',
        'rx:miglitol']		
		)
	AND NOT EXISTS
		(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date)
);
	
/***************
* Summary View *
***************/

--If patient is prediabetic and meets any one of the following criteria, they are considered type 1 diabetic
SELECT 
	f.patient_id, 
	c.date
INTO TEMPORARY 
	Type1_Summary
FROM
	cpeptide c
JOIN
	Frank_Diabetes f
ON 
	c.patient_id = f.patient_id
UNION
SELECT
	a1.patient_id,
	a1.date
FROM
	ab1 a1
JOIN 
	Frank_Diabetes f
ON
	a1.patient_id = f.patient_id
UNION
SELECT
	a2.patient_id,
	a2.date
FROM
	ab2 a2
JOIN 
	Frank_Diabetes f
ON
	a2.patient_id = f.patient_id
UNION
SELECT
	a3.patient_id,
	a3.date
FROM ab3 a3
JOIN 
	Frank_Diabetes f
ON
	a3.patient_id = f.patient_id
UNION
SELECT
	a4.patient_id,
	a4.date
FROM ab4 a4
JOIN
	Frank_Diabetes f
ON
	a4.patient_id = f.patient_id
UNION
/*SELECT
	a5.patient_id,
	a5.date
FROM ab5 a5
JOIN
	Frank_Diabetes f
ON
	a5.patient_id = f.patient_id
UNION
*/
SELECT
	a6.patient_id,
	a6.date
FROM ab6 a6
JOIN
	Frank_Diabetes f
ON a6.patient_id = f.patient_id
UNION
SELECT
	gluc.patient_id,
	gluc.date
FROM
	fiftypercent_glucagon gluc
JOIN 
	Frank_Diabetes f
ON
	gluc.patient_id = f.patient_id
UNION
SELECT
	a.patient_id,
	a.date
FROM 
	acetone a
JOIN 
	Frank_Diabetes f
ON
	a.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	hyp.date
FROM
	fiftypercent_nohypo hyp
JOIN 
	Frank_Diabetes f
ON
	hyp.patient_id = f.patient_id;

--If the patient is considered Frank Diabetic and does NOT meet any of the criteria for Type 1 Diabetes then they are Type2

SELECT 
	f.patient_id,
	f.date
INTO TEMPORARY
	type2_Summary
FROM
	Frank_Diabetes f
WHERE
	patient_id NOT IN (SELECT patient_id FROM type1_Summary);
	
--Display Count of Patients filtering out any patients over 85 and younger than 4
SELECT 
	patient_id,
	date,
	'type 2' as diabetes_type
INTO TEMPORARY
	final_counts
FROM 
	type2_Summary t2
JOIN
	emr_patient p
ON
	t2.patient_id = p.id
WHERE 
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) < 85
	AND
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) > 4
UNION
SELECT 
	patient_id,
	date,
	'type 1' as diabetes_type
FROM 
	type1_Summary t1
JOIN
	emr_patient p
ON
	t1.patient_id = p.id
WHERE 
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) < 85
	AND
	DATE_PART('year', NOW()) - DATE_PART('year', date_of_birth) > 4;


--this gets all patients with type1 and type2 diabetes as identified by the ESP diabetes plugin algorithm.
select distinct on (patient_id) patient_id, date, diabetes_type 
into temporary esp_diab_noin
from final_counts where diabetes_type in ('type 1','type 2')
order by patient_id, date, diabetes_type;

select distinct on (patient_id) patient_id, date, name
into temporary hef_a1c
from hef_event he
where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7','lx:a1c:range:gte:7:lt:8')
  and not exists (select null from inpat_days ipd where ipd.start_date=he.date and ipd.patient_id=he.patient_id)
order by patient_id, date desc,
  case
    when name='lx:a1c:threshold:lt:7' then 1
    when name='lx:a1c:range:gte:7:lt:8' then 2
    when name='lx:a1c:range:gte:8:lte:9' then 3
    when name='lx:a1c:threshold:gt:9' then 4
  end;


select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c_noin
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id,
       DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) as AGE,
       case 
         when DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 4 and 17 then '4-17'
         when DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 18 and 44 then '18-44'
         when DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 45 and 64 then '45-64'
         else '65+' 
       end as age_group
into temporary pat_w_age_noin
from emr_patient p
where DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date
				and not exists (select null from inpat_days ipd 
			            where ipd.start_date=ce.date and ipd.patient_id=ce.patient_id));

-- Create diab_assessed_control using pat_w_age_noin
select t01.patient_id,
       coalesce(t0.diabetes_type, 'Not diabetic') as condition,
       case 
         when t0.diabetes_type is null and t1.name is null then 'Not tested'
         when t0.diabetes_type is null and t1.name = 'lx:a1c:threshold:lt:7' then 'Under A1C 7'
         else 'Diabetic'
       end as control_status
into temporary diab_assessed_control
from pat_w_age_noin t01 
left join esp_diab_noin t0 on t0.patient_id = t01.patient_id
left join last_hef_a1c_noin t1 on t1.patient_id = t01.patient_id;


select count(*) as counts, age_group, condition, control_status
from diab_assessed_control
join pat_w_age_noin on diab_assessed_control.patient_id = pat_w_age_noin.patient_id
where condition = 'Not diabetic' and control_status in ('Not tested', 'Under A1C 7')
group by age_group, condition, control_status
order by age_group, condition, control_status;

