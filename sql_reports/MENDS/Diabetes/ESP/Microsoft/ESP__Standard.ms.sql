-- Step 1
-- Creating temporary table for esp_diab
-- Drop the #esp_diab table if it exists
IF OBJECT_ID('tempdb..#esp_diab') IS NOT NULL
    DROP TABLE #esp_diab;

-- Creating temporary table for esp_diab
SELECT patient_id, [date], condition 
INTO #esp_diab 
FROM nodis_case c
WHERE condition IN ('diabetes:type-1','diabetes:type-2')
AND (
    [date] <= CAST('2022-12-31' AS DATE) 
    OR EXISTS (
        SELECT 1
        FROM nodis_case_events ce
        JOIN hef_event h ON h.id = ce.event_id
        WHERE ce.case_id = c.id AND h.name <> 'rx:metformin' AND h.date <= CAST('2022-12-31' AS DATE)
    )
    OR EXISTS (
        SELECT 1
        FROM nodis_case_events ce
        JOIN hef_event h ON h.id=ce.event_id
        WHERE ce.case_id=c.id AND c.condition='diabetes:type-1' 
        AND h.date<=CAST('2022-12-31' AS DATE) 
        AND h.name IN (
            'lx:a1c:threshold:gte:6.5', 'lx:glucose-fasting:threshold:gte:126',
            'lx:ogtt50-fasting:threshold:gte:126', 'lx:ogtt75-fasting:threshold:gte:126',
            'lx:ogtt100-fasting:threshold:gte:126', 'rx:glyburide', 'rx:gliclazide',
            'rx:glipizide', 'rx:glimepiride', 'rx:pioglitazone', 'rx:rosiglitizone',
            'rx:repaglinide', 'rx:nateglinide', 'rx:meglitinide', 'rx:sitagliptin',
            'rx:exenatide', 'rx:pramlintide', 'rx:insulin'
        )
    )
    OR EXISTS (
        SELECT 1
        FROM nodis_case_events ce
        JOIN hef_event h ON h.id=ce.event_id
        WHERE ce.case_id=c.id AND h.date<=CAST('2022-12-31' AS DATE) 
        AND h.name LIKE 'dx:diabetes:all-types'
        GROUP BY patient_id 
        HAVING COUNT(*) > 1
    )
);

-- Creating the #last_hef_a1c table, ensure it's dropped if it exists
IF OBJECT_ID('tempdb..#last_hef_a1c') IS NOT NULL
    DROP TABLE #last_hef_a1c;

SELECT patient_id, date, name
INTO #last_hef_a1c
FROM (
    SELECT patient_id, date, name,
        ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC) AS rn
    FROM hef_event
    WHERE name IN ('lx:a1c:range:gte:8:lte:9', 'lx:a1c:threshold:gt:9', 'lx:a1c:threshold:lt:7', 'lx:a1c:range:gte:7:lt:8')
) t
WHERE rn = 1;

-- Creating the #pat_w_age table
IF OBJECT_ID('tempdb..#pat_w_age') IS NOT NULL
    DROP TABLE #pat_w_age;

SELECT id AS patient_id, 
       (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 AS AGE
INTO #pat_w_age
FROM emr_patient
WHERE (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 85
AND EXISTS (
    SELECT 1 
    FROM gen_pop_tools.clin_enc ce 
    WHERE ce.patient_id = emr_patient.id 
    AND ce.date BETWEEN '2020-12-31' AND '2022-12-31'
);

-- Creating the #diab_assessed_control table
IF OBJECT_ID('tempdb..#diab_assessed_control') IS NOT NULL
    DROP TABLE #diab_assessed_control;

SELECT p.patient_id, coalesce(e.condition, 'Not diabetic') AS condition, 
    CASE 
        WHEN e.condition IS NULL AND l.name IS NOT NULL AND l.name <> 'lx:a1c:threshold:lt:7' THEN 'Not tested'
        ELSE coalesce(l.name,'Not tested')
    END AS control
INTO #diab_assessed_control
FROM #pat_w_age p
LEFT JOIN #esp_diab e ON e.patient_id = p.patient_id
LEFT JOIN #last_hef_a1c l ON l.patient_id = p.patient_id;

-- Final SELECT with age group breakdown
SELECT
    CASE
        WHEN p.AGE BETWEEN 4 AND 17 THEN '04-17'
        WHEN p.AGE BETWEEN 18 AND 44 THEN '18-44'
        WHEN p.AGE BETWEEN 45 AND 64 THEN '45-64'
        ELSE '65+'
    END AS age_group,
    COUNT(*) AS counts,
    d.condition,
    d.control
FROM #diab_assessed_control d
INNER JOIN #pat_w_age p ON d.patient_id = p.patient_id
GROUP BY 
    CASE
        WHEN p.AGE BETWEEN 4 AND 17 THEN '04-17'
        WHEN p.AGE BETWEEN 18 AND 44 THEN '18-44'
        WHEN p.AGE BETWEEN 45 AND 64 THEN '45-64'
        ELSE '65+'
    END,
    d.condition,
    d.control
ORDER BY 
    age_group,
    CASE
        WHEN d.control = 'lx:a1c:threshold:lt:7' THEN 1
        WHEN d.control = 'lx:a1c:range:gte:7:lt:8' THEN 2
        WHEN d.control = 'lx:a1c:range:gte:8:lte:9' THEN 3
        WHEN d.control = 'lx:a1c:threshold:gt:9' THEN 4
        WHEN d.control = 'Not tested' THEN 5
    END;