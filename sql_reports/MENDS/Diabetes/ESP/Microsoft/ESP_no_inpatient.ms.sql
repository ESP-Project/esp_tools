/**********************************
* Step 0: Identify inpatient days *
**********************************/

if object_id('tempdb..#inpat_days') IS NOT NULL
	DROP TABLE #inpat_days;
if object_id('tempdb..#a1c_results') IS NOT NULL
	DROP TABLE #a1c_results;
if object_id('tempdb..#fasting_glucose') IS NOT NULL
	DROP TABLE #fasting_glucose;
if object_id('tempdb..#random_glucose') IS NOT NULL
	DROP TABLE #random_glucose;
if object_id('tempdb..#medication_disp') IS NOT NULL
	DROP TABLE #medication_disp;
if object_id('tempdb..#outpatient_code') IS NOT NULL
	DROP TABLE #outpatient_code;
if object_id('tempdb..#insulin_disp') IS NOT NULL
	DROP TABLE #insulin_disp;
if object_id('tempdb..#frank_diabetes') IS NOT NULL
	DROP TABLE #frank_diabetes;
if object_id('tempdb..#cpeptide') IS NOT NULL
	DROP TABLE #cpeptide
IF OBJECT_ID('tempdb..#ab1') IS NOT NULL
		DROP TABLE #ab1;
IF OBJECT_ID('tempdb..#acetone') IS NOT NULL
		DROP TABLE #acetone;
IF OBJECT_ID('tempdb..#fiftypercent_nohypoglycemic') IS NOT NULL
		DROP TABLE #fiftypercent_nohypoglycemic;
IF OBJECT_ID('tempdb..#FRANK_Summary') IS NOT NULL
		DROP TABLE #FRANK_Summary;
IF OBJECT_ID('tempdb..#fiftypercent_glucagon') IS NOT NULL
		DROP TABLE #fiftypercent_glucagon;
IF OBJECT_ID('tempdb..#Type1_Summary') IS NOT NULL
		DROP TABLE #Type1_Summary;
IF OBJECT_ID('tempdb..#Type2_Summary') IS NOT NULL
		DROP TABLE #Type2_Summary;
IF OBJECT_ID('tempdb..#final_counts') IS NOT NULL
		DROP TABLE #final_counts;
IF OBJECT_ID('tempdb..#diabetes_results') IS NOT NULL
		DROP TABLE #diabetes_results;


-- Step 0: Identify inpatient days

IF OBJECT_ID('tempdb..#inpat_days') IS NOT NULL DROP TABLE #inpat_days;
IF OBJECT_ID('tempdb..#a1c_results') IS NOT NULL DROP TABLE #a1c_results;
-- ... [repeat for all other tables you provided]

-- Create the inpat_days temporary table
SELECT 
    enc.patient_id, 
    enc.date AS start_date, 
    ISNULL(enc.hosp_dschrg_dt, enc.date) AS end_date
INTO #inpat_days
FROM 
    emr_encounter enc
JOIN 
    gen_pop_tools.rs_conf_mapping AS gpt 
ON 
    enc.raw_encounter_type = gpt.src_value 
AND 
    gpt.src_field = 'raw_encounter_type'
WHERE 
    gpt.mapped_value = 'inpatient';

-- Step 1: Classify as Frank Diabetes

-- a1c  >= 6.5
SELECT 
    patient_id,
    date,
    'a1c' AS event_type
INTO #a1c_results
FROM
    hef_event h
WHERE 
    name = 'lx:a1c:threshold:gte:6.5'
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- fasting plasma glucose concentration >= 126 mg/dl
SELECT 
    patient_id,
    date,
    'fasting glucose' AS event_type
INTO #fasting_glucose
FROM
    hef_event h 
WHERE 
    name = 'lx:glucose-fasting:threshold:gte:126'
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- random plasma glucose concentration >= 200 mg/dl
SELECT 
    patient_id,
    date,
    'random glucose' AS event_type
INTO #random_glucose
FROM
    hef_event h 
WHERE 
    name = 'lx:glucose-random:threshold:gte:200'
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- outpatient diagnosis code for diabetes
SELECT 
    h.patient_id, 
    h.date,
    'dx' AS event_type
INTO #outpatient_code
FROM 
    hef_event h
WHERE 
    name IN ('dx:diabetes:all-types')
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- any dispensation of antihyperglycemic medication
SELECT 
    patient_id, 
    date,
    name
INTO #medication_disp
FROM 
    hef_event AS h 
WHERE 
    LOWER(name) IN 
    (
    'rx:glyburide', 'rx:gliclazide', 'rx:glipizide', 'rx:glimepiride', 'rx:pioglitazone',
    'rx:rosiglitizone', 'rx:repaglinide', 'rx:nateglinide', 'rx:meglitinide',
    'rx:sitagliptin', 'rx:exenatide', 'rx:pramlintide'
    )
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- need metformin for error in diabetes plugin
SELECT 
    patient_id,
    date,
    name
INTO #metformin_disp
FROM
    hef_event AS h
WHERE
    LOWER(name) = 'rx:metformin'
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- Insulin outside of pregnancy
SELECT 
    patient_id, 
    name, 
    date
INTO #insulin_disp
FROM 
    hef_event AS h 
WHERE 
    name = 'rx:insulin' 
AND NOT EXISTS 
    (
        SELECT NULL 
        FROM hef_timespan AS ts 
        WHERE ts.patient_id = h.patient_id 
        AND ts.name = 'pregnancy'
        AND h.date > ts.start_date 
        AND h.date < ISNULL(ts.end_date, GETDATE())
    )
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- ogtt fasting
SELECT 
    patient_id, 
    name, 
    date
INTO #ogtt_fasting
FROM 
    hef_event AS h 
WHERE 
    name IN ('lx:ogtt50-fasting:threshold:gte:126','lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126')
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- ogtt random
SELECT 
    patient_id, 
    name, 
    date
INTO #ogtt_random
FROM 
    hef_event AS h 
WHERE 
    name = 'lx:ogtt50-random:threshold:gte:200'
AND NOT EXISTS
    (SELECT NULL FROM #inpat_days indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

-- age calculation
SELECT 
    id AS patient_id, 
    (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 as age
INTO #pat_w_age
FROM 
    emr_patient p
WHERE 
    (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 85
AND EXISTS 
    (SELECT NULL FROM gen_pop_tools.clin_enc ce 
    WHERE ce.patient_id = p.id AND ce.date BETWEEN '2020-12-31' AND '2022-12-31');


-- Union all tables into a single collection of Frank Diabetic patients
WITH t1 AS (
    SELECT patient_id, date
    FROM #a1c_results
),
t2 AS (
    SELECT patient_id, date
    FROM #fasting_glucose
),
t0_3 AS (
    SELECT patient_id, 
           ROW_NUMBER() OVER(PARTITION BY patient_id ORDER BY patient_id, date) AS row_n, 
           date 
    FROM #random_glucose
),
t3 AS (
    SELECT patient_id, date
    FROM t0_3
    WHERE row_n = 2
),
t4 AS (
    SELECT patient_id, date
    FROM #insulin_disp
),
t5 AS (
    SELECT patient_id, MIN(date) AS date
    FROM #outpatient_code
    GROUP BY patient_id
    HAVING COUNT(*) > 1 AND MIN(date) <= CAST('2023-12-31' AS DATE)
),
t6 AS (
    SELECT patient_id, date
    FROM #medication_disp
),
t7 AS (
    SELECT patient_id, date
    FROM #ogtt_fasting
),
t0_8 AS (
    SELECT patient_id,
           ROW_NUMBER() OVER(PARTITION BY patient_id ORDER BY date) AS row_n,
           date
    FROM #ogtt_random
),
t8 AS (
    SELECT patient_id, date
    FROM t0_8
    WHERE row_n = 2
),
t9 AS (
    SELECT patient_id, date, 'a1c' AS dtype
    FROM t1
    UNION
    SELECT patient_id, date, 'fasting glucose' AS dtype
    FROM t2
    UNION
    SELECT patient_id, date, 'random glucose' AS dtype
    FROM t3
    UNION
    SELECT patient_id, date, 'insulin' AS dtype
    FROM t4
    UNION
    SELECT patient_id, date, 'diabetes dx' AS dtype
    FROM t5
    UNION
    SELECT patient_id, date, 'meds' AS dtype
    FROM t6
    UNION
    SELECT patient_id, date, 'fasting ogtt' AS dtype
    FROM t7
    UNION
    SELECT patient_id, date, 'random ogtt' AS dtype
    FROM t8
),
t9_final AS (
    SELECT t9.patient_id, date, dtype,
           ROW_NUMBER() OVER(PARTITION BY t9.patient_id ORDER BY date) AS rn
    FROM t9
    JOIN #pat_w_age pwa ON t9.patient_id = pwa.patient_id
    WHERE CAST(date AS DATE) <= '2022-12-31'
       OR EXISTS (
            SELECT 1
            FROM #metformin_disp md
            WHERE CAST(md.date AS DATE) <= '2022-12-31' AND md.patient_id = t9.patient_id
        )
)
SELECT patient_id, date, dtype
INTO #frank_diabetes
FROM t9_final
WHERE rn = 1;

-- STEP 2: Differentiate Type 1 or Type 2 Diabetes

-- C-peptide test <0.8
SELECT patient_id, date
INTO #cpeptide
FROM hef_event
WHERE name = 'lx:c-peptide:threshold:lt:0.8'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = patient_id AND date BETWEEN indays.start_date AND indays.end_date
);

-- Diabetes auto-antibodies positive
-- Antibodies #1: GAD-65 AB > 1.0
SELECT COUNT(*) ciybts, patient_id, date, 'ab1' AS event_type
INTO #ab1
FROM hef_event h
WHERE name = 'lx:gad65:threshold:gt:1'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
)
GROUP BY patient_id, date;

-- Antibodies #2: ICA-512 Autoantibodies > 0.8
SELECT COUNT(*) counts, patient_id, date, 'ab2' AS event_type
INTO #ab2
FROM hef_event h
WHERE name = 'lx:ica512:threshold:gt:0.8'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
)
GROUP BY patient_id, date;

-- Antibodies #3: Islet Cell Antibody positive
SELECT patient_id, date, 'ab3' AS event_type
INTO #ab3
FROM hef_event h
WHERE name = 'lx:islet-cell-antibody:positive'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
);

-- Antibodies #4: Islet Cell Antibody Titer >= 1.25
SELECT COUNT(*) counts, patient_id, date, 'ab4' AS event_type
INTO #ab4
FROM hef_event h
WHERE name = 'lx:islet-cell-antibody:threshold:gte:1.25'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
)
GROUP BY patient_id, date;

-- Antibodies #6: Insulin AB >= .8
SELECT COUNT(*) counts, patient_id, date, 'ab6' AS event_type
INTO #ab6
FROM hef_event h
WHERE name = 'lx:insulin-antibody:threshold:gt:0.8'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
)
GROUP BY patient_id, date;

-- Prescription for URINE ACETONE TEST STRIPS
SELECT patient_id, date, 'acetone' AS event_type
INTO #acetone
FROM hef_event h
WHERE name = 'rx:acetone'
AND NOT EXISTS (
    SELECT 1
    FROM #inpat_days AS indays
    WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date
)
GROUP BY patient_id, date;

--50% ratio and glucagon prescription

SELECT DISTINCT
	enc.patient_id, 
	enc.date,
	'fiftypercent_glucagon' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO #fiftypercent_glucagon
FROM 
	emr_encounter AS enc
JOIN 
	emr_encounter_dx_codes AS codes 
ON 
	enc.id = codes.encounter_id 
WHERE 
	(
		(codes.dx_code_id like 'ICD10:E10.%'
		OR
		codes.dx_code_id like 'ICD10:E11.%')
	
	AND enc.patient_id IN(
		SELECT
			patient_id
		FROM emr_prescription
		WHERE LOWER(name) LIKE
			'%glucagon%')
	AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = enc.patient_id AND enc.date BETWEEN indays.start_date AND indays.end_date)
	)
GROUP BY
	enc.patient_id,
	enc.date
HAVING
	(SUM(CASE WHEN codes.dx_code_id like 'E10' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0
	AND
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;

--50% ratio and no oral hypoglycemic

SELECT DISTINCT 
	hef.patient_id,
	hef.date,
	'fiftypercent_nohypo' as event_type,
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) AS sum_type1,
	SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END) AS sum_type2
INTO #fiftypercent_nohypoglycemic
FROM 
	emr_encounter AS enc
JOIN 
	hef_event AS hef 
ON 
	enc.patient_id = hef.patient_id
JOIN
	emr_encounter_dx_codes as codes
ON
	enc.id = codes.encounter_id
WHERE 
	hef.name not in ('rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
)
	AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = hef.patient_id AND hef.date BETWEEN indays.start_date AND indays.end_date)
GROUP BY 
	hef.patient_id,
	hef.date
HAVING 
	(SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > 0 
	AND 
	SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) / (SUM(CASE WHEN codes.dx_code_id like 'E10%' THEN 1 ELSE 0 END) + SUM(CASE WHEN codes.dx_code_id like 'E11%' THEN 1 ELSE 0 END)) > .5;
	
/***************
* Summary View *
***************/

-- Add patient_id and date for pre diabetic patients into FRANK_Summary temp table
SELECT 
	patient_id, 
	date
INTO #FRANK_Summary
FROM 
	#a1c_results
UNION
SELECT
	patient_id,
	date
FROM 
	#fasting_glucose
UNION
SELECT
	patient_id,
	date
FROM
	#random_glucose
UNION
SELECT 
	o.patient_id,
	o.date
FROM
	#outpatient_code o
UNION
SELECT
	f.patient_id,
	f.date as frank_date
FROM
	#frank_diabetes f;

--If patient is prediabetic and meets any one of the following criteria, they are considered type 1 diabetic
SELECT 
	f.patient_id, 
	f.date
INTO #Type1_Summary
FROM
	#cpeptide c
JOIN
	#FRANK_Summary f
ON 
	c.patient_id = f.patient_id
UNION
SELECT
	a1.patient_id,
	a1.date
FROM
	#ab1 a1
JOIN 
	#FRANK_Summary f
ON
	a1.patient_id = f.patient_id
UNION
SELECT
	a2.patient_id,
	a2.date
FROM
	#ab2 a2
JOIN 
	#FRANK_Summary f
ON
	a2.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	f.date
FROM
	#acetone a
JOIN
	#FRANK_Summary f
ON
	a.patient_id = f.patient_id
UNION
SELECT
	gluc.patient_id,
	gluc.date
FROM
	#fiftypercent_glucagon gluc
JOIN 
	#FRANK_Summary f
ON
	gluc.patient_id = f.patient_id
UNION
SELECT
	f.patient_id,
	f.date
FROM
	#fiftypercent_nohypoglycemic hyp
JOIN 
	#FRANK_Summary f
ON
	hyp.patient_id = f.patient_id;

--If the patient is considered Frank Diabetic and does NOT meet any of the criteria for Type 1 Diabetes then they are Type2
CREATE TABLE #diabetes_results
(
	patient_id INT,
	diabetes_type NVARCHAR(50),
	diagnosis_date DATETIME
);

INSERT INTO #diabetes_results (patient_id, diabetes_type, diagnosis_date)
SELECT 
	f.patient_id,
	'type 2',
	CAST(f.date AS DATETIME)
FROM
	#FRANK_Summary f
JOIN
	emr_patient p
ON
	f.patient_id = p.id
WHERE
	patient_id NOT IN (SELECT patient_id FROM #type1_Summary)
	AND 
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) < 85
	AND
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) > 4;

-- Create a separate table to hold the information for type 1 diabetics

INSERT INTO #diabetes_results(patient_id, diabetes_type, diagnosis_date)
SELECT 
	t1.patient_id,
	'type 1',
	cast(t1.date as datetime)
FROM 
	#type1_Summary t1
JOIN
	emr_patient p
ON
	t1.patient_id = p.id
WHERE 
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) < 85
	AND
	YEAR(GETDATE()) - YEAR(CAST(p.date_of_birth AS DATETIME)) > 4;


--Apply A1C filtering to show Patients Under A1C threshold and Over A1C threshold.  Also show population of --non-diabetic
-- Step 1
SELECT patient_id, date, condition 
INTO #esp_diab 
FROM #diabetes_results
WHERE condition IN ('diabetes:type-1','diabetes:type-2');

--a1c results
WITH RankedEvents AS (
    SELECT 
        patient_id, 
        date, 
        name,
        ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC, name) as RowNum
    FROM 
        hef_event he
        where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7',                'lx:a1c:range:gte:7:lt:8')
        and not exists (select null from inpat_days ipd where ipd.start_date=he.date and ipd.patient_id=he.patient_id)
        and date<='2022-12-31'
        order by patient_id, date desc,
    case
        when name='lx:a1c:threshold:lt:7' then 1
        when name='lx:a1c:range:gte:7:lt:8' then 2
        when name='lx:a1c:range:gte:8:lte:9' then 3
        when name='lx:a1c:threshold:gt:9' then 4
    end;
)
SELECT 
    patient_id, 
    date, 
    name
FROM 
    RankedEvents
WHERE 
    RowNum = 1;

-- Step 5
SELECT patient_id, date, name
INTO #last_hef_a1c
FROM (
    SELECT *, ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC, name) AS rn
    FROM #hef_a1c
) AS t
WHERE rn = 1;

-- Step 6
SELECT id AS patient_id, DATEDIFF(YEAR, date_of_birth, '2023-12-31') AS AGE
INTO #pat_w_age_noin
FROM emr_patient
WHERE DATEDIFF(YEAR, date_of_birth, '2023-12-31') BETWEEN 4 AND 85
    AND EXISTS (SELECT 1 FROM gen_pop_tools.clin_enc ce 
                WHERE ce.patient_id = emr_patient.id AND ce.date BETWEEN '2020-12-31' AND '2022-12-31'
                    and not exists (select null from inpat_days ipd 
			            where ipd.start_date=ce.date and ipd.patient_id=ce.patient_id));

-- Step 7
select t0.patient_id, coalesce(diabetes_type, CAST('Not diabetic' AS varchar(50)) condition,
  case
    when diabetes_type is null and name is not null and name <> 'lx:a1c:threshold:lt:7' then 'Not tested'
    else coalesce(name,'Not tested')
  end control
into #diab_assessed_control
from #pat_w_age_noin t01
left join #esp_diab t0 on t0.patient_id=t01.patient_id
left join #last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;


-- Step 8
SELECT COUNT(*) AS counts, condition, control
FROM #diab_assessed_control 
GROUP BY condition, control
order by condition,
  case
    when control='lx:a1c:threshold:lt:7' then 1
    when control='lx:a1c:range:gte:7:lt:8' then 2
    when control='lx:a1c:range:gte:8:lte:9' then 3
    when control='lx:a1c:threshold:gt:9' then 4
    when control='Not tested' then 5
  end;
