/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 10/04/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Branched development from the ESP version to IQVIA.  Implemented IQVIA logic.
	V7				RJA			 Incorporated Bob Z's changes into Microsoft SQL format.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

-- Check and drop existing temporary tables
IF OBJECT_ID('tempdb..#inpat_days') IS NOT NULL
    DROP TABLE #inpat_days;
IF OBJECT_ID('tempdb..#inpatient_dx') IS NOT NULL
    DROP TABLE #inpatient_dx;
IF OBJECT_ID('tempdb..#a1c_results') IS NOT NULL
    DROP TABLE #a1c_results;
IF OBJECT_ID('tempdb..#fasting_glucose') IS NOT NULL
    DROP TABLE #fasting_glucose;
IF OBJECT_ID('tempdb..#random_glucose') IS NOT NULL
    DROP TABLE #random_glucose;
IF OBJECT_ID('tempdb..#outpatient_dx') IS NOT NULL
    DROP TABLE #outpatient_dx;
IF OBJECT_ID('tempdb..#medication_disp') IS NOT NULL
    DROP TABLE #medication_disp;
IF OBJECT_ID('tempdb..#diab_2_events') IS NOT NULL
    DROP TABLE #diab_2_events;
IF OBJECT_ID('tempdb..#diab_pats') IS NOT NULL
    DROP TABLE #diab_pats;
IF OBJECT_ID('tempdb..#last_hef_a1c') IS NOT NULL
    DROP TABLE #last_hef_a1c;
IF OBJECT_ID('tempdb..#pat_w_age') IS NOT NULL
    DROP TABLE #pat_w_age;
IF OBJECT_ID('tempdb..#diab_assessed_control') IS NOT NULL
    DROP TABLE #diab_assessed_control;

SELECT 
    enc.patient_id, 
    enc.date AS start_date, 
    ISNULL(enc.hosp_dschrg_dt, enc.date) AS end_date
INTO #inpat_days
FROM 
    emr_encounter enc
JOIN 
    gen_pop_tools.rs_conf_mapping AS gpt 
ON 
    enc.raw_encounter_type = gpt.src_value 
AND 
    gpt.src_field = 'raw_encounter_type'
WHERE 
    gpt.mapped_value = 'inpatient';

SELECT 
	h.patient_id, 
	h.date
INTO #inpatient_dx
FROM 
	hef_event h
WHERE 
	name IN ('dx:diabetes:all-types') AND date <= '2022-12-31'
AND EXISTS
	(SELECT 1 FROM #inpat_days AS indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

SELECT 
	patient_id,
	date,
	'a1c' as event_type
INTO #a1c_results
FROM
	hef_event h
WHERE 
	name = 'lx:a1c:threshold:gte:6.5';

SELECT 
	patient_id,
	date,
	'fasting glucose' as event_type
INTO #fasting_glucose
FROM
	hef_event h 
WHERE 
	name IN ('lx:glucose-fasting:threshold:gte:126','lx:ogtt50-fasting:threshold:gte:126',
      'lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126');

SELECT 
	patient_id,
	date,
	'random glucose' as event_type
INTO #random_glucose
FROM
	hef_event h 
WHERE 
	name IN ('lx:glucose-random:threshold:gte:200','lx:ogtt50-random:threshold:gte:200');

SELECT 
	h.patient_id, 
	h.date
INTO #outpatient_dx
FROM 
	hef_event h
WHERE 
	name IN ('dx:diabetes:all-types')
AND NOT EXISTS
	(SELECT 1 FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);

SELECT patient_id, 
	date,
	name
INTO #medication_disp
FROM 
	hef_event AS h 
WHERE 
	LOWER(name)
	IN 
	(
	'rx:glyburide',
    'rx:gliclazide',
    'rx:glipizide',
    'rx:glimepiride',
    'rx:pioglitazone',
    'rx:rosiglitizone',
    'rx:repaglinide',
    'rx:nateglinide',
    'rx:meglitinide',
    'rx:sitagliptin',
    'rx:exenatide',
    'rx:pramlintide'
	);

-- Note: "temporary" tables in PostgreSQL translate to "temporary" tables in MS SQL with a '#' prefix

-- Convert diab_2_events
SELECT patient_id, MIN(date) AS date
INTO #diab_2_events
FROM (
    SELECT t0.*, LEAD(date) OVER (PARTITION BY patient_id ORDER BY patient_id, date) AS next_date,
    LEAD(crit) OVER (PARTITION BY patient_id ORDER BY patient_id, date, crit) AS next_crit
    FROM (
        SELECT patient_id, date, 'a1c' AS crit FROM #a1c_results
        UNION SELECT patient_id, date, 'fast_gluc' AS crit FROM #fasting_glucose
        UNION SELECT patient_id, date, 'rand_gluc' AS crit FROM #random_glucose
        UNION SELECT patient_id, date, 'outp_diag' AS crit FROM #outpatient_dx
        UNION SELECT patient_id, date, 'antihyperg' AS crit FROM #medication_disp
    ) AS t0
) AS t00
WHERE DATEDIFF(MONTH, date, next_date) BETWEEN 0 AND 24
AND (date < next_date OR (date <= next_date AND crit <> next_crit))
AND next_date <= '2022-12-31'
GROUP BY patient_id;

-- Convert diab_pats
SELECT patient_id, MIN(date) AS date, 'diabetic' AS type
INTO #diab_pats
FROM (
    SELECT patient_id, date FROM #diab_2_events
    UNION SELECT patient_id, date FROM #inpatient_dx
) AS t0
GROUP BY patient_id;

-- Convert hef_a1c_gte
select patient_id, date, name
into #last_hef_a1c
from (select patient_id, date, name,
  row_number() over (partition by patient_id order by patient_id, date desc) as rn
from hef_event
where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7','lx:a1c:range:gte:7:lt:8') ) t0
where rn = 1;

-- Convert pat_w_age
SELECT id AS patient_id, 
       (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 AS AGE
INTO #pat_w_age
FROM emr_patient p
WHERE (0 + 20221231 - convert(char(8),date_of_birth,112))/10000 BETWEEN 4 AND 85
AND EXISTS (
    SELECT 1 
    FROM gen_pop_tools.clin_enc ce 
    WHERE ce.patient_id = p.id 
    AND ce.date BETWEEN CAST('2020-12-31' AS DATE) AND CAST('2022-12-31' AS DATE)
);

-- Convert diab_assessed_control
select t0.patient_id, coalesce(type, 'Not diabetic') condition,
    coalesce(name,'Not tested') control
into #diab_assessed_control
from #pat_w_age t01
left join #diab_pats t0 on t0.patient_id=t01.patient_id
left join #last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;

-- Final aggregated query with specific structure and age groupings
SELECT 
    age_group,
    A1C_Range,
    SUM(CASE WHEN condition = 'Diabetic' THEN 1 ELSE 0 END) AS Diabetic,
    SUM(CASE WHEN condition = 'Not Diabetic' THEN 1 ELSE 0 END) AS Not_Diabetic,
    SUM(CASE WHEN condition = 'Not Diabetic' AND control = 'lx:a1c:threshold:gte:6.5' THEN 1 ELSE 0 END) AS A1C_Greater_Than_6_5
FROM 
    (
        SELECT
            t01.patient_id,
            CASE
                WHEN t01.AGE BETWEEN 4 AND 17 THEN '04-17'
                WHEN t01.AGE BETWEEN 18 AND 44 THEN '18-44'
                WHEN t01.AGE BETWEEN 45 AND 64 THEN '45-64'
                ELSE '65+' 
            END AS age_group,
            CASE
                WHEN t1.name = 'lx:a1c:threshold:lt:7' THEN '<7'
                WHEN t1.name = 'lx:a1c:range:gte:7:lt:8' THEN '7-8'
                WHEN t1.name = 'lx:a1c:range:gte:8:lte:9' THEN '8-9'
                WHEN t1.name = 'lx:a1c:threshold:gt:9' THEN '>9'
                WHEN t1.name IS NULL THEN 'Not tested'
                ELSE 'Other'
            END AS A1C_Range,
            t0.condition,
            t1.name AS control
        FROM 
            #pat_w_age t01
        LEFT JOIN 
            #diab_pats t0 ON t0.patient_id = t01.patient_id
        LEFT JOIN 
            #last_hef_a1c t1 ON t1.patient_id = t01.patient_id
    ) subquery
GROUP BY 
    age_group, A1C_Range
ORDER BY 
    age_group,
    CASE
        WHEN A1C_Range = '<7' THEN 1
        WHEN A1C_Range = '7-8' THEN 2
        WHEN A1C_Range = '8-9' THEN 3
        WHEN A1C_Range = '>9' THEN 4
        WHEN A1C_Range = 'Not tested' THEN 5
    END;
