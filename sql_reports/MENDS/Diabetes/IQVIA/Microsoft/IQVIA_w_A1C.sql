--Patient immediately qualifies as diabetic if they have 1 inpatient diagnosis of codes 250.x, 357.2, 366.41, 362.01-362.07)
	IF OBJECT_ID('tempdb..#inpat_days') IS NOT NULL
		DROP TABLE #inpat_days;
	IF OBJECT_ID('tempdb..#inpatient_dx') IS NOT NULL
		DROP TABLE #inpatient_dx;
	IF OBJECT_ID('tempdb..#a1c_results') IS NOT NULL
		DROP TABLE #a1c_results;
	IF OBJECT_ID('tempdb..#fasting_glucose') IS NOT NULL
		DROP TABLE #fasting_glucose;
	IF OBJECT_ID('tempdb..#random_glucose') IS NOT NULL
		DROP TABLE #random_glucose;
	IF OBJECT_ID('tempdb..#inpat_days') IS NOT NULL
		DROP TABLE #inpat_days;
	IF OBJECT_ID('tempdb..#outpatient_dx') IS NOT NULL
		DROP TABLE #outpatient_dx;
	IF OBJECT_ID('tempdb..#medication_disp') IS NOT NULL
		DROP TABLE #medication_disp;
	IF OBJECT_ID('tempdb..#a1c_results') IS NOT NULL
		DROP TABLE #a1c_results;
	IF OBJECT_ID('tempdb..#diab_2_events') IS NOT NULL
		DROP TABLE #diab_2_events;
	IF OBJECT_ID('tempdb..#diab_pats') IS NOT NULL
		DROP TABLE #diab_pats;
	IF OBJECT_ID('tempdb..#pat_w_age') IS NOT NULL
		DROP TABLE #pat_w_age;
	IF OBJECT_ID('tempdb..#diab_assessed_control') IS NOT NULL
		DROP TABLE #diab_assessed_control;
	IF OBJECT_ID('tempdb..#lab_over_65') IS NOT NULL
		DROP TABLE #lab_over_65;

IF OBJECT_ID('tempdb..#last_hef_a1c') IS NOT NULL
    DROP TABLE #last_hef_a1c;

IF OBJECT_ID('tempdb..#hef_results') IS NOT NULL
    DROP TABLE #hef_results;

CREATE TABLE #last_hef_a1c (
    patient_id INT,
    date DATE,
    name VARCHAR(255)
);

CREATE TABLE #hef_results (
	patient_id INT,
	date DATE,
	name VARCHAR(255)
);

SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO #inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';
	
 --drop table inpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO #inpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types') and CAST(date AS DATE) <= '2023-12-31'
AND EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
--drop table a1c_results;
select 
	patient_id,
	date,
	'a1c' as event_type
into #a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5';
    
--drop table fasting_glucose;
select 
	patient_id,
	date,
	'fasting glucose' as event_type
into #fasting_glucose
from
	hef_event h 
WHERE 
	name in ('lx:glucose-fasting:threshold:gte:126','lx:ogtt50-fasting:threshold:gte:126','lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126');
    
--drop table random_glucose;
select 
	patient_id,
	date,
	'random glucose' as event_type
into #random_glucose
from
	hef_event h 
where 
	name in ('lx:glucose-random:threshold:gte:200','lx:ogtt50-random:threshold:gte:200');
 --drop table outpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO #outpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types')
AND NOT EXISTS
	(SELECT NULL FROM #inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
 
--drop table medication_disp 
SELECT patient_id, 
	date,
	name
INTO #medication_disp
FROM 
	hef_event AS h 
WHERE 
	lower(name)
	IN 
	(
	'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
	);
    
--drop table diab_2_events;
 select patient_id, min(date) as date
 into #diab_2_events
 from (
 select t0.*, lead(date) over (partition by patient_id order by patient_id, date) as next_date,
    lead(crit) over (partition by patient_id order by patient_id, date, crit) as next_crit
 from (
 select patient_id, date, 'a1c' as crit from #a1c_results
 union select patient_id, date, 'fast_gluc' as crit from #fasting_glucose
 union select patient_id, date, 'rand_gluc' as crit from #random_glucose
 union select patient_id, date, 'outp_diag' as crit from #outpatient_dx
 union select patient_id, date, 'antihyperg' as crit from #medication_disp) t0) t00
 WHERE
	DATEDIFF(MONTH, date, next_date) BETWEEN 0 AND 24
	AND (date < next_date OR (date <= next_date AND crit <> next_crit))
	AND next_date <= '2022-12-31'
 group by patient_id;
 
 --drop table diab_pats;
select patient_id, min(date) as date, 'diabetic' as type
 into #diab_pats
 from (
 select patient_id, date from #diab_2_events
 union select patient_id, date from #inpatient_dx) t0
 group by patient_id;
 
--select distinct on (patient_id) patient_id, date, name
WITH RankedResults AS (
    SELECT
        patient_id,
        date,
        name,
        ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC) AS rn
    FROM 
        hef_event
    WHERE 
        name IN ('lx:a1c:range:gte:8:lte:9', 'lx:a1c:threshold:gt:9', 'lx:a1c:threshold:lt:7', 'lx:a1c:range:gte:7:lt:8')
)
INSERT INTO #last_hef_a1c (patient_id, date, name)
SELECT
    patient_id,
    date, 
    name
FROM 
    RankedResults
WHERE
    rn = 1;

WITH hef_results AS
(
	select patient_id, date, name,
	ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY date DESC) AS rn
	from hef_a1c
)
INSERT INTO #hef_results
SELECT
	patient_id,
	date,
	name
FROM 
	hef_results
WHERE
	rn = 1;

SELECT
	id AS patient_id,
	DATEDIFF(YEAR, date_of_birth, '2022-12-31') AS AGE	
INTO #pat_w_age
FROM emr_patient p
where DATEDIFF(YEAR, date_of_birth, '2022-12-31') between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31' and '2022-12-31');

select t0.patient_id, coalesce(type, 'Not diabetic') condition,
    coalesce(name,'Not tested') control
into #diab_assessed_control
from #pat_w_age t01
left join #diab_pats t0 on t0.patient_id=t01.patient_id
left join last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;


-- Creating lab_over_65 #table
SELECT 
	t0.patient_id, 
	'lab results > 6.5' as result_category
INTO #lab_over_65
FROM emr_labresult t0
INNER JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE t0.result_float >= 6.5 AND t0.date < '2022-12-31'
GROUP BY t0.patient_id
HAVING COUNT(*) = 1;

-- Final query with proper scope for lo65 alias and breakdown by A1C >= 6.5
SELECT
    age_group,
    COUNT(*) AS counts,
    condition,
    control,
    COUNT(CASE WHEN lo65_patient_id IS NOT NULL THEN 1 END) AS "a1c >= 6.5"
FROM 
    (
        SELECT 
            t01.patient_id,
            t01.age,
            CASE
                WHEN t01.age BETWEEN 4 AND 17 THEN '04-17'
                WHEN t01.age BETWEEN 18 AND 44 THEN '18-44'
                WHEN t01.age BETWEEN 45 AND 64 THEN '45-64'
                WHEN t01.age >= 65 THEN '65+'
            END AS age_group,
            COALESCE(t0.type, 'Not diabetic') AS condition,
            COALESCE(t1.name, 'Not tested') AS control,
            lo65.patient_id AS lo65_patient_id
        FROM 
            #pat_w_age t01
        LEFT JOIN 
            #diab_pats t0 ON t0.patient_id = t01.patient_id
        LEFT JOIN 
            last_hef_a1c t1 ON t1.patient_id = t01.patient_id AND (t1.date >= t0.date OR t0.date IS NULL)
        LEFT JOIN
            #lab_over_65 lo65 ON t01.patient_id = lo65.patient_id
    ) subquery
GROUP BY 
    age_group, condition, control
ORDER BY 
    age_group,
	condition,
    CASE
        WHEN control = 'lx:a1c:threshold:lt:7' THEN 1
        WHEN control = 'lx:a1c:range:gte:7:lt:8' THEN 2
        WHEN control = 'lx:a1c:range:gte:8:lte:9' THEN 3
        WHEN control = 'lx:a1c:threshold:gt:9' THEN 4
        WHEN control = 'Not tested' THEN 5
    END;

