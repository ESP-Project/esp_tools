/*********************************************************************
PROJECT DETAILS:
Subject:  ESPHealth MENDS Diabetes/IQVIA Investigation
Created By: Robert Anderson
Create Date:  6/20/2023
Version: 1.07
Last Revision: 8/03/2023
Version History:

  VERSION		   AUTHOR						NOTES
	V1				RJA			 Initial Document Creation Per Project Goals
 	V2				RJA			 Updated a1c glucose query based on conversation with Bob Z	
	V3				RJA			 Updated all other queries following the template from V2 
	V4				RJA			 Added supplemental table queries
	V5				RJA			 Added filtering by age
	V6				RJA			 Branched development from the ESP version to IQVIA.  Implemented IQVIA logic.

PROJECT GOALS:
Compare the following variations of the ESPHealth MENDS Diabetes Algorithm
1)  ESPHealth Current Diabetes Algorithm
2)  ESPHealth Diabetes Algorithm excluding inpatient results
3)  Criteria as defined in the IQVIA Investigation Email from Bob Z

*********************************************************************/

--Patient immediately qualifies as diabetic if they have 1 inpatient diagnosis of codes 250.x, 357.2, 366.41, 362.01-362.07)
	
SELECT 
	enc.patient_id, 
	enc.date start_date, 
	COALESCE(enc.hosp_dschrg_dt, enc.date) end_date
INTO TEMPORARY 
	inpat_days
FROM 
	emr_encounter enc
JOIN 
	gen_pop_tools.rs_conf_mapping AS gpt 
ON 
	enc.raw_encounter_type = gpt.src_value 
AND 
	gpt.src_field = 'raw_encounter_type'
WHERE 
	gpt.mapped_value = 'inpatient';
	
 --drop table inpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO TEMPORARY
	inpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types') and date<='2023-12-31'::date
AND EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
--drop table a1c_results;
select 
	patient_id,
	date,
	'a1c' as event_type
into temporary
	a1c_results
from
	hef_event h
where 
	name = 'lx:a1c:threshold:gte:6.5';
    
--drop table fasting_glucose;
select 
	patient_id,
	date,
	'fasting glucose' as event_type
into temporary
	fasting_glucose
from
	hef_event h 
WHERE 
	name in ('lx:glucose-fasting:threshold:gte:126','lx:ogtt50-fasting:threshold:gte:126','lx:ogtt75-fasting:threshold:gte:126','lx:ogtt100-fasting:threshold:gte:126');
    
--drop table random_glucose;
select 
	patient_id,
	date,
	'random glucose' as event_type
into temporary
	random_glucose
from
	hef_event h 
where 
	name in ('lx:glucose-random:threshold:gte:200','lx:ogtt50-random:threshold:gte:200');
 
 --drop table outpatient_dx;	
SELECT --distinct
	h.patient_id, 
	h.date
INTO TEMPORARY
	outpatient_dx
FROM 
	hef_event h
WHERE 
	name in ('dx:diabetes:all-types')
AND NOT EXISTS
	(SELECT NULL FROM inpat_days as indays WHERE indays.patient_id = h.patient_id AND h.date BETWEEN indays.start_date AND indays.end_date);
    
 
--drop table medication_disp 
SELECT patient_id, 
	date,
	name
INTO TEMPORARY
	medication_disp
FROM 
	hef_event AS h 
WHERE 
	lower(name)
	IN 
	(
	'rx:glyburide',
        'rx:gliclazide',
        'rx:glipizide',
        'rx:glimepiride',
        'rx:pioglitazone',
        'rx:rosiglitizone',
        'rx:repaglinide',
        'rx:nateglinide',
        'rx:meglitinide',
        'rx:sitagliptin',
        'rx:exenatide',
        'rx:pramlintide'
	);
    
--drop table diab_2_events;
 select patient_id, min(date) as date
 into temporary diab_2_events
 from (
 select t0.*, lead(date) over (partition by patient_id order by patient_id, date) as next_date,
    lead(crit) over (partition by patient_id order by patient_id, date, crit) as next_crit
 from (
 select patient_id, date, 'a1c' as crit from a1c_results
 union select patient_id, date, 'fast_gluc' as crit from fasting_glucose
 union select patient_id, date, 'rand_gluc' as crit from random_glucose
 union select patient_id, date, 'outp_diag' as crit from outpatient_dx
 union select patient_id, date, 'antihyperg' as crit from medication_disp) t0) t00
 where (date_part('Year',next_date)-date_part('Year',date))*12+(date_part('Year',next_date)-date_part('Year',date)) between 0 and 24
 and (date<next_date or (date<=next_date and crit<>next_crit))
 and next_date<='2022-12-31'::date
 group by patient_id;
 
 --drop table diab_pats;
select patient_id, min(date) as date, 'diabetic' as type
 into temporary diab_pats
 from (
 select patient_id, date from diab_2_events
 union select patient_id, date from inpatient_dx) t0
 group by patient_id;
 
select distinct on (patient_id) patient_id, date, name
into temporary hef_a1c
from hef_event
where name in ('lx:a1c:range:gte:8:lte:9','lx:a1c:threshold:gt:9','lx:a1c:threshold:lt:7','lx:a1c:range:gte:7:lt:8')
order by patient_id, date desc,
  case
    when name='lx:a1c:threshold:lt:7' then 1
    when name='lx:a1c:range:gte:7:lt:8' then 2
    when name='lx:a1c:range:gte:8:lte:9' then 3
    when name='lx:a1c:threshold:gt:9' then 4
  end;

select distinct on (patient_id) patient_id, date, name
into temporary last_hef_a1c
from hef_a1c
order by patient_id, date desc, name;

select id as patient_id, DATE_PART('YEAR', AGE('2022-12-31'::date, date_of_birth)) as AGE
into temporary pat_w_age
from emr_patient p
where DATE_PART('YEAR', AGE('2023-12-31'::date, date_of_birth)) between 4 and 85
  and exists (select null from gen_pop_tools.clin_enc ce 
              where ce.patient_id=p.id and ce.date between '2020-12-31'::date and '2022-12-31'::date);

select t0.patient_id, coalesce(type, 'Not diabetic'::varchar(50)) condition,
    coalesce(name,'Not tested') control
into temporary diab_assessed_control
from pat_w_age t01
left join diab_pats t0 on t0.patient_id=t01.patient_id
left join last_hef_a1c t1 on t1.patient_id=t01.patient_id and (t1.date>=t0.date or t0.date is null) ;


-- Creating lab_over_65 temporary table
SELECT patient_id, 'lab results > 6.5'
INTO TEMPORARY lab_over_65
FROM emr_labresult t0
JOIN conf_labtestmap t1 ON t1.native_code = t0.native_code AND t1.test_name = 'a1c'
WHERE result_float >= 6.5 AND date < '2022-12-31'::Date
GROUP BY patient_id
HAVING COUNT(*) = 1;

SELECT
    subquery.age_group,
    COUNT(*) AS counts,
    subquery.condition,
    subquery.control,
    CASE 
        WHEN subquery.a1c_value < 7 THEN '<7'
        WHEN subquery.a1c_value >= 7 AND subquery.a1c_value < 8 THEN '7-8'
        WHEN subquery.a1c_value >= 8 AND subquery.a1c_value < 9 THEN '8-9'
        WHEN subquery.a1c_value >= 9 THEN '>=9'
        ELSE 'Not tested'
    END AS a1c_category
FROM 
    (
        SELECT 
            dac.patient_id,
            dac.condition,
            dac.control,
            pwa.age_group,
            hef.a1c_value
        FROM 
            #diab_assessed_control dac
        JOIN 
            #pat_w_age pwa ON dac.patient_id = pwa.patient_id
        LEFT JOIN 
            #last_hef_a1c hef ON dac.patient_id = hef.patient_id
        LEFT JOIN
            #hef_results hr ON dac.patient_id = hr.patient_id
    ) subquery
GROUP BY 
    subquery.age_group, subquery.condition, subquery.control, subquery.a1c_category
ORDER BY 
    subquery.age_group, subquery.condition, subquery.a1c_category;
