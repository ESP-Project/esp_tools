--query 1-2c
With 
T0 as (
  select 13 as min_age,
         55 as max_age
  )
,T1 as (
  select distinct patient_id, to_char(date,'yyyy') clin_enc_year
  from gen_pop_tools.clin_enc )
,T2 as (
  Select count(*) all_pat_counts, 
         count(case when cm.mapped_value='F' then p.id else null end) female_pat_counts,
    t1.clin_enc_year
  From emr_patient p
  join t1 on p.id=t1.patient_id
  join t0 on 1=1
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  WHERE date_part('year', age(to_date(t1.clin_enc_year,'yyyy'), p.date_of_birth)) between t0.min_age and t0.max_age
  GROUP BY t1.clin_enc_year )
,T3 as (
  Select count(distinct ht.patient_id) all_tmspn_preg_counts, 
    count(distinct case when cm.mapped_value='M' then ht.patient_id else null end) male_tmspn_preg_counts,
    count(distinct case when cm.mapped_value='F' then ht.patient_id else null end) female_tmspn_preg_counts,
    count(distinct case when cm.mapped_value not in ('M','F') OR cm.mapped_value IS NULL then ht.patient_id else null end) unkn_tmspn_preg_counts,
    to_char(ht.start_date, 'yyyy') preg_year
  From emr_patient p
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  JOIN hef_timespan ht ON p.id=ht.patient_id and ht.name='pregnancy'
  JOIN t0 on 1=1
  WHERE date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age 
  GROUP BY to_char(ht.start_date, 'yyyy') )
,T4 as (
  SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_95th_percentile,
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_95th_percentile,
    to_char(ht.start_date, 'yyyy') preg_year
  FROM hef_timespan ht
  JOIN emr_patient p ON p.id = ht.patient_id
  JOIN t0 ON 1=1
  WHERE ht.name = 'pregnancy'
  AND date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age
  group by to_char(ht.start_date, 'yyyy') )
,T5 as (
  Select count(distinct ht.patient_id) all_ht_preg_counts,
    count(distinct case when cm.mapped_value='M' then ht.patient_id else null end) male_ht_preg_counts,
    count(distinct case when cm.mapped_value='F' then ht.patient_id else null end) female_ht_preg_counts,
    count(distinct case when cm.mapped_value not in ('M','F') then ht.patient_id else null end) unkn_ht_preg_counts,
    to_char(ht.date, 'yyyy') preg_year
  From emr_patient p
  JOIN hef_event ht ON p.id=ht.patient_id and ht.name='dx:pregnancy'
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  JOIN t0 ON 1=1
  WHERE date_part('year', age(ht.date, p.date_of_birth)) between t0.min_age and t0.max_age
  GROUP BY to_char(ht.date, 'yyyy') )
,T6 as (
  SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.date, p.date_of_birth)) AS age_ht_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.date, p.date_of_birth)) AS age_ht_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.date, p.date_of_birth)) AS age_ht_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.date, p.date_of_birth)) AS age_ht_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.date, p.date_of_birth)) AS age_ht_95th_percentile,
    to_char(ht.date, 'yyyy') preg_year
  FROM hef_event ht
  JOIN emr_patient p ON p.id = ht.patient_id
  JOIN t0 ON 1=1
  WHERE ht.name = 'dx:pregnancy'
  AND date_part('year', age(ht.date, p.date_of_birth)) between t0.min_age and t0.max_age
  group by to_char(ht.date, 'yyyy') )
select all_pat_counts, 
       female_pat_counts,
       all_tmspn_preg_counts, 
       male_tmspn_preg_counts,
       female_tmspn_preg_counts,
       unkn_tmspn_preg_counts,
       age_tmspn_5th_percentile,
	   age_tmspn_25th_percentile,
	   age_tmspn_50th_percentile,
	   age_tmspn_75th_percentile,
	   age_tmspn_95th_percentile,
	   dur_tmspn_5th_percentile,
	   dur_tmspn_25th_percentile,
	   dur_tmspn_50th_percentile,
	   dur_tmspn_75th_percentile,
	   dur_tmspn_95th_percentile,
       all_ht_preg_counts,
       male_ht_preg_counts,
       female_ht_preg_counts,
       unkn_ht_preg_counts,
       age_ht_5th_percentile,
	   age_ht_25th_percentile,
	   age_ht_50th_percentile,
	   age_ht_75th_percentile,
	   age_ht_95th_percentile,
       t3.preg_year
--INTO temporary preg_results1
from T2 
join t3 on t2.clin_enc_year=t3.preg_year
join t4 on t2.clin_enc_year=t4.preg_year
join t5 on t2.clin_enc_year=t5.preg_year
join t6 on t2.clin_enc_year=t6.preg_year;


-- Query 2d:  Heat map of icd codes for pregnancy start
WITH t0 AS
(
	select ht.id, min(he.date) min_he_date, max(he.date) max_he_date from hef_timespan ht
	join hef_timespan_events hte 
	ON hte.timespan_id = ht.id
	join hef_event he
	ON he.id = hte.event_id
	WHERE ht.name = 'pregnancy'
	GROUP BY ht.id
)
, t1 as
(
   select combotypecode as dx_code_id, name
   from static_dx_code dx where  
	(dx.combotypecode ~ 'icd10:O09\.*'  -- Table 1 Start
      OR dx.combotypecode ~ 'icd10:O26\.2.*'
      OR dx.combotypecode ~ 'icd10:O30\..*'
      OR dx.combotypecode ~ 'icd10:O31\.' 
      OR dx.combotypecode ~ 'icd10:O31\.1.*'
      OR dx.combotypecode ~ 'icd10:O31\.2.*'
      OR dx.combotypecode ~ 'icd10:O31\.3.*'
      OR dx.combotypecode ~ 'icd10:O32\..*'
      OR dx.combotypecode ~ 'icd10:O33\..*'
      OR dx.combotypecode ~ 'icd10:O36\.7.*'
      OR dx.combotypecode ~ 'icd10:Z33\..*'
      OR dx.combotypecode ~ 'icd10:Z34\..*'
      OR dx.combotypecode ~ 'icd10:O10\..\d[1]' 
	OR dx.combotypecode ~ 'icd10:O11\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O12\..\d[45]'
	OR dx.combotypecode ~ 'icd10:O13\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O14\..\d[45]'
	OR dx.combotypecode = 'icd10:O15'
	OR dx.combotypecode ~ 'icd10:O15\.0.*'
	OR dx.combotypecode ~ 'icd10:O15\.9.*'
	OR dx.combotypecode ~ 'icd10:O16\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O20\..*'
	OR dx.combotypecode ~ 'icd10:O21\..*'
	OR dx.combotypecode ~ 'icd10:O22\..*'
	OR dx.combotypecode ~ 'icd10:O23\..*'
	OR dx.combotypecode ~ 'icd10:O24\..\d[1]'
	OR dx.combotypecode ~ 'icd10:O25\.\d[1].'
	OR dx.combotypecode ~ 'icd10:O26\..*'
	OR dx.combotypecode ~ 'icd10:O28\.*'
	OR dx.combotypecode ~ 'icd10:O29\.*'
	OR dx.combotypecode ~ 'icd10:O31\.8.*'
	OR dx.combotypecode ~ 'icd10:O34\..*'
	OR dx.combotypecode ~ 'icd10:O35\..*'
	OR dx.combotypecode ~ 'icd10:O36\.6.*'
	OR dx.combotypecode ~ 'icd10:O36\.8.*'
	OR dx.combotypecode ~ 'icd10:O37\..*'
	OR dx.combotypecode ~ 'icd10:O38\..*'
	OR dx.combotypecode ~ 'icd10:O39\..*'
	OR dx.combotypecode ~ 'icd10:O40\..*'
	OR dx.combotypecode ~ 'icd10:O41\..*'
	OR dx.combotypecode ~ 'icd10:O43\..*'
	OR dx.combotypecode ~ 'icd10:O44\..*'
	OR dx.combotypecode ~ 'icd10:O45\..*'
	OR dx.combotypecode ~ 'icd10:O46\..*'
	OR dx.combotypecode ~ 'icd10:O47\..*'
	OR dx.combotypecode ~ 'icd10:O48\..*'
	OR dx.combotypecode ~ 'icd10:O60\.O.*'
	OR dx.combotypecode ~ 'icd10:O99\.01.*'
	OR dx.combotypecode ~ 'icd10:O99\.11.*'
	OR dx.combotypecode ~ 'icd10:O99\.2.*.*'
	OR dx.combotypecode ~ 'icd10:O99\.3.*.*'
	OR dx.combotypecode ~ 'icd10:O99\.4\d[1]'
	OR dx.combotypecode ~ 'icd10:O99\.5\d[1]'
	OR dx.combotypecode ~ 'icd10:O99\.6\d[1]'
	OR dx.combotypecode ~ 'icd10:O99\.7\d[1]'
	OR dx.combotypecode ~ 'icd10:O99\.8.*\d[45]'
	OR dx.combotypecode ~ 'icd10:O99\.9.*\d[45]'
	OR dx.combotypecode ~ 'icd10:O9A\.1\d[1]'
	OR dx.combotypecode ~ 'icd10:O9A\.2\d[1]'
	OR dx.combotypecode ~ 'icd10:O9A\.3\d[1]'
	OR dx.combotypecode ~ 'icd10:O9A\.4\d[1]'
	OR dx.combotypecode ~ 'icd10:O9A\.5\d[1]'
	-- Table 3 Start
	OR dx.combotypecode ~ 'icd10:O00\..*'
	OR dx.combotypecode ~ 'icd10:O01\..*'
	OR dx.combotypecode ~ 'icd10:O02\..*'
	OR dx.combotypecode ~ 'icd10:O03\..*'
	OR dx.combotypecode ~ 'icd10:O08\..*'
	OR dx.combotypecode ~ 'icd10:O10\..*\d[23]'
	OR dx.combotypecode ~ 'icd10:O11\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O12\..*\d[45]'
	OR dx.combotypecode ~ 'icd10:O13\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O14\..*\d[45]'
	OR dx.combotypecode ~ 'icd10:O15\.1'
	OR dx.combotypecode ~ 'icd10:O15\.2'
	OR dx.combotypecode ~ 'icd10:O16\.\d[45]'
	OR dx.combotypecode ~ 'icd10:O24\..*\[23]'
	OR dx.combotypecode ~ 'icd10:O25\.\d[23].*'
	OR dx.combotypecode ~ 'icd10:O26\.62'
	OR dx.combotypecode ~ 'icd10:O26\.63'
	OR dx.combotypecode ~ 'icd10:O26\.72'
	OR dx.combotypecode ~ 'icd10:O26\.73'
	OR dx.combotypecode ~ 'icd10:O42\..*'
	OR dx.combotypecode ~ 'icd10:O60\.1.*'
	OR dx.combotypecode ~ 'icd10:O60\.2.*'
	OR dx.combotypecode ~ 'icd10:O6\d[789]\..*'
	OR dx.combotypecode ~ 'icd10:O7.*\.'
	OR dx.combotypecode ~ 'icd10:O8.*\.'
	OR dx.combotypecode ~ 'icd10:O90\..*'
	OR dx.combotypecode ~ 'icd10:Z37\..*'
	OR dx.combotypecode ~ 'icd10:Z39\..*'
	--Table 3 End
	-- Table 4 Start
    OR dx.combotypecode ~ 'icd10:Z3A\.08'  
    OR dx.combotypecode ~ 'icd10:Z3A\.09'
    OR dx.combotypecode ~ 'icd10:Z3A\.1.*'
    OR dx.combotypecode ~ 'icd10:Z3A\.2.*'
    OR dx.combotypecode ~ 'icd10:Z3A\.3.*'
    OR dx.combotypecode ~ 'icd10:Z3A\.4.*'
      )
	AND (dx.combotypecode <> 'icd10:Z33.2' --table 1 exclusion
      AND dx.combotypecode !~ 'icd10:O26\.2.*' --table 2 exclusions
      AND dx.combotypecode <> 'icd10:O26.62'
      AND dx.combotypecode <> 'icd10:O26.63'
      AND dx.combotypecode <> 'icd10:O26.72'
      AND dx.combotypecode <> 'icd10:O26.73'
      AND dx.combotypecode !~ 'icd10:O36\.4.*'
      AND dx.combotypecode !~ 'icd10:O36.80.*'
      AND dx.combotypecode !~ 'icd10:O99\.2.*\d[45]'
      AND dx.combotypecode !~ 'icd10:O99\.3.*\d[45]'
     ) 
)
,t2 as
(
  select encounter_id, dx_code_id
  from emr_encounter_dx_codes
  where dx_code_id in (select dx_code_id from t1)
)
SELECT count(case when t0.min_he_date = he.date
			 then dx.encounter_id else null end)
			 strt_cts,
			 count(case when t0.min_he_date = he.date AND p.gender ilike 'M%'
			 then dx.encounter_id else null end)
			 male_strt_cts,
			 count(case when t0.min_he_date = he.date AND p.gender ilike 'F%'
			 then dx.encounter_id else null end)
			 female_strt_cts,
			 count(case when t0.max_he_date = he.date
			 then dx.encounter_id else null end)
			 end_cts,
			 dx.dx_code_id, dxc.name
--INTO temporary preg_results2
FROM hef_timespan ht
JOIN hef_timespan_events hte
ON hte.timespan_id = ht.id
JOIN hef_event he
ON hte.event_id = he.id AND he.name ILIKE 'dx:%'
JOIN t0
ON t0.id = ht.id AND 
	(t0.min_he_date = he.date OR t0.max_he_date = he.date)
JOIN t2 dx
join t1 dxc on dx.dx_code_id=dxc.dx_code_id
ON dx.encounter_id = he.object_id
JOIN emr_patient p 
ON p.id = ht.patient_id
WHERE date_part('year', age(ht.start_date, p.date_of_birth)) between 13 and 55
GROUP BY dx.dx_code_id, dxc.name
ORDER BY dx.dx_code_id desc;


\COPY preg_results1 TO '/srv/esp/esp_tools/sql_reports/MENDS/Pregnancy/pregnancy_exploratory_eval1.csv' DELIMITER ',' CSV HEADER;
\COPY preg_results2 TO '/srv/esp/esp_tools/sql_reports/MENDS/Pregnancy/pregnancy_exploratory_eval2.csv' DELIMITER ',' CSV HEADER;
