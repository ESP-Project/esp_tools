/*
Exploratory evaluation of pregnancy data in MENDS
December 11, 2023

Please provide the following:
1.	Count of people with pregnancy overall, per year, and per site
a.	Use start-of-pregnancy from pregnancy timespan data to assign to year
b.	Use concept of tmp_preg_seq from diagnosed hypertension – start date here.

2.	Amongst pregnancy episodes stratified by year, by site: (questions a-d can have a timespan definition and a tmp_preg_seq definition)
a.	Sex distribution (no, %)
b.	Age (5th percentile, 25th percentile, median, 75th percentile, 95th percentile)
c.	Duration of pregnancy (5th percentile, 25th, median, 75th, 95th percentile)
d.	Frequency table of the codes being used to define pregnancy start
	i.	Codes, code text, and counts
e.	Frequency table of the codes being used to define pregnancy end
	i.	Codes, code text, and counts
f.  Max and Min ranges for all pregnant patients and get a count of all patients within that age range within entire population
g.  And a count of all females within entire population as a denominator

Pseudo code from Bob Z: 
*/

--run at each site
--this uses timespans

/*
1.	Count of people with pregnancy overall, per year, and per site
a.	Use start-of-pregnancy from pregnancy timespan data to assign to year
*/
Select count(distinct ht.patient_id) counts, 
  --Percentile_cont(.05) within group (order by date_part('year' from age(current_date, p.date_of_birth::date),
  to_char(ht.start_date, 'yyyy') preg_year,  
  p.gender
From hef_timespan ht
JOIN emr_patient p ON p.id=ht.patient_id
WHERE ht.name='pregnancy'
GROUP BY to_char(ht.start_date, 'yyyy'), p.gender;														

/*
b.	Use concept of tmp_preg_seq from diagnosed hypertension – start date here.
*/
--this uses tmp_preg_seq concept from diagnosed hypertension plugin
Select count(distinct he.patient_id) counts, to_char(he.date, 'yyyy') preg_year,
  p.gender
From hef_event he
Join emr_patient p on p.id=he.patient_id
Where he.name='dx:pregnancy'
Group by to_char(he.date, 'yyyy'), p.gender;



/* My Code */

-- Query 2a:  Sex

WITH total_counts AS (
	SELECT
		COUNT(DISTINCT ht.patient_id) total_count
	FROM
		hef_timespan ht
	WHERE
		ht.name = 'pregnancy'
)
SELECT 
	p.gender,
	COUNT(DISTINCT ht.patient_id) AS count,
	ROUND(COUNT(DISTINCT ht.patient_id) * 100.0 / total_counts.total_count, 2) AS percentage
FROM
	hef_timespan ht
JOIN
	emr_patient p ON p.id = ht.patient_id
CROSS JOIN
	total_counts
WHERE
	ht.name = 'pregnancy'
GROUP BY
	p.gender, total_counts.total_count;
	
-- Query 2a:  Sex (hypertension)
WITH total_counts AS (
	SELECT
		COUNT(DISTINCT he.patient_id) total_count
	FROM
		hef_event he
	WHERE
		he.name = 'dx:pregnancy'
)
SELECT
	p.gender,
	COUNT(DISTINCT he.patient_id) AS count,
	ROUND((COUNT(DISTINCT he.patient_id) * 100.0) / total_counts.total_count, 2) AS percentage 
FROM
	hef_event he
JOIN
	emr_patient p ON p.id = he.patient_id
CROSS JOIN
	total_counts
WHERE
	he.name = 'dx:pregnancy'
GROUP BY
	p.gender, total_counts.total_count;
	
-- Query 2b: Age by percentile 

SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_95th_percentile
FROM
	hef_timespan ht
JOIN
	emr_patient p ON p.id = ht.patient_id
WHERE
	ht.name = 'pregnancy';

-- Query 2b: Age by percentile (hypertension)
SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(he.date, p.date_of_birth)) AS age_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(he.date, p.date_of_birth)) AS age_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(he.date, p.date_of_birth)) AS age_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(he.date, p.date_of_birth)) AS age_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(he.date, p.date_of_birth)) AS age_95th_percentile
FROM
	hef_event he
JOIN
	emr_patient p ON p.id = he.patient_id
WHERE
	he.name = 'dx:pregnancy';
	
-- Query 2c:  Duration 
SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_95th_percentile
FROM
	hef_timespan ht
JOIN
	emr_patient p ON p.id = ht.patient_id
WHERE
	ht.name = 'pregnancy';

-- Query 2c:  Duration (hypertension) 
SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_5th_percentile,
	PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_25th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_50th_percentile,
	PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_75th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS age_95th_percentile
FROM
	hef_timespan ht
JOIN
	emr_patient p ON p.id = ht.patient_id
WHERE
	he.name = 'pregnancy';

-- Query 2d:  Heat map of icd codes for pregnancy start
WITH t0 AS
(
	select ht.id, ht.start_date, min(he.date) he_date from hef_timespan ht
	join hef_timespan_events hte 
	ON hte.timespan_id = ht.id
	join hef_event he
	ON he.id = hte.event_id
	WHERE ht.name = 'pregnancy'
	GROUP BY ht.id, ht.start_date
)
SELECT count(distinct e.id) cts, dx.dx_code_id, dxc.name
FROM hef_timespan ht
JOIN hef_timespan_events hte
ON hte.timespan_id = ht.id
JOIN hef_event he
ON hte.event_id = he.id AND he.name ILIKE 'dx:%'
JOIN t0
ON t0.id = ht.id AND t0.he_date = he.date
JOIN emr_encounter e
ON he.object_id = e.id
JOIN emr_encounter_dx_codes dx
ON dx.encounter_id = e.id
JOIN static_dx_code dxc
ON dxc.combotypecode = dx.dx_code_id
WHERE 
	dx.dx_code_id ~ 'icd10:O09\.*'  -- Table 1 Start
	OR dx.dx_code_id ~ 'icd10:O26\.2.*'
	OR dx.dx_code_id ~ 'icd10:O30\..*'
	OR dx.dx_code_id ~ 'icd10:O31\.' 
	OR dx.dx_code_id ~ 'icd10:O31\.1.*'
	OR dx.dx_code_id ~ 'icd10:O31\.2.*'
	OR dx.dx_code_id ~ 'icd10:O31\.3.*'
	OR dx.dx_code_id ~ 'icd10:O32\..*'
	OR dx.dx_code_id ~ 'icd10:O33\..*'
	OR dx.dx_code_id ~ 'icd10:O36\.7.*'
	OR dx.dx_code_id ~ 'icd10:Z33\..*'
		AND dx.dx_code_id <> 'icd10:Z33.2'
	OR dx.dx_code_id ~ 'icd10:Z34\..*'  -- Table 1 End
	OR dx.dx_code_id ~ 'icd10:O10\..\d[1]' -- Table 2 Start
	OR dx.dx_code_id ~ 'icd10:O11\.\d[45]'
	OR dx.dx_code_id ~ 'icd10:O12\..\d[45]'
	OR dx.dx_code_id ~ 'icd10:O13\.\d[45]'
	OR dx.dx_code_id ~ 'icd10:O14\..\d[45]'
	OR dx.dx_code_id = 'icd10:O15'
	OR dx.dx_code_id ~ 'icd10:O15\.0.*'
	OR dx.dx_code_id ~ 'icd10:O15\.9.*'
	OR dx.dx_code_id ~ 'icd10:O16\.\d[45]'
	OR dx.dx_code_id ~ 'icd10:O20\..*'
	OR dx.dx_code_id ~ 'icd10:O21\..*'
	OR dx.dx_code_id ~ 'icd10:O22\..*'
	OR dx.dx_code_id ~ 'icd10:O23\..*'
	OR dx.dx_code_id ~ 'icd10:O24\..\d[1]'
	OR dx.dx_code_id ~ 'icd10:O25\.\d[1].'
	OR dx.dx_code_id ~ 'icd10:O26\..*'
		AND (dx.dx_code_id !~ 'icd10:O26\.2.*'
			 OR dx.dx_code_id <> 'icd10:O26\.62'
			 OR dx.dx_code_id <> 'icd10:O26\.63'
			 OR dx.dx_code_id <> 'icd10:O26\.72'
			 OR dx.dx_code_id <> 'icd10:O26\.73')
	OR dx.dx_code_id ~ 'icd10:O28\.*'
	OR dx.dx_code_id ~ 'icd10:O29\.*'
	OR dx.dx_code_id ~ 'icd10:O31\.8.*'
	OR dx.dx_code_id ~ 'icd10:O34\..*'
	OR dx.dx_code_id ~ 'icd10:O35\..*'
	OR dx.dx_code_id ~ 'icd10:O36\.6.*'
		AND dx.dx_code_id !~ 'icd10:O36\.4.*'
	OR dx.dx_code_id ~ 'icd10:O36\.8.*'
	OR dx.dx_code_id ~ 'icd10:O37\..*'
	OR dx.dx_code_id ~ 'icd10:O38\..*'
	OR dx.dx_code_id ~ 'icd10:O39\..*'
	OR dx.dx_code_id ~ 'icd10:O40\..*'
	OR dx.dx_code_id ~ 'icd10:O41\..*'
	AND dx.dx_code_id !~ 'icd10:O36.80.*'
	OR dx.dx_code_id ~ 'icd10:O43\..*'
	OR dx.dx_code_id ~ 'icd10:O44\..*'
	OR dx.dx_code_id ~ 'icd10:O45\..*'
	OR dx.dx_code_id ~ 'icd10:O46\..*'
	OR dx.dx_code_id ~ 'icd10:O47\..*'
	OR dx.dx_code_id ~ 'icd10:O48\..*'
	OR dx.dx_code_id ~ 'icd10:O60\.O.*'
	OR dx.dx_code_id ~ 'icd10:O99\.01.*'
	OR dx.dx_code_id ~ 'icd10:O99\.11.*'
	OR dx.dx_code_id ~ 'icd10:O99\.2.*.*'
		AND dx.dx_code_id !~ 'icd10:O99\.2.*\d[45]'
	OR dx.dx_code_id ~ 'icd10:O99\.3.*.*'
		AND dx.dx_code_id !~ 'icd10:O99\.3.*\d[45]'
	OR dx.dx_code_id ~ 'icd10:O99\.4\d[1]'
	OR dx.dx_code_id ~ 'icd10:O99\.5\d[1]'
	OR dx.dx_code_id ~ 'icd10:O99\.6.*'
	OR dx.dx_code_id ~ 'icd10:O99\.7.*'
	OR dx.dx_code_id ~ 'icd10:Z3A\.08'  -- Table 4 Start
	OR dx.dx_code_id ~ 'icd10:Z3A\.09'
	OR dx.dx_code_id ~ 'icd10:Z3A\.1.*'
	OR dx.dx_code_id ~ 'icd10:Z3A\.2.*'
	OR dx.dx_code_id ~ 'icd10:Z3A\.3.*'
	OR dx.dx_code_id ~ 'icd10:Z3A\.4.*' -- Table 4 End
GROUP BY dx.dx_code_id, dxc.name
ORDER BY cts desc;

-- Query 2e:  Heat map of icd codes for pregnancy end
WITH t0 AS
(
	select ht.id, ht.start_date, max(he.date) he_date from hef_timespan ht
	join hef_timespan_events hte 
	ON hte.timespan_id = ht.id
	join hef_event he
	ON he.id = hte.event_id
	WHERE ht.name = 'pregnancy'
	GROUP BY ht.id, ht.start_date
)
SELECT count(distinct e.id) cts, dx.dx_code_id, dxc.name
FROM hef_timespan ht
JOIN hef_timespan_events hte
ON hte.timespan_id = ht.id
JOIN hef_event he
ON hte.event_id = he.id AND he.name ILIKE 'dx:%'
JOIN t0
ON t0.id = ht.id AND t0.he_date = he.date
JOIN emr_encounter e
ON he.object_id = e.id
JOIN emr_encounter_dx_codes dx
ON dx.encounter_id = e.id
JOIN static_dx_code dxc
ON dxc.combotypecode = dx.dx_code_id
WHERE 
	dx.dx_code_id ~ 'icd9:V27\..*'
	OR dx.dx_code_id ~ 'icd9:V24\..*' --if followed by preg ICD9 V22.x or V23.x or ICD10 on Preg list within 30 days then ignore
	OR 
GROUP BY dx.dx_code_id, dxc.name
ORDER BY cts desc;
	
