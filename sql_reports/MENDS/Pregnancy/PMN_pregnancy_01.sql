drop table if exists gen_pop_tools.pmn_pre_pregnancy_01_2025;
with t01 as (
    --subset the Dx codes we will want to use.  
    select patient_id, date, 
      case
       when (upper(dx.dx_code_id) like 'ICD10:Z38%' or upper(dx.dx_code_id) = 'ICD10:Z37.0' or upper(dx.dx_code_id) = 'ICD10:Z37.2' or upper(dx.dx_code_id) like 'ICD10:Z37.5%') then 'live birth'
       when (upper(dx.dx_code_id) = 'ICD10:Z37.1' or upper(dx.dx_code_id) = 'ICD10:Z37.3' or upper(dx.dx_code_id) = 'ICD10:Z37.4' or upper(dx.dx_code_id) like 'ICD10:Z37.6%' 
           or upper(dx.dx_code_id) = 'ICD10:Z37.7')  then 'still birth'
       when (upper(dx.dx_code_id) like 'ICD10:O00%' or upper(dx.dx_code_id) like 'ICD10:O01%' or upper(dx.dx_code_id) like 'ICD10:O02%') then 'ectopic, molar, other abnormal pregnancy' 
       when (upper(dx.dx_code_id) like 'ICD10:O03%' or upper(dx.dx_code_id) like 'ICD10:O04%' or upper(dx.dx_code_id) like 'ICD10:O05%' or upper(dx.dx_code_id) like 'ICD10:O06%') then 'abortion' 
       when (upper(dx.dx_code_id) like 'ICD10:O80%' or upper(dx.dx_code_id) like 'ICD10:O81%' or upper(dx.dx_code_id) like 'ICD10:O82%') then 'delivery'
       when upper(dx.dx_code_id) like 'ICD10:Z3A%' then 'gestational age'
       when upper(dx.dx_code_id) like 'ICD10:O07%' then 'failed attempted termination of pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O08%' then 'complictaions after ectopic, molar, or other aborted pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O09%' then 'supervision of high risk pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O10%' then 'preexisting hypertension complicating pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O11%' then 'pre-eclampsia that occurs on top of chronic hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O12%' then 'gestational edema and proteinuria without hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O13%' then 'gestational hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O14%' then 'pre-eclampsia'
       when upper(dx.dx_code_id) like 'ICD10:O15%' then 'eclampsia'
       when upper(dx.dx_code_id) like 'ICD10:O16%' then 'Unspecified maternal hypertension'
       when (upper(dx.dx_code_id) like 'ICD10:O20%' or upper(dx.dx_code_id) like 'ICD10:O21%' or upper(dx.dx_code_id) like 'ICD10:O22%' or upper(dx.dx_code_id) like 'ICD10:O23%'
           or upper(dx.dx_code_id) like 'ICD10:O25%' or upper(dx.dx_code_id) like 'ICD10:O26%' or upper(dx.dx_code_id) like 'ICD10:O28%' or upper(dx.dx_code_id) like 'ICD10:O29%'
           or upper(dx.dx_code_id) like 'ICD10:O3%' or upper(dx.dx_code_id) like 'ICD10:O4%' or upper(dx.dx_code_id) like 'ICD10:O6%' or upper(dx.dx_code_id) like 'ICD10:O7%'
           or upper(dx.dx_code_id) like 'ICD10:O85%' or upper(dx.dx_code_id) like 'ICD10:O86%' or upper(dx.dx_code_id) like 'ICD10:O87%' or upper(dx.dx_code_id) like 'ICD10:O88%'
           or upper(dx.dx_code_id) like 'ICD10:O89%' or upper(dx.dx_code_id) like 'ICD10:O90%' or upper(dx.dx_code_id) like 'ICD10:O91%' or upper(dx.dx_code_id) like 'ICD10:O92%'
            or upper(dx.dx_code_id) like 'ICD10:O94%' or upper(dx.dx_code_id) like 'ICD10:O98%' or upper(dx.dx_code_id) like 'ICD10:O99%' or upper(dx.dx_code_id) like 'ICD10:O9A%'
            ) then 'various complications of pregnancy and delivery'
       when upper(dx.dx_code_id) like 'ICD10:O24%' then 'diabetes during pregnancy'
       when (upper(dx.dx_code_id) like 'ICD10:Z33%' or upper(dx.dx_code_id) like 'ICD10:Z34%' or upper(dx.dx_code_id) like 'ICD10:Z36%')    then 'pregnancy or pregnancy related encounter coincidental with other dx'
      end as datatype       
    from emr_encounter e join emr_encounter_dx_codes dx on dx.encounter_id=e.id
    where (upper(dx.dx_code_id) like 'ICD10:Z38%' or upper(dx.dx_code_id) = 'ICD10:Z37.0' or upper(dx.dx_code_id) = 'ICD10:Z37.2' or upper(dx.dx_code_id) like 'ICD10:Z37.5%') --live birth
       or (upper(dx.dx_code_id) = 'ICD10:Z37.1' or upper(dx.dx_code_id) = 'ICD10:Z37.3' or upper(dx.dx_code_id) = 'ICD10:Z37.4' or upper(dx.dx_code_id) like 'ICD10:Z37.6%' or upper(dx.dx_code_id) = 'ICD10:Z37.7') --still birth
       or (upper(dx.dx_code_id) like 'ICD10:O00%' or upper(dx.dx_code_id) like 'ICD10:O01%' or upper(dx.dx_code_id) like 'ICD10:O02%') --ectopic, molar, other abnormal pregnancy 
       or (upper(dx.dx_code_id) like 'ICD10:O03%' or upper(dx.dx_code_id) like 'ICD10:O04%' or upper(dx.dx_code_id) like 'ICD10:O05%' or upper(dx.dx_code_id) like 'ICD10:O06%') -- abortion 
       or (upper(dx.dx_code_id) like 'ICD10:O80%' or upper(dx.dx_code_id) like 'ICD10:O81%' or upper(dx.dx_code_id) like 'ICD10:O82%') --delivery
       or upper(dx.dx_code_id) like 'ICD10:Z3A%' -- gestational age
       or upper(dx.dx_code_id) like 'ICD10:O07%' -- failed attempted termination of pregnancy
       or upper(dx.dx_code_id) like 'ICD10:O08%' -- complictaions after ectopic, molar, or other aborted pregnancy
       or upper(dx.dx_code_id) like 'ICD10:O09%' -- supervision of high risk pregnancy
       or upper(dx.dx_code_id) like 'ICD10:O10%' -- preexisting hypertension complicating pregnancy
       or upper(dx.dx_code_id) like 'ICD10:O11%' -- pre-eclampsia that occurs on top of chronic hypertension
       or upper(dx.dx_code_id) like 'ICD10:O12%' -- gestational edema and proteinuria without hypertension
       or upper(dx.dx_code_id) like 'ICD10:O13%' -- gestational hypertension
       or upper(dx.dx_code_id) like 'ICD10:O14%' -- pre-eclampsia
       or upper(dx.dx_code_id) like 'ICD10:O15%' -- eclampsia
       or upper(dx.dx_code_id) like 'ICD10:O16%' -- Unspecified maternal hypertension   
       or (upper(dx.dx_code_id) like 'ICD10:O20%' or upper(dx.dx_code_id) like 'ICD10:O21%' or upper(dx.dx_code_id) like 'ICD10:O22%' or upper(dx.dx_code_id) like 'ICD10:O23%'
           or upper(dx.dx_code_id) like 'ICD10:O25%' or upper(dx.dx_code_id) like 'ICD10:O26%' or upper(dx.dx_code_id) like 'ICD10:O28%' or upper(dx.dx_code_id) like 'ICD10:O29%'
           or upper(dx.dx_code_id) like 'ICD10:O3%' or upper(dx.dx_code_id) like 'ICD10:O4%' or upper(dx.dx_code_id) like 'ICD10:O6%' or upper(dx.dx_code_id) like 'ICD10:O7%'
           or upper(dx.dx_code_id) like 'ICD10:O85%' or upper(dx.dx_code_id) like 'ICD10:O86%' or upper(dx.dx_code_id) like 'ICD10:O87%' or upper(dx.dx_code_id) like 'ICD10:O88%'
           or upper(dx.dx_code_id) like 'ICD10:O89%' or upper(dx.dx_code_id) like 'ICD10:O90%' or upper(dx.dx_code_id) like 'ICD10:O91%' or upper(dx.dx_code_id) like 'ICD10:O92%'
            or upper(dx.dx_code_id) like 'ICD10:O94%' or upper(dx.dx_code_id) like 'ICD10:O98%' or upper(dx.dx_code_id) like 'ICD10:O99%' or upper(dx.dx_code_id) like 'ICD10:O9A%'
            ) -- various complications of pregnancy and delivery
       or upper(dx.dx_code_id) like 'ICD10:O24%' -- diabetes during pregnancy
       or (upper(dx.dx_code_id) like 'ICD10:Z33%' or upper(dx.dx_code_id) like 'ICD10:Z34%' or upper(dx.dx_code_id) like 'ICD10:Z36%')    -- pregnancy or pregnancy related encounter coincidental with other dx      
    group by patient_id, date, case
       when (upper(dx.dx_code_id) like 'ICD10:Z38%' or upper(dx.dx_code_id) = 'ICD10:Z37.0' or upper(dx.dx_code_id) = 'ICD10:Z37.2' or upper(dx.dx_code_id) like 'ICD10:Z37.5%') then 'live birth'
       when (upper(dx.dx_code_id) = 'ICD10:Z37.1' or upper(dx.dx_code_id) = 'ICD10:Z37.3' or upper(dx.dx_code_id) = 'ICD10:Z37.4' or upper(dx.dx_code_id) like 'ICD10:Z37.6%' 
           or upper(dx.dx_code_id) = 'ICD10:Z37.7')  then 'still birth'
       when (upper(dx.dx_code_id) like 'ICD10:O00%' or upper(dx.dx_code_id) like 'ICD10:O01%' or upper(dx.dx_code_id) like 'ICD10:O02%') then 'ectopic, molar, other abnormal pregnancy' 
       when (upper(dx.dx_code_id) like 'ICD10:O03%' or upper(dx.dx_code_id) like 'ICD10:O04%' or upper(dx.dx_code_id) like 'ICD10:O05%' or upper(dx.dx_code_id) like 'ICD10:O06%') then 'abortion' 
       when (upper(dx.dx_code_id) like 'ICD10:O80%' or upper(dx.dx_code_id) like 'ICD10:O81%' or upper(dx.dx_code_id) like 'ICD10:O82%') then 'delivery'
       when upper(dx.dx_code_id) like 'ICD10:Z3A%' then 'gestational age'
       when upper(dx.dx_code_id) like 'ICD10:O07%' then 'failed attempted termination of pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O08%' then 'complictaions after ectopic, molar, or other aborted pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O09%' then 'supervision of high risk pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O10%' then 'preexisting hypertension complicating pregnancy'
       when upper(dx.dx_code_id) like 'ICD10:O11%' then 'pre-eclampsia that occurs on top of chronic hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O12%' then 'gestational edema and proteinuria without hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O13%' then 'gestational hypertension'
       when upper(dx.dx_code_id) like 'ICD10:O14%' then 'pre-eclampsia'
       when upper(dx.dx_code_id) like 'ICD10:O15%' then 'eclampsia'
       when upper(dx.dx_code_id) like 'ICD10:O16%' then 'Unspecified maternal hypertension'
       when (upper(dx.dx_code_id) like 'ICD10:O20%' or upper(dx.dx_code_id) like 'ICD10:O21%' or upper(dx.dx_code_id) like 'ICD10:O22%' or upper(dx.dx_code_id) like 'ICD10:O23%'
           or upper(dx.dx_code_id) like 'ICD10:O25%' or upper(dx.dx_code_id) like 'ICD10:O26%' or upper(dx.dx_code_id) like 'ICD10:O28%' or upper(dx.dx_code_id) like 'ICD10:O29%'
           or upper(dx.dx_code_id) like 'ICD10:O3%' or upper(dx.dx_code_id) like 'ICD10:O4%' or upper(dx.dx_code_id) like 'ICD10:O6%' or upper(dx.dx_code_id) like 'ICD10:O7%'
           or upper(dx.dx_code_id) like 'ICD10:O85%' or upper(dx.dx_code_id) like 'ICD10:O86%' or upper(dx.dx_code_id) like 'ICD10:O87%' or upper(dx.dx_code_id) like 'ICD10:O88%'
           or upper(dx.dx_code_id) like 'ICD10:O89%' or upper(dx.dx_code_id) like 'ICD10:O90%' or upper(dx.dx_code_id) like 'ICD10:O91%' or upper(dx.dx_code_id) like 'ICD10:O92%'
            or upper(dx.dx_code_id) like 'ICD10:O94%' or upper(dx.dx_code_id) like 'ICD10:O98%' or upper(dx.dx_code_id) like 'ICD10:O99%' or upper(dx.dx_code_id) like 'ICD10:O9A%'
            ) then 'various complications of pregnancy and delivery'
       when upper(dx.dx_code_id) like 'ICD10:O24%' then 'diabetes during pregnancy'
       when (upper(dx.dx_code_id) like 'ICD10:Z33%' or upper(dx.dx_code_id) like 'ICD10:Z34%' or upper(dx.dx_code_id) like 'ICD10:Z36%')    then 'pregnancy or pregnancy related encounter coincidental with other dx'
      end),
  t02 as (
  --subset the encounters with EDD
    select patient_id, date, 'EDD' as datatype
    from emr_encounter
    where edd is not null
    group by patient_id, date ),
  t03 as (
    select patient_id, date,
      case when upper(native_name) like '%URINE%' and upper(native_name) like '%PREG%' then 'Urine preg positive'
           when upper(native_name) like '%FETO%' and upper(native_name) like '%ALPHA%' then 'FetaAlpha test performed'
      end as datatype
    from emr_labresult lx
    where  (upper(native_name) like '%FETO%' and upper(native_name) like '%ALPHA%')   
        or (upper(native_name) like '%URINE%' and upper(native_name) like '%PREG%' 
            and (upper(result_string) like '%POS%' or upper(result_string) like '%PRES%' or upper(result_string) like '%DET%' or upper(result_string) like '%PREG%') 
            and not upper(result_string) like '%NOT%')
    group by patient_id, date,
      case when upper(native_name) like '%URINE%' and upper(native_name) like '%PREG%' then 'Urine preg positive'
           when upper(native_name) like '%FETO%' and upper(native_name) like '%ALPHA%' then 'FetaAlpha test performed'
      end )
select * 
into gen_pop_tools.pmn_pre_pregnancy_01_2025
from (
    select * from t01
    union select * from t02
    union select * from t03) t_union;
    
    
    
