--query 1-2c
WITH 
T0 AS (
  select 13 as min_age,
         55 as max_age
),
T1 AS (
  SELECT patient_id, FORMAT(ce.date, 'yyyy') AS clin_enc_year
  FROM gen_pop_tools.clin_enc ce
  join emr_patient p on p.id=ce.patient_id
  JOIN T0 ON 1=1
  where date>='2017-01-01'
    and (0 + convert(char(8),ce.date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN T0.min_age AND T0.max_age
  group by patient_id, FORMAT(ce.date, 'yyyy')
),
T2 AS (
  SELECT COUNT(*) AS all_pat_counts, 
         COUNT(CASE WHEN cm.mapped_value='F' THEN p.id ELSE NULL END) AS female_pat_counts,
         T1.clin_enc_year
  FROM emr_patient p
  JOIN T1 ON p.id=T1.patient_id
  LEFT JOIN gen_pop_tools.rs_conf_mapping cm ON cm.src_field='gender' AND cm.src_value=p.gender
  GROUP BY T1.clin_enc_year
),
T3 AS
(
SELECT COUNT(DISTINCT ht.patient_id) AS all_tmspn_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value='M' THEN ht.patient_id ELSE NULL END) AS male_tmspn_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value='F' THEN ht.patient_id ELSE NULL END) AS female_tmspn_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value NOT IN ('M','F') OR cm.mapped_value IS NULL THEN ht.patient_id ELSE NULL END) AS unkn_tmspn_preg_counts,
FORMAT(ht.start_date, 'yyyy') AS preg_year
FROM emr_patient p
LEFT JOIN gen_pop_tools.rs_conf_mapping cm ON cm.src_field='gender' AND cm.src_value=p.gender
JOIN hef_timespan ht ON p.id=ht.patient_id AND ht.name='pregnancy'
JOIN T0 on 1=1
where ht.start_date>='2017-01-01'
  and (0 + convert(char(8),ht.start_date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN T0.min_age AND T0.max_age
GROUP BY FORMAT(ht.start_date, 'yyyy')
),
T4 as (
SELECT DISTINCT
  FORMAT(ht.start_date, 'yyyy') AS preg_year,
  --ms
  PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.start_date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS age_tmspn_5th_percentile,
  PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.start_date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS age_tmspn_25th_percentile,
  PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.start_date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS age_tmspn_50th_percentile,
  PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.start_date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS age_tmspn_75th_percentile,
  PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.start_date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS age_tmspn_95th_percentile,
  PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY DATEDIFF(DAY, ht.start_date, ht.end_date)) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS dur_tmspn_5th_percentile,
  PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY DATEDIFF(DAY, ht.start_date, ht.end_date)) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS dur_tmspn_25th_percentile,
  PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY DATEDIFF(DAY, ht.start_date, ht.end_date)) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS dur_tmspn_50th_percentile,
  PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY DATEDIFF(DAY, ht.start_date, ht.end_date)) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS dur_tmspn_75th_percentile,
  PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY DATEDIFF(DAY, ht.start_date, ht.end_date)) OVER (PARTITION BY FORMAT(ht.start_date, 'yyyy')) AS dur_tmspn_95th_percentile
FROM hef_timespan ht
JOIN emr_patient p ON p.id = ht.patient_id
JOIN T0 on 1=1
WHERE ht.name = 'pregnancy' and ht.start_date>='2017-01-01'
  and (0 + convert(char(8),ht.start_date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN T0.min_age AND T0.max_age
),
T5 AS (
SELECT
COUNT(DISTINCT ht.patient_id) AS all_ht_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value = 'M' THEN ht.patient_id ELSE NULL END) AS male_ht_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value = 'F' THEN ht.patient_id ELSE NULL END) AS female_ht_preg_counts,
COUNT(DISTINCT CASE WHEN cm.mapped_value NOT IN ('M', 'F') THEN ht.patient_id ELSE NULL END) AS unkn_ht_preg_counts,
FORMAT(ht.date, 'yyyy') AS preg_year
FROM emr_patient p
JOIN hef_event ht ON p.id = ht.patient_id AND ht.name = 'dx:pregnancy'
join T0 on 1=1
LEFT JOIN gen_pop_tools.rs_conf_mapping cm ON cm.src_field = 'gender' AND cm.src_value = p.gender
where ht.date between '2017-01-01' and '2024-12-31'
  and (0 + convert(char(8),ht.date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN T0.min_age AND T0.max_age
GROUP BY FORMAT(ht.date, 'yyyy')
),
T6 AS (
  SELECT distinct
    PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.date, 'yyyy')) AS age_ht_5th_percentile,
    PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.date, 'yyyy')) AS age_ht_25th_percentile,
    PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.date, 'yyyy')) AS age_ht_50th_percentile,
    PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.date, 'yyyy'))AS age_ht_75th_percentile,
    PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY (0 + convert(char(8), ht.date, 112) - convert(char(8), p.date_of_birth, 112)) / 10000) OVER (PARTITION BY FORMAT(ht.date, 'yyyy'))AS age_ht_95th_percentile,
    FORMAT(ht.date, 'yyyy') AS preg_year
  FROM hef_event ht
  JOIN emr_patient p ON p.id = ht.patient_id
  JOIN T0 on 1=1
  WHERE ht.name = 'dx:pregnancy' and ht.date between '2017-01-01' and '2024-12-31'
    and (0 + convert(char(8),ht.date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN T0.min_age AND T0.max_age
)
-- Final Select Query
SELECT 
  all_pat_counts, 
  female_pat_counts,
  all_tmspn_preg_counts, 
  male_tmspn_preg_counts,
  female_tmspn_preg_counts,
  unkn_tmspn_preg_counts,
age_tmspn_5th_percentile,
age_tmspn_25th_percentile,
age_tmspn_50th_percentile,
age_tmspn_75th_percentile,
age_tmspn_95th_percentile,
dur_tmspn_5th_percentile,
dur_tmspn_25th_percentile,
dur_tmspn_50th_percentile,
dur_tmspn_75th_percentile,
dur_tmspn_95th_percentile,
all_ht_preg_counts,
male_ht_preg_counts,
female_ht_preg_counts,
unkn_ht_preg_counts,
age_ht_5th_percentile,
age_ht_25th_percentile,
age_ht_50th_percentile,
age_ht_75th_percentile,
age_ht_95th_percentile,
T3.preg_year
--INTO #preg_results1
FROM T2
JOIN T3 ON T2.clin_enc_year = T3.preg_year
JOIN T4 ON T2.clin_enc_year = T4.preg_year
JOIN T5 ON T2.clin_enc_year = T5.preg_year
JOIN T6 ON T2.clin_enc_year = T6.preg_year
WHERE T2.clin_enc_year  between '2017' and '2024';


--2d
WITH t0 AS
(
    SELECT ht.id, MIN(he.date) AS min_he_date, MAX(he.date) AS max_he_date 
    FROM hef_timespan ht
    JOIN hef_timespan_events hte ON hte.timespan_id = ht.id
    JOIN hef_event he ON he.id = hte.event_id
    WHERE ht.name = 'pregnancy'
    GROUP BY ht.id
)
, t1 AS
(
   SELECT combotypecode AS dx_code_id, name
   FROM static_dx_code dx 
   WHERE  
   (
       dx.combotypecode LIKE 'icd10:O09.%'  -- Table 1 Start
      OR dx.combotypecode LIKE 'icd10:O26.2%'
      OR dx.combotypecode LIKE 'icd10:O30.%'
      OR dx.combotypecode LIKE 'icd10:O31%' 
      OR dx.combotypecode LIKE 'icd10:O31.1%'
      OR dx.combotypecode LIKE 'icd10:O31.2%'
      OR dx.combotypecode LIKE 'icd10:O31.3.%'
      OR dx.combotypecode LIKE 'icd10:O32.%'
      OR dx.combotypecode LIKE 'icd10:O33.%'
      OR dx.combotypecode LIKE 'icd10:O36.7%'
      OR dx.combotypecode LIKE 'icd10:Z33.%'
      OR dx.combotypecode LIKE 'icd10:Z34.%'
      OR dx.combotypecode LIKE 'icd10:O10.%'
		AND PATINDEX('icd10:O10.__1%', dx.combotypecode) > 0 
	OR dx.combotypecode LIKE 'icd10:O11.%'
		AND PATINDEX('icd10:O11._[45]%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O12.%'
		AND PATINDEX('icd10:O12.__[45]%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O13.%'
		AND PATINDEX('icd10:O13._[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O14.%' 
		AND PATINDEX('icd10:O14.__[45]%', dx.combotypecode) > 0
	OR dx.combotypecode = 'icd10:O15'
	OR dx.combotypecode LIKE 'icd10:O15.0.%'
	OR dx.combotypecode LIKE 'icd10:O15.9.%'
	OR dx.combotypecode LIKE 'icd10:O16.%'
		AND PATINDEX('icd10:O16._[45]%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O20.%'
	OR dx.combotypecode LIKE 'icd10:O21.%'
	OR dx.combotypecode LIKE 'icd10:O22.%'
	OR dx.combotypecode LIKE 'icd10:O23.%'
	OR dx.combotypecode LIKE 'icd10:O24.%'
		AND PATINDEX('icd10:O24.__1%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O25.%'
		AND PATINDEX('icd10:O25._1.%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O26.%'
	OR dx.combotypecode LIKE 'icd10:O28.%'
	OR dx.combotypecode LIKE 'icd10:O29.%'
	OR dx.combotypecode LIKE 'icd10:O31.8%'
	OR dx.combotypecode LIKE 'icd10:O34.%'
	OR dx.combotypecode LIKE 'icd10:O35.%'
	OR dx.combotypecode LIKE 'icd10:O36.%'
		AND PATINDEX('icd10:O36.[123456]%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O36.8%'
	OR dx.combotypecode LIKE 'icd10:O37.%'
	OR dx.combotypecode LIKE 'icd10:O38.%'
	OR dx.combotypecode LIKE 'icd10:O39.%'
	OR dx.combotypecode LIKE 'icd10:O40.%'
	OR dx.combotypecode LIKE 'icd10:O41.%'
	OR dx.combotypecode LIKE 'icd10:O43.%'
	OR dx.combotypecode LIKE 'icd10:O44.%'
	OR dx.combotypecode LIKE 'icd10:O45.%'
	OR dx.combotypecode LIKE 'icd10:O46.%'
	OR dx.combotypecode LIKE 'icd10:O47.%'
	OR dx.combotypecode LIKE 'icd10:O48.%'
	OR dx.combotypecode LIKE 'icd10:O60.O%'
	OR dx.combotypecode LIKE 'icd10:O99.01%'
	OR dx.combotypecode LIKE 'icd10:O99.11%'
	OR dx.combotypecode LIKE 'icd10:O99.2%'
	OR dx.combotypecode LIKE 'icd10:O99.3%'
	OR dx.combotypecode LIKE 'icd10:O99.%'
		AND PATINDEX('icd10:099.[4567]1', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O99.%'
		AND PATINDEX('icd10:099.[89]_[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O9A.%'
		AND PATINDEX('icd10:09A.[12345]1', dx.combotypecode) > 0
	-- Table 3 Start
	OR dx.combotypecode LIKE 'icd10:O0%'
		AND PATINDEX('icd10:O0[1234].%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O08.%'
	OR dx.combotypecode LIKE 'icd10:O10.%'
		AND PATINDEX('icd10:O10._[23]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O11.%'
		AND PATINDEX('icd10:O11.[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O12.%'
		AND PATINDEX('icd10:O12._[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O13.%'
		AND PATINDEX('icd10:O13.[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O14.%'
		AND PATINDEX('icd10:O14._[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O15\.%'
		AND PATINDEX('icd10:O15.[12]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O16.%'
		AND PATINDEX('icd10:O16.[45]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O24.%'
		AND PATINDEX('icd10:O24._[23]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O25.%'
		AND PATINDEX('icd10:O25.[23]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O26.%'
		AND PATINDEX('icd10:O26.[67][23]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE'icd10:O42.%'
	OR dx.combotypecode LIKE 'icd10:O60.%'
		AND PATINDEX('icd10:O60.[12]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O6%'
		AND PATINDEX('icd10:O6[789].%', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:O7_.%'
	OR dx.combotypecode LIKE 'icd10:O8_.%'
	OR dx.combotypecode LIKE 'icd10:090.%'
	OR dx.combotypecode LIKE 'icd10:99.%'
		AND PATINDEX('icd10:99.[01][23]', dx.combotypecode) > 0
	OR dx.combotypecode LIKE 'icd10:Z37.%'
	OR dx.combotypecode LIKE 'icd10:Z39.%'
	--Table 3 End
	-- Table 4 Start
    OR dx.combotypecode LIKE 'icd10:Z3A.08%'  
    OR dx.combotypecode LIKE 'icd10:Z3A.09%'
    OR dx.combotypecode LIKE 'icd10:Z3A.%'
		AND PATINDEX('icd10:Z3A.[1234]', dx.combotypecode) > 0 
   )
   AND 
   (
      (dx.combotypecode <> 'icd10:Z33.2' --table 1 exclusion
      AND dx.combotypecode NOT LIKE 'icd10:O26.2%' --table 2 exclusions
      AND dx.combotypecode <> 'icd10:O26.62'
      AND dx.combotypecode <> 'icd10:O26.63'
      AND dx.combotypecode <> 'icd10:O26.72'
      AND dx.combotypecode <> 'icd10:O26.73'
      AND dx.combotypecode NOT LIKE 'icd10:O36.4%'
      AND dx.combotypecode NOT LIKE 'icd10:O36.80%'
      AND dx.combotypecode NOT LIKE 'icd10:O99.2%'
		AND NOT PATINDEX('icd10:O99.2[45]', dx.combotypecode) > 0
      AND dx.combotypecode NOT LIKE 'icd10:O99.3%'
		AND NOT PATINDEX('icd10:O99.3[45]', dx.combotypecode) > 0
     ) 
 
   )
)
, t2 AS
(
  SELECT encounter_id, dx_code_id
  FROM emr_encounter_dx_codes
  WHERE dx_code_id IN (SELECT dx_code_id FROM t1)
)
SELECT 
    COUNT(CASE WHEN t0
.min_he_date = he.date THEN dx.encounter_id ELSE NULL END) AS strt_cts,
COUNT(CASE WHEN t0.min_he_date = he.date AND p.gender LIKE 'M%' THEN dx.encounter_id ELSE NULL END) AS male_strt_cts,
COUNT(CASE WHEN t0.min_he_date = he.date AND p.gender LIKE 'F%' THEN dx.encounter_id ELSE NULL END) AS female_strt_cts,
COUNT(CASE WHEN t0.max_he_date = he.date THEN dx.encounter_id ELSE NULL END) AS end_cts,
dx.dx_code_id,
dxc.name
--INTO #preg_results2
FROM hef_timespan ht
JOIN hef_timespan_events hte ON hte.timespan_id = ht.id
JOIN hef_event he ON hte.event_id = he.id AND he.name LIKE 'dx:%'
JOIN t0 ON t0.id = ht.id AND (t0.min_he_date = he.date OR t0.max_he_date = he.date)
JOIN t2 dx ON dx.encounter_id = he.object_id
JOIN t1 dxc ON dx.dx_code_id = dxc.dx_code_id
JOIN emr_patient p ON p.id = ht.patient_id
where (0 + convert(char(8),ht.start_date,112)-convert(char(8), p.date_of_birth, 112))/ 10000
	  BETWEEN 13 AND 55
GROUP BY dx.dx_code_id, dxc.name
ORDER BY dx.dx_code_id DESC;
