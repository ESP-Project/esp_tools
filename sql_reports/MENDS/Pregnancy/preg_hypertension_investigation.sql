/* Current django code to generate dx:pregnancy hef events
for diagnosed hypertension */
Select object_id into temporary pregbp_investigation_standard_known 
from hef_event 
where name = 'dx:pregnancy';

select distinct encounter_id, dx_code_id 
into temporary pregbp_investigation_standard_new 
from emr_encounter_dx_codes dx 
where dx_code_id like 'icd9:V22%' 
	or dx_code_id like 'icd9:V23%' 
	or dx_code_id like 'icd10:Z33%' 
	or dx_code_id like 'icd10:Z34%' 
	or dx_code_id like 'icd10:O09%'
and not exists (select null from pregbp_investigation_standard_known kdx where kdx.object_id=dx.encounter_id);

/* Expanded list of icd pregnancy codes */
Select object_id into temporary pregbp_investigation_expanded_known
from hef_event 
where name = 'dx:pregnancy';

select distinct encounter_id, dx.dx_code_id, sdc.name
into temporary pregb_investigation_expanded_new 
from emr_encounter_dx_codes dx 
join static_dx_code sdc ON dx.dx_code_id = sdc.combotypecode
where (sdc.combotypecode ~ 'icd10:O09\.*'  -- Table 1 Start
      OR sdc.combotypecode ~ 'icd10:O26\.2.*'
      OR sdc.combotypecode ~ 'icd10:O30\..*'
      OR sdc.combotypecode ~ 'icd10:O31\.' 
      OR sdc.combotypecode ~ 'icd10:O31\.1.*'
      OR sdc.combotypecode ~ 'icd10:O31\.2.*'
      OR sdc.combotypecode ~ 'icd10:O31\.3.*'
      OR sdc.combotypecode ~ 'icd10:O32\..*'
      OR sdc.combotypecode ~ 'icd10:O33\..*'
      OR sdc.combotypecode ~ 'icd10:O36\.7.*'
      OR sdc.combotypecode ~ 'icd10:Z33\..*'
      OR sdc.combotypecode ~ 'icd10:Z34\..*'
      OR sdc.combotypecode ~ 'icd10:O10\..\d[1]' 
	OR sdc.combotypecode ~ 'icd10:O11\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O12\..\d[45]'
	OR sdc.combotypecode ~ 'icd10:O13\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O14\..\d[45]'
	OR sdc.combotypecode = 'icd10:O15'
	OR sdc.combotypecode ~ 'icd10:O15\.0.*'
	OR sdc.combotypecode ~ 'icd10:O15\.9.*'
	OR sdc.combotypecode ~ 'icd10:O16\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O20\..*'
	OR sdc.combotypecode ~ 'icd10:O21\..*'
	OR sdc.combotypecode ~ 'icd10:O22\..*'
	OR sdc.combotypecode ~ 'icd10:O23\..*'
	OR sdc.combotypecode ~ 'icd10:O24\..\d[1]'
	OR sdc.combotypecode ~ 'icd10:O25\.\d[1].'
	OR sdc.combotypecode ~ 'icd10:O26\..*'
	OR sdc.combotypecode ~ 'icd10:O28\.*'
	OR sdc.combotypecode ~ 'icd10:O29\.*'
	OR sdc.combotypecode ~ 'icd10:O31\.8.*'
	OR sdc.combotypecode ~ 'icd10:O34\..*'
	OR sdc.combotypecode ~ 'icd10:O35\..*'
	OR sdc.combotypecode ~ 'icd10:O36\.6.*'
	OR sdc.combotypecode ~ 'icd10:O36\.8.*'
	OR sdc.combotypecode ~ 'icd10:O37\..*'
	OR sdc.combotypecode ~ 'icd10:O38\..*'
	OR sdc.combotypecode ~ 'icd10:O39\..*'
	OR sdc.combotypecode ~ 'icd10:O40\..*'
	OR sdc.combotypecode ~ 'icd10:O41\..*'
	OR sdc.combotypecode ~ 'icd10:O43\..*'
	OR sdc.combotypecode ~ 'icd10:O44\..*'
	OR sdc.combotypecode ~ 'icd10:O45\..*'
	OR sdc.combotypecode ~ 'icd10:O46\..*'
	OR sdc.combotypecode ~ 'icd10:O47\..*'
	OR sdc.combotypecode ~ 'icd10:O48\..*'
	OR sdc.combotypecode ~ 'icd10:O60\.O.*'
	OR sdc.combotypecode ~ 'icd10:O99\.01.*'
	OR sdc.combotypecode ~ 'icd10:O99\.11.*'
	OR sdc.combotypecode ~ 'icd10:O99\.2.*.*'
	OR sdc.combotypecode ~ 'icd10:O99\.3.*.*'
	OR sdc.combotypecode ~ 'icd10:O99\.4\d[1]'
	OR sdc.combotypecode ~ 'icd10:O99\.5\d[1]'
	OR sdc.combotypecode ~ 'icd10:O99\.6\d[1]'
	OR sdc.combotypecode ~ 'icd10:O99\.7\d[1]'
	OR sdc.combotypecode ~ 'icd10:O99\.8.*\d[45]'
	OR sdc.combotypecode ~ 'icd10:O99\.9.*\d[45]'
	OR sdc.combotypecode ~ 'icd10:O9A\.1\d[1]'
	OR sdc.combotypecode ~ 'icd10:O9A\.2\d[1]'
	OR sdc.combotypecode ~ 'icd10:O9A\.3\d[1]'
	OR sdc.combotypecode ~ 'icd10:O9A\.4\d[1]'
	OR sdc.combotypecode ~ 'icd10:O9A\.5\d[1]'
	-- Table 3 Start
	OR sdc.combotypecode ~ 'icd10:O00\..*'
	OR sdc.combotypecode ~ 'icd10:O01\..*'
	OR sdc.combotypecode ~ 'icd10:O02\..*'
	OR sdc.combotypecode ~ 'icd10:O03\..*'
	OR sdc.combotypecode ~ 'icd10:O08\..*'
	OR sdc.combotypecode ~ 'icd10:O10\..*\d[23]'
	OR sdc.combotypecode ~ 'icd10:O11\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O12\..*\d[45]'
	OR sdc.combotypecode ~ 'icd10:O13\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O14\..*\d[45]'
	OR sdc.combotypecode ~ 'icd10:O15\.1'
	OR sdc.combotypecode ~ 'icd10:O15\.2'
	OR sdc.combotypecode ~ 'icd10:O16\.\d[45]'
	OR sdc.combotypecode ~ 'icd10:O24\..*\[23]'
	OR sdc.combotypecode ~ 'icd10:O25\.\d[23].*'
	OR sdc.combotypecode ~ 'icd10:O26\.62'
	OR sdc.combotypecode ~ 'icd10:O26\.63'
	OR sdc.combotypecode ~ 'icd10:O26\.72'
	OR sdc.combotypecode ~ 'icd10:O26\.73'
	OR sdc.combotypecode ~ 'icd10:O42\..*'
	OR sdc.combotypecode ~ 'icd10:O60\.1.*'
	OR sdc.combotypecode ~ 'icd10:O60\.2.*'
	OR sdc.combotypecode ~ 'icd10:O6\d[789]\..*'
	OR sdc.combotypecode ~ 'icd10:O7.*\.'
	OR sdc.combotypecode ~ 'icd10:O8.*\.'
	OR sdc.combotypecode ~ 'icd10:O90\..*'
	OR sdc.combotypecode ~ 'icd10:Z37\..*'
	OR sdc.combotypecode ~ 'icd10:Z39\..*'
	--Table 3 End
	-- Table 4 Start
    OR sdc.combotypecode ~ 'icd10:Z3A\.08'  
    OR sdc.combotypecode ~ 'icd10:Z3A\.09'
    OR sdc.combotypecode ~ 'icd10:Z3A\.1.*'
    OR sdc.combotypecode ~ 'icd10:Z3A\.2.*'
    OR sdc.combotypecode ~ 'icd10:Z3A\.3.*'
    OR sdc.combotypecode ~ 'icd10:Z3A\.4.*'
      )
	AND (sdc.combotypecode <> 'icd10:Z33.2' --table 1 exclusion
      AND sdc.combotypecode !~ 'icd10:O26\.2.*' --table 2 exclusions
      AND sdc.combotypecode <> 'icd10:O26.62'
      AND sdc.combotypecode <> 'icd10:O26.63'
      AND sdc.combotypecode <> 'icd10:O26.72'
      AND sdc.combotypecode <> 'icd10:O26.73'
      AND sdc.combotypecode !~ 'icd10:O36\.4.*'
      AND sdc.combotypecode !~ 'icd10:O36.80.*'
      AND sdc.combotypecode !~ 'icd10:O99\.2.*\d[45]'
      AND sdc.combotypecode !~ 'icd10:O99\.3.*\d[45]'
     )
	and not exists (select null from pregbp_investigation_expanded_known kdx where kdx.object_id=dx.encounter_id);



