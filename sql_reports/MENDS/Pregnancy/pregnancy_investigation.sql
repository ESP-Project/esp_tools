create table preg_investigation_combined (
encounter_id INT,
dx_code_id INT,
source_table VARCHAR(255)
);
     

 INSERT INTO preg_investigation_combined (encounter_id, dx_code_id, source_table)
(
SELECT encounter_id, dx_code_id, 'standard' AS source_table
FROM pregbp_investigation_standard_new
EXCEPT
SELECT encounter_id, dx_code_id, 'standard' AS source_table
FROM pregb_investigation_expanded_new
)
UNION
(
SELECT encounter_id, dx_code_id, 'expanded' AS source_table
FROM pregb_investigation_expanded_new
EXCEPT
SELECT encounter_id, dx_code_id, 'expanded' AS source_table
FROM pregbp_investigation_standard_new
);


select t0.combotypecode as combotypecode_dx, t1.combotypecode as combotypecode_ts
INTO temporary temp_preg_results
FROM dx_pregnancy_codes t0
FULL OUTER JOIN timespan_pregnancy_codes t1
ON t1.combotypecode = t0.combotypecode;


SELECT distinct(encounter_id) INTO TEMPORARY not_timespan_encounters FROM emr_encounter_dx_codes
WHERE dx_code_id IN (SELECT combotypecode_dx FROM temp_preg_results WHERE combotypecode_ts IS NULL);

SELECT distinct(encounter_id) INTO TEMPORARY not_dx_encounters FROM emr_encounter_dx_codes
WHERE dx_code_id IN (SELECT combotypecode_ts FROM temp_preg_results WHERE combotypecode_dx IS NULL);
