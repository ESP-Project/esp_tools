--query 1-2c
With 
T0 as (
  select 13 as min_age,
         55 as max_age
  )
,T1 as (
  select distinct patient_id, to_char(date,'yyyy') clin_enc_year
  from gen_pop_tools.clin_enc
  where date between '2017-01-01'::date and '2023-12-31'::date  )
,T2 as (
  Select count(*) all_pat_counts, 
         count(case when cm.mapped_value='F' then p.id else null end) female_pat_counts,
    t1.clin_enc_year
  From emr_patient p
  join t1 on p.id=t1.patient_id
  join t0 on 1=1
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  WHERE date_part('year', age(to_date(t1.clin_enc_year,'yyyy'), p.date_of_birth)) between t0.min_age and t0.max_age
  GROUP BY t1.clin_enc_year )
,T3 as (
  Select count(distinct ht.patient_id) all_tmspn_preg_counts, 
    count(distinct case when cm.mapped_value='F' then ht.patient_id else null end) female_tmspn_preg_counts,
    to_char(ht.start_date, 'yyyy') preg_year
  From emr_patient p
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  JOIN hef_timespan ht ON p.id=ht.patient_id and ht.name='pregnancy'
  JOIN t0 on 1=1
  WHERE date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age 
    and ht.start_date between '2017-01-01'::date and '2023-12-31'::date
  GROUP BY to_char(ht.start_date, 'yyyy') )
,T4 as (
  SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_5th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_50th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_tmspn_95th_percentile,
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_5th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_50th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_tmspn_95th_percentile,
    to_char(ht.start_date, 'yyyy') preg_year
  FROM hef_timespan ht
  JOIN emr_patient p ON p.id = ht.patient_id
  JOIN t0 ON 1=1
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  WHERE ht.name = 'pregnancy' and cm.mapped_value='F'
  AND date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age
    and ht.start_date between '2017-01-01'::date and '2023-12-31'::date
  group by to_char(ht.start_date, 'yyyy') )
,T5 as (
  Select count(distinct ht.patient_id) all_matcho_preg_counts, 
    count(distinct case when cm.mapped_value='F' then ht.patient_id else null end) female_matcho_preg_counts,
    to_char(ht.start_date, 'yyyy') preg_year
  From emr_patient p
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  JOIN matcho_preg_timespan ht ON p.id=ht.patient_id 
  JOIN t0 on 1=1
  WHERE date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age 
    and ht.start_date between '2017-01-01'::date and '2023-12-31'::date
  GROUP BY to_char(ht.start_date, 'yyyy') )
,T6 as (
  SELECT
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_matcho_5th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_matcho_50th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.start_date, p.date_of_birth)) AS age_matcho_95th_percentile,
	PERCENTILE_CONT(0.05) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_matcho_5th_percentile,
	PERCENTILE_CONT(0.50) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_matcho_50th_percentile,
	PERCENTILE_CONT(0.95) WITHIN GROUP (ORDER BY age(ht.end_date, ht.start_date)) AS dur_matcho_95th_percentile,
    to_char(ht.start_date, 'yyyy') preg_year
  FROM matcho_preg_timespan ht
  JOIN emr_patient p ON p.id = ht.patient_id
  JOIN t0 ON 1=1
  left join gen_pop_tools.rs_conf_mapping cm on cm.src_field='gender' and cm.src_value=p.gender
  WHERE cm.mapped_value='F'
  AND date_part('year', age(ht.start_date, p.date_of_birth)) between t0.min_age and t0.max_age
    and ht.start_date between '2017-01-01'::date and '2023-12-31'::date
  group by to_char(ht.start_date, 'yyyy') )
select all_pat_counts, 
       female_pat_counts,
       female_tmspn_preg_counts,
       age_tmspn_5th_percentile,
	   age_tmspn_50th_percentile,
	   age_tmspn_95th_percentile,
	   dur_tmspn_5th_percentile,
	   dur_tmspn_50th_percentile,
	   dur_tmspn_95th_percentile,
       female_matcho_preg_counts,
       age_matcho_5th_percentile,
	   age_matcho_50th_percentile,
	   age_matcho_95th_percentile,
	   dur_matcho_5th_percentile,
	   dur_matcho_50th_percentile,
	   dur_matcho_95th_percentile,
       t3.preg_year
INTO temporary preg_results1
from T2 
join t3 on t2.clin_enc_year=t3.preg_year
join t4 on t2.clin_enc_year=t4.preg_year
join t5 on t2.clin_enc_year=t5.preg_year
join t6 on t2.clin_enc_year=t6.preg_year;


\COPY preg_results1 TO '/srv/esp/esp_tools/sql_reports/MENDS/Pregnancy/pregnancy_timespan_vs_Matcho.csv' DELIMITER ',' CSV HEADER;

