select patient_id, date, 'LB' as category
into temporary temp_live_birth
from emr_encounter e
join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id ilike 'icd10:Z38%' 
   or dx.dx_code_id = 'icd10:Z37.0'
   or dx.dx_code_id = 'icd10:Z37.2'
   or dx.dx_code_id ilike 'icd10:Z37.5%'
group by patient_id, date;

select patient_id, date, 'SB' as category
into temporary temp_stillbirth
from emr_encounter e
join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id = 'icd10:Z37.1'
   or dx.dx_code_id = 'icd10:Z37.3'
   or dx.dx_code_id = 'icd10:Z37.4'
   or dx.dx_code_id ilike 'icd10:Z37.6%'
   or dx.dx_code_id = 'icd10:Z37.7'
group by patient_id, date;

select patient_id, date, 'ECT' as category
into temporary temp_ectopic
from emr_encounter e
join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id ilike 'icd10:O00%' --ectopic pregnancy
   or dx.dx_code_id ilike 'icd10:O01%' --molar pregnancy
   or dx.dx_code_id ilike 'icd10:O02%' --other abnormal products of conception
group by patient_id, date;

select patient_id, date, 'AB' as category
into temporary temp_abortion
from emr_encounter e
join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id ilike 'icd10:O03%' 
   or dx.dx_code_id ilike 'icd10:O04%' 
   or dx.dx_code_id ilike 'icd10:O05%' 
   or dx.dx_code_id ilike 'icd10:O06%' 
group by patient_id, date;

select patient_id, date, 'DELIV' as category
into temporary temp_delivery
from emr_encounter e
join emr_encounter_dx_codes dx on dx.encounter_id=e.id
where dx.dx_code_id ilike 'icd10:O80%'
   or dx.dx_code_id ilike 'icd10:O81%'
   or dx.dx_code_id ilike 'icd10:O82%' 
--did not include complications of labor and delivery
group by patient_id, date;

Select patient_id, date, 'ANTE' as category
into temporary tmp_preg_antenatal
from hef_event where name in ('dx:pregnancy:onset','dx:pregnancy:complications','dx:pregnancy:gestational-age')
group by patient_id, date;

Select e.patient_id, 
  case when split_part(dx.dx_code_id,'.',2) ~ '^[0-9]+$' then e.date - interval '1 week' * split_part(dx.dx_code_id,'.',2)::integer
       else null
  end as preg_strt_date, 'GEST_AGE' as stype
into temporary tmp_preg_gest_age
from hef_event e
join emr_encounter enc on enc.id=e.object_id and e.name='dx:pregnancy:gestational-age'
join emr_encounter_dx_codes dx on dx.encounter_id=enc.id and dx.dx_code_id like 'icd10:Z3A%'
group by e.patient_id, e.id, 
         case 
           when split_part(dx.dx_code_id,'.',2) ~ '^[0-9]+$' then e.date - interval '1 week' * split_part(dx.dx_code_id,'.',2)::integer
           else null
         end;

select l.patient_id, l.date - interval '55 days' as preg_strt_date, 'UPRG' as stype
into temporary tmp_uprg
from emr_labresult l
where native_name ilike '%urine%'
  and native_name ilike '%preg%'
group by l.patient_id, l.date;

select l.patient_id, l.date - interval '123 days' as preg_strt_date, 'tmp_fetoalpha' as stype
into temporary tmp_fetoalpha
from emr_labresult l
where native_name ilike '%feto%' and native_name ilike '%alpha%'
group by l.patient_id, l.date;

select * into temporary temp_starts from (
select * from tmp_preg_gest_age
union select * from tmp_uprg
union select * from tmp_fetoalpha) t0;
create index temp_starts_pat_strt_idx on temp_starts (patient_id, preg_strt_date);
create index temp_starts_pat_id on temp_starts (patient_id);
analyze temp_starts;

drop table if exists preg_end;
create --temporary 
  table preg_end (patient_id integer, pdate date, category varchar(5));
--in the next few blocks we identify each of the patient pregnancy end points.
--the first on is the libe births.
do $$declare r record; d date; pid integer;
begin
  pid=-1;
  for r in select patient_id, date, category from temp_live_birth order by patient_id, date
  loop
    if (pid<>r.patient_id or d<= r.date - interval '182 days') then
        d:=r.date;
        pid=r.patient_id;
        execute 'insert into preg_end (patient_id, pdate, category) values ( ' || r.patient_id || ', ''' || r.date || ''', ''LB'')';
    end if;
  end loop;
end$$;

--now we add still births
do $$declare r record; d date; pid integer;
begin
  pid=-1;
  for r in select * from temp_stillbirth  ts
           where not exists (select null from preg_end pe where pe.patient_id=ts.patient_id 
                               and ((pe.pdate - ts.date between 0 and 182) or (ts.date - pe.pdate between 0 and 168)))
             and not exists (select null from tmp_preg_antenatal pa where pa.patient_id=ts.patient_id 
                               and pa.date - ts.date between 1 and 42)
           order by patient_id, date
  loop
    if (pid<>r.patient_id or (d<= r.date - interval '168 days')) then
        d:=r.date;
        pid=r.patient_id;
        execute 'insert into preg_end (patient_id, pdate, category) values ( ' || r.patient_id || ', ''' || r.date || ''', ''SB'')';
    end if;
  end loop;
end$$;

--then ectopic
do $$declare r record; d date; pid integer;
begin
  pid=-1;
  for r in select * from temp_ectopic  te
           where not exists (select null from preg_end pe where pe.patient_id=te.patient_id  
                               and ((pe.pdate - te.date between 0 and 168 and category='LB') 
                                 or (te.date - pe.pdate between 0 and 70 and category='LB')
                                 or (pe.pdate - te.date between 0 and 154 and category='SB') 
                                 or (te.date - pe.pdate between 0 and 70 and category='SB')))
             and not exists (select null from tmp_preg_antenatal pa where pa.patient_id=te.patient_id 
                               and pa.date - te.date between 1 and 42)
           order by patient_id, date
  loop
    if (pid<>r.patient_id or (d<= r.date - interval '56 days')) then
        d:=r.date;
        pid=r.patient_id;
        execute 'insert into preg_end (patient_id, pdate, category) values ( ' || r.patient_id || ', ''' || r.date || ''', ''ECT'')';
    end if;                              
  end loop;
end$$;

--now abortions
do $$declare r record; d date; pid integer;
begin
  pid=-1;
  for r in select * from temp_abortion  ta
           where not exists (select null from preg_end pe where pe.patient_id=ta.patient_id  
                               and ((pe.pdate - ta.date between 0 and 168 and category='LB') --abort to lb
                                 or (ta.date - pe.pdate between 0 and 70 and category='LB') --lb to abort
                                 or (pe.pdate - ta.date between 0 and 154 and category='SB') --abort to sb
                                 or (ta.date - pe.pdate between 0 and 70 and category='SB') --sb to abort
                                 or (pe.pdate - ta.date between 0 and 56 and category='ECT') --abort to ect
                                 or (ta.date - pe.pdate between 0 and 56 and category='ECT'))) --ect to abort
             and not exists (select null from tmp_preg_antenatal pa where pa.patient_id=ta.patient_id 
                               and pa.date - ta.date between 1 and 42)
           order by patient_id, date
  loop
    if (pid<>r.patient_id or (d<= r.date - interval '168 days')) then
        d:=r.date;
        pid=r.patient_id;
        execute 'insert into preg_end (patient_id, pdate, category) values ( ' || r.patient_id || ', ''' || r.date || ''', ''AB'')';
    end if;
  end loop;
end$$;

--last all the general delivery dates
do $$declare r record; d date; pid integer;
begin
  pid=-1;
  for r in select * from temp_delivery  td
           where not exists (select null from preg_end pe where pe.patient_id=td.patient_id  
                               and ((pe.pdate - td.date between 0 and 182 and category='LB') --deliv to lb
                                 or (td.date - pe.pdate between 0 and 168 and category='LB') --lb to deliv
                                 or (pe.pdate - td.date between 0 and 168 and category='SB') --deliv to sb
                                 or (td.date - pe.pdate between 0 and 168 and category='SB') --sb to deliv
                                 or (pe.pdate - td.date between 0 and 70 and category='ECT') --deliv to ect
                                 or (td.date - pe.pdate between 0 and 154 and category='ECT') --ect to deliv
                                 or (pe.pdate - td.date between 0 and 70 and category='AB') --deliv to abort
                                 or (td.date - pe.pdate between 0 and 154 and category='AB'))) --abort to deliv
             and not exists (select null from tmp_preg_antenatal pa where pa.patient_id=td.patient_id 
                               and pa.date - td.date between 1 and 42)
           order by patient_id, date
  loop
    if (pid<>r.patient_id or (d<= r.date - interval '168 days')) then
        d:=r.date;
        pid=r.patient_id;
        execute 'insert into preg_end (patient_id, pdate, category) values ( ' || r.patient_id || ', ''' || r.date || ''', ''LB'')';
    end if;
  end loop;
end$$;

--now we build the timespans
drop table if exists matcho_preg_timespan;
create table matcho_preg_timespan (patient_id integer, start_date date, end_date date, category varchar(5));

do $$declare r record; d date; pid integer; i integer;
  last_cat varchar(10); last_date date; 
  prelim_earliest date; earliest_start date; latest_start date; pstart date; 
  start_curs refcursor; start_rec record;
begin
  pid=-1;
  i:=0;
  for r in select * from preg_end order by patient_id, pdate
  --need to know the current preg-end category and date, as well as the prior-preg-end category and date
  --the prior category and date is used to determine the retry period
  loop
    if r.patient_id<>pid then
      pid:=r.patient_id;
      last_cat:='none';
      last_date:=null;
    end if;
    if r.category='LB' then
      prelim_earliest:=r.pdate - interval '301 days';
      latest_start:=r.pdate - interval '161 days';
    elsif r.category='SB' then
      prelim_earliest:=r.pdate - interval '301 days';
      latest_start:=r.pdate - interval '100 days';
    elsif r.category='AB' then
      prelim_earliest:=r.pdate - interval '168 days';
      latest_start:=r.pdate - interval '42 days';
    elsif r.category='ECT' then
      prelim_earliest:=r.pdate - interval '84 days';
      latest_start:=r.pdate - interval '42 days';
    end if;
    earliest_start:=case  --adjust for retry period
      when last_cat='none' then prelim_earliest
      when last_cat in ('LB','SB','DELIV') then greatest(prelim_earliest,last_date + interval '28 days')
      when last_cat in ('ECT','AB') then greatest(prelim_earliest,last_date + interval '14 days')
    end;
    open start_curs for select * from temp_starts ts 
                          where ts.patient_id=r.patient_id 
                            and ts.preg_strt_date between earliest_start and latest_start
                            order by ts.preg_strt_date;
    fetch first from start_curs into start_rec;
    pstart:= case (r.category)
      when 'LB' then COALESCE(start_rec.preg_strt_date,r.pdate - interval '280 days')
      when 'SB' then COALESCE(start_rec.preg_strt_date,r.pdate - interval '196 days')
      when 'AB' then COALESCE(start_rec.preg_strt_date,r.pdate - interval '70 days')
      when 'ECT' then COALESCE(start_rec.preg_strt_date,r.pdate - interval '56 days')
    end;
    execute 'insert into matcho_preg_timespan (patient_id, start_date, end_date, category) values ( ' || 
             r.patient_id || ', ''' || pstart || ''', ''' || r.pdate || ''', ''' || r.category || ''')';
    close start_curs;
    i:=i+1;
    if i=1000 then
      raise notice '%', i;
      i:=0;
    end if;
  end loop;
end$$;