drop table if exists gen_pop_tools.demographics;
create table gen_pop_tools.demographics
(zip_code varchar(5), category varchar(50), value integer);
alter table gen_pop_tools.demographics add primary key (zip_code,category);

\COPY gen_pop_tools.demographics(zip_code, category, value) FROM './tx.csv' DELIMITER ',' CSV HEADER;

\COPY gen_pop_tools.demographics(zip_code, category, value) FROM './la.csv' DELIMITER ',' CSV HEADER;