--postgres codes
select count(*) as counts,
       coalesce(zip5, gender, race, ethnicity, age_grp1, age_grp2, age_grp3, 'All') as groups,
       case when zip5 is not null then 'ZIP code'
            when gender is not null then 'Gender'
            when race is not null then 'Race'
            when ethnicity is not null then 'Ethnicity'
            when age_grp1 is not null then 'Age group 1'
            when age_grp2 is not null then 'Age group 2'
            when age_grp3 is not null then 'Age group 3'
            else 'All eligible patients'
        end as grouping_sets
from (
select ce.patient_id,
  case when gender.mapped_value='M' then 'Male'
       when gender.mapped_value='F' then 'Female'
       when gender.mapped_value='O' then 'Other'
                   else 'Unspecified gender' end as gender,
  case when race.mapped_value is null then 'Unspecified race' else race.mapped_value end as race,
  case when ethnicity.mapped_value is null then 'Unspecificed ethnicity' else ethnicity.mapped_value end as ethnicity,
  case
    when extract(year from age('20230101'::date, p.date_of_birth::date)) < 5 then '<5'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 5 and 9 then '5-9'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 10 and 14 then '10-14'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 15 and 19 then '15-19'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 20 and 24 then '20-24'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 25 and 34 then '25-34'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 35 and 44 then '35-44'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 45 and 54 then '45-54'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 55 and 59 then '55-59'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 60 and 64 then '60-64'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 65 and 74 then '65-74'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) between 75 and 84 then '75-84'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) >= 85 then '85+'
  end age_grp1,
  case
    when extract(year from age('20230101'::date, p.date_of_birth::date)) < 18 then '<18'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) >= 18 then '18+'
  end age_grp2,
  case
    when extract(year from age('20230101'::date, p.date_of_birth::date)) < 65 then '<65'
    when extract(year from age('20230101'::date, p.date_of_birth::date)) >= 65  then '65+'
  end age_grp3,
Case when nullif(trim(zip5),'') is null then 'Unspecified Zip' else zip5 end as zip5
from (Select patient_id 
      from gen_pop_tools.clin_enc
      where date >= '2021-06-01' group by patient_id) ce
join emr_patient p on p.id=ce.patient_id
left join gen_pop_tools.rs_conf_mapping gender
  on gender.src_table='emr_patient' and gender.src_field='gender' and gender.src_value=p.gender
left join gen_pop_tools.rs_conf_mapping race
  on race.src_table='emr_patient' and race.src_field='coverage_race' and race.src_value=p.race
left join gen_pop_tools.rs_conf_mapping ethnicity
  on ethnicity.src_table='emr_patient' and ethnicity.src_field='ethnicity_new' and ethnicity.src_value=p.ethnicity
where p.date_of_birth is not null) t00
group by grouping sets ( (), (zip5), (gender), (race), (ethnicity), (age_grp1), (age_grp2), (age_grp3) )
order by zip5, gender, race, ethnicity, age_grp1, age_grp2, age_grp3;