--this is a sample sql code template for dx_code based analyses
--using PMN distributed analytics
with cohort as --first define the cohort, or "patients at risk", or the denominator group
  (select t1.patient_id, 
          coalesce(t0.gender,'null gender') as gender, 
          coalesce(t0.race,'null race') as race, 
          coalesce(t0.ethnicity,'null eth') as ethnicity, 
          t1.date, t1.id as encounter_id,
          date_part('year',age('2021-12-31'::date,t0.date_of_birth)) as age
   from emr_patient t0
   join emr_encounter t1 on t0.id=t1.patient_id
   where --some sort of criteria here, age is used as example
     date_part('year',age(t1.date,t0.date_of_birth)) between 20 and 85 --dob can't be null, so I didn't need to coalesce age above.
     --or a study period
     and t1.date between '2020-01-01'::date and '2021-12-31'::date
     --but the exists (correlated subquery) is always useful to bring in other criteria
     --like here, we filter for patients with 2 or more clinical care events in the year prior to study start
     and exists(select null from gen_pop_tools.clin_enc t00
                where t00.patient_id=t1.patient_id 
                   and t00.date between '2019-01-01'::date and '2019-12-31'::date
                group by t00.patient_id
                having count(*)>=2)),
      cond as
   (select encounter_id from emr_encounter_dx_codes 
    where dx_code_id ~* 'icd10:J10*|icd10:J11*'
    group by encounter_id) -- using a group by is more efficient than a distinct if you can do it
--now identify your patients with the condition, and get counts of both groups 
-- by left joining cond to cohort
select count(distinct cohort.patient_id) as denom,
       count(distinct case when cond.encounter_id is not null then cohort.patient_id else null end) as num,
       cohort.gender, cohort.race, cohort.ethnicity, cohort.age
from cohort 
left join cond on cond.encounter_id=cohort.encounter_id
group by grouping sets (
 (cohort.gender),(cohort.race),(cohort.ethnicity),(cohort.age),
 (cohort.gender, cohort.race), (cohort.ethnicity,cohort.age),
 (cohort.gender, cohort.race, cohort.ethnicity) );
--when you use grouping sets, you have to be careful about null values in the grouping field 
--vs null values indicating the grouping field was not used in the grouping set.
--thus the coalesce function in the creation of the "Cohort" CTE