drop table #t0;
select *
into #t0
from (
select patient_id, year(dx.date) as year,
  case
    when dx.code='288571000119100' then 'icd10:Z64.1'
    when dx.code='32911000' then 'icd10:Z59.0'
    when dx.code='32911000' then 'icd10:Z59.00'
    when dx.code='284465006' then 'icd10:Z65.8'
    when dx.code='405058000' then 'icd10:Z60.0'
    when dx.code='105412007' then 'icd10:Z60.4'
    when dx.code='105413002' then 'icd10:Z60.3'
    when dx.code='620991000124103' then 'icd10:Z63.4'
    when dx.code='73438004' then 'icd10:Z56.0'
    when dx.code='620981000124101' then 'icd10:Z60.2'
    when dx.code='4506002' then 'icd10:Z55.9'
    when dx.code='105531004' then 'icd10:Z59.9'
    when dx.code='363870007' then 'icd10:Z65.9'
    when dx.code='1156191002' then 'icd10:Z59.811'
    when dx.code='1187272007' then 'icd10:Z59.8'
    when dx.code='310134006' then 'icd10:Z59.7'
    when dx.code='733423003' then 'icd10:Z59.41'
    when dx.code='105520007' then 'icd10:Z64.4'
    when dx.code='95934001' then 'icd10:Z65.4'
    when dx.code='266890009' then 'icd10:Z63.72'
    when dx.code='216004' then 'icd10:Z60.5'
    when dx.code='58532003' then 'icd10:Z64.0'
    when dx.code='288561000119106' then 'icd10:Z65.2'
    when dx.code='288171000119109' then 'icd10:Z62.21'
    when dx.code='224340002' then 'icd10:Z65'
    when dx.code='12347001' then 'icd10:Z63.7'
    when dx.code='16090731000119102' then 'icd10:Z57'
    when dx.code='224777007' then 'icd10:Z58'
    when dx.code='161081003' then 'icd10:Z61'
	else 'icd10:not'
  end dx_code_id
from emr_diagnosis dx
union
select patient_id, year(enc.date) as year,
  enc_dx.dx_code_id
from emr_encounter enc join emr_encounter_dx_codes enc_dx on enc.id=enc_dx.encounter_id) t0;

drop table #pat_counts;
Select * into #pat_counts from 
(select year, 
       count(*) as total_count_of_dx_codes,
       count(case when (lower(dx_code_id) like 'icd10:z55%' 
                                 or lower(dx_code_id) like 'icd10:z56%'
                                 or lower(dx_code_id) like 'icd10:z57%'
                                 or lower(dx_code_id) like 'icd10:z58%'
                                 or lower(dx_code_id) like 'icd10:z59%'
                                 or lower(dx_code_id) like 'icd10:z60%'
                                 or lower(dx_code_id) like 'icd10:z61%'
                                 or lower(dx_code_id) like 'icd10:z62%'
                                 or lower(dx_code_id) like 'icd10:z63%'
                                 or lower(dx_code_id) like 'icd10:z64%'
                                 or lower(dx_code_id) like 'icd10:z65%') then dx_code_id 
                           else null end) as total_count_of_z_codes,
       count(distinct patient_id) as total_patients_with_dx_codes,
       count(distinct case when (lower(dx_code_id) like 'icd10:z55%' 
                                 or lower(dx_code_id) like 'icd10:z56%'
                                 or lower(dx_code_id) like 'icd10:z57%'
                                 or lower(dx_code_id) like 'icd10:z58%'
                                 or lower(dx_code_id) like 'icd10:z59%'
                                 or lower(dx_code_id) like 'icd10:z60%'
                                 or lower(dx_code_id) like 'icd10:z61%'
                                 or lower(dx_code_id) like 'icd10:z62%'
                                 or lower(dx_code_id) like 'icd10:z63%'
                                 or lower(dx_code_id) like 'icd10:z64%'
                                 or lower(dx_code_id) like 'icd10:z65%')
                                 then patient_id 
                           else null end) as total_patients_with_z_codes
   from #t0 
   where year>=2017
   group by year) t0;

select * 
into #code_counts from
  (select year, Substring(dx_code_id,1,9) as dx_code_id, 
       count(*) as zcode_counts
   from #t0
   where (lower(dx_code_id) like 'icd10:z55%' 
                                 or lower(dx_code_id) like 'icd10:z56%'
                                 or lower(dx_code_id) like 'icd10:z57%'
                                 or lower(dx_code_id) like 'icd10:z58%'
                                 or lower(dx_code_id) like 'icd10:z59%'
                                 or lower(dx_code_id) like 'icd10:z60%'
                                 or lower(dx_code_id) like 'icd10:z61%'
                                 or lower(dx_code_id) like 'icd10:z62%'
                                 or lower(dx_code_id) like 'icd10:z63%'
                                 or lower(dx_code_id) like 'icd10:z64%'
                                 or lower(dx_code_id) like 'icd10:z65%')
   and year>=2017
   group by year, Substring(dx_code_id,1,9)) t0;

select *
into #code_counts_with_row_number from 
  (select *, row_number() over (partition by year order by zcode_counts desc) as rownum 
   from #code_counts) t0;

drop table #zcodes;
select pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes,
     max(case when rownum=1 then dx_code_id end) as first_code,
     max(case when rownum=1 then zcode_counts end) as first_code_count,
     max(case when rownum=2 then dx_code_id end)  as second_code,
     max(case when rownum=2 then zcode_counts end) as second_code_count,
     max(case when rownum=3 then dx_code_id end) as third_code,
     max(case when rownum=3 then zcode_counts end) as third_code_count,
     max(case when rownum=4 then dx_code_id end) as fourth_code,
     max(case when rownum=4 then zcode_counts end) as fourth_code_count,
     max(case when rownum=5 then dx_code_id end) as fifth_code,
     max(case when rownum=5 then zcode_counts end) as fifth_code_count,
     max(case when rownum=6 then dx_code_id end) as sixth_code,
     max(case when rownum=6 then zcode_counts end) as sixth_code_count,
     max(case when rownum=7 then dx_code_id end) as seventh_code,
     max(case when rownum=7 then zcode_counts end) as seventh_code_count,
     max(case when rownum=8 then dx_code_id end) as eighth_code,
     max(case when rownum=8 then zcode_counts end) as eighth_code_count,
     max(case when rownum=9 then dx_code_id end) as ninth_code,
     max(case when rownum=9 then zcode_counts end) as ninth_code_count,
     max(case when rownum=10 then dx_code_id end) as tenth_code,
     max(case when rownum=10 then zcode_counts end) as tenth_code_count,
     max(case when rownum=11 then dx_code_id end) as eleventh_code,
     max(case when rownum=11 then zcode_counts end) as eleventh_code_count
into #zcodes
 from #pat_counts pc join #code_counts_with_row_number ccrn on pc.year=ccrn.year
 group by pc.year, pc.total_count_of_dx_codes, pc.total_count_of_z_codes,
     pc.total_patients_with_dx_codes, pc.total_patients_with_z_codes;

select * from #zcodes;