select 
  case
    when count(*) < 11 then null
    else count(*)
  end as cohort_counts,
  case 
    when sum(asthma) < 11 then null
    else sum(asthma)
  end as asthma_counts,
  zip5, phr, race, gender, ethnicity, bmi, smoking,
  case
    when extract('year' from age('2020-01-01'::date,date_of_birth)) between 0 and 4 then 'ages 0-4'
    when extract('year' from age('2020-01-01'::date,date_of_birth)) between 5 and 9 then 'ages 5-9'
    when extract('year' from  age('2020-01-01'::date,date_of_birth)) between 10 and 17 then 'ages 10-17'
  end as age_group 
into temporary tx_asthma_counts --remove for PMN run
from
(with t0 as 
  (select id as patient_id, date_of_birth, zip5, race, gender, ethnicity
   from emr_patient 
   where  extract('year' from age('2021-12-31'::Date,date_of_birth)) <= 17 -- patient's 17th birthday is on or before end of measurement period)
   ),         
     t1 as
  (select distinct on (pp.patient_id) pp.patient_id, pp.primary_payer
   from gen_pop_tools.tt_primary_payer pp 
   join t0 on t0.patient_id=pp.patient_id
   where pp.year_month <= '2021_12' and to_date(pp.year_month,'yyyy_mm') <= t0.date_of_birth + interval '17 years' 
   order by pp.patient_id, pp.year_month desc), -- this gets us the last know primary payer during the measurement period
     t3 as
  (select distinct on (smk.patient_id) smk.patient_id, smk.smoking
   from gen_pop_tools.tt_smoking smk 
   join t0 on t0.patient_id=smk.patient_id
   where smk.year_month <= '2021_12'  and to_date(smk.year_month,'yyyy_mm') <= t0.date_of_birth + interval '17 years' --unspecified
   order by smk.patient_id, smk.year_month desc), -- this gets us the last know smoking status during the measurement period
     t4 as
  (select distinct on (bmi.patient_id) bmi.patient_id, bmi.bmi
   from gen_pop_tools.tt_bmi bmi 
   join t0 on t0.patient_id=bmi.patient_id
   where bmi.year_month <= '2021_12'  and to_date(bmi.year_month,'yyyy_mm') <= t0.date_of_birth + interval '17 years' --unspecified
   order by bmi.patient_id, bmi.year_month desc), -- this gets us the last know BMI during the measurement period
     t5 as
  (select distinct on (c.patient_id) c.patient_id, cah.status
   from nodis_case c
   join t1 on t1.patient_id=c.patient_id 
   join nodis_caseactivehistory cah on cah.case_id=c.id and c.condition='asthma'
   order by c.patient_id, cah.date)
select distinct t0.patient_id,
       case 
         when t5.patient_id is not null and t5.status<>'D' then 1
         else 0
       end as asthma,
       coalesce(t2_5.mapped_value,'Unspecified') as smoking,
       case t4.bmi
         when 1 then 'BMI <25'
         when 2 then 'BMI >=25 and BMI <30'
         when 3 then 'BMI >= 30'
         else 'Unspecified'
       end as BMI,
       coalesce(t2_1.mapped_value,'Unspecified') as race, 
       coalesce(t2_2.mapped_value,'Unspecified') as ethnicity, 
       coalesce(t2_3.mapped_value,'Unspecified')  as gender,
       coalesce(t2_4.mapped_value,'Unknown') as primary_payer,
       z2phr.phr, t0.zip5, t0.date_of_birth
from t0 --the emr_patient table contains all patient information on geo-loc, age, race, ethicity and sex
join gen_pop_tools.tt_tx_phr_to_zip as z2phr on z2phr.zip5=t0.zip5
left join t1 on t1.patient_id=t0.patient_id
left join gen_pop_tools.rs_conf_mapping as t2_1 on t2_1.src_field='race' and t2_1.src_value=t0.race 
left join gen_pop_tools.rs_conf_mapping as t2_2 on t2_2.src_field='ethnicity' and t2_2.src_value=t0.ethnicity 
left join gen_pop_tools.rs_conf_mapping as t2_3 on t2_3.src_field='gender' and t2_3.src_value=t0.gender 
left join gen_pop_tools.rs_conf_mapping as t2_4 on t2_4.src_field='primary_payer' and t2_4.code=t1.primary_payer
left join t3 on t3.patient_id=t0.patient_id
left join gen_pop_tools.rs_conf_mapping as t2_5 on t2_5.src_field='tobacco_use' and t2_5.code=t3.smoking::integer
left join t4 on t4.patient_id=t0.patient_id
left join t5 on t5.patient_id=t0.patient_id
where exists (select null from gen_pop_tools.clin_enc as t1 
              where t0.patient_id=t1.patient_id and t1.date between '2020-01-01' and '2021-12-31'
                  and t1.date <= t0.date_of_birth + interval '17 years' --the clin_enc table provides efficient access to patient-care event dates
                  ) 
  and extract('year' from age('2020-01-01'::date,date_of_birth))<=17 ) T00
group by grouping sets (
(phr), (race), (gender), (ethnicity), (smoking), (bmi), (zip5),
  (case
    when extract('year' from age('2020-01-01'::date,date_of_birth)) between 0 and 4 then 'ages 0-4'
    when extract('year' from age('2020-01-01'::date,date_of_birth)) between 5 and 9 then 'ages 5-9'
    when extract('year' from age('2020-01-01'::date,date_of_birth)) between 10 and 17 then 'ages 10-17'
  end),
  (phr, ethnicity),
  (phr, race), 
  (zip5, ethnicity),
  (zip5, race) );
