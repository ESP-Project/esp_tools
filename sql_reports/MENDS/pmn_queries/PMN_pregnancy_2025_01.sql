With t1 as (
    select t0.patient_id as preg_patient_id, pseq.patient_id as patient_id,
      substring(pseq.year_month,1,4) as year,
      case
        when pseq.age < 15 then 'Under 15'
        when pseq.age between 15 and 54 then '15 - 54'
        when pseq.age > 54 then 'Over 54'
        else 'Null age'
      end as age_group, coalesce(datatype,'No preg data') as datatype
    from gen_pop_tools.tt_pat tpat
    join gen_pop_tools.tt_pat_seq_enc pseq on tpat.patient_id=pseq.patient_id
    left join gen_pop_tools.pmn_pre_pregnancy_01_2025 t0 on t0.patient_id=pseq.patient_id and substring(cast(t0.date as varchar),1,4)=substring(pseq.year_month,1,4) 
          and substring(cast(t0.date as varchar),6,2)=substring(pseq.year_month,6,2) 
    where tpat.gender='F')
 , t2 as (
    select count(preg_patient_id) as preg_item_counts, count(distinct preg_patient_id) as preg_pat_counts, count(distinct patient_id) as all_pat_denom,
      year, age_group, datatype
    --into temporary t2
    from t1
    where year in('2018','2019','2020','2021','2022','2023')
    and age_group in ('Under 15','15 - 54','Over 54')
    group by grouping sets 
  ( (year), (age_group), (datatype),
    (year, age_group), (datatype, age_group), (year, datatype), 
    (datatype, year, age_group) ) )
select coalesce(t00.age_group,'All ages') as age_group, coalesce(t00.year,'All years') as year
    , t01.preg_item_counts as live_birth_items, t01.preg_pat_counts as live_birth_pats
    , t02.preg_item_counts as still_birth_items, t02.preg_pat_counts as still_birth_pats
    , t03.preg_item_counts as ecto_mo_oth_items, t03.preg_pat_counts as ecto_mo_oth_pats
    , t04.preg_item_counts as abort_items, t04.preg_pat_counts as abort_pats
    , t05.preg_item_counts as deliv_items, t05.preg_pat_counts as deliv_pats
    , t06.preg_item_counts as gest_age_items, t06.preg_pat_counts as gest_age_pats
    , t07.preg_item_counts as failabort_items, t07.preg_pat_counts as failabort_pats
    , t08.preg_item_counts as compabort_items, t08.preg_pat_counts as compabort_pats
    , t09.preg_item_counts as hirisk_items, t09.preg_pat_counts as hirisk_pats
    , t10.preg_item_counts as prehypert_items, t10.preg_pat_counts as prehypert_pats
    , t11.preg_item_counts as preeclamps_hypert_items, t11.preg_pat_counts as preeclamps_hypert_pats
    , t12.preg_item_counts as edema_items, t12.preg_pat_counts as edema_pats
    , t13.preg_item_counts as gesthypert_items, t13.preg_pat_counts as gesthypert_pats
    , t14.preg_item_counts as preeclamps_items, t14.preg_pat_counts as preeclamps_pats
    , t15.preg_item_counts as eclamps_items, t15.preg_pat_counts as eclamps_pats
    , t16.preg_item_counts as unspec_hypert_items, t16.preg_pat_counts as unspec_hypert_pats
    , t17.preg_item_counts as complic_items, t17.preg_pat_counts as complic_pats
    , t18.preg_item_counts as diab_items, t18.preg_pat_counts as diab_pats
    , t19.preg_item_counts as preg_also_items, t19.preg_pat_counts as preg_also_pats
    , t20.preg_item_counts as edd_items, t20.preg_pat_counts as edd_pats
    , t21.preg_item_counts as urine_pregpos_items, t21.preg_pat_counts as urine_pregpos_pats
    , t22.preg_item_counts as fetaalpha_items, t22.preg_pat_counts as fetaalpha_pats
    , t00.preg_pat_counts all_preg_pats, t00.all_pat_denom
--into temporary test_preg_counts
from (select preg_pat_counts, all_pat_denom, age_group, year from t2 where datatype is null) t00 
left join (select * from t2 where datatype='live birth') t01 
  on coalesce(t00.year,'n')=coalesce(t01.year,'n') and coalesce(t00.age_group,'n')=coalesce(t01.age_group,'n')
left join (select * from t2 where datatype='still birth') t02 
  on coalesce(t00.year,'n')=coalesce(t02.year,'n') and coalesce(t00.age_group,'n')=coalesce(t02.age_group,'n')
left join (select * from t2 where datatype='ectopic, molar, other abnormal pregnancy') t03 
  on coalesce(t00.year,'n')=coalesce(t03.year,'n') and coalesce(t00.age_group,'n')=coalesce(t03.age_group,'n')
left join (select * from t2 where datatype='abortion') t04 
  on coalesce(t00.year,'n')=coalesce(t04.year,'n') and coalesce(t00.age_group,'n')=coalesce(t04.age_group,'n')
left join (select * from t2 where datatype='delivery') t05 
  on coalesce(t00.year,'n')=coalesce(t05.year,'n') and coalesce(t00.age_group,'n')=coalesce(t05.age_group,'n')
left join (select * from t2 where datatype='gestational age') t06 
  on coalesce(t00.year,'n')=coalesce(t06.year,'n') and coalesce(t00.age_group,'n')=coalesce(t06.age_group,'n')
left join (select * from t2 where datatype='failed attempted termination of pregnancy') t07 
  on coalesce(t00.year,'n')=coalesce(t07.year,'n') and coalesce(t00.age_group,'n')=coalesce(t07.age_group,'n')
left join (select * from t2 where datatype='complictaions after ectopic, molar, or other aborted pregnancy') t08 
  on coalesce(t00.year,'n')=coalesce(t08.year,'n') and coalesce(t00.age_group,'n')=coalesce(t08.age_group,'n')
left join (select * from t2 where datatype='supervision of high risk pregnancy') t09 
  on coalesce(t00.year,'n')=coalesce(t09.year,'n') and coalesce(t00.age_group,'n')=coalesce(t09.age_group,'n')
left join (select * from t2 where datatype='preexisting hypertension complicating pregnancy') t10 
  on coalesce(t00.year,'n')=coalesce(t10.year,'n') and coalesce(t00.age_group,'n')=coalesce(t10.age_group,'n')
left join (select * from t2 where datatype='pre-eclampsia that occurs on top of chronic hypertension') t11 
  on coalesce(t00.year,'n')=coalesce(t11.year,'n') and coalesce(t00.age_group,'n')=coalesce(t11.age_group,'n')
left join (select * from t2 where datatype='gestational edema and proteinuria without hypertension') t12 
  on coalesce(t00.year,'n')=coalesce(t12.year,'n') and coalesce(t00.age_group,'n')=coalesce(t12.age_group,'n')
left join (select * from t2 where datatype='gestational hypertension') t13 
  on coalesce(t00.year,'n')=coalesce(t13.year,'n') and coalesce(t00.age_group,'n')=coalesce(t13.age_group,'n')
left join (select * from t2 where datatype='pre-eclampsia') t14 
  on coalesce(t00.year,'n')=coalesce(t14.year,'n') and coalesce(t00.age_group,'n')=coalesce(t14.age_group,'n')
left join (select * from t2 where datatype='eclampsia') t15 
  on coalesce(t00.year,'n')=coalesce(t15.year,'n') and coalesce(t00.age_group,'n')=coalesce(t15.age_group,'n')
left join (select * from t2 where datatype='Unspecified maternal hypertension') t16 
  on coalesce(t00.year,'n')=coalesce(t16.year,'n') and coalesce(t00.age_group,'n')=coalesce(t16.age_group,'n')
left join (select * from t2 where datatype='various complications of pregnancy and delivery') t17 
  on coalesce(t00.year,'n')=coalesce(t17.year,'n') and coalesce(t00.age_group,'n')=coalesce(t17.age_group,'n')
left join (select * from t2 where datatype='diabetes during pregnancy') t18 
  on coalesce(t00.year,'n')=coalesce(t18.year,'n') and coalesce(t00.age_group,'n')=coalesce(t18.age_group,'n')
left join (select * from t2 where datatype='pregnancy or pregnancy related encounter coincidental with other dx') t19 
  on coalesce(t00.year,'n')=coalesce(t19.year,'n') and coalesce(t00.age_group,'n')=coalesce(t19.age_group,'n')
left join (select * from t2 where datatype='EDD') t20 
  on coalesce(t00.year,'n')=coalesce(t20.year,'n') and coalesce(t00.age_group,'n')=coalesce(t20.age_group,'n')
left join (select * from t2 where datatype='Urine preg positive') t21 
  on coalesce(t00.year,'n')=coalesce(t21.year,'n') and coalesce(t00.age_group,'n')=coalesce(t21.age_group,'n')
left join (select * from t2 where datatype='FetaAlpha test performed') t22 
  on coalesce(t00.year,'n')=coalesce(t22.year,'n') and coalesce(t00.age_group,'n')=coalesce(t22.age_group,'n');


