drop table if exists random_obesity_patients;
select p.id as patient_id, p.natural_key, random() as random
into temporary random_obesity_patients
from emr_patient p;

drop table if exists tmp_bmi;
select max(date) max_date, patient_id 
into temporary tmp_bmi
from emr_encounter 
where bmi > 0 and date between '2020-01-01' and '2022-10-31'
group by patient_id;
--alter table tmp_bmi alter column patient_id int not null;
alter table tmp_bmi add primary key (patient_id);

drop table if exists tmp_bmi_age;
select patient_id, max_date, date_part('years', age(tbmi.max_date,p.date_of_birth)) age
into temporary tmp_bmi_age
from tmp_bmi tbmi
join emr_patient p on p.id=tbmi.patient_id;

--adult bmi <= 24.9
drop table if exists tmp_bmi_selected;
select r.natural_key, random, 'bmi <= 24.9'::varchar(20) t_type, e.date
into temporary tmp_bmi_selected
From emr_encounter e
join random_obesity_patients r on r.patient_id=e.patient_id
join tmp_bmi_age t0 on t0.patient_id=e.patient_id and t0.max_date=e.date
where e.bmi <= 24.9 and t0.age>=20
group by r.natural_key, r.random, e.date
order by r.random limit 3;

--adult bmi 25 - 29.9
insert into tmp_bmi_selected (natural_key, random, t_type, date)
select r.natural_key, r.random, 'bmi 25-29.9'::varchar(20) t_type, e.date
from emr_encounter e 
join random_obesity_patients r on r.patient_id=e.patient_id
join tmp_bmi_age t0 on t0.patient_id=e.patient_id and t0.max_date=e.date
where e.bmi>= 25 and e.bmi <= 29.9 and t0.age>=20
    and r.natural_key not in (select natural_key from tmp_selected)
group by r.natural_key, r.random, e.date
order by r.random limit 3;

--adult bmi 30 +
insert into tmp_bmi_selected (natural_key, random, t_type, date)
select r.natural_key, r.random, 'bmi >= 30'::varchar(20) t_type, e.date
from emr_encounter e
join random_obesity_patients r on r.patient_id=e.patient_id
join tmp_bmi_age t0 on t0.patient_id=e.patient_id and t0.max_date=e.date
where e.bmi>= 30  and t0.age>=20
    and r.natural_key not in (select natural_key from tmp_selected)
group by r.natural_key, r.random, e.date
order by r.random limit 4;

select ts.natural_key, p.race, p.ethnicity, p.date_of_birth, ts.t_type as testtype, ts.date
into temporary tmp_bmi_report
from tmp_bmi_selected ts
join emr_patient p on p.natural_key=ts.natural_key
order by t_type;

--child height weight
drop table if exists tmp_htwt;;
select max(date) max_date, patient_id 
into tmp_htwt
     from emr_encounter where height is not null and weight is not null
	   and date between '2020-01-01' and '2022-11-30'
     group by patient_id;
alter table tmp_htwt add primary key (patient_id);

drop table if exists tmp_htwt_age;
select patient_id, max_date, date_part('years', age(thtwt.max_date,p.date_of_birth)) age
into temporary tmp_htwt_age
from tmp_htwt thtwt
join emr_patient p on p.id=thtwt.patient_id;

select r.natural_key, r.random, max(e.height) height, max(e.weight) weight, e.date
into temporary tmp_rand_child_ht_wt
from emr_encounter e  
join random_obesity_patients r on r.patient_id=e.patient_id
join tmp_htwt_age t0 on t0.patient_id=e.patient_id and t0.max_date=e.date
where t0.age<=20
group by r.natural_key, r.random, e.date
order by r.random limit 10;

select t0.natural_key, p.race, p.ethnicity, p.date_of_birth::date, t0.date as meas_date, height, weight
into temporary tmp_htwt_report
from tmp_rand_child_ht_wt as t0
join emr_patient p on p.natural_key=t0.natural_key;

/copy tmp_bmi_report to '/tmp/bmi_report.csv' CSV delimiter ',' header;

/copy tmp_htwt_report to '/tmp/htwt_report.csv' CSV delimiter ',' header;
