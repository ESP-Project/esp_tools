drop table if exists random_patients;
create temporary table random_patients as
select p.id as patient_id, p.natural_key, random() as random
from emr_patient p 
where p.date_of_birth < '2002-01-01'::date and p.date_of_birth > '1942-01-01'::date;

--exclude patients with no clinical encounters in the last year.

--smoking-Current
drop table if exists tmp_selected_smk;
select p.natural_key, h.date, m.mapped_value smoking
into temporary tmp_selected_smk
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_socialhistory h on h.patient_id=p.id
join gen_pop_tools.rs_conf_mapping m on m.src_value=h.tobacco_use and m.src_table='emr_socialhistory' and m.src_field='tobacco_use'
where h.date<='2022-10-31' and m.mapped_value='Current'
 and not exists (select null from emr_socialhistory h1 
				 where h1.patient_id=h.patient_id and h1.date>h.date and h1.date<='2022-10-31')
order by h.date desc, r.random
limit 5;

--Never smoked
insert into tmp_selected_smk
select p.natural_key, h.date, m.mapped_value smoking
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_socialhistory h on h.patient_id=p.id
join gen_pop_tools.rs_conf_mapping m on m.src_value=h.tobacco_use and m.src_table='emr_socialhistory' and m.src_field='tobacco_use'
where h.date<='2022-10-31' and m.mapped_value='Never' and p.date_of_birth <= '2001-12-31'
 and p.natural_key not in (select natural_key from tmp_selected_smk) 
 and not exists (select null from emr_socialhistory h0 
                 join gen_pop_tools.rs_conf_mapping m0 on m0.src_value=h0.tobacco_use 
				     and m0.src_table='emr_socialhistory' and m0.src_field='tobacco_use'
			     where h0.patient_id=p.id and m0.mapped_value in ('Current','Former'))
and not exists (select null from emr_socialhistory h1 
			     where h1.patient_id=p.id and h1.date>h.date and h1.date<='2022-10-31')
order by h.date desc, r.random
limit 5;

--Former
insert into tmp_selected_smk
select p.natural_key, h.date, m.mapped_value smoking
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_socialhistory h on h.patient_id=p.id
join gen_pop_tools.rs_conf_mapping m on m.src_value=h.tobacco_use and m.src_table='emr_socialhistory' and m.src_field='tobacco_use'
where h.date<='2022-10-31' and m.mapped_value='Former' and p.date_of_birth <= '2001-12-31'
 and p.natural_key not in (select natural_key from tmp_selected_smk) 
and not exists (select null from emr_socialhistory h1 
			     where h1.patient_id=p.id and h1.date>h.date and h1.date<='2021-09-30')
order by h.date desc, r.random
limit 5;

--No Smoking status
insert into tmp_selected_smk
select p.natural_key, null::date, 'no data' smoking
from emr_patient p
join random_patients r on r.patient_id=p.id
where p.date_of_birth <= '2001-12-31'
 and p.natural_key not in (select natural_key from tmp_selected_smk) 
 and not exists (select null from emr_socialhistory h1 
			     where h1.patient_id=p.id and h1.date<='2022-10-31')
order by r.random
limit 5;


--cholesterol-hdl >= 60
drop table if exists tmp_selected_chol;
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-hdl >= 60'::varchar(100) test_type
into temporary tmp_selected_chol
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-hdl'
where l.date<='2022-10-31' and result_float >=60
and not exists (select null from emr_labresult l1 
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
order by r.random
limit 2;

--cholesterol-hdl < 40 
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-hdl < 40'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-hdl'
where l.date<='2022-10-31' and result_float < 40
and not exists (select null from emr_labresult l1 
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--cholesterol-hdl not measured 
insert into tmp_selected_chol
select p.natural_key, null::date, 'none' result_string, null result_float, 
'cholesterol-hdl not measured'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
where not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-hdl'
				 where l1.patient_id=p.id)
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by p.natural_key, r.random
limit 1;

--cholesterol-ldl >= 180
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-ldl >= 180'::varchar(100) test_name
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-ldl'
where l.date<='2022-10-31' and result_float >=180
and not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-ldl'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--cholesterol-ldl < 100 
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-ldl < 100'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-ldl'
where l.date<='2022-10-31' and result_float < 100
and not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-ldl'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by p.natural_key, r.random
limit 2;

--cholesterol-ldl not measured 
insert into tmp_selected_chol
select p.natural_key, null::date, 'none' result_string, null result_float, 
'cholesterol-ldl not measured'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
where not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-ldl'
			     where l1.patient_id=p.id)
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 1;

--cholesterol-total >= 250
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-total >= 250'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-total'
where l.date<='2022-10-31' and result_float >=250
and not exists (select null from emr_labresult l1  
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-total'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--cholesterol-total < 200 
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'cholesterol-total < 200'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='cholesterol-total'
where l.date<='2022-10-31' and result_float < 200
and not exists (select null from emr_labresult l1  
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-total'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--cholesterol-total not measured 
insert into tmp_selected_chol
select p.natural_key, null::date, 'none' result_string, null result_float, 
'cholesterol-total not measured'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
where not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='cholesterol-total'
			     where l1.patient_id=p.id)
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 1;


--triglycerides >= 400
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'triglycerides >= 400'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='triglycerides'
where l.date<='2022-10-31' and result_float >=400
and not exists (select null from emr_labresult l1  
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='triglycerides'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--triglycerides < 150 
insert into tmp_selected_chol
select p.natural_key, l.date, l.result_string, l.result_float, 
'triglycerides < 150'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
join emr_labresult l on l.patient_id=p.id
join conf_labtestmap m on m.native_code=l.native_code and m.test_name='triglycerides'
where l.date<='2022-10-31' and result_float < 150
and not exists (select null from emr_labresult l1  
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='triglycerides'
			     where l1.patient_id=p.id and l1.date>l.date and l1.date<='2022-10-31')
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 2;

--triglycerides not measured 
insert into tmp_selected_chol
select p.natural_key, null::date, 'none' result_string, null result_float, 
'triglycerides not measured'::varchar(100) test_type
from emr_patient p
join random_patients r on r.patient_id=p.id
where not exists (select null from emr_labresult l1 
                  join conf_labtestmap m on m.native_code=l1.native_code and m.test_name='triglycerides'
			     where l1.patient_id=p.id and l1.date<='2022-10-31' )
 and p.natural_key not in (select natural_key from tmp_selected_chol) 
order by r.random
limit 1;

select t0.natural_key as Global_ID, p.Race, p.Ethnicity, p.Date_of_Birth,p.gender, t0.date as smoking_history_date, smoking
into temporary tmp_smoking_report
from tmp_selected_smk t0
join emr_patient p on p.natural_key=t0.natural_key
order by smoking, t0.natural_key;

select t0.natural_key as Global_ID, p.Race, p.Ethnicity, p.Date_of_Birth, p.gender, t0.date as lab_result_date, result_float as lab_result,
test_type
into temporary tmp_chol_report
from tmp_selected_chol t0
join emr_patient p on p.natural_key=t0.natural_key
order by test_type, t0.natural_key;

\copy tmp_smoking_report to '/tmp/smoking_report.csv' CSV delimiter ',' header;

\copy tmp_chol_report to '/tmp/cholesterol_report.csv' CSV delimiter ',' header;
