drop table if exists random_patients;
create table random_patients as
select p.id as patient_id, p.natural_key, random() as random
from emr_patient p 
where p.date_of_birth < '2000-01-01'::date and p.date_of_birth > '1942-01-01'::date;

select patient_id, date into temporary tmp_bp_u
from emr_encounter 
group by patient_id, date
having (min(bp_diastolic)>90 or min(bp_systolic)>140);
create unique index tmp_bp_u_patid_date_idx on tmp_bp_u (patient_id, date);
select patient_id, date into temporary tmp_bp_c
from emr_encounter 
group by patient_id, date
having (min(bp_diastolic)<90 and min(bp_systolic)<140);
create unique index tmp_bp_c_patid_date_idx on tmp_bp_c (patient_id, date);



--hypertensives
drop table if exists tmp_selected;
create table tmp_selected as
select p.natural_key, random, 'chyper'::char(10) as ttype
from emr_patient p
join nodis_case c on c.patient_id=p.id
join nodis_caseactivehistory h on c.id=h.case_id
join random_patients r on r.patient_id=p.id
join (select max(h.date) max_date, case_id 
     from nodis_case c join nodis_caseactivehistory h on h.case_id=c.id
     where c.condition='hypertension' and h.date <= '2022-09-30'::date
     group by case_id) t0 on t0.case_id=c.id
where c.condition='hypertension' and h.date <= '2022-09-30'::date 
  and c.date> '2019-12-31' and h.date=t0.max_date and h.status<>'D'
group by p.natural_key, r.random
order by r.random limit 50;

--controlled hypertension
insert into tmp_selected
select p.natural_key, random, 'dhyper-c' as ttype
from emr_patient p
join nodis_case c on c.patient_id=p.id
join nodis_caseactivehistory h on c.id=h.case_id
join random_patients r on r.patient_id=p.id
join (select max(h.date) max_date, case_id 
     from nodis_case c join nodis_caseactivehistory h on h.case_id=c.id
     where c.condition='diagnosedhypertension' and h.date <= '2022-09-30'::date
     group by case_id) t0 on t0.case_id=c.id
join (select distinct patient_id, date from hef_event where name='dx:hypertension') t1 on t1.patient_id=p.id and t1.date<h.date and t1.date>'2019-12-31'
where c.condition='diagnosedhypertension' 
  and h.date <= '2022-09-30'::date and h.date> '2019-12-31' and h.date=t0.max_date and h.status='C'
  and p.natural_key not in (select natural_key from tmp_selected)  and c.date> '2019-12-31'
group by p.natural_key, r.random
order by r.random limit 50;

--uncontrolled hypertesnsion
insert into tmp_selected
select p.natural_key, random, 
'dhyper-u' as ttype
from emr_patient p
join nodis_case c on c.patient_id=p.id
join nodis_caseactivehistory h on c.id=h.case_id
join random_patients r on r.patient_id=p.id
join (select max(h.date) max_date, case_id 
     from nodis_case c join nodis_caseactivehistory h on h.case_id=c.id
     where c.condition='diagnosedhypertension' and h.date <= '2022-09-30'::date
     group by case_id) t0 on t0.case_id=c.id
join (select distinct patient_id, date from hef_event where name='dx:hypertension') t1 on t1.patient_id=p.id and t1.date<h.date and t1.date>'2019-12-31'
where c.condition='diagnosedhypertension' 
  and h.date <= '2022-09-30'::date and h.date> '2019-12-31' 
  and h.date=t0.max_date and h.status='U'
  and p.natural_key not in (select natural_key from tmp_selected)   
  and c.date> '2019-12-31'
  and exists (select null from tmp_bp_u bp_u where c.patient_id=p.id and bp_u.date=t0.max_date)
group by p.natural_key, r.random
order by r.random 
limit 14;

--Unknown hypertensives
--delete from tmp_selected where ttype='test 4';
insert into tmp_selected
select p.natural_key, random, 'dhyper-uk' as ttype
from emr_patient p
join nodis_case c on c.patient_id=p.id
join nodis_caseactivehistory h on c.id=h.case_id
join random_patients r on r.patient_id=p.id
join (select max(h.date) max_date, h.case_id 
     from nodis_case c join nodis_caseactivehistory h on h.case_id=c.id
     where c.condition='diagnosedhypertension' and h.date <= '2022-09-30'::date
     group by case_id) t0 on t0.case_id=c.id
where c.condition='diagnosedhypertension' 
  and h.date <= '2022-09-30'::date and h.date> '2019-12-31' 
  and h.date=t0.max_date and h.status='UK'
  and p.natural_key not in (select natural_key from tmp_selected) 
  and exists ( select null from hef_event he where he.name='dx:hypertension' and he.patient_id=p.id
			     and he.date>'2019-12-31'::date and he.date<=h.date) and c.date> '2019-12-31'
group by p.natural_key, r.random
order by r.random limit 20;

--Not hypertensive
delete from tmp_selected where ttype='Not hypert';
insert into tmp_selected
select p.natural_key, random, 'test 5' as ttype
from emr_patient p
left join nodis_case c on c.patient_id=p.id and c.condition in ('diagnosedhypertension', 'hypertension')
 and p.natural_key not in (select natural_key from tmp_selected) 
join random_patients r on r.patient_id=p.id
where c.id is null
order by r.random limit 20;

\ copy (select t0.*, p.Race, p.Ethnicity, p.Date_of_Birth, p.gender
from tmp_selected t0
join emr_patient p on p.natural_key=t0.natural_key
order by ttype, t0.natural_key) to '\tmp\hypertension_validation_report.csv' CSV delimiter ',' header;
