--Create Pool of Random Patients
drop table if exists random_patients;
create temporary table random_patients as
select p.id as patient_id, p.natural_key, random() as random
from emr_patient p;


--9 No Asthma Status Patients, with no dx or rx events
DROP TABLE IF EXISTS tmp_selected_asthma;
SELECT p.natural_key, null::date as date, 'No Asthma evidence' criteria
INTO TEMPORARY tmp_selected_asthma
FROM emr_patient p
JOIN random_patients r ON r.patient_id = p.id
WHERE NOT EXISTS (SELECT null from nodis_case nc WHERE nc.patient_id = r.patient_id 
				  AND nc.condition='asthma')
      and not exists (select null from hef_event he 
                      where he.patient_id=r.patient_id
                            and he.name in ('dx:asthma','rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol',
            'rx:formoterol','rx:indacaterol','rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh',
            'rx:alvesco','rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
            'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh','rx:intal','rx:omalizumab',
            'rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab','rx:theophylline','rx:fluticasone-salmeterol:generic',
             'rx:fluticasone-vilanterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formoterol:generic',
             'rx:budesonide-formoterol:generic','rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade',           'rx:albuterol-ipratropium:trade','rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade'))
ORDER BY r.random desc
limit 9;

--8 No Asthma Status Patients, with at least 1 dx or 1 rx events
insert into tmp_selected_asthma
SELECT p.natural_key, null::date as date, 'No Asthma, but at least 1 dx or rx' criteria
FROM emr_patient p
JOIN random_patients r ON r.patient_id = p.id
WHERE NOT EXISTS (SELECT null from nodis_case nc WHERE nc.patient_id = r.patient_id 
				  AND nc.condition='asthma')
      and exists (select null from hef_event he 
                      where he.patient_id=r.patient_id
                            and he.name in ('dx:asthma','rx:albuterol','rx:levalbuterol','rx:pirbuterol','rx:arformoterol',
            'rx:formoterol','rx:indacaterol','rx:salmeterol','rx:beclomethasone','rx:budesonide-inh','rx:pulmicort','rx:ciclesonide-inh',
            'rx:alvesco','rx:flunisolide-inh','rx:aerobid','rx:fluticasone-inh','rx:flovent','rx:mometasone-inh','rx:asmanex',
            'rx:montelukast','rx:zafirlukast','rx:zileuton','rx:ipratropium','rx:tiotropium','rx:cromolyn-inh','rx:intal','rx:omalizumab',
            'rx:benralizumab','rx:mepolizumab','rx:reslizumab','rx:dupilumab','rx:theophylline','rx:fluticasone-salmeterol:generic',
             'rx:fluticasone-vilanterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formoterol:generic',
             'rx:budesonide-formoterol:generic','rx:fluticasone-salmeterol:trade','rx:fluticasone-vilanterol:trade',           'rx:albuterol-ipratropium:trade','rx:mometasone-formoterol:trade','rx:budesonide-formoterol:trade')
                           and he.date<='2023-03-31'::date)
ORDER BY r.random desc
limit 8;

--Create Pool of Random Asthma Patients
--Select Patients Without Status of I or R and a status of Asthma within 2 years after their case status end date.
DROP TABLE IF EXISTS random_asthma_patients;

CREATE TEMPORARY TABLE random_asthma_patients AS
SELECT 
    sub.patient_id,
    sub.natural_key,
    sub.date_of_birth,
    sub.strtdt AS date,
    RANDOM() AS random
FROM (
    SELECT 
        p.id AS patient_id,
        p.natural_key,
        p.date_of_birth::date,
        cah.strtdt,
        ROW_NUMBER() OVER(PARTITION BY p.id ORDER BY cah.strtdt DESC) AS rn
    FROM emr_patient p
    JOIN nodis_case n ON n.patient_id = p.id
    JOIN (
        SELECT h.case_id, h.status, h.date as strtdt,
            CASE
                WHEN LEAD(h.date) OVER (PARTITION BY h.case_id ORDER BY h.date) IS NOT NULL
                    THEN LEAD(h.date) OVER (PARTITION BY h.case_id ORDER BY h.date)
                WHEN c.isactive THEN '2023-03-31'::date
            END AS enddt
        FROM nodis_caseactivehistory h
        JOIN nodis_case c ON c.id=h.case_id
        WHERE c.condition = 'asthma' 
    ) AS cah ON n.id = cah.case_id 
    WHERE cah.enddt >= '2023-03-31'::date AND EXTRACT(YEAR FROM AGE('2023-03-31'::date, cah.strtdt)) < 2 
		AND cah.status IN ('I', 'R')
) AS sub
WHERE sub.rn = 1;


-- 6 Patients with criteria #1 between 4 and 16 at the time of their case
INSERT INTO tmp_selected_asthma
SELECT rp.natural_key, rp.date,
    'Criteria 1: >= 2 Asthma Dx - Pt Aged 4-16' AS inclusion_reason
FROM nodis_case nc
JOIN random_asthma_patients rp ON rp.patient_id = nc.patient_id
WHERE nc.criteria ILIKE 'Criteria #1: Asthma%' 
AND EXTRACT(YEAR FROM AGE(nc.date, rp.date_of_birth)) BETWEEN 4 AND 16
AND rp.natural_key NOT IN (select natural_key FROM tmp_selected_asthma)
ORDER BY rp.date DESC, rp.random DESC
LIMIT 6;

-- 6 Patients with criteria #2 between 4 and 16 at the time of their case
INSERT INTO tmp_selected_asthma
SELECT rp.natural_key, rp.date, 
    'Criteria 2: >= 2 Asthma Rx - Pt Aged 4-16' AS inclusion_reason
FROM nodis_case nc
JOIN random_asthma_patients rp ON rp.patient_id = nc.patient_id
WHERE nc.criteria ILIKE 'Criteria #2: Asthma%' 
AND EXTRACT(YEAR FROM AGE(nc.date, rp.date_of_birth)) BETWEEN 4 AND 16
AND rp.natural_key NOT IN (select natural_key FROM tmp_selected_asthma)
ORDER BY rp.date DESC, rp.random DESC
LIMIT 6;

-- 6 Patients with criteria #1 over age 16 at the time of their case
INSERT INTO tmp_selected_asthma
SELECT rp.natural_key, rp.date, 
    'Criteria 1: >= 2 Asthma Dx - Pt Aged Over 16' AS inclusion_reason
FROM nodis_case nc
JOIN random_asthma_patients rp ON rp.patient_id = nc.patient_id
WHERE nc.criteria ILIKE 'Criteria #1: Asthma%' 
AND EXTRACT(YEAR FROM AGE(nc.date, rp.date_of_birth)) > 16
AND rp.natural_key NOT IN (select natural_key FROM tmp_selected_asthma)
ORDER BY rp.date DESC, rp.random DESC
LIMIT 6;

-- 7 Patients with criteria #2 over age 16 at the time of their case
INSERT INTO tmp_selected_asthma
SELECT rp.natural_key, rp.date, 
    'Criteria 2: >= 2 Asthma Rx - Pt Aged Over 16' AS inclusion_reason
FROM nodis_case nc
JOIN random_asthma_patients rp ON rp.patient_id = nc.patient_id
WHERE nc.criteria ILIKE 'Criteria #2: Asthma%' 
AND EXTRACT(YEAR FROM AGE(nc.date, rp.date_of_birth)) > 16
AND rp.natural_key NOT IN (select natural_key FROM tmp_selected_asthma)
ORDER BY rp.date DESC, rp.random DESC
LIMIT 7;

--8 patients who have a status of Deactivated
INSERT INTO tmp_selected_asthma
SELECT rp.natural_key, rp.date, 'Deactivated' AS criteria
FROM random_asthma_patients rp
JOIN nodis_case nc
ON nc.patient_id = rp.patient_id
JOIN nodis_caseactivehistory ncah
ON nc.id = ncah.case_id
WHERE ncah.status = 'D' 
ORDER BY rp.date DESC, rp.random DESC
LIMIT 8;

DROP TABLE IF EXISTS tmp_asthma_report;

SELECT substr(tsa.natural_key,1,40) as Global_ID, p.Race, p.Ethnicity, p.Date_of_Birth::date, p.gender, tsa.date as encounter_date, tsa.criteria as criteria
INTO tmp_asthma_report
FROM tmp_selected_asthma tsa
JOIN emr_patient p ON p.natural_key = tsa.natural_key
ORDER BY criteria, tsa.natural_key;

--CREATE TABLE persistent_asthma_report AS SELECT * FROM tmp_asthma_report;
--DROP TABLE persistent_asthma_report;

--SELECT * FROM tmp_selected_asthma;
--SELECT * FROM persistent_asthma_report;

--\copy persistent_asthma_report to 'C:\Files\Asthma2.csv' CSV delimiter ',' header;
