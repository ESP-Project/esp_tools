--
-- Script setup section 
--

DROP TABLE IF EXISTS mgtp_hef_w_lab_details CASCADE;
DROP TABLE IF EXISTS mgtp_cases_of_interest CASCADE;
DROP TABLE IF EXISTS mgtp_gon_events_pre CASCADE;
DROP TABLE IF EXISTS mgtp_gon_events CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_events CASCADE;
DROP TABLE IF EXISTS mgtp_syph_events CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_labs CASCADE;
DROP TABLE IF EXISTS mgtp_gon_counts CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_counts CASCADE;
DROP TABLE IF EXISTS mgtp_syph_counts CASCADE;
DROP TABLE IF EXISTS mgtp_syph_cases CASCADE;
DROP TABLE IF EXISTS mgtp_chlam_cases CASCADE;
DROP TABLE IF EXISTS mgtp_gon_cases CASCADE;
DROP TABLE IF EXISTS mgtp_hepb_pos_dna CASCADE;
DROP TABLE IF EXISTS mgtp_cpts_of_interest CASCADE;
DROP TABLE IF EXISTS mgtp_dx_codes CASCADE;
DROP TABLE IF EXISTS mgtp_diag_lab_window CASCADE;
DROP TABLE IF EXISTS mgtp_diag_fields_by_year_sub CASCADE;
DROP TABLE IF EXISTS mgtp_diag_fields_by_year CASCADE;
DROP TABLE IF EXISTS mgtp_diag_fields CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_counts CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_cases CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_meds CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_array CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_2mogap CASCADE;
DROP TABLE IF EXISTS mgtp_truvada_counts CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_all_details CASCADE;
DROP TABLE IF EXISTS mgtp_hiv_all_details_values CASCADE;
DROP TABLE IF EXISTS mgtp_spec_source_count CASCADE;
DROP TABLE IF EXISTS mgtp_spec_source_max CASCADE;
DROP TABLE IF EXISTS mgtp_spec_source_multisite CASCADE;
DROP TABLE IF EXISTS mgtp_multisite_data CASCADE;
DROP TABLE IF EXISTS mgtp_index_patients CASCADE;
DROP TABLE IF EXISTS mgtp_output_part_1 CASCADE;
DROP TABLE IF EXISTS mgtp_output_with_patient CASCADE;
DROP TABLE IF EXISTS mgtp_output_pat_and_enc CASCADE;
DROP TABLE IF EXISTS mgtp_ouput_pat_and_enc_counts CASCADE;
DROP TABLE IF EXISTS mgtp_denom_males CASCADE;


DROP TABLE IF EXISTS mgtp_report_final_output CASCADE;
DROP TABLE IF EXISTS mgtp_denom_male_counts CASCADE;


--
-- Script body 
--

-- BASE Join the hef events with the lab results
CREATE TABLE mgtp_hef_w_lab_details AS 
SELECT T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name
FROM emr_labresult T1,
hef_event T2, 
conf_labtestmap T3
WHERE T1.native_code = T3.native_code
AND T1.id = T2.object_id
AND T1.patient_id = T2.patient_id
AND test_name in ('chlamydia', 'gonorrhea')
AND T2.date >= '01-01-2008'
AND T2.date < '01-01-2019'
GROUP BY T1.id,T2.name,T2.patient_id,T2.date,T1.specimen_source,T2.object_id,T1.native_code, T1.native_name;

-- BASE - Get the cases we are interested in 
CREATE TABLE mgtp_cases_of_interest  AS 
SELECT T1.condition, T1.date, T1.patient_id, EXTRACT(YEAR FROM date) rpt_year 
FROM  nodis_case T1
WHERE  date >= '01-01-2008' 
AND date < '01-01-2019'
AND condition in ('syphilis', 'gonorrhea', 'chlamydia');

-- GONORRHEA - Gather up gonorrhea events
CREATE TABLE mgtp_gon_events_pre  AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year,
CASE WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(PHARYNGEAL|THROAT)' or native_code ~* '(PHARYNGEAL|THROAT)')then 'THROAT'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(RECTAL|ANAL)' OR native_code ~* '(RECTAL|ANAL)') THEN 'RECTAL'
WHEN (specimen_source is null or specimen_source = '') and (native_name ~* '(GENITAL|URINE|URN|ENDOCX|THINPREP|THIN PREP|PAP)' OR native_code  ~* '(GENITAL|URINE|URN|URI|UR|CERVIX|PENILE|URETHRAL|VAGINAL|ENDOCX|THINPREP|THIN PREP|PAP)') THEN 'UROG'
ELSE specimen_source END specimen_source,
specimen_source specimen_source_orig,
native_code
FROM mgtp_hef_w_lab_details T1
WHERE  name like 'lx:gonorrhea%'
AND date >= '01-01-2008'
AND date < '01-01-2019';


-- NORMALIZE THE SPECIMEN SOURCES
CREATE TABLE mgtp_gon_events AS
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year,
CASE WHEN T1.specimen_source ~* '(THROAT)' then 'THROAT'
WHEN T1.specimen_source ~* '(RECTUM|RECT|ANAL)' then  'RECTAL'
WHEN (T1.specimen_source is null or T1.specimen_source = '' OR T1.specimen_source ~* '(NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN)') then 'MISSING'
WHEN T1.specimen_source ~* '(GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)' then 'UROG' 
WHEN (T1.specimen_source is NOT null AND T1.specimen_source != '' 
	AND T1.specimen_source !~* '(THROAT|RECTUM|RECT|ANAL|NO SOURCE SPECIFIED|NONE GIVEN|SOURCE UNKNOWN|GENIT|CERV|UROG|URINE|URIN|ENDOCERV|PENILE|PENIS|URETHRAL|URETRA|VAG)') then 'OTHER'
end specimen_source,
specimen_source_orig,
native_code
FROM mgtp_gon_events_pre T1;


-- CHLAMYDIA - Gather up chlamydia events
CREATE TABLE mgtp_chlam_events  AS 
SELECT name, date, patient_id, object_id, EXTRACT(YEAR FROM date) rpt_year, specimen_source, native_code 
FROM mgtp_hef_w_lab_details
WHERE name like 'lx:chlamydia%'
AND date >= '01-01-2008'
AND date < '01-01-2019';


-- SYPHILLIS - Gather up syphillis events/tests
-- NEED TO GRAB ALL TESTS NOT JUST THOSE WITH HEF EVENTS OR ELSE YOU WILL MISS RPR TESTS
-- GROUP BY NAME and DATE TO AVOID DUPES
CREATE TABLE mgtp_syph_events AS 
SELECT name, T1.date, T1.patient_id, EXTRACT(YEAR FROM T1.date) rpt_year 
FROM emr_labresult T1
INNER JOIN conf_labtestmap T2 ON (T1.native_code = T2.native_code)
LEFT JOIN hef_event T3 ON ((T1.id = T3.object_id) AND (T1.patient_id = T3.patient_id))
WHERE T1.date >= '01-01-2006'
AND T1.date < '01-01-2019'
AND T2.test_name in ('rpr', 'vdrl','vdrl-csf', 'tppa', 'fta-abs', 'tp-igg', 'tp-igm')
AND result_string not in ('','Not Done','Not performed', 'Not tested', 'Not Tested', 'TNP', 'test')
and result_string is not null
GROUP BY name, T1.date, T1.patient_id, EXTRACT(YEAR FROM T1.date) ;


-- HEP B Gather up positive viral dna lab tests -- Any time before the end date
CREATE TABLE mgtp_hepb_pos_dna  AS 
SELECT T1.patient_id, 1::INT pos_hep_b_dna
FROM hef_event T1 
WHERE name = 'lx:hepatitis_b_viral_dna:positive'
AND date < '01-01-2019'
group by patient_id;


-- GONORRHEA - Counts & Column Creation
CREATE TABLE mgtp_gon_counts  AS 
SELECT T1.patient_id,count(*) t_gon_tests, 
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_gon_tests_08, count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_gon_tests_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_gon_tests_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_gon_tests_11, 
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_gon_tests_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_gon_tests_13, 
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_gon_tests_14, count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_gon_tests_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_gon_tests_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_gon_tests_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END)  t_gon_tests_18,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_gon_tests_08, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_gon_tests_09, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_11, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_13, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_15, 
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_17,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2018' THEN 1 END)  p_gon_tests_18,
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2008' THEN 1 END)  t_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source = 'THROAT' and  rpt_year = '2009' THEN 1 END)  t_gon_tests_throat_09, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2010' THEN 1 END)  t_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source = 'THROAT' and  rpt_year = '2011' THEN 1 END)  t_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source = 'THROAT' and  rpt_year = '2012' THEN 1 END)  t_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2013' THEN 1 END)  t_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2014' THEN 1 END)  t_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2015' THEN 1 END)  t_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2016' THEN 1 END)  t_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2017' THEN 1 END)  t_gon_tests_throat_17, 
count(CASE WHEN T1.specimen_source = 'THROAT' and rpt_year = '2018' THEN 1 END)  t_gon_tests_throat_18,
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_gon_tests_throat_08, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_gon_tests_throat_09,
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_throat_10, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_throat_11, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_throat_12, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_throat_13, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_throat_14, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_throat_15, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_throat_16, 
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_throat_17,
count(CASE WHEN T1.specimen_source = 'THROAT' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END)  p_gon_tests_throat_18,
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2008' THEN 1 END) t_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2009' THEN 1 END) t_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2010' THEN 1 END) t_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2011' THEN 1 END) t_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2012' THEN 1 END) t_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2013' THEN 1 END) t_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2014' THEN 1 END) t_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2015' THEN 1 END) t_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2016' THEN 1 END) t_gon_tests_rectal_16, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2017' THEN 1 END) t_gon_tests_rectal_17, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and rpt_year = '2018' THEN 1 END) t_gon_tests_rectal_18, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_rectal_08, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_rectal_09, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_rectal_10, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_rectal_11, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_rectal_12, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_rectal_13, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_rectal_14, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_rectal_15, 
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_rectal_16,  
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_rectal_17,
count(CASE WHEN T1.specimen_source = 'RECTAL' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_rectal_18,
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2008' THEN 1 END)  t_gon_tests_missing_08, 
count(CASE WHEN T1.specimen_source = 'MISSING' and  rpt_year = '2009' THEN 1 END)  t_gon_tests_missing_09, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2010' THEN 1 END)  t_gon_tests_missing_10, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2011' THEN 1 END)  t_gon_tests_missing_11, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2012' THEN 1 END)  t_gon_tests_missing_12, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2013' THEN 1 END)  t_gon_tests_missing_13, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2014' THEN 1 END)  t_gon_tests_missing_14, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2015' THEN 1 END)  t_gon_tests_missing_15, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2016' THEN 1 END)  t_gon_tests_missing_16, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2017' THEN 1 END)  t_gon_tests_missing_17, 
count(CASE WHEN T1.specimen_source = 'MISSING' and rpt_year = '2018' THEN 1 END)  t_gon_tests_missing_18, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_gon_tests_missing_08, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_gon_tests_missing_09, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_gon_tests_missing_10, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_gon_tests_missing_11, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_gon_tests_missing_12, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_gon_tests_missing_13, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_gon_tests_missing_14, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_gon_tests_missing_15, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_gon_tests_missing_16, 
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_gon_tests_missing_17,
count(CASE WHEN T1.specimen_source = 'MISSING' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END)  p_gon_tests_missing_18,
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2008' THEN 1 END) t_gon_tests_urog_08, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2009' THEN 1 END) t_gon_tests_urog_09, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2010' THEN 1 END) t_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2011' THEN 1 END) t_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2012' THEN 1 END) t_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2013' THEN 1 END) t_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2014' THEN 1 END) t_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2015' THEN 1 END) t_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2016' THEN 1 END) t_gon_tests_urog_16, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2017' THEN 1 END) t_gon_tests_urog_17, 
count(CASE WHEN T1.specimen_source = 'UROG' and rpt_year = '2018' THEN 1 END) t_gon_tests_urog_18, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_urog_08, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_urog_09,
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_urog_10, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_urog_11, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_urog_12, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_urog_13, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_urog_14, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_urog_15, 
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_urog_16,  
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_urog_17,
count(CASE WHEN T1.specimen_source = 'UROG' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_urog_18,
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2008' THEN 1 END) t_gon_tests_other_08, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2009' THEN 1 END) t_gon_tests_other_09, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2010' THEN 1 END) t_gon_tests_other_10, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2011' THEN 1 END) t_gon_tests_other_11, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2012' THEN 1 END) t_gon_tests_other_12, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2013' THEN 1 END) t_gon_tests_other_13, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2014' THEN 1 END) t_gon_tests_other_14, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2015' THEN 1 END) t_gon_tests_other_15, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2016' THEN 1 END) t_gon_tests_other_16, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2017' THEN 1 END) t_gon_tests_other_17, 
count(CASE WHEN T1.specimen_source = 'OTHER' and rpt_year = '2018' THEN 1 END) t_gon_tests_other_18, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2008' THEN 1 END) p_gon_tests_other_08,
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2009' THEN 1 END) p_gon_tests_other_09,
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2010' THEN 1 END) p_gon_tests_other_10, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2011' THEN 1 END) p_gon_tests_other_11, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2012' THEN 1 END) p_gon_tests_other_12, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2013' THEN 1 END) p_gon_tests_other_13, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2014' THEN 1 END) p_gon_tests_other_14, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2015' THEN 1 END) p_gon_tests_other_15, 
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2016' THEN 1 END) p_gon_tests_other_16,  
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2017' THEN 1 END) p_gon_tests_other_17,
count(CASE WHEN T1.specimen_source = 'OTHER' and T1.name like '%positive%' and rpt_year = '2018' THEN 1 END) p_gon_tests_other_18
FROM  mgtp_gon_events T1   
GROUP BY T1.patient_id;

-- CHLAMYDIA - Counts & Column Creation
CREATE TABLE mgtp_chlam_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_chlam_08, count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_chlam_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_chlam_10, count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_chlam_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_chlam_12, count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_chlam_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_chlam_14, count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_chlam_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_chlam_16, count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_chlam_17, 
count(CASE WHEN rpt_year = '2018' THEN 1 END)  t_chlam_18,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2008' THEN 1 END)  p_chlam_08, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2009' THEN 1 END)  p_chlam_09,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2010' THEN 1 END)  p_chlam_10, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2011' THEN 1 END)  p_chlam_11,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2012' THEN 1 END)  p_chlam_12, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2013' THEN 1 END)  p_chlam_13,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2014' THEN 1 END)  p_chlam_14, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2015' THEN 1 END)  p_chlam_15,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2016' THEN 1 END)  p_chlam_16, count(CASE WHEN T1.name like '%positive%' and rpt_year = '2017' THEN 1 END)  p_chlam_17,
count(CASE WHEN T1.name like '%positive%' and rpt_year = '2018' THEN 1 END)  p_chlam_18
FROM  mgtp_chlam_events T1   
GROUP BY T1.patient_id;

-- SYPHILLIS - Counts & Column Creation
CREATE TABLE mgtp_syph_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2008' THEN 1 END)  t_syph_08, 
count(CASE WHEN rpt_year = '2009' THEN 1 END)  t_syph_09,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_syph_10, 
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_syph_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_syph_12, 
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_syph_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_syph_14, 
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_syph_15,
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_syph_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_syph_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END)  t_syph_18
FROM  mgtp_syph_events T1   
GROUP BY T1.patient_id;

-- SYPHILIS - Cases
CREATE TABLE mgtp_syph_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2008' then 1 end) syphilis_08,
count(CASE WHEN T1.rpt_year = '2009' then 1 end) syphilis_09,
count(CASE WHEN T1.rpt_year = '2010' then 1 end) syphilis_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) syphilis_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) syphilis_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) syphilis_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) syphilis_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) syphilis_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) syphilis_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) syphilis_17,
count(CASE WHEN T1.rpt_year = '2018' then 1 end) syphilis_18
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'syphilis' GROUP BY T1.patient_id;

-- GONORRHEA - Cases
CREATE TABLE mgtp_gon_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2008' then 1 end) gon_08,
count(CASE WHEN T1.rpt_year = '2009' then 1 end) gon_09,
count(CASE WHEN T1.rpt_year = '2010' then 1 end) gon_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) gon_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) gon_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) gon_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) gon_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) gon_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) gon_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) gon_17,
count(CASE WHEN T1.rpt_year = '2018' then 1 end) gon_18
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'gonorrhea' GROUP BY T1.patient_id;

-- CHLAMYIDA - Cases
CREATE TABLE mgtp_chlam_cases  AS 
SELECT T1.patient_id, 
count(CASE WHEN T1.rpt_year = '2008' then 1 end) chlam_08,
count(CASE WHEN T1.rpt_year = '2009' then 1 end) chlam_09,
count(CASE WHEN T1.rpt_year = '2010' then 1 end) chlam_10,
count(CASE WHEN T1.rpt_year = '2011' then 1 end) chlam_11,
count(CASE WHEN T1.rpt_year = '2012' then 1 end) chlam_12, 
count(CASE WHEN T1.rpt_year = '2013' then 1 end) chlam_13,
count(CASE WHEN T1.rpt_year = '2014' then 1 end) chlam_14, 
count(CASE WHEN T1.rpt_year = '2015' then 1 end) chlam_15, 
count(CASE WHEN T1.rpt_year = '2016' then 1 end) chlam_16,
count(CASE WHEN T1.rpt_year = '2017' then 1 end) chlam_17,
count(CASE WHEN T1.rpt_year = '2018' then 1 end) chlam_18
FROM  mgtp_cases_of_interest T1   
WHERE condition = 'chlamydia' GROUP BY T1.patient_id;


-- Anal Cytology Test - Counts & Column Creation
CREATE TABLE mgtp_cpts_of_interest  AS 
SELECT T1.patient_id, 
1::INT as anal_cytology_test_ever 
FROM  emr_labresult T1   
WHERE native_code like '88160%' 
GROUP BY T1.patient_id
-- Atrius Lab Order
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_laborder T1
WHERE procedure_name in ( 'CYTOLOGY ANAL', 'CYTOLOGY THINPREP VIAL (ANAL)')
GROUP BY T1.patient_id
-- CHA Lab Order
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_laborder T1
WHERE procedure_name in ('CYTOLOGY SPECIMEN', 'CYTOLOGY SPECIMEN (NON-GYN)') and specimen_source in ('ANAL', 'ANAL PAP')
GROUP BY T1.patient_id
-- MLCHC Native Name and Procedure Names
UNION
SELECT T1.patient_id, 
1::INT AS anal_cytology_test_ever 
FROM emr_labresult T1
WHERE procedure_name in ('P-CN, NON-GYN CYTOLOGY (ANAL PAP, URINE CYTOLOGY)', 'P-CN, NON-GYN CYTOLOGY (ANAL PAP, URINE CYTOLOGY) CMH') 
or native_name in ('Anal Pap-CYTOLOGY, NON-GYN', 'Anal Pap-NON-GYN CYTOLOGY', 'Anal Pap-NON GYNECOLOGIC CYTOLOGY (ANAL PAP, URINE CYTOLOGY)')
GROUP BY T1.patient_id;



-- DX Gather up dx_codes 
CREATE TABLE mgtp_dx_codes  AS 
SELECT * FROM emr_encounter_dx_codes 
WHERE dx_code_id ~'^(icd9:796.7)' --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
OR dx_code_id in (
'icd9:788.7',   --urethral discharge/penile discharge
'icd10:R36.0',  --urethral discharge/penile discharge
'icd10:R36.9',  --urethral discharge/penile discharge
'icd9:099.40',  --urethritis
'icd9:597.80',  --urethritis
'icd10:N34.1',  --urethritis 
'icd10:N34.2',  --urethritis

'icd9:788.1',   --dysuria
'icd10:R30.0',  --dysuria
'icd9:604.90',  --epididymitis
'icd9:604.91',  --epididymitis
'icd9:604.99',  --epididymitis
'icd10:N45.1',  --epididymitis 
'icd9:608.89',  --testicular pain
'icd10:N50.81', --testicular pain
'icd10:N50.811',--testicular pain
'icd10:N50.812',--testicular pain
'icd10:N50.819',--testicular pain
'icd9:569.42',  --proctitis/rectal pain
'icd9:569.49',  --proctitis/rectal pain
'icd10:K62.89', --proctitis/rectal pain
'icd9:569.3',   --rectal bleeding
'icd10:K62.5',  --rectal bleeding
'icd9:462', 	--pharyngitis
'icd10:J02.9',  --pharyngitis
'icd9:463',     --tonsillitis
'icd10:J03.9',  --tonsillitis
'icd10:J03.90', --tonsillitis
'icd9:784.1',   --throat pain
'icd10:R07.0',  --throat pain
'icd9:372.00',  --conjunctivitis
'icd9:372.30',  --conjunctivitis
'icd10:H10.30', --conjunctivitis
'icd10:H10.31', --conjunctivitis
'icd10:H10.32', --conjunctivitis
'icd10:H10.33', --conjunctivitis
'icd10:H10.9',  --conjunctivitis
'icd9:379.91',  -- eye pain
'icd10:H57.10', -- eye pain
'icd10:H57.11', -- eye pain
'icd10:H57.12', -- eye pain
'icd10:H57.13', -- eye pain

'icd9:616.10',  --vaginitis
'icd10:N76.0',  --vaginitis
'icd10:N76.1',  --vaginitis
'icd10:N76.2',  --vaginitis
'icd10:N76.3',   --vaginitis
'icd9:616.0',   --cervicitis
'icd10:N72',    --cervicitis
'icd9:623.5',   --vaginal leucorrhea
'icd10:N89.8',  --vaginal leucorrhea
'icd9:099.54',  --chlamydia
'icd10:A56.19', --chlamydia
'icd9:V74.5',   --screening for STIs
'icd10:Z11.3',  --screening for STIs
'icd9:569.44',  --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ 
'icd9:230.5',   --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd9:230.6',   --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd10:D01.3',  --abnormal anal cytology, anal dysplasia, or anal carcinoma in situ
'icd9:V01.6',   --contact with or exposure to venereal disease
'icd10:Z20.2',  --contact with or exposure to venereal disease
'icd10:Z20.6',  --contact with or exposure to venereal disease
'icd9:V69.2',   --high risk sexual behavior
'icd10:Z72.5',  --high risk sexual behavior
'icd9:V65.44',  --HIV counseling
'icd10:Z71.7'   --HIV counseling
);


-- OLD QUERY -- USE FOR VALIDATION ONLY
-- CREATE TABLE mgtp_diag_lab_window as 
-- select T1.patient_id, T1.date as symptom_date, T1.id as enc_id, T2.dx_code_id, T3.date as lab_date, T3.name, T3.id as lab_id, T1.date - T3.date as date_diff, extract(year from T3.date) rpt_year
-- from emr_encounter T1,
-- mgtp_dx_codes T2,
-- mgtp_hef_w_lab_details T3
-- WHERE T1.id = T2.encounter_id
-- AND T1.patient_id = T3.patient_id
-- AND T3.name ilike '%gonorrhea%'
-- AND T1.date BETWEEN T3.date - interval '7 days' and T3.date + interval '7 days'
-- order by patient_id;

CREATE TABLE mgtp_diag_lab_window as
SELECT T1.patient_id, T2.dx_code_id, T3.date as lab_date, T3.name, T3.id as lab_id, extract(year from T3.date) rpt_year
from emr_encounter T1,
mgtp_dx_codes T2,
mgtp_hef_w_lab_details T3
WHERE T1.id = T2.encounter_id
AND T1.patient_id = T3.patient_id
AND T3.name ilike '%gonorrhea%'
AND T1.date BETWEEN T3.date - interval '7 days' and T3.date + interval '7 days'
GROUP BY T1.patient_id, T2.dx_code_id, T3.date, T3.name, T3.id, rpt_year
ORDER BY patient_id;


set enable_nestloop=off;

-- DX Create Fields

CREATE TABLE mgtp_diag_fields  AS 
SELECT T1.patient_id, 
rpt_year,
lab_id,
MAX(CASE WHEN T1.dx_code_id like 'icd9:796.7%' or T1.dx_code_id in ('icd9:569.44', 'icd9:230.5','icd9:230.6','icd10:D01.3') then 1 end) abnormal_anal_cytology,
MAX(CASE WHEN (T1.dx_code_id like 'icd9:796.7%' or T1.dx_code_id in ('icd9:569.44', 'icd9:230.5','icd9:230.6','icd10:D01.3')) and name = 'lx:gonorrhea:positive'  then 1 end) abnormal_anal_cytology_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:788.7', 'icd10:R36.0', 'icd10:R36.9') then 1 end) urethral_discharge,
MAX(CASE WHEN T1.dx_code_id in ('icd9:788.7', 'icd10:R36.0', 'icd10:R36.9') and name = 'lx:gonorrhea:positive' then 1 end) urethral_discharge_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:099.40', 'icd9:597.80', 'icd10:N34.1', 'icd10:N34.2') then 1 end) urethritis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:099.40', 'icd9:597.80', 'icd10:N34.1', 'icd10:N34.2') and name = 'lx:gonorrhea:positive' then 1 end) urethritis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:788.1', 'icd10:R30.0') then 1 end) dysuria,
MAX(CASE WHEN T1.dx_code_id in ('icd9:788.1', 'icd10:R30.0') and name = 'lx:gonorrhea:positive' then 1 end) dysuria_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:604.90', 'icd9:604.91', 'icd9:604.99', 'icd10:N45.1') then 1 end) epididymitis, 
MAX(CASE WHEN T1.dx_code_id in ('icd9:604.90', 'icd9:604.91', 'icd9:604.99', 'icd10:N45.1') and name = 'lx:gonorrhea:positive' then 1 end) epididymitis_gpos, 
MAX(CASE WHEN T1.dx_code_id in ('icd9:608.89', 'icd10:N50.81', 'icd10:N50.811', 'icd10:N50.812', 'icd10:N50.819') then 1 end) testicular_pain,
MAX(CASE WHEN T1.dx_code_id in ('icd9:608.89', 'icd10:N50.81', 'icd10:N50.811', 'icd10:N50.812', 'icd10:N50.819') and name = 'lx:gonorrhea:positive'  then 1 end) testicular_pain_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:569.42', 'icd9:569.49', 'icd10:K62.89') then 1 end) proctitis_rectal_pain,
MAX(CASE WHEN T1.dx_code_id in ('icd9:569.42', 'icd9:569.49', 'icd10:K62.89') and name = 'lx:gonorrhea:positive' then 1 end) proctitis_rectal_pain_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:569.3', 'icd10:K62.5') then 1 end) rectal_bleeding,
MAX(CASE WHEN T1.dx_code_id in ('icd9:569.3', 'icd10:K62.5') and name = 'lx:gonorrhea:positive' then 1 end) rectal_bleeding_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:462', 'icd10:J02.9') then 1 end) pharyngitis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:462', 'icd10:J02.9') and name = 'lx:gonorrhea:positive' then 1 end) pharyngitis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:463', 'icd10:J03.9', 'icd10:J03.90') then 1 end) tonsillitis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:463', 'icd10:J03.9', 'icd10:J03.90') and name = 'lx:gonorrhea:positive' then 1 end) tonsillitis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:784.1', 'icd10:R07.0') then 1 end) throat_pain,
MAX(CASE WHEN T1.dx_code_id in ('icd9:784.1', 'icd10:R07.0') and name = 'lx:gonorrhea:positive' then 1 end) throat_pain_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:372.00', 'icd9:372.30', 'icd10:H10.30', 'icd10:H10.31', 'icd10:H10.32', 'icd10:H10.33', 'icd10:H10.9') then 1 end) conjunctivitis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:372.00', 'icd9:372.30', 'icd10:H10.30', 'icd10:H10.31', 'icd10:H10.32', 'icd10:H10.33', 'icd10:H10.9') and name = 'lx:gonorrhea:positive' then 1 end) conjunctivitis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:379.91', 'icd10:H57.10', 'icd10:H57.11', 'icd10:H57.12', 'icd10:H57.13') then 1 end) eye_pain,
MAX(CASE WHEN T1.dx_code_id in ('icd9:379.91', 'icd10:H57.10', 'icd10:H57.11', 'icd10:H57.12', 'icd10:H57.13') and name = 'lx:gonorrhea:positive'  then 1 end) eye_pain_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:616.10', 'icd10:N76.0', 'icd10:N76.1', 'icd10:N76.2', 'icd10:N76.3') then 1 end) vaginitis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:616.10', 'icd10:N76.0', 'icd10:N76.1', 'icd10:N76.2', 'icd10:N76.3') and name = 'lx:gonorrhea:positive' then 1 end) vaginitis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:616.0', 'icd10:N72') then 1 end) cervicitis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:616.0', 'icd10:N72') and name = 'lx:gonorrhea:positive' then 1 end) cervicitis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:623.5', 'icd10:N89.8') then 1 end) vaginal_leucorrhea,
MAX(CASE WHEN T1.dx_code_id in ('icd9:623.5', 'icd10:N89.8') and name = 'lx:gonorrhea:positive' then 1 end) vaginal_leucorrhea_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:099.54','icd10:A56.19') then 1 end) chlamydia,
MAX(CASE WHEN T1.dx_code_id in ('icd9:099.54','icd10:A56.19') and name = 'lx:gonorrhea:positive'  then 1 end) chlamydia_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V74.5', 'icd10:Z11.3') then 1 end) screening_for_stis,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V74.5', 'icd10:Z11.3') and name = 'lx:gonorrhea:positive'  then 1 end) screening_for_stis_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V01.6', 'icd10:Z20.2','icd10:Z20.6') then 1 end)  cont_or_exp_to_vd,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V01.6', 'icd10:Z20.2','icd10:Z20.6') and name = 'lx:gonorrhea:positive' then 1 end)  cont_or_exp_to_vd_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V69.2', 'icd10:Z72.5') then 1 end) high_risk_sexual_behavior,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V69.2', 'icd10:Z72.5') and name = 'lx:gonorrhea:positive' then 1 end) high_risk_sexual_behavior_gpos,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V65.44','icd10:Z71.7') then 1 end) hiv_counseling,
MAX(CASE WHEN T1.dx_code_id in ('icd9:V65.44','icd10:Z71.7') and name = 'lx:gonorrhea:positive' then 1 end) hiv_counseling_gpos
FROM mgtp_diag_lab_window T1
GROUP BY T1.patient_id, rpt_year, lab_id;

set enable_nestloop=on;


CREATE TABLE mgtp_diag_fields_by_year_sub AS
SELECT patient_id, rpt_year,
COUNT(CASE WHEN abnormal_anal_cytology >= 1  then 1 end) abnormal_anal_cytology,
COUNT(CASE WHEN abnormal_anal_cytology_gpos >= 1  then 1 end) abnormal_anal_cytology_gpos,
COUNT(CASE WHEN urethral_discharge >= 1  then 1 end) urethral_discharge,
COUNT(CASE WHEN urethral_discharge_gpos >= 1  then 1 end) urethral_discharge_gpos,
COUNT(CASE WHEN urethritis >= 1  then 1 end) urethritis,
COUNT(CASE WHEN urethritis_gpos >= 1  then 1 end) urethritis_gpos,
COUNT(CASE WHEN dysuria >= 1  then 1 end) dysuria,
COUNT(CASE WHEN dysuria_gpos >= 1  then 1 end) dysuria_gpos,
COUNT(CASE WHEN epididymitis >= 1  then 1 end) epididymitis,
COUNT(CASE WHEN epididymitis_gpos >= 1  then 1 end) epididymitis_gpos,
COUNT(CASE WHEN testicular_pain >= 1  then 1 end) testicular_pain,
COUNT(CASE WHEN testicular_pain_gpos >= 1  then 1 end) testicular_pain_gpos,
COUNT(CASE WHEN proctitis_rectal_pain >= 1  then 1 end) proctitis_rectal_pain,
COUNT(CASE WHEN proctitis_rectal_pain_gpos >= 1  then 1 end) proctitis_rectal_pain_gpos,
COUNT(CASE WHEN rectal_bleeding >= 1  then 1 end) rectal_bleeding,
COUNT(CASE WHEN rectal_bleeding_gpos >= 1  then 1 end) rectal_bleeding_gpos,
COUNT(CASE WHEN pharyngitis >= 1  then 1 end) pharyngitis,
COUNT(CASE WHEN pharyngitis_gpos >= 1  then 1 end) pharyngitis_gpos,
COUNT(CASE WHEN tonsillitis >= 1  then 1 end) tonsillitis,
COUNT(CASE WHEN tonsillitis_gpos >= 1  then 1 end) tonsillitis_gpos,
COUNT(CASE WHEN throat_pain >= 1  then 1 end) throat_pain,
COUNT(CASE WHEN throat_pain_gpos >= 1  then 1 end) throat_pain_gpos,
COUNT(CASE WHEN conjunctivitis >= 1  then 1 end) conjunctivitis,
COUNT(CASE WHEN conjunctivitis_gpos >= 1  then 1 end) conjunctivitis_gpos,
COUNT(CASE WHEN eye_pain >= 1  then 1 end) eye_pain,
COUNT(CASE WHEN eye_pain_gpos >= 1  then 1 end) eye_pain_gpos,
COUNT(CASE WHEN vaginitis >= 1  then 1 end) vaginitis,
COUNT(CASE WHEN vaginitis_gpos >= 1  then 1 end) vaginitis_gpos,
COUNT(CASE WHEN cervicitis >= 1  then 1 end) cervicitis,
COUNT(CASE WHEN cervicitis_gpos >= 1  then 1 end) cervicitis_gpos,
COUNT(CASE WHEN vaginal_leucorrhea >= 1  then 1 end) vaginal_leucorrhea,
COUNT(CASE WHEN vaginal_leucorrhea_gpos >= 1  then 1 end) vaginal_leucorrhea_gpos,
COUNT(CASE WHEN chlamydia >= 1  then 1 end) chlamydia,
COUNT(CASE WHEN chlamydia_gpos >= 1  then 1 end) chlamydia_gpos,
COUNT(CASE WHEN screening_for_stis >= 1  then 1 end) screening_for_stis,
COUNT(CASE WHEN screening_for_stis_gpos >= 1  then 1 end) screening_for_stis_gpos,
COUNT(CASE WHEN cont_or_exp_to_vd >= 1  then 1 end) cont_or_exp_to_vd,
COUNT(CASE WHEN cont_or_exp_to_vd_gpos >= 1  then 1 end) cont_or_exp_to_vd_gpos,
COUNT(CASE WHEN high_risk_sexual_behavior >= 1  then 1 end) high_risk_sexual_behavior,
COUNT(CASE WHEN high_risk_sexual_behavior_gpos >= 1  then 1 end) high_risk_sexual_behavior_gpos,
COUNT(CASE WHEN hiv_counseling >= 1  then 1 end) hiv_counseling,
COUNT(CASE WHEN hiv_counseling_gpos >= 1  then 1 end) hiv_counseling_gpos
FROM mgtp_diag_fields
GROUP BY patient_id, rpt_year;


CREATE TABLE mgtp_diag_fields_by_year AS
SELECT patient_id,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2008 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_08,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2009 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_09,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2010 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_10,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2011 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_11,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2012 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_12,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2013 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_13,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2014 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_14,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2015 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_15,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2016 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_16,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2017 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_17,
MAX(CASE WHEN abnormal_anal_cytology >= 1 and rpt_year = 2018 then abnormal_anal_cytology else 0 end) abnormal_anal_cytology_18,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2008 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_08,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2009 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_09,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2010 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_10,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2011 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_11,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2012 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_12,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2013 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_13,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2014 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_14,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2015 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_15,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2016 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_16,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2017 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_17,
MAX(CASE WHEN abnormal_anal_cytology_gpos >= 1 and rpt_year = 2018 then abnormal_anal_cytology_gpos else 0 end) abnormal_anal_cytology_gpos_18,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2008 then urethral_discharge else 0 end) urethral_discharge_08,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2009 then urethral_discharge else 0 end) urethral_discharge_09,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2010 then urethral_discharge else 0 end) urethral_discharge_10,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2011 then urethral_discharge else 0 end) urethral_discharge_11,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2012 then urethral_discharge else 0 end) urethral_discharge_12,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2013 then urethral_discharge else 0 end) urethral_discharge_13,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2014 then urethral_discharge else 0 end) urethral_discharge_14,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2015 then urethral_discharge else 0 end) urethral_discharge_15,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2016 then urethral_discharge else 0 end) urethral_discharge_16,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2017 then urethral_discharge else 0 end) urethral_discharge_17,
MAX(CASE WHEN urethral_discharge >= 1 and rpt_year = 2018 then urethral_discharge else 0 end) urethral_discharge_18,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2008 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_08,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2009 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_09,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2010 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_10,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2011 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_11,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2012 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_12,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2013 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_13,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2014 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_14,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2015 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_15,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2016 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_16,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2017 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_17,
MAX(CASE WHEN urethral_discharge_gpos >= 1 and rpt_year = 2018 then urethral_discharge_gpos else 0 end) urethral_discharge_gpos_18,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2008 then urethritis else 0 end) urethritis_08,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2009 then urethritis else 0 end) urethritis_09,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2010 then urethritis else 0 end) urethritis_10,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2011 then urethritis else 0 end) urethritis_11,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2012 then urethritis else 0 end) urethritis_12,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2013 then urethritis else 0 end) urethritis_13,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2014 then urethritis else 0 end) urethritis_14,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2015 then urethritis else 0 end) urethritis_15,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2016 then urethritis else 0 end) urethritis_16,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2017 then urethritis else 0 end) urethritis_17,
MAX(CASE WHEN urethritis >= 1 and rpt_year = 2018 then urethritis else 0 end) urethritis_18,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2008 then urethritis_gpos else 0 end) urethritis_gpos_08,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2009 then urethritis_gpos else 0 end) urethritis_gpos_09,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2010 then urethritis_gpos else 0 end) urethritis_gpos_10,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2011 then urethritis_gpos else 0 end) urethritis_gpos_11,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2012 then urethritis_gpos else 0 end) urethritis_gpos_12,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2013 then urethritis_gpos else 0 end) urethritis_gpos_13,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2014 then urethritis_gpos else 0 end) urethritis_gpos_14,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2015 then urethritis_gpos else 0 end) urethritis_gpos_15,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2016 then urethritis_gpos else 0 end) urethritis_gpos_16,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2017 then urethritis_gpos else 0 end) urethritis_gpos_17,
MAX(CASE WHEN urethritis_gpos >= 1 and rpt_year = 2018 then urethritis_gpos else 0 end) urethritis_gpos_18,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2008 then dysuria else 0 end) dysuria_08,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2009 then dysuria else 0 end) dysuria_09,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2010 then dysuria else 0 end) dysuria_10,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2011 then dysuria else 0 end) dysuria_11,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2012 then dysuria else 0 end) dysuria_12,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2013 then dysuria else 0 end) dysuria_13,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2014 then dysuria else 0 end) dysuria_14,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2015 then dysuria else 0 end) dysuria_15,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2016 then dysuria else 0 end) dysuria_16,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2017 then dysuria else 0 end) dysuria_17,
MAX(CASE WHEN dysuria >= 1 and rpt_year = 2018 then dysuria else 0 end) dysuria_18,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2008 then dysuria_gpos else 0 end) dysuria_gpos_08,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2009 then dysuria_gpos else 0 end) dysuria_gpos_09,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2010 then dysuria_gpos else 0 end) dysuria_gpos_10,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2011 then dysuria_gpos else 0 end) dysuria_gpos_11,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2012 then dysuria_gpos else 0 end) dysuria_gpos_12,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2013 then dysuria_gpos else 0 end) dysuria_gpos_13,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2014 then dysuria_gpos else 0 end) dysuria_gpos_14,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2015 then dysuria_gpos else 0 end) dysuria_gpos_15,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2016 then dysuria_gpos else 0 end) dysuria_gpos_16,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2017 then dysuria_gpos else 0 end) dysuria_gpos_17,
MAX(CASE WHEN dysuria_gpos >= 1 and rpt_year = 2018 then dysuria_gpos else 0 end) dysuria_gpos_18,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2008 then epididymitis else 0 end) epididymitis_08,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2009 then epididymitis else 0 end) epididymitis_09,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2010 then epididymitis else 0 end) epididymitis_10,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2011 then epididymitis else 0 end) epididymitis_11,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2012 then epididymitis else 0 end) epididymitis_12,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2013 then epididymitis else 0 end) epididymitis_13,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2014 then epididymitis else 0 end) epididymitis_14,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2015 then epididymitis else 0 end) epididymitis_15,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2016 then epididymitis else 0 end) epididymitis_16,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2017 then epididymitis else 0 end) epididymitis_17,
MAX(CASE WHEN epididymitis >= 1 and rpt_year = 2018 then epididymitis else 0 end) epididymitis_18,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2008 then epididymitis_gpos else 0 end) epididymitis_gpos_08,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2009 then epididymitis_gpos else 0 end) epididymitis_gpos_09,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2010 then epididymitis_gpos else 0 end) epididymitis_gpos_10,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2011 then epididymitis_gpos else 0 end) epididymitis_gpos_11,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2012 then epididymitis_gpos else 0 end) epididymitis_gpos_12,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2013 then epididymitis_gpos else 0 end) epididymitis_gpos_13,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2014 then epididymitis_gpos else 0 end) epididymitis_gpos_14,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2015 then epididymitis_gpos else 0 end) epididymitis_gpos_15,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2016 then epididymitis_gpos else 0 end) epididymitis_gpos_16,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2017 then epididymitis_gpos else 0 end) epididymitis_gpos_17,
MAX(CASE WHEN epididymitis_gpos >= 1 and rpt_year = 2018 then epididymitis_gpos else 0 end) epididymitis_gpos_18,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2008 then testicular_pain else 0 end) testicular_pain_08,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2009 then testicular_pain else 0 end) testicular_pain_09,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2010 then testicular_pain else 0 end) testicular_pain_10,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2011 then testicular_pain else 0 end) testicular_pain_11,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2012 then testicular_pain else 0 end) testicular_pain_12,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2013 then testicular_pain else 0 end) testicular_pain_13,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2014 then testicular_pain else 0 end) testicular_pain_14,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2015 then testicular_pain else 0 end) testicular_pain_15,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2016 then testicular_pain else 0 end) testicular_pain_16,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2017 then testicular_pain else 0 end) testicular_pain_17,
MAX(CASE WHEN testicular_pain >= 1 and rpt_year = 2018 then testicular_pain else 0 end) testicular_pain_18,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2008 then testicular_pain_gpos else 0 end) testicular_pain_gpos_08,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2009 then testicular_pain_gpos else 0 end) testicular_pain_gpos_09,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2010 then testicular_pain_gpos else 0 end) testicular_pain_gpos_10,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2011 then testicular_pain_gpos else 0 end) testicular_pain_gpos_11,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2012 then testicular_pain_gpos else 0 end) testicular_pain_gpos_12,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2013 then testicular_pain_gpos else 0 end) testicular_pain_gpos_13,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2014 then testicular_pain_gpos else 0 end) testicular_pain_gpos_14,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2015 then testicular_pain_gpos else 0 end) testicular_pain_gpos_15,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2016 then testicular_pain_gpos else 0 end) testicular_pain_gpos_16,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2017 then testicular_pain_gpos else 0 end) testicular_pain_gpos_17,
MAX(CASE WHEN testicular_pain_gpos >= 1 and rpt_year = 2018 then testicular_pain_gpos else 0 end) testicular_pain_gpos_18,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2008 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_08,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2009 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_09,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2010 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_10,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2011 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_11,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2012 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_12,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2013 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_13,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2014 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_14,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2015 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_15,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2016 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_16,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2017 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_17,
MAX(CASE WHEN proctitis_rectal_pain >= 1 and rpt_year = 2018 then proctitis_rectal_pain else 0 end) proctitis_rectal_pain_18,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2008 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_08,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2009 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_09,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2010 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_10,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2011 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_11,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2012 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_12,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2013 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_13,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2014 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_14,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2015 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_15,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2016 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_16,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2017 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_17,
MAX(CASE WHEN proctitis_rectal_pain_gpos >= 1 and rpt_year = 2018 then proctitis_rectal_pain_gpos else 0 end) proctitis_rectal_pain_gpos_18,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2008 then rectal_bleeding else 0 end) rectal_bleeding_08,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2009 then rectal_bleeding else 0 end) rectal_bleeding_09,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2010 then rectal_bleeding else 0 end) rectal_bleeding_10,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2011 then rectal_bleeding else 0 end) rectal_bleeding_11,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2012 then rectal_bleeding else 0 end) rectal_bleeding_12,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2013 then rectal_bleeding else 0 end) rectal_bleeding_13,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2014 then rectal_bleeding else 0 end) rectal_bleeding_14,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2015 then rectal_bleeding else 0 end) rectal_bleeding_15,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2016 then rectal_bleeding else 0 end) rectal_bleeding_16,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2017 then rectal_bleeding else 0 end) rectal_bleeding_17,
MAX(CASE WHEN rectal_bleeding >= 1 and rpt_year = 2018 then rectal_bleeding else 0 end) rectal_bleeding_18,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2008 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_08,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2009 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_09,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2010 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_10,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2011 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_11,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2012 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_12,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2013 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_13,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2014 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_14,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2015 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_15,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2016 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_16,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2017 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_17,
MAX(CASE WHEN rectal_bleeding_gpos >= 1 and rpt_year = 2018 then rectal_bleeding_gpos else 0 end) rectal_bleeding_gpos_18,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2008 then pharyngitis else 0 end) pharyngitis_08,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2009 then pharyngitis else 0 end) pharyngitis_09,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2010 then pharyngitis else 0 end) pharyngitis_10,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2011 then pharyngitis else 0 end) pharyngitis_11,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2012 then pharyngitis else 0 end) pharyngitis_12,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2013 then pharyngitis else 0 end) pharyngitis_13,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2014 then pharyngitis else 0 end) pharyngitis_14,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2015 then pharyngitis else 0 end) pharyngitis_15,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2016 then pharyngitis else 0 end) pharyngitis_16,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2017 then pharyngitis else 0 end) pharyngitis_17,
MAX(CASE WHEN pharyngitis >= 1 and rpt_year = 2018 then pharyngitis else 0 end) pharyngitis_18,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2008 then pharyngitis_gpos else 0 end) pharyngitis_gpos_08,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2009 then pharyngitis_gpos else 0 end) pharyngitis_gpos_09,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2010 then pharyngitis_gpos else 0 end) pharyngitis_gpos_10,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2011 then pharyngitis_gpos else 0 end) pharyngitis_gpos_11,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2012 then pharyngitis_gpos else 0 end) pharyngitis_gpos_12,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2013 then pharyngitis_gpos else 0 end) pharyngitis_gpos_13,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2014 then pharyngitis_gpos else 0 end) pharyngitis_gpos_14,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2015 then pharyngitis_gpos else 0 end) pharyngitis_gpos_15,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2016 then pharyngitis_gpos else 0 end) pharyngitis_gpos_16,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2017 then pharyngitis_gpos else 0 end) pharyngitis_gpos_17,
MAX(CASE WHEN pharyngitis_gpos >= 1 and rpt_year = 2018 then pharyngitis_gpos else 0 end) pharyngitis_gpos_18,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2008 then tonsillitis else 0 end) tonsillitis_08,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2009 then tonsillitis else 0 end) tonsillitis_09,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2010 then tonsillitis else 0 end) tonsillitis_10,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2011 then tonsillitis else 0 end) tonsillitis_11,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2012 then tonsillitis else 0 end) tonsillitis_12,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2013 then tonsillitis else 0 end) tonsillitis_13,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2014 then tonsillitis else 0 end) tonsillitis_14,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2015 then tonsillitis else 0 end) tonsillitis_15,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2016 then tonsillitis else 0 end) tonsillitis_16,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2017 then tonsillitis else 0 end) tonsillitis_17,
MAX(CASE WHEN tonsillitis >= 1 and rpt_year = 2018 then tonsillitis else 0 end) tonsillitis_18,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2008 then tonsillitis_gpos else 0 end) tonsillitis_gpos_08,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2009 then tonsillitis_gpos else 0 end) tonsillitis_gpos_09,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2010 then tonsillitis_gpos else 0 end) tonsillitis_gpos_10,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2011 then tonsillitis_gpos else 0 end) tonsillitis_gpos_11,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2012 then tonsillitis_gpos else 0 end) tonsillitis_gpos_12,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2013 then tonsillitis_gpos else 0 end) tonsillitis_gpos_13,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2014 then tonsillitis_gpos else 0 end) tonsillitis_gpos_14,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2015 then tonsillitis_gpos else 0 end) tonsillitis_gpos_15,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2016 then tonsillitis_gpos else 0 end) tonsillitis_gpos_16,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2017 then tonsillitis_gpos else 0 end) tonsillitis_gpos_17,
MAX(CASE WHEN tonsillitis_gpos >= 1 and rpt_year = 2018 then tonsillitis_gpos else 0 end) tonsillitis_gpos_18,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2008 then throat_pain else 0 end) throat_pain_08,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2009 then throat_pain else 0 end) throat_pain_09,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2010 then throat_pain else 0 end) throat_pain_10,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2011 then throat_pain else 0 end) throat_pain_11,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2012 then throat_pain else 0 end) throat_pain_12,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2013 then throat_pain else 0 end) throat_pain_13,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2014 then throat_pain else 0 end) throat_pain_14,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2015 then throat_pain else 0 end) throat_pain_15,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2016 then throat_pain else 0 end) throat_pain_16,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2017 then throat_pain else 0 end) throat_pain_17,
MAX(CASE WHEN throat_pain >= 1 and rpt_year = 2018 then throat_pain else 0 end) throat_pain_18,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2008 then throat_pain_gpos else 0 end) throat_pain_gpos_08,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2009 then throat_pain_gpos else 0 end) throat_pain_gpos_09,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2010 then throat_pain_gpos else 0 end) throat_pain_gpos_10,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2011 then throat_pain_gpos else 0 end) throat_pain_gpos_11,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2012 then throat_pain_gpos else 0 end) throat_pain_gpos_12,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2013 then throat_pain_gpos else 0 end) throat_pain_gpos_13,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2014 then throat_pain_gpos else 0 end) throat_pain_gpos_14,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2015 then throat_pain_gpos else 0 end) throat_pain_gpos_15,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2016 then throat_pain_gpos else 0 end) throat_pain_gpos_16,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2017 then throat_pain_gpos else 0 end) throat_pain_gpos_17,
MAX(CASE WHEN throat_pain_gpos >= 1 and rpt_year = 2018 then throat_pain_gpos else 0 end) throat_pain_gpos_18,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2008 then conjunctivitis else 0 end) conjunctivitis_08,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2009 then conjunctivitis else 0 end) conjunctivitis_09,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2010 then conjunctivitis else 0 end) conjunctivitis_10,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2011 then conjunctivitis else 0 end) conjunctivitis_11,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2012 then conjunctivitis else 0 end) conjunctivitis_12,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2013 then conjunctivitis else 0 end) conjunctivitis_13,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2014 then conjunctivitis else 0 end) conjunctivitis_14,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2015 then conjunctivitis else 0 end) conjunctivitis_15,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2016 then conjunctivitis else 0 end) conjunctivitis_16,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2017 then conjunctivitis else 0 end) conjunctivitis_17,
MAX(CASE WHEN conjunctivitis >= 1 and rpt_year = 2018 then conjunctivitis else 0 end) conjunctivitis_18,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2008 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_08,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2009 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_09,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2010 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_10,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2011 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_11,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2012 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_12,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2013 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_13,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2014 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_14,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2015 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_15,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2016 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_16,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2017 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_17,
MAX(CASE WHEN conjunctivitis_gpos >= 1 and rpt_year = 2018 then conjunctivitis_gpos else 0 end) conjunctivitis_gpos_18,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2008 then eye_pain else 0 end) eye_pain_08,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2009 then eye_pain else 0 end) eye_pain_09,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2010 then eye_pain else 0 end) eye_pain_10,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2011 then eye_pain else 0 end) eye_pain_11,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2012 then eye_pain else 0 end) eye_pain_12,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2013 then eye_pain else 0 end) eye_pain_13,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2014 then eye_pain else 0 end) eye_pain_14,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2015 then eye_pain else 0 end) eye_pain_15,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2016 then eye_pain else 0 end) eye_pain_16,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2017 then eye_pain else 0 end) eye_pain_17,
MAX(CASE WHEN eye_pain >= 1 and rpt_year = 2018 then eye_pain else 0 end) eye_pain_18,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2008 then eye_pain_gpos else 0 end) eye_pain_gpos_08,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2009 then eye_pain_gpos else 0 end) eye_pain_gpos_09,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2010 then eye_pain_gpos else 0 end) eye_pain_gpos_10,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2011 then eye_pain_gpos else 0 end) eye_pain_gpos_11,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2012 then eye_pain_gpos else 0 end) eye_pain_gpos_12,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2013 then eye_pain_gpos else 0 end) eye_pain_gpos_13,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2014 then eye_pain_gpos else 0 end) eye_pain_gpos_14,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2015 then eye_pain_gpos else 0 end) eye_pain_gpos_15,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2016 then eye_pain_gpos else 0 end) eye_pain_gpos_16,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2017 then eye_pain_gpos else 0 end) eye_pain_gpos_17,
MAX(CASE WHEN eye_pain_gpos >= 1 and rpt_year = 2018 then eye_pain_gpos else 0 end) eye_pain_gpos_18,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2008 then vaginitis else 0 end) vaginitis_08,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2009 then vaginitis else 0 end) vaginitis_09,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2010 then vaginitis else 0 end) vaginitis_10,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2011 then vaginitis else 0 end) vaginitis_11,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2012 then vaginitis else 0 end) vaginitis_12,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2013 then vaginitis else 0 end) vaginitis_13,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2014 then vaginitis else 0 end) vaginitis_14,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2015 then vaginitis else 0 end) vaginitis_15,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2016 then vaginitis else 0 end) vaginitis_16,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2017 then vaginitis else 0 end) vaginitis_17,
MAX(CASE WHEN vaginitis >= 1 and rpt_year = 2018 then vaginitis else 0 end) vaginitis_18,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2008 then vaginitis_gpos else 0 end) vaginitis_gpos_08,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2009 then vaginitis_gpos else 0 end) vaginitis_gpos_09,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2010 then vaginitis_gpos else 0 end) vaginitis_gpos_10,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2011 then vaginitis_gpos else 0 end) vaginitis_gpos_11,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2012 then vaginitis_gpos else 0 end) vaginitis_gpos_12,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2013 then vaginitis_gpos else 0 end) vaginitis_gpos_13,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2014 then vaginitis_gpos else 0 end) vaginitis_gpos_14,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2015 then vaginitis_gpos else 0 end) vaginitis_gpos_15,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2016 then vaginitis_gpos else 0 end) vaginitis_gpos_16,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2017 then vaginitis_gpos else 0 end) vaginitis_gpos_17,
MAX(CASE WHEN vaginitis_gpos >= 1 and rpt_year = 2018 then vaginitis_gpos else 0 end) vaginitis_gpos_18,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2008 then cervicitis else 0 end) cervicitis_08,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2009 then cervicitis else 0 end) cervicitis_09,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2010 then cervicitis else 0 end) cervicitis_10,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2011 then cervicitis else 0 end) cervicitis_11,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2012 then cervicitis else 0 end) cervicitis_12,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2013 then cervicitis else 0 end) cervicitis_13,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2014 then cervicitis else 0 end) cervicitis_14,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2015 then cervicitis else 0 end) cervicitis_15,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2016 then cervicitis else 0 end) cervicitis_16,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2017 then cervicitis else 0 end) cervicitis_17,
MAX(CASE WHEN cervicitis >= 1 and rpt_year = 2018 then cervicitis else 0 end) cervicitis_18,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2008 then cervicitis_gpos else 0 end) cervicitis_gpos_08,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2009 then cervicitis_gpos else 0 end) cervicitis_gpos_09,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2010 then cervicitis_gpos else 0 end) cervicitis_gpos_10,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2011 then cervicitis_gpos else 0 end) cervicitis_gpos_11,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2012 then cervicitis_gpos else 0 end) cervicitis_gpos_12,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2013 then cervicitis_gpos else 0 end) cervicitis_gpos_13,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2014 then cervicitis_gpos else 0 end) cervicitis_gpos_14,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2015 then cervicitis_gpos else 0 end) cervicitis_gpos_15,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2016 then cervicitis_gpos else 0 end) cervicitis_gpos_16,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2017 then cervicitis_gpos else 0 end) cervicitis_gpos_17,
MAX(CASE WHEN cervicitis_gpos >= 1 and rpt_year = 2018 then cervicitis_gpos else 0 end) cervicitis_gpos_18,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2008 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_08,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2009 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_09,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2010 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_10,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2011 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_11,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2012 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_12,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2013 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_13,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2014 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_14,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2015 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_15,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2016 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_16,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2017 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_17,
MAX(CASE WHEN vaginal_leucorrhea >= 1 and rpt_year = 2018 then vaginal_leucorrhea else 0 end) vaginal_leucorrhea_18,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2008 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_08,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2009 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_09,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2010 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_10,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2011 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_11,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2012 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_12,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2013 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_13,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2014 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_14,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2015 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_15,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2016 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_16,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2017 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_17,
MAX(CASE WHEN vaginal_leucorrhea_gpos >= 1 and rpt_year = 2018 then vaginal_leucorrhea_gpos else 0 end) vaginal_leucorrhea_gpos_18,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2008 then chlamydia else 0 end) chlamydia_dx_08,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2009 then chlamydia else 0 end) chlamydia_dx_09,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2010 then chlamydia else 0 end) chlamydia_dx_10,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2011 then chlamydia else 0 end) chlamydia_dx_11,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2012 then chlamydia else 0 end) chlamydia_dx_12,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2013 then chlamydia else 0 end) chlamydia_dx_13,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2014 then chlamydia else 0 end) chlamydia_dx_14,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2015 then chlamydia else 0 end) chlamydia_dx_15,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2016 then chlamydia else 0 end) chlamydia_dx_16,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2017 then chlamydia else 0 end) chlamydia_dx_17,
MAX(CASE WHEN chlamydia >= 1 and rpt_year = 2018 then chlamydia else 0 end) chlamydia_dx_18,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2008 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_08,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2009 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_09,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2010 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_10,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2011 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_11,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2012 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_12,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2013 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_13,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2014 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_14,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2015 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_15,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2016 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_16,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2017 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_17,
MAX(CASE WHEN chlamydia_gpos >= 1 and rpt_year = 2018 then chlamydia_gpos else 0 end) chlamydia_dx_gpos_18,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2008 then screening_for_stis else 0 end) screening_for_stis_08,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2009 then screening_for_stis else 0 end) screening_for_stis_09,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2010 then screening_for_stis else 0 end) screening_for_stis_10,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2011 then screening_for_stis else 0 end) screening_for_stis_11,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2012 then screening_for_stis else 0 end) screening_for_stis_12,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2013 then screening_for_stis else 0 end) screening_for_stis_13,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2014 then screening_for_stis else 0 end) screening_for_stis_14,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2015 then screening_for_stis else 0 end) screening_for_stis_15,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2016 then screening_for_stis else 0 end) screening_for_stis_16,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2017 then screening_for_stis else 0 end) screening_for_stis_17,
MAX(CASE WHEN screening_for_stis >= 1 and rpt_year = 2018 then screening_for_stis else 0 end) screening_for_stis_18,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2008 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_08,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2009 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_09,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2010 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_10,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2011 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_11,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2012 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_12,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2013 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_13,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2014 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_14,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2015 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_15,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2016 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_16,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2017 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_17,
MAX(CASE WHEN screening_for_stis_gpos >= 1 and rpt_year = 2018 then screening_for_stis_gpos else 0 end) screening_for_stis_gpos_18,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2008 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_08,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2009 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_09,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2010 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_10,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2011 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_11,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2012 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_12,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2013 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_13,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2014 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_14,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2015 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_15,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2016 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_16,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2017 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_17,
MAX(CASE WHEN cont_or_exp_to_vd >= 1 and rpt_year = 2018 then cont_or_exp_to_vd else 0 end) cont_or_exp_to_vd_18,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2008 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_08,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2009 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_09,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2010 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_10,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2011 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_11,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2012 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_12,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2013 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_13,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2014 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_14,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2015 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_15,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2016 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_16,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2017 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_17,
MAX(CASE WHEN cont_or_exp_to_vd_gpos >= 1 and rpt_year = 2018 then cont_or_exp_to_vd_gpos else 0 end) cont_or_exp_to_vd_gpos_18,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2008 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_08,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2009 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_09,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2010 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_10,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2011 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_11,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2012 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_12,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2013 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_13,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2014 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_14,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2015 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_15,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2016 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_16,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2017 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_17,
MAX(CASE WHEN high_risk_sexual_behavior >= 1 and rpt_year = 2018 then high_risk_sexual_behavior else 0 end) high_risk_sexual_behavior_18,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2008 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_08,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2009 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_09,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2010 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_10,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2011 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_11,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2012 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_12,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2013 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_13,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2014 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_14,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2015 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_15,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2016 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_16,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2017 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_17,
MAX(CASE WHEN high_risk_sexual_behavior_gpos >= 1 and rpt_year = 2018 then high_risk_sexual_behavior_gpos else 0 end) high_risk_sexual_behavior_gpos_18,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2008 then hiv_counseling else 0 end) hiv_counseling_08,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2009 then hiv_counseling else 0 end) hiv_counseling_09,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2010 then hiv_counseling else 0 end) hiv_counseling_10,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2011 then hiv_counseling else 0 end) hiv_counseling_11,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2012 then hiv_counseling else 0 end) hiv_counseling_12,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2013 then hiv_counseling else 0 end) hiv_counseling_13,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2014 then hiv_counseling else 0 end) hiv_counseling_14,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2015 then hiv_counseling else 0 end) hiv_counseling_15,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2016 then hiv_counseling else 0 end) hiv_counseling_16,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2017 then hiv_counseling else 0 end) hiv_counseling_17,
MAX(CASE WHEN hiv_counseling >= 1 and rpt_year = 2018 then hiv_counseling else 0 end) hiv_counseling_18,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2008 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_08,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2009 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_09,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2010 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_10,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2011 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_11,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2012 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_12,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2013 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_13,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2014 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_14,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2015 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_15,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2016 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_16,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2017 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_17,
MAX(CASE WHEN hiv_counseling_gpos >= 1 and rpt_year = 2018 then hiv_counseling_gpos else 0 end) hiv_counseling_gpos_18
FROM mgtp_diag_fields_by_year_sub
GROUP BY patient_id;

-- HIV - Gather Up Lab Tests 
-- ONLY COUNTING ONE LAB TEST TYPE PER DAY
CREATE TABLE mgtp_hiv_labs AS 
SELECT T1.patient_id, T2.test_name, date, EXTRACT(YEAR FROM date) rpt_year 
FROM emr_labresult T1, 
conf_labtestmap T2
WHERE T1.native_code = T2.native_code
AND test_name ilike 'hiv_%' 
AND date >= '01-01-2010'
AND date < '01-01-2019'
GROUP BY patient_id, test_name, date;

-- HIV - Lab Counts & Column Creation
CREATE TABLE mgtp_hiv_counts  AS 
SELECT T1.patient_id, count(*) total_hiv_tests 
FROM  mgtp_hiv_labs T1   
GROUP BY T1.patient_id;

-- HIV - Cases
CREATE TABLE mgtp_hiv_cases  AS 
SELECT T1.patient_id, 1::int hiv_per_esp, min(EXTRACT(YEAR FROM date)) hiv_per_esp_date, T1.date
FROM  nodis_case T1
WHERE condition = 'hiv' 
AND date < '01-01-2019'
GROUP BY T1.patient_id,T1.date;

-- HIV - Medications
CREATE TABLE mgtp_hiv_meds AS 
SELECT T1.patient_id,
T1.name,
split_part(T1.name, ':', 2) stripped_name,
T1.date,
case when T1.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end truvada_rx,
case when T1.name not in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine') then 1 else 0 end other_hiv_rx_non_truvada,
CASE WHEN refills~E'^\\d+$' THEN (refills::real +1)  ELSE 1 END  refills_mod,
1::int hiv_meds,
string_to_array(replace(T1.name, 'rx:hiv_',''), '-') test4,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-')::text test5,
string_to_array(replace(split_part(T1.name, ':', 2), 'hiv_',''), '-') med_array,
quantity,
quantity_float,
quantity_type,
EXTRACT(YEAR FROM T1.date)  rpt_year
FROM hef_event T1,
emr_prescription T2
WHERE T1.object_id = T2.id
AND T1.name ilike 'rx:hiv_%'; 

-- HIV - Truvada Array - For those with more than 2 rx (includes refills)
CREATE TABLE mgtp_truvada_array  AS 
SELECT T1.patient_id, T1.rpt_year, sum(T1.refills_mod)  total_truvada_rx,
array_agg(T1.date)  truvada_array 
FROM  mgtp_hiv_meds T1 
WHERE truvada_rx = 1  
GROUP BY T1.patient_id,T1.rpt_year 
HAVING sum(T1.refills_mod) >=2;

-- MLCHC Cannot Order Arrays -- special table here to unnest and do the sort and calculations
CREATE TABLE mgtp_truvada_2mogap_prep AS
SELECT
T2.patient_id,
T1.rpt_year,
max(T1.val) final_rx,
min(T1.val) first_rx,
max(T1.val) - min(T1.val) two_month_check
FROM 
(SELECT
    patient_id,rpt_year,
    UNNEST(truvada_array) AS val
	FROM mgtp_truvada_array
	GROUP BY rpt_year, patient_id, truvada_array
	order by val) T1,
mgtp_truvada_array T2
where T1.patient_id = T2.patient_id
GROUP BY T2.patient_id, T1.rpt_year;

-- HIV Truvada - Find those that have prescriptions 2 or more months apart in the same year
CREATE TABLE mgtp_truvada_2mogap  AS 
SELECT T1.patient_id, 
first_rx,
final_rx,
rpt_year,
two_month_check,
1 truvada_criteria_met 
FROM  mgtp_truvada_2mogap_prep T1   
WHERE two_month_check >= 60;

-- CREATE TABLE mgtp_truvada_2mogap  AS 
-- SELECT T1.patient_id, 
-- T1.truvada_array[1] first_rx,
-- truvada_array[array_length(truvada_array, 1)]   final_rx,
-- T1.rpt_year,
-- truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] two_month_check,
-- 1 truvada_criteria_met 
-- FROM  mgtp_truvada_array T1   
-- WHERE truvada_array[array_length(truvada_array,1)] - T1.truvada_array[1] >= 60;

-- HIV - Truvada Counts & Column Creation 
CREATE TABLE mgtp_truvada_counts  AS 
SELECT T1.patient_id,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2010 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_10,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2011 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_11,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2012 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_12,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2013 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_13,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2014 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_14,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2015 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_15,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2016 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null  then 1 else 0 end) hiv_neg_truvada_16,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2017 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_17,
max(case when truvada_criteria_met = 1 and T1.rpt_year = 2018 and (hiv_per_esp is null or (hiv_per_esp =1 and T1.rpt_year < hiv_per_esp_date)) AND pos_hep_b_dna is null then 1 else 0 end) hiv_neg_truvada_18   
FROM mgtp_truvada_2mogap T1 
LEFT OUTER JOIN mgtp_hiv_cases T2 ON ((T1.patient_id = T2.patient_id))  
LEFT JOIN mgtp_hepb_pos_dna T3 ON ((T1.patient_id = T3.patient_id))
GROUP BY T1.patient_id;

-- HIV - Full Outer Join HIV Lab Events, Meds, and Cases - Derive Compound Values
CREATE TABLE mgtp_hiv_all_details  AS SELECT 
coalesce(lab.patient_id, cases.patient_id, truvada.patient_id) patient_id,
(case when total_hiv_tests is null and hiv_per_esp is null then 2
        when total_hiv_tests > 0 and hiv_per_esp is null then 0
        else hiv_per_esp end) as hiv_per_esp_final, 
coalesce(hiv_per_esp_date, null) as hiv_per_esp_date,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_neg_truvada_18,0) as hiv_neg_truvada_18
FROM mgtp_hiv_counts lab
FULL OUTER JOIN mgtp_hiv_cases cases
    ON lab.patient_id=cases.patient_id
FULL OUTER JOIN mgtp_truvada_counts truvada
    ON truvada.patient_id = coalesce(lab.patient_id, cases.patient_id);
	
-- -- FILTER OUT PATIENTS THAT HAVE HIV LAB TESTS BUT NOTHING ELSE
-- -- AS THAT EXEMPTS THEM FROM THE REPORT
-- CREATE TABLE mgtp_hiv_all_details_values  AS
-- SELECT *
-- FROM mgtp_hiv_all_details 
-- WHERE  hiv_per_esp_final != 0
-- OR hiv_per_esp_date is NOT null
-- OR hiv_neg_truvada_10 != 0
-- OR hiv_neg_truvada_11 != 0
-- OR hiv_neg_truvada_12 != 0
-- OR hiv_neg_truvada_13 != 0
-- OR hiv_neg_truvada_14 != 0
-- OR hiv_neg_truvada_15 != 0
-- OR hiv_neg_truvada_16 != 0
-- OR hiv_neg_truvada_17 != 0;


-- Count up unique specimen sites tested on the same date per year
CREATE TABLE mgtp_spec_source_count as
SELECT patient_id, count(distinct(specimen_source)) spec_source_count, date, rpt_year
FROM mgtp_gon_events
GROUP BY patient_id, rpt_year, date;


-- What is the max number of sites tested on a single date per year
CREATE TABLE mgtp_spec_source_max as
SELECT T1.patient_id, max(date) max_site_date, spec_source_count_max, T1.rpt_year
FROM mgtp_spec_source_count T1,
	(SELECT patient_id, max(spec_source_count) spec_source_count_max, rpt_year FROM mgtp_spec_source_count GROUP BY patient_id, rpt_year) T2
WHERE T1.spec_source_count = T2.spec_source_count_max
AND T1.patient_id = T2.patient_id
AND T1.rpt_year = T2.rpt_year
GROUP BY T1.patient_id, spec_source_count_max, T1.rpt_year;

-- now just create an array of the specimen sources for that date. Where any positive?
CREATE TABLE mgtp_spec_source_multisite as
select T1.patient_id, spec_source_count_max, T1.rpt_year, array_agg(distinct(specimen_source)) spec_source_array,
max(case when name = 'lx:gonorrhea:positive' then 1 else 0 end) multisite_positive
from mgtp_spec_source_max T1,
mgtp_gon_events T2
WHERE T1.patient_id = T2.patient_id
AND T1.max_site_date = T2.date
AND T1.rpt_year = T2.rpt_year
GROUP BY T1.patient_id, T1.spec_source_count_max, T1.rpt_year;

-- Same day site testing values
CREATE TABLE mgtp_multisite_data  AS 
SELECT T1.patient_id,
max(CASE when T1.rpt_year = 2008 then spec_source_count_max end) as max_num_same_day_sites_08,
max(CASE WHEN T1.rpt_year = 2009 then spec_source_count_max end) as max_num_same_day_sites_09,
max(CASE WHEN T1.rpt_year = 2010 then spec_source_count_max end) as max_num_same_day_sites_10,
max(CASE WHEN T1.rpt_year = 2011 then spec_source_count_max end) as max_num_same_day_sites_11,
max(CASE WHEN T1.rpt_year = 2012 then spec_source_count_max end) as max_num_same_day_sites_12,
max(CASE WHEN T1.rpt_year = 2013 then spec_source_count_max end) as max_num_same_day_sites_13,
max(CASE WHEN T1.rpt_year = 2014 then spec_source_count_max end) as max_num_same_day_sites_14,
max(CASE WHEN T1.rpt_year = 2015 then spec_source_count_max end) as max_num_same_day_sites_15,
max(CASE WHEN T1.rpt_year = 2016 then spec_source_count_max end) as max_num_same_day_sites_16,
max(CASE WHEN T1.rpt_year = 2017 then spec_source_count_max end) as max_num_same_day_sites_17,
max(CASE WHEN T1.rpt_year = 2018 then spec_source_count_max end) as max_num_same_day_sites_18,
max(CASE when T1.rpt_year = 2008 then spec_source_array end) as max_same_day_sites_tested_08,
max(CASE when T1.rpt_year = 2009 then spec_source_array end) as max_same_day_sites_tested_09,
max(CASE when T1.rpt_year = 2010 then spec_source_array end) as max_same_day_sites_tested_10,
max(CASE when T1.rpt_year = 2011 then spec_source_array end) as max_same_day_sites_tested_11,
max(CASE when T1.rpt_year = 2012 then spec_source_array end) as max_same_day_sites_tested_12,
max(CASE when T1.rpt_year = 2013 then spec_source_array end) as max_same_day_sites_tested_13,
max(CASE when T1.rpt_year = 2014 then spec_source_array end) as max_same_day_sites_tested_14,
max(CASE when T1.rpt_year = 2015 then spec_source_array end) as max_same_day_sites_tested_15,
max(CASE when T1.rpt_year = 2016 then spec_source_array end) as max_same_day_sites_tested_16,
max(CASE when T1.rpt_year = 2017 then spec_source_array end) as max_same_day_sites_tested_17,
max(CASE when T1.rpt_year = 2018 then spec_source_array end) as max_same_day_sites_tested_18,
max(CASE when T1.rpt_year = 2008 then multisite_positive end) as max_same_day_sites_positive_yn_08,
max(CASE when T1.rpt_year = 2009 then multisite_positive end) as max_same_day_sites_positive_yn_09,
max(CASE when T1.rpt_year = 2010 then multisite_positive end) as max_same_day_sites_positive_yn_10,
max(CASE when T1.rpt_year = 2011 then multisite_positive end) as max_same_day_sites_positive_yn_11,
max(CASE when T1.rpt_year = 2012 then multisite_positive end) as max_same_day_sites_positive_yn_12,
max(CASE when T1.rpt_year = 2013 then multisite_positive end) as max_same_day_sites_positive_yn_13,
max(CASE when T1.rpt_year = 2014 then multisite_positive end) as max_same_day_sites_positive_yn_14,
max(CASE when T1.rpt_year = 2015 then multisite_positive end) as max_same_day_sites_positive_yn_15,
max(CASE when T1.rpt_year = 2016 then multisite_positive end) as max_same_day_sites_positive_yn_16,
max(CASE when T1.rpt_year = 2017 then multisite_positive end) as max_same_day_sites_positive_yn_17,
max(CASE when T1.rpt_year = 2018 then multisite_positive end) as max_same_day_sites_positive_yn_18
FROM mgtp_spec_source_multisite T1
GROUP BY patient_id;

	

-- BASE -- Create Index Patients Table	
CREATE TABLE mgtp_index_patients AS
SELECT patient_id FROM mgtp_gon_counts
UNION
SELECT patient_id FROM mgtp_gon_cases
UNION
SELECT patient_id FROM mgtp_chlam_events
UNION
SELECT patient_id FROM mgtp_chlam_cases
UNION
SELECT patient_id FROM mgtp_syph_counts
UNION
SELECT patient_id FROM mgtp_syph_cases
UNION
SELECT patient_id FROM mgtp_cpts_of_interest
UNION
--SELECT patient_id FROM mgtp_hiv_all_details_values
--UNION
SELECT patient_id FROM mgtp_diag_fields
UNION 
SELECT patient_id FROM mgtp_spec_source_multisite;




-- FULL OUTER JOIN ALL OF THE DATA TOGETHER
CREATE TABLE mgtp_output_part_1  AS SELECT 
--coalesce(d.patient_id, g.patient_id, c.patient_id, s.patient_id, hc.patient_id, hcc.patient_id, hb.patient_id, hbc.patient_id, rx.patient_id, cpt.patient_id) as master_patient_id,
T1.patient_id,
coalesce(t_gon_tests_08, 0) as total_gonorrhea_tests_08,
coalesce(t_gon_tests_09, 0) as total_gonorrhea_tests_09,
coalesce(t_gon_tests_10, 0) as total_gonorrhea_tests_10,
coalesce(t_gon_tests_11, 0) as total_gonorrhea_tests_11,
coalesce(t_gon_tests_12, 0) as total_gonorrhea_tests_12,
coalesce(t_gon_tests_13, 0) as total_gonorrhea_tests_13,
coalesce(t_gon_tests_14, 0) as total_gonorrhea_tests_14,
coalesce(t_gon_tests_15, 0) as total_gonorrhea_tests_15,
coalesce(t_gon_tests_16, 0) as total_gonorrhea_tests_16,
coalesce(t_gon_tests_17, 0) as total_gonorrhea_tests_17,
coalesce(t_gon_tests_18, 0) as total_gonorrhea_tests_18,
coalesce(p_gon_tests_08, 0) as positive_gonorrhea_tests_08,
coalesce(p_gon_tests_09, 0) as positive_gonorrhea_tests_09,
coalesce(p_gon_tests_10, 0) as positive_gonorrhea_tests_10,
coalesce(p_gon_tests_11, 0) as positive_gonorrhea_tests_11,
coalesce(p_gon_tests_12, 0) as positive_gonorrhea_tests_12,
coalesce(p_gon_tests_13, 0) as positive_gonorrhea_tests_13,
coalesce(p_gon_tests_14, 0) as positive_gonorrhea_tests_14,
coalesce(p_gon_tests_15, 0) as positive_gonorrhea_tests_15,
coalesce(p_gon_tests_16, 0) as positive_gonorrhea_tests_16,
coalesce(p_gon_tests_17, 0) as positive_gonorrhea_tests_17,
coalesce(p_gon_tests_18, 0) as positive_gonorrhea_tests_18,
coalesce(gon_08, 0) as gonorrhea_per_esp_08,
coalesce(gon_09, 0) as gonorrhea_per_esp_09,
coalesce(gon_10, 0) as gonorrhea_per_esp_10,
coalesce(gon_11, 0) as gonorrhea_per_esp_11,
coalesce(gon_12, 0) as gonorrhea_per_esp_12,
coalesce(gon_13, 0) as gonorrhea_per_esp_13,
coalesce(gon_14, 0) as gonorrhea_per_esp_14,
coalesce(gon_15, 0) as gonorrhea_per_esp_15,
coalesce(gon_16, 0) as gonorrhea_per_esp_16,
coalesce(gon_17, 0) as gonorrhea_per_esp_17,
coalesce(gon_18, 0) as gonorrhea_per_esp_18,
coalesce(t_gon_tests_urog_08, 0) as total_gonorrhea_tests_urog_08,
coalesce(t_gon_tests_urog_09, 0) as total_gonorrhea_tests_urog_09,
coalesce(t_gon_tests_urog_10, 0) as total_gonorrhea_tests_urog_10,
coalesce(t_gon_tests_urog_11, 0) as total_gonorrhea_tests_urog_11,
coalesce(t_gon_tests_urog_12, 0) as total_gonorrhea_tests_urog_12,
coalesce(t_gon_tests_urog_13, 0) as total_gonorrhea_tests_urog_13,
coalesce(t_gon_tests_urog_14, 0) as total_gonorrhea_tests_urog_14,
coalesce(t_gon_tests_urog_15, 0) as total_gonorrhea_tests_urog_15,
coalesce(t_gon_tests_urog_16, 0) as total_gonorrhea_tests_urog_16,
coalesce(t_gon_tests_urog_17, 0) as total_gonorrhea_tests_urog_17,
coalesce(t_gon_tests_urog_18, 0) as total_gonorrhea_tests_urog_18,
coalesce(p_gon_tests_urog_08, 0) as positive_gonorrhea_tests_urog_08,
coalesce(p_gon_tests_urog_09, 0) as positive_gonorrhea_tests_urog_09,
coalesce(p_gon_tests_urog_10, 0) as positive_gonorrhea_tests_urog_10,
coalesce(p_gon_tests_urog_11, 0) as positive_gonorrhea_tests_urog_11,
coalesce(p_gon_tests_urog_12, 0) as positive_gonorrhea_tests_urog_12,
coalesce(p_gon_tests_urog_13, 0) as positive_gonorrhea_tests_urog_13,
coalesce(p_gon_tests_urog_14, 0) as positive_gonorrhea_tests_urog_14,
coalesce(p_gon_tests_urog_15, 0) as positive_gonorrhea_tests_urog_15,
coalesce(p_gon_tests_urog_16, 0) as positive_gonorrhea_tests_urog_16,
coalesce(p_gon_tests_urog_17, 0) as positive_gonorrhea_tests_urog_17,
coalesce(p_gon_tests_urog_18, 0) as positive_gonorrhea_tests_urog_18,
coalesce(t_gon_tests_rectal_08, 0) as total_gonorrhea_tests_rectal_08,
coalesce(t_gon_tests_rectal_09, 0) as total_gonorrhea_tests_rectal_09,
coalesce(t_gon_tests_rectal_10, 0) as total_gonorrhea_tests_rectal_10,
coalesce(t_gon_tests_rectal_11, 0) as total_gonorrhea_tests_rectal_11,
coalesce(t_gon_tests_rectal_12, 0) as total_gonorrhea_tests_rectal_12,
coalesce(t_gon_tests_rectal_13, 0) as total_gonorrhea_tests_rectal_13,
coalesce(t_gon_tests_rectal_14, 0) as total_gonorrhea_tests_rectal_14,
coalesce(t_gon_tests_rectal_15, 0) as total_gonorrhea_tests_rectal_15,
coalesce(t_gon_tests_rectal_16, 0) as total_gonorrhea_tests_rectal_16,
coalesce(t_gon_tests_rectal_17, 0) as total_gonorrhea_tests_rectal_17,
coalesce(t_gon_tests_rectal_18, 0) as total_gonorrhea_tests_rectal_18,
coalesce(p_gon_tests_rectal_08, 0) as positive_gonorrhea_tests_rectal_08,
coalesce(p_gon_tests_rectal_09, 0) as positive_gonorrhea_tests_rectal_09,
coalesce(p_gon_tests_rectal_10, 0) as positive_gonorrhea_tests_rectal_10,
coalesce(p_gon_tests_rectal_11, 0) as positive_gonorrhea_tests_rectal_11,
coalesce(p_gon_tests_rectal_12, 0) as positive_gonorrhea_tests_rectal_12,
coalesce(p_gon_tests_rectal_13, 0) as positive_gonorrhea_tests_rectal_13,
coalesce(p_gon_tests_rectal_14, 0) as positive_gonorrhea_tests_rectal_14,
coalesce(p_gon_tests_rectal_15, 0) as positive_gonorrhea_tests_rectal_15,
coalesce(p_gon_tests_rectal_16, 0) as positive_gonorrhea_tests_rectal_16,
coalesce(p_gon_tests_rectal_17, 0) as positive_gonorrhea_tests_rectal_17,
coalesce(p_gon_tests_rectal_18, 0) as positive_gonorrhea_tests_rectal_18,
coalesce(t_gon_tests_throat_08, 0) as total_gonorrhea_tests_throat_08,
coalesce(t_gon_tests_throat_09, 0) as total_gonorrhea_tests_throat_09,
coalesce(t_gon_tests_throat_10, 0) as total_gonorrhea_tests_throat_10,
coalesce(t_gon_tests_throat_11, 0) as total_gonorrhea_tests_throat_11,
coalesce(t_gon_tests_throat_12, 0) as total_gonorrhea_tests_throat_12,
coalesce(t_gon_tests_throat_13, 0) as total_gonorrhea_tests_throat_13,
coalesce(t_gon_tests_throat_14, 0) as total_gonorrhea_tests_throat_14,
coalesce(t_gon_tests_throat_15, 0) as total_gonorrhea_tests_throat_15,
coalesce(t_gon_tests_throat_16, 0) as total_gonorrhea_tests_throat_16,
coalesce(t_gon_tests_throat_17, 0) as total_gonorrhea_tests_throat_17,
coalesce(t_gon_tests_throat_18, 0) as total_gonorrhea_tests_throat_18,
coalesce(p_gon_tests_throat_08, 0) as positive_gonorrhea_tests_throat_08,
coalesce(p_gon_tests_throat_09, 0) as positive_gonorrhea_tests_throat_09,
coalesce(p_gon_tests_throat_10, 0) as positive_gonorrhea_tests_throat_10,
coalesce(p_gon_tests_throat_11, 0) as positive_gonorrhea_tests_throat_11,
coalesce(p_gon_tests_throat_12, 0) as positive_gonorrhea_tests_throat_12,
coalesce(p_gon_tests_throat_13, 0) as positive_gonorrhea_tests_throat_13,
coalesce(p_gon_tests_throat_14, 0) as positive_gonorrhea_tests_throat_14,
coalesce(p_gon_tests_throat_15, 0) as positive_gonorrhea_tests_throat_15,
coalesce(p_gon_tests_throat_16, 0) as positive_gonorrhea_tests_throat_16,
coalesce(p_gon_tests_throat_17, 0) as positive_gonorrhea_tests_throat_17,
coalesce(p_gon_tests_throat_18, 0) as positive_gonorrhea_tests_throat_18,
coalesce(t_gon_tests_other_08, 0) as total_gonorrhea_tests_other_08,
coalesce(t_gon_tests_other_09, 0) as total_gonorrhea_tests_other_09,
coalesce(t_gon_tests_other_10, 0) as total_gonorrhea_tests_other_10,
coalesce(t_gon_tests_other_11, 0) as total_gonorrhea_tests_other_11,
coalesce(t_gon_tests_other_12, 0) as total_gonorrhea_tests_other_12,
coalesce(t_gon_tests_other_13, 0) as total_gonorrhea_tests_other_13,
coalesce(t_gon_tests_other_14, 0) as total_gonorrhea_tests_other_14,
coalesce(t_gon_tests_other_15, 0) as total_gonorrhea_tests_other_15,
coalesce(t_gon_tests_other_16, 0) as total_gonorrhea_tests_other_16,
coalesce(t_gon_tests_other_17, 0) as total_gonorrhea_tests_other_17,
coalesce(t_gon_tests_other_18, 0) as total_gonorrhea_tests_other_18,
coalesce(p_gon_tests_other_08, 0) as positive_gonorrhea_tests_other_08,
coalesce(p_gon_tests_other_09, 0) as positive_gonorrhea_tests_other_09,
coalesce(p_gon_tests_other_10, 0) as positive_gonorrhea_tests_other_10,
coalesce(p_gon_tests_other_11, 0) as positive_gonorrhea_tests_other_11,
coalesce(p_gon_tests_other_12, 0) as positive_gonorrhea_tests_other_12,
coalesce(p_gon_tests_other_13, 0) as positive_gonorrhea_tests_other_13,
coalesce(p_gon_tests_other_14, 0) as positive_gonorrhea_tests_other_14,
coalesce(p_gon_tests_other_15, 0) as positive_gonorrhea_tests_other_15,
coalesce(p_gon_tests_other_16, 0) as positive_gonorrhea_tests_other_16,
coalesce(p_gon_tests_other_17, 0) as positive_gonorrhea_tests_other_17,
coalesce(p_gon_tests_other_18, 0) as positive_gonorrhea_tests_other_18,
coalesce(t_gon_tests_missing_08, 0) as total_gonorrhea_tests_missing_08,
coalesce(t_gon_tests_missing_09, 0) as total_gonorrhea_tests_missing_09,
coalesce(t_gon_tests_missing_10, 0) as total_gonorrhea_tests_missing_10,
coalesce(t_gon_tests_missing_11, 0) as total_gonorrhea_tests_missing_11,
coalesce(t_gon_tests_missing_12, 0) as total_gonorrhea_tests_missing_12,
coalesce(t_gon_tests_missing_13, 0) as total_gonorrhea_tests_missing_13,
coalesce(t_gon_tests_missing_14, 0) as total_gonorrhea_tests_missing_14,
coalesce(t_gon_tests_missing_15, 0) as total_gonorrhea_tests_missing_15,
coalesce(t_gon_tests_missing_16, 0) as total_gonorrhea_tests_missing_16,
coalesce(t_gon_tests_missing_17, 0) as total_gonorrhea_tests_missing_17,
coalesce(t_gon_tests_missing_18, 0) as total_gonorrhea_tests_missing_18,
coalesce(p_gon_tests_missing_08, 0) as positive_gonorrhea_tests_missing_08,
coalesce(p_gon_tests_missing_09, 0) as positive_gonorrhea_tests_missing_09,
coalesce(p_gon_tests_missing_10, 0) as positive_gonorrhea_tests_missing_10,
coalesce(p_gon_tests_missing_11, 0) as positive_gonorrhea_tests_missing_11,
coalesce(p_gon_tests_missing_12, 0) as positive_gonorrhea_tests_missing_12,
coalesce(p_gon_tests_missing_13, 0) as positive_gonorrhea_tests_missing_13,
coalesce(p_gon_tests_missing_14, 0) as positive_gonorrhea_tests_missing_14,
coalesce(p_gon_tests_missing_15, 0) as positive_gonorrhea_tests_missing_15,
coalesce(p_gon_tests_missing_16, 0) as positive_gonorrhea_tests_missing_16,
coalesce(p_gon_tests_missing_17, 0) as positive_gonorrhea_tests_missing_17,
coalesce(p_gon_tests_missing_18, 0) as positive_gonorrhea_tests_missing_18,
coalesce(max_num_same_day_sites_08, 0) as max_num_same_day_sites_08,
coalesce(max_same_day_sites_tested_08, null) as max_same_day_sites_tested_08,
coalesce(max_same_day_sites_positive_yn_08, 0) as max_same_day_sites_positive_yn_08,
coalesce(max_num_same_day_sites_09, 0) as max_num_same_day_sites_09,
coalesce(max_same_day_sites_tested_09, null) as max_same_day_sites_tested_09,
coalesce(max_same_day_sites_positive_yn_09, 0) as max_same_day_sites_positive_yn_09,
coalesce(max_num_same_day_sites_10, 0) as max_num_same_day_sites_10,
coalesce(max_same_day_sites_tested_10, null) as max_same_day_sites_tested_10,
coalesce(max_same_day_sites_positive_yn_10, 0) as max_same_day_sites_positive_yn_10,
coalesce(max_num_same_day_sites_11, 0) as max_num_same_day_sites_11,
coalesce(max_same_day_sites_tested_11, null) as max_same_day_sites_tested_11,
coalesce(max_same_day_sites_positive_yn_11, 0) as max_same_day_sites_positive_yn_11,
coalesce(max_num_same_day_sites_12, 0) as max_num_same_day_sites_12,
coalesce(max_same_day_sites_tested_12, null) as max_same_day_sites_tested_12,
coalesce(max_same_day_sites_positive_yn_12, 0) as max_same_day_sites_positive_yn_12,
coalesce(max_num_same_day_sites_13, 0) as max_num_same_day_sites_13,
coalesce(max_same_day_sites_tested_13, null) as max_same_day_sites_tested_13,
coalesce(max_same_day_sites_positive_yn_13, 0) as max_same_day_sites_positive_yn_13,
coalesce(max_num_same_day_sites_14, 0) as max_num_same_day_sites_14,
coalesce(max_same_day_sites_tested_14, null) as max_same_day_sites_tested_14,
coalesce(max_same_day_sites_positive_yn_14, 0) as max_same_day_sites_positive_yn_14,
coalesce(max_num_same_day_sites_15, 0) as max_num_same_day_sites_15,
coalesce(max_same_day_sites_tested_15, null) as max_same_day_sites_tested_15,
coalesce(max_same_day_sites_positive_yn_15, 0) as max_same_day_sites_positive_yn_15,
coalesce(max_num_same_day_sites_16, 0) as max_num_same_day_sites_16,
coalesce(max_same_day_sites_tested_16, null) as max_same_day_sites_tested_16,
coalesce(max_same_day_sites_positive_yn_16, 0) as max_same_day_sites_positive_yn_16,
coalesce(max_num_same_day_sites_17, 0) as max_num_same_day_sites_17,
coalesce(max_same_day_sites_tested_17, null) as max_same_day_sites_tested_17,
coalesce(max_same_day_sites_positive_yn_17, 0) as max_same_day_sites_positive_yn_17,
coalesce(max_num_same_day_sites_18, 0) as max_num_same_day_sites_18,
coalesce(max_same_day_sites_tested_18, null) as max_same_day_sites_tested_18,
coalesce(max_same_day_sites_positive_yn_18, 0) as max_same_day_sites_positive_yn_18,
coalesce(t_chlam_08, 0) as total_chlamydia_tests_08,
coalesce(t_chlam_09, 0) as total_chlamydia_tests_09,
coalesce(t_chlam_10, 0) as total_chlamydia_tests_10,
coalesce(t_chlam_11, 0) as total_chlamydia_tests_11,
coalesce(t_chlam_12, 0) as total_chlamydia_tests_12,
coalesce(t_chlam_13, 0) as total_chlamydia_tests_13,
coalesce(t_chlam_14, 0) as total_chlamydia_tests_14,
coalesce(t_chlam_15, 0) as total_chlamydia_tests_15,
coalesce(t_chlam_16, 0) as total_chlamydia_tests_16,
coalesce(t_chlam_17, 0) as total_chlamydia_tests_17,
coalesce(t_chlam_18, 0) as total_chlamydia_tests_18,
coalesce(p_chlam_08, 0) as positive_chlamydia_tests_08,
coalesce(p_chlam_09, 0) as positive_chlamydia_tests_09,
coalesce(p_chlam_10, 0) as positive_chlamydia_tests_10,
coalesce(p_chlam_11, 0) as positive_chlamydia_tests_11,
coalesce(p_chlam_12, 0) as positive_chlamydia_tests_12,
coalesce(p_chlam_13, 0) as positive_chlamydia_tests_13,
coalesce(p_chlam_14, 0) as positive_chlamydia_tests_14,
coalesce(p_chlam_15, 0) as positive_chlamydia_tests_15,
coalesce(p_chlam_16, 0) as positive_chlamydia_tests_16,
coalesce(p_chlam_17, 0) as positive_chlamydia_tests_17,
coalesce(p_chlam_18, 0) as positive_chlamydia_tests_18,
coalesce(chlam_08, 0) as chlamydia_per_esp_08,
coalesce(chlam_09, 0) as chlamydia_per_esp_09,
coalesce(chlam_10, 0) as chlamydia_per_esp_10,
coalesce(chlam_11, 0) as chlamydia_per_esp_11,
coalesce(chlam_12, 0) as chlamydia_per_esp_12,
coalesce(chlam_13, 0) as chlamydia_per_esp_13,
coalesce(chlam_14, 0) as chlamydia_per_esp_14,
coalesce(chlam_15, 0) as chlamydia_per_esp_15,
coalesce(chlam_16, 0) as chlamydia_per_esp_16,
coalesce(chlam_17, 0) as chlamydia_per_esp_17,
coalesce(chlam_18, 0) as chlamydia_per_esp_18,
coalesce(t_syph_08, 0) as total_syphilis_tests_08,
coalesce(t_syph_09, 0) as total_syphilis_tests_09,
coalesce(t_syph_10, 0) as total_syphilis_tests_10,
coalesce(t_syph_11, 0) as total_syphilis_tests_11,
coalesce(t_syph_12, 0) as total_syphilis_tests_12,
coalesce(t_syph_13, 0) as total_syphilis_tests_13,
coalesce(t_syph_14, 0) as total_syphilis_tests_14,
coalesce(t_syph_15, 0) as total_syphilis_tests_15,
coalesce(t_syph_16, 0) as total_syphilis_tests_16,
coalesce(t_syph_17, 0) as total_syphilis_tests_17,
coalesce(t_syph_18, 0) as total_syphilis_tests_18,
coalesce(syphilis_08, 0) as syphilis_per_esp_08,
coalesce(syphilis_09, 0) as syphilis_per_esp_09,
coalesce(syphilis_10, 0) as syphilis_per_esp_10,
coalesce(syphilis_11, 0) as syphilis_per_esp_11,
coalesce(syphilis_12, 0) as syphilis_per_esp_12,
coalesce(syphilis_13, 0) as syphilis_per_esp_13,
coalesce(syphilis_14, 0) as syphilis_per_esp_14,
coalesce(syphilis_15, 0) as syphilis_per_esp_15,
coalesce(syphilis_16, 0) as syphilis_per_esp_16,
coalesce(syphilis_17, 0) as syphilis_per_esp_17,
coalesce(syphilis_18, 0) as syphilis_per_esp_18,
coalesce(anal_cytology_test_ever, 0) as anal_cytology_test_ever,
coalesce(hiv_per_esp_final, 2) as hiv_per_esp_spec,
coalesce(hiv_neg_truvada_10,0) as hiv_neg_truvada_10,
coalesce(hiv_neg_truvada_11,0) as hiv_neg_truvada_11,
coalesce(hiv_neg_truvada_12,0) as hiv_neg_truvada_12,
coalesce(hiv_neg_truvada_13,0) as hiv_neg_truvada_13,
coalesce(hiv_neg_truvada_14,0) as hiv_neg_truvada_14,
coalesce(hiv_neg_truvada_15,0) as hiv_neg_truvada_15,
coalesce(hiv_neg_truvada_16,0) as hiv_neg_truvada_16,
coalesce(hiv_neg_truvada_17,0) as hiv_neg_truvada_17,
coalesce(hiv_neg_truvada_18,0) as hiv_neg_truvada_18,
coalesce(abnormal_anal_cytology_08, 0) as abnormal_anal_cytology_08,
coalesce(abnormal_anal_cytology_09, 0) as abnormal_anal_cytology_09,
coalesce(abnormal_anal_cytology_10, 0) as abnormal_anal_cytology_10,
coalesce(abnormal_anal_cytology_11, 0) as abnormal_anal_cytology_11,
coalesce(abnormal_anal_cytology_12, 0) as abnormal_anal_cytology_12,
coalesce(abnormal_anal_cytology_13, 0) as abnormal_anal_cytology_13,
coalesce(abnormal_anal_cytology_14, 0) as abnormal_anal_cytology_14,
coalesce(abnormal_anal_cytology_15, 0) as abnormal_anal_cytology_15,
coalesce(abnormal_anal_cytology_16, 0) as abnormal_anal_cytology_16,
coalesce(abnormal_anal_cytology_17, 0) as abnormal_anal_cytology_17,
coalesce(abnormal_anal_cytology_18, 0) as abnormal_anal_cytology_18,
coalesce(abnormal_anal_cytology_gpos_08, 0) as abnormal_anal_cytology_gpos_08,
coalesce(abnormal_anal_cytology_gpos_09, 0) as abnormal_anal_cytology_gpos_09,
coalesce(abnormal_anal_cytology_gpos_10, 0) as abnormal_anal_cytology_gpos_10,
coalesce(abnormal_anal_cytology_gpos_11, 0) as abnormal_anal_cytology_gpos_11,
coalesce(abnormal_anal_cytology_gpos_12, 0) as abnormal_anal_cytology_gpos_12,
coalesce(abnormal_anal_cytology_gpos_13, 0) as abnormal_anal_cytology_gpos_13,
coalesce(abnormal_anal_cytology_gpos_14, 0) as abnormal_anal_cytology_gpos_14,
coalesce(abnormal_anal_cytology_gpos_15, 0) as abnormal_anal_cytology_gpos_15,
coalesce(abnormal_anal_cytology_gpos_16, 0) as abnormal_anal_cytology_gpos_16,
coalesce(abnormal_anal_cytology_gpos_17, 0) as abnormal_anal_cytology_gpos_17,
coalesce(abnormal_anal_cytology_gpos_18, 0) as abnormal_anal_cytology_gpos_18,
coalesce(urethral_discharge_08, 0) as urethral_discharge_08,
coalesce(urethral_discharge_09, 0) as urethral_discharge_09,
coalesce(urethral_discharge_10, 0) as urethral_discharge_10,
coalesce(urethral_discharge_11, 0) as urethral_discharge_11,
coalesce(urethral_discharge_12, 0) as urethral_discharge_12,
coalesce(urethral_discharge_13, 0) as urethral_discharge_13,
coalesce(urethral_discharge_14, 0) as urethral_discharge_14,
coalesce(urethral_discharge_15, 0) as urethral_discharge_15,
coalesce(urethral_discharge_16, 0) as urethral_discharge_16,
coalesce(urethral_discharge_17, 0) as urethral_discharge_17,
coalesce(urethral_discharge_18, 0) as urethral_discharge_18,
coalesce(urethral_discharge_gpos_08, 0) as urethral_discharge_gpos_08,
coalesce(urethral_discharge_gpos_09, 0) as urethral_discharge_gpos_09,
coalesce(urethral_discharge_gpos_10, 0) as urethral_discharge_gpos_10,
coalesce(urethral_discharge_gpos_11, 0) as urethral_discharge_gpos_11,
coalesce(urethral_discharge_gpos_12, 0) as urethral_discharge_gpos_12,
coalesce(urethral_discharge_gpos_13, 0) as urethral_discharge_gpos_13,
coalesce(urethral_discharge_gpos_14, 0) as urethral_discharge_gpos_14,
coalesce(urethral_discharge_gpos_15, 0) as urethral_discharge_gpos_15,
coalesce(urethral_discharge_gpos_16, 0) as urethral_discharge_gpos_16,
coalesce(urethral_discharge_gpos_17, 0) as urethral_discharge_gpos_17,
coalesce(urethral_discharge_gpos_18, 0) as urethral_discharge_gpos_18,
coalesce(urethritis_08, 0) as urethritis_08,
coalesce(urethritis_09, 0) as urethritis_09,
coalesce(urethritis_10, 0) as urethritis_10,
coalesce(urethritis_11, 0) as urethritis_11,
coalesce(urethritis_12, 0) as urethritis_12,
coalesce(urethritis_13, 0) as urethritis_13,
coalesce(urethritis_14, 0) as urethritis_14,
coalesce(urethritis_15, 0) as urethritis_15,
coalesce(urethritis_16, 0) as urethritis_16,
coalesce(urethritis_17, 0) as urethritis_17,
coalesce(urethritis_18, 0) as urethritis_18,
coalesce(urethritis_gpos_08, 0) as urethritis_gpos_08,
coalesce(urethritis_gpos_09, 0) as urethritis_gpos_09,
coalesce(urethritis_gpos_10, 0) as urethritis_gpos_10,
coalesce(urethritis_gpos_11, 0) as urethritis_gpos_11,
coalesce(urethritis_gpos_12, 0) as urethritis_gpos_12,
coalesce(urethritis_gpos_13, 0) as urethritis_gpos_13,
coalesce(urethritis_gpos_14, 0) as urethritis_gpos_14,
coalesce(urethritis_gpos_15, 0) as urethritis_gpos_15,
coalesce(urethritis_gpos_16, 0) as urethritis_gpos_16,
coalesce(urethritis_gpos_17, 0) as urethritis_gpos_17,
coalesce(urethritis_gpos_18, 0) as urethritis_gpos_18,
coalesce(dysuria_08, 0) as dysuria_08,
coalesce(dysuria_09, 0) as dysuria_09,
coalesce(dysuria_10, 0) as dysuria_10,
coalesce(dysuria_11, 0) as dysuria_11,
coalesce(dysuria_12, 0) as dysuria_12,
coalesce(dysuria_13, 0) as dysuria_13,
coalesce(dysuria_14, 0) as dysuria_14,
coalesce(dysuria_15, 0) as dysuria_15,
coalesce(dysuria_16, 0) as dysuria_16,
coalesce(dysuria_17, 0) as dysuria_17,
coalesce(dysuria_18, 0) as dysuria_18,
coalesce(dysuria_gpos_08, 0) as dysuria_gpos_08,
coalesce(dysuria_gpos_09, 0) as dysuria_gpos_09,
coalesce(dysuria_gpos_10, 0) as dysuria_gpos_10,
coalesce(dysuria_gpos_11, 0) as dysuria_gpos_11,
coalesce(dysuria_gpos_12, 0) as dysuria_gpos_12,
coalesce(dysuria_gpos_13, 0) as dysuria_gpos_13,
coalesce(dysuria_gpos_14, 0) as dysuria_gpos_14,
coalesce(dysuria_gpos_15, 0) as dysuria_gpos_15,
coalesce(dysuria_gpos_16, 0) as dysuria_gpos_16,
coalesce(dysuria_gpos_17, 0) as dysuria_gpos_17,
coalesce(dysuria_gpos_18, 0) as dysuria_gpos_18,
coalesce(epididymitis_08, 0) as epididymitis_08,
coalesce(epididymitis_09, 0) as epididymitis_09,
coalesce(epididymitis_10, 0) as epididymitis_10,
coalesce(epididymitis_11, 0) as epididymitis_11,
coalesce(epididymitis_12, 0) as epididymitis_12,
coalesce(epididymitis_13, 0) as epididymitis_13,
coalesce(epididymitis_14, 0) as epididymitis_14,
coalesce(epididymitis_15, 0) as epididymitis_15,
coalesce(epididymitis_16, 0) as epididymitis_16,
coalesce(epididymitis_17, 0) as epididymitis_17,
coalesce(epididymitis_18, 0) as epididymitis_18,
coalesce(epididymitis_gpos_08, 0) as epididymitis_gpos_08,
coalesce(epididymitis_gpos_09, 0) as epididymitis_gpos_09,
coalesce(epididymitis_gpos_10, 0) as epididymitis_gpos_10,
coalesce(epididymitis_gpos_11, 0) as epididymitis_gpos_11,
coalesce(epididymitis_gpos_12, 0) as epididymitis_gpos_12,
coalesce(epididymitis_gpos_13, 0) as epididymitis_gpos_13,
coalesce(epididymitis_gpos_14, 0) as epididymitis_gpos_14,
coalesce(epididymitis_gpos_15, 0) as epididymitis_gpos_15,
coalesce(epididymitis_gpos_16, 0) as epididymitis_gpos_16,
coalesce(epididymitis_gpos_17, 0) as epididymitis_gpos_17,
coalesce(epididymitis_gpos_18, 0) as epididymitis_gpos_18,
coalesce(testicular_pain_08, 0) as testicular_pain_08,
coalesce(testicular_pain_09, 0) as testicular_pain_09,
coalesce(testicular_pain_10, 0) as testicular_pain_10,
coalesce(testicular_pain_11, 0) as testicular_pain_11,
coalesce(testicular_pain_12, 0) as testicular_pain_12,
coalesce(testicular_pain_13, 0) as testicular_pain_13,
coalesce(testicular_pain_14, 0) as testicular_pain_14,
coalesce(testicular_pain_15, 0) as testicular_pain_15,
coalesce(testicular_pain_16, 0) as testicular_pain_16,
coalesce(testicular_pain_17, 0) as testicular_pain_17,
coalesce(testicular_pain_18, 0) as testicular_pain_18,
coalesce(testicular_pain_gpos_08, 0) as testicular_pain_gpos_08,
coalesce(testicular_pain_gpos_09, 0) as testicular_pain_gpos_09,
coalesce(testicular_pain_gpos_10, 0) as testicular_pain_gpos_10,
coalesce(testicular_pain_gpos_11, 0) as testicular_pain_gpos_11,
coalesce(testicular_pain_gpos_12, 0) as testicular_pain_gpos_12,
coalesce(testicular_pain_gpos_13, 0) as testicular_pain_gpos_13,
coalesce(testicular_pain_gpos_14, 0) as testicular_pain_gpos_14,
coalesce(testicular_pain_gpos_15, 0) as testicular_pain_gpos_15,
coalesce(testicular_pain_gpos_16, 0) as testicular_pain_gpos_16,
coalesce(testicular_pain_gpos_17, 0) as testicular_pain_gpos_17,
coalesce(testicular_pain_gpos_18, 0) as testicular_pain_gpos_18,
coalesce(proctitis_rectal_pain_08, 0) as proctitis_rectal_pain_08,
coalesce(proctitis_rectal_pain_09, 0) as proctitis_rectal_pain_09,
coalesce(proctitis_rectal_pain_10, 0) as proctitis_rectal_pain_10,
coalesce(proctitis_rectal_pain_11, 0) as proctitis_rectal_pain_11,
coalesce(proctitis_rectal_pain_12, 0) as proctitis_rectal_pain_12,
coalesce(proctitis_rectal_pain_13, 0) as proctitis_rectal_pain_13,
coalesce(proctitis_rectal_pain_14, 0) as proctitis_rectal_pain_14,
coalesce(proctitis_rectal_pain_15, 0) as proctitis_rectal_pain_15,
coalesce(proctitis_rectal_pain_16, 0) as proctitis_rectal_pain_16,
coalesce(proctitis_rectal_pain_17, 0) as proctitis_rectal_pain_17,
coalesce(proctitis_rectal_pain_18, 0) as proctitis_rectal_pain_18,
coalesce(proctitis_rectal_pain_gpos_08, 0) as proctitis_rectal_pain_gpos_08,
coalesce(proctitis_rectal_pain_gpos_09, 0) as proctitis_rectal_pain_gpos_09,
coalesce(proctitis_rectal_pain_gpos_10, 0) as proctitis_rectal_pain_gpos_10,
coalesce(proctitis_rectal_pain_gpos_11, 0) as proctitis_rectal_pain_gpos_11,
coalesce(proctitis_rectal_pain_gpos_12, 0) as proctitis_rectal_pain_gpos_12,
coalesce(proctitis_rectal_pain_gpos_13, 0) as proctitis_rectal_pain_gpos_13,
coalesce(proctitis_rectal_pain_gpos_14, 0) as proctitis_rectal_pain_gpos_14,
coalesce(proctitis_rectal_pain_gpos_15, 0) as proctitis_rectal_pain_gpos_15,
coalesce(proctitis_rectal_pain_gpos_16, 0) as proctitis_rectal_pain_gpos_16,
coalesce(proctitis_rectal_pain_gpos_17, 0) as proctitis_rectal_pain_gpos_17,
coalesce(proctitis_rectal_pain_gpos_18, 0) as proctitis_rectal_pain_gpos_18,
coalesce(rectal_bleeding_08, 0) as rectal_bleeding_08,
coalesce(rectal_bleeding_09, 0) as rectal_bleeding_09,
coalesce(rectal_bleeding_10, 0) as rectal_bleeding_10,
coalesce(rectal_bleeding_11, 0) as rectal_bleeding_11,
coalesce(rectal_bleeding_12, 0) as rectal_bleeding_12,
coalesce(rectal_bleeding_13, 0) as rectal_bleeding_13,
coalesce(rectal_bleeding_14, 0) as rectal_bleeding_14,
coalesce(rectal_bleeding_15, 0) as rectal_bleeding_15,
coalesce(rectal_bleeding_16, 0) as rectal_bleeding_16,
coalesce(rectal_bleeding_17, 0) as rectal_bleeding_17,
coalesce(rectal_bleeding_18, 0) as rectal_bleeding_18,
coalesce(rectal_bleeding_gpos_08, 0) as rectal_bleeding_gpos_08,
coalesce(rectal_bleeding_gpos_09, 0) as rectal_bleeding_gpos_09,
coalesce(rectal_bleeding_gpos_10, 0) as rectal_bleeding_gpos_10,
coalesce(rectal_bleeding_gpos_11, 0) as rectal_bleeding_gpos_11,
coalesce(rectal_bleeding_gpos_12, 0) as rectal_bleeding_gpos_12,
coalesce(rectal_bleeding_gpos_13, 0) as rectal_bleeding_gpos_13,
coalesce(rectal_bleeding_gpos_14, 0) as rectal_bleeding_gpos_14,
coalesce(rectal_bleeding_gpos_15, 0) as rectal_bleeding_gpos_15,
coalesce(rectal_bleeding_gpos_16, 0) as rectal_bleeding_gpos_16,
coalesce(rectal_bleeding_gpos_17, 0) as rectal_bleeding_gpos_17,
coalesce(rectal_bleeding_gpos_18, 0) as rectal_bleeding_gpos_18,
coalesce(pharyngitis_08, 0) as pharyngitis_08,
coalesce(pharyngitis_09, 0) as pharyngitis_09,
coalesce(pharyngitis_10, 0) as pharyngitis_10,
coalesce(pharyngitis_11, 0) as pharyngitis_11,
coalesce(pharyngitis_12, 0) as pharyngitis_12,
coalesce(pharyngitis_13, 0) as pharyngitis_13,
coalesce(pharyngitis_14, 0) as pharyngitis_14,
coalesce(pharyngitis_15, 0) as pharyngitis_15,
coalesce(pharyngitis_16, 0) as pharyngitis_16,
coalesce(pharyngitis_17, 0) as pharyngitis_17,
coalesce(pharyngitis_18, 0) as pharyngitis_18,
coalesce(pharyngitis_gpos_08, 0) as pharyngitis_gpos_08,
coalesce(pharyngitis_gpos_09, 0) as pharyngitis_gpos_09,
coalesce(pharyngitis_gpos_10, 0) as pharyngitis_gpos_10,
coalesce(pharyngitis_gpos_11, 0) as pharyngitis_gpos_11,
coalesce(pharyngitis_gpos_12, 0) as pharyngitis_gpos_12,
coalesce(pharyngitis_gpos_13, 0) as pharyngitis_gpos_13,
coalesce(pharyngitis_gpos_14, 0) as pharyngitis_gpos_14,
coalesce(pharyngitis_gpos_15, 0) as pharyngitis_gpos_15,
coalesce(pharyngitis_gpos_16, 0) as pharyngitis_gpos_16,
coalesce(pharyngitis_gpos_17, 0) as pharyngitis_gpos_17,
coalesce(pharyngitis_gpos_18, 0) as pharyngitis_gpos_18,
coalesce(tonsillitis_08, 0) as tonsillitis_08,
coalesce(tonsillitis_09, 0) as tonsillitis_09,
coalesce(tonsillitis_10, 0) as tonsillitis_10,
coalesce(tonsillitis_11, 0) as tonsillitis_11,
coalesce(tonsillitis_12, 0) as tonsillitis_12,
coalesce(tonsillitis_13, 0) as tonsillitis_13,
coalesce(tonsillitis_14, 0) as tonsillitis_14,
coalesce(tonsillitis_15, 0) as tonsillitis_15,
coalesce(tonsillitis_16, 0) as tonsillitis_16,
coalesce(tonsillitis_17, 0) as tonsillitis_17,
coalesce(tonsillitis_18, 0) as tonsillitis_18,
coalesce(tonsillitis_gpos_08, 0) as tonsillitis_gpos_08,
coalesce(tonsillitis_gpos_09, 0) as tonsillitis_gpos_09,
coalesce(tonsillitis_gpos_10, 0) as tonsillitis_gpos_10,
coalesce(tonsillitis_gpos_11, 0) as tonsillitis_gpos_11,
coalesce(tonsillitis_gpos_12, 0) as tonsillitis_gpos_12,
coalesce(tonsillitis_gpos_13, 0) as tonsillitis_gpos_13,
coalesce(tonsillitis_gpos_14, 0) as tonsillitis_gpos_14,
coalesce(tonsillitis_gpos_15, 0) as tonsillitis_gpos_15,
coalesce(tonsillitis_gpos_16, 0) as tonsillitis_gpos_16,
coalesce(tonsillitis_gpos_17, 0) as tonsillitis_gpos_17,
coalesce(tonsillitis_gpos_18, 0) as tonsillitis_gpos_18,
coalesce(throat_pain_08, 0) as throat_pain_08,
coalesce(throat_pain_09, 0) as throat_pain_09,
coalesce(throat_pain_10, 0) as throat_pain_10,
coalesce(throat_pain_11, 0) as throat_pain_11,
coalesce(throat_pain_12, 0) as throat_pain_12,
coalesce(throat_pain_13, 0) as throat_pain_13,
coalesce(throat_pain_14, 0) as throat_pain_14,
coalesce(throat_pain_15, 0) as throat_pain_15,
coalesce(throat_pain_16, 0) as throat_pain_16,
coalesce(throat_pain_17, 0) as throat_pain_17,
coalesce(throat_pain_18, 0) as throat_pain_18,
coalesce(throat_pain_gpos_08, 0) as throat_pain_gpos_08,
coalesce(throat_pain_gpos_09, 0) as throat_pain_gpos_09,
coalesce(throat_pain_gpos_10, 0) as throat_pain_gpos_10,
coalesce(throat_pain_gpos_11, 0) as throat_pain_gpos_11,
coalesce(throat_pain_gpos_12, 0) as throat_pain_gpos_12,
coalesce(throat_pain_gpos_13, 0) as throat_pain_gpos_13,
coalesce(throat_pain_gpos_14, 0) as throat_pain_gpos_14,
coalesce(throat_pain_gpos_15, 0) as throat_pain_gpos_15,
coalesce(throat_pain_gpos_16, 0) as throat_pain_gpos_16,
coalesce(throat_pain_gpos_17, 0) as throat_pain_gpos_17,
coalesce(throat_pain_gpos_18, 0) as throat_pain_gpos_18,
coalesce(conjunctivitis_08, 0) as conjunctivitis_08,
coalesce(conjunctivitis_09, 0) as conjunctivitis_09,
coalesce(conjunctivitis_10, 0) as conjunctivitis_10,
coalesce(conjunctivitis_11, 0) as conjunctivitis_11,
coalesce(conjunctivitis_12, 0) as conjunctivitis_12,
coalesce(conjunctivitis_13, 0) as conjunctivitis_13,
coalesce(conjunctivitis_14, 0) as conjunctivitis_14,
coalesce(conjunctivitis_15, 0) as conjunctivitis_15,
coalesce(conjunctivitis_16, 0) as conjunctivitis_16,
coalesce(conjunctivitis_17, 0) as conjunctivitis_17,
coalesce(conjunctivitis_18, 0) as conjunctivitis_18,
coalesce(conjunctivitis_gpos_08, 0) as conjunctivitis_gpos_08,
coalesce(conjunctivitis_gpos_09, 0) as conjunctivitis_gpos_09,
coalesce(conjunctivitis_gpos_10, 0) as conjunctivitis_gpos_10,
coalesce(conjunctivitis_gpos_11, 0) as conjunctivitis_gpos_11,
coalesce(conjunctivitis_gpos_12, 0) as conjunctivitis_gpos_12,
coalesce(conjunctivitis_gpos_13, 0) as conjunctivitis_gpos_13,
coalesce(conjunctivitis_gpos_14, 0) as conjunctivitis_gpos_14,
coalesce(conjunctivitis_gpos_15, 0) as conjunctivitis_gpos_15,
coalesce(conjunctivitis_gpos_16, 0) as conjunctivitis_gpos_16,
coalesce(conjunctivitis_gpos_17, 0) as conjunctivitis_gpos_17,
coalesce(conjunctivitis_gpos_18, 0) as conjunctivitis_gpos_18,
coalesce(eye_pain_08, 0) as eye_pain_08,
coalesce(eye_pain_09, 0) as eye_pain_09,
coalesce(eye_pain_10, 0) as eye_pain_10,
coalesce(eye_pain_11, 0) as eye_pain_11,
coalesce(eye_pain_12, 0) as eye_pain_12,
coalesce(eye_pain_13, 0) as eye_pain_13,
coalesce(eye_pain_14, 0) as eye_pain_14,
coalesce(eye_pain_15, 0) as eye_pain_15,
coalesce(eye_pain_16, 0) as eye_pain_16,
coalesce(eye_pain_17, 0) as eye_pain_17,
coalesce(eye_pain_18, 0) as eye_pain_18,
coalesce(eye_pain_gpos_08, 0) as eye_pain_gpos_08,
coalesce(eye_pain_gpos_09, 0) as eye_pain_gpos_09,
coalesce(eye_pain_gpos_10, 0) as eye_pain_gpos_10,
coalesce(eye_pain_gpos_11, 0) as eye_pain_gpos_11,
coalesce(eye_pain_gpos_12, 0) as eye_pain_gpos_12,
coalesce(eye_pain_gpos_13, 0) as eye_pain_gpos_13,
coalesce(eye_pain_gpos_14, 0) as eye_pain_gpos_14,
coalesce(eye_pain_gpos_15, 0) as eye_pain_gpos_15,
coalesce(eye_pain_gpos_16, 0) as eye_pain_gpos_16,
coalesce(eye_pain_gpos_17, 0) as eye_pain_gpos_17,
coalesce(eye_pain_gpos_18, 0) as eye_pain_gpos_18,
coalesce(vaginitis_08, 0) as vaginitis_08,
coalesce(vaginitis_09, 0) as vaginitis_09,
coalesce(vaginitis_10, 0) as vaginitis_10,
coalesce(vaginitis_11, 0) as vaginitis_11,
coalesce(vaginitis_12, 0) as vaginitis_12,
coalesce(vaginitis_13, 0) as vaginitis_13,
coalesce(vaginitis_14, 0) as vaginitis_14,
coalesce(vaginitis_15, 0) as vaginitis_15,
coalesce(vaginitis_16, 0) as vaginitis_16,
coalesce(vaginitis_17, 0) as vaginitis_17,
coalesce(vaginitis_18, 0) as vaginitis_18,
coalesce(vaginitis_gpos_08, 0) as vaginitis_gpos_08,
coalesce(vaginitis_gpos_09, 0) as vaginitis_gpos_09,
coalesce(vaginitis_gpos_10, 0) as vaginitis_gpos_10,
coalesce(vaginitis_gpos_11, 0) as vaginitis_gpos_11,
coalesce(vaginitis_gpos_12, 0) as vaginitis_gpos_12,
coalesce(vaginitis_gpos_13, 0) as vaginitis_gpos_13,
coalesce(vaginitis_gpos_14, 0) as vaginitis_gpos_14,
coalesce(vaginitis_gpos_15, 0) as vaginitis_gpos_15,
coalesce(vaginitis_gpos_16, 0) as vaginitis_gpos_16,
coalesce(vaginitis_gpos_17, 0) as vaginitis_gpos_17,
coalesce(vaginitis_gpos_18, 0) as vaginitis_gpos_18,
coalesce(cervicitis_08, 0) as cervicitis_08,
coalesce(cervicitis_09, 0) as cervicitis_09,
coalesce(cervicitis_10, 0) as cervicitis_10,
coalesce(cervicitis_11, 0) as cervicitis_11,
coalesce(cervicitis_12, 0) as cervicitis_12,
coalesce(cervicitis_13, 0) as cervicitis_13,
coalesce(cervicitis_14, 0) as cervicitis_14,
coalesce(cervicitis_15, 0) as cervicitis_15,
coalesce(cervicitis_16, 0) as cervicitis_16,
coalesce(cervicitis_17, 0) as cervicitis_17,
coalesce(cervicitis_18, 0) as cervicitis_18,
coalesce(cervicitis_gpos_08, 0) as cervicitis_gpos_08,
coalesce(cervicitis_gpos_09, 0) as cervicitis_gpos_09,
coalesce(cervicitis_gpos_10, 0) as cervicitis_gpos_10,
coalesce(cervicitis_gpos_11, 0) as cervicitis_gpos_11,
coalesce(cervicitis_gpos_12, 0) as cervicitis_gpos_12,
coalesce(cervicitis_gpos_13, 0) as cervicitis_gpos_13,
coalesce(cervicitis_gpos_14, 0) as cervicitis_gpos_14,
coalesce(cervicitis_gpos_15, 0) as cervicitis_gpos_15,
coalesce(cervicitis_gpos_16, 0) as cervicitis_gpos_16,
coalesce(cervicitis_gpos_17, 0) as cervicitis_gpos_17,
coalesce(cervicitis_gpos_18, 0) as cervicitis_gpos_18,
coalesce(vaginal_leucorrhea_08, 0) as vaginal_leucorrhea_08,
coalesce(vaginal_leucorrhea_09, 0) as vaginal_leucorrhea_09,
coalesce(vaginal_leucorrhea_10, 0) as vaginal_leucorrhea_10,
coalesce(vaginal_leucorrhea_11, 0) as vaginal_leucorrhea_11,
coalesce(vaginal_leucorrhea_12, 0) as vaginal_leucorrhea_12,
coalesce(vaginal_leucorrhea_13, 0) as vaginal_leucorrhea_13,
coalesce(vaginal_leucorrhea_14, 0) as vaginal_leucorrhea_14,
coalesce(vaginal_leucorrhea_15, 0) as vaginal_leucorrhea_15,
coalesce(vaginal_leucorrhea_16, 0) as vaginal_leucorrhea_16,
coalesce(vaginal_leucorrhea_17, 0) as vaginal_leucorrhea_17,
coalesce(vaginal_leucorrhea_18, 0) as vaginal_leucorrhea_18,
coalesce(vaginal_leucorrhea_gpos_08, 0) as vaginal_leucorrhea_gpos_08,
coalesce(vaginal_leucorrhea_gpos_09, 0) as vaginal_leucorrhea_gpos_09,
coalesce(vaginal_leucorrhea_gpos_10, 0) as vaginal_leucorrhea_gpos_10,
coalesce(vaginal_leucorrhea_gpos_11, 0) as vaginal_leucorrhea_gpos_11,
coalesce(vaginal_leucorrhea_gpos_12, 0) as vaginal_leucorrhea_gpos_12,
coalesce(vaginal_leucorrhea_gpos_13, 0) as vaginal_leucorrhea_gpos_13,
coalesce(vaginal_leucorrhea_gpos_14, 0) as vaginal_leucorrhea_gpos_14,
coalesce(vaginal_leucorrhea_gpos_15, 0) as vaginal_leucorrhea_gpos_15,
coalesce(vaginal_leucorrhea_gpos_16, 0) as vaginal_leucorrhea_gpos_16,
coalesce(vaginal_leucorrhea_gpos_17, 0) as vaginal_leucorrhea_gpos_17,
coalesce(vaginal_leucorrhea_gpos_18, 0) as vaginal_leucorrhea_gpos_18,
coalesce(chlamydia_dx_08, 0) as chlamydia_dx_08,
coalesce(chlamydia_dx_09, 0) as chlamydia_dx_09,
coalesce(chlamydia_dx_10, 0) as chlamydia_dx_10,
coalesce(chlamydia_dx_11, 0) as chlamydia_dx_11,
coalesce(chlamydia_dx_12, 0) as chlamydia_dx_12,
coalesce(chlamydia_dx_13, 0) as chlamydia_dx_13,
coalesce(chlamydia_dx_14, 0) as chlamydia_dx_14,
coalesce(chlamydia_dx_15, 0) as chlamydia_dx_15,
coalesce(chlamydia_dx_16, 0) as chlamydia_dx_16,
coalesce(chlamydia_dx_17, 0) as chlamydia_dx_17,
coalesce(chlamydia_dx_18, 0) as chlamydia_dx_18,
coalesce(chlamydia_dx_gpos_08, 0) as chlamydia_dx_gpos_08,
coalesce(chlamydia_dx_gpos_09, 0) as chlamydia_dx_gpos_09,
coalesce(chlamydia_dx_gpos_10, 0) as chlamydia_dx_gpos_10,
coalesce(chlamydia_dx_gpos_11, 0) as chlamydia_dx_gpos_11,
coalesce(chlamydia_dx_gpos_12, 0) as chlamydia_dx_gpos_12,
coalesce(chlamydia_dx_gpos_13, 0) as chlamydia_dx_gpos_13,
coalesce(chlamydia_dx_gpos_14, 0) as chlamydia_dx_gpos_14,
coalesce(chlamydia_dx_gpos_15, 0) as chlamydia_dx_gpos_15,
coalesce(chlamydia_dx_gpos_16, 0) as chlamydia_dx_gpos_16,
coalesce(chlamydia_dx_gpos_17, 0) as chlamydia_dx_gpos_17,
coalesce(chlamydia_dx_gpos_18, 0) as chlamydia_dx_gpos_18,
coalesce(screening_for_stis_08, 0) as screening_for_stis_08,
coalesce(screening_for_stis_09, 0) as screening_for_stis_09,
coalesce(screening_for_stis_10, 0) as screening_for_stis_10,
coalesce(screening_for_stis_11, 0) as screening_for_stis_11,
coalesce(screening_for_stis_12, 0) as screening_for_stis_12,
coalesce(screening_for_stis_13, 0) as screening_for_stis_13,
coalesce(screening_for_stis_14, 0) as screening_for_stis_14,
coalesce(screening_for_stis_15, 0) as screening_for_stis_15,
coalesce(screening_for_stis_16, 0) as screening_for_stis_16,
coalesce(screening_for_stis_17, 0) as screening_for_stis_17,
coalesce(screening_for_stis_18, 0) as screening_for_stis_18,
coalesce(screening_for_stis_gpos_08, 0) as screening_for_stis_gpos_08,
coalesce(screening_for_stis_gpos_09, 0) as screening_for_stis_gpos_09,
coalesce(screening_for_stis_gpos_10, 0) as screening_for_stis_gpos_10,
coalesce(screening_for_stis_gpos_11, 0) as screening_for_stis_gpos_11,
coalesce(screening_for_stis_gpos_12, 0) as screening_for_stis_gpos_12,
coalesce(screening_for_stis_gpos_13, 0) as screening_for_stis_gpos_13,
coalesce(screening_for_stis_gpos_14, 0) as screening_for_stis_gpos_14,
coalesce(screening_for_stis_gpos_15, 0) as screening_for_stis_gpos_15,
coalesce(screening_for_stis_gpos_16, 0) as screening_for_stis_gpos_16,
coalesce(screening_for_stis_gpos_17, 0) as screening_for_stis_gpos_17,
coalesce(screening_for_stis_gpos_18, 0) as screening_for_stis_gpos_18,
coalesce(cont_or_exp_to_vd_08, 0) as cont_or_exp_to_vd_08,
coalesce(cont_or_exp_to_vd_09, 0) as cont_or_exp_to_vd_09,
coalesce(cont_or_exp_to_vd_10, 0) as cont_or_exp_to_vd_10,
coalesce(cont_or_exp_to_vd_11, 0) as cont_or_exp_to_vd_11,
coalesce(cont_or_exp_to_vd_12, 0) as cont_or_exp_to_vd_12,
coalesce(cont_or_exp_to_vd_13, 0) as cont_or_exp_to_vd_13,
coalesce(cont_or_exp_to_vd_14, 0) as cont_or_exp_to_vd_14,
coalesce(cont_or_exp_to_vd_15, 0) as cont_or_exp_to_vd_15,
coalesce(cont_or_exp_to_vd_16, 0) as cont_or_exp_to_vd_16,
coalesce(cont_or_exp_to_vd_17, 0) as cont_or_exp_to_vd_17,
coalesce(cont_or_exp_to_vd_18, 0) as cont_or_exp_to_vd_18,
coalesce(cont_or_exp_to_vd_gpos_08, 0) as cont_or_exp_to_vd_gpos_08,
coalesce(cont_or_exp_to_vd_gpos_09, 0) as cont_or_exp_to_vd_gpos_09,
coalesce(cont_or_exp_to_vd_gpos_10, 0) as cont_or_exp_to_vd_gpos_10,
coalesce(cont_or_exp_to_vd_gpos_11, 0) as cont_or_exp_to_vd_gpos_11,
coalesce(cont_or_exp_to_vd_gpos_12, 0) as cont_or_exp_to_vd_gpos_12,
coalesce(cont_or_exp_to_vd_gpos_13, 0) as cont_or_exp_to_vd_gpos_13,
coalesce(cont_or_exp_to_vd_gpos_14, 0) as cont_or_exp_to_vd_gpos_14,
coalesce(cont_or_exp_to_vd_gpos_15, 0) as cont_or_exp_to_vd_gpos_15,
coalesce(cont_or_exp_to_vd_gpos_16, 0) as cont_or_exp_to_vd_gpos_16,
coalesce(cont_or_exp_to_vd_gpos_17, 0) as cont_or_exp_to_vd_gpos_17,
coalesce(cont_or_exp_to_vd_gpos_18, 0) as cont_or_exp_to_vd_gpos_18,
coalesce(high_risk_sexual_behavior_08, 0) as high_risk_sexual_behavior_08,
coalesce(high_risk_sexual_behavior_09, 0) as high_risk_sexual_behavior_09,
coalesce(high_risk_sexual_behavior_10, 0) as high_risk_sexual_behavior_10,
coalesce(high_risk_sexual_behavior_11, 0) as high_risk_sexual_behavior_11,
coalesce(high_risk_sexual_behavior_12, 0) as high_risk_sexual_behavior_12,
coalesce(high_risk_sexual_behavior_13, 0) as high_risk_sexual_behavior_13,
coalesce(high_risk_sexual_behavior_14, 0) as high_risk_sexual_behavior_14,
coalesce(high_risk_sexual_behavior_15, 0) as high_risk_sexual_behavior_15,
coalesce(high_risk_sexual_behavior_16, 0) as high_risk_sexual_behavior_16,
coalesce(high_risk_sexual_behavior_17, 0) as high_risk_sexual_behavior_17,
coalesce(high_risk_sexual_behavior_18, 0) as high_risk_sexual_behavior_18,
coalesce(high_risk_sexual_behavior_gpos_08, 0) as high_risk_sexual_behavior_gpos_08,
coalesce(high_risk_sexual_behavior_gpos_09, 0) as high_risk_sexual_behavior_gpos_09,
coalesce(high_risk_sexual_behavior_gpos_10, 0) as high_risk_sexual_behavior_gpos_10,
coalesce(high_risk_sexual_behavior_gpos_11, 0) as high_risk_sexual_behavior_gpos_11,
coalesce(high_risk_sexual_behavior_gpos_12, 0) as high_risk_sexual_behavior_gpos_12,
coalesce(high_risk_sexual_behavior_gpos_13, 0) as high_risk_sexual_behavior_gpos_13,
coalesce(high_risk_sexual_behavior_gpos_14, 0) as high_risk_sexual_behavior_gpos_14,
coalesce(high_risk_sexual_behavior_gpos_15, 0) as high_risk_sexual_behavior_gpos_15,
coalesce(high_risk_sexual_behavior_gpos_16, 0) as high_risk_sexual_behavior_gpos_16,
coalesce(high_risk_sexual_behavior_gpos_17, 0) as high_risk_sexual_behavior_gpos_17,
coalesce(high_risk_sexual_behavior_gpos_18, 0) as high_risk_sexual_behavior_gpos_18,
coalesce(hiv_counseling_08, 0) as hiv_counseling_08,
coalesce(hiv_counseling_09, 0) as hiv_counseling_09,
coalesce(hiv_counseling_10, 0) as hiv_counseling_10,
coalesce(hiv_counseling_11, 0) as hiv_counseling_11,
coalesce(hiv_counseling_12, 0) as hiv_counseling_12,
coalesce(hiv_counseling_13, 0) as hiv_counseling_13,
coalesce(hiv_counseling_14, 0) as hiv_counseling_14,
coalesce(hiv_counseling_15, 0) as hiv_counseling_15,
coalesce(hiv_counseling_16, 0) as hiv_counseling_16,
coalesce(hiv_counseling_17, 0) as hiv_counseling_17,
coalesce(hiv_counseling_18, 0) as hiv_counseling_18,
coalesce(hiv_counseling_gpos_08, 0) as hiv_counseling_gpos_08,
coalesce(hiv_counseling_gpos_09, 0) as hiv_counseling_gpos_09,
coalesce(hiv_counseling_gpos_10, 0) as hiv_counseling_gpos_10,
coalesce(hiv_counseling_gpos_11, 0) as hiv_counseling_gpos_11,
coalesce(hiv_counseling_gpos_12, 0) as hiv_counseling_gpos_12,
coalesce(hiv_counseling_gpos_13, 0) as hiv_counseling_gpos_13,
coalesce(hiv_counseling_gpos_14, 0) as hiv_counseling_gpos_14,
coalesce(hiv_counseling_gpos_15, 0) as hiv_counseling_gpos_15,
coalesce(hiv_counseling_gpos_16, 0) as hiv_counseling_gpos_16,
coalesce(hiv_counseling_gpos_17, 0) as hiv_counseling_gpos_17,
coalesce(hiv_counseling_gpos_18, 0) as hiv_counseling_gpos_18
FROM mgtp_index_patients T1
LEFT JOIN mgtp_gon_counts T2 ON T1.patient_id = T2.patient_id
LEFT JOIN mgtp_gon_cases T3  ON T1.patient_id = T3.patient_id
LEFT JOIN mgtp_chlam_counts T4 ON T1.patient_id = T4.patient_id
LEFT JOIN mgtp_chlam_cases T5 ON T1.patient_id = T5.patient_id
LEFT JOIN mgtp_syph_counts T6 ON T1.patient_id = T6.patient_id
LEFT JOIN mgtp_syph_cases T7 ON T1.patient_id = T7.patient_id
LEFT JOIN mgtp_cpts_of_interest T8 ON T1.patient_id = T8.patient_id
LEFT JOIN mgtp_hiv_all_details T9 ON T1.patient_id = T9.patient_id
LEFT JOIN mgtp_diag_fields_by_year T10 ON T1.patient_id = T10.patient_id
LEFT JOIN mgtp_multisite_data T11 on T1.patient_id = T11.patient_id;


-- JOIN TO PATIENT TABLE
-- Restrict to patients age ≥15.
-- Add center filtering here
CREATE TABLE mgtp_output_with_patient  AS
SELECT date_part('year',age(date_of_birth)) today_age,
date_part('year', age('2018-12-31', date_of_birth)) age_end_of_2018,
T1.gender,
T1.race,
T2.*
FROM emr_patient T1 
INNER JOIN mgtp_output_part_1 T2 ON ((T1.id = T2.patient_id))
WHERE date_part('year', age('2018-12-31', date_of_birth)) >= 15;
AND (center_id not in ('1') or center_id IS NULL);


-- ENCOUNTERS - Gather up encounters
-- CREATE TABLE mgtp_output_pat_and_enc  AS 
-- SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, T1.id as enc_id  
-- FROM emr_encounter T1, 
-- mgtp_output_with_patient T2,
-- static_enc_type_lookup T3
-- WHERE T1.patient_id = T2.patient_id  
-- AND T1.raw_encounter_type = T3.raw_encounter_type 
-- AND (T1.raw_encounter_type is NULL or T3.ambulatory = 1)
-- AND  T1.date >= '01-01-2010' 
-- AND T1.date < '01-01-2018';

-- ENCOUNTERS - Gather up encounters
-- CHANGING ENCOUNTER METHOD TO NEW FILTER
CREATE TABLE mgtp_output_pat_and_enc  AS 
SELECT T2.patient_id, EXTRACT(YEAR FROM date)  rpt_year, date
FROM gen_pop_tools.clin_enc T1,
mgtp_output_with_patient T2
WHERE T1.patient_id = T2.patient_id
AND  T1.date >= '01-01-2010' 
AND T1.date < '01-01-2019'
GROUP BY T2.patient_id, date;


-- ENCOUNTERS - Counts & Column Creation
CREATE TABLE mgtp_ouput_pat_and_enc_counts  AS 
SELECT T1.patient_id,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_encounters_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_encounters_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_encounters_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_encounters_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_encounters_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_encounters_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_encounters_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_encounters_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END)  t_encounters_18
FROM mgtp_output_pat_and_enc T1 
INNER JOIN mgtp_output_with_patient T2 ON ((T1.patient_id = T2.patient_id))  
GROUP BY T1.patient_id;



-- JOIN TO PREVIOUS DATA WITH ENCOUNTERS FOR FULL REPORT
CREATE TABLE mgtp_report_final_output  AS SELECT 
--T1.patient_id,
age_end_of_2018,
gender,
race,
coalesce(t_encounters_10, 0) t_encounters_10,
coalesce(t_encounters_11, 0) t_encounters_11,
coalesce(t_encounters_12, 0) t_encounters_12,
coalesce(t_encounters_13, 0) t_encounters_13,
coalesce(t_encounters_14, 0) t_encounters_14,
coalesce(t_encounters_15, 0) t_encounters_15,
coalesce(t_encounters_16, 0) t_encounters_16,
coalesce(t_encounters_17, 0) t_encounters_17,
coalesce(t_encounters_18, 0) t_encounters_18,
total_gonorrhea_tests_08,
total_gonorrhea_tests_09,
total_gonorrhea_tests_10,
total_gonorrhea_tests_11,
total_gonorrhea_tests_12,
total_gonorrhea_tests_13,
total_gonorrhea_tests_14,
total_gonorrhea_tests_15,
total_gonorrhea_tests_16,
total_gonorrhea_tests_17,
total_gonorrhea_tests_18,
positive_gonorrhea_tests_08,
positive_gonorrhea_tests_09,
positive_gonorrhea_tests_10,
positive_gonorrhea_tests_11,
positive_gonorrhea_tests_12,
positive_gonorrhea_tests_13,
positive_gonorrhea_tests_14,
positive_gonorrhea_tests_15,
positive_gonorrhea_tests_16,
positive_gonorrhea_tests_17,
positive_gonorrhea_tests_18,
gonorrhea_per_esp_08,
gonorrhea_per_esp_09,
gonorrhea_per_esp_10,
gonorrhea_per_esp_11,
gonorrhea_per_esp_12,
gonorrhea_per_esp_13,
gonorrhea_per_esp_14,
gonorrhea_per_esp_15,
gonorrhea_per_esp_16,
gonorrhea_per_esp_17,
gonorrhea_per_esp_18,
total_gonorrhea_tests_urog_08,
total_gonorrhea_tests_urog_09,
total_gonorrhea_tests_urog_10,
total_gonorrhea_tests_urog_11,
total_gonorrhea_tests_urog_12,
total_gonorrhea_tests_urog_13,
total_gonorrhea_tests_urog_14,
total_gonorrhea_tests_urog_15,
total_gonorrhea_tests_urog_16,
total_gonorrhea_tests_urog_17,
total_gonorrhea_tests_urog_18,
positive_gonorrhea_tests_urog_08,
positive_gonorrhea_tests_urog_09,
positive_gonorrhea_tests_urog_10,
positive_gonorrhea_tests_urog_11,
positive_gonorrhea_tests_urog_12,
positive_gonorrhea_tests_urog_13,
positive_gonorrhea_tests_urog_14,
positive_gonorrhea_tests_urog_15,
positive_gonorrhea_tests_urog_16,
positive_gonorrhea_tests_urog_17,
positive_gonorrhea_tests_urog_18,
total_gonorrhea_tests_rectal_08,
total_gonorrhea_tests_rectal_09,
total_gonorrhea_tests_rectal_10,
total_gonorrhea_tests_rectal_11,
total_gonorrhea_tests_rectal_12,
total_gonorrhea_tests_rectal_13,
total_gonorrhea_tests_rectal_14,
total_gonorrhea_tests_rectal_15,
total_gonorrhea_tests_rectal_16,
total_gonorrhea_tests_rectal_17,
total_gonorrhea_tests_rectal_18,
positive_gonorrhea_tests_rectal_08,
positive_gonorrhea_tests_rectal_09,
positive_gonorrhea_tests_rectal_10,
positive_gonorrhea_tests_rectal_11,
positive_gonorrhea_tests_rectal_12,
positive_gonorrhea_tests_rectal_13,
positive_gonorrhea_tests_rectal_14,
positive_gonorrhea_tests_rectal_15,
positive_gonorrhea_tests_rectal_16,
positive_gonorrhea_tests_rectal_17,
positive_gonorrhea_tests_rectal_18,
total_gonorrhea_tests_throat_08,
total_gonorrhea_tests_throat_09,
total_gonorrhea_tests_throat_10,
total_gonorrhea_tests_throat_11,
total_gonorrhea_tests_throat_12,
total_gonorrhea_tests_throat_13,
total_gonorrhea_tests_throat_14,
total_gonorrhea_tests_throat_15,
total_gonorrhea_tests_throat_16,
total_gonorrhea_tests_throat_17,
total_gonorrhea_tests_throat_18,
positive_gonorrhea_tests_throat_08,
positive_gonorrhea_tests_throat_09,
positive_gonorrhea_tests_throat_10,
positive_gonorrhea_tests_throat_11,
positive_gonorrhea_tests_throat_12,
positive_gonorrhea_tests_throat_13,
positive_gonorrhea_tests_throat_14,
positive_gonorrhea_tests_throat_15,
positive_gonorrhea_tests_throat_16,
positive_gonorrhea_tests_throat_17,
positive_gonorrhea_tests_throat_18,
total_gonorrhea_tests_other_08,
total_gonorrhea_tests_other_09,
total_gonorrhea_tests_other_10,
total_gonorrhea_tests_other_11,
total_gonorrhea_tests_other_12,
total_gonorrhea_tests_other_13,
total_gonorrhea_tests_other_14,
total_gonorrhea_tests_other_15,
total_gonorrhea_tests_other_16,
total_gonorrhea_tests_other_17,
total_gonorrhea_tests_other_18,
positive_gonorrhea_tests_other_08,
positive_gonorrhea_tests_other_09,
positive_gonorrhea_tests_other_10,
positive_gonorrhea_tests_other_11,
positive_gonorrhea_tests_other_12,
positive_gonorrhea_tests_other_13,
positive_gonorrhea_tests_other_14,
positive_gonorrhea_tests_other_15,
positive_gonorrhea_tests_other_16,
positive_gonorrhea_tests_other_17,
positive_gonorrhea_tests_other_18,
total_gonorrhea_tests_missing_08,
total_gonorrhea_tests_missing_09,
total_gonorrhea_tests_missing_10,
total_gonorrhea_tests_missing_11,
total_gonorrhea_tests_missing_12,
total_gonorrhea_tests_missing_13,
total_gonorrhea_tests_missing_14,
total_gonorrhea_tests_missing_15,
total_gonorrhea_tests_missing_16,
total_gonorrhea_tests_missing_17,
total_gonorrhea_tests_missing_18,
positive_gonorrhea_tests_missing_08,
positive_gonorrhea_tests_missing_09,
positive_gonorrhea_tests_missing_10,
positive_gonorrhea_tests_missing_11,
positive_gonorrhea_tests_missing_12,
positive_gonorrhea_tests_missing_13,
positive_gonorrhea_tests_missing_14,
positive_gonorrhea_tests_missing_15,
positive_gonorrhea_tests_missing_16,
positive_gonorrhea_tests_missing_17,
positive_gonorrhea_tests_missing_18,
max_num_same_day_sites_08,
max_same_day_sites_tested_08,
max_same_day_sites_positive_yn_08,
max_num_same_day_sites_09,
max_same_day_sites_tested_09,
max_same_day_sites_positive_yn_09,
max_num_same_day_sites_10,
max_same_day_sites_tested_10,
max_same_day_sites_positive_yn_10,
max_num_same_day_sites_11,
max_same_day_sites_tested_11,
max_same_day_sites_positive_yn_11,
max_num_same_day_sites_12,
max_same_day_sites_tested_12,
max_same_day_sites_positive_yn_12,
max_num_same_day_sites_13,
max_same_day_sites_tested_13,
max_same_day_sites_positive_yn_13,
max_num_same_day_sites_14,
max_same_day_sites_tested_14,
max_same_day_sites_positive_yn_14,
max_num_same_day_sites_15,
max_same_day_sites_tested_15,
max_same_day_sites_positive_yn_15,
max_num_same_day_sites_16,
max_same_day_sites_tested_16,
max_same_day_sites_positive_yn_16,
max_num_same_day_sites_17,
max_same_day_sites_tested_17,
max_same_day_sites_positive_yn_17,
max_num_same_day_sites_18,
max_same_day_sites_tested_18,
max_same_day_sites_positive_yn_18,
total_chlamydia_tests_08,
total_chlamydia_tests_09,
total_chlamydia_tests_10,
total_chlamydia_tests_11,
total_chlamydia_tests_12,
total_chlamydia_tests_13,
total_chlamydia_tests_14,
total_chlamydia_tests_15,
total_chlamydia_tests_16,
total_chlamydia_tests_17,
total_chlamydia_tests_18,
positive_chlamydia_tests_08,
positive_chlamydia_tests_09,
positive_chlamydia_tests_10,
positive_chlamydia_tests_11,
positive_chlamydia_tests_12,
positive_chlamydia_tests_13,
positive_chlamydia_tests_14,
positive_chlamydia_tests_15,
positive_chlamydia_tests_16,
positive_chlamydia_tests_17,
positive_chlamydia_tests_18,
chlamydia_per_esp_08,
chlamydia_per_esp_09,
chlamydia_per_esp_10,
chlamydia_per_esp_11,
chlamydia_per_esp_12,
chlamydia_per_esp_13,
chlamydia_per_esp_14,
chlamydia_per_esp_15,
chlamydia_per_esp_16,
chlamydia_per_esp_17,
chlamydia_per_esp_18,
total_syphilis_tests_08,
total_syphilis_tests_09,
total_syphilis_tests_10,
total_syphilis_tests_11,
total_syphilis_tests_12,
total_syphilis_tests_13,
total_syphilis_tests_14,
total_syphilis_tests_15,
total_syphilis_tests_16,
total_syphilis_tests_17,
total_syphilis_tests_18,
syphilis_per_esp_08,
syphilis_per_esp_09,
syphilis_per_esp_10,
syphilis_per_esp_11,
syphilis_per_esp_12,
syphilis_per_esp_13,
syphilis_per_esp_14,
syphilis_per_esp_15,
syphilis_per_esp_16,
syphilis_per_esp_17,
syphilis_per_esp_18,
anal_cytology_test_ever,
hiv_per_esp_spec,
hiv_neg_truvada_10,
hiv_neg_truvada_11,
hiv_neg_truvada_12,
hiv_neg_truvada_13,
hiv_neg_truvada_14,
hiv_neg_truvada_15,
hiv_neg_truvada_16,
hiv_neg_truvada_17,
hiv_neg_truvada_18,
abnormal_anal_cytology_08,
abnormal_anal_cytology_gpos_08,
abnormal_anal_cytology_09,
abnormal_anal_cytology_gpos_09,
abnormal_anal_cytology_10,
abnormal_anal_cytology_gpos_10,
abnormal_anal_cytology_11,
abnormal_anal_cytology_gpos_11,
abnormal_anal_cytology_12,
abnormal_anal_cytology_gpos_12,
abnormal_anal_cytology_13,
abnormal_anal_cytology_gpos_13,
abnormal_anal_cytology_14,
abnormal_anal_cytology_gpos_14,
abnormal_anal_cytology_15,
abnormal_anal_cytology_gpos_15,
abnormal_anal_cytology_16,
abnormal_anal_cytology_gpos_16,
abnormal_anal_cytology_17,
abnormal_anal_cytology_gpos_17,
abnormal_anal_cytology_18,
abnormal_anal_cytology_gpos_18,
urethral_discharge_08,
urethral_discharge_gpos_08,
urethral_discharge_09,
urethral_discharge_gpos_09,
urethral_discharge_10,
urethral_discharge_gpos_10,
urethral_discharge_11,
urethral_discharge_gpos_11,
urethral_discharge_12,
urethral_discharge_gpos_12,
urethral_discharge_13,
urethral_discharge_gpos_13,
urethral_discharge_14,
urethral_discharge_gpos_14,
urethral_discharge_15,
urethral_discharge_gpos_15,
urethral_discharge_16,
urethral_discharge_gpos_16,
urethral_discharge_17,
urethral_discharge_gpos_17,
urethral_discharge_18,
urethral_discharge_gpos_18,
urethritis_08,
urethritis_gpos_08,
urethritis_09,
urethritis_gpos_09,
urethritis_10,
urethritis_gpos_10,
urethritis_11,
urethritis_gpos_11,
urethritis_12,
urethritis_gpos_12,
urethritis_13,
urethritis_gpos_13,
urethritis_14,
urethritis_gpos_14,
urethritis_15,
urethritis_gpos_15,
urethritis_16,
urethritis_gpos_16,
urethritis_17,
urethritis_gpos_17,
urethritis_18,
urethritis_gpos_18,
dysuria_08,
dysuria_gpos_08,
dysuria_09,
dysuria_gpos_09,
dysuria_10,
dysuria_gpos_10,
dysuria_11,
dysuria_gpos_11,
dysuria_12,
dysuria_gpos_12,
dysuria_13,
dysuria_gpos_13,
dysuria_14,
dysuria_gpos_14,
dysuria_15,
dysuria_gpos_15,
dysuria_16,
dysuria_gpos_16,
dysuria_17,
dysuria_gpos_17,
dysuria_18,
dysuria_gpos_18,
epididymitis_08,
epididymitis_gpos_08,
epididymitis_09,
epididymitis_gpos_09,
epididymitis_10,
epididymitis_gpos_10,
epididymitis_11,
epididymitis_gpos_11,
epididymitis_12,
epididymitis_gpos_12,
epididymitis_13,
epididymitis_gpos_13,
epididymitis_14,
epididymitis_gpos_14,
epididymitis_15,
epididymitis_gpos_15,
epididymitis_16,
epididymitis_gpos_16,
epididymitis_17,
epididymitis_gpos_17,
epididymitis_18,
epididymitis_gpos_18,
testicular_pain_08,
testicular_pain_gpos_08,
testicular_pain_09,
testicular_pain_gpos_09,
testicular_pain_10,
testicular_pain_gpos_10,
testicular_pain_11,
testicular_pain_gpos_11,
testicular_pain_12,
testicular_pain_gpos_12,
testicular_pain_13,
testicular_pain_gpos_13,
testicular_pain_14,
testicular_pain_gpos_14,
testicular_pain_15,
testicular_pain_gpos_15,
testicular_pain_16,
testicular_pain_gpos_16,
testicular_pain_17,
testicular_pain_gpos_17,
testicular_pain_18,
testicular_pain_gpos_18,
proctitis_rectal_pain_08,
proctitis_rectal_pain_gpos_08,
proctitis_rectal_pain_09,
proctitis_rectal_pain_gpos_09,
proctitis_rectal_pain_10,
proctitis_rectal_pain_gpos_10,
proctitis_rectal_pain_11,
proctitis_rectal_pain_gpos_11,
proctitis_rectal_pain_12,
proctitis_rectal_pain_gpos_12,
proctitis_rectal_pain_13,
proctitis_rectal_pain_gpos_13,
proctitis_rectal_pain_14,
proctitis_rectal_pain_gpos_14,
proctitis_rectal_pain_15,
proctitis_rectal_pain_gpos_15,
proctitis_rectal_pain_16,
proctitis_rectal_pain_gpos_16,
proctitis_rectal_pain_17,
proctitis_rectal_pain_gpos_17,
proctitis_rectal_pain_18,
proctitis_rectal_pain_gpos_18,
rectal_bleeding_08,
rectal_bleeding_gpos_08,
rectal_bleeding_09,
rectal_bleeding_gpos_09,
rectal_bleeding_10,
rectal_bleeding_gpos_10,
rectal_bleeding_11,
rectal_bleeding_gpos_11,
rectal_bleeding_12,
rectal_bleeding_gpos_12,
rectal_bleeding_13,
rectal_bleeding_gpos_13,
rectal_bleeding_14,
rectal_bleeding_gpos_14,
rectal_bleeding_15,
rectal_bleeding_gpos_15,
rectal_bleeding_16,
rectal_bleeding_gpos_16,
rectal_bleeding_17,
rectal_bleeding_gpos_17,
rectal_bleeding_18,
rectal_bleeding_gpos_18,
pharyngitis_08,
pharyngitis_gpos_08,
pharyngitis_09,
pharyngitis_gpos_09,
pharyngitis_10,
pharyngitis_gpos_10,
pharyngitis_11,
pharyngitis_gpos_11,
pharyngitis_12,
pharyngitis_gpos_12,
pharyngitis_13,
pharyngitis_gpos_13,
pharyngitis_14,
pharyngitis_gpos_14,
pharyngitis_15,
pharyngitis_gpos_15,
pharyngitis_16,
pharyngitis_gpos_16,
pharyngitis_17,
pharyngitis_gpos_17,
pharyngitis_18,
pharyngitis_gpos_18,
tonsillitis_08,
tonsillitis_gpos_08,
tonsillitis_09,
tonsillitis_gpos_09,
tonsillitis_10,
tonsillitis_gpos_10,
tonsillitis_11,
tonsillitis_gpos_11,
tonsillitis_12,
tonsillitis_gpos_12,
tonsillitis_13,
tonsillitis_gpos_13,
tonsillitis_14,
tonsillitis_gpos_14,
tonsillitis_15,
tonsillitis_gpos_15,
tonsillitis_16,
tonsillitis_gpos_16,
tonsillitis_17,
tonsillitis_gpos_17,
tonsillitis_18,
tonsillitis_gpos_18,
throat_pain_08,
throat_pain_gpos_08,
throat_pain_09,
throat_pain_gpos_09,
throat_pain_10,
throat_pain_gpos_10,
throat_pain_11,
throat_pain_gpos_11,
throat_pain_12,
throat_pain_gpos_12,
throat_pain_13,
throat_pain_gpos_13,
throat_pain_14,
throat_pain_gpos_14,
throat_pain_15,
throat_pain_gpos_15,
throat_pain_16,
throat_pain_gpos_16,
throat_pain_17,
throat_pain_gpos_17,
throat_pain_18,
throat_pain_gpos_18,
conjunctivitis_08,
conjunctivitis_gpos_08,
conjunctivitis_09,
conjunctivitis_gpos_09,
conjunctivitis_10,
conjunctivitis_gpos_10,
conjunctivitis_11,
conjunctivitis_gpos_11,
conjunctivitis_12,
conjunctivitis_gpos_12,
conjunctivitis_13,
conjunctivitis_gpos_13,
conjunctivitis_14,
conjunctivitis_gpos_14,
conjunctivitis_15,
conjunctivitis_gpos_15,
conjunctivitis_16,
conjunctivitis_gpos_16,
conjunctivitis_17,
conjunctivitis_gpos_17,
conjunctivitis_18,
conjunctivitis_gpos_18,
eye_pain_08,
eye_pain_gpos_08,
eye_pain_09,
eye_pain_gpos_09,
eye_pain_10,
eye_pain_gpos_10,
eye_pain_11,
eye_pain_gpos_11,
eye_pain_12,
eye_pain_gpos_12,
eye_pain_13,
eye_pain_gpos_13,
eye_pain_14,
eye_pain_gpos_14,
eye_pain_15,
eye_pain_gpos_15,
eye_pain_16,
eye_pain_gpos_16,
eye_pain_17,
eye_pain_gpos_17,
eye_pain_18,
eye_pain_gpos_18,
vaginitis_08,
vaginitis_gpos_08,
vaginitis_09,
vaginitis_gpos_09,
vaginitis_10,
vaginitis_gpos_10,
vaginitis_11,
vaginitis_gpos_11,
vaginitis_12,
vaginitis_gpos_12,
vaginitis_13,
vaginitis_gpos_13,
vaginitis_14,
vaginitis_gpos_14,
vaginitis_15,
vaginitis_gpos_15,
vaginitis_16,
vaginitis_gpos_16,
vaginitis_17,
vaginitis_gpos_17,
vaginitis_18,
vaginitis_gpos_18,
cervicitis_08,
cervicitis_gpos_08,
cervicitis_09,
cervicitis_gpos_09,
cervicitis_10,
cervicitis_gpos_10,
cervicitis_11,
cervicitis_gpos_11,
cervicitis_12,
cervicitis_gpos_12,
cervicitis_13,
cervicitis_gpos_13,
cervicitis_14,
cervicitis_gpos_14,
cervicitis_15,
cervicitis_gpos_15,
cervicitis_16,
cervicitis_gpos_16,
cervicitis_17,
cervicitis_gpos_17,
cervicitis_18,
cervicitis_gpos_18,
vaginal_leucorrhea_08,
vaginal_leucorrhea_gpos_08,
vaginal_leucorrhea_09,
vaginal_leucorrhea_gpos_09,
vaginal_leucorrhea_10,
vaginal_leucorrhea_gpos_10,
vaginal_leucorrhea_11,
vaginal_leucorrhea_gpos_11,
vaginal_leucorrhea_12,
vaginal_leucorrhea_gpos_12,
vaginal_leucorrhea_13,
vaginal_leucorrhea_gpos_13,
vaginal_leucorrhea_14,
vaginal_leucorrhea_gpos_14,
vaginal_leucorrhea_15,
vaginal_leucorrhea_gpos_15,
vaginal_leucorrhea_16,
vaginal_leucorrhea_gpos_16,
vaginal_leucorrhea_17,
vaginal_leucorrhea_gpos_17,
vaginal_leucorrhea_18,
vaginal_leucorrhea_gpos_18,
chlamydia_dx_08,
chlamydia_dx_gpos_08,
chlamydia_dx_09,
chlamydia_dx_gpos_09,
chlamydia_dx_10,
chlamydia_dx_gpos_10,
chlamydia_dx_11,
chlamydia_dx_gpos_11,
chlamydia_dx_12,
chlamydia_dx_gpos_12,
chlamydia_dx_13,
chlamydia_dx_gpos_13,
chlamydia_dx_14,
chlamydia_dx_gpos_14,
chlamydia_dx_15,
chlamydia_dx_gpos_15,
chlamydia_dx_16,
chlamydia_dx_gpos_16,
chlamydia_dx_17,
chlamydia_dx_gpos_17,
chlamydia_dx_18,
chlamydia_dx_gpos_18,
screening_for_stis_08,
screening_for_stis_gpos_08,
screening_for_stis_09,
screening_for_stis_gpos_09,
screening_for_stis_10,
screening_for_stis_gpos_10,
screening_for_stis_11,
screening_for_stis_gpos_11,
screening_for_stis_12,
screening_for_stis_gpos_12,
screening_for_stis_13,
screening_for_stis_gpos_13,
screening_for_stis_14,
screening_for_stis_gpos_14,
screening_for_stis_15,
screening_for_stis_gpos_15,
screening_for_stis_16,
screening_for_stis_gpos_16,
screening_for_stis_17,
screening_for_stis_gpos_17,
screening_for_stis_18,
screening_for_stis_gpos_18,
cont_or_exp_to_vd_08,
cont_or_exp_to_vd_gpos_08,
cont_or_exp_to_vd_09,
cont_or_exp_to_vd_gpos_09,
cont_or_exp_to_vd_10,
cont_or_exp_to_vd_gpos_10,
cont_or_exp_to_vd_11,
cont_or_exp_to_vd_gpos_11,
cont_or_exp_to_vd_12,
cont_or_exp_to_vd_gpos_12,
cont_or_exp_to_vd_13,
cont_or_exp_to_vd_gpos_13,
cont_or_exp_to_vd_14,
cont_or_exp_to_vd_gpos_14,
cont_or_exp_to_vd_15,
cont_or_exp_to_vd_gpos_15,
cont_or_exp_to_vd_16,
cont_or_exp_to_vd_gpos_16,
cont_or_exp_to_vd_17,
cont_or_exp_to_vd_gpos_17,
cont_or_exp_to_vd_18,
cont_or_exp_to_vd_gpos_18,
high_risk_sexual_behavior_08,
high_risk_sexual_behavior_gpos_08,
high_risk_sexual_behavior_09,
high_risk_sexual_behavior_gpos_09,
high_risk_sexual_behavior_10,
high_risk_sexual_behavior_gpos_10,
high_risk_sexual_behavior_11,
high_risk_sexual_behavior_gpos_11,
high_risk_sexual_behavior_12,
high_risk_sexual_behavior_gpos_12,
high_risk_sexual_behavior_13,
high_risk_sexual_behavior_gpos_13,
high_risk_sexual_behavior_14,
high_risk_sexual_behavior_gpos_14,
high_risk_sexual_behavior_15,
high_risk_sexual_behavior_gpos_15,
high_risk_sexual_behavior_16,
high_risk_sexual_behavior_gpos_16,
high_risk_sexual_behavior_17,
high_risk_sexual_behavior_gpos_17,
high_risk_sexual_behavior_18,
high_risk_sexual_behavior_gpos_18,
hiv_counseling_08,
hiv_counseling_gpos_08,
hiv_counseling_09,
hiv_counseling_gpos_09,
hiv_counseling_10,
hiv_counseling_gpos_10,
hiv_counseling_11,
hiv_counseling_gpos_11,
hiv_counseling_12,
hiv_counseling_gpos_12,
hiv_counseling_13,
hiv_counseling_gpos_13,
hiv_counseling_14,
hiv_counseling_gpos_14,
hiv_counseling_15,
hiv_counseling_gpos_15,
hiv_counseling_16,
hiv_counseling_gpos_16,
hiv_counseling_17,
hiv_counseling_gpos_17,
hiv_counseling_18,
hiv_counseling_gpos_18
FROM mgtp_output_with_patient T1
LEFT JOIN mgtp_ouput_pat_and_enc_counts T2
	ON T1.patient_id = T2.patient_id;
	
	
-- DENOMINATOR LINELIST CREATION
-- MALES ONLY
CREATE TABLE mgtp_denom_males  AS 
SELECT T1.patient_id, 
gender,
date_part('year', age('2018-12-31', date_of_birth)) age_end_of_2018, 
race,
ethnicity,
EXTRACT(YEAR FROM date)  rpt_year, date
FROM gen_pop_tools.clin_enc T1,
emr_patient T2
WHERE T1.patient_id = T2.id
AND gender IN ('M', 'MALE')
AND  T1.date >= '01-01-2010' 
AND T1.date < '01-01-2019'
AND date_part('year', age('2018-12-31', date_of_birth)) >= 15
AND (center_id not in ('1') or center_id IS NULL or center_id = '')
GROUP BY T1.patient_id, gender, race, ethnicity, date_of_birth, date;


-- ENCOUNTERS - Counts & Column Creation
CREATE TABLE mgtp_denom_male_counts  AS 
SELECT null::integer masked_patient_id,
--T1.patient_id,
gender,
age_end_of_2018,
race,
ethnicity,
count(CASE WHEN rpt_year = '2010' THEN 1 END)  t_encounters_10,
count(CASE WHEN rpt_year = '2011' THEN 1 END)  t_encounters_11,
count(CASE WHEN rpt_year = '2012' THEN 1 END)  t_encounters_12,
count(CASE WHEN rpt_year = '2013' THEN 1 END)  t_encounters_13,
count(CASE WHEN rpt_year = '2014' THEN 1 END)  t_encounters_14,
count(CASE WHEN rpt_year = '2015' THEN 1 END)  t_encounters_15, 
count(CASE WHEN rpt_year = '2016' THEN 1 END)  t_encounters_16,
count(CASE WHEN rpt_year = '2017' THEN 1 END)  t_encounters_17,
count(CASE WHEN rpt_year = '2018' THEN 1 END)  t_encounters_18
FROM mgtp_denom_males T1 
GROUP BY T1.patient_id, gender, age_end_of_2018, race, ethnicity;

-- FOR OUTPUT
-- SELECT * FROM mgtp_report_final_output;
--SELECT * FROM mgtp_denom_male_counts;

	
	

-- CREATE TABLE mgtp_report_final_output_agetestpat_filter AS
-- SELECT * FROM mgtp_report_final_output
-- WHERE age >= 15 
-- AND master_patient_id not in 
-- (4856539,
-- 2388755,
-- 20021289,
-- 2622639,
-- 7393799,
-- 7405902,
-- 19086,
-- 2245972,
-- 9345414,
-- 1121717,
-- 1120095,
-- 5092053,
-- 1119941,
-- 15810936,
-- 2801547,
-- 7395105,
-- 6808599,
-- 467766,
-- 465618,
-- 9345404,
-- 24120559,
-- 20257460,
-- 6840308,
-- 467872,
-- 4512833,
-- 454136,
-- 8429720,
-- 8000774,
-- 377556,
-- 456070,
-- 8012787,
-- 7441382,
-- 7997968,
-- 3268327,
-- 3268319,
-- 3268323,
-- 8726972,
-- 9645683,
-- 456988,
-- 7395153,
-- 4476409,
-- 1400172,
-- 269278,
-- 5044824,
-- 5092208,
-- 269284,
-- 269622,
-- 269290,
-- 5689686,
-- 5651871,
-- 269282,
-- 269276,
-- 5042048,
-- 6272750,
-- 299112,
-- 269286,
-- 269288,
-- 5044808,
-- 291608,
-- 2733840,
-- 269280,
-- 4476411,
-- 8425284,
-- 7394534,
-- 2733842,
-- 5092285,
-- 7394532,
-- 3906359,
-- 1629211,
-- 7394546,
-- 7394548,
-- 5638755,
-- 3069021,
-- 7394544,
-- 16697245,
-- 16697270,
-- 16700804,
-- 16803981);

--
-- Script shutdown section 
--

-- DROP TABLE IF EXISTS mgtp_hef_w_lab_details CASCADE;
-- DROP TABLE IF EXISTS mgtp_cases_of_interest CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_events_pre CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_events CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_labs CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_syph_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_chlam_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_gon_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_hepb_pos_dna CASCADE;
-- DROP TABLE IF EXISTS mgtp_cpts_of_interest CASCADE;
-- DROP TABLE IF EXISTS mgtp_dx_codes CASCADE;
-- DROP TABLE IF EXISTS mgtp_diag_lab_window CASCADE;
-- DROP TABLE IF EXISTS mgtp_diag_fields_by_year CASCADE;
-- DROP TABLE IF EXISTS mgtp_diag_fields CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_cases CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_meds CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_array CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_2mogap CASCADE;
-- DROP TABLE IF EXISTS mgtp_truvada_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_all_details CASCADE;
-- DROP TABLE IF EXISTS mgtp_hiv_all_details_values CASCADE;
-- DROP TABLE IF EXISTS mgtp_spec_source_count CASCADE;
-- DROP TABLE IF EXISTS mgtp_spec_source_max CASCADE;
-- DROP TABLE IF EXISTS mgtp_spec_source_multisite CASCADE;
-- DROP TABLE IF EXISTS mgtp_multisite_data CASCADE;
-- DROP TABLE IF EXISTS mgtp_index_patients CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_part_1 CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_with_patient CASCADE;
-- DROP TABLE IF EXISTS mgtp_output_pat_and_enc CASCADE;
-- DROP TABLE IF EXISTS mgtp_ouput_pat_and_enc_counts CASCADE;
-- DROP TABLE IF EXISTS mgtp_denom_males CASCADE;

















