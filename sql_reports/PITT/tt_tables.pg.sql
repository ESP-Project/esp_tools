﻿/*--------------------------------------------------------------------------------
--
--                                ESP Health
--                             TT Monthly Tables
--
--------------------------------------------------------------------------------
--
-- @author: Bob Zambarano <bzambarano@commoninf.com>
-- @organization: Commonwealth Informatics <http://www.commoninf.com>
-- @contact: http://esphealth.org
-- @copyright: (c) 2016 Commonwealth Informatics
-- @license: LGPL 3.0 - http://www.gnu.org/licenses/lgpl-3.0.txt
--
--------------------------------------------------------------------------------
--
-- This query contains some PostgreSQL-specific functions.  It will 
-- not run on other RDBMS without porting.
--
--------------------------------------------------------------------------------*/
set search_path to gen_pop_tools, public;
---
-- Build the mapping table.  If your local site has different values, please add them here
---

drop table if exists tt_racemap;
create table tt_racemap
  (source_field  varchar(65),
   source_value  varchar(100),
   target_value  varchar(100),
   primary key (source_field, source_value)); --primary key enforces unique mapping
--add any local race values to be mapped to the tt set here.
insert into tt_racemap
(source_field, source_value, target_value) values
('ethnicity','Y','HISPANIC'),
('ethnicity','HISPANIC/LATINO','HISPANIC'),
('race','American Indian / Alaska Native','OTHER'),
('race','American Indian/Alaska Native','OTHER'),
('race','Asian','ASIAN'),
('race','Black / African American','BLACK'),
('race','Black/African American','BLACK'),
('race','More than One Race','OTHER'),
('race','Native Hawaiian','OTHER'),
('race','Other Pacific Islander','OTHER'),
('race','Pacific Islander','OTHER'),
('race','White','CAUCASIAN'),
('race','NATIVE HAWAI','OTHER'),
('race','ALASKAN','OTHER'),
('race','ASIAN','ASIAN'),
('race','CAUCASIAN','CAUCASIAN'),
('race','INDIAN','ASIAN'),
('race','NAT AMERICAN','OTHER'),
('race','HISPANIC','HISPANIC'),
('race','OTHER','OTHER'),
('race','PACIFIC ISLANDER/HAWAIIAN','OTHER'),
('race','AMERICAN INDIAN/ALASKAN NATIVE','OTHER'),
('race','WHITE','CAUCASIAN'),
('race','BLACK','BLACK');

--
-- now build patient table
--
select 'Starting to build tt_pat at: ',now();
drop table if exists tt_pat;
create table tt_pat as
SELECT 
  pat.id AS patient_id, date_part('year', age(pat.date_of_birth::date)) as age, upper(substr(pat.gender,1,1)) gender, 
  date_part('year',date_of_birth::date) birth_year,
  case 
     when (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=pat.ethnicity) is not null
	   then (select target_value from tt_racemap t00 where t00.source_field='ethnicity' and t00.source_value=pat.ethnicity)
	 else (select target_value from tt_racemap t00 where t00.source_field='race' and t00.source_value=pat.race)
  end as race, -- pat.race as old_race, pat.ethnicity, --uncomment this for QA
  pat.date_of_death, pat.date_of_birth,
  case
    when substring(pat.zip,6,1)='-' then substring(pat.zip,1,5)
    else pat.zip
  end as zip
FROM emr_patient pat;
alter table tt_pat add primary key (patient_id);
analyze tt_pat;
--
-- Build the patient_encounter list
---
select 'Starting to build tt_enc_pat at: ',now();
drop table if exists tt_enc_pat;
create table tt_enc_pat as
select patient_id, 
       to_char(date,'yyyy_mm') year_month, 
       count(*) as counts
from gen_pop_tools.clin_enc
group by patient_id, to_char(date,'yyyy_mm');
alter table tt_enc_pat add primary key (patient_id, year_month);
analyze tt_enc_pat;
drop table if exists tt_1stenc_pat;
create table tt_1stenc_pat as
select patient_id, min(year_month) as year_month
from tt_enc_pat
group by patient_id;
alter table tt_1stenc_pat add primary key (patient_id);
analyze tt_1stenc_pat;
--
--utilty table with all months in seqence to current date;
--
drop table if exists tt_month_series;
create table tt_month_series as 
select year_month from
(select to_char(generate_series('2010-01-01'::timestamp,current_timestamp,'1 month')::date,'YYYY_MM') as year_month) t0
where year_month <= to_char(now() - interval '1 month','YYYY_MM');
--Since we will be running this on at least a lag of 2 days, and we only want full months of data, the current date will never represent a full month.
alter table tt_month_series add primary key (year_month); 
analyze tt_month_series;
--
-- pat_seq table with age at each year_month
--
drop table if exists tt_pat_seq;
create table tt_pat_seq as
select ser.year_month, p1st.patient_id
from tt_month_series ser
join tt_1stenc_pat p1st on p1st.year_month<=ser.year_month;
alter table tt_pat_seq add primary key (patient_id, year_month);
--now pat_seq_enc, which adds count of encounters and age at each month
drop table if exists tt_pat_seq_enc;
create table tt_pat_seq_enc as
select pat.patient_id, seq.year_month, 
       date_part('year', age(to_date(seq.year_month,'yyyy_mm'),pat.date_of_birth::date)) as age,
       sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month range between unbounded preceding and current row) as allprior,
	   sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 23 preceding and current row) as prior2,
	   sum(eseq.counts) over (partition by pat.patient_id order by seq.year_month rows between 11 preceding and current row) as prior1
from tt_pat pat
join tt_pat_seq seq on pat.patient_id=seq.patient_id
left join tt_enc_pat eseq on seq.patient_id=eseq.patient_id and seq.year_month=eseq.year_month;
--that was a left join of the encounters, so if there are no prior1 or prior2 counts, you get null values.
alter table tt_pat_seq_enc add primary key (patient_id, year_month);
analyze tt_pat_seq_enc;
--
-- BMI - by month for patients age >=12 on detection date 
--
SELECT 'Starting to build tt_bmi at: ',now();
DROP TABLE IF EXISTS tt_bmi;
CREATE TABLE tt_bmi AS
  SELECT pat.id patient_id
   , to_char('1960-01-01'::date + cond.date,'yyyy_mm')  year_month, 
  max(case when condition = 'BMI <25' then 1
       WHEN condition = 'BMI >=25 and <30' THEN 2
       WHEN condition = 'BMI >= 30' THEN 3
       WHEN condition = 'No Measured BMI' THEN 0
  END) bmi
  , max(to_char('1960-01-01'::date + cond.date,'yyyy_mm')) over (partition by pat.id) max_ym
FROM esp_condition cond
JOIN public.emr_patient pat ON (cond.patid = pat.natural_key)
WHERE condition ilike '%bmi%' AND age_at_detect_year >= 12
group by pat.id, to_char('1960-01-01'::date + cond.date,'yyyy_mm');  
--
-- BMI PCT 
--

select 'Starting to build tt_bmi_pct at: ',now();
drop table if exists tt_bmi_pct;
create table tt_bmi_pct as
         select enc.patient_id
        , max(gen_pop_tools.cdc_bmi((extract(year from age(enc.date, pat.date_of_birth::date))*12
                               + extract(month from age(enc.date, pat.date_of_birth::date)))::numeric,
                (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar,
                null::numeric,
                null::numeric,
                enc.bmi::numeric,
                'BMIPCT'::varchar )) as bmipct
		, to_char(enc.date,'YYYY_MM') as year_month
	    , max(to_char(enc.date,'YYYY_MM')) over (partition by enc.patient_id) max_ym
from emr_encounter enc
JOIN emr_patient pat on pat.id=enc.patient_id
--where enc.raw_encounter_type <> 'HISTORY'
where (enc.weight>0 or enc.height>0
                       or enc.bp_systolic>0 or enc.bp_diastolic>0
                       or enc.temperature>0 or enc.pregnant=TRUE or enc.edd is not null)
        or exists (select null from emr_encounter_dx_codes dx
                  where dx.encounter_id=enc.id and dx.dx_code_id<>'icd9:799.9')
GROUP BY enc.patient_id, to_char(enc.date,'YYYY_MM');
--
-- pregnancy by month
--
select 'Starting to build tt_preg1 at: ',now();
drop table if exists tt_preg1;
create table tt_preg1 as
SELECT seq.patient_id
	, seq.year_month
	, max(1) recent_pregnancy
    , max(seq.year_month) over (partition by seq.patient_id) max_ym
FROM hef_timespan span
JOIN tt_pat_seq_enc seq on seq.patient_id=span.patient_id 
     and to_date(seq.year_month,'yyyy_mm') + interval '1 month' > span.start_date
	 and to_date(seq.year_month,'yyyy_mm') + interval '1 month' < case 
	                             when span.end_date is not null then span.end_date
				     when span.start_date + interval '10 months' > now() then now()
				   end
GROUP BY seq.patient_id, seq.year_month;
--
-- Recent Gestational diabetes 1 year
--
select 'Starting to build tt_gdm1 at: ',now();
drop table if exists tt_gdm1;
create table tt_gdm1 as
SELECT seq.patient_id
    , seq.year_month       
	, max(1) AS gdm
    , max(seq.year_month) over (partition by seq.patient_id) max_ym
FROM nodis_case c
JOIN nodis_case_timespans nct ON nct.case_id = c.id
JOIN hef_timespan ts ON nct.timespan_id = ts.id
JOIN tt_pat_seq_enc seq on seq.patient_id=ts.patient_id 
     and to_date(seq.year_month,'yyyy_mm') + interval '1 month' > ts.start_date
	 and to_date(seq.year_month,'yyyy_mm') + interval '1 month' < case 
	                            when ts.end_date is not null then ts.end_date
	                            when ts.start_date + interval '10 months' > now() then now()
				  end
WHERE c.condition = 'diabetes:gestational' 
group by seq.patient_id, seq.year_month;
--
-- Max blood pressure 
--
select 'Starting to build tt_enc_bp at: ',now();
drop table if exists tt_enc_bp;
create table tt_enc_bp as
select patient_id, date, bp_systolic, bp_diastolic, (2*bp_diastolic + bp_systolic)/3 as mean_arterial, height
from emr_encounter e
where bp_systolic is not null and bp_diastolic is not null ;
create index tt_enc_bp_idx
on tt_enc_bp (patient_id, date);
analyze tt_enc_bp;
--
-- Max blood pressure using max mean aterial pressure for the period
--
select 'Starting to build tt_bp1 at: ',now();
drop table if exists tt_bp1;
create table tt_bp1 as
select * from (
	SELECT 
	  t0.patient_id
	, t0.bp_systolic AS max_bp_systolic
	, t0.bp_diastolic AS max_bp_diastolic
    , t1.max_mean_arterial as map
    , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_systolic::numeric, 
					 'SYS'::varchar, 
					 'BPPCT'::varchar) as syspct
    , gen_pop_tools.NHBP(extract(year from age(t0.date, pat.date_of_birth::date))::numeric, 
                     upper(substr(pat.gender,1,1)), 
                     gen_pop_tools.cdc_hgt((extract(year from age(t0.date, pat.date_of_birth::date))*12 + 
                                            extract(month from age(t0.date, pat.date_of_birth::date)))::numeric, 
			         (case when upper(substr(pat.gender,1,1))='M' then '1' when upper(substr(pat.gender,1,1))='F' then 2 else null end)::varchar, 
					 t0.height::numeric, 
					 'HTZ'::varchar ), 
					 t0.bp_diastolic::numeric, 
					 'DIA'::varchar, 
					 'BPPCT'::varchar) as diapct
    , row_number() over (partition by t0.patient_id order by t0.date desc) as rownum
    , t1.year_month
    , max(t1.year_month) over (partition by t1.patient_id) max_ym
	FROM tt_enc_bp t0,
         (select seq.patient_id, seq.year_month, max(encbp.mean_arterial) as max_mean_arterial  
              from tt_enc_bp encbp 
			  join tt_pat_seq_enc seq on seq.patient_id=encbp.patient_id and to_char(encbp.date,'yyyy_mm')=seq.year_month
              group by seq.patient_id, seq.year_month) as t1,
          emr_patient as pat
	WHERE to_char(t0.date,'yyyy_mm')=t1.year_month 
	and t0.mean_arterial = t1.max_mean_arterial and t0.patient_id=t1.patient_id
        and t0.patient_id=pat.id) as t
        where rownum=1;
--
-- lab subset
--
select 'Starting to build tt_sublab at: ',now();
drop table if exists tt_sublab;
create table tt_sublab as
select patient_id, date, result_float, t1.test_name
from emr_labresult t0
  inner join conf_labtestmap t1
    on t1.native_code=t0.native_code 
where t1.test_name in ('a1c','cholesterol-ldl','triglycerides','hepatitis_c_rna','hepatitis_c_elisa',
                       'rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf') 
  and (t0.result_float is not null or t1.test_name in ('hepatitis_c_rna','hepatitis_c_elisa',
                       'rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf'));
create index tt_sublab_patid_idx on tt_sublab (patient_id);
create index tt_sublab_testname_idx on tt_sublab (test_name);
create index tt_sublab_date_idx on tt_sublab (date);
analyze tt_sublab;
--
-- Max ldl lab result
--
select 'Starting to build tt_ldl1 at: ',now();
drop table if exists tt_ldl1;
create table tt_ldl1 as
	SELECT 
	  l0.patient_id
	, to_char(date,'yyyy_mm') year_month
	, MAX(l0.result_float) AS max_ldl1
	, max(to_char(date,'yyyy_mm')) over (partition by l0.patient_id) max_ym
	FROM tt_sublab l0
	WHERE l0.test_name = 'cholesterol-ldl'
	GROUP BY l0.patient_id, to_char(date,'yyyy_mm');
--
-- Max trig lab result last year
--
select 'Starting to build tt_trig1 at: ',now();
drop table if exists tt_trig1;
create table tt_trig1 as
	SELECT 
	  l0.patient_id
	, to_char(date,'yyyy_mm') year_month
	, MAX(l0.result_float) AS max_trig1
	, max(to_char(date,'yyyy_mm')) over (partition by l0.patient_id) max_ym
	FROM tt_sublab l0
	WHERE l0.test_name = 'triglycerides'
	GROUP BY 
	  l0.patient_id, to_char(date,'yyyy_mm');
--
-- Max A1C lab result last year
--
select 'Starting to build tt_a1c1 at: ',now();
drop table if exists tt_a1c1;
create table tt_a1c1 as
	SELECT 
	  l0.patient_id
	, to_char(date,'yyyy_mm') year_month
	, MAX(l0.result_float) AS max_a1c1
	, max(to_char(date,'yyyy_mm')) over (partition by l0.patient_id) max_ym
	FROM tt_sublab l0
	WHERE l0.test_name = 'a1c'
	GROUP BY 
	  l0.patient_id, to_char(date,'yyyy_mm');
--
-- Prediabetes
--
select 'Starting to build tt_predm at: ',now();
drop table if exists tt_predm;
create table tt_predm as
	SELECT max(1) predm
	, c.patient_id
	, seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
	FROM nodis_case c
	join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
	join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive then current_date
               end enddt 
	    from nodis_caseactivehistory h 
		join nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>to_date(seq.year_month,'yyyy_mm')
	WHERE c.condition = 'diabetes:prediabetes' and cah.status in ('I','R')
	group by c.patient_id, seq.year_month;
--
-- Type 1 Diabetes
--
select 'Starting to build tt_type1 at: ',now();
drop table if exists tt_type1;
create table tt_type1 as
	SELECT 
	patient_id
	, to_char(date,'yyyy_mm') year_month
	, max(1) AS type_1_diabetes
	FROM nodis_case
	WHERE condition = 'diabetes:type-1'
	group by patient_id, to_char(date,'yyyy_mm');
--
-- Type 2 Diabetes
--
select 'Starting to build tt_type2 at: ',now();
drop table if exists tt_type2;
create table tt_type2 as
	SELECT max(1) type2
	, c.patient_id
	, seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
	FROM nodis_case c
	join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
	join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive then current_date
               end enddt 
	    from nodis_caseactivehistory h 
		join nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>to_date(seq.year_month,'yyyy_mm')
	WHERE c.condition = 'diabetes:type-2' and cah.status in ('I','R')
	group by c.patient_id, seq.year_month;
--
-- Insulin
--    Prescription for insulin within the previous year
--
select 'Starting to build tt_insulin at: ',now();
drop table if exists tt_insulin;
create table tt_insulin as
	SELECT 
	patient_id
	, max(1) AS insulin
	, to_char(date,'yyyy_mm') year_month
	FROM emr_prescription
	WHERE name ILIKE '%insulin%'
	group by patient_id, to_char(date,'yyyy_mm');
--
-- Metformin
--     Prescription for metformin within the previous year
--
select 'Starting to build tt_metformin at: ',now();
drop table if exists tt_metformin;
create table tt_metformin as 
	SELECT 
	  patient_id
	, to_char(date,'yyyy_mm') year_month
	, max(1) AS metformin
	FROM emr_prescription
	WHERE name ILIKE '%metformin%'
	group by patient_id, to_char(date,'yyyy_mm');
--
-- Influenza vaccine
--     Prescription for influenza vaccine current flu season
--
select 'Starting to build tt_flu_cur at: ',now();
drop table if exists tt_flu_cur;
create table tt_flu_cur as
	SELECT 
	  patient_id
	, to_char(date,'yyyy_mm') year_month
	, max(1) AS influenza_vaccine
	FROM emr_immunization
	WHERE (name ILIKE '%influenza%' or name = 'flu')
	group by patient_id, to_char(date,'yyyy_mm');
--
-- tdap vaccine
--
select 'Starting to build tt_tdap at: ',now();
drop table if exists tt_tdap_pre0;
create table tt_tdap_pre0 as
	SELECT 
	  i.patient_id
	, to_char(i.date,'yyyy_mm') year_month
	, max(1) AS tdap_vaccine
	FROM emr_immunization i
	WHERE (i.name ILIKE '%tdap%')
	group by i.patient_id, to_char(i.date,'yyyy_mm');
drop table if exists tt_tdap_pre;
create table tt_tdap_pre as
select t0.*
      , date_part('year',age(to_date(t0.year_month,'yyyy_mm'),p.date_of_birth::date)) age
from tt_tdap_pre0 t0 join emr_patient p on p.id=t0.patient_id;
drop table if exists tt_tdap;
create table tt_tdap as
select tdapp.patient_id, pse.year_month, max(tdapp.tdap_vaccine) tdap_vaccine
  from gen_pop_tools.tt_pat_seq_enc pse
  join gen_pop_tools.tt_tdap_pre tdapp on pse.patient_id=tdapp.patient_id 
    and ((pse.year_month>=tdapp.year_month and tdapp.age<=18 and pse.age<=18) 
         or (pse.year_month>=tdapp.year_month and tdapp.age>18 and pse.age>18)) 
  group by tdapp.patient_id, pse.year_month;
--
-- Smoking
create table tt_smoking as
select case when t1.latest=1 then '4'
               when t2.yesOrQuit='QUIT' then '3'
               when t3.passive='PASSIVE' then '2'
               when t4.never='NEVER' then '1'
           end as smoking,
           seq.patient_id,
                   seq.year_month
               , max(seq.year_month) over (partition by seq.patient_id) max_ym
from tt_pat_seq_enc seq
left join (select max(case when upper(tobacco_use) = 'YES' or tobacco_use in
                ('Y','Current','X','Yes. .','precontemplative','counseled','current','one',
                          'Very Ready','Post Partum','ready','contemplative','Yes. mom. .',
                          'Yes. outside. .','relapse','occasional','rare',
                          'Yes. father. .','Still smoking.','Yes. outside only. .','Yes. mother. .',
                          'socially','Cessation discussed.','social','Avoid complications',
                          'feels addicted, other','Have cut back','rarely','HEAVY TOBACCO SMOKER',
                          'LIGHT TOBACCO SMOKER','CURRENT EVERY DAY SMOKER','CURRENT SOME DAY SMOKER','SMOKER, CURRENT STATUS UNKNOWN')
          or tobacco_use similar to '(improve|stress|<|>|More than|Yes|occ|Some|roll|Continue|less|Current|Still|social|half|one|pack)%'
          or tobacco_use ~ '^[0123456789]' or tobacco_use ~ '^.[0123456789]'
          or tobacco_use ilike '%smokes%'
          or tobacco_use similar to '%(cut down|continues|almost|ready to|cessation|addicted|current|contemplat)%'
        then 1 else 0 end) as latest,
                patient_id,
                to_char(date,'yyyy_mm') year_month
     from emr_socialhistory
     where tobacco_use is not null and tobacco_use<>''
     group by patient_id, to_char(date,'yyyy_mm') ) t1 on t1.patient_id=seq.patient_id and t1.year_month=seq.year_month
left outer join (select max(val) as yesOrQuit, patient_id,
      to_char(date,'yyyy_mm') year_month
      from (select 'QUIT'::text as val, patient_id, date
            from emr_socialhistory where upper(tobacco_use) in ('YES','QUIT') or tobacco_use in
                          ('Y','Current','X','Yes. .','precontemplative','counseled','current','one',
                          'Very Ready','Post Partum','ready','contemplative','Yes. mom. .',
                          'Yes. outside. .','relapse','occasional','rare',
                          'Yes. father. .','Still smoking.','Yes. outside only. .','Yes. mother. .',
                          'socially','Cessation discussed.','social','Avoid complications',
                          'feels addicted, other','Have cut back','rarely', 'FORMER SMOKER')
                        or tobacco_use similar to '(quit|Quit|Previous|Used to|Former)%'
                        or tobacco_use in ('Quit')
                        or tobacco_use similar to '(improve|stress|<|>|More than|Yes|occ|Some|roll|Continue|less|Current|Still|social|half|     
                        one|pack)%'
                 or tobacco_use ~ '^[0123456789]'
                 or tobacco_use ~ '^.[0123456789]'
                 or tobacco_use ilike '%smokes%'
                 or tobacco_use similar to '%(cut down|continues|almost|ready to|cessation|addicted|current|contemplat)%') t00
            group by patient_id, to_char(date,'yyyy_mm')) t2 on seq.patient_id=t2.patient_id and t2.year_month=seq.year_month
left outer join (select max(val) as passive, patient_id,
      to_char(date,'yyyy_mm') year_month
      from (select 'PASSIVE'::text as val, patient_id, date
            from emr_socialhistory where upper(tobacco_use) in ('PASSIVE','PASSIVE SMOKE EXPOSURE - NEVER SMOKER')) t00
            group by patient_id, to_char(date,'yyyy_mm') ) t3 on seq.patient_id=t3.patient_id and seq.year_month=t3.year_month
left outer join (select max(val) as never, patient_id,
      to_char(date,'yyyy_mm') year_month
      from (select 'NEVER'::text as val, patient_id, date
            from emr_socialhistory
                        where upper(tobacco_use) ='NEVER' or tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .',
              'none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None',
              'Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.','NEVER SMOKER','Non smoker.','nonsmoker')) t00
            group by patient_id, to_char(date,'yyyy_mm') ) t4 on seq.patient_id=t4.patient_id and seq.year_month=t4.year_month
where t1.latest=1 or t2.yesOrQuit='QUIT' or t3.passive='PASSIVE' or t4.never='NEVER';

--
-- Asthma
--
select 'Starting to build tt_asthma at: ',now();
drop table if exists tt_asthma;
create table tt_asthma as
  select max(1) asthma,
  c.patient_id,
  seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
from nodis_case c
  join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
  join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive then current_date
               end enddt 
	    from nodis_caseactivehistory h 
		join nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>to_date(seq.year_month,'yyyy_mm')
where c.condition='asthma' and cah.status in ('I','R')
group by c.patient_id, seq.year_month;
--
-- events for chlamydia and gonnorhea
--
select 'Starting to build tt_subevents at: ',now();
drop table if exists tt_subevents;
create table tt_subevents as
    select name, patient_id, to_char(date, 'yyyy_mm') year_month
	from hef_event
	where name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative', 'lx:gonorrhea:indeterminate',
	               'lx:chlamydia:positive', 'lx:chlamydia:negative', 'lx:chlamydia:indeterminate');
--
-- most recent chlamydia lab result
--
select 'Starting to build tt_chlamydia at: ',now();
drop table if exists tt_chlamydia;
create table tt_chlamydia as
	SELECT 
	  e0.patient_id
	, e0.year_month
	, max(case when e0.name='lx:chlamydia:indeterminate' then 1
               when e0.name='lx:chlamydia:negative' then 2
               when e0.name='lx:chlamydia:positive' then 3
          end) recent_chlamydia
	, max(e0.year_month) over (partition by e0.patient_id) max_ym
	FROM tt_subevents e0
	WHERE e0.name in ('lx:chlamydia:positive', 'lx:chlamydia:negative', 'lx:chlamydia:indeterminate')
	GROUP BY e0.patient_id, e0.year_month;
--
-- Depression
--
select 'Starting to build tt_depression at: ',now();
drop table if exists tt_depression;
create table tt_depression as
  select max(1) depression,
  c.patient_id,
  seq.year_month
	, max(seq.year_month) over (partition by c.patient_id) max_ym
from nodis_case c
  join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
  join (select h.case_id, h.status, h.date as strtdt, 
               case 
			     when lead(h.date) over (partition by h.case_id order by h.date) is not null 
                    then lead(h.date) over (partition by h.case_id order by h.date)
				 when c.isactive then current_date
               end enddt 
	    from nodis_caseactivehistory h 
		join nodis_case c on c.id=h.case_id) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>to_date(seq.year_month,'yyyy_mm')
where c.condition='depression' and cah.status in ('I','R')
group by c.patient_id, seq.year_month;
--
-- Opioid
--
select 'Starting to build tt_opi at: ',now();
drop table if exists gen_pop_tools.tt_opioidrx;
CREATE TABLE gen_pop_tools.tt_opioidrx  AS 
select t1.patient_id, t1.year_month
  , max(1) opioidrx  
from esp_condition t0
join emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
     and date_trunc('month', t0.cond_start_date)::date  <= to_date(t1.year_month,'yyyy_mm')
     and date_trunc('month', t0.cond_expire_date)::date >= to_date(t1.year_month,'yyyy_mm')
where t0.condition='opioidrx'
group by t1.patient_id, t1.year_month;

--simple identification of benzo prescription months per patient
drop table if exists gen_pop_tools.tt_benzorx;
CREATE TABLE gen_pop_tools.tt_benzorx  AS 
select t1.patient_id, t1.year_month
  , max(1) benzorx  
from esp_condition t0
join emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
     and date_trunc('month', t0.cond_start_date)::date  <= to_date(t1.year_month,'yyyy_mm')
     and date_trunc('month', t0.cond_expire_date)::date >= to_date(t1.year_month,'yyyy_mm')
where t0.condition='benzodiarx'
group by t1.patient_id, t1.year_month;

--months with overlapping benzo-opioid prescriptions per patient
drop table if exists gen_pop_tools.tt_opibenzorx;
CREATE TABLE gen_pop_tools.tt_opibenzorx  AS 
select t1.patient_id, t1.year_month
  , max(1) opibenzorx  
from esp_condition t0
join emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
     and date_trunc('month', t0.cond_start_date)::date  <= to_date(t1.year_month,'yyyy_mm')
     and date_trunc('month', t0.cond_expire_date)::date >= to_date(t1.year_month,'yyyy_mm')
where t0.condition='benzopiconcurrent'
group by t1.patient_id, t1.year_month;

--months with high opioid prescriptions (> 100 for > 90 days)
drop table if exists gen_pop_tools.tt_highopi;
create table gen_pop_tools.tt_highopi as
select t1.patient_id, t1.year_month
  , max(1) highopi  
from esp_condition t0
join emr_patient t00 on t0.patid = t00.natural_key
join  gen_pop_tools.tt_pat_seq t1
  on t1.patient_id=t00.id 
     and date_trunc('month', t0.cond_start_date)::date  <= to_date(t1.year_month,'yyyy_mm')
     and date_trunc('month', t0.cond_expire_date)::date >= to_date(t1.year_month,'yyyy_mm')
where t0.condition='highopioiduse'
group by t1.patient_id, t1.year_month;
--
-- most recent gonorrhea lab result
--
select 'Starting to build tt_gonorrhea at: ',now();
drop table if exists tt_gonorrhea;
create table tt_gonorrhea as
		SELECT 
	  e0.patient_id
	, e0.year_month
	, max(case when e0.name='lx:gonorrhea:indeterminate' then 1
               when e0.name='lx:gonorrhea:negative' then 2
               when e0.name='lx:gonorrhea:positive' then 3
          end) recent_gonorrhea
	, max(e0.year_month) over (partition by e0.patient_id) max_ym
	FROM tt_subevents e0
	WHERE e0.name in ('lx:gonorrhea:positive', 'lx:gonorrhea:negative', 'lx:gonorrhea:indeterminate')
	GROUP BY e0.patient_id, e0.year_month;
--
-- Hypertension
--
select 'Starting to build tt_hypertension at: ',now();
drop table if exists tt_hypertension;
create table tt_hypertension as
    select patient_id, year_month,
	  case when hypertension = '3' then '1' else hypertension end as hypertension 
	, max(year_month) over (partition by patient_id) max_ym
from (select max(
        case
          when cah.status in ('I','R') then '3'
          when cah.status = 'D' then '2'
          else '0'
        end) as hypertension,
        c.patient_id,
        seq.year_month
     from nodis_case c
     join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
     join (select case_id, status, date as strtdt, 
               case 
                   when lead(date) over (partition by case_id order by date) is not null 
                       then lead(date) over (partition by case_id order by date)
                   else now()
               end enddt 
	    from nodis_caseactivehistory) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>=to_date(seq.year_month,'yyyy_mm')+interval '1 month'
where c.condition='hypertension' 
group by c.patient_id, seq.year_month) t0;
--
-- Diagnosed Hypertension
--
select 'Starting to build tt_diaghypert at: ',now();
drop table if exists tt_diaghypert;
create table tt_diaghypert as
    select patient_id, year_month,
	  case 
          when hypertension = '3' then '1' 
          when hypertension = '1' then '3'
          else hypertension
      end as hypertension 
	, max(year_month) over (partition by patient_id) max_ym
from (select max(
        case
          when cah.status ='C' then '3'
          when cah.status = 'U' then '2'
          when cah.status = 'UK' then '1'
          else '0'
        end) as hypertension,
        c.patient_id,
        seq.year_month
     from nodis_case c
     join tt_pat_seq_enc seq on seq.patient_id=c.patient_id
     join (select case_id, status, date as strtdt, 
               case 
                   when lead(date) over (partition by case_id order by date) is not null 
                       then lead(date) over (partition by case_id order by date)
                   else now()
               end enddt 
	    from nodis_caseactivehistory) cah on c.id=cah.case_id 
		                                  and date_trunc('month', cah.strtdt)<=to_date(seq.year_month,'yyyy_mm')
                                          and date_trunc('month', cah.enddt)>=to_date(seq.year_month,'yyyy_mm')+interval '1 month'
where c.condition='diagnosedhypertension' 
group by c.patient_id, seq.year_month) t0;
--
-- ILI
--
select 'Starting to build tt_ili_cur at: ',now();
drop table if exists tt_ili_cur;
create table tt_ili_cur as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
	, max(1) ili
from nodis_case c
where c.condition='ili' 
group by patient_id, to_char(date,'yyyy_mm');
--
-- hep c
--
select 'Starting to build tt_hepc at: ',now();
drop table if exists tt_hepc;
create table tt_hepc as
	SELECT 
	  sl.patient_id
    , to_char(date,'yyyy_mm') year_month	  
	, max(1) hepc 
	, max(to_char(date,'yyyy_mm')) over (partition by sl.patient_id) max_ym
	FROM tt_sublab sl
	WHERE sl.test_name in ('hepatitis_c_rna','hepatitis_c_elisa')
	GROUP BY sl.patient_id, to_char(date,'yyyy_mm');
--
-- Lyme
--
select 'Starting to build tt_lyme at: ',now();
drop table if exists tt_lyme;
create table tt_lyme as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
	, max(1) lyme
from nodis_case c
where c.condition='lyme'
group by patient_id, to_char(date,'yyyy_mm');
--
-- syphilis
--
select 'Starting to build tt_syph at: ',now();
drop table if exists tt_syph;
create table tt_syph as
	SELECT seq.patient_id
	, seq.year_month
	, case 
	    when to_date(seq.year_month,'yyyy_mm')-to_date(lb_lead.year_month,'yyyy_mm') between 0 and 365 then 1
	    when to_date(seq.year_month,'yyyy_mm')-to_date(lb_lead.year_month,'yyyy_mm') between 366 and 730 then 2
	    when to_date(seq.year_month,'yyyy_mm')-to_date(lb_lead.year_month,'yyyy_mm') > 730 then 3
		else 0
	  end as syph
    from
	(select patient_id,
	       year_month,
		   lead(year_month) over (partition by patient_id order by year_month) as next_ym
    from
    (select patient_id, 
	       to_char(lb.date,'yyyy_mm') year_month
	 FROM (select * from tt_sublab 
	      where test_name in ('rpr','vdrl','tpps','fta-abs','tp-igg','tp-igm','vdrl-csf','tppa-csf','fta-avs-csf')) lb
     group by patient_id, to_char(lb.date, 'yyyy_mm')) lb_agg) lb_lead
     right join tt_pat_seq seq on lb_lead.patient_id=seq.patient_id 
	   and (lb_lead.next_ym>seq.year_month or lb_lead.next_ym is null) 
	   and lb_lead.year_month <=seq.year_month
		;
--
-- Pertussis
--
select 'Starting to build tt_pertussis at: ',now();
drop table if exists tt_pertussis;
create table tt_pertussis as
  select patient_id
    , to_char(date,'yyyy_mm') year_month
	, max(1) pertussis
	from nodis_case c
where c.condition='pertussis' 
group by patient_id, to_char(date,'yyyy_mm');
--
-- Cardio Risk
--
\ir ../ContinuityOfCare/Cardiac_risk/cardiac-risk-prediction-historical.pg.sql
select 'Starting to build tt_cvrisk at: ',now();
drop table if exists tt_cvrisk;
create table tt_cvrisk as
  select patient_id
    , year_month
	, case
        when cv_risk = 1 then 1
        when cv_risk < 1 and cv_risk >= .2 then 2
        when cv_risk < .2 and cv_risk >= .15 then 3
        when cv_risk < .15 and cv_risk >= .1 then 4
        when cv_risk < .1 and cv_risk >= .05 then 5
        when cv_risk < .05 then 6
      end cvrisk
	from gen_pop_tools.cc_cr_risk_score
    where cv_risk <= 1;
