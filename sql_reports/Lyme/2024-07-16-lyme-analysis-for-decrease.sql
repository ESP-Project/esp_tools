		
-- # of patients with any one of the ICD-10 dx codes in the algorithm – overall/across all codes*
DROP TABLE IF EXISTS kre_report.lyme_dx_all_counts;
CREATE TABLE kre_report.lyme_dx_all_counts AS
SELECT rpt_year, count(*) as lyme_dx_pats
FROM (SELECT DISTINCT patient_id, extract(year from date) rpt_year
	FROM hef_event
	WHERE name = 'dx:lyme'
	AND date >= '2012-01-01'
	AND date < '2025-01-01') T1
GROUP BY rpt_year
ORDER BY rpt_year;


DROP TABLE IF EXISTS kre_report.lyme_hef_dx;
CREATE TABLE kre_report.lyme_hef_dx as
SELECT DISTINCT patient_id, name, date, 1 as lyme_dx
FROM hef_event
WHERE name = 'dx:lyme'
AND date >= '2012-01-01'
AND date < '2025-01-01'
ORDER BY patient_id, date;

DROP TABLE IF EXISTS kre_report.lyme_hef_rx;
CREATE TABLE kre_report.lyme_hef_rx as
SELECT DISTINCT patient_id, name, date, 1 as lyme_rx
FROM hef_event
WHERE name in ('rx:lyme_other_antibiotics', 'rx:lyme_doxycycline')
AND date >= '2012-01-01'
AND date < '2025-01-01'
ORDER BY patient_id, date;

-- # of patients with one of the ICD-10 codes and a qualifying antibiotic for >=7 days within a 14 day period*
DROP TABLE IF EXISTS kre_report.lyme_dx_and_rx;
CREATE TABLE kre_report.lyme_dx_and_rx AS
SELECT count(DISTINCT(patient_id)) lyme_dx_rx_pats, rpt_year
FROM 
	(SELECT T1.patient_id, extract(year from T1.date) as rpt_year, T1.date, abs(T1.date - T2.date)
	FROM kre_report.lyme_hef_dx T1
	JOIN kre_report.lyme_hef_rx T2 ON (T1.patient_id = T2.patient_id)
	AND abs(T1.date - T2.date) <= 14) T1
GROUP BY rpt_year;


-- # of patients with a Lyme Western blot performed
-- # of Lyme Western blots that were positive

DROP TABLE IF EXISTS kre_report.lyme_wb_lx_all;
CREATE TABLE kre_report.lyme_wb_lx_all as 
SELECT DISTINCT patient_id, extract(year from date) rpt_year, name
FROM hef_event 
WHERE name ilike 'lx:lyme_wb:%'
AND date >= '2012-01-01'
AND date < '2025-01-01';

DROP TABLE IF EXISTS kre_report.lyme_wb_counts;
CREATE TABLE kre_report.lyme_wb_counts AS 
SELECT count(distinct(patient_id)), rpt_year,
COUNT(DISTINCT(patient_id)) as lyme_wb_perf_pats,
COUNT(DISTINCT((CASE WHEN name ilike '%pos%' THEN patient_id end)))  lyme_wb_pos_pats
FROM kre_report.lyme_wb_lx_all
GROUP BY rpt_year
ORDER BY rpt_year;

-- # of patients with a Lyme PCR performed
-- # of Lyme PCRs that were positive

DROP TABLE IF EXISTS kre_report.lyme_pcr_lx_all;
CREATE TABLE kre_report.lyme_pcr_lx_all as 
SELECT DISTINCT patient_id, extract(year from date) rpt_year, name
FROM hef_event 
WHERE name ilike 'lx:lyme_pcr:%'
AND date >= '2012-01-01'
AND date < '2025-01-01';


DROP TABLE IF EXISTS kre_report.lyme_pcr_counts;
CREATE TABLE kre_report.lyme_pcr_counts AS 
SELECT count(distinct(patient_id)), rpt_year,
COUNT(DISTINCT(patient_id)) as lyme_pcr_perf_pats,
COUNT(DISTINCT((CASE WHEN name ilike '%pos%' THEN patient_id end)))  lyme_pcr_pos_pats
FROM kre_report.lyme_pcr_lx_all
GROUP BY rpt_year
ORDER BY rpt_year;

-- # of patients with a two-tiered test
-- # of those that were positive

DROP TABLE IF EXISTS kre_report.lyme_mtta_lx_all;
CREATE TABLE kre_report.lyme_mtta_lx_all as 
SELECT DISTINCT patient_id, extract(year from date) rpt_year, name
FROM hef_event 
WHERE name ilike 'lx:lyme_mttta_conf:%'
AND date >= '2012-01-01'
AND date < '2025-01-01';


DROP TABLE IF EXISTS kre_report.lyme_mtta_counts;
CREATE TABLE kre_report.lyme_mtta_counts AS 
SELECT count(distinct(patient_id)), rpt_year,
COUNT(DISTINCT(patient_id)) as lyme_mtta_perf_pats,
COUNT(DISTINCT((CASE WHEN name ilike '%pos%' THEN patient_id end)))  lyme_mtta_pos_pats
FROM kre_report.lyme_mtta_lx_all
GROUP BY rpt_year
ORDER BY rpt_year;


DROP TABLE IF EXISTS kre_report.lyme_cases_criteria;
CREATE TABLE kre_report.lyme_cases_criteria AS 
SELECT count(distinct(id)) case_count, criteria, extract(year from date) rpt_year
FROM nodis_case
WHERE condition = 'lyme'
AND date >= '2012-01-01'
AND date < '2025-01-01'
GROUP BY rpt_year, criteria
ORDER BY rpt_year;

-- CREATE OUTPUT
-- DROP TEMP TABLES

DROP TABLE IF EXISTS kre_report.lyme_analysis_output;
CREATE TABLE kre_report.lyme_analysis_output AS 
SELECT T1.rpt_year, 
coalesce(lyme_dx_pats, 0) lyme_dx_pats,
coalesce(lyme_dx_rx_pats, 0) lyme_dx_rx_pats,
coalesce(lyme_wb_perf_pats, 0) lyme_wb_perf_pats,
coalesce(lyme_wb_pos_pats, 0) lyme_wb_pos_pats,
coalesce(lyme_pcr_perf_pats, 0) lyme_pcr_perf_pats,
coalesce(lyme_pcr_pos_pats, 0) lyme_pcr_pos_pats,
coalesce(lyme_mtta_perf_pats, 0) lyme_mtta_perf_pats,
coalesce(lyme_mtta_pos_pats, 0) lyme_mtta_pos_pats,
MAX(CASE WHEN T6.criteria = 'Criteria #1 Lyme DxCode and antibiotics within 14 days' then case_count ELSE 0 END) as crit1_lyme_dx_plus_antib,
MAX(CASE WHEN T6.criteria = 'Criteria #2 Positive Lyme Western Blot' then case_count ELSE 0 END) as crit2_pos_wb,
MAX(CASE WHEN T6.criteria = 'Criteria #3 Positive Lyme PCR' then case_count ELSE 0 END) as crit3_pos_pcr,
MAX(CASE WHEN T6.criteria = 'Criteria #4 Two Positive EIA from FDA-approved MTTTA or MTTA Confirmatory' then case_count ELSE 0 END) as crit4_mttt
FROM kre_report.lyme_dx_all_counts T1
LEFT JOIN kre_report.lyme_dx_and_rx T2 ON (T1.rpt_year = T2.rpt_year)
LEFT JOIN kre_report.lyme_wb_counts T3 ON (T1.rpt_year = T3.rpt_year)
LEFT JOIN kre_report.lyme_pcr_counts T4 ON (T1.rpt_year = T4.rpt_year)
LEFT JOIN kre_report.lyme_mtta_counts T5 ON (T1.rpt_year = T5.rpt_year)
LEFT JOIN kre_report.lyme_cases_criteria T6 ON (T1.rpt_year = T6.rpt_year)
GROUP BY T1.rpt_year, lyme_dx_pats, lyme_dx_rx_pats, lyme_wb_perf_pats, lyme_wb_pos_pats, lyme_pcr_perf_pats, lyme_pcr_pos_pats, lyme_mtta_perf_pats, lyme_mtta_pos_pats;


DROP TABLE IF EXISTS kre_report.lyme_dx_all_counts;
DROP TABLE IF EXISTS kre_report.lyme_hef_dx;
DROP TABLE IF EXISTS kre_report.lyme_hef_rx;
DROP TABLE IF EXISTS kre_report.lyme_dx_and_rx;
DROP TABLE IF EXISTS kre_report.lyme_wb_lx_all;
DROP TABLE IF EXISTS kre_report.lyme_wb_counts;
DROP TABLE IF EXISTS kre_report.lyme_pcr_lx_all;
DROP TABLE IF EXISTS kre_report.lyme_pcr_counts;
DROP TABLE IF EXISTS kre_report.lyme_mtta_lx_all;
DROP TABLE IF EXISTS kre_report.lyme_mtta_counts;
DROP TABLE IF EXISTS kre_report.lyme_cases_criteria;

SELECT * FROM kre_report.lyme_analysis_output;


--Atrius, BMC, CHA, Fenway, Greater Lawrence



-- \COPY (SELECT * FROM kre_report.lyme_analysis_output) TO '/tmp/ATR-2025-02-13-lyme-analysis-for-decrease.csv' WITH CSV HEADER;
-- /srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n ATR-2025-02-13-lyme-analysis-for-decrease.csv -s "ATR LYME ANALYSIS" 


-- \COPY (SELECT * FROM kre_report.lyme_analysis_output) TO '/tmp/CHA-2025-02-13-lyme-analysis-for-decrease.csv' WITH CSV HEADER;
-- /srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n CHA-2025-02-13-lyme-analysis-for-decrease.csv -s "CHA LYME ANALYSIS" 

-- \COPY (SELECT * FROM kre_report.lyme_analysis_output) TO '/tmp/BMC-2025-02-13-lyme-analysis-for-decrease.csv' WITH CSV HEADER;
-- /srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@bmc.org -r keberhardt@commoninf.com -p /tmp -n BMC-2025-02-13-lyme-analysis-for-decrease.csv -s "BMC LYME ANALYSIS"

-- \COPY (SELECT * FROM kre_report.lyme_analysis_output) TO '/tmp/FWY-2025-02-13-lyme-analysis-for-decrease.csv' WITH CSV HEADER;
-- /srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n FWY-2025-02-13-lyme-analysis-for-decrease.csv -s "FWY LYME ANALYSIS" 

-- \COPY (SELECT * FROM kre_report.lyme_analysis_output) TO '/tmp/GLFHC-2025-02-13-lyme-analysis-for-decrease.csv' WITH CSV HEADER;
-- /srv/esp/esp_tools/send_attached_file.py -f noreply@glfhc.org -r keberhardt@commoninf.com -p /tmp -n GLFHC-2025-02-13-lyme-analysis-for-decrease.csv -s "GLFHC LYME ANALYSIS" 









