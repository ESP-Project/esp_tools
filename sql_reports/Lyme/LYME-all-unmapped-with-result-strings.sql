drop table if exists temp_unmapped_lyme;

create table temp_unmapped_lyme as
select e.count, e.native_name, e.native_code, procedure_name, max(date) most_recent_date
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
-- UNCOMMENT THIS FOR WIDE RANGE OF STRINGS
and e.native_name ~* '(lyme|borr|burg|mttt|zeus)'
group by e.count, e.native_name, e.native_code, procedure_name;

DROP TABLE IF EXISTS temp_unmapped_with_result_strings_lyme;
CREATE TABLE temp_unmapped_with_result_strings_lyme AS
SELECT T2.lab_count, T2.most_recent_date, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_unmapped_lyme S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name, most_recent_date
  from emr_labresult T1,
  temp_unmapped_lyme T2
  where T1.native_code = T2.native_code 
  group by T1.native_code, T1.native_name, T1.procedure_name, most_recent_date) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, T2.most_recent_date
ORDER BY T2.native_code, T2.native_name;


\COPY temp_unmapped_with_result_strings_lyme  TO '/tmp/lyme_unmapped_labs_with_result_strings.csv' DELIMITER ',' CSV HEADER;


drop table temp_unmapped_lyme;
drop table temp_unmapped_with_result_strings_lyme;