
drop table if exists kre_report.new_center_labs;
create table kre_report.new_center_labs AS
select T1.id as lab_id, patient_id, date
from emr_labresult T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('48', '49', '50', '52', '51')
AND date < '12/1/2022';

drop table if exists kre_report.new_center_lab_hef;
create table kre_report.new_center_lab_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_labs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.lab_id = T2.object_id
AND T2.name ilike 'lx:%';


drop table if exists kre_report.oldnew_center_lab_nces;
create table kre_report.oldnew_center_lab_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_lab_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_lab_hef T2 ON (T1.event_id =T2.hef_id);


DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_lab_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_lab_hef);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.old_center_labs);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.new_center_labs);

----------------------------------------------------------------------------------------
drop table if exists kre_report.new_center_meds;
create table kre_report.new_center_meds AS
select T1.id as med_id, patient_id, date
from emr_prescription T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('48', '49', '50', '52', '51')
AND date < '12/1/2022';

drop table if exists kre_report.new_center_med_hef;
create table kre_report.new_center_med_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.med_id = T2.object_id
AND T2.name ilike 'rx:%';

drop table if exists kre_report.oldnew_center_med_nces;
create table kre_report.oldnew_center_med_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_med_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_med_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_med_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_med_hef);
DELETE FROM emr_prescription where id in (select med_id from kre_report.old_center_meds);
DELETE FROM emr_prescription where id in (select med_id from kre_report.new_center_meds);

---------------------------------------------------------------------------------------
drop table if exists kre_report.new_center_encs;
create table kre_report.new_center_encs AS
select T1.id as enc_id, patient_id, date
from emr_encounter T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('48', '49', '50', '52', '51')
AND date < '12/1/2022';

drop table if exists kre_report.new_center_enc_hef;
create table kre_report.new_center_enc_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_encs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.enc_id = T2.object_id
AND (T2.name ilike 'enc:%' or T2.name ilike 'dx:%');

drop table if exists kre_report.oldnew_center_enc_nces;
create table kre_report.oldnew_center_enc_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_enc_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_enc_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.new_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.new_center_encs);

----------------------------------------------------------------------------------------------
drop table if exists kre_report.new_center_labs;
create table kre_report.new_center_labs AS
select T1.id as lab_id, patient_id, date
from emr_labresult T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('56')
AND date < '04/1/2023';

drop table if exists kre_report.new_center_lab_hef;
create table kre_report.new_center_lab_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_labs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.lab_id = T2.object_id
AND T2.name ilike 'lx:%';

drop table if exists kre_report.oldnew_center_lab_nces;
create table kre_report.oldnew_center_lab_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_lab_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_lab_hef T2 ON (T1.event_id =T2.hef_id);


DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_lab_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_lab_hef);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.old_center_labs);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.new_center_labs);

------------------------------------------------------------------------------
drop table if exists kre_report.new_center_labs;
create table kre_report.new_center_labs AS
select T1.id as lab_id, patient_id, date
from emr_labresult T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('47')
AND date < '05/1/2022';

drop table if exists kre_report.new_center_lab_hef;
create table kre_report.new_center_lab_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_labs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.lab_id = T2.object_id
AND T2.name ilike 'lx:%';


drop table if exists kre_report.oldnew_center_lab_nces;
create table kre_report.oldnew_center_lab_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_lab_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_lab_hef T2 ON (T1.event_id =T2.hef_id);


DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_lab_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_lab_hef);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.old_center_labs);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.new_center_labs);

--------------------------------------------------
drop table if exists kre_report.new_center_labs;
create table kre_report.new_center_labs AS
select T1.id as lab_id, patient_id, date
from emr_labresult T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('46')
AND date < '04/1/2021';

drop table if exists kre_report.new_center_lab_hef;
create table kre_report.new_center_lab_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_labs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.lab_id = T2.object_id
AND T2.name ilike 'lx:%';


drop table if exists kre_report.oldnew_center_lab_nces;
create table kre_report.oldnew_center_lab_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_lab_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_lab_hef T2 ON (T1.event_id =T2.hef_id);


DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_lab_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_lab_hef);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.old_center_labs);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.new_center_labs);

-------------------------------------------------
drop table if exists kre_report.new_center_labs;
create table kre_report.new_center_labs AS
select T1.id as lab_id, patient_id, date
from emr_labresult T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('60')
AND date < '06/1/2024';

drop table if exists kre_report.new_center_lab_hef;
create table kre_report.new_center_lab_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_labs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.lab_id = T2.object_id
AND T2.name ilike 'lx:%';


drop table if exists kre_report.oldnew_center_lab_nces;
create table kre_report.oldnew_center_lab_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_lab_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_lab_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_lab_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_lab_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_lab_hef);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.old_center_labs);
DELETE FROM emr_labresult where id in (select lab_id from kre_report.new_center_labs);



-------------------------------------------------
drop table if exists kre_report.new_center_meds;
create table kre_report.new_center_meds AS
select T1.id as med_id, patient_id, date
from emr_prescription T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('56')
AND date < '04/1/2023';

drop table if exists kre_report.new_center_med_hef;
create table kre_report.new_center_med_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.med_id = T2.object_id
AND T2.name ilike 'rx:%';

drop table if exists kre_report.oldnew_center_med_nces;
create table kre_report.oldnew_center_med_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_med_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_med_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_med_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_med_hef);
DELETE FROM emr_prescription where id in (select med_id from kre_report.old_center_meds);
DELETE FROM emr_prescription where id in (select med_id from kre_report.new_center_meds);

--------------------------------------------------------------------------------------------
drop table if exists kre_report.new_center_meds;
create table kre_report.new_center_meds AS
select T1.id as med_id, patient_id, date
from emr_prescription T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('47')
AND date < '05/1/2022';

drop table if exists kre_report.new_center_med_hef;
create table kre_report.new_center_med_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.med_id = T2.object_id
AND T2.name ilike 'rx:%';

drop table if exists kre_report.oldnew_center_med_nces;
create table kre_report.oldnew_center_med_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_med_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_med_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_med_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_med_hef);
DELETE FROM emr_prescription where id in (select med_id from kre_report.old_center_meds);
DELETE FROM emr_prescription where id in (select med_id from kre_report.new_center_meds);

------------------------------------------------------------------------------

drop table if exists kre_report.new_center_meds;
create table kre_report.new_center_meds AS
select T1.id as med_id, patient_id, date
from emr_prescription T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('46')
AND date < '03/1/2021';

drop table if exists kre_report.new_center_med_hef;
create table kre_report.new_center_med_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.med_id = T2.object_id
AND T2.name ilike 'rx:%';

drop table if exists kre_report.oldnew_center_med_nces;
create table kre_report.oldnew_center_med_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_med_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_med_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_med_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_med_hef);
DELETE FROM emr_prescription where id in (select med_id from kre_report.old_center_meds);
DELETE FROM emr_prescription where id in (select med_id from kre_report.new_center_meds);

-----------------------------------------------------------------------------

drop table if exists kre_report.new_center_meds;
create table kre_report.new_center_meds AS
select T1.id as med_id, patient_id, date
from emr_prescription T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('60')
AND date < '06/1/2024';

drop table if exists kre_report.new_center_med_hef;
create table kre_report.new_center_med_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_meds T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.med_id = T2.object_id
AND T2.name ilike 'rx:%';

drop table if exists kre_report.oldnew_center_med_nces;
create table kre_report.oldnew_center_med_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_med_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_med_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_med_nces);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_med_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_med_hef);
DELETE FROM emr_prescription where id in (select med_id from kre_report.old_center_meds);
DELETE FROM emr_prescription where id in (select med_id from kre_report.new_center_meds);

-----------------------------------------------------------------------------

drop table if exists kre_report.new_center_encs;
create table kre_report.new_center_encs AS
select T1.id as enc_id, patient_id, date
from emr_encounter T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('56')
AND date < '04/1/2023';

drop table if exists kre_report.new_center_enc_hef;
create table kre_report.new_center_enc_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_encs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.enc_id = T2.object_id
AND (T2.name ilike 'enc:%' or T2.name ilike 'dx:%');

drop table if exists kre_report.oldnew_center_enc_nces;
create table kre_report.oldnew_center_enc_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_enc_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_enc_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.new_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.new_center_encs);

-------------------------------------------------------------------------------

drop table if exists kre_report.new_center_encs;
create table kre_report.new_center_encs AS
select T1.id as enc_id, patient_id, date
from emr_encounter T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('47')
AND date < '05/1/2022';

drop table if exists kre_report.new_center_enc_hef;
create table kre_report.new_center_enc_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_encs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.enc_id = T2.object_id
AND (T2.name ilike 'enc:%' or T2.name ilike 'dx:%');

drop table if exists kre_report.oldnew_center_enc_nces;
create table kre_report.oldnew_center_enc_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_enc_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_enc_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.new_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.new_center_encs);

-----------------------------------------------------------------------------
drop table if exists kre_report.new_center_encs;
create table kre_report.new_center_encs AS
select T1.id as enc_id, patient_id, date
from emr_encounter T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('46')
AND date < '04/1/2021';

drop table if exists kre_report.new_center_enc_hef;
create table kre_report.new_center_enc_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_encs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.enc_id = T2.object_id
AND (T2.name ilike 'enc:%' or T2.name ilike 'dx:%');

drop table if exists kre_report.oldnew_center_enc_nces;
create table kre_report.oldnew_center_enc_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_enc_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_enc_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.new_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.new_center_encs);

----------------------------------------------------------------------------

drop table if exists kre_report.new_center_encs;
create table kre_report.new_center_encs AS
select T1.id as enc_id, patient_id, date
from emr_encounter T1
INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)
WHERE T2.center_id in ('60')
AND date < '06/1/2024';

drop table if exists kre_report.new_center_enc_hef;
create table kre_report.new_center_enc_hef AS
select T1.patient_id, T2.id as hef_id, T2.date as hef_date
from kre_report.new_center_encs T1
INNER JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
WHERE T1.enc_id = T2.object_id
AND (T2.name ilike 'enc:%' or T2.name ilike 'dx:%');

drop table if exists kre_report.oldnew_center_enc_nces;
create table kre_report.oldnew_center_enc_nces AS
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.old_center_enc_hef T2 ON (T1.event_id =T2.hef_id)
UNION
select case_id, id as nce_id, event_id
from nodis_case_events T1
INNER JOIN kre_report.new_center_enc_hef T2 ON (T1.event_id =T2.hef_id);

DELETE FROM nodis_case_timespans where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_caseactivehistory where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case_events where case_id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM nodis_case where id in (select case_id from kre_report.oldnew_center_enc_nces);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_timespan_events where event_id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.old_center_enc_hef);
DELETE FROM hef_event where id in (select hef_id from kre_report.new_center_enc_hef);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter_dx_codes where encounter_id in (select enc_id from kre_report.new_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.old_center_encs);
DELETE FROM emr_encounter where id in (select enc_id from kre_report.new_center_encs);
