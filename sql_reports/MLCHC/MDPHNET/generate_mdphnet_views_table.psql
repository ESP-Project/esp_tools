drop table if exists mdphnet_schema_update_history;
CREATE TABLE mdphnet_schema_update_history
(
  latest_update timestamp without time zone NOT NULL,
  patients_replaced integer,
  CONSTRAINT update_timestamp_pk PRIMARY KEY (latest_update)
);
insert into mdphnet_schema_update_history
select current_timestamp, 0;

drop view if exists esp_demographic_v;
drop table if exists esp_pretemp_smoking;
create table esp_pretemp_smoking as
select patient_id, date,
case when tobacco_use in ('N','Never','no','Denies.','Nonsmoker.','never','No. .',
                          'none','0','negative','Nonsmoker','None.','Denies','Non-smoker','None',
                          'Non smoker','Never smoked.','non smoker','Non-smoker','Non-smoker.',
                          'Non smoker.','nonsmoker') then 'Never'
     when tobacco_use in ('R','Prev','Previous','quit','Quit smoking','Former smoker.','Former smoker',
                          'Former','prev.','Quit.','quit smoking') 
          or tobacco_use ~ '^[0123456789]{1,2}/[[0123456789]{1,2}/[0123456789]{2,4}' 
          or tobacco_use similar to '(quit|Quit|Previous|Used to|Former)%' then 'Former'
     when tobacco_use in ('Y','Current','X','Yes. .','precontemplative','counseled','current','one',
                          'Very Ready','Post Partum','ready','contemplative','Yes. mom. .', 
                          'Yes. outside. .','relapse','occasional','rare',
                          'Yes. father. .','Still smoking.','Yes. outside only. .','Yes. mother. .',
                          'socially','Cessation discussed.','social','Avoid complications',
                          'feels addicted, other','Have cut back','rarely')
       or tobacco_use similar to '(improve|stress|<|>|More than|Yes|occ|Some|roll|Continue|less|Current|Still|social|half|one|pack)%' 
       or tobacco_use ~ '^[0123456789]'
       or tobacco_use ~ '^.[0123456789]'
       or tobacco_use ilike '%smokes%' 
       or tobacco_use similar to '%(cut down|continues|almost|ready to|cessation|addicted|current|contemplat)%' then 'Current'
       else 'Not available' 
end as smoking
from emr_socialhistory;
create index esp_pretemp_smoking_pid_idx on esp_pretemp_smoking (patient_id);
vacuum analyse esp_pretemp_smoking;

drop table if exists esp_temp_smoking;
create table esp_temp_smoking as
   select case when t1.latest='Current' then 'Current'
               when t2.yesOrQuit='Former' then 'Former'
               when t4.never='Never' then 'Never'
               else 'Not available' 
           end as smoking,
           t0.natural_key as patid
   from
     (select distinct id, natural_key from emr_patient) t0
   left outer join 
     (select case when bool_or(t00.smoking='Current') then 'Current' end as latest, t00.patient_id 
      from esp_pretemp_smoking t00
      inner join
      (select max(date) as maxdate, patient_id 
       from esp_pretemp_smoking  
       group by patient_id) t01 on t00.patient_id=t01.patient_id and t00.date=t01.maxdate
       group by t00.patient_id) t1 on t0.id=t1.patient_id
   left outer join
     (select max(val) as yesOrQuit, patient_id
      from (select 'Former'::text as val, patient_id
            from esp_pretemp_smoking where smoking in ('Current','Former')) t00
            group by patient_id) t2 on t0.id=t2.patient_id
   left outer join
     (select max(val) as never, patient_id
      from (select 'Never'::text as val, patient_id
            from esp_pretemp_smoking where smoking ='Never') t00
            group by patient_id) t4 on t0.id=t4.patient_id;
alter table esp_temp_smoking add primary key (patid);

drop view if exists esp_demographic_v;
CREATE or replace VIEW esp_demographic_v AS
SELECT pat.center_id centerid,
       pat.natural_key patid,
       pat.date_of_birth::date - ('1960-01-01'::date) birth_date,
       CASE
         WHEN UPPER(gender) = 'M' THEN 'M'::char(1)
         WHEN UPPER(gender) = 'F' THEN 'F'::char(1)
         WHEN UPPER(gender) = 'UNKNOWN' THEN 'U'::char(1)
         ELSE 'U'::char(1)
       END sex,
       CASE
         WHEN UPPER(ethnicity) = 'HISPANIC/LATINO' THEN 'Y'::char(1)
         WHEN UPPER(replace(ethnicity,' ','')) = 'NON-HISPANIC/LATINO' THEN 'N'::char(1)
         ELSE 'U'::char(1)
       END Hispanic,
       CASE
         WHEN UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE' THEN 1
         WHEN UPPER(race) = 'ASIAN' THEN 2
         WHEN UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' THEN 3
         WHEN UPPER(race) in ('NATIVE HAWAIIAN','OTHER PACIFIC ISLANDER') then 4
         WHEN UPPER(race) = 'WHITE' THEN 5
         ELSE 0
       END race,
       CASE
         WHEN UPPER(ethnicity)= 'HISPANIC/LATINO' THEN 6 
         WHEN UPPER(race) = 'WHITE' THEN 5 
         WHEN UPPER(replace(race,' ','')) = 'BLACK/AFRICANAMERICAN' THEN 3 
         WHEN UPPER(race) = 'ASIAN' THEN 2 
         WHEN UPPER(race) in ('NATIVE HAWAIIAN','OTHER PACIFIC ISLANDER') then 1 
         WHEN UPPER(replace(race,' ','')) = 'AMERICANINDIAN/ALASKANATIVE' THEN 1 
         ELSE 0 
       END race_ethnicity,
       pat.zip5,
       smk.smoking
  FROM public.emr_patient pat,
       public.emr_provenance prvn,
       esp_temp_smoking smk,
       (select distinct patient_id from emr_encounter) encpat
  WHERE pat.provenance_id=prvn.provenance_id and prvn.source ilike 'epicmem%'

        and pat.natural_key=smk.patid and pat.id=encpat.patient_id;


DROP TABLE if exists esp_current_asthma_cases cascade;
CREATE TABLE esp_current_asthma_cases AS
        select id, patient_id from
        (select max(public.hef_event.date) as MAXEVTDT, public.nodis_case.patient_id, public.nodis_case.id from hef_event, public.nodis_case where 
        condition = 'asthma'
        and
        public.hef_event.patient_id = public.nodis_case.patient_id
        and public.hef_event.name in ('dx:asthma', 'rx:albuterol', 'rx:alvesco', 'rx:pulmicort', 'rx:flovent', 'rx:asmanex', 'rx:aerobid', 'rx:montelukast','rx:intal','rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium','rx:tiotropium','rx:omalizumab', 'rx:fluticasone-inh', 'rx:mometasone-inh','rx:budesonide-inh','rx:ciclesonide-inh','rx:flunisolide-inh','rx:cromolyn-inh','rx:pirbuterol','rx:levalbuterol','rx:arformoterol','rx:formeterol','rx:indacaterol','rx:salmeterol',
'rx:beclomethasone','rx:fluticasone-salmeterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formeterol:generic',
'rx:budesonide-formeterol:generic', 'rx:fluticasone-salmeterol:trade','rx:albuterol-ipratropium:trade','rx:mometasone-formeterol:trade','rx:budesonide-formeterol:trade') group by public.nodis_case.patient_id, public.nodis_case.id) A 
        where
        current_date - MAXEVTDT <= (365.25*2);
CREATE INDEX esp_current_asthma_cases_caseid_idx on esp_current_asthma_cases (id);
VACUUM ANALYZE esp_current_asthma_cases;
ALTER TABLE esp_current_asthma_cases add primary key (patient_id);

drop view if exists esp_encounter_v;
CREATE or replace VIEW esp_encounter_v AS
SELECT pat.center_id AS centerid, 
       pat.natural_key AS patid, 
                   enc.natural_key AS encounterid, 
                   enc.date - '1960-01-01'::date AS a_date, 
                   enc.date_closed - '1960-01-01'::date AS d_date, 
                   prov.natural_key AS provider, 
                   case 
                     when strpos(enc.site_name::text, '-'::text)<1 
                                    then enc.site_name::character varying(100)
                                else substring(enc.site_name::text, 
                                           1, 
                                           strpos(enc.site_name::text, '-'::text) - 1)::character varying(100)
                   end AS facility_location, 
                   'AV'::character varying(10) AS enc_type, 
                   enc.site_natural_key AS facility_code, 
                   date_part('year'::text, enc.date)::integer AS enc_year, 
                   age_at_year_start(enc.date, pat.date_of_birth::date) AS age_at_enc_year, 
                   age_group_5yr(enc.date, pat.date_of_birth::date)::character varying(5) AS age_group_5yr, 
                   age_group_10yr(enc.date, pat.date_of_birth::date)::character varying(5) AS age_group_10yr, age_group_ms(enc.date, pat.date_of_birth::date)::character varying(5) AS age_group_ms
   FROM public.emr_encounter enc
   JOIN public.emr_patient pat ON enc.patient_id = pat.id
   JOIN public.emr_provenance prvn ON pat.provenance_id = prvn.provenance_id
   LEFT JOIN public.emr_provider prov ON enc.provider_id = prov.id
   WHERE prvn.source::text ~~ 'epicmem%'::text;
  


drop view if exists esp_diagnosis_v;
CREATE or replace VIEW esp_diagnosis_v AS
SELECT pat.center_id centerid,
       pat.natural_key patid,
       enc.natural_key encounterid,
       enc.date - ('1960-01-01'::date) a_date,
       prov.natural_key provider,
       'AV'::varchar(10) enc_type, --this is initial value for Mass League data
       split_part(dx_code_id,':', 2) dx,
       split_part(dx_code_id, ':', 1) dx_type,

case
                     when strpos(enc.site_name::text, '-'::text)<1
                                    then enc.site_name::character varying(100)
                                else substring(enc.site_name::text,
                                           1,
                                           strpos(enc.site_name::text, '-'::text) - 1)::character varying(100)
                   end AS facility_location,
       enc.site_natural_key facility_code,
       date_part('year', enc.date)::integer enc_year,
       age_at_year_start(enc.date, pat.date_of_birth::date) age_at_enc_year,
       age_group_5yr(enc.date, pat.date_of_birth::date)::varchar(5) age_group_5yr,
       age_group_10yr(enc.date, pat.date_of_birth::date)::varchar(5) age_group_10yr,
       age_group_ms(enc.date, pat.date_of_birth::date)::varchar(5) age_group_ms
  FROM public.emr_encounter enc
         INNER JOIN public.emr_patient pat ON enc.patient_id = pat.id
         INNER JOIN public.emr_provenance prvn on pat.provenance_id = prvn.provenance_id
         INNER JOIN (select * from public.emr_encounter_dx_codes
                     where strpos(trim(dx_code_id),'.')<>3
                       and length(trim(dx_code_id))>=3 ) diag ON enc.id = diag.encounter_id
         LEFT JOIN public.emr_provider prov ON enc.provider_id = prov.id
  WHERE prvn.source::text ~~ 'epicmem%'::text;

drop view if exists esp_disease_v;
CREATE or replace VIEW esp_disease_v AS
SELECT pat.center_id centerid,
       pat.natural_key patid,
       disease.condition,
       disease.date - ('1960-01-01'::date) date,
       age_at_year_start(disease.date, pat.date_of_birth::date) age_at_detect_year,
       age_group_5yr(disease.date, pat.date_of_birth::date)::varchar(5) age_group_5yr,
       age_group_10yr(disease.date, pat.date_of_birth::date)::varchar(5) age_group_10yr,
       age_group_ms(disease.date, pat.date_of_birth::date)::varchar(5) age_group_ms,
       disease.criteria,
       disease.status,
       disease.notes
  FROM public.nodis_case disease
         INNER JOIN public.emr_patient pat ON disease.patient_id = pat.id
         INNER JOIN public.emr_provenance prvn on pat.provenance_id = prvn.provenance_id
  WHERE prvn.source like 'epicmem%' 
  and (disease.condition in ('ili', 'diabetes:type-1', 'diabetes:type-2', 'diabetes:gestational', 'diabetes:prediabetes', 'depression')
  or disease.id in (select id from esp_current_asthma_cases));

-- Instantiate tables from previously created views
drop table if exists esp_demographic cascade;
create table esp_demographic as select * from esp_demographic_v;
create unique index esp_demographic_patid_unique_idx on esp_demographic (patid);
create index esp_demographic_centerid_idx on esp_demographic (centerid);
create index esp_demographic_birth_date_idx on esp_demographic (birth_date);
create index esp_demographic_sex_idx on esp_demographic (sex);
create index esp_demographic_hispanic_idx on esp_demographic (hispanic);
create index esp_demographic_race_idx on esp_demographic (race);
create index esp_demographic_race_eth_idx on esp_demographic (race_ethnicity);
create index esp_demographic_zip5_idx on esp_demographic (zip5); 
create index esp_demographic_smk_idx on esp_demographic (smoking);
alter table esp_demographic add primary key (patid);

drop table if exists esp_encounter cascade;
create table esp_encounter as select t0.* from esp_encounter_v 
as t0 inner join esp_demographic as t1 
            on t0.patid=t1.patid;


create index esp_encounter_centerid_idx on esp_encounter (centerid);
create index esp_encounter_patid_idx on esp_encounter (patid);
create unique index esp_encounter_encounterid_idx on esp_encounter (encounterid);
create index esp_encounter_a_date_idx on esp_encounter (a_date);
create index esp_encounter_d_date_idx on esp_encounter (d_date);
create index esp_encounter_provider_idx on esp_encounter (provider);
create index esp_encounter_facility_location_idx on esp_encounter (facility_location);
create index esp_encounter_facility_code_idx on esp_encounter (facility_code);
create index esp_encounter_enc_year_idx on esp_encounter (enc_year);
create index esp_encounter_age_at_enc_year_idx on esp_encounter (age_at_enc_year);
create index esp_encounter_age_group_5yr_idx on esp_encounter (age_group_5yr);
create index esp_encounter_age_group_10yr_idx on esp_encounter (age_group_10yr);
create index esp_encounter_age_group_ms_idx on esp_encounter (age_group_ms);
alter table esp_encounter add primary key (encounterid);
alter table esp_encounter add foreign key (patid) references esp_demographic (patid);

drop table if exists esp_diagnosis cascade;

create table esp_diagnosis as select t0.* from esp_diagnosis_v
            as t0 inner join esp_demographic as t1
            on t0.patid=t1.patid;

create index esp_diagnosis_dx_idx on esp_diagnosis (dx);
create index esp_diagnosis_centerid_idx on esp_diagnosis (centerid);
create index esp_diagnosis_patid_idx on esp_diagnosis (patid);
create index esp_diagnosis_encounterid_idx on esp_diagnosis (encounterid);
create index esp_diagnosis_provider_idx on esp_diagnosis (provider);
create index esp_diagnosis_enc_type_idx on esp_diagnosis (enc_type);
create index esp_diagnosis_facility_loc_idx on esp_diagnosis (facility_location);
create index esp_diagnosis_facility_code_idx on esp_diagnosis (facility_code);
create index esp_diagnosis_enc_year_idx on esp_diagnosis (enc_year);
create index esp_diagnosis_age_at_enc_year_idx on esp_diagnosis (age_at_enc_year);
create index esp_diagnosis_age_group_5yr_idx on esp_diagnosis (age_group_5yr);
create index esp_diagnosis_age_group_10yr_idx on esp_diagnosis (age_group_10yr);
create index esp_diagnosis_age_group_ms_idx on esp_diagnosis (age_group_ms);
CREATE INDEX exp_diagnosis_dx_like_idx ON esp_diagnosis USING btree (dx varchar_pattern_ops);
alter table esp_diagnosis add primary key (patid, encounterid, dx);
alter table esp_diagnosis add foreign key (patid) references esp_demographic (patid);
alter table esp_diagnosis add foreign key (encounterid) references esp_encounter (encounterid);

drop table if exists esp_disease cascade;
create table esp_disease as select t0.* from esp_disease_v
            as t0 inner join esp_demographic as t1
            on t0.patid=t1.patid;

--INSERT INTO esp_disease (select t0.* from esp_condition
--            as t0 inner join esp_demographic as t1
--            on t0.patid=t1.patid
--            where (current_date-('1960-01-01'::date) - t0.date <= 365));


 --POPULATE THE ESP DISEASE VIEW WITH OPIOID/BENZO CONDITIONS FOR THE LAST 365 DAYS (FROM ESP_CONDITION TABLE)
 INSERT INTO esp_disease (
             SELECT T1.centerid, T1.patid, T1.condition, T1.date, age_at_detect_year, age_group_5yr, age_group_10yr, age_group_ms, criteria, status, notes
             FROM esp_condition AS T1,
                       (SELECT patid, condition, max(date) date FROM esp_condition WHERE condition in ('benzodiarx', 'benzopiconcurrent', 'highopioiduse', 'opioidrx') AND (current_date-('1960-01-01'::date) - date <= 365) GROUP BY  patid, condition) AS T2,
             esp_demographic AS T3
             WHERE T1.patid = T2.patid
             AND T1.patid = T3.patid
             AND T1.date = T2.date
              AND T1.condition = T2.condition
             AND (current_date-('1960-01-01'::date) - T1.date <= 365)
             AND T1.condition in ('benzodiarx', 'benzopiconcurrent', 'highopioiduse', 'opioidrx')
 );


 --POPULATE THE ESP DISEASE VIEW WITH OBESITY CONDITIONS. SINGLE CONDITION PER PATIENT
 INSERT INTO esp_disease (
             SELECT DISTINCT ON (T1.patid) T1.centerid, T1.patid, condition, T1.date, age_at_detect_year, age_group_5yr, age_group_10yr, age_group_ms, criteria, status, notes
             FROM esp_condition AS T1,
             (SELECT patid, max(date) date FROM esp_condition WHERE condition ilike '%bmi%' GROUP BY  patid) AS T2,
             esp_demographic AS T3
             WHERE T1.patid = T2.patid
             AND T1.patid = T3.patid
             AND T1.date = T2.date
             AND T1.age_at_detect_year >= 20
             AND condition ilike '%bmi%');













create index esp_disease_age_group_10yr_idx on esp_disease (age_group_10yr);
create index esp_disease_age_group_5yr_idx on esp_disease (age_group_5yr);
create index esp_disease_age_group_ms_idx on esp_disease (age_group_ms);
create index esp_disease_centerid_idx on esp_disease (centerid);
create index esp_disease_patid_idx on esp_disease (patid);
create index esp_disease_condition_idx on esp_disease (condition);
create index esp_disease_date_idx on esp_disease (date);
create index esp_disease_age_at_detect_year_idx on esp_disease (age_at_detect_year);
create index esp_disease_criteria_idx on esp_disease (criteria);
create index esp_disease_status_idx on esp_disease (status);
alter table esp_disease add primary key (patid, condition, date);
alter table esp_disease add foreign key (patid) references esp_demographic (patid);

drop view esp_disease_v;
drop view esp_diagnosis_v;
drop view esp_encounter_v;
drop view esp_demographic_v;


-- UVT_TABLES
--    UVT_SEX
      DROP TABLE UVT_SEX;
      CREATE TABLE UVT_SEX AS
      SELECT DISTINCT
             pat.sex item_code,
             CASE
               WHEN pat.sex = 'M' THEN 'Male'::varchar(10)
               WHEN pat.sex = 'F' THEN 'Female'::varchar(10)
               WHEN pat.sex = 'U' THEN 'Unknown'::varchar(10)
               ELSE 'Not Mapped'::varchar(10)
             END item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_SEX ADD PRIMARY KEY (item_code);

--    UVT_RACE
      DROP TABLE UVT_RACE;
      CREATE TABLE UVT_RACE AS
      SELECT DISTINCT
             pat.race item_code,
             CASE
               WHEN pat.race = 0 THEN 'Unknown'::varchar(50)
               WHEN pat.race = 1 THEN 'American Indian or Alaska Native'::varchar(50)
               WHEN pat.race = 2 THEN 'Asian'::varchar(50)
               WHEN pat.race = 3 THEN 'Black or African American'::varchar(50)
               WHEN pat.race = 4 THEN 'Native Hawaiian or Other Pacific Islander'::varchar(50)
               WHEN pat.race = 5 THEN 'White'::varchar(50)
               ELSE 'Not Mapped'::varchar(50)
             END item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_RACE ADD PRIMARY KEY (item_code);

--    UVT_RACE_ETHNICITY
      DROP TABLE UVT_RACE_ETHNICITY;
      CREATE TABLE UVT_RACE_ETHNICITY AS
      SELECT DISTINCT
             pat.race_ethnicity item_code,
             CASE
               WHEN pat.race_ethnicity = 1 THEN 'Native American'::varchar(50) 
               WHEN pat.race_ethnicity = 2 THEN 'Asian'::varchar(50)
               WHEN pat.race_ethnicity = 3 THEN 'Black'::varchar(50)
               WHEN pat.race_ethnicity = 5 THEN 'White'::varchar(50)
               WHEN pat.race_ethnicity = 6 then 'Hispanic'::varchar(50)
               WHEN pat.race_ethnicity = 0 then 'Unknown'::varchar(50)
               ELSE 'Not Mapped'::varchar(50)
             END item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_RACE_ETHNICITY ADD PRIMARY KEY (item_code);

-- UVT_ZIP5
DROP TABLE if exists uvt_zip5;
create table uvt_zip5 
as select distinct 
zip5 as item_code, 
null::varchar(10) item_text
from esp_demographic where zip5 is not null;
alter table uvt_zip5 add primary key (item_code);


--    UVT_AGEGROUP_10YR
      DROP TABLE UVT_AGEGROUP_10YR;
      CREATE TABLE UVT_AGEGROUP_10YR AS
      SELECT DISTINCT
             enc.age_group_10yr item_code,
             enc.age_group_10yr::varchar(5) item_text
        FROM esp_encounter enc where enc.age_group_10yr is not null;
        ALTER TABLE UVT_AGEGROUP_10YR ADD PRIMARY KEY (item_code);




--    REMOTE KEYS USING UVTs
      ALTER TABLE esp_demographic ADD FOREIGN KEY (sex) REFERENCES uvt_sex (item_code);
      ALTER TABLE esp_demographic ADD FOREIGN KEY (race) REFERENCES uvt_race (item_code);
      ALTER TABLE esp_encounter ADD FOREIGN KEY (age_group_10yr) 
                  REFERENCES uvt_agegroup_10yr (item_code);

