#!/usr/bin/perl
# Author: keberhardt@commoninf.com

use strict;
use warnings;
use Data::Dumper;
use File::Copy;

#############################
## Set Your Variables Here ##
#############################

my ($encfile, $dxfile) = @ARGV;
#my $archive_dir = "/home/esp/data/epic/incoming/2020/April_2020/April_18_2020/archive";
my $archive_dir = "/srv/esp/data/epic/archive";

############################
## End Variable Set       ##
############################

#Sort out the filenames so we archive and make backups
my $dxfile_name = ( split '/', $dxfile )[ -1 ];
my $encfile_name = ( split '/', $encfile )[ -1 ];

my $archive_dx_path = $archive_dir . "/" . $dxfile_name;
my $archive_enc_path = $archive_dir . "/" . $encfile_name . ".orig";

#Make a backup of the encounter file before we overwrite it
copy($encfile,$archive_enc_path) or die "ERROR: The move of $encfile to $archive_enc_path failed: $!";


#Process the dx file and create a hash with the values
open(DXDATA, $dxfile) || die "ERROR: Can't open $dxfile: $!\n";

my %dxdata;

while(<DXDATA>) {
    chomp;
    my ($encounter, $codetype, $dxcode) = split(/\^/);
    #The following line may not be needed depending on input.
    chop($dxcode);
    my $codestring = $codetype. ":" .$dxcode . "\;";
    $dxdata{$encounter} .= $codestring;

}

#print Dumper(\%dxdata);

for my $enc_natural_key ( keys %dxdata ) {
        my $icd_string = $dxdata{$enc_natural_key};
}


close(DXDATA);


#Process the encounters to create the new row
open(ENCDATA, $encfile) || die "ERROR: Can't open $encfile: $!\n";

my @row_final;
while ( my $encline = <ENCDATA> ) {
    my @row = split( /\^/, $encline );
    my $enc_to_match = $row[2];
    if( exists($dxdata{$enc_to_match}) ){    
        $row[19] = $dxdata{$enc_to_match};
        @row =  join( '^', @row );
        push @row_final, @row;
    } else {
       @row = join( '^', @row );
       push @row_final, @row;
    }
}

close(ENCDATA);


open(ENCDATA, ">$encfile") || die "ERROR: Can't open $encfile: $!\n";

#Overwrtite the file with the new data

print ENCDATA @row_final;

close(ENCDATA);


#MOVE DX FILE TO ARCHIVE WHEN DONE
move($dxfile, $archive_dx_path) or die "ERROR: The move of $dxfile to $archive_dx_path failed: $!"; 

