CREATE SCHEMA IF NOT EXISTS gen_pop_tools;

create table gen_pop_tools.clin_enc
( patient_id integer
 ,date date
 ,source varchar(3),
 primary key(patient_id, date, source));
 
 
 
create or replace function gen_pop_tools.make_clin_enc(_pulldate text) 
returns date as $$
  declare
    cursrec record;
  begin
    --loop through lx, rx, dx, vs, immu tables by last updated timestamp and create clin_enc util table
    --for cursrec in select patient_id, date, 'lx'::varchar(3) as source
    for cursrec in select patient_id, coalesce(collection_date::date, result_date::date, date) as date, 'lx'::varchar(3) as source
                     from emr_labresult
                     --where date=_pulldate::date
                     where (collection_date::date=_pulldate::date
                            or (result_date::date=_pulldate::date and collection_date is null)
                            or (date=_pulldate::date and collection_date is null and result_date is null))
    loop
      begin
        insert into gen_pop_tools.clin_enc (patient_id, date, source)
          values (cursrec.patient_id, cursrec.date, cursrec.source);
      exception when unique_violation then
        --do nothing, just skip this record  
      end;
    end loop;
    for cursrec in select patient_id, date, 'rx'::varchar(3) as source 
                     from emr_prescription
                     where date=_pulldate::date
    loop
      begin
        insert into gen_pop_tools.clin_enc (patient_id, date, source)
          values (cursrec.patient_id, cursrec.date, cursrec.source);
      exception when unique_violation then
        --do nothing, just skip this record  
      end;
    end loop;
    for cursrec in select patient_id, date, 'imu'::varchar(3) as source 
                     from emr_immunization
                     where date=_pulldate::date
    loop
      begin
        insert into gen_pop_tools.clin_enc (patient_id, date, source)
          values (cursrec.patient_id, cursrec.date, cursrec.source);
      exception when unique_violation then
        --do nothing, just skip this record  
      end;
    end loop;
    for cursrec in select patient_id, date, 'enc'::varchar(3) as source 
                     from emr_encounter e
                     where date=_pulldate::date and substring(raw_encounter_type from 1 for 1) = 'Y' and ((e.weight>0 or e.height>0
                       or e.bp_systolic>0 or e.bp_diastolic>0
                       or e.temperature>0 or e.pregnant=TRUE or e.edd is not null)
                       or exists (select null from emr_encounter_dx_codes dx
                                  where dx.encounter_id=e.id and dx.dx_code_id<>'icd9:799.9'))
    loop
      begin
        insert into gen_pop_tools.clin_enc (patient_id, date, source)
          values (cursrec.patient_id, cursrec.date, cursrec.source);
      exception when unique_violation then
        --do nothing, just skip this record  
      end;
    end loop;
	return _pulldate::date;
  end;
$$ language plpgsql
