

--Jan 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 1/1/2014
drop table if exists temp_hivreport_haslabtestresults_01012014;
create table temp_hivreport_haslabtestresults_01012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JAN-2013' and date < '01-JAN-2014';
select count(*) from temp_hivreport_haslabtestresults_01012014;
--23888

--Jan 2014
-- 2. Create the table of patients who have HIV prior to 1/1/2014
drop table if exists temp_hivreport_hashivcase_01012014;
create table temp_hivreport_hashivcase_01012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JAN-2014';
select count(*) from temp_hivreport_hashivcase_01012014;
--3873

--Jan 2014-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 1/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 1/1/2014
-- and are NOT in the list of patients who have HIV before 1/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_01012014;
create table temp_hiv_report2_firstsubsetpats_01012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JAN-2013'    
and emr_encounter.date < '01-JAN-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_01012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_01012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_01012014;
--309,614

--Jan 2014
--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_01012014;
create table temp_hiv_report2_masterpats_01012014 as select temp_hiv_report2_firstsubsetpats_01012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_01012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_01012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JAN-2014' 
and emr_encounter.date < '01-Feb-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_01012014;
--95,332

--Jan 2014
--4.  BUILD COLUMN 1 2014
drop table if exists temp_hiv_report2_col1_01012014;
create table temp_hiv_report2_col1_01012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_01012014
group by center_id, dategroup order by center_id, dategroup;

 

--Jan 2014
-- 5. Prep COLUMN 2 01012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_01012014;
create table temp_hiv_report2_col2_masterpats_01012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2014'   
and emr_labresult.date < '01-FEB-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_01012014;
--1326

--Jan 2014
--6. Column 2 Report 2 0102014 
drop table if exists temp_hiv_report2_col2_01012014;
create table temp_hiv_report2_col2_01012014 
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_01012014
group by center_id, dategroup;


--Jan 2014
--7. Prep Column 3 01012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_01012014;
create table temp_hiv_report2_col3_haselisaagab_01012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2014'   
and emr_labresult.date < '01-FEB-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_01012014;
--1309


--Jan 2014
--8. Create master table for column 3 01012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_01012014;
create table temp_hiv_report2_col3_masterpats_01012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_01012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_01012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2014'   
and emr_labresult.date < '01-FEB-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_01012014);
select count(*) from temp_hiv_report2_col3_masterpats_01012014;
--80


--Jan 2014-- 9. Column 3 Report 2 For Jan 2014
drop table if exists temp_hiv_report2_col3_01012014;
create table temp_hiv_report2_col3_01012014 
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_01012014
group by center_id, dategroup;


--Jan 2014
--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_01012014;
create table temp_hiv_report2_col4_masterpats_01012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_01012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_01012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JAN-2014'
and hef_event.date < '01-FEB-2014';
select count(*) from temp_hiv_report2_col4_masterpats_01012014;
--7


--Jan 2014-- 11. Column 4 Report 2 for Jan 2014
drop table if exists temp_hiv_report2_col4_01012014;
create table temp_hiv_report2_col4_01012014 
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_01012014
group by center_id, dategroup;


--Jan 2014
---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--Jan 2014
--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012014
where temp_hiv_report2_masterpats_01012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JAN-2014'
and hef_event.date < '31-JAN-2014'::date + interval '60 days';

--Jan 2014
--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012014
where temp_hiv_report2_masterpats_01012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JAN-2014'
and hef_event.date < '31-JAN-2014'::date + interval '60 days';


--Jan 2014
--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_01012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_01012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JAN-2014'
and emr_labresult.date < '31-JAN-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
--Jan 2014
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012014
where temp_hiv_report2_masterpats_01012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JAN-2014'
and hef_event.date < '31-JAN-2014'::date + interval '60 days';


--Jan 2014--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_01012014
where temp_hiv_report2_masterpats_01012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JAN-2014'
and hef_event.date < '31-JAN-2014'::date + interval '60 days';


--Jan 2014
--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS AGAB
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--Jan 2014
--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--Jan 2014
--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--Jan 2014
--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--Jan 2014
--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_01012014;
create table temp_hiv_report2_col5_source_01012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--Jan 2014
--22.  Refine column 5 data to get minimum/first date
--Make sure the dates are within 60 days of each other
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_01012014
where datea-dateb<=60;

--Jan 2014
--23  Put into final form
drop table temp_hiv_report2_col5_01012014;
create table temp_hiv_report2_col5_01012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JAN-2014'
group by center_id, dategroup;


--REPORT 2:
drop table if exists temp_hiv_report2_01012014;
create table temp_hiv_report2_01012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_01012014 a
left join temp_hiv_report2_col2_01012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_01012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_01012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_01012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

---------------------------------------------------------------------------

--Feb 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 2/1/2014
drop table if exists temp_hivreport_haslabtestresults_02012014;
create table temp_hivreport_haslabtestresults_02012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-FEB-2013' and date < '01-FEB-2014';
select count(*) from temp_hivreport_haslabtestresults_02012014;
--24822

-- 2. Create the table of patients who have HIV prior to 2/1/2014
drop table if exists temp_hivreport_hashivcase_02012014;
create table temp_hivreport_hashivcase_02012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-FEB-2014';
select count(*) from temp_hivreport_hashivcase_02012014;
--3910

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 2/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 2/1/2014
-- and are NOT in the list of patients who have HIV before 2/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_02012014;
create table temp_hiv_report2_firstsubsetpats_02012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-FEB-2013'    
and emr_encounter.date < '01-FEB-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_02012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_02012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_02012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--FEB 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_02012014;
create table temp_hiv_report2_masterpats_02012014 as 
select temp_hiv_report2_firstsubsetpats_02012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_02012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_02012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-FEB-2014' 
and emr_encounter.date < '01-MAR-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_02012014;
--93,799

--4.  BUILD COLUMN 1 Feb 2014
drop table if exists temp_hiv_report2_col1_02012014;
create table temp_hiv_report2_col1_02012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_02012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 02012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_02012014;
create table temp_hiv_report2_col2_masterpats_02012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2014'   
and emr_labresult.date < '01-MAR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_02012014;
--1488

--6. Column 2 Report 2 0202014 
drop table if exists temp_hiv_report2_col2_02012014;
create table temp_hiv_report2_col2_02012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_02012014
group by center_id, dategroup;

--7. Prep Column 3 02012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_02012014;
create table temp_hiv_report2_col3_haselisaagab_02012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2014'   
and emr_labresult.date < '01-MAR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_02012014;
--1475


--8. Create master table for column 3 02012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_02012014;
create table temp_hiv_report2_col3_masterpats_02012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_02012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_02012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2014'   
and emr_labresult.date < '01-MAR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_02012014);
select count(*) from temp_hiv_report2_col3_masterpats_02012014;
--84

-- 9. Column 3 Report 2 For Feb 2014
drop table if exists temp_hiv_report2_col3_02012014;
create table temp_hiv_report2_col3_02012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_02012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_02012014;
create table temp_hiv_report2_col4_masterpats_02012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_02012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_02012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-FEB-2014'
and hef_event.date < '01-MAR-2014';
select count(*) from temp_hiv_report2_col4_masterpats_02012014;
--100

-- 11. Column 4 Report 2 for Feb 2014
drop table if exists temp_hiv_report2_col4_02012014;
create table temp_hiv_report2_col4_02012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_02012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012014
where temp_hiv_report2_masterpats_02012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-FEB-2014'
and hef_event.date < '28-FEB-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012014
where temp_hiv_report2_masterpats_02012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-FEB-2014'
and hef_event.date < '28-FEB-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_02012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_02012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-FEB-2014'
and emr_labresult.date < '28-FEB-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012014
where temp_hiv_report2_masterpats_02012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-FEB-2014'
and hef_event.date < '28-FEB-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_02012014
where temp_hiv_report2_masterpats_02012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-FEB-2014'
and hef_event.date < '28-FEB-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_02012014;
create table temp_hiv_report2_col5_source_02012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_02012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_02012014;
create table temp_hiv_report2_col5_02012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-FEB-2014'
group by center_id, dategroup;


--REPORT 2:  Feb 2014
drop table if exists temp_hiv_report2_02012014;
create table temp_hiv_report2_02012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_02012014 a
left join temp_hiv_report2_col2_02012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_02012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_02012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_02012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

---------------------------------------------------------------------------

--MAR 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 3/1/2014
drop table if exists temp_hivreport_haslabtestresults_03012014;
create table temp_hivreport_haslabtestresults_03012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-MAR-2013' and date < '01-MAR-2014';
select count(*) from temp_hivreport_haslabtestresults_03012014;
--26106

-- 2. Create the table of patients who have HIV prior to 3/1/2014
drop table if exists temp_hivreport_hashivcase_03012014;
create table temp_hivreport_hashivcase_03012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-MAR-2014';
select count(*) from temp_hivreport_hashivcase_03012014;
--3938

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 3/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 3/1/2014
-- and are NOT in the list of patients who have HIV before 3/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_03012014;
create table temp_hiv_report2_firstsubsetpats_03012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-MAR-2013'    
and emr_encounter.date < '01-MAR-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_03012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_03012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_03012014;
--304,481

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--MAR 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_03012014;
create table temp_hiv_report2_masterpats_03012014 as 
select temp_hiv_report2_firstsubsetpats_03012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_03012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_03012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-MAR-2014' 
and emr_encounter.date < '01-APR-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_03012014;
--109,497

--4.  BUILD COLUMN 1 MAR 2014
drop table if exists temp_hiv_report2_col1_03012014;
create table temp_hiv_report2_col1_03012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_03012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 03012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_03012014;
create table temp_hiv_report2_col2_masterpats_03012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2014'   
and emr_labresult.date < '01-APR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_03012014;
--1565

--6. Column 2 Report 2 0302014 
drop table if exists temp_hiv_report2_col2_03012014;
create table temp_hiv_report2_col2_03012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_03012014
group by center_id, dategroup;

--7. Prep Column 3 03012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_03012014;
create table temp_hiv_report2_col3_haselisaagab_03012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2014'   
and emr_labresult.date < '01-APR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_03012014;
--1550


--8. Create master table for column 3 03012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_03012014;
create table temp_hiv_report2_col3_masterpats_03012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_03012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_03012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2014'   
and emr_labresult.date < '01-APR-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_03012014);
select count(*) from temp_hiv_report2_col3_masterpats_03012014;
--67

-- 9. Column 3 Report 2 For MAR 2014
drop table if exists temp_hiv_report2_col3_03012014;
create table temp_hiv_report2_col3_03012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_03012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_03012014;
create table temp_hiv_report2_col4_masterpats_03012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_03012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_03012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-MAR-2014'
and hef_event.date < '01-APR-2014';
select count(*) from temp_hiv_report2_col4_masterpats_03012014;
--48

-- 11. Column 4 Report 2 for MAR 2014
drop table if exists temp_hiv_report2_col4_03012014;
create table temp_hiv_report2_col4_03012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_03012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012014
where temp_hiv_report2_masterpats_03012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-MAR-2014'
and hef_event.date < '31-MAR-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012014
where temp_hiv_report2_masterpats_03012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-MAR-2014'
and hef_event.date < '31-MAR-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_03012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_03012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAR-2014'
and emr_labresult.date < '31-MAR-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012014
where temp_hiv_report2_masterpats_03012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-MAR-2014'
and hef_event.date < '31-MAR-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_03012014
where temp_hiv_report2_masterpats_03012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-MAR-2014'
and hef_event.date < '31-MAR-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_03012014;
create table temp_hiv_report2_col5_source_03012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_03012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_03012014;
create table temp_hiv_report2_col5_03012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-MAR-2014'
group by center_id, dategroup;


--REPORT 2:  MAR 2014
drop table if exists temp_hiv_report2_03012014;
create table temp_hiv_report2_03012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_03012014 a
left join temp_hiv_report2_col2_03012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_03012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_03012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_03012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------

--APRIL 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 4/1/2014
drop table if exists temp_hivreport_haslabtestresults_04012014;
create table temp_hivreport_haslabtestresults_04012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-APR-2013' and date < '01-APR-2014';
select count(*) from temp_hivreport_haslabtestresults_04012014;
--27344

-- 2. Create the table of patients who have HIV prior to 4/1/2014
drop table if exists temp_hivreport_hashivcase_04012014;
create table temp_hivreport_hashivcase_04012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-APR-2014';
select count(*) from temp_hivreport_hashivcase_04012014;
--3968

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 4/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 4/1/2014
-- and are NOT in the list of patients who have HIV before 4/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_04012014;
create table temp_hiv_report2_firstsubsetpats_04012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-APR-2013'    
and emr_encounter.date < '01-APR-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_04012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_04012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_04012014;
--298280

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--APR 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_04012014;
create table temp_hiv_report2_masterpats_04012014 as 
select temp_hiv_report2_firstsubsetpats_04012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_04012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_04012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-APR-2014' 
and emr_encounter.date < '01-MAY-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_04012014;
--96,203

--4.  BUILD COLUMN 1 APR 2014
drop table if exists temp_hiv_report2_col1_04012014;
create table temp_hiv_report2_col1_04012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_04012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 04012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_04012014;
create table temp_hiv_report2_col2_masterpats_04012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2014'   
and emr_labresult.date < '01-MAY-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_04012014;
--1358

--6. Column 2 Report 2 0302014 
drop table if exists temp_hiv_report2_col2_04012014;
create table temp_hiv_report2_col2_04012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_04012014
group by center_id, dategroup;

--7. Prep Column 3 04012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_04012014;
create table temp_hiv_report2_col3_haselisaagab_04012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2014'   
and emr_labresult.date < '01-MAY-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_04012014;
--1339

--8. Create master table for column 3 04012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_04012014;
create table temp_hiv_report2_col3_masterpats_04012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_04012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_04012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2014'   
and emr_labresult.date < '01-MAY-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_04012014);
select count(*) from temp_hiv_report2_col3_masterpats_04012014;
--90

-- 9. Column 3 Report 2 For APR 2014
drop table if exists temp_hiv_report2_col3_04012014;
create table temp_hiv_report2_col3_04012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_04012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_04012014;
create table temp_hiv_report2_col4_masterpats_04012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_04012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_04012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-APR-2014'
and hef_event.date < '01-MAY-2014';
select count(*) from temp_hiv_report2_col4_masterpats_04012014;
--45

-- 11. Column 4 Report 2 for APR 2014
drop table if exists temp_hiv_report2_col4_04012014;
create table temp_hiv_report2_col4_04012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_04012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012014
where temp_hiv_report2_masterpats_04012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-APR-2014'
and hef_event.date < '30-APR-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012014
where temp_hiv_report2_masterpats_04012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-APR-2014'
and hef_event.date < '30-APR-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_04012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_04012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-APR-2014'
and emr_labresult.date < '30-APR-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012014
where temp_hiv_report2_masterpats_04012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-APR-2014'
and hef_event.date < '30-APR-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_04012014
where temp_hiv_report2_masterpats_04012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-APR-2014'
and hef_event.date < '30-APR-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_04012014;
create table temp_hiv_report2_col5_source_04012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_04012014
where datea-dateb<=60;

--23  Put into final form
drop table if exists temp_hiv_report2_col5_04012014;
create table temp_hiv_report2_col5_04012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-APR-2014'
group by center_id, dategroup;


--REPORT 2:  APR 2014
drop table if exists temp_hiv_report2_04012014;
create table temp_hiv_report2_04012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_04012014 a
left join temp_hiv_report2_col2_04012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_04012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_04012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_04012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------



--MAY 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 5/1/2014
drop table if exists temp_hivreport_haslabtestresults_05012014;
create table temp_hivreport_haslabtestresults_05012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-MAY-2013' and date < '01-MAY-2014';
select count(*) from temp_hivreport_haslabtestresults_05012014;
--28281

-- 2. Create the table of patients who have HIV prior to 5/1/2014
drop table if exists temp_hivreport_hashivcase_05012014;
create table temp_hivreport_hashivcase_05012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-MAY-2014';
select count(*) from temp_hivreport_hashivcase_05012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 5/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 5/1/2014
-- and are NOT in the list of patients who have HIV before /1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_05012014;
create table temp_hiv_report2_firstsubsetpats_05012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-MAY-2013'    
and emr_encounter.date < '01-MAY-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_05012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_05012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_05012014;
--291,344

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--MAY 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_05012014;
create table temp_hiv_report2_masterpats_05012014 as 
select temp_hiv_report2_firstsubsetpats_05012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_05012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_05012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-MAY-2014' 
and emr_encounter.date < '01-JUN-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_05012014;
--93,313

--4.  BUILD COLUMN 1 MAY 2014
drop table if exists temp_hiv_report2_col1_05012014;
create table temp_hiv_report2_col1_05012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_05012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 05012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_05012014;
create table temp_hiv_report2_col2_masterpats_05012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2014'   
and emr_labresult.date < '01-JUN-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_05012014;
--1467

--6. Column 2 Report 2 0302014 
drop table if exists temp_hiv_report2_col2_05012014;
create table temp_hiv_report2_col2_05012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_05012014
group by center_id, dategroup;

--7. Prep Column 3 05012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_05012014;
create table temp_hiv_report2_col3_haselisaagab_05012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2014'   
and emr_labresult.date < '01-JUN-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_05012014;
--1455


--8. Create master table for column 3 05012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_05012014;
create table temp_hiv_report2_col3_masterpats_05012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_05012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_05012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2014'   
and emr_labresult.date < '01-JUN-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_05012014);
select count(*) from temp_hiv_report2_col3_masterpats_05012014;
--74

-- 9. Column 3 Report 2 For MAY 2014
drop table if exists temp_hiv_report2_col3_05012014;
create table temp_hiv_report2_col3_05012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_05012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_05012014;
create table temp_hiv_report2_col4_masterpats_05012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_05012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_05012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-MAY-2014'
and hef_event.date < '01-JUN-2014';
select count(*) from temp_hiv_report2_col4_masterpats_05012014;
--36

-- 11. Column 4 Report 2 for MAY 2014
drop table if exists temp_hiv_report2_col4_05012014;
create table temp_hiv_report2_col4_05012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_05012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012014
where temp_hiv_report2_masterpats_05012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-MAY-2014'
and hef_event.date < '31-MAY-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012014
where temp_hiv_report2_masterpats_05012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-MAY-2014'
and hef_event.date < '31-MAY-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_05012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_05012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-MAY-2014'
and emr_labresult.date < '31-MAY-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012014
where temp_hiv_report2_masterpats_05012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-MAY-2014'
and hef_event.date < '31-MAY-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_05012014
where temp_hiv_report2_masterpats_05012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-MAY-2014'
and hef_event.date < '31-MAY-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_05012014;
create table temp_hiv_report2_col5_source_05012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_05012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_05012014;
create table temp_hiv_report2_col5_05012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-MAY-2014'
group by center_id, dategroup;


--REPORT 2:  MAY 2014
drop table if exists temp_hiv_report2_05012014;
create table temp_hiv_report2_05012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_05012014 a
left join temp_hiv_report2_col2_05012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_05012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_05012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_05012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------



--JUNE 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 6/1/2014
drop table if exists temp_hivreport_haslabtestresults_06012014;
create table temp_hivreport_haslabtestresults_06012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JUN-2013' and date < '01-JUN-2014';
select count(*) from temp_hivreport_haslabtestresults_06012014;
--29513

-- 2. Create the table of patients who have HIV prior to 6/1/2014
drop table if exists temp_hivreport_hashivcase_06012014;
create table temp_hivreport_hashivcase_06012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JUN-2014';
select count(*) from temp_hivreport_hashivcase_06012014;
--4019

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 6/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_06012014;
create table temp_hiv_report2_firstsubsetpats_06012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JUN-2013'    
and emr_encounter.date < '01-JUN-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_06012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_06012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_06012014;
--285237

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--JUNE 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_06012014;
create table temp_hiv_report2_masterpats_06012014 as 
select temp_hiv_report2_firstsubsetpats_06012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_06012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_06012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JUN-2014' 
and emr_encounter.date < '01-JUL-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_06012014;
--93,799

--4.  BUILD COLUMN 1 JUN 2014
drop table if exists temp_hiv_report2_col1_06012014;
create table temp_hiv_report2_col1_06012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_06012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 06012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_06012014;
create table temp_hiv_report2_col2_masterpats_06012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2014'   
and emr_labresult.date < '01-JUL-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_06012014;
--1488

--6. Column 2 Report 2 06012014 
drop table if exists temp_hiv_report2_col2_06012014;
create table temp_hiv_report2_col2_06012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_06012014
group by center_id, dategroup;

--7. Prep Column 3 06012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_06012014;
create table temp_hiv_report2_col3_haselisaagab_06012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2014'   
and emr_labresult.date < '01-JUL-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_06012014;
--1475


--8. Create master table for column 3 06012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_06012014;
create table temp_hiv_report2_col3_masterpats_06012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_06012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_06012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2014'   
and emr_labresult.date < '01-JUL-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_06012014);
select count(*) from temp_hiv_report2_col3_masterpats_06012014;
--84

-- 9. Column 3 Report 2 For JUN 2014
drop table if exists temp_hiv_report2_col3_06012014;
create table temp_hiv_report2_col3_06012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_06012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_06012014;
create table temp_hiv_report2_col4_masterpats_06012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_06012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_06012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JUN-2014'
and hef_event.date < '01-JUL-2014';
select count(*) from temp_hiv_report2_col4_masterpats_06012014;
--100

-- 11. Column 4 Report 2 for JUN 2014
drop table if exists temp_hiv_report2_col4_06012014;
create table temp_hiv_report2_col4_06012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_06012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012014
where temp_hiv_report2_masterpats_06012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JUN-2014'
and hef_event.date < '30-JUN-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012014
where temp_hiv_report2_masterpats_06012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JUN-2014'
and hef_event.date < '30-JUN-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_06012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_06012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUN-2014'
and emr_labresult.date < '30-JUN-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012014
where temp_hiv_report2_masterpats_06012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JUN-2014'
and hef_event.date < '30-JUN-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_06012014
where temp_hiv_report2_masterpats_06012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JUN-2014'
and hef_event.date < '30-JUN-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_06012014;
create table temp_hiv_report2_col5_source_06012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_06012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_06012014;
create table temp_hiv_report2_col5_06012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JUN-2014'
group by center_id, dategroup;


--REPORT 2:  JUN 2014
drop table if exists temp_hiv_report2_06012014;
create table temp_hiv_report2_06012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_06012014 a
left join temp_hiv_report2_col2_06012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_06012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_06012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_06012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------

--JULY 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 7/1/2014
drop table if exists temp_hivreport_haslabtestresults_07012014;
create table temp_hivreport_haslabtestresults_07012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-JUL-2013' and date < '01-JUL-2014';
select count(*) from temp_hivreport_haslabtestresults_07012014;
--28281

-- 2. Create the table of patients who have HIV prior to 7/1/2014
drop table if exists temp_hivreport_hashivcase_07012014;
create table temp_hivreport_hashivcase_07012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-JUL-2014';
select count(*) from temp_hivreport_hashivcase_07012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 7/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_07012014;
create table temp_hiv_report2_firstsubsetpats_07012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-JUL-2013'    
and emr_encounter.date < '01-AUG-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_07012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_07012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_07012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--JUL 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_07012014;
create table temp_hiv_report2_masterpats_07012014 as 
select temp_hiv_report2_firstsubsetpats_07012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_07012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_07012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-JUL-2014' 
and emr_encounter.date < '01-AUG-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_07012014;
--93,799

--4.  BUILD COLUMN 1 JUL 2014
drop table if exists temp_hiv_report2_col1_07012014;
create table temp_hiv_report2_col1_07012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_07012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 07012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_07012014;
create table temp_hiv_report2_col2_masterpats_07012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2014'   
and emr_labresult.date < '01-AUG-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_07012014;
--1488

--6. Column 2 Report 2 07012014 
drop table if exists temp_hiv_report2_col2_07012014;
create table temp_hiv_report2_col2_07012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_07012014
group by center_id, dategroup;

--7. Prep Column 3 07012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_07012014;
create table temp_hiv_report2_col3_haselisaagab_07012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2014'   
and emr_labresult.date < '01-AUG-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_07012014;
--1475


--8. Create master table for column 3 07012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_07012014;
create table temp_hiv_report2_col3_masterpats_07012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_07012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_07012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2014'   
and emr_labresult.date < '01-AUG-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_07012014);
select count(*) from temp_hiv_report2_col3_masterpats_07012014;
--84

-- 9. Column 3 Report 2 For JUL 2014
drop table if exists temp_hiv_report2_col3_07012014;
create table temp_hiv_report2_col3_07012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_07012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_07012014;
create table temp_hiv_report2_col4_masterpats_07012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_07012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_07012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-JUL-2014'
and hef_event.date < '01-AUG-2014';
select count(*) from temp_hiv_report2_col4_masterpats_07012014;
--100

-- 11. Column 4 Report 2 for JUL 2014
drop table if exists temp_hiv_report2_col4_07012014;
create table temp_hiv_report2_col4_07012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_07012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012014
where temp_hiv_report2_masterpats_07012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-JUL-2014'
and hef_event.date < '31-JUL-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012014
where temp_hiv_report2_masterpats_07012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-JUL-2014'
and hef_event.date < '31-JUL-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_07012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_07012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-JUL-2014'
and emr_labresult.date < '31-JUL-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012014
where temp_hiv_report2_masterpats_07012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-JUL-2014'
and hef_event.date < '31-JUL-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_07012014
where temp_hiv_report2_masterpats_07012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-JUL-2014'
and hef_event.date < '31-JUL-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_07012014;
create table temp_hiv_report2_col5_source_07012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_07012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_07012014;
create table temp_hiv_report2_col5_07012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-JUL-2014'
group by center_id, dategroup;


--REPORT 2:  JUL 2014
drop table if exists temp_hiv_report2_07012014;
create table temp_hiv_report2_07012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_07012014 a
left join temp_hiv_report2_col2_07012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_07012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_07012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_07012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

--AUG 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 8/1/2014
drop table if exists temp_hivreport_haslabtestresults_08012014;
create table temp_hivreport_haslabtestresults_08012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-AUG-2013' and date < '01-AUG-2014';
select count(*) from temp_hivreport_haslabtestresults_08012014;
--28281

-- 2. Create the table of patients who have HIV prior to 7/1/2014
drop table if exists temp_hivreport_hashivcase_08012014;
create table temp_hivreport_hashivcase_08012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-AUG-2014';
select count(*) from temp_hivreport_hashivcase_08012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 7/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_08012014;
create table temp_hiv_report2_firstsubsetpats_08012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-AUG-2013'    
and emr_encounter.date < '01-SEP-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_08012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_08012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_08012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--AUG 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_08012014;
create table temp_hiv_report2_masterpats_08012014 as 
select temp_hiv_report2_firstsubsetpats_08012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_08012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_08012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-AUG-2014' 
and emr_encounter.date < '01-SEP-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_08012014;
--93,799

--4.  BUILD COLUMN 1 AUG 2014
drop table if exists temp_hiv_report2_col1_08012014;
create table temp_hiv_report2_col1_08012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_08012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 08012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_08012014;
create table temp_hiv_report2_col2_masterpats_08012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2014'   
and emr_labresult.date < '01-SEP-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_08012014;
--1488

--6. Column 2 Report 2 08012014 
drop table if exists temp_hiv_report2_col2_08012014;
create table temp_hiv_report2_col2_08012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_08012014
group by center_id, dategroup;

--7. Prep Column 3 08012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_08012014;
create table temp_hiv_report2_col3_haselisaagab_08012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2014'   
and emr_labresult.date < '01-SEP-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_08012014;
--1475


--8. Create master table for column 3 08012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_08012014;
create table temp_hiv_report2_col3_masterpats_08012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_08012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_08012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2014'   
and emr_labresult.date < '01-SEP-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_08012014);
select count(*) from temp_hiv_report2_col3_masterpats_08012014;
--84

-- 9. Column 3 Report 2 For AUG 2014
drop table if exists temp_hiv_report2_col3_08012014;
create table temp_hiv_report2_col3_08012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_08012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_08012014;
create table temp_hiv_report2_col4_masterpats_08012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_08012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_08012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-AUG-2014'
and hef_event.date < '01-SEP-2014';
select count(*) from temp_hiv_report2_col4_masterpats_08012014;
--100

-- 11. Column 4 Report 2 for AUG 2014
drop table if exists temp_hiv_report2_col4_08012014;
create table temp_hiv_report2_col4_08012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_08012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012014
where temp_hiv_report2_masterpats_08012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-AUG-2014'
and hef_event.date < '31-AUG-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012014
where temp_hiv_report2_masterpats_08012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-AUG-2014'
and hef_event.date < '31-AUG-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_08012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_08012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-AUG-2014'
and emr_labresult.date < '31-AUG-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012014
where temp_hiv_report2_masterpats_08012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-AUG-2014'
and hef_event.date < '31-AUG-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_08012014
where temp_hiv_report2_masterpats_08012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-AUG-2014'
and hef_event.date < '31-AUG-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_08012014;
create table temp_hiv_report2_col5_source_08012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_08012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_08012014;
create table temp_hiv_report2_col5_08012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-AUG-2014'
group by center_id, dategroup;


--REPORT 2:  AUG 2014
drop table if exists temp_hiv_report2_08012014;
create table temp_hiv_report2_08012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_08012014 a
left join temp_hiv_report2_col2_08012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_08012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_08012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_08012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

--SEPT 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 9/1/2014
drop table if exists temp_hivreport_haslabtestresults_09012014;
create table temp_hivreport_haslabtestresults_09012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-SEP-2013' and date < '01-SEP-2014';
select count(*) from temp_hivreport_haslabtestresults_09012014;
--28281

-- 2. Create the table of patients who have HIV prior to 7/1/2014
drop table if exists temp_hivreport_hashivcase_09012014;
create table temp_hivreport_hashivcase_09012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-SEP-2014';
select count(*) from temp_hivreport_hashivcase_09012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 9/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_09012014;
create table temp_hiv_report2_firstsubsetpats_09012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-SEP-2013'    
and emr_encounter.date < '01-OCT-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_09012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_09012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_09012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--SEP 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_09012014;
create table temp_hiv_report2_masterpats_09012014 as 
select temp_hiv_report2_firstsubsetpats_09012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_09012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_09012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-SEP-2014' 
and emr_encounter.date < '01-OCT-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_09012014;
--93,799

--4.  BUILD COLUMN 1 SEP 2014
drop table if exists temp_hiv_report2_col1_09012014;
create table temp_hiv_report2_col1_09012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_09012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 09012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_09012014;
create table temp_hiv_report2_col2_masterpats_09012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_09012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_09012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-SEP-2014'   
and emr_labresult.date < '01-OCT-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_09012014;
--1488

--6. Column 2 Report 2 09012014 
drop table if exists temp_hiv_report2_col2_09012014;
create table temp_hiv_report2_col2_09012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_09012014
group by center_id, dategroup;

--7. Prep Column 3 09012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_09012014;
create table temp_hiv_report2_col3_haselisaagab_09012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_09012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_09012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-SEP-2014'   
and emr_labresult.date < '01-OCT-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_09012014;
--1475


--8. Create master table for column 3 09012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_09012014;
create table temp_hiv_report2_col3_masterpats_09012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_09012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_09012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-SEP-2014'   
and emr_labresult.date < '01-OCT-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_09012014);
select count(*) from temp_hiv_report2_col3_masterpats_09012014;
--84

-- 9. Column 3 Report 2 For SEP 2014
drop table if exists temp_hiv_report2_col3_09012014;
create table temp_hiv_report2_col3_09012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_09012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_09012014;
create table temp_hiv_report2_col4_masterpats_09012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_09012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_09012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-SEP-2014'
and hef_event.date < '01-OCT-2014';
select count(*) from temp_hiv_report2_col4_masterpats_09012014;
--100

-- 11. Column 4 Report 2 for SEP 2014
drop table if exists temp_hiv_report2_col4_09012014;
create table temp_hiv_report2_col4_09012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_09012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_09012014
where temp_hiv_report2_masterpats_09012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-SEP-2014'
and hef_event.date < '30-SEP-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_09012014
where temp_hiv_report2_masterpats_09012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-SEP-2014'
and hef_event.date < '30-SEP-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_09012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_09012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-SEP-2014'
and emr_labresult.date < '30-SEP-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_09012014
where temp_hiv_report2_masterpats_09012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-SEP-2014'
and hef_event.date < '30-SEP-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_09012014
where temp_hiv_report2_masterpats_09012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-SEP-2014'
and hef_event.date < '30-SEP-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_09012014;
create table temp_hiv_report2_col5_source_09012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_09012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_09012014;
create table temp_hiv_report2_col5_09012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-SEP-2014'
group by center_id, dategroup;


--REPORT 2:  SEP 2014
drop table if exists temp_hiv_report2_09012014;
create table temp_hiv_report2_09012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_09012014 a
left join temp_hiv_report2_col2_09012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_09012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_09012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_09012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------


--OCT 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 10/1/2014
drop table if exists temp_hivreport_haslabtestresults_10012014;
create table temp_hivreport_haslabtestresults_10012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-OCT-2013' and date < '01-OCT-2014';
select count(*) from temp_hivreport_haslabtestresults_10012014;
--28281

-- 2. Create the table of patients who have HIV prior to 10/1/2014
drop table if exists temp_hivreport_hashivcase_10012014;
create table temp_hivreport_hashivcase_10012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-OCT-2014';
select count(*) from temp_hivreport_hashivcase_10012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 11/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_10012014;
create table temp_hiv_report2_firstsubsetpats_10012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-OCT-2013'    
and emr_encounter.date < '01-OCT-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_10012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_10012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_10012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--OCT 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_10012014;
create table temp_hiv_report2_masterpats_10012014 as 
select temp_hiv_report2_firstsubsetpats_10012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_10012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_10012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-OCT-2014' 
and emr_encounter.date < '01-NOV-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_10012014;
--93,799

--4.  BUILD COLUMN 1 OCT 2014
drop table if exists temp_hiv_report2_col1_10012014;
create table temp_hiv_report2_col1_10012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_10012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 10012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_10012014;
create table temp_hiv_report2_col2_masterpats_10012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_10012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_10012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-OCT-2014'   
and emr_labresult.date < '01-NOV-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_10012014;
--1488

--6. Column 2 Report 2 10012014 
drop table if exists temp_hiv_report2_col2_10012014;
create table temp_hiv_report2_col2_10012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_10012014
group by center_id, dategroup;

--7. Prep Column 3 10012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_10012014;
create table temp_hiv_report2_col3_haselisaagab_10012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_10012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_10012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-OCT-2014'   
and emr_labresult.date < '01-NOV-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_10012014;
--1475


--8. Create master table for column 3 10012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_10012014;
create table temp_hiv_report2_col3_masterpats_10012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_10012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_10012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-OCT-2014'   
and emr_labresult.date < '01-NOV-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_10012014);
select count(*) from temp_hiv_report2_col3_masterpats_10012014;
--84

-- 9. Column 3 Report 2 For OCT 2014
drop table if exists temp_hiv_report2_col3_10012014;
create table temp_hiv_report2_col3_10012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_10012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_10012014;
create table temp_hiv_report2_col4_masterpats_10012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_10012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_10012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-OCT-2014'
and hef_event.date < '01-NOV-2014';
select count(*) from temp_hiv_report2_col4_masterpats_10012014;
--100

-- 11. Column 4 Report 2 for OCT 2014
drop table if exists temp_hiv_report2_col4_10012014;
create table temp_hiv_report2_col4_10012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_10012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_10012014
where temp_hiv_report2_masterpats_10012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-OCT-2014'
and hef_event.date < '31-OCT-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_10012014
where temp_hiv_report2_masterpats_10012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-OCT-2014'
and hef_event.date < '31-OCT-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_10012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_10012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-OCT-2014'
and emr_labresult.date < '31-OCT-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_10012014
where temp_hiv_report2_masterpats_10012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-OCT-2014'
and hef_event.date < '31-OCT-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_10012014
where temp_hiv_report2_masterpats_10012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-OCT-2014'
and hef_event.date < '31-OCT-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_10012014;
create table temp_hiv_report2_col5_source_10012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_10012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_10012014;
create table temp_hiv_report2_col5_10012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-OCT-2014'
group by center_id, dategroup;


--REPORT 2:  OCT 2014
drop table if exists temp_hiv_report2_10012014;
create table temp_hiv_report2_10012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_10012014 a
left join temp_hiv_report2_col2_10012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_10012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_10012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_10012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;

-------------------------------------------------------------------------------

--NOV 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 11/1/2014
drop table if exists temp_hivreport_haslabtestresults_11012014;
create table temp_hivreport_haslabtestresults_11012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-NOV-2013' and date < '01-NOV-2014';
select count(*) from temp_hivreport_haslabtestresults_11012014;
--28281

-- 2. Create the table of patients who have HIV prior to 10/1/2014
drop table if exists temp_hivreport_hashivcase_11012014;
create table temp_hivreport_hashivcase_11012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-NOV-2014';
select count(*) from temp_hivreport_hashivcase_11012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 11/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 6/1/2014
-- and are NOT in the list of patients who have HIV before 6/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_11012014;
create table temp_hiv_report2_firstsubsetpats_11012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-NOV-2013'    
and emr_encounter.date < '01-DEC-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_11012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_11012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_11012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--NOV 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_11012014;
create table temp_hiv_report2_masterpats_11012014 as 
select temp_hiv_report2_firstsubsetpats_11012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_11012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_11012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-NOV-2014' 
and emr_encounter.date < '01-DEC-2014'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_11012014;
--93,799

--4.  BUILD COLUMN 1 NOV 2014
drop table if exists temp_hiv_report2_col1_11012014;
create table temp_hiv_report2_col1_11012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_11012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 11012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_11012014;
create table temp_hiv_report2_col2_masterpats_11012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_11012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_11012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-NOV-2014'   
and emr_labresult.date < '01-DEC-2014'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_11012014;
--1488

--6. Column 2 Report 2 11012014 
drop table if exists temp_hiv_report2_col2_11012014;
create table temp_hiv_report2_col2_11012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_11012014
group by center_id, dategroup;

--7. Prep Column 3 11012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_11012014;
create table temp_hiv_report2_col3_haselisaagab_11012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_11012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_11012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-NOV-2014'   
and emr_labresult.date < '01-DEC-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_11012014;
--1475


--8. Create master table for column 3 11012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_11012014;
create table temp_hiv_report2_col3_masterpats_11012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_11012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_11012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-NOV-2014'   
and emr_labresult.date < '01-DEC-2014'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_11012014);
select count(*) from temp_hiv_report2_col3_masterpats_11012014;
--84

-- 9. Column 3 Report 2 For NOV 2014
drop table if exists temp_hiv_report2_col3_11012014;
create table temp_hiv_report2_col3_11012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_11012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_11012014;
create table temp_hiv_report2_col4_masterpats_11012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_11012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_11012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-NOV-2014'
and hef_event.date < '01-DEC-2014';
select count(*) from temp_hiv_report2_col4_masterpats_11012014;
--100

-- 11. Column 4 Report 2 for NOV 2014
drop table if exists temp_hiv_report2_col4_11012014;
create table temp_hiv_report2_col4_11012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_11012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_11012014
where temp_hiv_report2_masterpats_11012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-NOV-2014'
and hef_event.date < '30-NOV-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_11012014
where temp_hiv_report2_masterpats_11012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-NOV-2014'
and hef_event.date < '30-NOV-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_11012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_11012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-NOV-2014'
and emr_labresult.date < '30-NOV-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_11012014
where temp_hiv_report2_masterpats_11012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-NOV-2014'
and hef_event.date < '30-NOV-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_11012014
where temp_hiv_report2_masterpats_11012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-NOV-2014'
and hef_event.date < '30-NOV-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_11012014;
create table temp_hiv_report2_col5_source_11012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_11012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_11012014;
create table temp_hiv_report2_col5_11012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-NOV-2014'
group by center_id, dategroup;


--REPORT 2:  NOV 2014
drop table if exists temp_hiv_report2_11012014;
create table temp_hiv_report2_11012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_11012014 a
left join temp_hiv_report2_col2_11012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_11012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_11012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_11012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;


-------------------------------------------------------------------------------

--DEC 2014
-- 1. Create the table of unique patients who HAVE HIV lab tests in the 12 months prior to 12/1/2014
drop table if exists temp_hivreport_haslabtestresults_12012014;
create table temp_hivreport_haslabtestresults_12012014 as
select distinct patient_id from emr_labresult 
where native_code in (select native_code from conf_labtestmap where test_name like 'hiv%') 
and date >= '01-DEC-2013' and date < '01-DEC-2014';
select count(*) from temp_hivreport_haslabtestresults_12012014;
--28281

-- 2. Create the table of patients who have HIV prior to 12/1/2014
drop table if exists temp_hivreport_hashivcase_12012014;
create table temp_hivreport_hashivcase_12012014 as
select patient_id from nodis_case 
where condition = 'hiv'
and date < '01-DEC-2014';
select count(*) from temp_hivreport_hashivcase_12012014;
--3997

-- 3A. Now.. get the index list of patients who have medical encounters in the year prior to 12/1/2014
-- But are NOT in the list of patients who have HIV results 12 months prior to 12/1/2014
-- and are NOT in the list of patients who have HIV before 12/1/2014
drop table if exists temp_hiv_report2_firstsubsetpats_12012014;
create table temp_hiv_report2_firstsubsetpats_12012014 as select distinct emr_patient.id, center_id
from emr_patient, emr_encounter where emr_patient.id = emr_encounter.patient_id 
and emr_encounter.date >= '01-DEC-2013'    
and emr_encounter.date < '01-DEC-2014'
and emr_patient.id not in (select patient_id from temp_hivreport_haslabtestresults_12012014) 
and emr_patient.id not in (select patient_id from temp_hivreport_hashivcase_12012014)
and substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_firstsubsetpats_12012014;
--303,363

--3B.  NOW - get the list of patient ids who are in the Master subset list 
--but who had an actual MEDICAL encounter in this month of
--DEC 2014
--THIS IS OUR MAIN DENOMINATOR LIST FOR THIS MONTH
drop table if exists temp_hiv_report2_masterpats_12012014;
create table temp_hiv_report2_masterpats_12012014 as 
select temp_hiv_report2_firstsubsetpats_12012014.id patid, center_id, date
from temp_hiv_report2_firstsubsetpats_12012014, emr_encounter
where temp_hiv_report2_firstsubsetpats_12012014.id = emr_encounter.patient_id
and emr_encounter.date >= '01-DEC-2014' 
and emr_encounter.date < '01-JAN-2015'
and  substring(raw_encounter_type from 1 for 1) = 'Y';
select count(*) from temp_hiv_report2_masterpats_12012014;
--93,799

--4.  BUILD COLUMN 1 DEC 2014
drop table if exists temp_hiv_report2_col1_12012014;
create table temp_hiv_report2_col1_12012014 as 
select count(distinct patid) as hiv_denominator, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_masterpats_12012014
group by center_id, dategroup order by center_id, dategroup;

 
-- 5. Prep COLUMN 2 12012014
-- From the Denominator patients,
-- Assemble list of patients who have viral or elisa or agab or PCR HIV lab test results
-- Get their lab test result (by mapped lab heauristic name) and the date
drop table if exists temp_hiv_report2_col2_masterpats_12012014;
create table temp_hiv_report2_col2_masterpats_12012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_12012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_12012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-DEC-2014'   
and emr_labresult.date < '01-JAN-2015'
and emr_labresult.native_code = conf_labtestmap.native_code
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab' or 
conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral' );
select count(distinct patid) from temp_hiv_report2_col2_masterpats_12012014;
--1488

--6. Column 2 Report 2 12012014 
drop table if exists temp_hiv_report2_col2_12012014;
create table temp_hiv_report2_col2_12012014
as select count(distinct patid) as screenedtested, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col2_masterpats_12012014
group by center_id, dategroup;

--7. Prep Column 3 12012014
--Get the list of those who have ELISA or AGAB so we can NOT count those patients
drop table if exists temp_hiv_report2_col3_haselisaagab_12012014;
create table temp_hiv_report2_col3_haselisaagab_12012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_12012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_12012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-DEC-2014'   
and emr_labresult.date < '01-JAN-2015'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_elisa' or conf_labtestmap.test_name = 'hiv_ag_ab');
select count(distinct patid) from temp_hiv_report2_col3_haselisaagab_12012014;
--1475


--8. Create master table for column 3 12012014
-- Count up the patients who had ONLY a VIRAL LOAD OR PCR test
drop table if exists temp_hiv_report2_col3_masterpats_12012014;
create table temp_hiv_report2_col3_masterpats_12012014 as 
select patid, center_id, test_name, emr_labresult.date testdate from temp_hiv_report2_masterpats_12012014, emr_labresult, conf_labtestmap where
temp_hiv_report2_masterpats_12012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-DEC-2014'   
and emr_labresult.date < '01-JAN-2015'
and emr_labresult.native_code = conf_labtestmap.native_code 
and (conf_labtestmap.test_name = 'hiv_pcr' or conf_labtestmap.test_name = 'hiv_rna_viral')
and patid not in (select patid from temp_hiv_report2_col3_haselisaagab_12012014);
select count(*) from temp_hiv_report2_col3_masterpats_12012014;
--84

-- 9. Column 3 Report 2 For DEC 2014
drop table if exists temp_hiv_report2_col3_12012014;
create table temp_hiv_report2_col3_12012014
as select count(distinct patid) as screenedtestedviavl, center_id, date_trunc('month', testdate)::date dategroup from temp_hiv_report2_col3_masterpats_12012014
group by center_id, dategroup;


--10. Column 4 Report is is positive elisa or positive ag_ab testing.
--Create master table for column 4
drop table if exists temp_hiv_report2_col4_masterpats_12012014;
create table temp_hiv_report2_col4_masterpats_12012014 as
select patid, center_id, name, date from temp_hiv_report2_col3_haselisaagab_12012014, hef_event 
where hef_event.patient_id = temp_hiv_report2_col3_haselisaagab_12012014.patid
and (hef_event.name = 'lx:hiv_ag_ab:positive' or hef_event.name = 'lx:hiv_elisa:positive')
and hef_event.date >= '01-DEC-2014'
and hef_event.date < '01-JAN-2015';
select count(*) from temp_hiv_report2_col4_masterpats_12012014;
--100

-- 11. Column 4 Report 2 for DEC 2014
drop table if exists temp_hiv_report2_col4_12012014;
create table temp_hiv_report2_col4_12012014
as select count(distinct patid) as numpatswithposelisaoragab, center_id, date_trunc('month', date)::date dategroup from temp_hiv_report2_col4_masterpats_12012014
group by center_id, dategroup;


---COLUMN 5
--[positive antibody test (ELISA) or pos antibody/antigen] 
--AND
--(positive Western Blot, or viral load >200, or positive qualitative HIV PCR/NAT) 
--Report on this in the month if there is an event in this month that meets the overall POSITIVE criteria condition
--  So.. gather the events for the patients - NOT JUST IN JANUARY but in the 60 days timeframe from last day of January.


--12. LIST OF TARGET PATIENTS WITH POSITIVE AB/AG TEST
drop table if exists temp_hiv_report2_col5_posagab;
create table temp_hiv_report2_col5_posagab as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_12012014
where temp_hiv_report2_masterpats_12012014.patid = hef_event.patient_id
and name = 'lx:hiv_ag_ab:positive' 
and hef_event.date >= '01-DEC-2014'
and hef_event.date < '30-DEC-2014'::date + interval '60 days';

--13.  LIST OF TARGET PATIENTS WITH POS ELISA
drop table if exists temp_hiv_report2_col5_poselisa;
create table temp_hiv_report2_col5_poselisa as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_12012014
where temp_hiv_report2_masterpats_12012014.patid = hef_event.patient_id
and name = 'lx:hiv_elisa:positive' 
and hef_event.date >= '01-DEC-2014'
and hef_event.date < '30-DEC-2014'::date + interval '60 days';


--14. LIST OF TARGET PATIENTS WITH VIRAL LOAD > 200***
drop table if exists temp_hiv_report2_col5_posviralload;
create table temp_hiv_report2_col5_posviralload as select emr_labresult.date, center_id, emr_labresult.patient_id
from temp_hiv_report2_masterpats_12012014, emr_labresult, conf_labtestmap 
where temp_hiv_report2_masterpats_12012014.patid = emr_labresult.patient_id 
and emr_labresult.date >= '01-DEC-2014'
and emr_labresult.date < '30-DEC-2014'::date + interval '60 days'
and conf_labtestmap.test_name = 'hiv_rna_viral' 
and emr_labresult.native_code = conf_labtestmap.native_code and result_float > 200;

--15.  LIST OF TARGET PATIENTS WITH POS WB
drop table if exists temp_hiv_report2_col5_poswb;
create table temp_hiv_report2_col5_poswb as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_12012014
where temp_hiv_report2_masterpats_12012014.patid = hef_event.patient_id
and name = 'lx:hiv_wb:positive' 
and hef_event.date >= '01-DEC-2014'
and hef_event.date < '30-DEC-2014'::date + interval '60 days';


--16.  LIST OF TARGET PATIENTS WITH POS PCR
drop table if exists temp_hiv_report2_col5_pospcr;
create table temp_hiv_report2_col5_pospcr as select hef_event.date, center_id, hef_event.patient_id 
from hef_event, temp_hiv_report2_masterpats_12012014
where temp_hiv_report2_masterpats_12012014.patid = hef_event.patient_id
and name = 'lx:hiv_pcr:positive' 
and hef_event.date >= '01-DEC-2014'
and hef_event.date < '30-DEC-2014'::date + interval '60 days';


--17. LIST OF TARGET PATIENTS WHO HAVE POS ELISA OR POS agab
drop table if exists temp_hiv_report2_col5_poselisaoragab;
create table temp_hiv_report2_col5_poselisaoragab as    
(select * from temp_hiv_report2_col5_posagab
union
select * from temp_hiv_report2_col5_poselisa);


--18. List of patients who meet ELISA OR agab condition the first possible time they do
drop table if exists temp_hiv_report2_col5_poselisaoragab_earliest_date;
create table temp_hiv_report2_col5_poselisaoragab_earliest_date
as select patient_id, min(date) as mindate from 
temp_hiv_report2_col5_poselisaoragab
group by patient_id;

--19 List of patients who meet WB or VIRAL LOAD or PCR conditions
drop table if exists temp_hiv_report2_col5_poswborviralorpcr;
create table temp_hiv_report2_col5_poswborviralorpcr as
(select * from temp_hiv_report2_col5_poswb
union
select * from temp_hiv_report2_col5_pospcr
union 
select * from temp_hiv_report2_col5_posviralload);

--20. LIST OF TARGET PATIENTS WITH EARLIEST VIRAL LOAD > 200*** or Positive PCR or WB
drop table if exists temp_hiv_report2_col5_poswborviralorpcr_earliest_date;
create table temp_hiv_report2_col5_poswborviralorpcr_earliest_date as
select patient_id, min(date) as mindate from
temp_hiv_report2_col5_poswborviralorpcr
group by patient_id;


--21. Pull together COL 5 counts with MINIMUM date patient met criteria
drop table if exists temp_hiv_report2_col5_source_12012014;
create table temp_hiv_report2_col5_source_12012014 as
select c.center_id, a.patient_id, a.datea, b.dateb from
(select patient_id, mindate as datea from temp_hiv_report2_col5_poswborviralorpcr_earliest_date ) a,
(select patient_id, mindate as dateb from temp_hiv_report2_col5_poselisaoragab_earliest_date ) b,
emr_patient c
where a.patient_id = b.patient_id
and a.patient_id = c.id;


--22.  Refine column 5 data to get minimum/first date
drop table if exists temp_hiv_report2_col5_almost;
create table temp_hiv_report2_col5_almost as
select center_id, patient_id, least(datea, dateb) as date from temp_hiv_report2_col5_source_12012014
where datea-dateb<=60;

--23  Put into final form
drop table temp_hiv_report2_col5_12012014;
create table temp_hiv_report2_col5_12012014 as
select count(patient_id) as confirmedtests, center_id, date_trunc('month', date) dategroup
from temp_hiv_report2_col5_almost
where date_trunc('month', date) = '01-DEC-2014'
group by center_id, dategroup;


--REPORT 2:  DEC 2014
drop table if exists temp_hiv_report2_12012014;
create table temp_hiv_report2_12012014 as
select a.center_id, 
a.dategroup::date monthyear,
hiv_denominator,
screenedtested,
screenedtestedviavl,
numpatswithposelisaoragab,
confirmedtests
from temp_hiv_report2_col1_12012014 a
left join temp_hiv_report2_col2_12012014 b on a.center_id = b.center_id and a.dategroup = b.dategroup
left join temp_hiv_report2_col3_12012014 c on a.center_id = c.center_id and a.dategroup = c.dategroup
left join temp_hiv_report2_col4_12012014 d on a.center_id = d.center_id and a.dategroup = d.dategroup
left join temp_hiv_report2_col5_12012014 e on a.center_id = e.center_id and a.dategroup = e.dategroup
where a.center_id is not null
 order by a.center_id::int, monthyear asc;


--FINAL UNION
--REPORT 2 all MONTHS
drop table if exists temp_hiv_report2_2014;
create table temp_hiv_report2_2014 as
(select * from temp_hiv_report2_01012014)
union 
(select * from temp_hiv_report2_02012014)
union 
(select * from temp_hiv_report2_03012014)
union
(select * from temp_hiv_report2_04012014)
union
(select * from temp_hiv_report2_05012014)
union
(select * from temp_hiv_report2_06012014)
union
(select * from temp_hiv_report2_07012014)
union
(select * from temp_hiv_report2_08012014)
union
(select * from temp_hiv_report2_09012014)
union
(select * from temp_hiv_report2_10012014)
union
(select * from temp_hiv_report2_11012014)
union
(select * from temp_hiv_report2_12012014)
;
select * from temp_hiv_report2_2014 order by center_id::int, monthyear;



--Clean up
/*
drop table temp_hiv_report2_col1_01012014;
drop table temp_hiv_report2_col2_01012014;
drop table temp_hiv_report2_col3_01012014;
drop table temp_hiv_report2_col4_01012014;
drop table temp_hiv_report2_col5_01012014;
drop table temp_hiv_report2_col1_02012014;
drop table temp_hiv_report2_col2_02012014;
drop table temp_hiv_report2_col3_02012014;
drop table temp_hiv_report2_col4_02012014;
drop table temp_hiv_report2_col5_02012014;
drop table temp_hiv_report2_col1_03012014;
drop table temp_hiv_report2_col2_03012014;
drop table temp_hiv_report2_col3_03012014;
drop table temp_hiv_report2_col4_03012014;
drop table temp_hiv_report2_col5_03012014;
drop table temp_hiv_report2_col1_04012014;
drop table temp_hiv_report2_col2_04012014;
drop table temp_hiv_report2_col3_04012014;
drop table temp_hiv_report2_col4_04012014;
drop table temp_hiv_report2_col5_04012014;
drop table temp_hiv_report2_col1_05012014;
drop table temp_hiv_report2_col2_05012014;
drop table temp_hiv_report2_col3_05012014;
drop table temp_hiv_report2_col4_05012014;
drop table temp_hiv_report2_col5_05012014;
drop table temp_hiv_report2_col1_06012014;
drop table temp_hiv_report2_col2_06012014;
drop table temp_hiv_report2_col3_06012014;
drop table temp_hiv_report2_col4_06012014;
drop table temp_hiv_report2_col5_06012014;
drop table temp_hiv_report2_col1_07012014;
drop table temp_hiv_report2_col2_07012014;
drop table temp_hiv_report2_col3_07012014;
drop table temp_hiv_report2_col4_07012014;
drop table temp_hiv_report2_col5_07012014;
drop table temp_hiv_report2_col1_08012014;
drop table temp_hiv_report2_col2_08012014;
drop table temp_hiv_report2_col3_08012014;
drop table temp_hiv_report2_col4_08012014;
drop table temp_hiv_report2_col5_08012014;
drop table temp_hiv_report2_col1_09012014;
drop table temp_hiv_report2_col2_09012014;
drop table temp_hiv_report2_col3_09012014;
drop table temp_hiv_report2_col4_09012014;
drop table temp_hiv_report2_col5_09012014;
drop table temp_hiv_report2_col1_10012014;
drop table temp_hiv_report2_col2_10012014;
drop table temp_hiv_report2_col3_10012014;
drop table temp_hiv_report2_col4_10012014;
drop table temp_hiv_report2_col5_10012014;
drop table temp_hiv_report2_col1_11012014;
drop table temp_hiv_report2_col2_11012014;
drop table temp_hiv_report2_col3_11012014;
drop table temp_hiv_report2_col4_11012014;
drop table temp_hiv_report2_col5_11012014;
drop table temp_hiv_report2_col1_12012014;
drop table temp_hiv_report2_col2_12012014;
drop table temp_hiv_report2_col3_12012014;
drop table temp_hiv_report2_col4_12012014;
drop table temp_hiv_report2_col5_12012014;

drop table temp_hiv_report2_masterpats_01012014;
drop table temp_hiv_report2_col2_masterpats_01012014;
drop table temp_hiv_report2_col3_masterpats_01012014;
drop table temp_hiv_report2_col3_haselisaagab_01012014;
drop table temp_hiv_report2_col4_masterpats_01012014;

drop table temp_hiv_report2_masterpats_02012014;
drop table temp_hiv_report2_col2_masterpats_02012014;
drop table temp_hiv_report2_col3_masterpats_02012014;
drop table temp_hiv_report2_col3_haselisaagab_02012014;
drop table temp_hiv_report2_col4_masterpats_02012014;

drop table temp_hiv_report2_masterpats_03012014;
drop table temp_hiv_report2_col2_masterpats_03012014;
drop table temp_hiv_report2_col3_masterpats_03012014;
drop table temp_hiv_report2_col3_haselisaagab_03012014;
drop table temp_hiv_report2_col4_masterpats_03012014;

drop table temp_hiv_report2_masterpats_04012014;
drop table temp_hiv_report2_col2_masterpats_04012014;
drop table temp_hiv_report2_col3_masterpats_04012014;
drop table temp_hiv_report2_col3_haselisaagab_04012014;
drop table temp_hiv_report2_col4_masterpats_04012014;

drop table temp_hiv_report2_masterpats_05012014;
drop table temp_hiv_report2_col2_masterpats_05012014;
drop table temp_hiv_report2_col3_masterpats_05012014;
drop table temp_hiv_report2_col3_haselisaagab_05012014;
drop table temp_hiv_report2_col4_masterpats_05012014;

drop table temp_hiv_report2_masterpats_06012014;
drop table temp_hiv_report2_col2_masterpats_06012014;
drop table temp_hiv_report2_col3_masterpats_06012014;
drop table temp_hiv_report2_col3_haselisaagab_06012014;
drop table temp_hiv_report2_col4_masterpats_06012014;


drop table temp_hiv_report2_masterpats_07012014;
drop table temp_hiv_report2_col2_masterpats_07012014;
drop table temp_hiv_report2_col3_masterpats_07012014;
drop table temp_hiv_report2_col3_haselisaagab_07012014;
drop table temp_hiv_report2_col4_masterpats_07012014;


drop table temp_hiv_report2_masterpats_08012014;
drop table temp_hiv_report2_col2_masterpats_08012014;
drop table temp_hiv_report2_col3_masterpats_08012014;
drop table temp_hiv_report2_col3_haselisaagab_08012014;
drop table temp_hiv_report2_col4_masterpats_08012014;

drop table temp_hiv_report2_masterpats_09012014;
drop table temp_hiv_report2_col2_masterpats_09012014;
drop table temp_hiv_report2_col3_masterpats_09012014;
drop table temp_hiv_report2_col3_haselisaagab_09012014;
drop table temp_hiv_report2_col4_masterpats_09012014;

drop table temp_hiv_report2_masterpats_10012014;
drop table temp_hiv_report2_col2_masterpats_10012014;
drop table temp_hiv_report2_col3_masterpats_10012014;
drop table temp_hiv_report2_col3_haselisaagab_10012014;
drop table temp_hiv_report2_col4_masterpats_10012014;

drop table temp_hiv_report2_masterpats_11012014;
drop table temp_hiv_report2_col2_masterpats_11012014;
drop table temp_hiv_report2_col3_masterpats_11012014;
drop table temp_hiv_report2_col3_haselisaagab_11012014;
drop table temp_hiv_report2_col4_masterpats_11012014;


drop table temp_hiv_report2_masterpats_12012014;
drop table temp_hiv_report2_col2_masterpats_12012014;
drop table temp_hiv_report2_col3_masterpats_12012014;
drop table temp_hiv_report2_col3_haselisaagab_12012014;
drop table temp_hiv_report2_col4_masterpats_12012014;


drop table temp_hiv_report2_01012014;
drop table temp_hiv_report2_firstsubsetpats_01012014;
drop table temp_hiv_report2_haslabtestresults_01012014;
drop table temp_hiv_report2_hashivcase_01012014;

drop table temp_hiv_report2_02012014;
drop table temp_hiv_report2_firstsubsetpats_02012014;
drop table temp_hiv_report2_haslabtestresults_02012014;
drop table temp_hiv_report2_hashivcase_02012014;

drop table temp_hiv_report2_03012014;
drop table temp_hiv_report2_firstsubsetpats_03012014;
drop table temp_hiv_report2_haslabtestresults_03012014;
drop table temp_hiv_report2_hashivcase_03012014;

drop table temp_hiv_report2_04012014;
drop table temp_hiv_report2_firstsubsetpats_04012014;
drop table temp_hiv_report2_haslabtestresults_04012014;
drop table temp_hiv_report2_hashivcase_04012014;

drop table temp_hiv_report2_05012014;
drop table temp_hiv_report2_firstsubsetpats_05012014;
drop table temp_hiv_report2_haslabtestresults_05012014;
drop table temp_hiv_report2_hashivcase_05012014;

drop table temp_hiv_report2_06012014;
drop table temp_hiv_report2_firstsubsetpats_06012014;
drop table temp_hiv_report2_haslabtestresults_06012014;
drop table temp_hiv_report2_hashivcase_06012014;

drop table temp_hiv_report2_07012014;
drop table temp_hiv_report2_firstsubsetpats_07012014;
drop table temp_hiv_report2_haslabtestresults_07012014;
drop table temp_hiv_report2_hashivcase_07012014;

drop table temp_hiv_report2_08012014;
drop table temp_hiv_report2_firstsubsetpats_08012014;
drop table temp_hiv_report2_haslabtestresults_08012014;
drop table temp_hiv_report2_hashivcase_08012014;

drop table temp_hiv_report2_09012014;
drop table temp_hiv_report2_firstsubsetpats_09012014;
drop table temp_hiv_report2_haslabtestresults_09012014;
drop table temp_hiv_report2_hashivcase_09012014;

drop table temp_hiv_report2_10012014;
drop table temp_hiv_report2_firstsubsetpats_10012014;
drop table temp_hiv_report2_haslabtestresults_10012014;
drop table temp_hiv_report2_hashivcase_10012014;

drop table temp_hiv_report2_11012014;
drop table temp_hiv_report2_firstsubsetpats_11012014;
drop table temp_hiv_report2_haslabtestresults_11012014;
drop table temp_hiv_report2_hashivcase_11012014;


drop table temp_hiv_report2_12012014;
drop table temp_hiv_report2_firstsubsetpats_12012014;
drop table temp_hiv_report2_haslabtestresults_12012014;
drop table temp_hiv_report2_hashivcase_12012014;


drop table temp_hiv_report2_col5_poselisaoragab;
drop table temp_hiv_report2_col5_poselisa;
drop table temp_hiv_report2_col5_poswb;
drop table temp_hiv_report2_col5_posviralload;
drop table temp_hiv_report2_col5_pospcr;
drop table temp_hiv_report2_col5_posagab;
drop table temp_hiv_report2_col5_poselisaoragab_earliest_date;
drop table temp_hiv_report2_col5_poswborviralorpcr;
drop table temp_hiv_report2_col5_almost;
drop table temp_hiv_report2_col5_poswborviralorpcr_earliest_date;

drop table temp_hiv_report2_col1_01012014;
drop table temp_hiv_report2_col1_02012014;
drop table temp_hiv_report2_col1_03012014;
drop table temp_hiv_report2_col1_04012014;
drop table temp_hiv_report2_col1_05012014;
drop table temp_hiv_report2_col1_06012014;
drop table temp_hiv_report2_col1_07012014;
drop table temp_hiv_report2_col1_08012014;


drop table temp_hiv_report2_col2_01012014;
drop table temp_hiv_report2_col2_02012014;
drop table temp_hiv_report2_col2_03012014;
drop table temp_hiv_report2_col2_04012014;
drop table temp_hiv_report2_col2_05012014;
drop table temp_hiv_report2_col2_06012014;
drop table temp_hiv_report2_col2_07012014;
drop table temp_hiv_report2_col2_08012014;

drop table temp_hiv_report2_col3_01012014;
drop table temp_hiv_report2_col3_02012014;
drop table temp_hiv_report2_col3_03012014;
drop table temp_hiv_report2_col3_04012014;
drop table temp_hiv_report2_col3_05012014;
drop table temp_hiv_report2_col3_06012014;
drop table temp_hiv_report2_col3_07012014;
drop table temp_hiv_report2_col3_08012014;

drop table temp_hiv_report2_col4_01012014;
drop table temp_hiv_report2_col4_02012014;
drop table temp_hiv_report2_col4_03012014;
drop table temp_hiv_report2_col4_04012014;
drop table temp_hiv_report2_col4_05012014;
drop table temp_hiv_report2_col4_06012014;
drop table temp_hiv_report2_col4_07012014;
drop table temp_hiv_report2_col4_08012014;

drop table temp_hiv_report2_col5_01012014;
drop table temp_hiv_report2_col5_02012014;
drop table temp_hiv_report2_col5_03012014;
drop table temp_hiv_report2_col5_04012014;
drop table temp_hiv_report2_col5_05012014;
drop table temp_hiv_report2_col5_06012014;
drop table temp_hiv_report2_col5_07012014;
drop table temp_hiv_report2_col5_08012014;

drop table temp_hiv_report2_col5_source_01012014;
drop table temp_hiv_report2_col5_source_02012014;
drop table temp_hiv_report2_col5_source_03012014;
drop table temp_hiv_report2_col5_source_04012014;
drop table temp_hiv_report2_col5_source_05012014;
drop table temp_hiv_report2_col5_source_06012014;
drop table temp_hiv_report2_col5_source_07012014;
drop table temp_hiv_report2_col5_source_08012014;
drop table temp_hiv_report2_col5_source_09012014;
drop table temp_hiv_report2_col5_source_10012014;
drop table temp_hiv_report2_col5_source_11012014;
drop table temp_hiv_report2_col5_source_12012014;


drop table temp_hivreport_haslabtestresults_01012014;
drop table temp_hivreport_haslabtestresults_02012014;
drop table temp_hivreport_haslabtestresults_03012014;
drop table temp_hivreport_haslabtestresults_04012014;
drop table temp_hivreport_haslabtestresults_05012014;
drop table temp_hivreport_haslabtestresults_06012014;
drop table temp_hivreport_haslabtestresults_07012014;
drop table temp_hivreport_haslabtestresults_08012014;
drop table temp_hivreport_haslabtestresults_09012014;
drop table temp_hivreport_haslabtestresults_10012014;
drop table temp_hivreport_haslabtestresults_11012014;
drop table temp_hivreport_haslabtestresults_12012014;


drop table temp_hivreport_hashivcase_01012014;
drop table temp_hivreport_hashivcase_02012014;
drop table temp_hivreport_hashivcase_03012014;
drop table temp_hivreport_hashivcase_04012014;
drop table temp_hivreport_hashivcase_05012014;
drop table temp_hivreport_hashivcase_06012014;
drop table temp_hivreport_hashivcase_07012014;
drop table temp_hivreport_hashivcase_08012014;
drop table temp_hivreport_hashivcase_09012014;
drop table temp_hivreport_hashivcase_10012014;
drop table temp_hivreport_hashivcase_11012014;
drop table temp_hivreport_hashivcase_12012014;
*/


