drop table if exists temp_unmapped;
drop table if exists temp_unmapped_proc;
drop table if exists temp_unmapped_combo;

create table temp_unmapped as
select e.count, e.native_name, e.native_code, procedure_name
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
-- UNCOMMENT THIS FOR WIDE RANGE OF STRINGS
--and e.native_name ~* '(alt|spgt|ast|aminotrans|sgot|hep|anaplasma|phago|hge|bili|tbil|hb|hbv|cd4|chlam|trac|cr|creat|fta|gc|gon|hcv|genotype|hiv|immuno|pcr|lyme|borr|burg|plt|rbc|red bl|rpr|rapid plasma reagin|tb|tuber|afb|cult|igra|feron|t-spot|mycob|tp|trep|syph|pallidium|particle|vdrl|wbc|white bl|qft|sgpt|platelet|helper|hematocrit|hct)'
-- THIS LINE IS FOR STANDARD SEARCH STRINGS ONLY. SEE LINE ABOVE TO EXPAND IF DESIRED.
--and e.native_name ~* '(alt|sgpt|ast|aminotrans|sgot|rbc|red bl|wbc|white bl|platelet|plt|cd4|helper|genotype|creat|hematocrit|hct)'
and e.native_name ~* '(hcv|hep)' 
group by e.count, e.native_name, e.native_code, procedure_name;

create table temp_unmapped_proc as
select count(*), native_name, native_code, procedure_name
from emr_labresult
where native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode)
and native_code not in (select native_code from temp_unmapped)
-- UNCOMMENT THIS FOR WIDE RANGE OF STRINGS
--and e.native_name ~* '(alt|spgt|ast|aminotrans|sgot|hep|anaplasma|phago|hge|bili|tbil|hb|hbv|cd4|chlam|trac|cr|creat|fta|gc|gon|hcv|genotype|hiv|immuno|pcr|lyme|borr|burg|plt|rbc|red bl|rpr|rapid plasma reagin|tb|tuber|afb|cult|igra|feron|t-spot|mycob|tp|trep|syph|pallidium|particle|vdrl|wbc|white bl|qft|sgpt|platelet|helper|hematocrit|hct)'
-- THIS LINE IS FOR STANDARD SEARCH STRINGS ONLY. SEE LINE ABOVE TO EXPAND IF DESIRED.
--and e.native_name ~* '(alt|sgpt|ast|aminotrans|sgot|rbc|red bl|wbc|white bl|platelet|plt|cd4|helper|genotype|creat|hematocrit|hct)'
and procedure_name ~* '(hcv|hep)' 
group by native_name, native_code, procedure_name;


create table temp_unmapped_combo AS
select * from temp_unmapped
UNION
select * from temp_unmapped_proc
WHERE native_code not in (
'13951-9--HEPATITIS A VIRUS AB [PRESENCE] IN SERUM BY IMMUNO',
'13952-7--HEPATITIS B VIRUS CORE AB [PRESENCE] IN SERUM OR P',
'20507-0--Reagin Ab [Presence] in Serum by RPR'
);


DROP TABLE IF EXISTS temp_unmapped_with_result_strings;
CREATE TABLE temp_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)),
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER (
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank
FROM
   emr_labresult S1,
   temp_unmapped_combo S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_unmapped_combo T2
  where T1.native_code = T2.native_code
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;


\COPY temp_unmapped_with_result_strings  TO '/tmp/unmapped_labs_with_result_strings.csv' DELIMITER ',' CSV HEADER;


