update emr_labresult
set native_code = concat(native_code, '-LOG')
where native_code in ('11011-4--HEPATITIS C VIRAL RNA, QUANTITATIVE, REAL-TIME PCR | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '29609-5--HCV RNA, QUANTITATIVE REAL TIME PCR POSITIVE | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '48398-2--HEPATITIS B VIRUS DNA, QN, REAL TIME PCR | HEPATITIS B VIRUS DNA',
					 '11011-4--HEPATITIS C AB W/RFL RNA, PCR W/RFL GENOTYPE,LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HEPATITIS C VIRAL RNA, QN REAL TIME PCR W/REFLS | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '11011-4--IADNA HEPATITIS C QUANT & REVERSE TRANSCRIPTION | HCVDNA',
					 '11011-4--IADNA HEPATITIS C QUANT & REVERSE TRANSCRIPTION - HCVDNA',
					 '20416-4--HCV RNA, QUANTITATIVE REAL TIME PCR | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HCV RNA, QUANTITATIVE REAL TIME PCR.HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HEPATITIS C VIRUS RNA [#/VOLUME] (VIRAL LOAD) IN S',
					 '38180-6--HEPATITIS C VIRUS RNA [LOG UNITS/VOLUME] (VIRAL LO',
					 '49605-9--HEPATITIS C VIRUS RNA [LOG UNITS/VOLUME] (VIRAL LO'
					 '--HCV RNA PCR Qn',
					 '48398-2--HBVCOPIESML',
					 '42595-9--HBVDNA',
					 '48398-2--HBVLOGDNA',
					 '20416-4--HEPATITIS PANEL, ACUTE W/REFLEX TO CONFIRMATION | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 'HepBLab--HEPATITIS B DNA, QUANTITATIVE, SERUM - HEPATITIS B',
					 'HepBLab--Hepatitis B Virus DNA',
					 'Unknown--HEPATITIS B VIRUS DNA',
					 'Unknown--HEP B DNA COPIES',
					 'Hep C Viral Load--HEP C VIRAL RNA: HEPATITIS C VIRAL RNA PCR',
					 '20416-4--HCV RNA BY PCR, QN RFX GENO | HCV RNA PCR QN',
					 '--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA (NON ORDERABLE) | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '11011-4--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '21008-8--HIV-1 RNA QUANT REAL TIME PCR, PLASMA | HIV 1 RNA, QN PCR',
					 '--HIV 1 RNA, QN PCR W/RFL GENO (RTI,PI,INTEGRASE).HIV 1 RNA, QN PCR', 
					 '21008-8--HIV 1 RNA, QN PCR W/REFLEX TO GENOTYPE.HIV 1 RNA, QN PCR',
					 '29541-0--HIV 1, QUANT, REAL-TIME PCR | COPIESML',
					 '49890-7--HIV 1, QUANT, REAL-TIME PCR | COPIESML')
and result_string ilike '%.%';


update emr_labresult
set native_code = concat(native_code, '-COPIES')
where native_code in ('11011-4--HEPATITIS C VIRAL RNA, QUANTITATIVE, REAL-TIME PCR | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '29609-5--HCV RNA, QUANTITATIVE REAL TIME PCR POSITIVE | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '48398-2--HEPATITIS B VIRUS DNA, QN, REAL TIME PCR | HEPATITIS B VIRUS DNA',
					 '11011-4--HEPATITIS C AB W/RFL RNA, PCR W/RFL GENOTYPE,LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HEPATITIS C VIRAL RNA, QN REAL TIME PCR W/REFLS | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '11011-4--IADNA HEPATITIS C QUANT & REVERSE TRANSCRIPTION | HCVDNA',
					 '11011-4--IADNA HEPATITIS C QUANT & REVERSE TRANSCRIPTION - HCVDNA',
					 '20416-4--HCV RNA, QUANTITATIVE REAL TIME PCR | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HCV RNA, QUANTITATIVE REAL TIME PCR.HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '20416-4--HEPATITIS C VIRUS RNA [#/VOLUME] (VIRAL LOAD) IN S',
					 '38180-6--HEPATITIS C VIRUS RNA [LOG UNITS/VOLUME] (VIRAL LO',
					 '49605-9--HEPATITIS C VIRUS RNA [LOG UNITS/VOLUME] (VIRAL LO'
					 '--HCV RNA PCR Qn',
					 '48398-2--HBVCOPIESML',
					 '42595-9--HBVDNA',
					 '48398-2--HBVLOGDNA',
					 '20416-4--HEPATITIS PANEL, ACUTE W/REFLEX TO CONFIRMATION | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 'HepBLab--HEPATITIS B DNA, QUANTITATIVE, SERUM - HEPATITIS B',
					 'HepBLab--Hepatitis B Virus DNA',
					 'Unknown--HEPATITIS B VIRUS DNA',
					 'Unknown--HEP B DNA COPIES',
					 'Hep C Viral Load--HEP C VIRAL RNA: HEPATITIS C VIRAL RNA PCR',
					 '20416-4--HCV RNA BY PCR, QN RFX GENO | HCV RNA PCR QN',
					 '--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA (NON ORDERABLE) | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '11011-4--HCV RNA, QN,REAL TIME PCR W/REFL GENOTYPE, LIPA | HCV RNA, QUANTITATIVE REAL TIME PCR',
					 '21008-8--HIV-1 RNA QUANT REAL TIME PCR, PLASMA | HIV 1 RNA, QN PCR',
					 '--HIV 1 RNA, QN PCR W/RFL GENO (RTI,PI,INTEGRASE).HIV 1 RNA, QN PCR',
					 '21008-8--HIV 1 RNA, QN PCR W/REFLEX TO GENOTYPE.HIV 1 RNA, QN PCR',
					 '29541-0--HIV 1, QUANT, REAL-TIME PCR | COPIESML',
					 '49890-7--HIV 1, QUANT, REAL-TIME PCR | COPIESML')
and result_string NOT ilike '%.%';