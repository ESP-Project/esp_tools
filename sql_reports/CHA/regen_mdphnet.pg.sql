drop table if exists mdphnet_schema_update_history;
CREATE TABLE mdphnet_schema_update_history
(
  latest_update timestamp without time zone NOT NULL,
  patients_replaced integer,
  CONSTRAINT update_timestamp_pk PRIMARY KEY (latest_update)
);
insert into mdphnet_schema_update_history
select current_timestamp, 0;
drop view if exists esp_demographic_v;
drop table if exists esp_temp_smoking;
create table esp_temp_smoking as
   select distinct case when upper(t1.latest)='YES' then 'Current'
               when upper(t2.yesOrQuit)='QUIT' then 'Former'
               when upper(t3.passive)='PASSIVE' then 'Passive'
               when upper(t4.never)='NEVER' then 'Never'
               else 'Not available' 
           end as smoking, 
           t0.natural_key as patid
   from
     emr_patient t0
   left outer join 
     (select patient_id, case when bool_or(upper(latest)='YES') then 'YES' else 'NO' end as latest from
      (select t00.tobacco_use as latest, t00.patient_id 
      from emr_socialhistory t00
      inner join
      (select max(date) as maxdate, patient_id 
       from emr_socialhistory 
       where tobacco_use is not null and tobacco_use<>''
       group by patient_id) t01 on t00.patient_id=t01.patient_id and t00.date=t01.maxdate) t11 group by patient_id) t1 on t0.id=t1.patient_id
   left outer join
     (select max(val) as yesOrQuit, patient_id
      from (select 'QUIT'::text as val, patient_id
            from emr_socialhistory where upper(tobacco_use) in ('YES','QUIT')) t00
            group by patient_id) t2 on t0.id=t2.patient_id
   left outer join
     (select max(val) as passive, patient_id
      from (select 'PASSIVE'::text as val, patient_id
            from emr_socialhistory where upper(tobacco_use) ='PASSIVE') t00
            group by patient_id) t3 on t0.id=t3.patient_id
   left outer join
     (select max(val) as never, patient_id
      from (select 'NEVER'::text as val, patient_id
            from emr_socialhistory where upper(tobacco_use) ='NEVER') t00
            group by patient_id) t4 on t0.id=t4.patient_id;
alter table esp_temp_smoking add primary key (patid);
drop view if exists esp_demographic_v;
CREATE OR REPLACE VIEW esp_demographic_v AS
SELECT '1'::varchar(1) as centerid,
       pat.natural_key as patid,
       pat.date_of_birth::date - ('1960-01-01'::date) as birth_date,
       CASE
         WHEN UPPER(gender) in ('M','MALE') THEN 'M'::char(1)
         WHEN UPPER(gender) in ('F','FEMALE') THEN 'F'::char(1)
         WHEN UPPER(gender) = 'U' THEN 'U'::char(1)
         ELSE 'U'::char(1)
       END as sex,
       CASE
         WHEN UPPER(race) = 'HISPANIC' THEN 'Y'::char(1)
         WHEN ETHNICITY = 'HISPANIC' then 'Y'::char(1)
         ELSE 'U'::char(1)
       END as Hispanic,
       CASE
         WHEN UPPER(race) in ('NAT AMERICAN','ALASKAN','AMERICAN INDIAN/ALASKAN NATIVE') THEN 1
         WHEN UPPER(race) in ('ASIAN','INDIAN') THEN 2
         WHEN UPPER(race) = 'BLACK'  THEN 3
         WHEN UPPER(race) in ('NATIVE HAWAI','PACIFIC ISLANDER/HAWAIIAN') then 4
         WHEN UPPER(race) in ('CAUCASIAN','WHITE') THEN 5
         ELSE 0
       END as race,
       case 
         when upper(race)='HISPANIC' then 6 
         when ethnicity = 'HISPANIC' then 6
         when UPPER(race) in ('CAUCASIAN','WHITE') then 5
         when UPPER(race) in ('ASIAN','INDIAN','NATIVE HAWAI') then 2
         when UPPER(race) = 'BLACK'then 3
         when UPPER(race) in ('NAT AMERICAN','ALASKAN','AMERICAN INDIAN/ALASKAN NATIVE') then 1
         else 0
       end as race_ethnicity,
       pat.zip5,
       smk.smoking
  FROM public.emr_patient pat,
       public.emr_provenance prvn,
       esp_temp_smoking smk,
       (select distinct patient_id from emr_encounter) encpat
  WHERE pat.provenance_id=prvn.provenance_id and prvn.source ilike 'epicmem%' and pat.id=encpat.patient_id
        and pat.natural_key=smk.patid;

DROP TABLE if exists esp_current_asthma_cases cascade;
CREATE TABLE esp_current_asthma_cases AS
	select id, patient_id from
	(select max(public.hef_event.date) as MAXEVTDT, public.nodis_case.patient_id, public.nodis_case.id from hef_event, public.nodis_case where 
	condition = 'asthma'
	and
	public.hef_event.patient_id = public.nodis_case.patient_id
	and public.hef_event.name in ('dx:asthma', 'rx:albuterol', 'rx:alvesco', 'rx:pulmicort', 'rx:flovent', 'rx:asmanex', 'rx:aerobid', 'rx:montelukast','rx:intal','rx:zafirlukast', 'rx:zileuton', 'rx:ipratropium','rx:tiotropium','rx:omalizumab', 'rx:fluticasone-inh', 'rx:mometasone-inh','rx:budesonide-inh','rx:ciclesonide-inh','rx:flunisolide-inh','rx:cromolyn-inh','rx:pirbuterol','rx:levalbuterol','rx:arformoterol','rx:formeterol','rx:indacaterol','rx:salmeterol',
'rx:beclomethasone','rx:fluticasone-salmeterol:generic','rx:albuterol-ipratropium:generic','rx:mometasone-formeterol:generic',
'rx:budesonide-formeterol:generic', 'rx:fluticasone-salmeterol:trade','rx:albuterol-ipratropium:trade','rx:mometasone-formeterol:trade','rx:budesonide-formeterol:trade') group by public.nodis_case.patient_id, public.nodis_case.id) A 
	where
	current_date - MAXEVTDT <= (365.25*2);
CREATE INDEX esp_current_asthma_cases_caseid_idx on esp_current_asthma_cases (id);
VACUUM ANALYZE esp_current_asthma_cases;
ALTER TABLE esp_current_asthma_cases add primary key (patient_id);


drop view if exists esp_encounter_v;
CREATE OR REPLACE VIEW esp_encounter_v AS
SELECT '1'::varchar(1) as centerid,
       pat.natural_key as patid,
       enc.natural_key as encounterid,
       enc.date - ('1960-01-01'::date) as a_date,
       enc.date_closed - ('1960-01-01'::date) as d_date,
       prov.natural_key as provider,
       enc.site_name as facility_location,
       'AV'::varchar(10) as enc_type, --this is initial value for Mass League data
       enc.site_natural_key as facility_code,
       date_part('year', enc.date)::integer as enc_year,
       esp_mdphnet.age_at_year_start(enc.date, pat.date_of_birth::date) as age_at_enc_year,
       esp_mdphnet.age_group_5yr(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_5yr,
       esp_mdphnet.age_group_10yr(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_10yr,
       esp_mdphnet.age_group_ms(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_ms
  FROM public.emr_encounter enc
         INNER JOIN public.emr_patient pat ON enc.patient_id = pat.id
         INNER JOIN public.emr_provenance prvn ON pat.provenance_id = prvn.provenance_id
         LEFT JOIN public.emr_provider prov ON enc.provider_id = prov.id
		 LEFT JOIN  public.static_enc_type_lookup s ON enc.raw_encounter_type = s.raw_encounter_type
  WHERE prvn.source ilike 'epicmem%'
  AND (enc.raw_encounter_type is null or s.rs_mdphnet = 1);

drop view if exists esp_diagnosis_v;
CREATE OR REPLACE VIEW esp_diagnosis_v AS
SELECT '1'::varchar(1) as centerid,
       pat.natural_key as patid,
       enc.natural_key as encounterid,
       enc.date - ('1960-01-01'::date) as a_date,
       prov.natural_key as provider,
       'AV'::varchar(10) as enc_type, --this is initial value for Mass League data
       split_part(dx_code_id,':', 2) dx,
       split_part(dx_code_id, ':', 1) dx_type,

       enc.site_name as facility_location,
       enc.site_natural_key as facility_code,
       date_part('year', enc.date)::integer as enc_year,
       esp_mdphnet.age_at_year_start(enc.date, pat.date_of_birth::date) as age_at_enc_year,
       esp_mdphnet.age_group_5yr(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_5yr,
       esp_mdphnet.age_group_10yr(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_10yr,
       esp_mdphnet.age_group_ms(enc.date, pat.date_of_birth::date)::varchar(5) as age_group_ms
  FROM public.emr_encounter enc
         INNER JOIN public.emr_patient pat ON enc.patient_id = pat.id
         INNER JOIN public.emr_provenance prvn on pat.provenance_id = prvn.provenance_id
         INNER JOIN (select * from public.emr_encounter_dx_codes
                     where strpos(trim(dx_code_id),'.')<>3
                       and length(trim(dx_code_id))>=3 ) diag ON enc.id = diag.encounter_id
         LEFT JOIN public.emr_provider prov ON enc.provider_id = prov.id
		 LEFT JOIN  public.static_enc_type_lookup s ON enc.raw_encounter_type = s.raw_encounter_type
  WHERE prvn.source ilike 'epicmem%'
  AND (enc.raw_encounter_type is null or s.rs_mdphnet = 1);

drop view if exists esp_disease_v;
CREATE OR REPLACE VIEW esp_disease_v AS
SELECT '1'::varchar(1) as centerid,
       pat.natural_key as patid,
       disease.condition,
       disease.date - ('1960-01-01'::date) as date,
       esp_mdphnet.age_at_year_start(disease.date, pat.date_of_birth::date) as age_at_detect_year,
       esp_mdphnet.age_group_5yr(disease.date, pat.date_of_birth::date)::varchar(5) as age_group_5yr,
       esp_mdphnet.age_group_10yr(disease.date, pat.date_of_birth::date)::varchar(5) as age_group_10yr,
       esp_mdphnet.age_group_ms(disease.date, pat.date_of_birth::date)::varchar(5) as age_group_ms,
       disease.criteria,
       disease.status,
       disease.notes
  FROM public.nodis_case disease
         INNER JOIN public.emr_patient pat ON disease.patient_id = pat.id
         INNER JOIN public.emr_provenance prvn ON pat.provenance_id = prvn.provenance_id
  WHERE prvn.source ilike 'epicmem%'
        and (disease.condition in ('depression', 'ili', 'diabetes:type-1', 'diabetes:type-2', 'diabetes:gestational', 'diabetes:prediabetes')
        or disease.id in (select id from esp_current_asthma_cases));

-- Instantiate tables from previously created views
drop table if exists esp_demographic cascade;
create table esp_demographic as select * from esp_demographic_v;
create unique index esp_demographic_patid_unique_idx on esp_demographic (patid);
create index esp_demographic_centerid_idx on esp_demographic (centerid);
create index esp_demographic_birth_date_idx on esp_demographic (birth_date);
create index esp_demographic_sex_idx on esp_demographic (sex);
create index esp_demographic_hispanic_idx on esp_demographic (hispanic);
create index esp_demographic_race_idx on esp_demographic (race);
create index esp_demographic_race_eth_idx on esp_demographic (race_ethnicity);
create index esp_demographic_zip5_idx on esp_demographic (zip5); 
create index esp_demographic_smk_idx on esp_demographic (smoking);
alter table esp_demographic add primary key (patid);

drop table if exists esp_encounter cascade;
create table esp_encounter as select t0.* from esp_encounter_v 
	as t0 inner join esp_demographic as t1 
            on t0.patid=t1.patid;


create index esp_encounter_centerid_idx on esp_encounter (centerid);
create index esp_encounter_patid_idx on esp_encounter (patid);
create unique index esp_encounter_encounterid_idx on esp_encounter (encounterid);
create index esp_encounter_a_date_idx on esp_encounter (a_date);
create index esp_encounter_d_date_idx on esp_encounter (d_date);
create index esp_encounter_provider_idx on esp_encounter (provider);
create index esp_encounter_facility_location_idx on esp_encounter (facility_location);
create index esp_encounter_facility_code_idx on esp_encounter (facility_code);
create index esp_encounter_enc_year_idx on esp_encounter (enc_year);
create index esp_encounter_age_at_enc_year_idx on esp_encounter (age_at_enc_year);
create index esp_encounter_age_group_5yr_idx on esp_encounter (age_group_5yr);
create index esp_encounter_age_group_10yr_idx on esp_encounter (age_group_10yr);
create index esp_encounter_age_group_ms_idx on esp_encounter (age_group_ms);
alter table esp_encounter add primary key (encounterid);
alter table esp_encounter add foreign key (patid) references esp_demographic (patid);

drop table if exists esp_diagnosis cascade;

create table esp_diagnosis as select t0.* from esp_diagnosis_v
            as t0 inner join esp_demographic as t1
            on t0.patid=t1.patid;
create index esp_diagnosis_dx_idx on esp_diagnosis (dx);
CREATE INDEX esp_diagnosis_dx_like_idx ON esp_diagnosis USING btree (dx varchar_pattern_ops);
create index esp_diagnosis_centerid_idx on esp_diagnosis (centerid);
create index esp_diagnosis_patid_idx on esp_diagnosis (patid);
create index esp_diagnosis_encounterid_idx on esp_diagnosis (encounterid);
create index esp_diagnosis_enc_year_idx on esp_diagnosis (enc_year);
create index esp_diagnosis_age_at_enc_year_idx on esp_diagnosis (age_at_enc_year);
create index esp_diagnosis_age_group_10yr_idx on esp_diagnosis (age_group_10yr);
create index esp_diagnosis_age_group_ms_idx on esp_diagnosis (age_group_ms);
alter table esp_diagnosis add primary key (patid, encounterid, dx);
alter table esp_diagnosis add foreign key (patid) references esp_demographic (patid);
alter table esp_diagnosis add foreign key (encounterid) references esp_encounter (encounterid);

drop table if exists esp_disease cascade;
create table esp_disease as select t0.* from esp_disease_v
            as t0 inner join esp_demographic as t1
            on t0.patid=t1.patid;

--POPULATE THE ESP DISEASE VIEW WITH OPIOID/BENZO CONDITIONS FOR THE LAST 365 DAYS (FROM ESP_CONDITION TABLE)
INSERT INTO esp_disease (
            SELECT T1.centerid, T1.patid, T1.condition, T1.date, age_at_detect_year, age_group_5yr, age_group_10yr, age_group_ms, criteria, status, notes   
            FROM esp_condition AS T1,
			(SELECT patid, condition, max(date) date FROM esp_condition WHERE condition in ('benzodiarx', 'benzopiconcurrent', 'highopioiduse', 'opioidrx') AND (current_date-('1960-01-01'::date) - date <= 365) GROUP BY  patid, condition) AS T2,
            esp_demographic AS T3
            WHERE T1.patid = T2.patid 
            AND T1.patid = T3.patid
            AND T1.date = T2.date
	        AND T1.condition = T2.condition
            AND (current_date-('1960-01-01'::date) - T1.date <= 365)
            AND T1.condition in ('benzodiarx', 'benzopiconcurrent', 'highopioiduse', 'opioidrx')
			);

--POPULATE THE ESP DISEASE VIEW WITH OBESITY CONDITIONS. SINGLE CONDITION PER PATIENT
INSERT INTO esp_disease (
            SELECT DISTINCT ON (T1.patid) T1.centerid, T1.patid, condition, T1.date, age_at_detect_year, age_group_5yr, age_group_10yr, age_group_ms, criteria, status, notes 
            FROM esp_condition AS T1,
            (SELECT patid, max(date) date FROM esp_condition WHERE condition ilike '%bmi%' GROUP BY  patid) AS T2,
            esp_demographic AS T3
            WHERE T1.patid = T2.patid 
            AND T1.patid = T3.patid
            AND T1.date = T2.date
            AND T1.age_at_detect_year >= 20
            AND condition ilike '%bmi%'); 

create index esp_disease_age_group_10yr_idx on esp_disease (age_group_10yr);
create index esp_disease_age_group_5yr_idx on esp_disease (age_group_5yr);
create index esp_disease_age_group_ms_idx on esp_disease (age_group_ms);
create index esp_disease_centerid_idx on esp_disease (centerid);
create index esp_disease_patid_idx on esp_disease (patid);
create index esp_disease_condition_idx on esp_disease (condition);
create index esp_disease_date_idx on esp_disease (date);
create index esp_disease_age_at_detect_year_idx on esp_disease (age_at_detect_year);
create index esp_disease_criteria_idx on esp_disease (criteria);
create index esp_disease_status_idx on esp_disease (status);
alter table esp_disease add primary key (patid, condition, date);
alter table esp_disease add foreign key (patid) references esp_demographic (patid);

drop view if exists esp_disease_v;
drop view if exists esp_diagnosis_v;
drop view if exists esp_encounter_v;
drop view if exists esp_demographic_v;

-- UVT_TABLES
--    UVT_SEX
      DROP TABLE if exists UVT_SEX;
      CREATE TABLE UVT_SEX AS
      SELECT DISTINCT
             pat.sex as item_code,
             CASE
               WHEN pat.sex = 'M' THEN 'Male'::varchar(10)
               WHEN pat.sex = 'F' THEN 'Female'::varchar(10)
               WHEN pat.sex = 'U' THEN 'Unknown'::varchar(10)
               ELSE 'Not Mapped'::varchar(10)
             END as item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_SEX ADD PRIMARY KEY (item_code);

--    UVT_RACE
      DROP TABLE if exists UVT_RACE;
      CREATE TABLE UVT_RACE AS
      SELECT DISTINCT
             pat.race as item_code,
             CASE
               WHEN pat.race = 0 THEN 'Unknown'::varchar(50)
               WHEN pat.race = 1 THEN 'American Indian or Alaska Native'::varchar(50)
               WHEN pat.race = 2 THEN 'Asian'::varchar(50)
               WHEN pat.race = 3 THEN 'Black or African American'::varchar(50)
               WHEN pat.race = 4 THEN 'Native Hawaiian or Other Pacific Islander'::varchar(50)
               WHEN pat.race = 5 THEN 'White'::varchar(50)
               ELSE 'Not Mapped'::varchar(50)
             END as item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_RACE ADD PRIMARY KEY (item_code);

--    UVT_RACE_ETHNICITY
      DROP TABLE IF EXISTS UVT_RACE_ETHNICITY;
      CREATE TABLE UVT_RACE_ETHNICITY AS
      SELECT DISTINCT
             pat.race_ethnicity item_code,
             CASE
               WHEN pat.race_ethnicity = 1 THEN 'Native American'::varchar(50) 
               WHEN pat.race_ethnicity = 2 THEN 'Asian'::varchar(50)
               WHEN pat.race_ethnicity = 3 THEN 'Black'::varchar(50)
               WHEN pat.race_ethnicity = 5 THEN 'White'::varchar(50)
               WHEN pat.race_ethnicity = 6 then 'Hispanic'::varchar(50)
               WHEN pat.race_ethnicity = 0 then 'Unknown'::varchar(50)
               ELSE 'Not Mapped'::varchar(50)
             END item_text
        FROM esp_demographic pat;
        ALTER TABLE UVT_RACE_ETHNICITY ADD PRIMARY KEY (item_code);

--    UVT_ZIP5
      DROP TABLE if exists uvt_zip5;
      create table uvt_zip5 as 
      select distinct 
             zip5 as item_code, 
	         null::varchar(10) as item_text
      from esp_demographic where zip5 is not null;
      alter table uvt_zip5 add primary key (item_code);


--    UVT_AGEGROUP_10YR
      DROP TABLE if exists UVT_AGEGROUP_10YR;
      CREATE TABLE UVT_AGEGROUP_10YR AS
      SELECT DISTINCT
             enc.age_group_10yr as item_code,
             enc.age_group_10yr::varchar(5) as item_text
        FROM esp_encounter enc
      WHERE enc.age_group_10yr is not null;
        ALTER TABLE UVT_AGEGROUP_10YR ADD PRIMARY KEY (item_code);




--    REMOTE KEYS USING UVTs
      ALTER TABLE esp_demographic ADD FOREIGN KEY (sex) REFERENCES uvt_sex (item_code);
      ALTER TABLE esp_demographic ADD FOREIGN KEY (race) REFERENCES uvt_race (item_code);
      ALTER TABLE esp_encounter ADD FOREIGN KEY (age_group_10yr) 
                  REFERENCES uvt_agegroup_10yr (item_code);
				  
				  
		GRANT SELECT ON esp_demographic TO mdphnet_user;
        GRANT SELECT ON esp_diagnosis TO mdphnet_user;
        GRANT SELECT ON esp_disease TO mdphnet_user;
        GRANT SELECT ON esp_encounter TO mdphnet_user;
        GRANT SELECT ON mdphnet_schema_update_history TO mdphnet_user;
        GRANT SELECT ON mdphnet_updated_patients TO mdphnet_user;
        GRANT SELECT ON uvt_agegroup_10yr TO mdphnet_user;
        GRANT SELECT ON uvt_agegroup_5yr TO mdphnet_user;
        GRANT SELECT ON uvt_agegroup_ms TO mdphnet_user;
        GRANT SELECT ON uvt_center TO mdphnet_user;
        GRANT SELECT ON uvt_detected_condition TO mdphnet_user;
        GRANT SELECT ON uvt_detected_criteria TO mdphnet_user;
        GRANT SELECT ON uvt_detected_status TO mdphnet_user;
        GRANT SELECT ON uvt_site TO mdphnet_user;
        GRANT SELECT ON uvt_encounter TO mdphnet_user;
        GRANT SELECT ON uvt_period TO mdphnet_user;
        GRANT SELECT ON uvt_provider TO mdphnet_user;
        GRANT SELECT ON uvt_race TO mdphnet_user;
        GRANT SELECT ON uvt_race_ethnicity TO mdphnet_user;
        GRANT SELECT ON uvt_sex TO mdphnet_user;
        GRANT SELECT ON uvt_site TO mdphnet_user;
        GRANT SELECT ON uvt_zip5 TO mdphnet_user;

