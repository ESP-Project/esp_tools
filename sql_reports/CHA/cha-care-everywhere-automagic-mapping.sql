--covid19_pcr (with Flu A/B)
DROP TABLE IF EXISTS kre_report.carev_covid19_pcr_fluab;
CREATE TABLE kre_report.carev_covid19_pcr_fluab AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE EVERWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SARS COV2 RNA CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CARE PCR EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- COVID-19 CARE PCR EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE PCR EVERYWHERE%'
	   OR native_name ilike 'COVID FLU A/B CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 CARE EVERYWHRE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 PCR POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
       )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode)
and native_name !~ ('SOURCE|RAPID');

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_pcr', native_code, 'exact', 'both', null, true, '94558-4', '840539006', 'MDPH-R502', null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_pcr_fluab);

DROP TABLE IF EXISTS kre_report.carev_covid19_pcr_fluab;

--covid19_pcr
DROP TABLE IF EXISTS kre_report.carev_covid19_pcr;
CREATE TABLE kre_report.carev_covid19_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 OVERALL RESULT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 CARE PCR EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS COV2 RNA CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 CARE PCR EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND RSV CARE EVERYWHERE-- COVID-19 PCR CARE EVERYWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode)
and native_name !~ ('SOURCE|POC|RAPID');

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_pcr', native_code, 'exact', 'both', null, true, 'MDPH-324', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_covid19_pcr);

DROP TABLE IF EXISTS kre_report.carev_covid19_pcr;


--covid19_igg
DROP TABLE IF EXISTS kre_report.carev_covid19_igg;
CREATE TABLE kre_report.carev_covid19_igg AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE IGG INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE IGG ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY INTERP IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY INDEX IGG CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SARS-COV-2 IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGG CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_igg', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_igg);

DROP TABLE IF EXISTS kre_report.carev_covid19_igg;

--covid19_igm
DROP TABLE IF EXISTS kre_report.carev_covid19_igm;
CREATE TABLE kre_report.carev_covid19_igm AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGM CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 IGM CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY IGM CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_igm', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_igm);

DROP TABLE IF EXISTS kre_report.carev_covid19_igm;



--influenza
DROP TABLE IF EXISTS kre_report.carev_influenza;
CREATE TABLE kre_report.carev_influenza AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- INFLUENZA%'
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN RAPID CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE A AND B RAPID CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE A AND B RAPID CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE A AND B RAPID CARE EVERYWHERE-- INFLUENZA % RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA H1N1 PCR CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE AND RSV POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- INFLUENZA CARE EVERYWHERE%'
	   OR native_name ilike 'COVID FLU A/B CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID FLU A/B POC CARE EVERYWHERE-- INFLUENZA % POC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA % ANTIGEN POC CARE EVERYWHERE-- RAPID INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE A AND B RAPID CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- INFLUENZA A AND B POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA % PCR CARE EVERYWHERE-- INFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN RAPID CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA % CARE EVERYWHERE%'
	  )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'influenza', native_code, 'exact', 'both', null, true, null, null, null, null, 
 'care everywhere automagic add' FROM kre_report.carev_influenza);

DROP TABLE IF EXISTS kre_report.carev_influenza;




--covid19_ag
DROP TABLE IF EXISTS kre_report.carev_covid19_ag;
CREATE TABLE kre_report.carev_covid19_ag AS
select * from emr_labtestconcordance
where (native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN POC CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 ANTIGEN CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- SARS-COV-2 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 RAPID POC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 ANTIGEN CARE%'
	   OR native_name ilike 'COVID FLU A/B POC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- COVID-19 CARE POC EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 CARE POC EVERYWHERE%'
       OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 RAPID CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
       OR native_name ilike 'COVID FLU A/B POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID-- COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 ANTIGEN POC CARE EVERWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 POC CARE EVERYWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_ag', native_code, 'exact', 'both', null, true, '94558-4', '10828004', '260385009', '42425007', 'care everywhere automagic add' FROM kre_report.carev_covid19_ag);


DROP TABLE IF EXISTS kre_report.carev_covid19_ag;

--covid19_ab_total
DROP TABLE IF EXISTS kre_report.carev_covid19_ab_total;
CREATE TABLE kre_report.carev_covid19_ab_total AS
select * from emr_labtestconcordance
where (native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 AB NUCLEOCAPSID AB CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY%'
	   OR native_name ilike 'SARS-COV-2 AB NUCLEOCAPSID CARE EVERYWHERE-- SARS-COV-2 AB NUCLEOCAPSID AB CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE IGG ANTIBODY CARE EVERYWHERE-- COVID-19 ANTIBODY STATUS CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY TOTAL INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SARS-COV-2 AB NUCLEOCAPSID AB CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY CARE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'covid19_ab_total', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_covid19_ab_total);

DROP TABLE IF EXISTS kre_report.carev_covid19_ab_total;


--creatinine
DROP TABLE IF EXISTS kre_report.carev_creatinine;
CREATE TABLE kre_report.carev_creatinine AS
select * from emr_labtestconcordance
where (native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE CARE EVERWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE POC CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE POC CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE POC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE WHOLE BLOOD CARE EVERYWHERE-- CREATININE WHOLE BLOOD CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE POC CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- CREATININE WHOLE BLOOD POC CARE EVERWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE POC CARE EVERWHERE%'
       OR native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE POC CARE EVERWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CREATININE PLASMA CARE EVERYWHERE%'
	   OR native_name ilike 'ESTIMATED GLOMERULAR FILTRATION RATE CARE EVERYWHERE-- CREATININE POC CARE EVERWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'creatinine', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_creatinine);

DROP TABLE IF EXISTS kre_report.carev_creatinine;

--a1c
DROP TABLE IF EXISTS kre_report.carev_a1c;
CREATE TABLE kre_report.carev_a1c AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOGIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- GLYCOHEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMOGLOBIN A1C CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- POC HGB A1C CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- GLYCOHEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMOGLOBIN A1C POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN POC CARE EVERYWHERE-- HEMOGLOBIN A1C CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'a1c', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_a1c);

DROP TABLE IF EXISTS kre_report.carev_a1c;

--hemoglobin
DROP TABLE IF EXISTS kre_report.carev_hemoglobin;
CREATE TABLE kre_report.carev_hemoglobin AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMOGLOBIN CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN PLASMA CARE EVERYWHERE-- HEMOGLOBIN TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN POC CARE EVERYWHERE-- HEMOGLOBIN POC CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOLOBIN AND HEMATOCRIT CARE EVERYWHERE-- HEMOGLOBIN POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN POC CARE EVERYWHERE-- HEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'CBC WITH PLATELET CARE EVERYWHERE -- HEMOGLOBIN CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEMOGLOBIN A1C CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMOGLOBIN POC CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hemoglobin', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_hemoglobin);

DROP TABLE IF EXISTS kre_report.carev_hemoglobin;


--hepatitis_a_igm_antibody
DROP TABLE IF EXISTS kre_report.carev_hepatitis_a_igm_antibody;
CREATE TABLE kre_report.carev_hepatitis_a_igm_antibody AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGM INTERP CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_a_igm_antibody', native_code, 'exact', 'both', null, true, '22314-9', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_a_igm_antibody);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_a_igm_antibody;


--hepatitis_b_core_ab
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_ab;
CREATE TABLE kre_report.carev_hepatitis_b_core_ab AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE TOTAL ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B CORE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY INTERP CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS BE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_core_ab', native_code, 'exact', 'both', null, true, '16933-4', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_core_ab);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_ab;


--hepatitis_b_e_antigen
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_e_antigen;
CREATE TABLE kre_report.carev_hepatitis_b_e_antigen AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS BE ANTIGEN CARE EVERYWHERE-- HEPATITIS BE ANTIGEN CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS BE ANTIGEN ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS BE ANTIGEN CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_e_antigen', native_code, 'exact', 'both', null, true, '13954-3', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_e_antigen);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_e_antigen;

--hepatitis_b_surface_antigen
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_surface_antigen;
CREATE TABLE kre_report.carev_hepatitis_b_surface_antigen AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE--%HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CONFIRM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN CONFIRM CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_surface_antigen', native_code, 'exact', 'both', null, true, '5195-3', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_surface_antigen);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_surface_antigen;



--hepatitis_c_elisa
DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_elisa;
CREATE TABLE kre_report.carev_hepatitis_c_elisa AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C ANTIBODY INTERPRETATION CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_c_elisa', native_code, 'exact', 'both', null, true, '16128-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_c_elisa);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_elisa;


--hepatitis_c_rna
--QUALITATIVE RESULTS - PCR IN NAME
DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_rna;
CREATE TABLE kre_report.carev_hepatitis_c_rna AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS C RNA PCR CARE EVERYWHERE-- HEPATITIS C RNA PCR CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS C RNA PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_c_rna', native_code, 'exact', 'both', null, true, '5012-0', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_c_rna);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_c_rna;


--hepatitis_b_core_antigen_igm_antibody
DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_antigen_igm_antibody;
CREATE TABLE kre_report.carev_hepatitis_b_core_antigen_igm_antibody AS
select * from emr_labtestconcordance
where (native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE IGM ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hepatitis_b_core_antigen_igm_antibody', native_code, 'exact', 'both', null, true, '31204-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hepatitis_b_core_antigen_igm_antibody);

DROP TABLE IF EXISTS kre_report.carev_hepatitis_b_core_antigen_igm_antibody;


--hematocrit
DROP TABLE IF EXISTS kre_report.carev_hematocrit;
CREATE TABLE kre_report.carev_hematocrit AS
select * from emr_labtestconcordance
where (native_name ilike 'HEMATOCRIT POC CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
       OR native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOLOBIN AND HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT MANUAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT POC CARE EVERYWHERE-- HEMATOCRIT POC CARE EVERYWHERE%'
	   OR native_name ilike 'CBC WITH PLATELET CARE EVERYWHERE -- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- HEMATOCRIT CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOLOBIN AND HEMATOCRIT CARE EVERYWHERE-- HEMATOCRIT POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN AND HEMATOCRIT CARE EVERWHERE-- HEMATOCRIT POC CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hematocrit', native_code, 'exact', 'both', null, true, '20570-8', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_hematocrit);

DROP TABLE IF EXISTS kre_report.carev_hematocrit;

--platelet_count
DROP TABLE IF EXISTS kre_report.carev_platelet_count;
CREATE TABLE kre_report.carev_platelet_count AS
select * from emr_labtestconcordance
where (native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- PLATELET COUNT CARE EVERYWHERE%'
       OR native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- PLATELET COUNT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PLATELET COUNT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PLATELETS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PLATELET COUNT BLUE TOP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PLATELET COUNT (CITRATED BLD) CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- PLATELETS CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'platelet_count', native_code, 'exact', 'both', null, true, '13056-7', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_platelet_count);

DROP TABLE IF EXISTS kre_report.carev_platelet_count;


--rsv
DROP TABLE IF EXISTS kre_report.carev_rsv;
CREATE TABLE kre_report.carev_rsv AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- RSV % CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RSV ANTIGEN CARE EVERYWHERE-- RSV ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'RSV CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV PCR CARE PCR EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'RSV POC CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE AND RSV POC CARE EVERYWHERE-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'RSV POC CARE EVERYWHERE-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RSV % CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- RSV CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV A CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV B CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV ANTIGEN CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- RSV PCR POC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RSV RAPID POC CARE EVERYWHERE%'
	   OR native_name ilike 'RSV CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV RAPID POC CARE EVERYWHERE%'
	   OR native_name ilike 'RSV CARE EVERYWHERE-- RSV POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND RSV CARE EVERYWHERE-- RSV PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RSV RAPID PCR CARE EVERYWHERE%'
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'rsv', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_rsv);

DROP TABLE IF EXISTS kre_report.carev_rsv;

--adenovirus
DROP TABLE IF EXISTS kre_report.carev_adenovirus;
CREATE TABLE kre_report.carev_adenovirus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- ADENOVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- ADENOVIRUS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ADENOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- ADENOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ADENOVIRUS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'adenovirus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_adenovirus);

DROP TABLE IF EXISTS kre_report.carev_adenovirus;


--parapertussis
DROP TABLE IF EXISTS kre_report.carev_parapertussis;
CREATE TABLE kre_report.carev_parapertussis AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PARAPERTUSSIS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- BORDETELLA PARAPERTUSSIS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- BORDETELLA PARAPERTUSSIS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'parapertussis', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_parapertussis);

DROP TABLE IF EXISTS kre_report.carev_parapertussis;

--pertussis_pcr
DROP TABLE IF EXISTS kre_report.carev_pertussis_pcr;
CREATE TABLE kre_report.carev_pertussis_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA PERTUSSIS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PERTUSSIS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- BORDETELLA PERTUSSIS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- BORDETELLA PERTUSSIS CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'pertussis_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_pertussis_pcr);

DROP TABLE IF EXISTS kre_report.carev_pertussis_pcr;

--h_metapneumovirus
DROP TABLE IF EXISTS kre_report.carev_h_metapneumovirus;
CREATE TABLE kre_report.carev_h_metapneumovirus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HUMAN METAPNEUMOVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HUMAN METAPNEUMOVIRUS CARE PCR EVERYWHERE%'
       OR native_name ilike 'HUMAN METAPNEUMOVIRUS CARE EVERYWEHRE-- HUMAN METAPNEUMOVIRUS CARE PCR EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN METAPNEUMOVIRUS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HUMAN METAPNEUMOVIRUS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'h_metapneumovirus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_h_metapneumovirus);

DROP TABLE IF EXISTS kre_report.carev_h_metapneumovirus;

--rhino_entero_virus
DROP TABLE IF EXISTS kre_report.carev_rhino_entero_virus;
CREATE TABLE kre_report.carev_rhino_entero_virus AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- HUMAN RHINOVIRUS/ENTEROVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN RHINOVIRUS/ENTEROVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RHINOVIRUS / ENTEROVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HUMAN RHINOVIRUS/ENTEROVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RHINOVIRUS / ENTEROVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RHINOVIRUS / ENTEROVIRUS PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- HUMAN RHINOVIRUS/ENTEROVIRUS PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'rhino_entero_virus', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_rhino_entero_virus);

DROP TABLE IF EXISTS kre_report.carev_rhino_entero_virus;


--parainfluenza
DROP TABLE IF EXISTS kre_report.carev_parainfluenza;
CREATE TABLE kre_report.carev_parainfluenza AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- PARAINFLUENZA % PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PARAINFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- % PARAINFLUENZA % CARE EVERYWHERE%' 
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
       OR native_name ilike 'PARAINFLUENZA CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
	   OR native_name ilike 'PARAINFLUENZA % CARE EVERYWHERE-- PARAINFLUENZA % CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'parainfluenza', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_parainfluenza);

DROP TABLE IF EXISTS kre_report.carev_parainfluenza;

--c_pneumoniae_pcr
DROP TABLE IF EXISTS kre_report.carev_c_pneumoniae_pcr;
CREATE TABLE kre_report.carev_c_pneumoniae_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CHLAMYDIA PNEUMONIA CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CHLAMYDIA PNEUMONIA PCR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CHLAMYDIA PNEUMONIA CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CHLAMYDIA PNEUMONIA PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'c_pneumoniae_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_c_pneumoniae_pcr);

DROP TABLE IF EXISTS kre_report.carev_c_pneumoniae_pcr;

--m_pneumoniae_pcr
DROP TABLE IF EXISTS kre_report.carev_m_pneumoniae_pcr;
CREATE TABLE kre_report.carev_m_pneumoniae_pcr AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- MYCOPLASMA PNEUMONIAE CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- MYCOPLASMA PNEUMONIAE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- MYCOPLASMA PNEUMONIAE PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'm_pneumoniae_pcr', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_m_pneumoniae_pcr);

DROP TABLE IF EXISTS kre_report.carev_m_pneumoniae_pcr;

--coronavirus_non19
DROP TABLE IF EXISTS kre_report.carev_coronavirus_non19;
CREATE TABLE kre_report.carev_coronavirus_non19 AS
select * from emr_labtestconcordance
where (native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS 229E CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS HKU1 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS NL63 CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS OC43 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS 229E CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS HKU1 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS NL63 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS OC43 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONA(229E, HKU1, NL63, OC43) CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS, NOT COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CORONAVIRUS CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONA(229E, OC43) CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONA(NL63, HKU1) CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS HKU CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONA(229E, HKU1, NL63, OC43) CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS HKU1 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS NL63 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS OC43 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- CORONAVIRUS 229E PCR CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'coronavirus_non19', native_code, 'exact', 'both', null, true, null, null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_coronavirus_non19);

DROP TABLE IF EXISTS kre_report.carev_coronavirus_non19;


/* --hiv_elisa_1and2
   -- DISABLE FOR NOW PENDING REVIEW
DROP TABLE IF EXISTS kre_report.carev_hiv_elisa_1and2;
CREATE TABLE kre_report.carev_hiv_elisa_1and2 AS
select * from emr_labtestconcordance
where (native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV RAPID CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY SCREEN%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV-1/2 ANTIBODY RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV-1/2 ANTIBODY RAPID CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hiv_elisa', native_code, 'exact', 'both', null, true, '43010-8', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hiv_elisa_1and2);

DROP TABLE IF EXISTS kre_report.carev_hiv_elisa_1and2; */


--hiv_ag_ab
DROP TABLE IF EXISTS kre_report.carev_hiv_ag_ab;
CREATE TABLE kre_report.carev_hiv_ag_ab AS
select * from emr_labtestconcordance
where (native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY/ P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1/2 PLUS O AG/AB SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY/ P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1 AND 2 ANTIBODY/ P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIVDUO RESULT CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- HIV 1 AND 2 ANTIBODY/ P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV SCREEN CARE EVERYWHERE-- HIV CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB SCREEN S/CO CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1/2 ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1/2 ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1/2 RAPID ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HIV 1/2 PLUS O AG/AB CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'hiv_ag_ab', native_code, 'exact', 'both', null, true, '56888-1', '10828004', '260385009', '42425007', 
                'care everywhere automagic add' FROM kre_report.carev_hiv_ag_ab);

DROP TABLE IF EXISTS kre_report.carev_hiv_ag_ab;


--wbc
DROP TABLE IF EXISTS kre_report.carev_wbc;
CREATE TABLE kre_report.carev_wbc AS
select * from emr_labtestconcordance
where (native_name ilike 'CBC, PLATELET & DIFFERENTIAL CARE EVERYWHERE-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
	   OR native_name ilike 'WHITE BLOOD CELL COUNT CARE EVERYWHERE-- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
	   OR native_name ilike 'CBC WITH PLATELET CARE EVERYWHERE -- WHITE BLOOD CELL COUNT CARE EVERYWHERE%'
      )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_labtestmap (id, test_name, native_code, code_match_type, record_type, threshold, reportable, output_code, snomed_pos, snomed_neg, snomed_ind, notes) 
(SELECT nextval('conf_labtestmap_id_seq'), 'wbc', native_code, 'exact', 'both', null, true, '26464-8', null, null, null, 
                'care everywhere automagic add' FROM kre_report.carev_wbc);

DROP TABLE IF EXISTS kre_report.carev_wbc;




--all IGNORES
DROP TABLE IF EXISTS kre_report.carev_ignores;
CREATE TABLE kre_report.carev_ignores AS
select DISTINCT native_code, native_name from emr_labtestconcordance
where (native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- DAT CORD BLOOD CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- ESTIMATED AVERAGE GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- MEAN PLASMA GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DAT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- MEAN PLATELET VOLUME CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- BORDETELLA HOLMESII DNA CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1 P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- CT VALUE TARGET 1 CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ABO GROUP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RH TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- WILL PATIENT BE ADMITTED? CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUT OFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL/CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUT OFF CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS B SURFACE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- APTT CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- PROTHROMBIN TIME CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV P24 ANTIGEN RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- POC QC PERFORMED? CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- PROCEDURAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- S/CO RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'	 
       OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- COVID-19 CARE EVERWHERE SOURCE%'
       OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'	   
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COMMENT, COVID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 CARE EVERWHERE SOURCE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- EMPLOYED IN A HEALTHCARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- ETHNICITY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- METHOD SUMMARY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- MOLECULAR DEVICE ID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT RACE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SOURCE, COVID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TEST INFORMATION CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT RACE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TEST PERFORMED BY CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CARE EVERYWHERE-- DAT CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CARE EVERYWHERE-- DAT IGG CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- ABO GROUP CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- DAT ANTI IGG CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CORD CARE EVERYWHERE-- RH TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'ESTIMATED GLOMERULAR FILTRATION RATE CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'ESTIMATED GLOMERULAR FILTRATION RATE CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- CALCULATED MEAN GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- MEAN BLOOD GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN PLASMA CARE EVERYWHERE-- OXYHEMOGLOBIN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGM CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY IGM CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- PERFORMING LAB CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS D ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS D ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS D ANTIGEN CARE EVERYWHERE-- HEPATITIS D VIRUS ANTIGEN%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS E IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS E IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP A IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP C AB CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- NOTE, HEP C IGM CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- CONSENT FORM CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- HIV CARTRIDGE LOT # CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- REFERRAL SOURCE CARE EVERYWHERE%'
       OR native_name ilike 'HIV 1 ANTIBODY POC CARE EVERYWHERE-- SAMPLE PORT QC CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
       OR native_name ilike 'HIV ANTIBODY POC CARE EVERYWHERE-- KIT LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- NOTE, HIV 1/2 AB + HIV 1 AG CARE EVERYWHERE%'
	   OR native_name ilike 'NFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- QC CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ABO/RH CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID TESTING STATUS CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- FILE HISTORY CHECK CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY VIRUS PANEL CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- PRIOR TEST DATE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUT OFFCARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEP ACUTE INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUTOFF RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INR CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'INR POC CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'INR POC CARE EVERYWHERE-- INR POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- ANTICOAG THERAPY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- ISOLATION GUIDELINES CARE EVERY%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PRIOR TEST DATE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B CORE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- CONFIRMATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL TO CUT OFFCARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- TESTING LOCATION CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME CARE EVERYWHERE-- ANTICOAG THERAPY CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- HEALTHCARE WORKER CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- NOTE CARE EVERYWHERE%'	  
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'	 
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INTERNAL KIT QC CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
       OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- ISOLATION GUIDELINES CARE EVERY%'
       OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- PROCEDURAL COMMENT CARE EVERYWHERE%'	   
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- CONGREGATE RESIDENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- EMPLOYED IN A HEALTHCARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PATIENT INSTRUCTIONS CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- RESP COMBO COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INTERNAL CONTROLS CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INTERNAL NEGATIVE QC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- INTERNAL POSITIVE QC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT INSTRUCTIONS CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SPECIMEN TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- DATE DRAWN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- DEVICE ID CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- DRAWN BY CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- OPERATOR ID CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- SAMPLE TYPE POC CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- TIME DRAWN CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL/CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS D ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SIGNAL TO CUT OFF CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME POC CARE EVERYWHERE-- INR CARE EVERYWHERE%'
	   OR native_name ilike 'PROTHROMBIN TIME POC CARE EVERYWHERE-- PROTHROMBIN TIME POC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 TARGET 1 CARE EVERYWHERE%'
	   OR native_name ilike 'RSV ANTIGEN CARE EVERYWHERE-- RSV SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG ANTIBODY CARE EVERYWHERE-- SARS-COV-2 ANTIBODY COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE-- HEPATITIS A ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SIGNAL TO CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- INFLUENZA COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- CONTROL BAND CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- INTERNAL COVID OQ CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE WHOLE BLOOD CARE EVERYWHERE-- ESTIMATED GFR AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE WHOLE BLOOD CARE EVERYWHERE-- ESTIMATED GFR CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS D ANTIBODY IGM CARE EVERYWHERE-- HEPATITIS D ANTIBODY IGM CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- REFLEX CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 AB NUCLEOCAPSID CARE EVERYWHERE-- ETHNICITY CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 AB NUCLEOCAPSID CARE EVERYWHERE-- RACE CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- CONGREGATE RESIDENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- HAS PT HAD SIGNIFICANT EXPOSURE TO COVID19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- IF SYMPTOMATIC, DATE OF ONSET? CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- IS THIS THE PT FIRST COVID19 TEST? CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SYMPTOMATIC? CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- RACE CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- TYPE OF SPECIMEN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- USER ID CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA H1N1 PCR CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- PROTHROMBIN CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 QUALITATIVE PCR INDICATION CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RAPID FLU A, B, RSV, SARS-COV-2 PCR CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC POC CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED AVERAGE GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SOFIA CASSETTE LOT # EXT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SOFIA PROCEDURAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C RNA PCR CARE EVERYWHERE-- HCV VIRAL LOAD PERFORMED BY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C RNA PCR CARE EVERYWHERE-- HEPATITIS C VIRAL LOAD COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- INTERNAL CONTROL VALID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- SA25 COVID 19 POC TEST BRAND NAME CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- SOFIA CASSETTE LOT # EXT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- SOFIA PROCEDURAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- INTERNAL CONTROL RESULT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- COVID-19 POC INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'POC COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- QC PASS? CARE EVERYWHERE%'
	   OR native_name ilike 'RSV POC CARE EVERYWHERE-- RSV LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'RSV POC CARE EVERYWHERE-- RSV QC CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- BLOOD BANK HISTORY CHECK CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- EMPLOYED IN A HEALTHCARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- PRIOR TEST DATE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- PRIOR TEST RESULT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SYMPTOMATIC PER CDC CARE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL CARE EVERWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL TO CUT-OFF CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- HEPATITIS E IGG IGM CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR ADDITIONAL INFO CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS-COV-2 RNA SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR ADDITIONAL INFO CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- RAPID INFLUENZA LOT CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- PERFORMED BY CARE EVERYWHERE%'
       OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- TIME OF TEST CARE EVERYWHERE%'	
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY TOTAL CARE EVERWHERE%'	   
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- EXP DATE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- TEST LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- COVID-19 CYCLE NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 CARE EVERWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- POC COVID COMMENTS CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- SARS IDNOW INFORMATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- A1C COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- ADDITIONAL TESTING CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 CARE EVERWHERE SOURCE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RVP COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY QUANT CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- THERAPEUTIC HEP RANGE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- HEPATITIS C RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TEST ORDERED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19 COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- FIRST TEST FOR COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- HOSPITALIZED FOR COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- ICU PATIENT FOR COVID-19 CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- IF YES, DATE OF SYMPTOM ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- IS PATIENT PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- IS PATIENT SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- SPECIMEN DESCRIPTION CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COMMENT, SARS COV-2 RNA, RT PCR CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS-COV-2 CT VALUE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID AG KIT LOT# CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID AG TEST KIT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- INTERNAL CONTROL SARS-COV-2 ANTIGEN, RAPID CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- QUALITY CHECK CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- TEST KIT EXP. DATE, SARS-COV-2 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- TEST LOT #, SARS COV-2 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- RESULTS TO PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- SIGNAL CUTOFF CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B PCR CARE EVERYWHERE-- PERFORMING LAB CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- A1C COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- A1C INFORMATION CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- TECH ID CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'PT & PTT CARE EVERYWHERE-- SAMPLE TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS A ANTIBODY IGG CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGG INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS A ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- KIT EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- KIT LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- RESULTING DEVICE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- PATIENT SYMPTOMATIC? CARE EVERYWHERE%'
	   OR native_name ilike 'COVID FLU A/B POC CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR POC CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- PERFORMED AT CARE EVERYWHERE%'
	   OR native_name ilike 'HEMATOCRIT CARE EVERYWHERE-- PROVIDER CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTER P CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INTL UNITS CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- HEPATITIS E IGG CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- HEPATITIS E IGM CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS, ACUTE PANEL INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS C ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- COVID-19 RAPID COMMENT POC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- POC COVID EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- POC COVID INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- POC COVID LOT ID CARE EVERYWHERE%' 
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- IS PATIENT HAVING SURGERY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- REASON FOR TESTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- SPECIMEN DESCRIPTION CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- MESA TEST BRAND NAME CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DIRECT COOMBS, IGG CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA ANTIGEN CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID DMT INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- EMPLOYED IN HEALTHCARE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- COVID-19 CARE METHOD EVERWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SPECIMEN DESCRIPTION CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- ESTIMATED AVERAGE GLUCOSE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- MICRO NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SPECIMEN QUALITY CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- STATUS CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- COVID-19 POC INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- REASON FOR TESTING CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SPECIMEN DESCRIPTION CARE EVERYWHERE%'
	   OR native_name ilike 'TYPE AND SCREEN NEWBORN CARE EVERYWHERE-- ABO/RH CARE EVERYWHERE%'
	   OR native_name ilike 'TYPE AND SCREEN NEWBORN CARE EVERYWHERE-- ANTIBODY SCREEN CARE EVERYWHERE%'
	   OR native_name ilike 'TYPE AND SCREEN NEWBORN CARE EVERYWHERE-- DAT IGG CARE EVERYWHERE%'
	   OR native_name ilike 'TYPE AND SCREEN NEWBORN CARE EVERYWHERE-- SPECIMEN EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- ESTIMATED GFR POC AFRICAN AMERICAN CARE EVERYWHERE%'
	   OR native_name ilike 'DAT CARE EVERYWHERE-- DAT (IGG AND C3D) CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- CONGREGATE RESIDENT CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- EMPLOYER % CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- HEALTHCARE WORKER CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- OCCUPATION CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- ON SCHOOL PREMISES IN LAST 7 DAYS CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 IGG/IGM ANTIBODY CARE EVERYWHERE-- SYMPTOMATIC? CARE EVERYWHERE%'	  
       OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE AB INDEX CARE EVERYWHERE%'	  
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- REFERENCE RANGE CARE EVERYWHERE%'	   
       OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- TEST INFORMATION CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- PERFORMED BY CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID SOURCE CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- FIRST TIME TESTED FOR COVID CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- METHOD OF COMMUNICATION CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SYMPTOMATIC ON SET DATE CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TESTING LOCATION CARE EVERYWHERE%'
       OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR INTERPRETATION CARE EVERYWHERE%'
       OR native_name ilike 'HEMOGLOBIN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
       OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY INTERP CARE EVERWHERE%'
       OR native_name ilike 'HEPATITIS A ANTIBODY PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY TOTAL AND IGM CARE EVERWHERE%'
       OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- LAB STATUS CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B ANTIGEN CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INTERNAL CONTROL CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- KIT EXPIRATION DATE CARE EVERYWHERE%'
       OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- KIT LOT NUMBER CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- EMPLOYED IN HEALTH CARE CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ESTIMATED GFR INTERPRETATION CARE EVERYWHERE%'
       OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PERFORMED BY CARE EVERYWHERE%'
       OR native_name ilike 'COVID-19 CARE EVERYWHERE-- TESTING LOCATION CARE EVERYWHERE%'
	   OR native_name ilike 'APTT CARE EVERYWHERE-- APTT HEPARIN CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- EMPLOYED IN HEALTH CARE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- PATIENT HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- SYMPTOMATIC PER CDC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID-19 NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- IDNOW SERIAL# CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS-COV-2 CT COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B CARE EVERYWHERE-- IDNOW SERIAL# CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV PCR CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZAE A AND B RAPID CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HEPATITIS D VIRUS RNA CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PHYSICIAN INSTRUCTIONS CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- OPERATOR ID CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SARS-COV-2 CT COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN POC CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ANTIBODY ID CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SPECIMEN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- DEVICE SN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- POC OPERATOR CARE EVERYWHERE'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY TITER CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- DAT IGG CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- COMPONENT TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- ORDER TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- STATUS OF UNIT CARE EVERYWHERE%'
	   OR native_name ilike 'PLATELET COUNT CARE EVERYWHERE-- UNIT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- HEMOGLOBIN A1C INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1+2 AB COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- COVID-19 NOTE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- IDNOW SERIAL# CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- SPECIMEN TYPE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS BE ANTIGEN ANTIBODY CARE EVERYWHERE-- HEPATITIS BE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID-19 COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1 ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV2 ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1 P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV 1 P24 ANTIGEN INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV ANTIGEN-ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- HIV TESTING SUMMARY CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV INTERP CARE EVERYWHERE%'
	   OR native_name ilike 'PARAINFLUENZA 2 CARE EVERYWHERE-- PARA PANEL CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- INTERNAL % CONTROL CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B POC CARE EVERYWHERE-- KIT LOT# CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- DATE OF ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- EMPLOYED IN A HEALTHCARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN AND INFLUENZA ANTIGEN CARE EVERYWHERE-- SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- INTERNAL QC CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 POC CARE EVERYWHERE-- OPERATOR CARE EVERYWHERE%'
	   OR native_name ilike 'HEMOGLOBIN A1C CARE EVERYWHERE-- METER SERIAL# A1C - POC CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- DISCLAIMER CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- ELECTRONICALLY SIGNED BY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- METHODOLOGY CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- REFERENCE CUT-OFF INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- REFERENCES CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS E ANTIBODY IGG IGM CARE EVERYWHERE-- RESULT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HCV AB ADDTIONAL TESTING? CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HCV RATIO CARE EVERYWHERE%'
	   OR native_name ilike 'HIV ANTIGEN ANTIBODY CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV CARE EVERYWHERE-- COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA AND RSV CARE EVERYWHERE-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'POC HEMOGLOBIN A1C CARE EVERYWHERE-- ESTIMATED AVERAGE GLUCOSE CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 AND 2 ANTIBODY SCREEN CARE EVERYWHERE-- HIV 1+2 AB COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA POC CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS A ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- QC TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- METHOD CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SARS COV 2 METHODOLOGY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SCANNED REPORT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- SYMPTOM-ONSET CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- SARS-COV-2 E CT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- SARS-COV-2 N2 CT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- SARS-COV-2 RDRP CT CARE EVERWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- CREATININE COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- ESTIMATED GFR COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN RAPID CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA POC CARE EVERYWHERE-- LOT NUMBER CARE EVERYWHERE%'
	   OR native_name ilike 'POC HIV RAPID CARE EVERYWHERE-- QC TEST CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- COVID SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- FLU A/B & SARS-COV-2 PCR COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- FLU A/B & SARS-COV-2 PCR SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SARS COV 2 METHODOLOGY CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SYMPTOMATIC CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- CT VALUE TARGET 2 CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- INFO XCRT EX CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE POC CARE EVERYWHERE-- POINT OF CARE TESTING LOCATION CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B PANEL INTERPRETATION CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 SPIKE TOTAL ANTIBODY CARE EVERYWHERE-- DEVICE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS C ANTIBODY CARE EVERYWHERE-- SIGNAL CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- SIGNAL CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'CREATININE CARE EVERYWHERE-- PERFORMING LAB CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIBODY CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIBODY IMMUNITY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND INFLUENZA CARE EVERYWHERE-- COVID-19, INFLUENZA A/B COMMENT CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 AND RSV CARE EVERYWHERE-- SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- LOT EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 RAPID CARE EVERYWHERE-- QC MEDIA LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN RAPID CARE EVERYWHERE-- LOT EXPIRATION DATE CARE EVERYWHERE%'
	   OR native_name ilike 'INFLUENZA A AND B ANTIGEN RAPID CARE EVERYWHERE-- QC MEDIA LOT # CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- SOURCE, RESP PANEL PCR CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HBSAG S/CO CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS B SURFACE ANTIGEN CARE EVERYWHERE-- HEPATITIS B SURFACE ANTIGEN INDEX CARE EVERYWHERE%'
	   OR native_name ilike 'HIV 1 ANTIGEN CARE EVERYWHERE-- HIV 1 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL BASIC CARE EVERYWHERE-- SARS-COV-2 CT VALUE CARE EVERYWHERE%'
	   OR native_name ilike 'SARS-COV-2 TOTAL ANTIBODY CARE EVERYWHERE-- SARS-COV-2 SPIKE ANTIBODY CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 ANTIGEN CARE EVERYWHERE-- QC TEST CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- COVID TESTING STATUS CARE EVERYWHERE%'
	   OR native_name ilike 'COVID-19 CARE EVERYWHERE-- NEGATIVE IQC? CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- FIRST TEST CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HOSPITALIZED CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- ICU PATIENT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- PREGNANT CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- RESIDENT IN A CONGREGATE CARE SETTING CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SYMPTOMATIC PER CDC CARE CARE EVERYWHERE%'
	   OR native_name ilike 'RESPIRATORY PANEL EXTENDED CARE EVERYWHERE-- RESPIRATORY PATHOGEN PCR PANEL SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'HEPATITIS PANEL CARE EVERYWHERE-- HEPATITIS B CORE ANTIBODY IGG CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- HIV 1 P24 ANTIGEN CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SPECIMEN SOURCE CARE EVERYWHERE%'
	   OR native_name ilike 'LAB EXTERNAL RESULT UNMAPPED-- SPECIMEN TYPE CARE EVERYWHERE%'
	   
	   )
and native_code not in (select native_code from conf_labtestmap)
and native_code not in (select native_code from conf_ignoredcode);

INSERT INTO conf_ignoredcode (id, native_code) 
(SELECT nextval('conf_ignoredcode_id_seq'), native_code FROM kre_report.carev_ignores);

DROP TABLE IF EXISTS kre_report.carev_ignores;