drop table if exists temp_hep_all_flavors_unmapped;

create table temp_hep_all_flavors_unmapped as
select e.count, e.native_name, e.native_code, procedure_name
from emr_labtestconcordance e,
emr_labresult l
where e.native_code = l.native_code
and e.native_code not in (select native_code from conf_labtestmap)
and e.native_code not in (select native_code from conf_ignoredcode)
and e.native_name ~* '(hep|alt|ast|bili|tbil|hcv|sgpt|sgot|aminotrans|genotype|signal|hbc|hbv|hbsag|hbs|hav|hcv)'
group by e.count, e.native_name, e.native_code, procedure_name;



DROP TABLE IF EXISTS temp_hep_all_flavors_strings_unmapped_with_result_strings;
CREATE TABLE temp_hep_all_flavors_strings_unmapped_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name, array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)), 
 S1.native_code, S1.native_name, S1.procedure_name,
 result_string,
   RANK () OVER ( 
      PARTITION BY S1.native_code, S1.native_name, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank 
FROM
   emr_labresult S1,
   temp_hep_all_flavors_unmapped S2
   where S1.native_code = S2.native_code
   and S2.native_name ~* '(hep|alt|ast|bili|tbil|hcv|sgpt|sgot|aminotrans|genotype|signal|hbc|hbv|hbsag|hbs|hav|hcv)'
   group by S1.native_code, result_string, S1.native_name, S1.procedure_name) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name, T1.procedure_name
  from emr_labresult T1,
  temp_hep_all_flavors_unmapped T2
  where T1.native_code = T2.native_code 
  and T2.native_name ~* '(hep|alt|ast|bili|tbil|hcv|sgpt|sgot|aminotrans|genotype|signal|hbc|hbv|hbsag|hbs|hav|hcv)'
  group by T1.native_code, T1.native_name, T1.procedure_name) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_hep_all_flavors_strings_unmapped_with_result_strings_final;
CREATE TABLE temp_hep_all_flavors_strings_unmapped_with_result_strings_final AS
select T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, max(date) as most_recent_lab_date, T1.top_result_strings
from temp_hep_all_flavors_strings_unmapped_with_result_strings T1,
emr_labresult T2
where T1.native_code = T2.native_code
group by T1.lab_count, T1.native_code, T1.native_name, T1.procedure_name, T1.top_result_strings
order by T1.native_code, T1.native_name, T1.procedure_name;


-- \COPY temp_hep_all_flavors_strings_unmapped_with_result_strings_final  TO '/tmp/hepatitis_labs_for_review.csv' DELIMITER ',' CSV HEADER;



