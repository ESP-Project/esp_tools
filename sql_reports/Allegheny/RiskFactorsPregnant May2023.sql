select case 
           when count(*) <= 5 then null 
           else count(*) 
       end as counts, age_group, race, ethnicity, primary_payer, census_tract, zip, smoking, bmi_alt1, bmi_alt2,  bp_grp1, bp_grp2, max_a1c, max_ldl, max_trig, pregnancy
from 
    (
        select p.patient_id, 
            case
                when extract(YEAR from age(now(), p.date_of_birth)) between 15 and 17 then '15-17'::varchar(5)
                when extract(YEAR from age(now(), p.date_of_birth)) between 18 and 24 then '18-24'::varchar(5)
                when extract(YEAR from age(now(), p.date_of_birth)) between 25 and 34 then '25-34'::varchar(5)
                when extract(YEAR from age(now(), p.date_of_birth)) between 35 and 44 then '35-44'::varchar(5)
            end age_group,
            case
                when p.race is not null then p.race
                else 'NULL'
            end race,
            case
                when p.ethnicity is not null then p.ethnicity
                else 'NULL'
            end ethnicity,
            case
                when pp.primary_payer is not null then primary_payer
                else 'NULL'::varchar(4)
            end primary_payer,
            case
                when ct.census_tract_id is not null then to_char(ct.census_tract_id,'fm000000')::varchar(6)
                else 'NULL'::varchar(6)
            end census_tract,
            case
                when zip.zip is not null then zip.zip
                else 'NULL'::varchar(5)
            end zip,
            case
                when trim(s.smoking) = '1' then 'Never'::varchar(7) 
                when trim(s.smoking) = '2' then 'Passive'::varchar(7)
                when trim(s.smoking) = '3' then 'Former'::varchar(7)
                when trim(s.smoking) = '4' then 'Current'::varchar(7)
                else 'NULL'::varchar(7)
            end smoking,
            case
                when b.bmi_alt is not null then b.bmi_alt::varchar(13)
                else 'NULL'::varchar(13)
            end bmi_alt1,
            case
                when b.bmi_alt ilike 'obese%' then 'Obese'::varchar(13)
                when b.bmi_alt is not null then b.bmi_alt::varchar(13)
                else 'NULL'::varchar(13)
            end bmi_alt2,
            case
               when bp.mbp_syst < 120 and bp.mbp_dias<80 then 'Normal'::varchar(10)
               when bp.mbp_syst < 140 and bp.mbp_dias<90 then 'Borderline'::varchar(10)
               when bp.mbp_syst >= 140 or bp.mbp_dias>=90 then 'High'::varchar(10)
               else 'NULL'::varchar(10)
            end bp_grp1,
            case 
               when bp.mbp_syst < 120 and bp.mbp_dias<80 then 'Normal'::varchar(8)
               when bp.mbp_syst < 130 and bp.mbp_dias<80 then 'Elevated'::varchar(8)
               when bp.mbp_syst < 140 and bp.mbp_dias<90 then 'Stage 1'::varchar(8)
               when bp.mbp_syst >= 140 or bp.mbp_dias>=90 then 'Stage 2'::varchar(8)
               else 'NULL'::varchar(8)
            end bp_grp2,
            case 
                when a.max_a1c1 < 5.7 then '<5.7'::varchar(7)
                when a.max_a1c1 >= 5.7 and a.max_a1c1 < 6.5 then '5.7-6.4'::varchar(7)
                when a.max_a1c1 >= 6.5 and a.max_a1c1 <= 8 then '6.5-8'::varchar(7)
                when a.max_a1c1 > 8 then '>8'::varchar(7)
               else 'NULL'::varchar(7)
            end max_a1c,
            case
               when l.max_ldl1 < 100 then '<100'::varchar(7)
               when l.max_ldl1 >= 100 and l.max_ldl1 < 130 then '100-129'::varchar(7)
               when l.max_ldl1 >= 130 and l.max_ldl1 < 160 then '130-159'::varchar(7)
               when l.max_ldl1 >=160 then '>160'::varchar(7)
               else 'NULL'::varchar(7)
            end max_ldl,
            case
               when t.max_trig1 < 200 then '<200'::varchar(7)
               when t.max_trig1 >= 200 and t.max_trig1 < 500 then '200-499'::varchar(7)
               when t.max_trig1 >= 500 then '>500'::varchar(7)
               else 'NULL'::varchar(7)
            end max_trig,
            'Pregnant'::varchar(12) pregnancy
     from gen_pop_tools.tt_pat_alt p
          join (select distinct on (patient_id) patient_id, start_date, end_date
                from hef_timespan
                order by patient_id, start_date desc) pg
                on pg.patient_id=p.patient_id
          left join (select ct0.* from gen_pop_tools.patient_census_tract ct0
                       join gen_pop_tools.county_censustract cct on cct.censustract = to_char(ct0.census_tract_id,'fm000000')) ct
                       on ct.patient_id=p.patient_id
          left join (select z0.zip, p0.id as patient_id from gen_pop_tools.county_zip z0 join emr_patient p0 on p0.zip5=z0.zip) zip
                       on zip.patient_id=p.patient_id
          left join (select distinct on (tpp.patient_id) tpp.patient_id, year_month, cm.mapped_value  primary_payer
                     from gen_pop_tools.tt_primary_payer tpp
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on tpp.patient_id=pg0.patient_id and to_date(tpp.year_month,'yyyy_mm') <= pg0.start_date
		join gen_pop_tools.rs_conf_mapping cm on tpp.primary_payer=cm.code and cm.src_field='primary_payer' 
                     order by tpp.patient_id, year_month desc) pp 
              on pp.patient_id=p.patient_id 
          left join (select distinct on (ts.patient_id) ts.patient_id, year_month, smoking
                     from gen_pop_tools.tt_smoking ts
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on ts.patient_id=pg0.patient_id and to_date(ts.year_month,'yyyy_mm') <= pg0.start_date
                     order by ts.patient_id, year_month desc) s
              on s.patient_id=p.patient_id
          left join (select distinct on (tb.patient_id) tb.patient_id, year_month, bmi_alt
                     from gen_pop_tools.tt_bmi_alt tb
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on tb.patient_id=pg0.patient_id and to_date(tb.year_month,'yyyy_mm') <= pg0.start_date
                     order by tb.patient_id, year_month desc) b
              on b.patient_id=p.patient_id
          left join (select distinct on (tbp.patient_id) tbp.patient_id, year_month, tbp.mbp_syst, tbp.mbp_dias
                     from gen_pop_tools.tt_bp_alt tbp
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on tbp.patient_id=pg0.patient_id and to_date(tbp.year_month,'yyyy_mm') <= pg0.start_date
                     order by tbp.patient_id, year_month desc) bp
              on bp.patient_id=p.patient_id
          left join (select distinct on (ta.patient_id) ta.patient_id, year_month, ta.max_a1c1
                     from gen_pop_tools.tt_a1c1 ta
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on ta.patient_id=pg0.patient_id and to_date(ta.year_month,'yyyy_mm') <= pg0.start_date
                     order by ta.patient_id, year_month desc) a
              on a.patient_id=p.patient_id 
          left join (select distinct on (tl.patient_id) tl.patient_id, year_month, tl.max_ldl1
                     from gen_pop_tools.tt_ldl1 tl
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on tl.patient_id=pg0.patient_id and to_date(tl.year_month,'yyyy_mm') <= pg0.start_date
                     order by tl.patient_id, year_month desc) l
              on l.patient_id=p.patient_id 
          left join (select distinct on (tt.patient_id) tt.patient_id, year_month, tt.max_trig1
                     from gen_pop_tools.tt_trig1 tt
                     join (select distinct on (patient_id) patient_id, start_date, end_date
                          from hef_timespan 
                          order by patient_id, start_date desc) pg0 
                     on tt.patient_id=pg0.patient_id and to_date(tt.year_month,'yyyy_mm') <= pg0.start_date
                     order by tt.patient_id, year_month desc) t
              on t.patient_id=p.patient_id
     where p.gender='F' and extract(YEAR from age(now(), date_of_birth )) between 15 and 44
    ) t0
     group by grouping sets 
          ((age_group),
          (race),
          (ethnicity),
		  (primary_payer),
	(census_tract), (zip), (smoking), (bmi_alt1), (bmi_alt2),  (bp_grp1), (bp_grp2), (max_a1c), (max_ldl), (max_trig), (pregnancy))
