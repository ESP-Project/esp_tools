--2021 results
copy (select case
           when depression_counts <= 5 then null 
           else depression_counts 
       end as depression_counts,
       case
           when denom_counts <= 5 then null 
           else denom_counts 
       end as denom_counts,       
       age0_13, age12_17, age14_17, age_group, race, ethnicity, gender, primary_payer, zip, census
from 
    (
with t0 as (select aa patient_id, max(be) as depression, ae as zip, ct.censustract as census
            from gen_pop_tools.tt_out tto 
            join gen_pop_tools.county_zip cz on cz.zip=tto.ae 
            join gen_pop_tools.county_censustract ct on ct.censustract=to_char(tto.cb,'fm000000')  
            where to_date(af||ag,'yyyymm') between '2021-01-01'::date and '2021-12-31'::date --date set here
            group by aa, ae, ct.censustract),
     t4 as (select p.id as patient_id, t0.depression, --t1.bz primary_payer,
   	          case
                when t1.bz=0 then 'Other - Unknown - Self-Pay'
                when t1.bz=4 then 'Medicare'
                when t1.bz=2 then 'Auto - Worker''s Comp'
                when t1.bz=3 then 'Medicaid'
                when t1.bz=1 then 'Commercial - Blue Cross'
                else 'NULL payer'
              end primary_payer,
              case
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 0 and 13 then '0-13' --age date set here
                else 'Not 0-13'
              end age0_13,
              case
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 12 and 17 then '12-17' --and here
                else 'Not 12-17'
              end age12_17,
              case
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 14 and 17 then '14-17' --and here 
                else 'Not 14-17'
              end age14_17,
              case
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) < 18 then 'Under 18'::varchar(5) --added this,
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 18 and 29 then '18-29'::varchar(5) --and here, 
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 30 and 44 then '30-44'::varchar(5) --here, 
                when extract(YEAR from age('2021-12-31'::date, p.date_of_birth)) between 45 and 64 then '45-64'::varchar(5) --here
                else '65+'::varchar(5) -- this needs to include the under 18 folks or the count is messed up.
              end age_group,
              case
                when nullif(trim(race.mapped_value),'') is not null then race.mapped_value
                else 'NULL Race'
              end race,
              case
                when nullif(trim(ethn.mapped_value),'') is not null then ethn.mapped_value
                else 'NULL ethn'
              end ethnicity,
              case
                when nullif(trim(sex.mapped_value),'') is not null then sex.mapped_value
                else 'NULL Gender'::varchar(4)
              end gender,
              case when nullif(trim(t0.census),'') is not null then t0.census else 'NULL census' end census,
              case when nullif(trim(t0.zip),'') is not null then t0.zip else 'NULL zip' end zip
            from emr_patient p
            left join gen_pop_tools.rs_conf_mapping race on p.race=race.src_value and race.src_field='race'
            left join gen_pop_tools.rs_conf_mapping ethn on p.ethnicity=ethn.src_value and ethn.src_field='ethnicity'
            left join gen_pop_tools.rs_conf_mapping sex on p.gender=sex.src_value and sex.src_field='gender'
            join t0 on p.id=t0.patient_id
            join gen_pop_tools.tt_out t1 on t1.aa=t0.patient_id and t1.af='2021' --change this year too
                    and t1.ag='12' --this is just the last month of data in the sequence and provided the most recent p-payer.  Leave this as 12.
            where p.date_of_birth between '1910-01-01'::date and now()
            )
     select 
       sum(depression) as depression_counts,
       count(*) denom_counts,
       age0_13, age12_17, age14_17, age_group, race, ethnicity, gender, primary_payer, zip, census
     from t4
     group by grouping sets ((age_group), (race), (ethnicity), (gender),  
     (zip), (census), (age0_13), (age12_17), (age14_17), (primary_payer),
     (age_group, race), (age0_13, race), (age12_17, race), (age14_17,race),     
     (age_group, gender), (age0_13, gender), (age12_17, gender), (age14_17, gender),     
     (age_group, census), (age0_13, census), (age12_17, census), (age14_17,census),     
     (age_group, zip), (age0_13, zip), (age12_17, zip), (age14_17, zip),     
     (age_group, ethnicity), (age0_13, ethnicity), (age12_17, ethnicity), (age14_17, ethnicity),     
     (age_group, primary_payer), (age0_13, primary_payer), (age12_17, primary_payer), (age14_17, primary_payer),
     (age_group, race, gender), (age0_13, race, gender), (age12_17, race, gender), (age14_17,race,gender),     
     (age_group, race, ethnicity), (age0_13, race, ethnicity), (age12_17, race, ethnicity), (age14_17,race,ethnicity),     
     (age_group, gender, ethnicity), (age0_13, gender, ethnicity), (age12_17, gender, ethnicity), (age14_17,gender,ethnicity),     
     (race, gender), (race, ethnicity), (race, primary_payer), (race, gender, ethnicity), (race, census), (race, zip),  
     (gender, ethnicity), (gender, primary_payer), (gender, census), (gender, zip),
     (ethnicity, primary_payer), (ethnicity, census), (ethnicity, zip),
     (primary_payer, census), (primary_payer, zip) )
     ) t00 )to '/tmp/ahn-depression2021-final.csv' delimiter ',' header csv;
