--2022 results
copy (select case
           when all_lyme_dx_counts <= 5 then null 
           else all_lyme_dx_counts 
       end as all_lyme_dx_counts,
       case
           when treated_lyme_counts <= 5 then null 
           else treated_lyme_counts 
       end as treated_lyme_counts,
       case
           when under_care_counts <= 5 then null 
           else under_care_counts 
       end as under_care_counts,       
       age_group, race, gender
from 
    (
with lyme_dx as (select distinct enc.patient_id, enc.date
                 from emr_encounter_dx_codes dx
                 join emr_encounter enc on enc.id=dx.encounter_id
                 where enc.date between '2022-01-01'::date and '2022-12-31'::date --date set here
                   and dx.dx_code_id in ('icd10:A69.2','icd10:A69.20','icd10:A69.21','icd10:A69.22','icd10:A69.23','icd10:A69.29')), 
                   --need to get DX codes as our lyme algorithm leaves out A69.2 from lyme hef event dx:lyme
     lyme_rx as (select distinct rx.patient_id, rx.date
                 from emr_prescription rx 
                 join static_drugsynonym syn on rx.name ilike '%'||syn.other_name||'%' 
                   and lower(syn.generic_name) in ('doxycycline', 'amoxicillin', 'penicillin', 'ceftriaxone', 'azithromycin', 'cefuroxime axetil')
                 where rx.date between '2022-01-01'::date and '2022-12-31'::date), --date set here
     under_care as (select distinct patient_id from gen_pop_tools.clin_enc
                    where date between '2022-01-01'::date and '2022-12-31'::date), --date set here
     all_lyme as (select distinct on (under_care.patient_id) under_care.patient_id,
                        case when lyme_dx.patient_id is not null then 1
                             else 0 end all_lyme,
                        case when lyme_rx.patient_id is not null and lyme_dx.patient_id is not null  
                                and lyme_rx.date between (lyme_dx.date - interval '14 days') and (lyme_dx.date + interval '14 days')
                                then 1
                             else 0 end treated_lyme
                 from under_care
                 left join lyme_dx on lyme_dx.patient_id=under_care.patient_id
                 left join lyme_rx on lyme_rx.patient_id=under_care.patient_id
                 order by under_care.patient_id, 
                          case when lyme_dx.patient_id is not null then 1
                             else 0 end desc,
                          case when lyme_rx.patient_id is not null and lyme_dx.patient_id is not null  
                                and lyme_rx.date between (lyme_dx.date - interval '14 days') and (lyme_dx.date + interval '14 days')
                                then 1
                             else 0 end desc)
     select count(patient_id) as under_care_counts,
            sum(all_lyme) as all_lyme_dx_counts,
            sum(treated_lyme) as treated_lyme_counts,
            case
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) <4 then '0-4'::varchar(5) --change date here,
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 5 and 9 then '5-9'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 10 and 14 then '10-14'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 15 and 19 then '15-19'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 20 and 24 then '20-24'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 25 and 29 then '25-29'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 30 and 34 then '30-34'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 35 and 39 then '35-39'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 40 and 44 then '40-44'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 45 and 49 then '45-49'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 50 and 54 then '50-54'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 55 and 59 then '55-59'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 60 and 64 then '60-64'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 65 and 69 then '65-69'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 70 and 74 then '70-74'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 75 and 79 then '75-79'::varchar(5) --and here, 
                else '80+'::varchar(5)
            end age_group,
              case
                when nullif(trim(race.mapped_value),'') is not null then race.mapped_value
                else 'NULL Race'
              end race,
              case
                when nullif(trim(sex.mapped_value),'') is not null then sex.mapped_value
                else 'NULL Gender'::varchar(4)
              end gender
            from emr_patient p
            left join gen_pop_tools.rs_conf_mapping race on p.race=race.src_value and race.src_field='race'
            left join gen_pop_tools.rs_conf_mapping sex on p.gender=sex.src_value and sex.src_field='gender'
            join all_lyme t0 on p.id=t0.patient_id
            where p.date_of_birth between '1910-01-01'::date and now()
            group by cube (
            case
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) <4 then '0-4'::varchar(5) --change date here,
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 5 and 9 then '5-9'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 10 and 14 then '10-14'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 15 and 19 then '15-19'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 20 and 24 then '20-24'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 25 and 29 then '25-29'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 30 and 34 then '30-34'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 35 and 39 then '35-39'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 40 and 44 then '40-44'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 45 and 49 then '45-49'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 50 and 54 then '50-54'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 55 and 59 then '55-59'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 60 and 64 then '60-64'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 65 and 69 then '65-69'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 70 and 74 then '70-74'::varchar(5) --and here, 
                when extract(YEAR from age('2022-12-31'::date, p.date_of_birth)) between 75 and 79 then '75-79'::varchar(5) --and here, 
                else '80+'::varchar(5)
            end,
              case
                when nullif(trim(race.mapped_value),'') is not null then race.mapped_value
                else 'NULL Race'
              end,
              case
                when nullif(trim(sex.mapped_value),'') is not null then sex.mapped_value
                else 'NULL Gender'::varchar(4)
              end)
) t00 )to '/tmp/lyme2022.csv' delimiter ',' header csv;
