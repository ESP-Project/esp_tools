copy (select case
           when count(*) <= 5 then null 
           else count(*) 
       end as all_pats,       
       case 
           when sum(abtested) <= 5 then null 
           else sum(abtested) 
       end as abtested, 
       case 
           when sum(abpos) <= 5 then null 
           else sum(abpos) 
       end as abpos, 
       case 
           when sum(rnatested) <= 5 then null 
           else sum(rnatested) 
       end as rnatested, 
       case 
           when sum(rnapos) <= 5 then null 
           else sum(rnapos) 
       end as rnapos, 
       case 
           when sum(ab_and_rna_pos) <= 5 then null 
           else sum(ab_and_rna_pos) 
       end as ab_and_rna_pos, 
       case 
           when sum(diagnosed) <= 5 then null 
           else sum(diagnosed) 
       end as diagnosed, 
       case 
           when sum(hcacase) <= 5 then null 
           else sum(hcacase) 
       end as hcacase, 
       case 
           when sum(hcccase) <= 5 then null 
           else sum(hcccase) 
       end as hcccase, 
       case 
           when sum(abpos_dx_nocase) <= 5 then null 
           else sum(abpos_dx_nocase) 
       end as abpos_dx_nocase,        
       case when sum(treated) <= 5 then null
            else sum(treated)
       end as treated,
       age_group, race, gender, pregnancy
from 
    (
select p.id as patient_id, 
        case
                when extract(YEAR from age('2020-01-01'::date, p.date_of_birth)) between 0 and 2 then '0-2'::varchar(5)
                when extract(YEAR from age('2020-01-01'::date, p.date_of_birth)) between 3 and 17 then '3-17'::varchar(5)
                when extract(YEAR from age('2020-01-01'::date, p.date_of_birth)) between 18 and 49 then '18-49'::varchar(5)
                when extract(YEAR from age('2020-01-01'::date, p.date_of_birth)) between 50 and 64 then '50-64'::varchar(5)
                else '65+'::varchar(5)
            end age_group,
            case
                when p.race is not null then p.race
                else 'NULL Race'
            end race,
            case
                when upper(substr(p.gender,1,1)) in ('M','F') then upper(substr(p.gender,1,1))
    else 'NULL Gender'::varchar(4)
            end gender,
            case
                when pg.pregnant>0 and upper(substr(p.gender,1,1))='F' then 'pregnant'
                when upper(substr(p.gender,1,1))='F' then 'not pregnant'
                else 'NA'
            end pregnancy,
            case when hcabtest.tested > 0 then 1 else 0 end abtested,
            case when hcabpos.positive > 0 then 1 else 0 end abpos,
            case when hcrna.tested > 0 then 1 else 0 end rnatested,
            case when hcrnapos.positive > 0 then 1 else 0 end rnapos,
            case when hcrnapos.positive > 0 and hcabpos.positive > 0 then 1 else 0 end ab_and_rna_pos,
            case when hcdx.diagnosed > 0 then 1 else 0 end diagnosed,
            case when hcacase.patient_id is not null then 1 else 0 end hcacase,
case when hcccase.patient_id is not null then 1 else 0 end hcccase,
case when hcabpos.positive>0 and hcdx.diagnosed>0 and hcacase.patient_id is null and hcccase.patient_id is null then 1 else 0 end abpos_dx_nocase,
            case when hcrx.prescription > 0 then 1 else 0 end treated
     from emr_patient p
          join gen_pop_tools.rs_conf_mapping race on p.race=race.src_value and race.src_field='race'
          left join 
          (select patient_id, sum(coalesce(recent_pregnancy,0)) pregnant
           from gen_pop_tools.tt_preg1 where substr(year_month,1,4)='2018' group by patient_id) pg on p.id=pg.patient_id          
          left join 
          (select patient_id, count(*) tested
           from hef_event where to_char(date,'yyyy')='2018' 
                          and (name like 'lx:hepatitis_c_elisa%' or name like 'lx:hepatitis_c_signal_cutoff%' or name like 'lx:hepatitis_c_riba%')
           group by patient_id) hcabtest on p.id=hcabtest.patient_id
          left join 
          (select patient_id, count(*) positive
           from hef_event where to_char(date,'yyyy')='2018' 
                          and (name = 'lx:hepatitis_c_elisa:positive' or name = 'lx:hepatitis_c_signal_cutoff:positive' or name = 'lx:hepatitis_c_riba:positive')
           group by patient_id) hcabpos on p.id=hcabpos.patient_id
          left join 
          (select patient_id, count(*) tested
           from hef_event where to_char(date,'yyyy')='2018' and name ilike 'lx:hepatitis_c_rna%' 
           group by patient_id) hcrna on p.id=hcrna.patient_id
          left join 
          (select patient_id, count(*) positive
           from hef_event where to_char(date,'yyyy')='2018' and name = 'lx:hepatitis_c_rna:positive' 
           group by patient_id) hcrnapos on p.id=hcrnapos.patient_id
          left join
          (select patient_id, count(*) diagnosed 
           from hef_event where to_char(date,'yyyy')='2018' and name like 'dx:hepatitis_c%' 
   group by patient_id) hcdx on p.id=hcdx.patient_id
          left join (select count(*) prescription, patient_id from gen_pop_tools.cc_hepc_all_rx where to_char(date,'yyyy') = '2018' 
             group by patient_id) hcrx on p.id=hcrx.patient_id
          left join (select distinct patient_id from nodis_case c
  join nodis_caseactivehistory cah on  c.id=cah.case_id 
             where condition = 'hepatitis_c' and cah.status='HEP_C-A'
   and to_char(c.date,'yyyy') = '2018') hcacase on hcacase.patient_id = p.id
          left join (select distinct patient_id from nodis_case c
  join nodis_caseactivehistory cah on  c.id=cah.case_id 
             where condition = 'hepatitis_c' and cah.status='HEP_C-C'
   and to_char(c.date,'yyyy') = '2018') hcccase on hcccase.patient_id = p.id
          
     where date_of_birth < now() and exists (select null from gen_pop_tools.clin_enc ce where ce.patient_id=p.id and to_char(ce.date,'yyyy')='2018')
) t0
     group by grouping sets (age_group), (race), (gender), (pregnancy)) to '/tmp/hepc.csv' delimiter ',' header csv;
