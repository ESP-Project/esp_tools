select count(*), name, dose, refills, quantity as quantity_text, quantity_float, max(date) as most_recent_date
from emr_prescription 
where name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%'
group by name, dose, refills, quantity, quantity_float
order by name;


select count(*), name, dose, refills, quantity_float, directions
from emr_prescription 
where (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
and date >= '2024-01-01'
group by name, dose, refills, quantity_float, directions
order by name;


--IDENTIIFICATION STARTS HERE 

DROP TABLE IF EXISTS kre_report.doxy_inlcusion_meds;
CREATE TABLE kre_report.doxy_inlcusion_meds AS
SELECT *
FROM emr_prescription
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
--AND upper(directions) ~ ('SEX|CONDOM|INTERCOURSE|STD| STI |STIS|DOXYPEP|DOXY PEP|DOXY PREP');
--AND upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECTED SEX|AFTER SEX|POST SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|STD| STI |STIS|(STI |DOXYPEP|DOXY PEP|DOXY PREP');
AND upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECT.* SEX|UNPROTECT.* SX|AFTER SEX|POST SEX|RISK.* SEX|24H.* OF SEX|24 HOURS OF SEX|72H.* OF SEX|72 HOURS OF SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|SEX.* EXPOSURE|STD| STI |STIS|\(STI |DOXYPEP|DOXY PEP|DOXY PREP|DOXY-PEP|DOXY- PEP|DoxyPrEP|DOXY PREP|DOXY-PREP|DOXY- PREP|72h.* OF INTERCOURSE|72 HOURS OF INTERCOURSE');

--ABSOLUTE INCLUSION
select count(*), name, dose, quantity_float, refills, directions
from kre_report.doxy_inlcusion_meds
GROUP BY name, dose, quantity_float, refills, directions;

-- IDENTIFY ALL MEDS MINUS ABSOLUTE INCLUSION AND EXCLUSION
DROP TABLE IF EXISTS kre_report.doxy_all_minus_exclu_and_incl;
CREATE TABLE kre_report.doxy_all_minus_exclu_and_incl AS
SELECT * FROM 
emr_prescription
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
AND id not in (select id from kre_report.doxy_inlcusion_meds)
--AND upper(directions) !~ ('TICK|LYME|DAILY|PROCEDURE|DENT|DAYS|EVERY 12|EXPEDITED');
AND upper(directions) !~ ('TIC|LYME|PROCEDURE|DENT|BIRTH CONTROL|BIRTHCONTROL|RASH|BITE|LEPTOSPIROSIS');

-- FROM REMAINING IDENTIFY RELATIVE INCLUSUION MEDS
DROP TABLE IF EXISTS kre_report.doxy_relative_incl;
CREATE TABLE kre_report.doxy_relative_incl AS
SELECT * FROM 
kre_report.doxy_all_minus_exclu_and_incl
WHERE upper(directions) ~ ('INTERCOURSE|SEX')
AND upper(directions) !~ ('DAILY|DAYS|EVERY 12|7 DAYS|ONCE A WEEK|ONCE PER WEEK|WEEKLY');


-- FROM THE RELATIVE INCLUSION MEDS MUST MEET ALL REQUIREMENTS
DROP TABLE IF EXISTS kre_report.doxy_final_remaining;
CREATE TABLE kre_report.doxy_final_remaining AS
SELECT * FROM
kre_report.doxy_relative_incl
WHERE (name ilike '%100 MG%' or name ilike '%100MG%')
AND upper(directions) ~ ('ONCE|ONE DOSE|SINGLE DOSE|1 DOSE')
AND (dose in ('200', '200mg') OR upper(directions) ~ ('TAKE 2|TAKE TWO|200mg|200 MG|2 TAB|2 PILL|2 PO|TWO TAB|TWO PILL|TWO PO'));

-- see what is dropped
select * from kre_report.doxy_relative_incl
except
select * from kre_report.doxy_final_remaining;



--CHECK FOR LYME DX ON SAME DATE AS DOXY (FROM RELATIVE INCLUSION LIST)
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_dx;
CREATE TABLE kre_report.doxy_with_lyme_dx AS
SELECT DISTINCT T1.*
--SELECT T3.dx_code_id, T4.name as dx_name, T2.date enc_date, T1.date rx_date, T2.date - T1.date as date_diff
from kre_report.doxy_final_remaining T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE (dx_code_id ilike 'icd10:A69.2%' or dx_code_id ilike 'icd10:W57%' or dx_code_id in ('icd9:088.81', 'icd9:919.4'))
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days';
AND T2.date = T1.date;

--DROP RX ON SAME DATE AS LYME DX
DROP TABLE IF EXISTS kre_report.doxy_final_remaining_no_lyme_dx;
CREATE TABLE kre_report.doxy_final_remaining_no_lyme_dx AS
SELECT * FROM kre_report.doxy_final_remaining
EXCEPT
SELECT * FROM kre_report.doxy_with_lyme_dx;


--CHECK FOR LYME LAB ON SAME DATE AS DOXY (FROM REMAINDER)
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_lx;
CREATE TABLE kre_report.doxy_with_lyme_lx AS
SELECT DISTINCT T1.*
--SELECT T2.native_name, T2.date lab_date, T1.date rx_date, T2.date - T1.date as date_diff
FROM kre_report.doxy_final_remaining_no_lyme_dx T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days'
AND T2.date = T1.date;

--DROP RX ON SAME DATE AS LYME LAB
DROP TABLE IF EXISTS kre_report.doxy_final_remaining_no_lyme_dx_or_lab;
CREATE TABLE kre_report.doxy_final_remaining_no_lyme_dx_or_lab AS
SELECT * FROM kre_report.doxy_final_remaining_no_lyme_dx
EXCEPT
SELECT * FROM kre_report.doxy_with_lyme_lx;

--REMAINING MEDS
select count(*), name, dose, quantity_float, refills, directions
from kre_report.doxy_final_remaining_no_lyme_dx_or_lab
GROUP BY name, dose, quantity_float, refills, directions;


--BRING TOGETHER ABSOLUTE INCLUSION LIST AND REMAINING RELATIVE INCLUSIONS 
DROP TABLE IF EXISTS kre_report.doxy_all_combo;
CREATE TABLE kre_report.doxy_all_combo as
SELECT * FROM kre_report.doxy_inlcusion_meds
UNION
SELECT * FROM kre_report.doxy_final_remaining_no_lyme_dx_or_lab;

--EXCLUDED 
select count(*), name, dose, refills, quantity_float, directions
from emr_prescription 
where (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
and date >= '2024-01-01'
and id not in (select id from kre_report.doxy_all_combo)
group by name, dose, refills, quantity_float, directions
order by name;


--------- ENDS HERE--------------------------------------

----DX ON SAME DATE AS DOXY
DROP TABLE IF EXISTS kre_report.doxy_dx_same_as_rx;
CREATE TABLE kre_report.doxy_dx_same_as_rx AS
--SELECT T1.patient_id, T3.dx_code_id, T4.name as dx_name, T2.date enc_date, T1.date rx_date, T2.date - T1.date as date_diff
SELECT DISTINCT T1.patient_id, T3.dx_code_id, T4.name as dx_name, T1.date
--from kre_report.doxy_inlcusion_meds T1
FROM kre_report.doxy_final_remaining_no_lyme_dx_or_lab T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE T1.date = T2.date
AND dx_code_id != 'icd9:799.9';

-- FREQUENCY COUNTS
SELECT count(*), dx_code_id, dx_name
FROM kre_report.doxy_dx_same_as_rx
GROUP BY dx_code_id, dx_name
ORDER BY COUNT(*) desc;

--FREQUENCY COUNTS WITH PCT OF PATS & PCT OF RX
SELECT count(*) dx_count, dx_code_id, dx_name, round(((count(*)::real / pat_count::real) * 100)::numeric, 2) as pct_of_pats,
round(((count(*)::real / rx_count::real) * 100)::numeric, 2) pct_of_rx
FROM kre_report.doxy_dx_same_as_rx T1
LEFT JOIN (SELECT count(distinct(patient_id)) pat_count from kre_report.doxy_inlcusion_meds) T2 ON (1=1)
LEFT JOIN (SELECT count(*) rx_count from kre_report.doxy_inlcusion_meds) T3 ON (1=1)
GROUP BY dx_code_id, dx_name, pat_count, rx_count
ORDER BY COUNT(*) desc;


--REMAINDER FREQUENCY COUNTS WITH PCT OF PATS & PCT OF RX
SELECT count(*) dx_count, dx_code_id, dx_name, round(((count(*)::real / pat_count::real) * 100)::numeric, 2) as pct_of_pats,
round(((count(*)::real / rx_count::real) * 100)::numeric, 2) pct_of_rx
FROM kre_report.doxy_dx_same_as_rx T1
LEFT JOIN (SELECT count(distinct(patient_id)) pat_count from kre_report.doxy_final_remaining_no_lyme_dx_or_lab) T2 ON (1=1)
LEFT JOIN (SELECT count(*) rx_count from kre_report.doxy_final_remaining_no_lyme_dx_or_lab) T3 ON (1=1)
GROUP BY dx_code_id, dx_name, pat_count, rx_count
ORDER BY COUNT(*) desc;








select count(*), name, dose, quantity_float, refills, directions
from kre_report.doxy_with_lyme_dx
GROUP BY name, dose, quantity_float, refills, directions;

select count(T1.*) elgible_rx, count(T2.*) as rx_w_lyme_dx, ((count(T2.*)::real / count(T1.*)::real) * 100)::real as pct_with_lymedx
FROM kre_report.doxy_all_combo T1
LEFT JOIN kre_report.doxy_with_lyme_dx T2 ON (T1.id = T2.id);

DROP TABLE IF EXISTS kre_report.doxy_with_lyme_lx;
CREATE TABLE kre_report.doxy_with_lyme_lx AS
SELECT DISTINCT T1.*
--SELECT T2.native_name, T2.date lab_date, T1.date rx_date, T2.date - T1.date as date_diff
FROM kre_report.doxy_all_combo T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
AND T2.date >= T1.date - INTERVAL '7 days'
AND T2.date <= T1.date + INTERVAL '7 days';

select count(*), name, dose, quantity_float, refills, directions
from kre_report.doxy_with_lyme_lx
GROUP BY name, dose, quantity_float, refills, directions;

DROP TABLE IF EXISTS kre_report.doxy_all_dx_and_lx;
CREATE TABLE kre_report.doxy_all_dx_and_lx as
SELECT * FROM kre_report.doxy_with_lyme_dx
UNION
SELECT * FROM kre_report.doxy_with_lyme_lx;

select count(*), name, dose, quantity_float, refills, directions
from kre_report.doxy_all_dx_and_lx
GROUP BY name, dose, quantity_float, refills, directions;

select count(T1.*) elgible_rx, count(T2.*) as rx_w_lyme_dx_or_lx, ((count(T2.*)::real / count(T1.*)::real) * 100)::real as pct_with_lyme_dx_or_lx
FROM kre_report.doxy_all_combo T1
LEFT JOIN kre_report.doxy_all_dx_and_lx T2 ON (T1.id = T2.id);

sex 
intercourse
risky

