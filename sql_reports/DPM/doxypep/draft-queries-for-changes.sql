--patient with doxypep > 30 and < 180 days apart

select * from 
(	   
select patient_id, min(date), max(date), max(date) - min(date) as date_diff
from kre_report.doxy_all_combo   
where patient_id in (
select patient_id
from kre_report.doxy_all_combo
group by patient_id 
having count(*) > 1)
group by patient_id) foo
where date_diff > 30 and date_diff < 180
order by patient_id;



--draft work for chlamydia, gon, syph AFTER most recent doxy rx (within 183 days)

SELECT DISTINCT rx_mon_yr, T1.patient_id, condition
--, T1.date as rx_date, T2.date, T2.date - T1.date as date_diff
FROM kre_report.doxy_all_combo T1
JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
WHERE condition = 'gonorrhea'
AND T2.date > T1.date
AND T2.date - T1.date <= 183
AND T1.patient_id in (243296,340906)
ORDER BY patient_id




drop table if exists kre_table_temp_3;
create table kre_table_temp_3 as
SELECT patient_id, rx_mon_yr, date, lag(date) OVER (PARTITION BY patient_id ORDER BY date) as rx_before, lead(date) OVER (PARTITION BY patient_id ORDER BY date) as rx_after, 
(lag(date) OVER (PARTITION BY patient_id)) - (lead(date) OVER (PARTITION BY patient_id)) as date_diff
              FROM kre_report.doxy_all_combo 
			  GROUP BY patient_id, date, rx_mon_yr
              ORDER BY patient_id, date
			  


drop table if exists kre_temp_4;
create table kre_temp_4 as
select *, sum(date_diff) OVER (PARTITION BY patient_id ROWS UNBOUNDED PRECEDING) running_total
from kre_table_temp_3



drop table if exists kre_temp_5;
create table kre_temp_5 as
select * from kre_temp_4
where ( ((date_diff > 30  and date_diff < 183) or date_diff is null) and running_total <= 183)




-- patients who have more than 1 rx 

SELECT distinct(patient_id)
FROM (
	select T1.patient_id, T1.rx_mon_yr
	FROM kre_report.doxy_all_combo T1
	JOIN (
		select patient_id, min(rx_mon_yr) first_rx_mon_yr
		from kre_report.doxy_all_combo
		where patient_id in (select patient_id
			from kre_report.doxy_all_combo
			WHERE rx_mon_yr ilike '2023%' or (rx_mon_yr ilike '2024%' and substring(rx_mon_yr, 6,7)::int <= 8)
			group by patient_id
			having count(*) > 1)
		AND (rx_mon_yr ilike '2023%' or (rx_mon_yr ilike '2024%' and substring(rx_mon_yr, 6,7)::int <= 8))
		GROUP BY patient_id
		ORDER BY patient_id) T2 ON (T1.patient_id = T2.patient_id)
	WHERE T1.rx_mon_yr != T2.first_rx_mon_yr
	AND (rx_mon_yr ilike '2023%' or (rx_mon_yr ilike '2024%' and substring(rx_mon_yr, 6,7)::int <= 8))
	ORDER BY T1.patient_id) T1