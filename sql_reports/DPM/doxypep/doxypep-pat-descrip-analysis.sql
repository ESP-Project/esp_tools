
--CLINICAL ENCOUNTER OF TYPE ENC IN LAST 2 YEARS
DROP TABLE IF EXISTS kre_report.doxy2_clin_enc_pats;
CREATE TABLE kre_report.doxy2_clin_enc_pats AS
SELECT distinct(patient_id)
FROM gen_pop_tools.clin_enc 
WHERE date >= now()::date - INTERVAL '2 years'
AND source = 'enc';

-- Z CODE OF INTEREST IN THE LAST 2 YEARS
DROP TABLE IF EXISTS kre_report.doxy2_dx_details;
CREATE TABLE kre_report.doxy2_dx_details AS
SELECT DISTINCT T1.patient_id, dx_code_id, T2.date dx_date
FROM kre_report.doxy2_clin_enc_pats T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 ON (T3.encounter_id = T2.id)
WHERE T2.date >= now()::date - INTERVAL '2 years'
AND (T3.dx_code_id in ('icd10:Z20.2', 'icd10:Z20.6') OR T3.dx_code_id ilike 'icd10:Z72.5%');

-- HIV RISK SCORE IN LAST 2 YEARS ABOVE THRESHOLDS
DROP TABLE IF EXISTS kre_report.doxy2_hiv_risk_scores;
CREATE TABLE kre_report.doxy2_hiv_risk_scores AS
SELECT DISTINCT T1.patient_id, rpt_year, hiv_risk_score
FROM kre_report.doxy2_clin_enc_pats T1
JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
WHERE T2.rpt_year::integer >= extract(year from now()::date) - 2
AND hiv_risk_score > '0.001'
ORDER BY T1.patient_id;

-- STI (CHLAM, GON, SYPHILIS) IN THE LAST YEAR
DROP TABLE IF EXISTS kre_report.doxy2_sti_ly;
CREATE TABLE kre_report.doxy2_sti_ly AS
SELECT DISTINCT T1.patient_id, condition
FROM kre_report.doxy2_clin_enc_pats T1
JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id)
WHERE condition in ('syphilis', 'chlamydia', 'gonorrhea')
AND T2.date >= now()::date - INTERVAL '1 year';

--DOXY RX LAST 2 YEARS FOR PATS WITH CLIN ENC IN LAST 2 YEARS
DROP TABLE IF EXISTS kre_report.doxy2_all_meds;
CREATE TABLE kre_report.doxy2_all_meds AS
SELECT T2.*
FROM kre_report.doxy2_clin_enc_pats T1
JOIN emr_prescription T2 ON (T1.patient_id = T2.patient_id)
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
and T2.date >= now()::date - INTERVAL '2 years';

-- ABSOULUTE INCLUSION
DROP TABLE IF EXISTS kre_report.doxy2_abs_inclusion_meds;
CREATE TABLE kre_report.doxy2_abs_inclusion_meds AS
SELECT *
FROM kre_report.doxy2_all_meds
WHERE upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECT.* SEX|UNPROTECT.* SX|AFTER SEX|POST SEX|RISK.* SEX|24H.* OF SEX|24 HOURS OF SEX|72H.* OF SEX|72 HOURS OF SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|SEX.* EXPOSURE|STD| STI |STIS|\(STI |DOXYPEP|DOXY PEP|DOXY PREP|DOXY-PEP|DOXY- PEP|DoxyPrEP|DOXY PREP|DOXY-PREP|DOXY- PREP|72h.* OF INTERCOURSE|72 HOURS OF INTERCOURSE');

-- IDENTIFY ALL MEDS MINUS ABSOLUTE INCLUSION AND EXCLUSION
DROP TABLE IF EXISTS kre_report.doxy2_all_minus_exclu_and_incl;
CREATE TABLE kre_report.doxy2_all_minus_exclu_and_incl AS
SELECT * FROM 
kre_report.doxy2_all_meds
WHERE id not in (select id from kre_report.doxy2_abs_inclusion_meds)
AND upper(directions) !~ ('TIC|LYME|PROCEDURE|DENT|BIRTH CONTROL|BIRTHCONTROL|RASH|BITE|LEPTOSPIROSIS');

-- RELATIVE INCLUSUION MEDS
DROP TABLE IF EXISTS kre_report.doxy2_relative_incl;
CREATE TABLE kre_report.doxy2_relative_incl AS
SELECT * FROM 
kre_report.doxy2_all_minus_exclu_and_incl
WHERE upper(directions) ~ ('INTERCOURSE|SEX')
AND upper(directions) !~ ('DAILY|DAYS|EVERY 12|7 DAYS|ONCE A WEEK|ONCE PER WEEK|WEEKLY');

-- ABSOULTE AND RELATIVE INCLUSION MEDS TOGETHER
DROP TABLE IF EXISTS kre_report.doxy2_inclu_all;
CREATE TABLE kre_report.doxy2_inclu_all AS
SELECT * FROM kre_report.doxy2_abs_inclusion_meds
UNION 
SELECT * FROM kre_report.doxy2_relative_incl;

-- IDENTIFY ALL MEDS MINUS ABS INCLUSION AND EXCLUSION AND RELATIVE INCLUSION
DROP TABLE IF EXISTS kre_report.doxy2_all_minus_exclu_and_incl_and_relinc;
CREATE TABLE kre_report.doxy2_all_minus_exclu_and_incl_and_relinc AS
SELECT * FROM 
kre_report.doxy2_all_minus_exclu_and_incl
WHERE id not in (select id from kre_report.doxy2_relative_incl);

-- REMAINDER MEDS: MUST MEET ALL REQUIREMENTS
DROP TABLE IF EXISTS kre_report.doxy2_remainder_meds;
CREATE TABLE kre_report.doxy2_remainder_meds AS
SELECT * FROM
kre_report.doxy2_all_minus_exclu_and_incl_and_relinc
WHERE (name ilike '%100 MG%' or name ilike '%100MG%')
AND upper(directions) ~ ('ONCE|ONE DOSE|SINGLE DOSE|1 DOSE')
AND (dose in ('200', '200mg') OR upper(directions) ~ ('TAKE 2|TAKE TWO|200mg|200 MG|2 TAB|2 PILL|2 PO|TWO TAB|TWO PILL|TWO PO'));

-- CHECK FOR LYME DX ON SAME DATE AS DOXY (FROM REMAINDER MEDS)
DROP TABLE IF EXISTS kre_report.doxy2_with_lyme_dx;
CREATE TABLE kre_report.doxy2_with_lyme_dx AS
SELECT DISTINCT T1.*
FROM kre_report.doxy2_remainder_meds T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE (dx_code_id ilike 'icd10:A69.2%' or dx_code_id ilike 'icd10:W57%' or dx_code_id in ('icd9:088.81', 'icd9:919.4'))
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME DX
DROP TABLE IF EXISTS kre_report.doxy2_remainder_no_lyme_dx;
CREATE TABLE kre_report.doxy2_remainder_no_lyme_dx AS
SELECT * FROM kre_report.doxy2_remainder_meds
EXCEPT
SELECT * FROM kre_report.doxy2_with_lyme_dx;

-- CHECK FOR LYME LAB ON SAME DATE AS DOXY (FROM REMAINDER WITH NO LYME DX)
DROP TABLE IF EXISTS kre_report.doxy2_with_lyme_lx;
CREATE TABLE kre_report.doxy2_with_lyme_lx AS
SELECT DISTINCT T1.*
FROM kre_report.doxy2_remainder_no_lyme_dx T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME LAB
-- REMAINDER MEDS FINAL LIST
DROP TABLE IF EXISTS kre_report.doxy2_remainder_meds_final;
CREATE TABLE kre_report.doxy2_remainder_meds_final AS
SELECT * FROM kre_report.doxy2_remainder_no_lyme_dx
EXCEPT
SELECT * FROM kre_report.doxy2_with_lyme_lx;

--BRING TOGETHER INCLUSION LISTS AND REMAINDER MEDS
DROP TABLE IF EXISTS kre_report.doxy2_all_combo;
CREATE TABLE kre_report.doxy2_all_combo as
SELECT * FROM kre_report.doxy2_inclu_all
UNION
SELECT * FROM kre_report.doxy2_remainder_meds_final
ORDER BY date;

-- GENERATE COUNTS FOR THE REPORT
DROP TABLE IF EXISTS kre_report.doxy2_rpt_output;
CREATE TABLE kre_report.doxy2_rpt_output AS
SELECT COUNT(DISTINCT(T1.patient_id)) AS clin_enc_pats,
COUNT(DISTINCT(CASE WHEN T1.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T1.patient_id END)) as clin_enc_pats_doxypep,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.2' then T2.patient_id END)) as sti_contact_dx_pats_l2yr,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.2' and T2.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T2.patient_id END)) as sti_contact_dx_pats_l2yr_doxypep,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.6' then T2.patient_id END)) as hiv_contact_dx_pats_l2yr,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.6' and T2.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T2.patient_id END)) as hiv_contact_dx_pats_l2yr_doxypep,
COUNT(DISTINCT(CASE WHEN dx_code_id ilike 'icd10:Z72.5%' then T2.patient_id END)) as high_risk_sex_dx_pats_l2yr,
COUNT(DISTINCT(CASE WHEN dx_code_id ilike 'icd10:Z72.5%' and T2.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T2.patient_id END)) as high_risk_sex_dx_pats_l2yr_doxypep,
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.001' then T3.patient_id END)) as hiv_risk_gt_001_pats,
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.001' and T3.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T3.patient_id END)) as hiv_risk_gt_001_pats_doxypep,
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.01' then T3.patient_id END)) as hiv_risk_gt_01_pats,
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.01' and T3.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T3.patient_id END)) as hiv_risk_gt_01_pats_doxypep,
COUNT(DISTINCT(T4.patient_id)) as sti_l1yr_pats,
COUNT(DISTINCT(CASE WHEN T4.patient_id in (select patient_id FROM kre_report.doxy2_all_combo) then T4.patient_id END)) as sti_l1yr_pats_doxypep
FROM kre_report.doxy2_clin_enc_pats T1
LEFT JOIN kre_report.doxy2_dx_details T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN kre_report.doxy2_hiv_risk_scores T3 ON (T1.patient_id = T3.patient_id)
LEFT JOIN kre_report.doxy2_sti_ly T4 ON (T1.patient_id = T4.patient_id);


DROP TABLE IF EXISTS kre_report.doxy2_rpt_output_formatted;
CREATE TABLE kre_report.doxy2_rpt_output_formatted AS
SELECT now()::date as report_run_date, 
clin_enc_pats as clin_enc_pats_l2yr,
clin_enc_pats_doxypep as clin_enc_pats_with_doxy,
sti_contact_dx_pats_l2yr,
sti_contact_dx_pats_l2yr_doxypep,
round((sti_contact_dx_pats_l2yr_doxypep::numeric/sti_contact_dx_pats_l2yr::numeric) * 100, 1) sti_contact_dx_w_doxy_pct,
hiv_contact_dx_pats_l2yr,
hiv_contact_dx_pats_l2yr_doxypep,
round((hiv_contact_dx_pats_l2yr_doxypep::numeric/hiv_contact_dx_pats_l2yr::numeric) * 100, 1) hiv_contact_dx_w_doxy_pct,
high_risk_sex_dx_pats_l2yr,
high_risk_sex_dx_pats_l2yr_doxypep,
round((high_risk_sex_dx_pats_l2yr_doxypep::numeric/high_risk_sex_dx_pats_l2yr::numeric) * 100, 1) high_risk_sex_dx_w_doxy_pct,
hiv_risk_gt_001_pats,
hiv_risk_gt_001_pats_doxypep,
round((hiv_risk_gt_001_pats_doxypep::numeric/hiv_risk_gt_001_pats::numeric) * 100, 1) hiv_risk_gt_001_w_doxy_pct,
hiv_risk_gt_01_pats,
hiv_risk_gt_01_pats_doxypep,
round((hiv_risk_gt_01_pats_doxypep::numeric/hiv_risk_gt_01_pats::numeric) * 100, 1) hiv_risk_gt_01_w_doxy_pct,
sti_l1yr_pats,
sti_l1yr_pats_doxypep,
round((sti_l1yr_pats_doxypep::numeric/sti_l1yr_pats::numeric) * 100, 1) sti_l1yr_w_doxy_pct
FROM kre_report.doxy2_rpt_output;

-- nohup psql -d esp -f doxypep-pat-descrip-analysis.sql &


--\COPY (select * from kre_report.doxy2_rpt_output_formatted) TO '/tmp/2024-10-15-CHA-doxypep-denom-analysis-L2Y.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n 2024-10-15-CHA-doxypep-denom-analysis-L2Y.csv -s "CHA DoxyPEP2"

--\COPY (select * from kre_report.doxy2_rpt_output_formatted) TO '/tmp/2024-10-15-FWY-doxypep-denom-analysis-L2Y.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n 2024-10-15-FWY-doxypep-denom-analysis-L2Y.csv -s "FWY DoxyPEP2"

--\COPY (select * from kre_report.doxy2_rpt_output_formatted) TO '/tmp/2024-10-15-GLFHC-doxypep-denom-analysis-L2Y.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f noreply@glfhc.org -r keberhardt@commoninf.com -p /tmp -n 2024-10-15-GLFHC-doxypep-denom-analysis-L2Y.csv -s "GLFHC DoxyPEP2"

--\COPY (select * from kre_report.doxy2_rpt_output_formatted) TO '/tmp/2024-10-15-ATR-doxypep-denom-analysis-L2Y.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n 2024-10-15-ATR-doxypep-denom-analysis-L2Y.csv -s "ATR DoxyPEP2"