DROP TABLE IF EXISTS advp.doxy_all_meds;
CREATE TABLE advp.doxy_all_meds AS
SELECT id, patient_id, date, name, directions, dose, to_char(date, 'YYYY_MM') as rx_mon_yr
FROM emr_prescription 
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%');

-- ABSOULUTE INCLUSION
DROP TABLE IF EXISTS advp.doxy_abs_inclusion_meds;
CREATE TABLE advp.doxy_abs_inclusion_meds AS
SELECT *
FROM advp.doxy_all_meds
WHERE upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECT.* SEX|UNPROTECT.* SX|AFTER SEX|POST SEX|RISK.* SEX|24H.* OF SEX|24 HOURS OF SEX|72H.* OF SEX|72 HOURS OF SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|SEX.* EXPOSURE|STD| STI |STIS|\(STI |DOXYPEP|DOXY PEP|DOXY PREP|DOXY-PEP|DOXY- PEP|DoxyPrEP|DOXY PREP|DOXY-PREP|DOXY- PREP|72h.* OF INTERCOURSE|72 HOURS OF INTERCOURSE');

-- IDENTIFY ALL MEDS MINUS ABSOLUTE INCLUSION AND EXCLUSION
DROP TABLE IF EXISTS advp.doxy_all_minus_exclu_and_incl;
CREATE TABLE advp.doxy_all_minus_exclu_and_incl AS
SELECT * FROM 
advp.doxy_all_meds
WHERE id not in (select id from advp.doxy_abs_inclusion_meds)
AND upper(directions) !~ ('TIC|LYME|PROCEDURE|DENT|BIRTH CONTROL|BIRTHCONTROL|RASH|BITE|LEPTOSPIROSIS');

-- RELATIVE INCLUSUION MEDS
DROP TABLE IF EXISTS advp.doxy_relative_incl;
CREATE TABLE advp.doxy_relative_incl AS
SELECT * FROM 
advp.doxy_all_minus_exclu_and_incl
WHERE upper(directions) ~ ('INTERCOURSE|SEX')
AND upper(directions) !~ ('DAILY|DAYS|EVERY 12|7 DAYS|ONCE A WEEK|ONCE PER WEEK|WEEKLY');

-- ABSOULTE AND RELATIVE INCLUSION MEDS TOGETHER
DROP TABLE IF EXISTS advp.doxy_inclu_all;
CREATE TABLE advp.doxy_inclu_all AS
SELECT * FROM advp.doxy_abs_inclusion_meds
UNION 
SELECT * FROM advp.doxy_relative_incl;

-- IDENTIFY ALL MEDS MINUS ABS INCLUSION AND EXCLUSION AND RELATIVE INCLUSION
DROP TABLE IF EXISTS advp.doxy_all_minus_exclu_and_incl_and_relinc;
CREATE TABLE advp.doxy_all_minus_exclu_and_incl_and_relinc AS
SELECT * FROM 
advp.doxy_all_minus_exclu_and_incl
WHERE id not in (select id from advp.doxy_relative_incl);

-- REMAINDER MEDS: MUST MEET ALL REQUIREMENTS
DROP TABLE IF EXISTS advp.doxy_remainder_meds;
CREATE TABLE advp.doxy_remainder_meds AS
SELECT * FROM
advp.doxy_all_minus_exclu_and_incl_and_relinc
WHERE (name ilike '%100 MG%' or name ilike '%100MG%')
AND upper(directions) ~ ('ONCE|ONE DOSE|SINGLE DOSE|1 DOSE')
AND (dose in ('200', '200mg') OR upper(directions) ~ ('TAKE 2|TAKE TWO|200mg|200 MG|2 TAB|2 PILL|2 PO|TWO TAB|TWO PILL|TWO PO'));

-- CHECK FOR LYME DX ON SAME DATE AS DOXY (FROM REMAINDER MEDS)
DROP TABLE IF EXISTS advp.doxy_with_lyme_dx;
CREATE TABLE advp.doxy_with_lyme_dx AS
SELECT DISTINCT T1.*
FROM advp.doxy_remainder_meds T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE (dx_code_id ilike 'icd10:A69.2%' or dx_code_id ilike 'icd10:W57%' or dx_code_id in ('icd9:088.81', 'icd9:919.4'))
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days';
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME DX
DROP TABLE IF EXISTS advp.doxy_remainder_no_lyme_dx;
CREATE TABLE advp.doxy_remainder_no_lyme_dx AS
SELECT * FROM advp.doxy_remainder_meds
EXCEPT
SELECT * FROM advp.doxy_with_lyme_dx;

-- CHECK FOR LYME LAB ON SAME DATE AS DOXY (FROM REMAINDER WITH NO LYME DX)
DROP TABLE IF EXISTS advp.doxy_with_lyme_lx;
CREATE TABLE advp.doxy_with_lyme_lx AS
SELECT DISTINCT T1.*
FROM advp.doxy_remainder_no_lyme_dx T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days'
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME LAB
-- REMAINDER MEDS FINAL LIST
DROP TABLE IF EXISTS advp.doxy_remainder_meds_final;
CREATE TABLE advp.doxy_remainder_meds_final AS
SELECT * FROM advp.doxy_remainder_no_lyme_dx
EXCEPT
SELECT * FROM advp.doxy_with_lyme_lx;

--BRING TOGETHER INCLUSION LISTS AND REMAINDER MEDS
DROP TABLE IF EXISTS advp.doxy_all_combo;
CREATE TABLE advp.doxy_all_combo as
SELECT * FROM advp.doxy_inclu_all
UNION
SELECT * FROM advp.doxy_remainder_meds_final
ORDER BY date;

-- COUNT ONLY ONE RX PER MONTH
DROP TABLE IF EXISTS advp.doxy_pep_rx;
CREATE TABLE advp.doxy_pep_rx as
SELECT patient_id, to_char(date, 'YYYY_MM') as year_month, 1 as doxy_pep_rx
FROM advp.doxy_all_combo
GROUP BY patient_id, year_month;