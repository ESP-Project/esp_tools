
DROP TABLE IF EXISTS kre_report.doxy_all_meds;
CREATE TABLE kre_report.doxy_all_meds AS
SELECT *, to_char(date, 'YYYY-MM') as rx_mon_yr
FROM emr_prescription 
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%')
and date >= '2018-01-01';

-- ABSOULUTE INCLUSION
DROP TABLE IF EXISTS kre_report.doxy_abs_inclusion_meds;
CREATE TABLE kre_report.doxy_abs_inclusion_meds AS
SELECT *
FROM kre_report.doxy_all_meds
WHERE upper(directions) ~ ('CONDOMLESS|UNPROTECTED INTERCOURSE|UNPROTECT.* SEX|UNPROTECT.* SX|AFTER SEX|POST SEX|RISK.* SEX|24H.* OF SEX|24 HOURS OF SEX|72H.* OF SEX|72 HOURS OF SEX|HIGH RISK SEX|HIGH RISK INTERCOURSE|SEX.* EXPOSURE|STD| STI |STIS|\(STI |DOXYPEP|DOXY PEP|DOXY PREP|DOXY-PEP|DOXY- PEP|DoxyPrEP|DOXY PREP|DOXY-PREP|DOXY- PREP|72h.* OF INTERCOURSE|72 HOURS OF INTERCOURSE');

-- IDENTIFY ALL MEDS MINUS ABSOLUTE INCLUSION AND EXCLUSION
DROP TABLE IF EXISTS kre_report.doxy_all_minus_exclu_and_incl;
CREATE TABLE kre_report.doxy_all_minus_exclu_and_incl AS
SELECT * FROM 
kre_report.doxy_all_meds
WHERE id not in (select id from kre_report.doxy_abs_inclusion_meds)
AND upper(directions) !~ ('TIC|LYME|PROCEDURE|DENT|BIRTH CONTROL|BIRTHCONTROL|RASH|BITE|LEPTOSPIROSIS');

-- RELATIVE INCLUSUION MEDS
DROP TABLE IF EXISTS kre_report.doxy_relative_incl;
CREATE TABLE kre_report.doxy_relative_incl AS
SELECT * FROM 
kre_report.doxy_all_minus_exclu_and_incl
WHERE upper(directions) ~ ('INTERCOURSE|SEX')
AND upper(directions) !~ ('DAILY|DAYS|EVERY 12|7 DAYS|ONCE A WEEK|ONCE PER WEEK|WEEKLY');

-- ABSOULTE AND RELATIVE INCLUSION MEDS TOGETHER
DROP TABLE IF EXISTS kre_report.doxy_inclu_all;
CREATE TABLE kre_report.doxy_inclu_all AS
SELECT * FROM kre_report.doxy_abs_inclusion_meds
UNION 
SELECT * FROM kre_report.doxy_relative_incl;

-- IDENTIFY ALL MEDS MINUS ABS INCLUSION AND EXCLUSION AND RELATIVE INCLUSION
DROP TABLE IF EXISTS kre_report.doxy_all_minus_exclu_and_incl_and_relinc;
CREATE TABLE kre_report.doxy_all_minus_exclu_and_incl_and_relinc AS
SELECT * FROM 
kre_report.doxy_all_minus_exclu_and_incl
WHERE id not in (select id from kre_report.doxy_relative_incl);

-- REMAINDER MEDS: MUST MEET ALL REQUIREMENTS
DROP TABLE IF EXISTS kre_report.doxy_remainder_meds;
CREATE TABLE kre_report.doxy_remainder_meds AS
SELECT * FROM
kre_report.doxy_all_minus_exclu_and_incl_and_relinc
WHERE (name ilike '%100 MG%' or name ilike '%100MG%')
AND upper(directions) ~ ('ONCE|ONE DOSE|SINGLE DOSE|1 DOSE')
AND (dose in ('200', '200mg') OR upper(directions) ~ ('TAKE 2|TAKE TWO|200mg|200 MG|2 TAB|2 PILL|2 PO|TWO TAB|TWO PILL|TWO PO'));

-- CHECK FOR LYME DX ON SAME DATE AS DOXY (FROM REMAINDER MEDS)
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_dx;
CREATE TABLE kre_report.doxy_with_lyme_dx AS
SELECT DISTINCT T1.*
FROM kre_report.doxy_remainder_meds T1
LEFT JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN emr_encounter_dx_codes T3 On (T2.id = T3.encounter_id)
LEFT JOIN static_dx_code T4 ON (T3.dx_code_id = T4.combotypecode)
WHERE (dx_code_id ilike 'icd10:A69.2%' or dx_code_id ilike 'icd10:W57%' or dx_code_id in ('icd9:088.81', 'icd9:919.4'))
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days';
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME DX
DROP TABLE IF EXISTS kre_report.doxy_remainder_no_lyme_dx;
CREATE TABLE kre_report.doxy_remainder_no_lyme_dx AS
SELECT * FROM kre_report.doxy_remainder_meds
EXCEPT
SELECT * FROM kre_report.doxy_with_lyme_dx;

-- CHECK FOR LYME LAB ON SAME DATE AS DOXY (FROM REMAINDER WITH NO LYME DX)
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_lx;
CREATE TABLE kre_report.doxy_with_lyme_lx AS
SELECT DISTINCT T1.*
FROM kre_report.doxy_remainder_no_lyme_dx T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
--AND T2.date >= T1.date - INTERVAL '7 days'
--AND T2.date <= T1.date + INTERVAL '7 days'
AND T2.date = T1.date;

-- DROP RX ON SAME DATE AS LYME LAB
-- REMAINDER MEDS FINAL LIST
DROP TABLE IF EXISTS kre_report.doxy_remainder_meds_final;
CREATE TABLE kre_report.doxy_remainder_meds_final AS
SELECT * FROM kre_report.doxy_remainder_no_lyme_dx
EXCEPT
SELECT * FROM kre_report.doxy_with_lyme_lx;

--BRING TOGETHER INCLUSION LISTS AND REMAINDER MEDS
DROP TABLE IF EXISTS kre_report.doxy_all_combo;
CREATE TABLE kre_report.doxy_all_combo as
SELECT * FROM kre_report.doxy_inclu_all
UNION
SELECT * FROM kre_report.doxy_remainder_meds_final
ORDER BY date;


-- GENERATE COUNTS FOR THE REPORT
DROP TABLE IF EXISTS kre_report.doxy_rx_counts_step1;
CREATE TABLE kre_report.doxy_rx_counts_step1 AS
SELECT T1.rx_mon_yr, 
count(distinct(T1.patient_id)) as doxy_pats_any,
count(distinct(T2.patient_id)) as doxy_pats_rx_inclusion,
count(distinct(T3.patient_id)) as doxy_pats_rx_remainder
--count(distinct(T4.patient_id)) as doxy_pats_rx_all
FROM kre_report.doxy_all_meds T1
LEFT JOIN kre_report.doxy_inclu_all T2 ON (T1.rx_mon_yr = T2.rx_mon_yr)
LEFT JOIN kre_report.doxy_remainder_meds_final T3 ON (T1.rx_mon_yr = T3.rx_mon_yr)
--LEFT JOIN kre_report.doxy_all_combo T4 ON (T1.rx_mon_yr = T4.rx_mon_yr)
GROUP BY T1.rx_mon_yr;

--NOT FAST AS SINGLE QUERY SO COMBINING
DROP TABLE IF EXISTS kre_report.doxy_rx_counts;
CREATE TABLE kre_report.doxy_rx_counts AS
SELECT T1.*,
count(distinct(T4.patient_id)) as doxy_pats_rx_all
FROM kre_report.doxy_rx_counts_step1 T1
LEFT JOIN kre_report.doxy_all_combo T4 ON (T1.rx_mon_yr = T4.rx_mon_yr)
GROUP BY T1.rx_mon_yr, doxy_pats_any, doxy_pats_rx_inclusion, doxy_pats_rx_remainder;

-- PATS WITH CLINICAL ENCOUNTER OF TYPE ENC
-- THIS IS NOW WORKING (PATIENTS WITH 2 RX IN SAME MONTH 87426, 2773001)
DROP TABLE IF EXISTS kre_report.doxy_clin_enc;
CREATE TABLE kre_report.doxy_clin_enc AS
SELECT rx_mon_yr, 
sum(clin_enc_183) as clin_enc_pats_183,
sum(clin_enc_365) as clin_enc_pats_365
FROM (
	SELECT DISTINCT patient_id, rx_mon_yr,
	CASE WHEN date_diff <= 365 then 1 ELSE 0 END as clin_enc_365,
	CASE WHEN date_diff <= 183 then 1 ELSE 0 END as clin_enc_183
	FROM(
		SELECT patient_id, rx_mon_yr, min(date_diff) as date_diff
		FROM (
			SELECT T1.patient_id, T1.date as rx_date, T1.rx_mon_yr, T2.date as enc_date, T1.date - T2.date as date_diff
			FROM kre_report.doxy_all_combo T1
			INNER JOIN gen_pop_tools.clin_enc T2 ON (T1.patient_id = T2.patient_id)
			WHERE T2.date < T1.date 
			AND T2.date >= (T1.date - INTERVAL '365 days')
			AND source = 'enc') S1
		GROUP BY patient_id, rx_mon_yr
		) S2
	) S3
GROUP BY rx_mon_yr;


-- PATS DEMOG
DROP TABLE IF EXISTS kre_report.doxy_pat_demog;
CREATE TABLE kre_report.doxy_pat_demog AS
SELECT rx_mon_yr, 
sum(male_pats) as male_pats,
sum(female_pats) as female_pats,
sum(sexor_straight) as sexor_straight_pats,
sum(sexor_gay_lesbian) as sexor_gay_lesbian_pats,
sum(sexor_bisexual) as sexor_bisexual_pats,
sum(sexor_other) as sexor_other_pats,
sum(sexor_unk) as sexor_unk_pats
	FROM
	(
	SELECT DISTINCT patient_id, rx_mon_yr,
	CASE WHEN upper(gender) in ('M', 'MALE') THEN 1 ELSE 0 END as male_pats,
	CASE WHEN upper(gender) in ('F', 'FEMALE') THEN 1 ELSE 0 END as female_pats,
	CASE WHEN upper(sex_orientation) in ('STRAIGHT', 'STRAIGHT OR HETEROSEXUAL', 'STRAIGHT (NOT LESBIAN OR GAY)') then 1 ELSE 0 END as sexor_straight,
	CASE WHEN upper(sex_orientation) in ('GAY', 'LESBIAN', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL', 'LESBIAN OR GAY') then 1 ELSE 0 END as sexor_gay_lesbian,
	CASE WHEN upper(sex_orientation) in ('BISEXUAL', 'BI')  then 1 ELSE 0 END as sexor_bisexual,
	CASE WHEN upper(sex_orientation) not in ('--', 'STRAIGHT', 'STRAIGHT OR HETEROSEXUAL', 'STRAIGHT (NOT LESBIAN OR GAY)','GAY', 'LESBIAN', 'LESBIAN, GAY OR HOMOSEXUAL', 'LESBIAN, GAY, OR HOMOSEXUAL', 'LESBIAN OR GAY','BISEXUAL', 'BI', 'CHOOSE NOT TO DISCLOSE', '''DON''''T KNOW''', 'DON''T KNOW', 'NOT REPORTED', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'NOTDISCLOSED', '') and sex_orientation is not null THEN 1 ELSE 0  END as sexor_other,
	CASE WHEN upper(sex_orientation) in ('--', 'CHOOSE NOT TO DISCLOSE', '''DON''''T KNOW''', 'DON''T KNOW', 'NOT REPORTED', 'OFFICE USE ONLY: INFO NOT COLLECTED', 'OFFICE USE ONLY: UNABLE TO COLLECT THIS INFORMATION DUE TO LACK OF CLINICAL CAPACITY', 'PATIENT CHOOSES NOT TO ANSWER', 'PATIENT IS NOT SURE/DOESN''T KNOW', 'NOTDISCLOSED', '') or sex_orientation is null THEN 1 ELSE 0 END as sexor_unk
	FROM 
		(SELECT DISTINCT T1.patient_id, T1.rx_mon_yr, T2.gender, T2.sex_orientation
		FROM kre_report.doxy_all_combo T1
		INNER JOIN emr_patient T2 ON (T1.patient_id = T2.id)) S1
	) S2
GROUP BY rx_mon_yr;

-- Count of  distinct "rx all"  patients with a nodis HIV case date ON or AFTER the doxy rx date.
DROP TABLE IF EXISTS kre_report.doxy_hiv_post;
CREATE TABLE kre_report.doxy_hiv_post AS
SELECT count(distinct(patient_id)) as hiv_post_doxy, rx_mon_yr
FROM 
	(SELECT DISTINCT T1.patient_id, rx_mon_yr, T1.date rx_date, T2.date hiv_date, T1.date - T2.date as date_diff
	FROM kre_report.doxy_all_combo T1
	INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id and condition = 'hiv')
	WHERE T1.date <= T2.date 
	ORDER BY date_diff asc) S1
GROUP BY rx_mon_yr;

-- Count of  distinct "rx all"  patients with a nodis HIV case date prior to doxy rx date.
DROP TABLE IF EXISTS kre_report.doxy_hiv_prior;
CREATE TABLE kre_report.doxy_hiv_prior AS
SELECT count(distinct(patient_id)) as hiv_prior_doxy, rx_mon_yr
FROM 
	(SELECT DISTINCT T1.patient_id, rx_mon_yr, T1.date rx_date, T2.date hiv_date, T1.date - T2.date as date_diff
	FROM kre_report.doxy_all_combo T1
	INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id and condition = 'hiv')
	WHERE T1.date > T2.date 
	ORDER BY date_diff asc) S1
GROUP BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_syph_days;
CREATE TABLE kre_report.doxy_syph_days AS
SELECT rx_mon_yr, count(distinct(patient_id)) syph_pats, round(avg(date_diff),2) syph_doxy_mean,
round(PERCENTILE_CONT(0.1) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) syph_doxy_days_10th,
round(PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) syph_doxy_days_25th,
round(PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) syph_doxy_days_50th,
round(PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) syph_doxy_days_75th,
round(PERCENTILE_CONT(0.9) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) syph_doxy_days_90th
FROM (
	SELECT patient_id, rx_mon_yr, min(rx_date) earliest_doxy, max(syphilis_date) most_recent_syph,  min(rx_date) - max(syphilis_date) as date_diff
	FROM (
		SELECT DISTINCT T1.patient_id, rx_mon_yr, T1.date rx_date, T2.date syphilis_date
		FROM kre_report.doxy_all_combo T1
		INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id and condition = 'syphilis')
		WHERE T1.date >= T2.date 
		) T1
	--WHERE patient_id in (87426, 2773001)
	--WHERE patient_id in (87426)
	GROUP BY patient_id, rx_mon_yr
    ORDER BY date_diff) S1
GROUP BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_gon_days;
CREATE TABLE kre_report.doxy_gon_days AS
SELECT rx_mon_yr, count(distinct(patient_id)) gon_pats, round(avg(date_diff),2) gon_doxy_mean,
round(PERCENTILE_CONT(0.1) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) gon_doxy_days_10th,
round(PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) gon_doxy_days_25th,
round(PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) gon_doxy_days_50th,
round(PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) gon_doxy_days_75th,
round(PERCENTILE_CONT(0.9) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) gon_doxy_days_90th
FROM (
	SELECT patient_id, rx_mon_yr, min(rx_date) earliest_doxy, max(gon_date) most_recent_gon,  min(rx_date) - max(gon_date) as date_diff
	FROM (
		SELECT DISTINCT T1.patient_id, rx_mon_yr, T1.date rx_date, T2.date gon_date
		FROM kre_report.doxy_all_combo T1
		INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id and condition = 'gonorrhea')
		WHERE T1.date >= T2.date 
		) T1
	GROUP BY patient_id, rx_mon_yr
    ORDER BY date_diff) S1
GROUP BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_chlam_days;
CREATE TABLE kre_report.doxy_chlam_days AS
SELECT rx_mon_yr, count(distinct(patient_id)) chlam_pats, round(avg(date_diff),2) chlam_doxy_mean,
round(PERCENTILE_CONT(0.1) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) chlam_doxy_days_10th,
round(PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) chlam_doxy_days_25th,
round(PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) chlam_doxy_days_50th,
round(PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) chlam_doxy_days_75th,
round(PERCENTILE_CONT(0.9) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) chlam_doxy_days_90th
FROM (
	SELECT patient_id, rx_mon_yr, min(rx_date) earliest_doxy, max(chlam_date) most_recent_chlam,  min(rx_date) - max(chlam_date) as date_diff
	FROM (
		SELECT DISTINCT T1.patient_id, rx_mon_yr, T1.date rx_date, T2.date chlam_date
		FROM kre_report.doxy_all_combo T1
		INNER JOIN nodis_case T2 ON (T1.patient_id = T2.patient_id and condition = 'chlamydia')
		WHERE T1.date >= T2.date 
		) T1
	GROUP BY patient_id, rx_mon_yr
    ORDER BY date_diff) S1
GROUP BY rx_mon_yr;


DROP TABLE IF EXISTS kre_report.doxy_dx_details;
CREATE TABLE kre_report.doxy_dx_details AS
SELECT DISTINCT T1.patient_id, rx_mon_yr, dx_code_id, T1.date rx_date, T2.date dx_date
FROM kre_report.doxy_all_combo T1
INNER JOIN emr_encounter T2 ON (T1.patient_id = T2.patient_id)
INNER JOIN emr_encounter_dx_codes T3 ON (T3.encounter_id = T2.id)
-- ICD/DX DATE BEFORE DOXY DATE
-- DOXY DATE AFTER ICD DATE
WHERE T1.date > T2.date 
AND (T3.dx_code_id in ('icd10:Z20.2', 'icd10:Z20.6')
OR T3.dx_code_id ilike 'icd10:Z72.5%');

DROP TABLE IF EXISTS kre_report.doxy_dx_counts;
CREATE TABLE kre_report.doxy_dx_counts AS
SELECT rx_mon_yr, count(distinct(patient_id)) any_z_code_pats,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.2' then patient_id END)) as sti_contact_dx_pats,
COUNT(DISTINCT(CASE WHEN dx_code_id = 'icd10:Z20.6' then patient_id END)) as hiv_contact_dx_pats,
COUNT(DISTINCT(CASE WHEN dx_code_id ilike 'icd10:Z72.5%' then patient_id END)) as high_risk_sex_dx
FROM kre_report.doxy_dx_details
GROUP BY rx_mon_yr;
		

--Number of days from most recent use of one of these ICD10 codes until first prescription for doxycycline 200mg PO x 1.
--test patient 44447174

DROP TABLE IF EXISTS kre_report.doxy_dx_days_diff;
CREATE TABLE kre_report.doxy_dx_days_diff AS
SELECT T1.patient_id, rx_mon_yr, min(rx_date) as most_recent_rx_after_dx, most_recent_dx_date, min(rx_date) - most_recent_dx_date as date_diff
FROM kre_report.doxy_dx_details T1
JOIN (SELECT patient_id, max(dx_date) most_recent_dx_date from kre_report.doxy_dx_details GROUP BY patient_id) T2 ON (T1.patient_id = T2.patient_id)
--WHERE T1.patient_id = 44447174
WHERE rx_date > most_recent_dx_date
GROUP BY T1.patient_id, rx_mon_yr, most_recent_dx_date
ORDER BY date_diff;

DROP TABLE IF EXISTS kre_report.doxy_dx_days_pct;
CREATE TABLE kre_report.doxy_dx_days_pct AS
SELECT rx_mon_yr, count(distinct(patient_id)) dx_pats, round(avg(date_diff),2) dx_doxy_mean,
round(PERCENTILE_CONT(0.1) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) dx_doxy_days_10th,
round(PERCENTILE_CONT(0.25) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) dx_doxy_days_25th,
round(PERCENTILE_CONT(0.50) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) dx_doxy_days_50th,
round(PERCENTILE_CONT(0.75) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) dx_doxy_days_75th,
round(PERCENTILE_CONT(0.9) WITHIN GROUP(ORDER BY date_diff)::numeric, 2) dx_doxy_days_90th
FROM kre_report.doxy_dx_days_diff
GROUP BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_hiv_risk_scores;
CREATE TABLE kre_report.doxy_hiv_risk_scores AS
SELECT DISTINCT T1.patient_id, T1.date, rx_mon_yr, rpt_year, hiv_risk_score
FROM kre_report.doxy_all_combo T1
JOIN gen_pop_tools.cc_hiv_risk_score T2 ON (T1.patient_id = T2.patient_id)
WHERE extract(year from T1.date) = rpt_year::integer
AND hiv_risk_score > '0.001'
ORDER BY T1.patient_id;

DROP TABLE IF EXISTS kre_report.doxy_hiv_risk_score_counts;
CREATE TABLE kre_report.doxy_hiv_risk_score_counts AS
SELECT rx_mon_yr, 
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.001' then patient_id END)) as hiv_risk_gt_001_pats,
COUNT(DISTINCT(CASE WHEN hiv_risk_score > '0.01' then patient_id END)) as hiv_risk_gt_01_pats
FROM kre_report.doxy_hiv_risk_scores
GROUP BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_prep_eval;
CREATE TABLE kre_report.doxy_prep_eval AS
SELECT rx_mon_yr, count(distinct(patient_id)) prep_6_mos_pats
FROM (
	SELECT T1.patient_id, rx_mon_yr, T1.date as doxy_date, T2.date as hef_date, T2.name, T3.date as case_date, condition, T3.id
	FROM kre_report.doxy_all_combo T1
	JOIN hef_event T2 ON (T1.patient_id = T2.patient_id)
	LEFT JOIN nodis_case T3 ON (T1.patient_id = T3.patient_id and condition = 'hiv')
	WHERE T2.name in ('rx:hiv_tenofovir-emtricitabine:generic', 'rx:hiv_tenofovir-emtricitabine', 
	'rx:hiv_tenofovir_alafenamide-emtricitabine', 'rx:hiv_tenofovir_alafenamide-emtricitabine:generic',
	'rx:hiv_cabotegravir_er_600')
	--don't count if prep date is after hiv case date
	AND (T3.date > T2.date or T3.date is null)
	-- received prep in 6 months prior to doxy
	AND T2.date >= (T1.date - INTERVAL '6 months')
	) S1
GROUP BY rx_mon_yr;

--DENOM DATA REQUEST
-- PATS WITH CLINICAL ENCOUNTER OF TYPE ENC
DROP TABLE IF EXISTS kre_report.doxy_denom;
CREATE TABLE kre_report.doxy_denom AS
SELECT count(distinct(patient_id)) denom_pats_clinenc_vis, to_char(date, 'YYYY-MM') as rx_mon_yr
FROM  
gen_pop_tools.clin_enc T1 
WHERE date >= '2018-01-01'
AND source = 'enc'
GROUP BY rx_mon_yr;


DROP TABLE IF EXISTS kre_report.doxy_rpt_output;
CREATE TABLE kre_report.doxy_rpt_output AS
SELECT
T1.rx_mon_yr,
T13.denom_pats_clinenc_vis,
doxy_pats_any,
doxy_pats_rx_inclusion,
doxy_pats_rx_remainder,
doxy_pats_rx_all,
coalesce(clin_enc_pats_183, 0) clin_enc_pats_183,
coalesce(clin_enc_pats_365, 0) clin_enc_pats_365,
coalesce(male_pats, 0) male_pats,
coalesce(female_pats, 0) female_pats,
coalesce(sexor_straight_pats, 0) sexor_straight_pats,
coalesce(sexor_gay_lesbian_pats, 0) sexor_gay_lesbian_pats,
coalesce(sexor_bisexual_pats, 0) sexor_bisexual_pats,
coalesce(sexor_other_pats, 0) sexor_other_pats,
coalesce(sexor_unk_pats, 0) sexor_unk_pats,
coalesce(hiv_prior_doxy, 0) as hiv_prior_doxy,
coalesce(hiv_post_doxy, 0) as hiv_post_doxy,
coalesce(syph_pats, 0) as syph_pats,
syph_doxy_days_10th,
syph_doxy_days_25th,
syph_doxy_days_50th,
syph_doxy_days_75th,
syph_doxy_days_90th,
coalesce(chlam_pats, 0) as chlam_pats,
chlam_doxy_days_10th,
chlam_doxy_days_25th,
chlam_doxy_days_50th,
chlam_doxy_days_75th,
chlam_doxy_days_90th,
coalesce(gon_pats, 0) as gon_pats,
gon_doxy_days_10th,
gon_doxy_days_25th,
gon_doxy_days_50th,
gon_doxy_days_75th,
gon_doxy_days_90th,
coalesce(any_z_code_pats, 0) as any_z_code_pats,
coalesce(sti_contact_dx_pats, 0) as sti_contact_dx_pats,
coalesce(hiv_contact_dx_pats, 0) as hiv_contact_dx_pats,
coalesce(high_risk_sex_dx, 0) as high_risk_sex_dx,
dx_doxy_days_10th,
dx_doxy_days_25th,
dx_doxy_days_50th,
dx_doxy_days_75th,
dx_doxy_days_90th,
coalesce(hiv_risk_gt_001_pats, 0) as hiv_risk_gt_001_pats,
coalesce(hiv_risk_gt_01_pats, 0) as hiv_risk_gt_01_pats,
coalesce(prep_6_mos_pats, 0) as prep_6_mos_pats
FROM kre_report.doxy_rx_counts T1
LEFT JOIN kre_report.doxy_clin_enc T2 ON (T1.rx_mon_yr = T2.rx_mon_yr)
LEFT JOIN kre_report.doxy_pat_demog T3 ON (T1.rx_mon_yr = T3.rx_mon_yr)
LEFT JOIN kre_report.doxy_hiv_prior T4 ON (T1.rx_mon_yr = T4.rx_mon_yr)
LEFT JOIN kre_report.doxy_hiv_post T5 ON (T1.rx_mon_yr = T5.rx_mon_yr)
LEFT JOIN kre_report.doxy_syph_days T6 ON (T1.rx_mon_yr = T6.rx_mon_yr)
LEFT JOIN kre_report.doxy_chlam_days T7 ON (T1.rx_mon_yr = T7.rx_mon_yr)
LEFT JOIN kre_report.doxy_gon_days T8 ON (T1.rx_mon_yr = T8.rx_mon_yr)
LEFT JOIN kre_report.doxy_dx_counts T9 ON (T1.rx_mon_yr = T9.rx_mon_yr)
LEFT JOIN kre_report.doxy_dx_days_pct T10 ON (T1.rx_mon_yr = T10.rx_mon_yr)
LEFT JOIN kre_report.doxy_hiv_risk_score_counts T11 ON (T1.rx_mon_yr = T11.rx_mon_yr)
LEFT JOIN kre_report.doxy_prep_eval T12 ON (T1.rx_mon_yr = T12.rx_mon_yr)
LEFT JOIN kre_report.doxy_denom T13 ON (T1.rx_mon_yr = T13.rx_mon_yr)
ORDER BY rx_mon_yr;

DROP TABLE IF EXISTS kre_report.doxy_all_meds;
DROP TABLE IF EXISTS kre_report.doxy_abs_inclusion_meds;
DROP TABLE IF EXISTS kre_report.doxy_all_minus_exclu_and_incl;
DROP TABLE IF EXISTS kre_report.doxy_relative_incl;
DROP TABLE IF EXISTS kre_report.doxy_inclu_all;
DROP TABLE IF EXISTS kre_report.doxy_all_minus_exclu_and_incl_and_relinc;
DROP TABLE IF EXISTS kre_report.doxy_remainder_meds;
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_dx;
DROP TABLE IF EXISTS kre_report.doxy_remainder_no_lyme_dx;
DROP TABLE IF EXISTS kre_report.doxy_with_lyme_lx;
DROP TABLE IF EXISTS kre_report.doxy_remainder_meds_final;
DROP TABLE IF EXISTS kre_report.doxy_all_combo;
DROP TABLE IF EXISTS kre_report.doxy_rx_counts_step1;
DROP TABLE IF EXISTS kre_report.doxy_rx_counts;
DROP TABLE IF EXISTS kre_report.doxy_clin_enc;
DROP TABLE IF EXISTS kre_report.doxy_pat_demog;
DROP TABLE IF EXISTS kre_report.doxy_hiv_prior;
DROP TABLE IF EXISTS kre_report.doxy_hiv_post;
DROP TABLE IF EXISTS kre_report.doxy_syph_days;
DROP TABLE IF EXISTS kre_report.doxy_gon_days;
DROP TABLE IF EXISTS kre_report.doxy_chlam_days;
DROP TABLE IF EXISTS kre_report.doxy_dx_details;
DROP TABLE IF EXISTS kre_report.doxy_dx_counts;
DROP TABLE IF EXISTS kre_report.doxy_dx_days_diff;
DROP TABLE IF EXISTS kre_report.doxy_dx_days_pct;
DROP TABLE IF EXISTS kre_report.doxy_hiv_risk_scores;
DROP TABLE IF EXISTS kre_report.doxy_hiv_risk_score_counts;
DROP TABLE IF EXISTS kre_report.doxy_prep_eval;



--\COPY (select * from kre_report.doxy_rpt_output) TO '/tmp/2024-09-20-CHA-doxypep-assessment.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@challiance.org -r keberhardt@commoninf.com -p /tmp -n 2024-09-20-CHA-doxypep-assessment.csv -s "CHA DoxyPEP"

--\COPY (select * from kre_report.doxy_rpt_output) TO '/tmp/2024-09-20-FWY-doxypep-assessment.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f smtpuser@fenwayhealth.org -r keberhardt@commoninf.com -p /tmp -n 2024-09-20-FWY-doxypep-assessment.csv -s "FWY DoxyPEP"

--\COPY (select * from kre_report.doxy_rpt_output) TO '/tmp/2024-09-20-GLFHC-doxypep-assessment.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f noreply@glfhc.org -r keberhardt@commoninf.com -p /tmp -n 2024-09-20-GLFHC-doxypep-assessment.csv -s "GLFHC DoxyPEP"

--\COPY (select * from kre_report.doxy_rpt_output) TO '/tmp/2024-09-20-ATR-doxypep-assessment.csv' WITH CSV HEADER;
--/srv/esp/esp_tools/send_attached_file.py -f esp-no-reply@atriushealth.org -r keberhardt@commoninf.com -p /tmp -n 2024-09-20-ATR-doxypep-assessment.csv -s "ATR DoxyPEP"