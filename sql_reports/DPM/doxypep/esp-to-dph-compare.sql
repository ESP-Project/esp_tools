DROP TABLE IF EXISTS kre_report.doxy_compare_all_meds;
CREATE TABLE kre_report.doxy_compare_all_meds AS
SELECT id, patient_id, date, name, directions, dose, to_char(date, 'YYYY_MM') as rx_mon_yr
FROM emr_prescription 
WHERE (name ilike '%DOXYCYCLINE%'
or name ilike 'ADOXA%'
or name ilike 'DORYX%'
or name ilike 'DOXY-CAPS%'
or name ilike 'MONODOX%'
or name ilike 'VIBRAMYCIN%'
or name ilike 'VIBRA-TABS%');

--EXCLUDE RX WITH LYME LAB WITHIN 60 DAYS
-- CHECK FOR LYME LAB WITHIN 60 DAYS AS DOXY 
DROP TABLE IF EXISTS kre_report.doxy_compare_dph_with_lyme_lx60;
CREATE TABLE kre_report.doxy_compare_dph_with_lyme_lx60 AS
SELECT DISTINCT T1.*
FROM kre_report.doxy_compare_all_meds T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name ilike 'lyme%'
AND T2.date >= T1.date - INTERVAL '60 days'
AND T2.date <= T1.date + INTERVAL '60 days';

-- DROP RX ON SAME DATE AS LYME LAB
-- REMAINDER MEDS FINAL LIST
DROP TABLE IF EXISTS kre_report.doxy_compare_dph_meds_no_lyme;
CREATE TABLE kre_report.doxy_compare_dph_meds_no_lyme AS
SELECT * FROM kre_report.doxy_compare_all_meds
EXCEPT
SELECT * FROM kre_report.doxy_compare_dph_with_lyme_lx60;

-- CLAIM FOR STI TEST W/IN 60 DAYS OF DOXY RX
DROP TABLE IF EXISTS kre_report.doxy_compare_dph_with_sti_lx60;
CREATE TABLE kre_report.doxy_compare_dph_with_sti_lx60 AS
SELECT DISTINCT T1.*
FROM kre_report.doxy_compare_dph_meds_no_lyme T1
LEFT JOIN emr_labresult T2 ON (T1.patient_id = T2.patient_id)
LEFT JOIN conf_labtestmap T3 On (T2.native_code = T3.native_code)
WHERE test_name in ('chlamydia', 'gonorrhea', 'rpr', 'vdrl', 'rpr_riskscape', 'tppa', 'fta-abs', 'tp-igg', 'tp-cia', 'tp-igm', 'vdrl-csf', 'tppa-csf', 'fta-abs-csf')
AND T2.date >= T1.date - INTERVAL '60 days'
AND T2.date <= T1.date + INTERVAL '60 days';


