set client_min_messages to error;

--Get labs of interest for selected patients who have facility records
drop table if exists temp_patient_labs;

create table temp_patient_labs as select emr_labresult.patient_id, emr_labresult.id as labresultid, emr_labresult.date, emr_labresult.native_code, native_name, test_name, specimen_source, result_string from conf_labtestmap, emr_labresult
where (test_name = 'gonorrhea' or test_name = 'chlamydia' or test_name = 'hivcdc' or test_name = 'pregnancy') 
and emr_labresult.native_code = conf_labtestmap.native_code
and patient_id in (select distinct patient_id from emr_stiencounterextended);


--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key and enc.date >= :start_date and enc.date < :end_date;


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_labs;
create table temp_patient_stivisits_labs as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, native_code, native_name, test_name, result_string, specimen_source
from temp_patient_stivisits stivis
join temp_patient_labs on
temp_patient_labs.patient_id = stivis.patient_id
and
temp_patient_labs.date = stivis.encounter_date;

--De-duplicate data based on CDC reporting fields
drop table if exists temp_patient_stivisits_labs_dd;
create table temp_patient_stivisits_labs_dd as 
select patient_id, encounter_id, encounter_date, native_code, native_name, test_name, result_string, specimen_source from temp_patient_stivisits_labs group by patient_id, encounter_id, encounter_date, native_code, native_name, test_name, result_string, specimen_source;

-- Test set of labs included
Select distinct native_code from temp_patient_stivisits_labs;
select distinct native_name from temp_patient_stivisits_labs;
select distinct test_name from temp_patient_stivisits_labs;
select distinct result_string from temp_patient_stivisits_labs;
select distinct specimen_source from temp_patient_stivisits_labs;

--Report
drop table if exists temp_ssun_labs_report;
create table temp_ssun_labs_report as select t.patient_id as "F3_PatientID", t.encounter_id as "F3_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F3_Visdate",
case 
	when t.test_name = 'gonorrhea' then 2
	when t.test_name = 'chlamydia' then 3
	when t.test_name = 'hivcdc' then 6
	when t.test_name = 'pregnancy' then 20
end as "F3_Condtested",

case 
	--Culture
	when t.native_code = '0691-6--LOI-0691-6' then 1
	when t.native_code = '0688-2--LOI-0688-2' then 1
	when t.native_code = '0696-5--LOI-0696-5' then 1
	when t.native_code = '0697-3--LOI-0697-3' then 1
	when t.native_code = '1270003--LOC-1270003' then 1
	when t.native_code = '7293--MLI-7293' then 1
	--NAAT
	when t.native_code = '110694--MLI-110694' then 2
	when t.native_code = '117632--MLI-117632' then 2
	when t.native_code = '76052--MLI-76052' then 2	
	when t.native_code = '274152--MLI-274152' then 2
	when t.native_code = '274151--MLI-274151' then 2
	when t.native_code = '274150--MLI-274150' then 2
	when t.native_code = '274149--MLI-274149' then 2
	when t.native_code = '7293--MLI-7293' then 2	
	when t.native_code = '117631--MLI-117631' then 2	
	when t.native_code = '274153--MLI-274153' then 2
	when t.native_code = '274154--MLI-274154' then 2
	when t.native_code = '274156--MLI-274156' then 2
	when t.native_code = '274155--MLI-274155' then 2
	when t.native_code = '210025--LOC-210025' then 2
	when t.native_code = '6857--MLI-6857' then 2
	--Probe
	when t.native_code = '4001.61--MLI-4001.61' then 3
	when t.native_code = '11209--MLI-11209' then 3
	--Gram Stain
	when t.native_code = 'Gram stain' then 4
	--HIV Nucleic acid
	when t.native_code = 'HIV Nucleic acid test (NAT)' then 10
	-- HIV rapid HIV-1 or HIV-1/2 antibody (Ab) test
	when t.native_code = 'rapid HIV-1 or HIV-1/2 antibody (Ab) test' then 11
	--HIV-1 Immunoassay (IA)
	when t.native_code = '5220-9--LOI-5220-9' then 12
	-- HIV-1/2 IA
	when t.native_code = '5223-3--LOI-5223-3' then 13
	when t.native_code = '7918-6--LOI-7918-6' then 13 
	--HIV-1/2 Ag/Ab IA
	when t.native_code = '443470--MLI-443470' then 14
	when t.native_code = '4312--MLI-4312' then 14 
	--WB
	when t.native_code = 'HIV-1 WB' then 15
	--IFA	
	when t.native_code = 'HIV-1 IFA' then 16
	--HIV-1/HIV-2 differentiation IA
	when t.native_code = 'HIV-1/HIV-2 differentiation IA' then 17
	--HIV pooled RNA
	when t.native_code = '17320--MLI-17320' then 18
	--Pregnancy
	when t.native_code = 'Pregnancy' then 40
	--Other HIV-2
	when t.native_code = '82948--MLI-82948' then 88 
	--Else OTHER
	--CHANGE THIS TO 88 after validation.. make sure the OTHERS are OK
	else 9999
end as "F3_Test_Type",

case
	when t.result_string = 'Negative' then 0
	when t.result_string = 'NOT DETECTED' then 0
	when t.result_string = 'Not Detected' then 0
	when t.result_string = 'Positive' then 1
	when t.result_string = 'NON-REACTIVE' then 2
	when t.result_string = 'Reactive' then 3
	when t.result_string = 'Indeterminant' then 4
	when t.result_string = 'Undetermined' then 4
	when t.result_string = 'Weakly Reactive' then 5
	when t.result_string = 'QNS/Contaminated/Unsaturated' then 6
	when t.result_string = 	null then 9
	when t.result_string = 'other/pending' then 8
	else 9999
end as "F3_Qualres",

case 
	when t.specimen_source = 'urethra' then 1
	when t.specimen_source = 'cervix' then 2
	when t.specimen_source = 'Urine' then 3
	when t.specimen_source = 'rectum' then 4
	when t.specimen_source = 'throat' then 5
	when t.specimen_source = 'Blood' or t.test_name = 'hivcdc' then 6
	when t.specimen_source = null then null
	when t.specimen_source = 'unknown' then null
	when t.specimen_source ='other' then 8
	when t.specimen_source = 'Not Captured' then 9
	else 9999
end as "F3_Anatsite"
from 	temp_patient_stivisits_labs_dd t;
select count(*) from temp_ssun_labs_report;
\copy temp_ssun_labs_report to '/tmp/SSUN_labs.csv' csv header

