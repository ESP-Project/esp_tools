set client_min_messages to error;

--Get meds for selected patients who have facility records
drop table if exists temp_patient_meds;
create table temp_patient_meds as select emr_prescription.patient_id, emr_prescription.date, name, dose, quantity, frequency, start_date, end_date 
from emr_prescription where 
patient_id in (select distinct patient_id from emr_stiencounterextended);

--Join extended table against encounter to get the encounter id for the report
drop table if exists temp_patient_stivisits;
create table temp_patient_stivisits as select sti.patient_id, enc.id as encounter_id, sti.date as encounter_date from emr_stiencounterextended sti
join emr_encounter enc on
sti.natural_key = enc.natural_key and enc.date >= :start_date and enc.date < :end_date;


-- Build up the data needed for the report from joining the two tables on date
drop table if exists temp_patient_stivisits_meds;
create table temp_patient_stivisits_meds as 
select stivis.patient_id, stivis.encounter_id, stivis.encounter_date, name, dose, frequency, start_date, end_date
from temp_patient_stivisits stivis
join temp_patient_meds on
temp_patient_meds.date = stivis.encounter_date
and temp_patient_meds.patient_id = stivis.patient_id;

-- Deduplicating step by CDC reporting fields
drop table if exists temp_patient_stivisits_meds_dd;
create table temp_patient_stivisits_meds_dd as 
select patient_id, encounter_id, encounter_date, name, dose, frequency, start_date, end_date
from temp_patient_stivisits_meds group by patient_id, encounter_id, encounter_date, name, dose, frequency, start_date, end_date;


drop table if exists temp_ssun_meds_report;
create table temp_ssun_meds_report as select t.patient_id as "F4_PatientID", t.encounter_id as "F4_EventID", to_char(encounter_date, 'mm/dd/yyyy') as "F4_Visdate",
case 
	when t.name = 'AZITHROMYCIN 250 MG TABS' or t.name = 'AZITHROMYCIN 1 GM PACK' 
		or t.name = 'Zithromax Dose'  or t.name = 'ZITHROMAX UP TO 1GM' then '20'
	when t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPS' 
		or t.name= 'DOXYCYCL HYC CAP 100MG' 
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG TABS' 
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABS'
		or t.name = 'Doxycycline dose' 
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE' then '50'
	when t.name = 'Rocephin (Ceftriaxone) Injection' 
		or t.name = 'CEFTRIAXONE EA 250MG' 
		or t.name = 'CEFTRIAXONE 250MG' then '37'
	when t.name = 'Cefixime' then '30'
	when t.name = 'TRUVADA 200-300 MG TABS'
	    or t.name = 'TRUVADA 200-300 MG ORAL TABS' 
		or t.name = 'TRUVADA 200-300 MG ORAL TABLET'
		or t.name = 'TRUVADA 200 MG-300 MG TABLET' 
		or t.name = 'TRUVADA 200-300MG 340B' then '70'
	when t.name = 'METRONIDAZOLE 0.75 % VAG GEL'
           or t.name = 'METRONIDAZOLE 0.75 % VAGINAL GEL'
		or t.name = 'METRONIDAZOLE 500 MG  TABS'
		or t.name = 'METRONIDAZOLE 500 MG TABS'
		or t.name = 'METRONIDAZOLE TAB 500MG' 
		or t.name = 'METRONIDAZOLE 500 MG ORAL TABS' 
		or t.name = 'METRONIDAZOLE 500 MG ORAL TABLET' then '60' 
	when t.name = 'FACTIVE 320 MG ORAL TABS' then '44'
	when t.name = 'CIPRO 500 MG TAB' then '40' 
	when t.name = 'TINIDAZOLE 500 MG ORAL TABLET' then '61' 
	when t.name ilike '%BICILLIN%' then null
	when t.name ilike '%LEVAQUIN%' then null
	when t.name ilike 'PENICILLIN V POTASSIUM 500 MG ORAL TABLET' then null
	else concat('99999:med_name: ', name)
	
end as "F4_Medication",

----- Can code this out in future but right now all meds in file but all meds are mapped above
null "F4_Medication_other",

	case 
	when t.name = 'AZITHROMYCIN 250 MG TABS' 
		or t.name = 'CEFTRIAXONE EA 250MG' 
		or t.name = 'CEFTRIAXONE 250MG' then '6'
	when t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPS' 
		or t.name = 'DOXYCYCL HYC CAP 100MG'
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG TABS' 
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL TABS' 
		or t.name = 'DOXYCYCLINE HYCLATE 100 MG ORAL CAPSULE' then '1'
	when t.name = 'AZITHROMYCIN 1 GM PACK' or t.name = 'ZITHROMAX UP TO 1GM' then '14'
	when t.name = 'Rocephin (Ceftriaxone) Injection' or t.name = 'Zithromax Dose' 
		or t.name = 'Cefixime' then '88'
	when t.name = 'TRUVADA 200-300 MG TABS' 
		or t.name = 'TRUVADA 200-300 MG ORAL TABS' 
		or t.name = 'TRUVADA 200-300 MG ORAL TABLET'
		or t.name = 'TRUVADA 200 MG-300 MG TABLET' 
		or t.name = 'TRUVADA 200-300MG 340B' then '88'
	when t.name = 'METRONIDAZOLE 500 MG  TABS'
		or t.name = 'METRONIDAZOLE 500 MG TABS'
		or t.name = 'METRONIDAZOLE TAB 500MG'
		or t.name = 'CIPRO 500 MG TAB' 
		or t.name = 'METRONIDAZOLE 500 MG ORAL TABS' 
		or t.name = 'METRONIDAZOLE 500 MG ORAL TABLET' 
		or t.name = 'TINIDAZOLE 500 MG ORAL TABLET' then '10'
	when t.name = 'METRONIDAZOLE 0.75 % VAG GEL'
		or t.name = 'METRONIDAZOLE 0.75 % VAGINAL GEL' then '88'
	when t.name = 'FACTIVE 320 MG ORAL TABS' then '8'
	when t.name ilike '%BICILLIN%' then null
	when t.name = 'Doxycycline dose' then null
	when t.name ilike '%LEVAQUIN%' then null
	when t.name ilike 'PENICILLIN V POTASSIUM 500 MG ORAL TABLET%' then null
	else concat('99999:med_name for dosage: ', name)
end as "F4_Dosage",

	case
	when t.name = 'Zithromax Dose' or t.name = 'AZITHROMYCIN 1 GM PACK' 						or t.name = 'Rocephin (Ceftriaxone) Injection' then 1
	else null
end as "F4_Number_doses",

	case
	when t.name = 'Zithromax Dose' or t.name = 'AZITHROMYCIN 1 GM PACK' 						or t.name = 'Rocephin (Ceftriaxone) Injection' then 1
	else null
end as "F4_Dose_freq",

	case
	when (t.end_date - t.start_date <1 ) or (t.end_date - t.start_date >100) then null
	when (t.end_date - t.start_date) = 1 then 1
	when (t.end_date - t.start_date) = 3 then 2
	when (t.end_date - t.start_date) = 5 then 3
	when (t.end_date - t.start_date) = 7 then 4
	when (t.end_date - t.start_date) = 10 then 5
	when (t.end_date - t.start_date) = 14 then 6
	else 8
end as "F4_Duration"

from temp_patient_stivisits_meds_dd t;
\copy temp_ssun_meds_report to '/tmp/SSUN_meds.csv' csv header

