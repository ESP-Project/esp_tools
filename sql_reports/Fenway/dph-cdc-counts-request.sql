
-- 1.
-- -- Total STI encounters
-- SELECT count(*) total_sti_encounters
-- FROM emr_stiencounterextended 
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018';


-- 2. 
-- -- Total STI encounter patients
-- SELECT count(distinct(patient_id)) total_sti_pat_count
-- FROM emr_stiencounterextended 
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018';


-- 3. 
-- -- Clinic visits missing gender
-- SELECT count(patient_id) missing_gender_count
-- FROM emr_stiencounterextended sti, emr_patient p
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (gender in ('U' ,'UNKNOWN','' ) or gender is null)
-- AND sti.patient_id = p.id;


-- 4. 
-- -- Clinic visits missing race
-- SELECT count(patient_id) missing_race_count
-- FROM emr_stiencounterextended sti, emr_patient p
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN', '' ) or race is null)
-- AND sti.patient_id = p.id;

-- 5.
-- -- Clinic visits missing sexual behavior

-- -- f1_mensex
-- SELECT count(*) missing_f1_mensex
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (num_male_partners is null);

-- -- f1_femsex
-- SELECT count(*) missing_f1_femsex
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (num_female_partners is null);


-- -- f1_sexor3
-- SELECT count(*) missing_f1_sexor3
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (sex_mwb is null);

-- -- f1_numsex3
-- SELECT count(*) missing_f1_numsex3
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (total_partners is null);

-- -- f1_sexuality
-- SELECT count(*) missing_f1_sexuality
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (total_partners is null or sexuality in ('--'));

-- -- f1_newsex
-- SELECT 'f1_newsex is not captured at Fenway' as f1_newsex;


-- ================================================================================

SELECT T1.*, T2.*, T3.*, T4.*, T5.*, T6.*, T7.*, T8.*, T9.*, T10.*

FROM

-- Total STI encounters
(SELECT count(*) total_sti_encounters
FROM emr_stiencounterextended 
WHERE date BETWEEN '04-01-2017' AND '03-31-2018') T1,

-- Total STI encounter patients
(SELECT count(distinct(patient_id)) total_sti_pat_count
FROM emr_stiencounterextended 
WHERE date BETWEEN '04-01-2017' AND '03-31-2018') T2,

-- Clinic visits missing gender
(SELECT count(patient_id) missing_gender_count
FROM emr_stiencounterextended sti, emr_patient p
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (gender in ('U' ,'UNKNOWN','' ) or gender is null)
AND sti.patient_id = p.id) T3,

-- Clinic visits missing race
(SELECT count(patient_id) missing_race_count
FROM emr_stiencounterextended sti, emr_patient p
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (race in ('Patient Declined' ,'Undetermined/Not Reported' ,'Unknown' ,'UNKNOWN', '' ) or race is null)
AND sti.patient_id = p.id) T4,

-- Clinic visits missing sexual behavior

-- f1_mensex
-- (SELECT count(*) missing_f1_mensex
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (num_male_partners is null)) T5,

(SELECT 'f1_mensex is not captured at Fenway' as f1_mensex) T5;

-- f1_femsex
-- (SELECT count(*) missing_f1_femsex
-- FROM emr_stiencounterextended sti
-- WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
-- AND (num_female_partners is null)) T6,

(SELECT 'f1_femsex is not captured at Fenway' as f1_femsex) T5;

-- f1_sexor3
(SELECT count(*) missing_f1_sexor3
FROM emr_stiencounterextended sti
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (sex_mwb is null)) T7,

-- f1_numsex3
(SELECT count(*) missing_f1_numsex3
FROM emr_stiencounterextended sti
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (total_partners is null)) T8,

-- f1_sexuality
(SELECT count(*) missing_f1_sexuality
FROM emr_stiencounterextended sti
WHERE date BETWEEN '04-01-2017' AND '03-31-2018'
AND (total_partners is null or sexuality in ('--'))) T9,

-- f1_newsex
(SELECT 'f1_newsex is not captured at Fenway' as f1_newsex) T10;





