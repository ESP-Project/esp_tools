#!/bin/bash

## SET YOUR VARIABLES HERE ##

DB_NAME=esp

SCRIPT_DIR=/home/esp/SSUN_CDC_Scripts

MED_SCRIPT='SSUN_meds_report_FENWAY.sql'
STI_SCRIPT='SSUN_sti_report_FENWAY.sql'
DIAG_SCRIPT='SSUN_diags_report_FENWAY.sql'
LAB_SCRIPT='SSUN_labs_report_FENWAY.sql'

## END VARIABLE SET ##
## DO NOT MODIFY BELOW THIS LINE ##

OUTPUT_DIR=/tmp

MED_FILE=$SCRIPT_DIR/$MED_SCRIPT
ORIG_MED_OUTPUT_FILE=SSUN_meds.csv

STI_FILE=$SCRIPT_DIR/$STI_SCRIPT
ORIG_STI_OUTPUT_FILE=SSUN_clinic.csv

DIAG_FILE=$SCRIPT_DIR/$DIAG_SCRIPT
ORIG_DIAG_OUTPUT_FILE=SSUN_diags.csv

LAB_FILE=$SCRIPT_DIR/$LAB_SCRIPT
ORIG_LAB_OUTPUT_FILE=SSUN_labs.csv


echo "Insert Start Date: (i.e. 2015-01-01) "
read start_date 
#echo "You entered: $start_date"

echo "Insert End Date: (i.e. 2016-01-01"
read end_date
#echo "You entered: $end_date"


FINAL_MED_OUTPUT_FILE="SSUN_meds-$start_date-$end_date.csv"
FINAL_STI_OUTPUT_FILE="SSUN_sti-$start_date-$end_date.csv"
FINAL_DIAG_OUTPUT_FILE="SSUN_diag-$start_date-$end_date.csv"
FINAL_LAB_OUTPUT_FILE="SSUN_labs-$start_date-$end_date.csv"


med_sql=$(psql $DB_NAME -f "$MED_FILE" -v start_date="'$start_date'" -v end_date="'$end_date'" -t | tr -d ' \n')
mv "$OUTPUT_DIR/$ORIG_MED_OUTPUT_FILE" "$OUTPUT_DIR/$FINAL_MED_OUTPUT_FILE" 

echo "FINISHED MED FILE"

sti_sql=$(psql $DB_NAME -f "$STI_FILE" -v start_date="'$start_date'" -v end_date="'$end_date'" -t | tr -d ' \n')
mv "$OUTPUT_DIR/$ORIG_STI_OUTPUT_FILE" "$OUTPUT_DIR/$FINAL_STI_OUTPUT_FILE"

echo "FINISHED STI FILE"

diag_sql=$(psql $DB_NAME -f "$DIAG_FILE" -v start_date="'$start_date'" -v end_date="'$end_date'" -t | tr -d ' \n')
mv "$OUTPUT_DIR/$ORIG_DIAG_OUTPUT_FILE" "$OUTPUT_DIR/$FINAL_DIAG_OUTPUT_FILE"

echo "FINISHED DIAG FILE"

lab_sql=$(psql $DB_NAME -f "$LAB_FILE" -v start_date="'$start_date'" -v end_date="'$end_date'" -t | tr -d ' \n')
mv "$OUTPUT_DIR/$ORIG_LAB_OUTPUT_FILE" "$OUTPUT_DIR/$FINAL_LAB_OUTPUT_FILE"

echo "FINISHED LAB FILE"


