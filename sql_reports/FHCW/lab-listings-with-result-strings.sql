drop table if exists temp_hiv_mappings;

create table temp_hiv_mappings as
select count(*), c.test_name, l.native_name, l.native_code --, l.procedure_name
from conf_labtestmap c,
emr_labresult l
where c.native_code = l.native_code
--and c.test_name ilike '%hiv%'
--and c.test_name != 'hiv not a test'
group by c.test_name, l.native_name, l.native_code --, l.procedure_name
;

DROP TABLE IF EXISTS temp_hiv_mappings_with_result_strings;
CREATE TABLE temp_hiv_mappings_with_result_strings AS
SELECT T2.lab_count, T2.native_code, T2.native_name, --T2.procedure_name, 
array_agg(distinct(T1.result_string)) top_result_strings
FROM
(SELECT
count(distinct(S1.id)),
 S1.native_code, S1.native_name --, S1.procedure_name,
 , result_string,
   RANK () OVER (
      PARTITION BY S1.native_code, S1.native_name --, S1.procedure_name
      ORDER BY count(result_string) DESC
   ) rank
FROM
   emr_labresult S1,
   temp_hiv_mappings S2
   where S1.native_code = S2.native_code
   group by S1.native_code, result_string, S1.native_name 
   --, S1.procedure_name
   ) T1,
 (select count(distinct(T1.id)) lab_count, T1.native_code, T1.native_name --, T1.procedure_name
  from emr_labresult T1,
  temp_hiv_mappings T2
  where T1.native_code = T2.native_code
  group by T1.native_code, T1.native_name
  --, T1.procedure_name
  ) T2
WHERE rank <= 5
AND T1.native_code = T2.native_code
AND T1.native_name = T2.native_name
--AND T1.procedure_name = T2.procedure_name
GROUP BY T2.lab_count, T2.native_code, T2.native_name --, T2.procedure_name
ORDER BY T2.native_code, T2.native_name;

DROP TABLE IF EXISTS temp_hiv_mappings_with_result_strings_final;
CREATE TABLE temp_hiv_mappings_with_result_strings_final AS
select T1.lab_count, 
T3.test_name as mapping,
T1.native_code, 
T1.native_name, 
--T1.procedure_name, 
max(date) as most_recent_lab_date, 
min(date) as oldest_lab_date, 
T3.test_name, 
T3.threshold,
T3.output_code,
T3.snomed_pos,
T3.snomed_neg,
T3.snomed_ind,
T1.top_result_strings
from temp_hiv_mappings_with_result_strings T1,
emr_labresult T2,
conf_labtestmap T3
where T1.native_code = T2.native_code
and T1.native_code = T3.native_code
--T2.date > '01-01-2021' 
group by T1.lab_count, T1.native_code, T1.native_name, 
--T1.procedure_name, 
T1.top_result_strings, T3.test_name, T3.threshold, T3.output_code,T3.snomed_pos,T3.snomed_neg,T3.snomed_ind
order by T3.test_name, 
--T1.native_code, T1.native_name, 
--T1.procedure_name, 
T3.output_code;



\COPY temp_hiv_mappings_with_result_strings_final TO '/tmp/lab_mappings_for_review.csv' DELIMITER ',' CSV HEADER;
