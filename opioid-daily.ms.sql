-- Analysis script generated on Wednesday, 08 Jul 2015 13:34:16 EDT
-- Analysis name: opioidrx
-- Analysis description: opioidrx
-- Script generated for database: POSTGRESQL

--
-- Script setup section 
--


IF object_id('dbo.opi_a_100007_s_6') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_6;
IF object_id('dbo.opi_a_100007_s_7') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_7;
IF object_id('dbo.opi_a_100007_s_8') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_8;
IF object_id('dbo.opi_a_100007_s_10') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_10;
IF object_id('dbo.opi_a_100007_s_11') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_11;
IF object_id('dbo.opi_a_100007_s_12') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_12;
IF object_id('dbo.opi_a_100007_s_13') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_13;
IF object_id('dbo.opi_a_100007_s_14') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_14;
IF object_id('dbo.opi_a_100007_s_15') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_15;
IF object_id('dbo.opi_a_100007_s_16') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_16;
IF object_id('dbo.opi_a_100007_s_19') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_19;
IF object_id('dbo.opi_a_100007_s_20') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_20;
IF object_id('dbo.opi_a_100007_s_21') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_21;
IF object_id('dbo.opi_a_100007_s_22') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_22;
IF object_id('dbo.opi_a_100007_s_23') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_23;
IF object_id('dbo.highopi_temp_1') IS NOT NULL DROP TABLE dbo.highopi_temp_1;
IF object_id('dbo.highopi_temp_2') IS NOT NULL DROP TABLE dbo.highopi_temp_2;
IF object_id('dbo.highopi_temp_3') IS NOT NULL DROP TABLE dbo.highopi_temp_3;
IF object_id('dbo.highopi_temp_4') IS NOT NULL DROP TABLE dbo.highopi_temp_4;
IF object_id('dbo.highopi_temp_5') IS NOT NULL DROP TABLE dbo.highopi_temp_5;

IF object_id('dbo.ccda_opibenzo') IS NOT NULL DROP TABLE ccda_opibenzo;

--
-- Script body 
--

-- Join - BASE Prescription Records for the Last Year -- Join the prescription table on the patient table. Limit by records with a start_date within the last year and have a valid status
SELECT T1.patient_id,T1.name,T2.date_of_birth,T2.cdate_of_birth,T1.start_date,T2.natural_key,
CASE WHEN (T1.end_date > T1.start_date) THEN end_date ELSE DATEADD(day, 30, start_date) END end_date,
T1.quantity_float,T1.quantity,T1.quantity_type,
CASE WHEN (refills LIKE '[0-9]%' AND refills NOT LIKE '%[^0-9]%') THEN cast(refills AS real) ELSE 0 END  refills, T1.natural_key rx_natural_key 
into dbo.opi_a_100007_s_6
FROM dbo.emr_prescription T1 INNER JOIN dbo.emr_patient T2 ON ((T1.patient_id = T2.id))
--WHERE (start_date >= '2004-01-01' and start_date < '2005-01-01')
WHERE start_date >= dateadd(year, -1, GETDATE()) 
AND start_date <= getdate()
and (T1.patient_class is null or T1.patient_class like '1.%');

-- Join - BASE Join (limit) on drugs in the drug_lookup table
SELECT T1.patient_id,T2.name,T2.type_id,T1.date_of_birth,T1.start_date,T1.natural_key,T1.end_date,T1.quantity_float,T1.quantity,T1.quantity_type,T2.conversion_factor,T2.dosage_strength,T1.refills, T1.rx_natural_key 
into dbo.opi_a_100007_s_7   
FROM dbo.opi_a_100007_s_6 T1 
INNER JOIN dbo.static_rx_lookup T2 ON ((T1.name = T2.name)) ;

-- BenzOpiConcurrent Find patients who have a prescription for BOTH an opioid and a benzo within 30 days of each other
-- Start_date corresponds to start_date of when the overlap starts
-- End_date corresponds to 365 days from the start_date
SELECT opi.patient_id, opi.natural_key, opi.date_of_birth, 
  CASE WHEN opi.start_date > benzo.start_date THEN opi.start_date ELSE benzo.start_date END AS start_date,
  CASE WHEN opi.end_date < benzo.end_date THEN opi.end_date ELSE benzo.end_date END AS end_date
into dbo.opi_a_100007_s_8
FROM (SELECT * from dbo.opi_a_100007_s_7 where type_id =1) opi
JOIN (SELECT * from dbo.opi_a_100007_s_7 where type_id =2) benzo
ON opi.patient_id=benzo.patient_id
AND (
  (DATEADD(day, -30, opi.start_date) > benzo.start_date AND DATEADD(day, -30, opi.start_date) < benzo.end_date) OR
  (DATEADD(day, 30, opi.end_date) > benzo.start_date AND DATEADD(day, 30, opi.end_date) < benzo.end_date) OR
  (DATEADD(day, -30, opi.start_date) < benzo.start_date AND DATEADD(day, 30, opi.end_date) > benzo.end_date) );
GO

SELECT T1.patient_id, T1.natural_key, T1.date_of_birth, start_date,  
CAST('benzopiconcurrent' AS varchar(max)) local_condition, 
CAST('a prescription for a benzodiazepine within 30 days before or after a prescription for an opioid within the past year' 
      AS varchar(max)) local_criteria,
start_date disease_date,
DATEDIFF(day, CONVERT(date, '01/01/1960', 101), start_date) start_date_mdphnet,
DATEADD(year, 1, start_date) cond_expire_date
INTO dbo.opi_a_100007_s_10
FROM dbo.opi_a_100007_s_8 T1
GROUP BY patient_id, natural_key, date_of_birth, start_date, DATEADD(year, 1, start_date);
GO

-- SQL - OpioidRx, BenzodiaenzoRx - Query for only the record with the earliest date for the first prescription (disease_date) 
SELECT patient_id,  type_id, natural_key, date_of_birth, start_date AS "disease_date" 
into dbo.opi_a_100007_s_11 
FROM dbo.opi_a_100007_s_7 
GROUP BY patient_id, type_id, natural_key, date_of_birth, start_date;

-- Filter - OpioidRx, BenzodiaRx - Prescription for Opioid or Benzo 
SELECT * 
into dbo.opi_a_100007_s_12 
FROM dbo.opi_a_100007_s_11 WHERE type_id in (1,2);

-- Derive Columns - OpioidRx, BenzodiaRx - Assign Condition Name, Criteria, Status, Notes, & Start Date in mdphnet format
SELECT T1.*,CASE when  (type_id) = 1  THEN  text('opioidrx')
when (type_id) = 2  THEN  text('benzodiarx')
END local_condition,CASE when  (type_id) = 1  THEN  text('>= 1 prescription for opioid within the last year')
when (type_id) = 2  THEN  text('>= 1 prescription for benzodiazepine within the last year')
END local_criteria, DATEDIFF(day, CONVERT(date, '01/01/1960', 101), disease_date)  start_date_mdphnet,
DATEADD(year, 1, disease_date) cond_expire_date
into dbo.opi_a_100007_s_13 
FROM dbo.opi_a_100007_s_12 T1;

-- Filter - HighOpioidDose - Just get the opioid prescriptions with valid quantity_types
SELECT * 
into dbo.opi_a_100007_s_14 
FROM dbo.opi_a_100007_s_7 
WHERE  type_id = 1 
and (quantity_type similar to '%(cc|cap|count|each|film|lozenge|ml|patch|pill|strip|suppos|tab|unit)%' 
or quantity_type is null 
or quantity_type = '') 
and dosage_strength is not null 
and quantity_float > 0;


-- Step 15: Derive Columns - HighOpioidUse - Compute script_days & proper refill amount (1 refill means 2 prescriptions)
SELECT T1.*,
CASE when (cast(refills as real) = 0) then 1 
else (cast(refills as real) + 1) end mod_refills,
CASE when ((end_date - start_date) > 0) then (end_date - start_date) 
else 30 end script_days 
into dbo.opi_a_100007_s_15 
FROM dbo.opi_a_100007_s_14 T1;

-- Step 16: Derive Columns - HighOpioidUse - Compute morphine equiv per day
SELECT T1.*,(((quantity_float*dosage_strength)*conversion_factor)*mod_refills) /  script_days  morphine_equiv 
into dbo.opi_a_100007_s_16   
FROM dbo.opi_a_100007_s_15 T1;

-- Step 17: Aggregate - HighOpioidUse - Patients prescribed ≥100 milligrams of morphine equivalents per day for ≥90 days within the past year
SELECT patient_id, name, date_of_birth, natural_key, rx_natural_key, start_date, end_date, morphine_equiv 
into dbo.highopi_temp_1 
FROM  dbo.opi_a_100007_s_16 where 1=2; --stub table for subsequent inserts

begin
  declare @patid integer;
  declare @patid_inner integer;
  DECLARE @patidinner integer;
  declare @enddt date;
  declare @insrtprfx varchar(max);
  DECLARE @morphine_equiv numeric;
  DECLARE @end_date DATE;
  DECLARE @end_date_t DATE;
  DECLARE @next_date DATE;
  DECLARE @start_date DATE;
  DECLARE @next_start DATE;
  DECLARE @date_of_birth DATE;
  DECLARE @name varchar(max);
  DECLARE @natural_key varchar(max);
  DECLARE @rx_natural_key varchar(max);
  SET @insrtprfx='INSERT INTO dbo.highopi_temp_1 (patient_id, name, date_of_birth, natural_key, rx_natural_key, start_date, end_date, morphine_equiv) values (';
  DECLARE cur CURSOR FOR select distinct patient_id from dbo.opi_a_100007_s_16 order by patient_id;
  OPEN cur;
  FETCH NEXT FROM cur INTO @patid;
  WHILE @@FETCH_STATUS = 0
    begin
      SET @enddt=null; --set this values to null for each new @patid
      DECLARE currow CURSOR FOR SELECT patient_id, name, natural_key, date_of_birth, rx_natural_key, start_date,
          DATEADD(day, script_days, start_date) end_date, morphine_equiv, lead(start_date,1) over
          (partition by patient_id order by start_date, DATEADD(day, script_days, start_date), rx_natural_key) next_start
        FROM dbo.opi_a_100007_s_16 WHERE patient_id = @patid and type_id=1 and
          morphine_equiv is not null order by start_date, DATEADD(day, script_days, start_date), rx_natural_key;
      OPEN currow;
      FETCH NEXT FROM cur INTO @patid_inner, @name, @natural_key, @date_of_birth, @rx_natural_key, @start_date,
          @end_date, @morphine_equiv, @next_start;
      WHILE @@FETCH_STATUS = 0
        BEGIN
          if @morphine_equiv>=100 or @end_date >= @next_start or @start_date <= @enddt
            DECLARE @sql varchar(max) = @insrtprfx + CAST(@patid_inner AS varchar(10))
              + ', ''' + @name + ''', '''
              + COALESCE(CAST(@date_of_birth AS varchar(max)), '1900-01-01')
              + ''', ''' + @natural_key + ''', ''' + @rx_natural_key
              + ''', @start_date, @end_date, ' + CAST(@morphine_equiv AS varchar(10)) + ')';

            EXECUTE sp_executesql @sql;

          FETCH NEXT FROM cur INTO @patidinner, @name, @natural_key, @date_of_birth, @rx_natural_key, @start_date,
            @end_date, @morphine_equiv, @next_start;
        END;
        CLOSE currow;
        DEALLOCATE currow;
                SET @end_date_t = coalesce(@enddt, CONVERT(DATE, '01/01/1900', 101));
                IF @end_date > @end_date_t
                  SET @enddt = @end_date;
                ELSE
                  SET @enddt = @end_date_t;
      FETCH NEXT FROM cur INTO @patid;
    end;
  CLOSE cur;
  DEALLOCATE cur;
END;
GO

DECLARE @min_date DATE;
DECLARE @max_date DATE;
SELECT @min_date = min(start_date) FROM dbo.highopi_temp_1;
SELECT @max_date = max(end_date) FROM dbo.highopi_temp_1;

SELECT dates
INTO dbo.highopi_temp_2
FROM gen_pop_tools.generate_day_series(@min_date, @max_date)
GO


SELECT t0.*, t1.dates
into dbo.highopi_temp_3
FROM dbo.highopi_temp_1 t0 JOIN dbo.highopi_temp_2 t1 ON t1.dates between t0.start_date and t0.end_date;
;

-- Find all dates the patient had over 100 morh equiv and keey a running tally of the number of days
SELECT patient_id, date_of_birth, natural_key, dates, array_agg(rx_natural_key order by rx_natural_key) as keys, sum(morphine_equiv) as morphine_tots,
count(dates) OVER (partition BY patient_id ORDER BY patient_id, dates) as running_count
into dbo.highopi_temp_4
FROM dbo.highopi_temp_3
GROUP BY patient_id, date_of_birth, natural_key, dates
HAVING SUM(morphine_equiv) >= 100;

-- Find the date the patient reaches 90 days and use this as the condition start_date
SELECT patient_id, 
date_of_birth, 
natural_key, 
running_count, 
dates as cond_start_date, 
dates + 365 cond_expire_date, 
dates as disease_date, 
text('prescribed >= 100 milligrams of morphine equivalents per day for >= 90 days within the past year') criteria,
text('highopioiduse') local_condition,
dates - CONVERT(date, '01/01/1960', 101)  start_date_mdphnet
into dbo.highopi_temp_5
FROM highopi_temp_4
WHERE running_count = 90
ORDER BY patient_id;

-- Union - UNION the results together and exclude duplicates
SELECT patient_id,natural_key,date_of_birth,disease_date,local_condition,local_criteria,start_date_mdphnet, disease_date cond_start_date, cond_expire_date 
inyo dbo.opi_a_100007_s_19
FROM dbo.opi_a_100007_s_10
UNION (SELECT patient_id,natural_key,date_of_birth,disease_date,local_condition,local_criteria,start_date_mdphnet, disease_date cond_start_date, cond_expire_date FROM dbo.opi_a_100007_s_13) 
UNION (SELECT patient_id,natural_key,date_of_birth,disease_date,local_condition,criteria,start_date_mdphnet, cond_start_date, cond_expire_date FROM dbo.highopi_temp_5);

-- Join - STANDARD - Find patients who don't already have this condition starting on this date.
SELECT T1.patient_id,T1.natural_key,T1.disease_date,T1.date_of_birth,T1.start_date_mdphnet,T1.local_condition,T1.local_criteria,T1.cond_start_date, T1.cond_expire_date
into dbo.opi_a_100007_s_20
FROM dbo.opi_a_100007_s_19 T1 
LEFT OUTER JOIN dbo.esp_condition T2 ON ((T1.natural_key = T2.patid) 
AND (T1.start_date_mdphnet = T2.date) 
AND (T1.local_condition = T2.condition))  
WHERE (T2.patid is null) ;

-- Derive Columns - STANDARD - local_notes, local_status, age_at_detect_year, centerid
SELECT T1.*,
case when date_of_birth = '1900-01-01' then null 
  else datediff(day, disease_date, date_of_birth)/365.25 end age_at_detect_year, 
CAST('1' AS CHARACTER VARYING(1)) centerid, text('') local_notes,text('NO') local_status 
into dbo.opi_a_100007_s_21
FROM dbo.opi_a_100007_s_20 T1;

-- Derive Columns - STANDRD - age_group_10yr, age_group_5yr, age_group_ms
SELECT T1.*,CASE when (age_at_detect_year <= 9) then '0-9'
when (age_at_detect_year <=19) then '10-19' 
when (age_at_detect_year <=29) then '20-29' 
when (age_at_detect_year <=39) then '30-39' 
when (age_at_detect_year <=49) then '40-49' 
when (age_at_detect_year <=59) then '50-59' 
when (age_at_detect_year <=69) then '60-69' 
when (age_at_detect_year <=79) then '70-79' 
when (age_at_detect_year <=89) then '80-89' 
when (age_at_detect_year <=99) then '90-99' 
else
'100+'
end age_group_10yr,CASE when  (age_at_detect_year) <= 4  THEN  '0-4'
when age_at_detect_year <= 9  THEN '5-9'
when age_at_detect_year <= 14 THEN '10-14'
when age_at_detect_year <= 19 THEN '15-19'
when age_at_detect_year <= 24 THEN '20-24'
when age_at_detect_year <= 29 THEN '25-29'
when age_at_detect_year <= 34 THEN '30-34'
when age_at_detect_year <= 39 THEN '35-39'
when age_at_detect_year <= 44 THEN '40-44'
when age_at_detect_year <= 49 THEN '45-49'
when age_at_detect_year <= 54 THEN '50-54'
when age_at_detect_year <= 59 THEN '55-59'
when age_at_detect_year <= 64 THEN '60-64'
when age_at_detect_year <= 69 THEN '65-69'
when age_at_detect_year <= 74 THEN '70-74'
when age_at_detect_year <= 79 THEN '75-79'
when age_at_detect_year <= 84 THEN '80-84'
when age_at_detect_year <= 89 THEN '85-89'
when age_at_detect_year <= 94 THEN '90-94'
when age_at_detect_year <= 99 THEN '95-99'
ELSE
'100+'
END age_group_5yr,CASE when age_at_detect_year <= 1  THEN '0-1'
when age_at_detect_year <= 4 THEN '2-4'
when age_at_detect_year <= 9 THEN '5-9'
when age_at_detect_year <= 14 THEN '10-14'
when age_at_detect_year <= 18 THEN '15-18'
when age_at_detect_year <= 21 THEN '19-21'
when age_at_detect_year <= 44 THEN '22-44'
when age_at_detect_year <= 64 THEN '45-64'
when age_at_detect_year <= 74 THEN '65-74'
ELSE
'75+'
END  age_group_ms 
into dbo.opi_a_100007_s_22
FROM dbo.opi_a_100007_s_21 T1;

-- Subset Columns - STANDARD - Prepare Output
SELECT "centerid","natural_key","local_condition","start_date_mdphnet","age_at_detect_year","age_group_5yr","age_group_10yr","age_group_ms","local_criteria","local_status","local_notes", "cond_start_date", "cond_expire_date" 
into dbo.opi_a_100007_s_23
FROM dbo.opi_a_100007_s_22;

-- Save - STANDARD-- Populate ccda_opibenzo table
SELECT *
into ccda_opibenzo 
FROM dbo.opi_a_100007_s_23;

-- Populate the ESP Condition Table
INSERT INTO esp_condition (SELECT * from ccda_opibenzo);

--
-- Script shutdown section 
--
IF object_id('dbo.opi_a_100007_s_6') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_6;
IF object_id('dbo.opi_a_100007_s_7') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_7;
IF object_id('dbo.opi_a_100007_s_8') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_8;
IF object_id('dbo.opi_a_100007_s_10') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_10;
IF object_id('dbo.opi_a_100007_s_11') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_11;
IF object_id('dbo.opi_a_100007_s_12') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_12;
IF object_id('dbo.opi_a_100007_s_13') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_13;
IF object_id('dbo.opi_a_100007_s_14') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_14;
IF object_id('dbo.opi_a_100007_s_15') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_15;
IF object_id('dbo.opi_a_100007_s_16') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_16;
IF object_id('dbo.opi_a_100007_s_19') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_19;
IF object_id('dbo.opi_a_100007_s_20') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_20;
IF object_id('dbo.opi_a_100007_s_21') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_21;
IF object_id('dbo.opi_a_100007_s_22') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_22;
IF object_id('dbo.opi_a_100007_s_23') IS NOT NULL DROP TABLE dbo.opi_a_100007_s_23;
IF object_id('dbo.highopi_temp_1') IS NOT NULL DROP TABLE dbo.highopi_temp_1;
IF object_id('dbo.highopi_temp_2') IS NOT NULL DROP TABLE dbo.highopi_temp_2;
IF object_id('dbo.highopi_temp_3') IS NOT NULL DROP TABLE dbo.highopi_temp_3;
IF object_id('dbo.highopi_temp_4') IS NOT NULL DROP TABLE dbo.highopi_temp_4;
IF object_id('dbo.highopi_temp_5') IS NOT NULL DROP TABLE dbo.highopi_temp_5;
